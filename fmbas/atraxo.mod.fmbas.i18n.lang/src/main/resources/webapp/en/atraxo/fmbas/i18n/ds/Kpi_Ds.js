Ext.override(atraxo.fmbas.i18n.ds.Kpi_Ds, {
	active__lbl: "Active?",
	createdBy__lbl: "Created by",
	description__lbl: "Description",
	downLimit__lbl: "Down limit",
	dsName__lbl: "Data store name",
	frameBundle__lbl: "Bundle",
	frameId__lbl: "Menu Item (ID)",
	frameName__lbl: "Frame",
	frameTitle__lbl: "Title",
	glyphCode__lbl: "Glyph code",
	glyphFont__lbl: "Glyph font",
	kpiOrder__lbl: "KPI order",
	methodName__lbl: "Method name",
	reverse__lbl: "Reverse",
	titleAr__lbl: "Title AR",
	titleDe__lbl: "Title DE",
	titleEn__lbl: "Title EN",
	titleEs__lbl: "Title ES",
	titleFr__lbl: "Title FR",
	titleJp__lbl: "Title JP",
	title__lbl: "Title",
	unit__lbl: "Unit",
	upLimit__lbl: "Up limit"
});
