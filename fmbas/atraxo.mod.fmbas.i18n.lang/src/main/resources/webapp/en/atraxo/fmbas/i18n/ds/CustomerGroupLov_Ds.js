Ext.override(atraxo.fmbas.i18n.ds.CustomerGroupLov_Ds, {
	code__lbl: "Customer code",
	code__tlp: "Customer code",
	customerId__lbl: "Customer ID",
	customerParentId__lbl: "Customer parent ID",
	id__lbl: "ID",
	isCustomer__lbl: "Is customer?",
	isCustomer__tlp: "Is customer?",
	parentGroupCode__lbl: "Parent",
	parentGroupCode__tlp: "Customer code",
	parentGroupId__lbl: "Parent (ID)",
	type__lbl: "Company type",
	type__tlp: "Company type"
});
