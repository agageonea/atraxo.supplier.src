Ext.override(atraxo.fmbas.i18n.frame.Dashboard_Ui, {
	/* view */
	Dashboards__ttl: "Dashboards",
	Layouts__ttl: "Layouts",
	Widgets__ttl: "Widgets",
	dashboardWidgetList__ttl: "Assigned Widgets",
	wdwAssignedParams__ttl: "Edit Widget Parameter",
	widgetParameterList__ttl: "Parameters",
	/* menu */
	/* button */
	btnWidgetParams__lbl: "Parameters",
	btnWidgetParams__tlp: "Parameters",
	cancelParameter__lbl: "Cancel",
	cancelParameter__tlp: "Cancel",
	updateParameter__lbl: "Update",
	updateParameter__tlp: "Update",
	
	title: "Dashboards & Widgets"
});
