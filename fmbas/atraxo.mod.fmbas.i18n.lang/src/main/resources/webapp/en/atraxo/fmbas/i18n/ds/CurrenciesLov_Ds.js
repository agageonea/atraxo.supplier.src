Ext.override(atraxo.fmbas.i18n.ds.CurrenciesLov_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	code__lbl: "Currency code",
	code__tlp: "Currency code",
	name__lbl: "Name",
	name__tlp: "Name"
});
