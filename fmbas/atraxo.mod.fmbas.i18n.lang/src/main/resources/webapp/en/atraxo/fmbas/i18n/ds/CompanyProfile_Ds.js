Ext.override(atraxo.fmbas.i18n.ds.CompanyProfile_Ds, {
	code__lbl: "Code",
	code__tlp: "Code",
	labelField__lbl: "",
	name__lbl: "Organization name",
	name__tlp: "Organization name",
	sysCurrency__lbl: "",
	sysCurrency__tlp: "System currency",
	sysFinancialSource__lbl: "",
	sysFinancialSource__tlp: "Default financial source",
	sysUnits__lbl: "",
	sysUnits__tlp: "System of units"
});
