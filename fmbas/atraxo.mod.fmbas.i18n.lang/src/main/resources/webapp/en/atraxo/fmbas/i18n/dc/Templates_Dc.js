
Ext.override(atraxo.fmbas.i18n.dc.Templates_Dc$Edit, {
	fileName__lbl: "File name",
	systemUse__lbl: "System use"
});

Ext.override(atraxo.fmbas.i18n.dc.Templates_Dc$Filter, {
	fileName__lbl: "Source file name"
});

Ext.override(atraxo.fmbas.i18n.dc.Templates_Dc$List, {
	fileName__lbl: "Source file name",
	systemUse__lbl: "System use",
	typeName__lbl: "Used for",
	userName__lbl: "Loaded by"
});
