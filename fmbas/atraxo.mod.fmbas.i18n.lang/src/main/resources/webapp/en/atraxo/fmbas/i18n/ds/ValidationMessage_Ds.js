Ext.override(atraxo.fmbas.i18n.ds.ValidationMessage_Ds, {
	labelField__lbl: "",
	message__lbl: "Validation Message",
	message__tlp: "Validation Message",
	objectId__lbl: "Object Reference ID",
	objectType__lbl: "Object type",
	severity__lbl: "Severity",
	severity__tlp: "Severity"
});
