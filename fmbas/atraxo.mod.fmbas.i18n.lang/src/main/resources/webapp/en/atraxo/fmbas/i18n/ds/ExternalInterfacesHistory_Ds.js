Ext.override(atraxo.fmbas.i18n.ds.ExternalInterfacesHistory_Ds, {
	externalInterfaceId__lbl: "External Interface (ID)",
	externalInterfaceName__lbl: "External interface",
	externalInterfaceName__tlp: "Name",
	requestReceived__lbl: "Request received",
	requestReceived__tlp: "Request received",
	requester__lbl: "Requester",
	requester__tlp: "Requester",
	responseSent__lbl: "Response sent",
	responseSent__tlp: "Response sent",
	result__lbl: "Result",
	result__tlp: "Result",
	status__lbl: "Status",
	status__tlp: "Status"
});
