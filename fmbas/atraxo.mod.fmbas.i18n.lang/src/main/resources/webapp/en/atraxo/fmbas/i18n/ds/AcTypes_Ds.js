Ext.override(atraxo.fmbas.i18n.ds.AcTypes_Ds, {
	active__lbl: "Active?",
	active__tlp: "Active?",
	burnoffProH__lbl: "Fuel consumption/hour",
	burnoffProH__tlp: "Fuel consumption/hour",
	code__lbl: "IATA code",
	code__tlp: "IATA code",
	name__lbl: "Description",
	name__tlp: "Description",
	tankCapacity__lbl: "Tank capacity",
	tankCapacity__tlp: "Tank capacity",
	unitCode__lbl: "Unit",
	unitCode__tlp: "Code",
	unitId__lbl: "Unit (ID)"
});
