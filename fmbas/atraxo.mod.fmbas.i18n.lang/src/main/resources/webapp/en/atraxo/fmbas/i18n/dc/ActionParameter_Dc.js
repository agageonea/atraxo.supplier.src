
Ext.override(atraxo.fmbas.i18n.dc.ActionParameter_Dc$EditList, {
	defaultValue__lbl: "Default value",
	description__lbl: "Description",
	name__lbl: "Name",
	readOnly__lbl: "Read only",
	type__lbl: "Parameter type",
	value__lbl: "Parameter value"
});
