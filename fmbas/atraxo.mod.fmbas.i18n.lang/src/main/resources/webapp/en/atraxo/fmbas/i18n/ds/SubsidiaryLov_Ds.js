Ext.override(atraxo.fmbas.i18n.ds.SubsidiaryLov_Ds, {
	code__lbl: "Account code",
	code__tlp: "Account code",
	isCustomer__lbl: "Is customer?",
	isCustomer__tlp: "Is customer?",
	isSubsidiary__lbl: "Is subsidiary?",
	name__lbl: "Subsidiary name",
	name__tlp: "Subsidiary name",
	refid__lbl: "RefID",
	status__lbl: "Status",
	status__tlp: "Status",
	type__lbl: "Type",
	type__tlp: "Type"
});
