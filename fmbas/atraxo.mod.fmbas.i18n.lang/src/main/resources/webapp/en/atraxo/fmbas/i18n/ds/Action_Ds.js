Ext.override(atraxo.fmbas.i18n.ds.Action_Ds, {
	batchJobId__lbl: "Batch job (ID)",
	batchJobName__lbl: "Batch job",
	description__lbl: "Description",
	jobChainId__lbl: "Job chain (ID)",
	name__lbl: "Name",
	order__lbl: "Order"
});
