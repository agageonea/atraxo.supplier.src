Ext.override(atraxo.fmbas.i18n.ds.EmailTemplateLov_Ds, {
	code__lbl: "Name",
	code__tlp: "Name",
	fileName__lbl: "Source file name",
	fileName__tlp: "File name",
	fileReference__lbl: "File reference",
	fileReference__tlp: "File reference",
	subsidiaryCode__lbl: "Subsidiary code",
	subsidiaryCode__tlp: "Account code",
	subsidiaryId__lbl: "Subsidiary (ID)",
	typeId__lbl: "Type (ID)",
	typeName__lbl: "Used for",
	typeName__tlp: "Name",
	uploadDate__lbl: "Upload date",
	uploadDate__tlp: "Upload date",
	userCode__lbl: "User",
	userId__lbl: "User (ID)",
	userName__lbl: "Loaded by"
});
