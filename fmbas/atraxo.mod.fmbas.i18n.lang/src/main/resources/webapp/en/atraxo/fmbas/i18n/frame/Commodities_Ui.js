Ext.override(atraxo.fmbas.i18n.frame.Commodities_Ui, {
	/* view */
	/* menu */
	/* button */
	btnCancelAll__lbl: "Cancel all",
	btnCancelAll__tlp: "Cancel all",
	btnCancelSelected__lbl: "Cancel",
	btnCancelSelected__tlp: "Cancel",
	btnCancelWithMenu__lbl: "Cancel",
	btnCancelWithMenu__tlp: "Either abort all changes, or only the changes of the selected line.",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	
	title: "Commodities"
});
