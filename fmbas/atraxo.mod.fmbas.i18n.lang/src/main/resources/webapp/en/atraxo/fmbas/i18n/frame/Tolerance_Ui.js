Ext.override(atraxo.fmbas.i18n.frame.Tolerance_Ui, {
	/* view */
	wdwTolerance__ttl: "Tolerance",
	/* menu */
	/* button */
	btnOnEditWdwClose__lbl: "Cancel",
	btnOnEditWdwClose__tlp: "Cancel",
	btnSaveCloseWdw__lbl: "Save & Close",
	btnSaveCloseWdw__tlp: "Save & Close",
	btnSaveNewWdw__lbl: "Save & New",
	btnSaveNewWdw__tlp: "Save & New",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	helpWdw1__lbl: "Help",
	helpWdw1__tlp: "Help",
	
	title: "Tolerances"
});
