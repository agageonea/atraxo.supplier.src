Ext.override(atraxo.fmbas.i18n.ds.Layout_Ds, {
	cfgPath__lbl: "Path to configuration file",
	cfgPath__tlp: "Path to configuration file",
	description__lbl: "Description",
	description__tlp: "Description",
	name__lbl: "Name",
	name__tlp: "Name",
	thumbPath__lbl: "Thumbnail path",
	thumbPath__tlp: "Thumbnail path"
});
