Ext.override(atraxo.fmbas.i18n.ds.UnitsLov_Ds, {
	code__lbl: "Code",
	code__tlp: "Code",
	iataCode__lbl: "IATA code",
	iataCode__tlp: "IATA code",
	id__lbl: "ID",
	name__lbl: "Name",
	name__tlp: "Name",
	unitTypeInd__lbl: "Unit type indicator",
	unitTypeInd__tlp: "Unit type indicator"
});
