Ext.override(atraxo.fmbas.i18n.ds.ExchangeRateToFinancialSourceAndAvg_Ds, {
	avgMthdCode__lbl: "Averaging method",
	avgMthdCode__tlp: "Code",
	avgMthdId__lbl: "Averaging Method Indicator (ID)",
	avgMthdName__lbl: "Averaging method",
	avgMthdName__tlp: "Name",
	currencyCode__lbl: "Currency1",
	currencyCode__tlp: "Currency code",
	currencyId__lbl: "Currency1 (ID)",
	financialSourceCode__lbl: "Financial source",
	financialSourceCode__tlp: "Code",
	financialSourceId__lbl: "Finsource (ID)",
	financialSourceName__lbl: "Financial source",
	financialSourceName__tlp: "Name",
	name__lbl: "Name",
	name__tlp: "Name"
});
