Ext.override(atraxo.fmbas.i18n.ds.CompositeTimeSeriesSource_Ds, {
	currCode__lbl: "Currency 1 ID",
	currCode__tlp: "Currency code",
	currencyId__lbl: "Currency1 ID (ID)",
	factor__lbl: "Factor",
	financialSourceCode__lbl: "Financial source",
	financialSourceCode__tlp: "Code",
	financialSourceId__lbl: "Financial Source (ID)",
	timeSerieCompId__lbl: "Composite (ID)",
	timeSerieDescription__lbl: "Description",
	timeSerieDescription__tlp: "Description",
	timeSerieId__lbl: "Source (ID)",
	timeSerieName__lbl: "Series name",
	timeSerieName__tlp: "Series name",
	unitCode__lbl: "Unit ID",
	unitCode__tlp: "Code",
	unitId__lbl: "Unit ID (ID)",
	weighting__lbl: "Weighting"
});
