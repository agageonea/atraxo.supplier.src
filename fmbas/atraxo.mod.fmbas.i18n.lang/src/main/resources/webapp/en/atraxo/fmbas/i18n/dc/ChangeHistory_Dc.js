
Ext.override(atraxo.fmbas.i18n.dc.ChangeHistory_Dc$ListReason, {
	createdAt__lbl: "Date",
	createdBy__lbl: "User",
	objectValue__lbl: "Action performed",
	remarks__lbl: "Reason"
});
