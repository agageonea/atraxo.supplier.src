Ext.override(atraxo.fmbas.i18n.ds.StepExecution_Ds, {
	commitCount__lbl: "Commit Count",
	endTime__lbl: "End time",
	exitCode__lbl: "Exit Code",
	exitMessage__lbl: "Exit message",
	jobExecStepExitCode__lbl: "Exit Code",
	jobExecStepExitMessage__lbl: "Exit message",
	jobExecStepId__lbl: "Job Exec Step(ID)",
	jobExecStepStatus__lbl: "Released status",
	jobInstanceId__lbl: "Job Instance (ID)",
	jobInstanceKey__lbl: "Job Key",
	jobName__lbl: "Job name",
	readCount__lbl: "Read Count",
	rollbackCount__lbl: "Rollback Count",
	startTime__lbl: "Start Time",
	status__lbl: "Invoice status",
	writeCount__lbl: "Write Count"
});
