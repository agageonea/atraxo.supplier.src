Ext.override(atraxo.fmbas.i18n.frame.AcTypes_Ui, {
	/* view */
	/* menu */
	/* button */
	btnCancelAll__lbl: "Cancel all",
	btnCancelAll__tlp: "Cancel all",
	btnCancelSelected__lbl: "Cancel",
	btnCancelSelected__tlp: "Cancel",
	btnCancelWithMenu__lbl: "Cancel",
	btnCancelWithMenu__tlp: "Either abort all changes, or only the changes of the selected line.",
	btnDelete__lbl: "Delete",
	btnDelete__tlp: "Delete",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	
	title: "Aircraft Types"
});
