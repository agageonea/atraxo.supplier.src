
Ext.override(atraxo.fmbas.i18n.dc.BankAccount_Dc$BankAccountList, {
	accountHolder__lbl: "Account holder",
	accountNumber__lbl: "Account #",
	bankAdress__lbl: "Bank address",
	bankName__lbl: "Bank name",
	branchCode__lbl: "Branch code",
	branchName__lbl: "Branch name",
	currency__lbl: "Currency",
	customerCodes__lbl: "For customer",
	customerCodes__tlp: "Customer code(s)",
	iban__lbl: "IBAN",
	subsidiary__lbl: "For subsidiary",
	swift__lbl: "SWIFT"
});

Ext.override(atraxo.fmbas.i18n.dc.BankAccount_Dc$Filter, {
	accountHolder__lbl: "Account holder",
	accountNumber__lbl: "Account #",
	bankAdress__lbl: "Bank address",
	bankName__lbl: "Bank name",
	branchCode__lbl: "Branch code",
	branchName__lbl: "Branch name",
	currency__lbl: "Currency",
	customerCodes__lbl: "For customer",
	iban__lbl: "IBAN",
	subsidiary__lbl: "Subsidiary",
	swift__lbl: "SWIFT"
});
