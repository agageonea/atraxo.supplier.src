Ext.override(atraxo.fmbas.i18n.ds.BatchJobParameter_Ds, {
	assignType__lbl: "Assign type",
	batchJobId__lbl: "Batch Job (ID)",
	batchJobName__lbl: "Batch job",
	description__lbl: "Description",
	name__lbl: "Name",
	readOnly__lbl: "Read only",
	refBusinessValues__lbl: "Reference business values",
	refValues__lbl: "Possible values",
	type__lbl: "Type",
	value__lbl: "Value"
});
