
Ext.override(atraxo.fmbas.i18n.dc.ActionResultHistory_Dc$Filter, {
	duration__lbl: "Duration",
	endTime__lbl: "Finished at",
	exitCode__lbl: "Result",
	exitMessage__lbl: "Error",
	name__lbl: "Action name",
	startTime__lbl: "Started at"
});

Ext.override(atraxo.fmbas.i18n.dc.ActionResultHistory_Dc$List, {
	duration__lbl: "Duration",
	endTime__lbl: "Finished at",
	exitCode__lbl: "Result",
	msg__lbl: "Result message",
	name__lbl: "Action name",
	startTime__lbl: "Started at"
});
