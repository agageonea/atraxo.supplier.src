Ext.override(atraxo.fmbas.i18n.ds.ExternalInterfaces_Ds, {
	description__lbl: "Description",
	description__tlp: "Description",
	enabled__lbl: "Endpoint status",
	enabled__tlp: "Enabled?",
	externalInterfaceType__lbl: "Type",
	externalInterfaceType__tlp: "Type",
	lastRequestStatus__lbl: "Last request status",
	lastRequestStatus__tlp: "Last request status",
	lastRequest__lbl: "Last request",
	lastRequest__tlp: "Last request",
	name__lbl: "Web service name",
	name__tlp: "Name"
});
