/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.payableAccruals.service;

import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import atraxo.acc.business.api.payableAccruals.IPayableAccrualService;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.acc.presenter.impl.payableAccruals.model.PayableAccrualLocGrouping_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class PayableAccrualLocGrouping_DsService
		extends AbstractEntityDsService<PayableAccrualLocGrouping_Ds, PayableAccrualLocGrouping_Ds, Object, PayableAccrual>
		implements IDsService<PayableAccrualLocGrouping_Ds, PayableAccrualLocGrouping_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<PayableAccrualLocGrouping_Ds, PayableAccrualLocGrouping_Ds, Object> builder,
			List<PayableAccrualLocGrouping_Ds> result) throws Exception {
		super.postFind(builder, result);
		IPayableAccrualService payableAccrualService = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		List<PayableAccrual> payableAccruals = payableAccrualService.findAll();
		String filter = builder.getFilter().getMonth();
		HashMap<String, PayableAccrualDsStore> hashMap = new HashMap<>();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
		for (PayableAccrual pa : payableAccruals) {
			if (filter.equalsIgnoreCase(sdf.format(this.formatDate(pa.getFuelingDate())))) {
				String key = pa.getLocation().getCode();
				if (hashMap.containsKey(key)) {
					PayableAccrualDsStore store = hashMap.get(key);
					store.setDate(pa.getFuelingDate());
					store.setLocationCode(key);
					store.setEvents(store.getEvents() + 1);
					store.setNetAmount(store.getNetAmount().add(pa.getAccrualAmountSys(), MathContext.DECIMAL64));
					store.setVatAmount(store.getVatAmount().add(pa.getAccrualVATSys(), MathContext.DECIMAL64));
					store.setGrossAmount(store.getNetAmount().add(store.getVatAmount(), MathContext.DECIMAL64));
				} else {
					PayableAccrualDsStore store = new PayableAccrualDsStore();
					store.setLocationCode(key);
					store.setDate(pa.getFuelingDate());
					store.setEvents(1);
					store.setNetAmount(pa.getAccrualAmountSys());
					store.setVatAmount(pa.getAccrualVATSys());
					store.setGrossAmount(store.getNetAmount().add(store.getVatAmount(), MathContext.DECIMAL64));
					hashMap.put(key, store);
				}
			}
		}

		List<PayableAccrualLocGrouping_Ds> retList = new ArrayList<>();
		for (PayableAccrualDsStore store : hashMap.values()) {
			PayableAccrualLocGrouping_Ds ds = new PayableAccrualLocGrouping_Ds();
			ds.setEvents(store.getEvents());
			ds.setMonth(sdf.format(store.getDate()));
			ds.setGrossAmount(store.getGrossAmount());
			ds.setVatAmount(store.getVatAmount());
			ds.setNetAmount(store.getNetAmount());
			ds.setLocation(store.getLocationCode());
			retList.add(ds);
		}
		result.addAll(retList);
		result.retainAll(retList);
	}

	@Override
	public Long count(IQueryBuilder<PayableAccrualLocGrouping_Ds, PayableAccrualLocGrouping_Ds, Object> builder) throws Exception {
		IPayableAccrualService payableAccrualService = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		List<PayableAccrual> payableAccruals = payableAccrualService.findAll();
		HashMap<Date, Integer> hashMap = new HashMap<>();
		for (PayableAccrual pa : payableAccruals) {
			Date usedDate = this.formatDate(pa.getFuelingDate());
			if (hashMap.containsKey(usedDate)) {
				Integer count = hashMap.get(usedDate);
				count = count + 1;
			} else {
				hashMap.put(usedDate, 1);
			}
		}
		return new Long(hashMap.size());
	}

	private Date formatDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

}
