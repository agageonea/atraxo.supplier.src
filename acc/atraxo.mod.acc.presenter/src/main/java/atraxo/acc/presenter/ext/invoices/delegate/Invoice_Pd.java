package atraxo.acc.presenter.ext.invoices.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import atraxo.acc.business.api.invoices.IInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.presenter.impl.invoices.model.Invoice_Ds;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class Invoice_Pd extends AbstractPresenterDelegate {

	private static final String APPROVE = "Approve";
	private static final String SUBMIT_FOR_APPROVAL = "Submit for approval";
	private static final String RECHECK = "Recheck";
	private static final String FINISH_INPUT = "Finish input";

	public void reset(Invoice_Ds invoice) throws Exception {
		List<Invoice_Ds> invoices = new ArrayList<>();
		invoices.add(invoice);
		this.resetList(invoices);
	}

	public void resetList(List<Invoice_Ds> invoices) throws Exception {
		IInvoiceService invoiceService = (IInvoiceService) this.findEntityService(Invoice.class);
		List<Invoice> invoiceList = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		for (Invoice_Ds i : invoices) {
			Invoice invTemp = invoiceService.findById(i.getId());
			if (!BillStatus._PAID_.equals(invTemp.getStatus()) && !BillStatus._DRAFT_.equals(invTemp.getStatus())) {
				invoiceList.add(invTemp);
			} else {
				sb.append("<li>").append(String.format(AccErrorCode.INVOICE_DRAFT_PAID.getErrMsg(), i.getInvoiceNo())).append("</li>");
			}
		}
		invoiceService.resetInvoice(invoiceList);
		sb.append("</ul>");
		String msg = sb.toString();
		if (msg.contains("<li>")) {
			throw new BusinessException(AccErrorCode.INVOICE_RPC_ACTION_ERROR, String.format(AccErrorCode.INVOICE_RPC_ACTION_ERROR.getErrMsg(), msg));
		}

	}

	public void finishInput(Invoice_Ds invoice) throws Exception {
		IInvoiceService service = (IInvoiceService) this.findEntityService(Invoice.class);
		Invoice inv = service.findById(invoice.getId());
		if (BillStatus._DRAFT_.equals(inv.getStatus())) {
			service.finishInput(inv, FINISH_INPUT);
		} else {
			throw new BusinessException(AccErrorCode.INVOICE_FINISH_INPUT_DRAFT, AccErrorCode.INVOICE_FINISH_INPUT_DRAFT.getErrMsg());
		}
	}

	public void recheck(Invoice_Ds invoice) throws Exception {
		IInvoiceService service = (IInvoiceService) this.findEntityService(Invoice.class);
		Invoice inv = service.findById(invoice.getId());
		if (BillStatus._DRAFT_.equals(inv.getStatus())) {
			service.finishInput(inv, RECHECK);
		} else {
			throw new BusinessException(AccErrorCode.INVOICE_FINISH_INPUT_DRAFT, AccErrorCode.INVOICE_FINISH_INPUT_DRAFT.getErrMsg());
		}
	}

	public void submit(Invoice_Ds ds) throws Exception {
		this.submitList(Arrays.asList(ds));
	}

	public void submitList(List<Invoice_Ds> dss) throws Exception {
		this.setStatus(dss, SUBMIT_FOR_APPROVAL, BillStatus._AWAITING_APPROVAL_, BillStatus._CHECK_PASSED_, AccErrorCode.INVOICE_STATUS_CHECK_PASSED);
	}

	public void release(Invoice_Ds ds) throws Exception {
		this.releaseList(Arrays.asList(ds));
	}

	public void releaseList(List<Invoice_Ds> dss) throws Exception {
		for (Invoice_Ds invoice : dss) {
			if (Calendar.getInstance().getTime().before(invoice.getIssueDate())) {
				throw new BusinessException(AccErrorCode.INVOICE_DATE_AFTER_TODAY, AccErrorCode.INVOICE_DATE_AFTER_TODAY.getErrMsg());
			}
		}
		this.setStatus(dss, APPROVE, BillStatus._AWAITING_PAYMENT_, BillStatus._AWAITING_APPROVAL_, AccErrorCode.INVOICE_STATUS_AWAITING_APPROVAL);
	}

	public void pay(Invoice_Ds ds) throws Exception {
		this.payList(Arrays.asList(ds));
	}

	public void payList(List<Invoice_Ds> dss) throws Exception {
		this.setStatus(dss, BillStatus._PAID_.getName(), BillStatus._PAID_, BillStatus._AWAITING_PAYMENT_,
				AccErrorCode.INVOICE_STATUS_AWAITING_FOR_PAYMENT);
	}

	private void setStatus(List<Invoice_Ds> dss, String action, BillStatus status, BillStatus condition, AccErrorCode error) throws Exception {
		IInvoiceService invoiceService = (IInvoiceService) this.findEntityService(Invoice.class);
		List<Invoice> invoices = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		for (Invoice_Ds ds : dss) {
			Invoice invoice = invoiceService.findByBusiness(ds.getId());
			if (!condition.equals(invoice.getStatus())) {
				sb.append("<li>").append(String.format(error.getErrMsg(), invoice.getInvoiceNo())).append("</li>");
			} else {
				invoices.add(invoice);
			}
		}
		invoiceService.modifyStatus(invoices, action, status);
		sb.append("</ul>");
		String msg = sb.toString();
		if (msg.contains("<li>")) {
			throw new BusinessException(AccErrorCode.INVOICE_RPC_ACTION_ERROR, String.format(AccErrorCode.INVOICE_RPC_ACTION_ERROR.getErrMsg(), msg));
		}
	}

}
