/**
 *
 */
package atraxo.acc.presenter.ext.invoices.service;

import org.joda.time.DateTimeComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.ext.exceptions.workflow.ClaimedWorkflowException;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * @author zspeter
 */
public abstract class AbstractCreditMemoService extends AbstractPresenterBaseService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCreditMemoService.class);

	protected static final String UL_START = "<ul>";
	protected static final String UL_END = "</ul>";
	protected static final String LI_START = "<li>";
	protected static final String LI_END = "</li>";
	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;
	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntitySrv;
	@Autowired
	private IWorkflowBpmManager workflowBpmManager;

	protected synchronized void completeCurrentTask(Integer invoiceId, boolean accept, String note) throws BusinessException {

		OutgoingInvoice outInvoice = this.outgoingInvoiceService.findById(invoiceId);

		if (!outInvoice.getApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
			throw new BusinessException(AccErrorCode.CREDIT_MEMO_WORKFLOW_APPROVED, AccErrorCode.CREDIT_MEMO_WORKFLOW_APPROVED.getErrMsg());
		}

		if (DateTimeComparator.getDateOnlyInstance().compare(outInvoice.getInvoiceDate(), outInvoice.getDeliverydateTo()) < 0) {
			throw new BusinessException(AccErrorCode.INVOICE_DATE_GRATER_THAN_DATE_TO, AccErrorCode.INVOICE_DATE_GRATER_THAN_DATE_TO.getErrMsg());
		}

		WorkflowInstanceEntity workflowInstanceEntity = this.workflowInstanceEntitySrv
				.findByBusinessKey(WorkflowNames.CREDIT_MEMO_APPROVAL.getWorkflowName(), outInvoice.getId(), outInvoice.getClass().getSimpleName());

		try {
			this.workflowBpmManager.claimCompleteTask(workflowInstanceEntity.getWorkflowInstance().getId(), accept, note);
		} catch (ClaimedWorkflowException e) {
			throw e;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			this.workflowBpmManager.terminateWorkflow(workflowInstanceEntity.getWorkflowInstance().getId(),
					AccErrorCode.CREDIT_MEMO_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage());
			throw new BusinessException(AccErrorCode.CREDIT_MEMO_WORKFLOW_IDENTIFICATION_ERROR,
					AccErrorCode.CREDIT_MEMO_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage(), e);
		}
	}

}
