/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.fuelEvents.model;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelEvent.class, jpqlWhere = "e.billPriceSourceType = 'FuelOrderLocation'")
public class FuelEventPurchaseOrder_Ds
		extends
			AbstractSubsidiaryDs_Ds<FuelEvent> {

	public static final String ALIAS = "acc_FuelEventPurchaseOrder_Ds";

	public static final String F_EVENTTYPE = "eventType";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_BILLABLECOST = "billableCost";
	public static final String F_BILLABELCOSTCURRENCY = "billabelCostCurrency";
	public static final String F_ORDERCODE = "orderCode";
	public static final String F_OPERATIONALTYPE = "operationalType";
	public static final String F_PRODUCT = "product";
	public static final String F_CONVERTEDAMOUNT = "convertedAmount";
	public static final String F_SYSTEMCURRENCY = "systemCurrency";
	public static final String F_ORDERLOCATIONID = "orderLocationId";
	public static final String F_FUELINGDATE = "fuelingDate";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_QUANTITY = "quantity";

	@DsField
	private FlightTypeIndicator eventType;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField
	private BigDecimal billableCost;

	@DsField(join = "left", path = "billableCostCurrency.code")
	private String billabelCostCurrency;

	@DsField(fetch = false)
	private String orderCode;

	@DsField(fetch = false)
	private String operationalType;

	@DsField(fetch = false)
	private String product;

	@DsField(fetch = false)
	private BigDecimal convertedAmount;

	@DsField(fetch = false)
	private String systemCurrency;

	@DsField(path = "billPriceSourceId")
	private Integer orderLocationId;

	@DsField
	private Date fuelingDate;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField
	private BigDecimal quantity;

	/**
	 * Default constructor
	 */
	public FuelEventPurchaseOrder_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelEventPurchaseOrder_Ds(FuelEvent e) {
		super(e);
	}

	public FlightTypeIndicator getEventType() {
		return this.eventType;
	}

	public void setEventType(FlightTypeIndicator eventType) {
		this.eventType = eventType;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public BigDecimal getBillableCost() {
		return this.billableCost;
	}

	public void setBillableCost(BigDecimal billableCost) {
		this.billableCost = billableCost;
	}

	public String getBillabelCostCurrency() {
		return this.billabelCostCurrency;
	}

	public void setBillabelCostCurrency(String billabelCostCurrency) {
		this.billabelCostCurrency = billabelCostCurrency;
	}

	public String getOrderCode() {
		return this.orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getOperationalType() {
		return this.operationalType;
	}

	public void setOperationalType(String operationalType) {
		this.operationalType = operationalType;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public BigDecimal getConvertedAmount() {
		return this.convertedAmount;
	}

	public void setConvertedAmount(BigDecimal convertedAmount) {
		this.convertedAmount = convertedAmount;
	}

	public String getSystemCurrency() {
		return this.systemCurrency;
	}

	public void setSystemCurrency(String systemCurrency) {
		this.systemCurrency = systemCurrency;
	}

	public Integer getOrderLocationId() {
		return this.orderLocationId;
	}

	public void setOrderLocationId(Integer orderLocationId) {
		this.orderLocationId = orderLocationId;
	}

	public Date getFuelingDate() {
		return this.fuelingDate;
	}

	public void setFuelingDate(Date fuelingDate) {
		this.fuelingDate = fuelingDate;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
}
