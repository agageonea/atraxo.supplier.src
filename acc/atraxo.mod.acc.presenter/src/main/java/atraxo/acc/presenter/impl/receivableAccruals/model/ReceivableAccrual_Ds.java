/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.receivableAccruals.model;

import atraxo.acc.domain.impl.acc_type.InvoiceProcessStatus;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.ops_type.FuelQuoteType;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ReceivableAccrual.class, sort = {@SortField(field = ReceivableAccrual_Ds.F_FUELINGDATE, desc = true)})
@RefLookups({
		@RefLookup(refId = ReceivableAccrual_Ds.F_FUELEVENTID),
		@RefLookup(refId = ReceivableAccrual_Ds.F_LOCID),
		@RefLookup(refId = ReceivableAccrual_Ds.F_CUSTID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ReceivableAccrual_Ds.F_CUSTCODE)}),
		@RefLookup(refId = ReceivableAccrual_Ds.F_SUPPID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = ReceivableAccrual_Ds.F_SUPPCODE)}),
		@RefLookup(refId = ReceivableAccrual_Ds.F_EXPCURRID),
		@RefLookup(refId = ReceivableAccrual_Ds.F_INVCURRID),
		@RefLookup(refId = ReceivableAccrual_Ds.F_ACCRUELCURRID)})
public class ReceivableAccrual_Ds extends AbstractDs_Ds<ReceivableAccrual> {

	public static final String ALIAS = "acc_ReceivableAccrual_Ds";

	public static final String F_FUELINGDATE = "fuelingDate";
	public static final String F_BUSINESSTYPE = "businessType";
	public static final String F_INVPROCESSSTATUS = "invProcessStatus";
	public static final String F_EXPECTEDQUANTITY = "expectedQuantity";
	public static final String F_EXPECTEDAMOUNTSET = "expectedAmountSet";
	public static final String F_EXPECTEDAMOUNTSYS = "expectedAmountSys";
	public static final String F_EXPECTEDVATSET = "expectedVATSet";
	public static final String F_EXPECTEDVATSYS = "expectedVATSys";
	public static final String F_INVOICEDQUANTITY = "invoicedQuantity";
	public static final String F_INVOICEDAMOUNTSET = "invoicedAmountSet";
	public static final String F_INVOICEDAMOUNTSYS = "invoicedAmountSys";
	public static final String F_INVOICEDVATSET = "invoicedVATSet";
	public static final String F_INVOICEDVATSYS = "invoicedVATSys";
	public static final String F_ACCRUALQUANTITY = "accrualQuantity";
	public static final String F_ACCRUALAMOUNTSET = "accrualAmountSet";
	public static final String F_ACCRUALAMOUNTSYS = "accrualAmountSys";
	public static final String F_ACCRUALVATSET = "accrualVATSet";
	public static final String F_ACCRUALVATSYS = "accrualVATSys";
	public static final String F_EXCHANGERATE = "exchangeRate";
	public static final String F_DENSITY = "density";
	public static final String F_CLOSINGDATE = "closingDate";
	public static final String F_ISOPEN = "isOpen";
	public static final String F_MONTH = "month";
	public static final String F_FUELEVENTID = "fuelEventId";
	public static final String F_FEFUELINGDATE = "feFuelingDate";
	public static final String F_FEFLIGHTNUMBER = "feFlightNumber";
	public static final String F_AIRCRAFTID = "aircraftId";
	public static final String F_AICRAFTREGISTRATION = "aicraftRegistration";
	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_CUSTID = "custId";
	public static final String F_CUSTCODE = "custCode";
	public static final String F_CUSTNAME = "custName";
	public static final String F_SUPPID = "suppId";
	public static final String F_SUPPCODE = "suppCode";
	public static final String F_EXPCURRID = "expCurrId";
	public static final String F_EXPCURRCODE = "expCurrCode";
	public static final String F_INVCURRID = "invCurrId";
	public static final String F_INVCURRCODE = "invCurrCode";
	public static final String F_ACCRUELCURRID = "accruelCurrId";
	public static final String F_ACCRUELCURRCODE = "accruelCurrCode";
	public static final String F_EVENTS = "events";
	public static final String F_NETAMOUNT = "netAmount";
	public static final String F_VATAMOUNT = "vatAmount";
	public static final String F_GROSSAMOUNT = "grossAmount";

	@DsField
	private Date fuelingDate;

	@DsField
	private FuelQuoteType businessType;

	@DsField
	private InvoiceProcessStatus invProcessStatus;

	@DsField
	private BigDecimal expectedQuantity;

	@DsField
	private BigDecimal expectedAmountSet;

	@DsField
	private BigDecimal expectedAmountSys;

	@DsField
	private BigDecimal expectedVATSet;

	@DsField
	private BigDecimal expectedVATSys;

	@DsField
	private BigDecimal invoicedQuantity;

	@DsField
	private BigDecimal invoicedAmountSet;

	@DsField
	private BigDecimal invoicedAmountSys;

	@DsField
	private BigDecimal invoicedVATSet;

	@DsField
	private BigDecimal invoicedVATSys;

	@DsField
	private BigDecimal accrualQuantity;

	@DsField
	private BigDecimal accrualAmountSet;

	@DsField
	private BigDecimal accrualAmountSys;

	@DsField
	private BigDecimal accrualVATSet;

	@DsField
	private BigDecimal accrualVATSys;

	@DsField
	private BigDecimal exchangeRate;

	@DsField
	private BigDecimal density;

	@DsField
	private Date closingDate;

	@DsField
	private Boolean isOpen;

	@DsField(fetch = false)
	private String month;

	@DsField(join = "left", path = "fuelEvent.id")
	private Integer fuelEventId;

	@DsField(join = "left", path = "fuelEvent.fuelingDate")
	private Date feFuelingDate;

	@DsField(join = "left", path = "fuelEvent.flightNumber")
	private String feFlightNumber;

	@DsField(join = "left", path = "fuelEvent.aircraft.id")
	private Integer aircraftId;

	@DsField(join = "left", path = "fuelEvent.aircraft.registration")
	private String aicraftRegistration;

	@DsField(join = "left", path = "location.id")
	private Integer locId;

	@DsField(join = "left", path = "location.code")
	private String locCode;

	@DsField(join = "left", path = "customer.id")
	private Integer custId;

	@DsField(join = "left", path = "customer.code")
	private String custCode;

	@DsField(join = "left", path = "customer.name")
	private String custName;

	@DsField(join = "left", path = "supplier.id")
	private Integer suppId;

	@DsField(join = "left", path = "supplier.code")
	private String suppCode;

	@DsField(join = "left", path = "expectedCurrency.id")
	private Integer expCurrId;

	@DsField(join = "left", path = "expectedCurrency.code")
	private String expCurrCode;

	@DsField(join = "left", path = "invoicedCurrency.id")
	private Integer invCurrId;

	@DsField(join = "left", path = "invoicedCurrency.code")
	private String invCurrCode;

	@DsField(join = "left", path = "accrualCurrency.id")
	private Integer accruelCurrId;

	@DsField(join = "left", path = "accrualCurrency.code")
	private String accruelCurrCode;

	@DsField(fetch = false)
	private Integer events;

	@DsField(fetch = false)
	private BigDecimal netAmount;

	@DsField(fetch = false)
	private BigDecimal vatAmount;

	@DsField(fetch = false)
	private BigDecimal grossAmount;

	/**
	 * Default constructor
	 */
	public ReceivableAccrual_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public ReceivableAccrual_Ds(ReceivableAccrual e) {
		super(e);
	}

	public Date getFuelingDate() {
		return this.fuelingDate;
	}

	public void setFuelingDate(Date fuelingDate) {
		this.fuelingDate = fuelingDate;
	}

	public FuelQuoteType getBusinessType() {
		return this.businessType;
	}

	public void setBusinessType(FuelQuoteType businessType) {
		this.businessType = businessType;
	}

	public InvoiceProcessStatus getInvProcessStatus() {
		return this.invProcessStatus;
	}

	public void setInvProcessStatus(InvoiceProcessStatus invProcessStatus) {
		this.invProcessStatus = invProcessStatus;
	}

	public BigDecimal getExpectedQuantity() {
		return this.expectedQuantity;
	}

	public void setExpectedQuantity(BigDecimal expectedQuantity) {
		this.expectedQuantity = expectedQuantity;
	}

	public BigDecimal getExpectedAmountSet() {
		return this.expectedAmountSet;
	}

	public void setExpectedAmountSet(BigDecimal expectedAmountSet) {
		this.expectedAmountSet = expectedAmountSet;
	}

	public BigDecimal getExpectedAmountSys() {
		return this.expectedAmountSys;
	}

	public void setExpectedAmountSys(BigDecimal expectedAmountSys) {
		this.expectedAmountSys = expectedAmountSys;
	}

	public BigDecimal getExpectedVATSet() {
		return this.expectedVATSet;
	}

	public void setExpectedVATSet(BigDecimal expectedVATSet) {
		this.expectedVATSet = expectedVATSet;
	}

	public BigDecimal getExpectedVATSys() {
		return this.expectedVATSys;
	}

	public void setExpectedVATSys(BigDecimal expectedVATSys) {
		this.expectedVATSys = expectedVATSys;
	}

	public BigDecimal getInvoicedQuantity() {
		return this.invoicedQuantity;
	}

	public void setInvoicedQuantity(BigDecimal invoicedQuantity) {
		this.invoicedQuantity = invoicedQuantity;
	}

	public BigDecimal getInvoicedAmountSet() {
		return this.invoicedAmountSet;
	}

	public void setInvoicedAmountSet(BigDecimal invoicedAmountSet) {
		this.invoicedAmountSet = invoicedAmountSet;
	}

	public BigDecimal getInvoicedAmountSys() {
		return this.invoicedAmountSys;
	}

	public void setInvoicedAmountSys(BigDecimal invoicedAmountSys) {
		this.invoicedAmountSys = invoicedAmountSys;
	}

	public BigDecimal getInvoicedVATSet() {
		return this.invoicedVATSet;
	}

	public void setInvoicedVATSet(BigDecimal invoicedVATSet) {
		this.invoicedVATSet = invoicedVATSet;
	}

	public BigDecimal getInvoicedVATSys() {
		return this.invoicedVATSys;
	}

	public void setInvoicedVATSys(BigDecimal invoicedVATSys) {
		this.invoicedVATSys = invoicedVATSys;
	}

	public BigDecimal getAccrualQuantity() {
		return this.accrualQuantity;
	}

	public void setAccrualQuantity(BigDecimal accrualQuantity) {
		this.accrualQuantity = accrualQuantity;
	}

	public BigDecimal getAccrualAmountSet() {
		return this.accrualAmountSet;
	}

	public void setAccrualAmountSet(BigDecimal accrualAmountSet) {
		this.accrualAmountSet = accrualAmountSet;
	}

	public BigDecimal getAccrualAmountSys() {
		return this.accrualAmountSys;
	}

	public void setAccrualAmountSys(BigDecimal accrualAmountSys) {
		this.accrualAmountSys = accrualAmountSys;
	}

	public BigDecimal getAccrualVATSet() {
		return this.accrualVATSet;
	}

	public void setAccrualVATSet(BigDecimal accrualVATSet) {
		this.accrualVATSet = accrualVATSet;
	}

	public BigDecimal getAccrualVATSys() {
		return this.accrualVATSys;
	}

	public void setAccrualVATSys(BigDecimal accrualVATSys) {
		this.accrualVATSys = accrualVATSys;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public BigDecimal getDensity() {
		return this.density;
	}

	public void setDensity(BigDecimal density) {
		this.density = density;
	}

	public Date getClosingDate() {
		return this.closingDate;
	}

	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	public Boolean getIsOpen() {
		return this.isOpen;
	}

	public void setIsOpen(Boolean isOpen) {
		this.isOpen = isOpen;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Integer getFuelEventId() {
		return this.fuelEventId;
	}

	public void setFuelEventId(Integer fuelEventId) {
		this.fuelEventId = fuelEventId;
	}

	public Date getFeFuelingDate() {
		return this.feFuelingDate;
	}

	public void setFeFuelingDate(Date feFuelingDate) {
		this.feFuelingDate = feFuelingDate;
	}

	public String getFeFlightNumber() {
		return this.feFlightNumber;
	}

	public void setFeFlightNumber(String feFlightNumber) {
		this.feFlightNumber = feFlightNumber;
	}

	public Integer getAircraftId() {
		return this.aircraftId;
	}

	public void setAircraftId(Integer aircraftId) {
		this.aircraftId = aircraftId;
	}

	public String getAicraftRegistration() {
		return this.aicraftRegistration;
	}

	public void setAicraftRegistration(String aicraftRegistration) {
		this.aicraftRegistration = aicraftRegistration;
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public Integer getCustId() {
		return this.custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public Integer getSuppId() {
		return this.suppId;
	}

	public void setSuppId(Integer suppId) {
		this.suppId = suppId;
	}

	public String getSuppCode() {
		return this.suppCode;
	}

	public void setSuppCode(String suppCode) {
		this.suppCode = suppCode;
	}

	public Integer getExpCurrId() {
		return this.expCurrId;
	}

	public void setExpCurrId(Integer expCurrId) {
		this.expCurrId = expCurrId;
	}

	public String getExpCurrCode() {
		return this.expCurrCode;
	}

	public void setExpCurrCode(String expCurrCode) {
		this.expCurrCode = expCurrCode;
	}

	public Integer getInvCurrId() {
		return this.invCurrId;
	}

	public void setInvCurrId(Integer invCurrId) {
		this.invCurrId = invCurrId;
	}

	public String getInvCurrCode() {
		return this.invCurrCode;
	}

	public void setInvCurrCode(String invCurrCode) {
		this.invCurrCode = invCurrCode;
	}

	public Integer getAccruelCurrId() {
		return this.accruelCurrId;
	}

	public void setAccruelCurrId(Integer accruelCurrId) {
		this.accruelCurrId = accruelCurrId;
	}

	public String getAccruelCurrCode() {
		return this.accruelCurrCode;
	}

	public void setAccruelCurrCode(String accruelCurrCode) {
		this.accruelCurrCode = accruelCurrCode;
	}

	public Integer getEvents() {
		return this.events;
	}

	public void setEvents(Integer events) {
		this.events = events;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}
}
