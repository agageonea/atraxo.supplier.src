/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoices.model;

import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.EventType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Contract.class, sort = {@SortField(field = Contract_Ds.F_CODE)})
@RefLookups({
		@RefLookup(refId = Contract_Ds.F_SUPPLIERID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_SUPPLIERCODE)}),
		@RefLookup(refId = Contract_Ds.F_LOCATIONID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Contract_Ds.F_LOCATIONCODE)})})
public class Contract_Ds extends AbstractSubsidiaryDs_Ds<Contract>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "acc_Contract_Ds";

	public static final String F_SUPPLIERID = "supplierId";
	public static final String F_SUPPLIERCODE = "supplierCode";
	public static final String F_LOCATIONID = "locationId";
	public static final String F_LOCATIONCODE = "locationCode";
	public static final String F_STATUS = "status";
	public static final String F_SCOPE = "scope";
	public static final String F_EVENTTYPE = "eventType";
	public static final String F_SUBTYPE = "subType";
	public static final String F_TYPE = "type";
	public static final String F_CODE = "code";
	public static final String F_ID = "id";
	public static final String F_FLIGHTTYPE = "flightType";

	@DsField(join = "left", path = "supplier.id")
	private Integer supplierId;

	@DsField(join = "left", path = "supplier.code")
	private String supplierCode;

	@DsField(join = "left", path = "location.id")
	private Integer locationId;

	@DsField(join = "left", path = "location.code")
	private String locationCode;

	@DsField
	private ContractStatus status;

	@DsField
	private ContractScope scope;

	@DsField
	private EventType eventType;

	@DsField
	private ContractSubType subType;

	@DsField
	private ContractType type;

	@DsField
	private String code;

	@DsField
	private Integer id;

	@DsField(path = "limitedTo")
	private FlightTypeIndicator flightType;

	/**
	 * Default constructor
	 */
	public Contract_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Contract_Ds(Contract e) {
		super(e);
	}

	public Integer getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierCode() {
		return this.supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public ContractStatus getStatus() {
		return this.status;
	}

	public void setStatus(ContractStatus status) {
		this.status = status;
	}

	public ContractScope getScope() {
		return this.scope;
	}

	public void setScope(ContractScope scope) {
		this.scope = scope;
	}

	public EventType getEventType() {
		return this.eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public ContractSubType getSubType() {
		return this.subType;
	}

	public void setSubType(ContractSubType subType) {
		this.subType = subType;
	}

	public ContractType getType() {
		return this.type;
	}

	public void setType(ContractType type) {
		this.type = type;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}
}
