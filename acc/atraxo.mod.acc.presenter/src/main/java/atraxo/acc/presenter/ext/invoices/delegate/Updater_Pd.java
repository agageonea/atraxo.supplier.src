package atraxo.acc.presenter.ext.invoices.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTimeComparator;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_Ds;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class Updater_Pd extends AbstractPresenterDelegate {

	public void approve(OutgoingInvoice_Ds ds) throws Exception {
		this.approveList(Arrays.asList(ds));
	}

	public void approveList(List<OutgoingInvoice_Ds> list) throws Exception {
		IOutgoingInvoiceService srv = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		List<OutgoingInvoice> invoices = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		for (OutgoingInvoice_Ds ds : list) {
			OutgoingInvoice oi = srv.findByBusiness(ds.getId());
			if (BillStatus._AWAITING_APPROVAL_.equals(oi.getStatus())) {
				if (new Date().before(ds.getInvoiceDate())) {
					sb.append("<li>").append(AccErrorCode.INVOICE_DATE_AFTER_TODAY.getErrMsg()).append("</li>");
				} else if (DateTimeComparator.getDateOnlyInstance().compare(ds.getInvoiceDate(), ds.getDeliveryDateTo()) < 0) {
					sb.append("<li>").append(AccErrorCode.INVOICE_DATE_GRATER_THAN_DATE_TO.getErrMsg()).append("</li>");
				} else {
					invoices.add(oi);
				}
			} else {
				sb.append("<li>").append(String.format(AccErrorCode.INVOICE_STATUS_AWAITING_APPROVAL.getErrMsg(), oi.getInvoiceNo())).append("</li>");
			}
		}
		sb.append("</ul>");
		srv.approve(invoices);
		String msg = sb.toString();
		if (msg.contains("<li>")) {
			throw new BusinessException(AccErrorCode.BLANK_ERROR_MESSAGE, String.format(AccErrorCode.BLANK_ERROR_MESSAGE.getErrMsg(), msg));
		}

	}

	public void pay(OutgoingInvoice_Ds ds) throws Exception {
		this.payList(Arrays.asList(ds));
	}

	public void payList(List<OutgoingInvoice_Ds> list) throws Exception {
		IOutgoingInvoiceService srv = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		for (OutgoingInvoice_Ds ds : list) {
			OutgoingInvoice outgoingInvoice = srv.findById(ds.getId());
			if (!BillStatus._AWAITING_PAYMENT_.equals(outgoingInvoice.getStatus())) {
				sb.append("<li>").append(String.format(AccErrorCode.INVOICE_STATUS_AWAITING_FOR_PAYMENT.getErrMsg(), outgoingInvoice.getInvoiceNo()))
						.append("</li>");
			}
			if (outgoingInvoice.getInvoiceReference() == null) {
				srv.markAsPaid(Arrays.asList(outgoingInvoice));
			} else {
				srv.markAsPaidCreditMemo(outgoingInvoice);
			}
		}
		sb.append("</ul>");
		String msg = sb.toString();
		if (msg.contains("<li>")) {
			throw new BusinessException(AccErrorCode.INVOICE_RPC_ACTION_ERROR, String.format(AccErrorCode.INVOICE_RPC_ACTION_ERROR.getErrMsg(), msg));
		}
	}
}
