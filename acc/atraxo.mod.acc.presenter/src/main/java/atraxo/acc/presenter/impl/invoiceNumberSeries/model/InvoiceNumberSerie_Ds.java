/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoiceNumberSeries.model;

import atraxo.fmbas.domain.impl.documentNumberSeries.DocumentNumberSeries;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DocumentNumberSeries.class)
public class InvoiceNumberSerie_Ds extends AbstractDs_Ds<DocumentNumberSeries> {

	public static final String ALIAS = "acc_InvoiceNumberSerie_Ds";

	public static final String F_PREFIX = "prefix";
	public static final String F_STARTINDEX = "startIndex";
	public static final String F_ENDINDEX = "endIndex";
	public static final String F_CURRENTINDEX = "currentIndex";
	public static final String F_ENABLED = "enabled";
	public static final String F_RESTART = "restart";
	public static final String F_TYPE = "type";

	@DsField
	private String prefix;

	@DsField
	private Integer startIndex;

	@DsField
	private Integer endIndex;

	@DsField
	private Integer currentIndex;

	@DsField
	private Boolean enabled;

	@DsField
	private Boolean restart;

	@DsField
	private DocumentNumberSeriesType type;

	/**
	 * Default constructor
	 */
	public InvoiceNumberSerie_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public InvoiceNumberSerie_Ds(DocumentNumberSeries e) {
		super(e);
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Integer getStartIndex() {
		return this.startIndex;
	}

	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}

	public Integer getEndIndex() {
		return this.endIndex;
	}

	public void setEndIndex(Integer endIndex) {
		this.endIndex = endIndex;
	}

	public Integer getCurrentIndex() {
		return this.currentIndex;
	}

	public void setCurrentIndex(Integer currentIndex) {
		this.currentIndex = currentIndex;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getRestart() {
		return this.restart;
	}

	public void setRestart(Boolean restart) {
		this.restart = restart;
	}

	public DocumentNumberSeriesType getType() {
		return this.type;
	}

	public void setType(DocumentNumberSeriesType type) {
		this.type = type;
	}
}
