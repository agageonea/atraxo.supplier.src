/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoiceLines.model;

/**
 * Generated code. Do not modify in this file.
 */
public class OutgoingRefInvoiceLine_DsParam {

	public static final String f_invoiceId = "invoiceId";

	private Integer invoiceId;

	public Integer getInvoiceId() {
		return this.invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
}
