/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.invoiceLines.service;

import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.presenter.impl.invoiceLines.model.OutgoingRefInvoiceLine_Ds;
import atraxo.acc.presenter.impl.invoiceLines.model.OutgoingRefInvoiceLine_DsParam;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class OutgoingRefInvoiceLine_DsService
		extends
			AbstractEntityDsService<OutgoingRefInvoiceLine_Ds, OutgoingRefInvoiceLine_Ds, OutgoingRefInvoiceLine_DsParam, OutgoingInvoiceLine>
		implements
			IDsService<OutgoingRefInvoiceLine_Ds, OutgoingRefInvoiceLine_Ds, OutgoingRefInvoiceLine_DsParam> {

	// Implement me ...

}
