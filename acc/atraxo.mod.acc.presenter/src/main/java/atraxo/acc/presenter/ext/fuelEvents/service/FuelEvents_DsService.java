/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.fuelEvents.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.acc.domain.impl.acc_type.ReceivedStatus;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.presenter.impl.fuelEvents.model.FuelEvents_Ds;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelEvents_DsService extends AbstractEntityDsService<FuelEvents_Ds, FuelEvents_Ds, Object, FuelEvent>
		implements IDsService<FuelEvents_Ds, FuelEvents_Ds, Object> {
	private static final Logger LOG = LoggerFactory.getLogger(FuelEvents_DsService.class);

	@Autowired
	private ISystemParameterService paramSrv;
	@Autowired
	private IContractService contractSrv;

	@Override
	protected void preFind(IQueryBuilder<FuelEvents_Ds, FuelEvents_Ds, Object> builder) throws Exception {
		FuelEvents_Ds filter = builder.getFilter();

		if (filter.getBillPriceSourceId() != null) {
			Contract contract = this.contractSrv.findById(filter.getBillPriceSourceId());

			// customer code on fuel event is not invoice receiber (bill to from the contract)
			// it is customer from the contract
			// from the UI customer code could not be set
			filter.setCustomerCode(contract.getCustomer().getCode());
			filter.setReceivedStatus(ReceivedStatus._RELEASED_);
		}
		super.preFind(builder);
	}

	@Override
	protected void postFind(IQueryBuilder<FuelEvents_Ds, FuelEvents_Ds, Object> builder, List<FuelEvents_Ds> result) throws Exception {
		super.postFind(builder, result);
		Map<String, String> resaleDetails = this.buildResaleReference(result);
		for (FuelEvents_Ds ds : result) {
			if (ds.getIsResale() != null && ds.getIsResale()) {
				String key = ds.getTicketNumber() + ds.getSupplierCode();
				String details = resaleDetails.containsKey(key) ? resaleDetails.get(ds.getTicketNumber() + "_S")
						: resaleDetails.get(ds.getTicketNumber() + ds.getCustomerCode());
				ds.setResaleDetail(details);
			}
		}

		Integer decimals = this.paramSrv.getDecimalsForCurrency();
		Map<Integer, String> contractMap = new HashMap<>();
		for (FuelEvents_Ds feDs : result) {
			if (feDs.getPayableCost() != null) {
				feDs.setPayableCostAndCurr(feDs.getPayableCost().setScale(decimals, BigDecimal.ROUND_HALF_UP) + " " + feDs.getPayCurrencyCode());
			}
			if (feDs.getBillabeCost() != null) {
				if (Contract.class.getSimpleName().equalsIgnoreCase(feDs.getBillPriceSourceType())) {
					try {
						if (contractMap.containsKey(feDs.getBillPriceSourceId())) {
							feDs.setSettlementUnitCode(contractMap.get(feDs.getBillPriceSourceId()));
						} else {
							Contract c = this.contractSrv.findById(feDs.getBillPriceSourceId());
							String settelementUnitCode = c.getSettlementUnit().getCode();
							feDs.setSettlementUnitCode(settelementUnitCode);
							contractMap.put(feDs.getBillPriceSourceId(), settelementUnitCode);
						}
					} catch (Exception e) {
						LOG.error("Contract not found.", e);
						feDs.setSettlementUnitCode("");
					}
				}
				feDs.setBillableCostAndCurr(feDs.getBillabeCost().setScale(decimals, BigDecimal.ROUND_HALF_UP) + " " + feDs.getBillCurrencyCode());
			}
		}
		this.buildResaleDetails(result);
	}

	private void buildResaleDetails(List<FuelEvents_Ds> results) throws Exception {
		Map<String, FuelEvents_Ds> dsMap = new HashMap<>();
		for (FuelEvents_Ds ds : results) {
			if (!ds.getIsResale()) {
				continue;
			}
			if (!dsMap.containsKey(ds.getTicketNumber())) {
				dsMap.put(ds.getTicketNumber(), ds);
			} else {
				FuelEvents_Ds tempDs = dsMap.get(ds.getTicketNumber());
				ds.setResaleFuelEventId(tempDs.getId());
				ds.setResaleFuelEventResaleStatus(tempDs.getReceivedStatus().getName());
				ds.setResaleFuelEventInvoiceStatus(tempDs.getInvoiceStatus().toString());
				tempDs.setResaleFuelEventId(ds.getId());
				tempDs.setResaleFuelEventResaleStatus(ds.getReceivedStatus().getName());
				tempDs.setResaleFuelEventInvoiceStatus(ds.getInvoiceStatus().toString());
			}
		}
	}

	private Map<String, String> buildResaleReference(List<FuelEvents_Ds> result) {
		Map<String, String> map = new HashMap<>();
		for (FuelEvents_Ds ds : result) {
			if (ds.getIsResale() != null && ds.getIsResale()) {
				if (ds.getIsSubsidiary() != null && ds.getIsSubsidiary()) {
					map.put(ds.getTicketNumber() + "_S", ds.getSupplierCode() + " - " + ds.getCustomerCode());
				} else {
					map.put(ds.getTicketNumber() + ds.getSupplierCode(), ds.getSupplierCode() + " - " + ds.getCustomerCode());
				}
			}
		}
		return map;
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void deleteByIds(List<Object> ids) throws Exception {
		List<FuelEvent> events = this.getEntityService().findByIds(ids);
		this.getEntityService().delete(events);
	}

}
