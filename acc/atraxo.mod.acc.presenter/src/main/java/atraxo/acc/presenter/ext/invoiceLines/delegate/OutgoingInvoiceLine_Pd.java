package atraxo.acc.presenter.ext.invoiceLines.delegate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.util.StringUtils;

import atraxo.acc.business.api.invoiceLines.IOutgoingInvoiceLineService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.creditMemo.CreditMemo_Service;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.presenter.impl.invoiceLines.model.OutgoingInvoiceLine_Ds;
import atraxo.acc.presenter.impl.invoiceLines.model.OutgoingInvoiceLine_DsParam;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class OutgoingInvoiceLine_Pd extends AbstractPresenterDelegate {

	public void newCreditMemo(List<OutgoingInvoiceLine_Ds> list, OutgoingInvoiceLine_DsParam params) throws Exception {
		InfoForCreditMemo ifcm = this.getInvoiceData(list, params);
		CreditMemo_Service creditMemoService = this.getApplicationContext().getBean(CreditMemo_Service.class);
		OutgoingInvoice creditMemo = creditMemoService.createAndInsertCreditMemo(ifcm.outgoingInvoice, ifcm.oilList,
				!StringUtils.isEmpty(params.getCreditMemoReason()) ? RevocationReason.getByName(params.getCreditMemoReason())
						: RevocationReason._EMPTY_);
		if (creditMemo.getInvoiceNo().contains("CM-" + new SimpleDateFormat("yyyyMMddHH").format(new Date()))) {
			params.setNumberSeriesGeneratorMessage(
					"The system automatically created a credit memo number as there is no valid document number series defined for credit memos. Please configure a document number series for credit memos in the Accounting settings.");
		}
		params.setGeneratedCreditMemoId(creditMemo.getId());
	}

	public void addNewCreditMemoLine(List<OutgoingInvoiceLine_Ds> list, OutgoingInvoiceLine_DsParam params) throws Exception {
		InfoForCreditMemo ifcm = this.getInvoiceData(list, params);
		CreditMemo_Service creditMemoService = this.getApplicationContext().getBean(CreditMemo_Service.class);
		creditMemoService.addNewInvoiceLine(ifcm.outgoingInvoice, ifcm.oilList);
	}

	private InfoForCreditMemo getInvoiceData(List<OutgoingInvoiceLine_Ds> list, OutgoingInvoiceLine_DsParam params) throws Exception {
		IOutgoingInvoiceService oiService = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		IOutgoingInvoiceLineService oilService = (IOutgoingInvoiceLineService) this.findEntityService(OutgoingInvoiceLine.class);
		OutgoingInvoice oi = oiService.findById(params.getInvoiceId());
		List<OutgoingInvoiceLine> oilList = new ArrayList<>();
		for (OutgoingInvoiceLine_Ds ds : list) {
			OutgoingInvoiceLine oil = oilService.findById(ds.getId());
			oilList.add(oil);
		}
		return new InfoForCreditMemo(oi, oilList);
	}

	private class InfoForCreditMemo {
		OutgoingInvoice outgoingInvoice;
		List<OutgoingInvoiceLine> oilList;

		public InfoForCreditMemo(OutgoingInvoice outgoingInvoice, List<OutgoingInvoiceLine> oilList) {
			super();
			this.outgoingInvoice = outgoingInvoice;
			this.oilList = oilList;
		}

	}
}
