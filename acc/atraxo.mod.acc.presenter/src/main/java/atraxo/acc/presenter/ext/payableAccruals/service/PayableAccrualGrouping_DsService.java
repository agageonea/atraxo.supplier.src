/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.payableAccruals.service;

import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.acc.business.api.payableAccruals.IPayableAccrualService;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.acc.presenter.ext.invoices.service.Invoice_DsService;
import atraxo.acc.presenter.impl.payableAccruals.model.PayableAccrualGrouping_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class PayableAccrualGrouping_DsService
		extends AbstractEntityDsService<PayableAccrualGrouping_Ds, PayableAccrualGrouping_Ds, Object, PayableAccrual>
		implements IDsService<PayableAccrualGrouping_Ds, PayableAccrualGrouping_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(Invoice_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<PayableAccrualGrouping_Ds, PayableAccrualGrouping_Ds, Object> builder,
			List<PayableAccrualGrouping_Ds> result) throws Exception {
		super.postFind(builder, result);
		IPayableAccrualService payableAccrualService = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		List<PayableAccrual> payableAccruals = payableAccrualService.findAll();
		HashMap<Date, PayableAccrualDsStore> hashMap = new HashMap<>();
		for (PayableAccrual pa : payableAccruals) {
			Date usedDate = this.formatDate(pa.getFuelingDate());
			if (hashMap.containsKey(usedDate)) {
				PayableAccrualDsStore store = hashMap.get(usedDate);
				store.setEvents(store.getEvents() + 1);
				store.setNetAmount(store.getNetAmount().add(pa.getAccrualAmountSys(), MathContext.DECIMAL64));
				store.setVatAmount(store.getVatAmount().add(pa.getAccrualVATSys(), MathContext.DECIMAL64));
				store.setGrossAmount(store.getNetAmount().add(store.getVatAmount(), MathContext.DECIMAL64));
			} else {
				PayableAccrualDsStore store = new PayableAccrualDsStore();
				store.setDate(usedDate);
				store.setEvents(1);
				store.setNetAmount(pa.getAccrualAmountSys());
				store.setVatAmount(pa.getAccrualVATSys());
				store.setGrossAmount(store.getNetAmount().add(store.getVatAmount(), MathContext.DECIMAL64));
				hashMap.put(usedDate, store);
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
		List<PayableAccrualGrouping_Ds> retList = new ArrayList<>();
		for (PayableAccrualDsStore store : hashMap.values()) {
			PayableAccrualGrouping_Ds ds = new PayableAccrualGrouping_Ds();
			ds.setEvents(store.getEvents());
			ds.setMonth(sdf.format(store.getDate()));
			ds.setGrossAmount(store.getGrossAmount());
			ds.setVatAmount(store.getVatAmount());
			ds.setNetAmount(store.getNetAmount());
			retList.add(ds);
		}
		result.addAll(retList);
		result.retainAll(retList);
		Collections.sort(result, new Comparator<PayableAccrualGrouping_Ds>() {
			@Override
			public int compare(PayableAccrualGrouping_Ds arg0, PayableAccrualGrouping_Ds arg1) {
				SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
				try {
					Date date1 = sdf.parse(arg0.getMonth());
					Date date2 = sdf.parse(arg1.getMonth());
					return date1.compareTo(date2);
				} catch (ParseException e) {
					LOG.error("Error", e);
				}
				return 0;
			}
		});

	}

	@Override
	public Long count(IQueryBuilder<PayableAccrualGrouping_Ds, PayableAccrualGrouping_Ds, Object> builder) throws Exception {
		IPayableAccrualService payableAccrualService = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		List<PayableAccrual> payableAccruals = payableAccrualService.findAll();
		HashMap<Date, Integer> hashMap = new HashMap<>();
		for (PayableAccrual pa : payableAccruals) {
			Date usedDate = this.formatDate(pa.getFuelingDate());
			if (hashMap.containsKey(usedDate)) {
				Integer count = hashMap.get(usedDate);
				count = count + 1;
			} else {
				hashMap.put(usedDate, 1);
			}
		}
		return new Long(hashMap.size());
	}

	private Date formatDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

}
