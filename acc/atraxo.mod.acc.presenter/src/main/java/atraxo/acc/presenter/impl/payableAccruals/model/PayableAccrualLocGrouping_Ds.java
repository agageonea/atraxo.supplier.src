/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.payableAccruals.model;

import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = PayableAccrual.class)
@RefLookups({@RefLookup(refId = PayableAccrualLocGrouping_Ds.F_EXPCURRID),
		@RefLookup(refId = PayableAccrualLocGrouping_Ds.F_INVCURRID),
		@RefLookup(refId = PayableAccrualLocGrouping_Ds.F_ACCRUELCURRID),
		@RefLookup(refId = PayableAccrualLocGrouping_Ds.F_UNITID)})
public class PayableAccrualLocGrouping_Ds extends AbstractDs_Ds<PayableAccrual> {

	public static final String ALIAS = "acc_PayableAccrualLocGrouping_Ds";

	public static final String F_FUELINGDATE = "fuelingDate";
	public static final String F_EXPCURRID = "expCurrId";
	public static final String F_EXPCURRCODE = "expCurrCode";
	public static final String F_INVCURRID = "invCurrId";
	public static final String F_INVCURRCODE = "invCurrCode";
	public static final String F_ACCRUELCURRID = "accruelCurrId";
	public static final String F_ACCRUELCURRCODE = "accruelCurrCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_MONTH = "month";
	public static final String F_LOCATION = "location";
	public static final String F_EVENTS = "events";
	public static final String F_NETAMOUNT = "netAmount";
	public static final String F_VATAMOUNT = "vatAmount";
	public static final String F_GROSSAMOUNT = "grossAmount";

	@DsField
	private Date fuelingDate;

	@DsField(join = "left", path = "expectedCurrency.id")
	private Integer expCurrId;

	@DsField(join = "left", path = "expectedCurrency.code")
	private String expCurrCode;

	@DsField(join = "left", path = "invoicedCurrency.id")
	private Integer invCurrId;

	@DsField(join = "left", path = "invoicedCurrency.code")
	private String invCurrCode;

	@DsField(join = "left", path = "accrualCurrency.id")
	private Integer accruelCurrId;

	@DsField(join = "left", path = "accrualCurrency.code")
	private String accruelCurrCode;

	@DsField(join = "left", path = "fuelEvent.unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "fuelEvent.unit.code")
	private String unitCode;

	@DsField(fetch = false)
	private String month;

	@DsField(fetch = false)
	private String location;

	@DsField(fetch = false)
	private Integer events;

	@DsField(fetch = false)
	private BigDecimal netAmount;

	@DsField(fetch = false)
	private BigDecimal vatAmount;

	@DsField(fetch = false)
	private BigDecimal grossAmount;

	/**
	 * Default constructor
	 */
	public PayableAccrualLocGrouping_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PayableAccrualLocGrouping_Ds(PayableAccrual e) {
		super(e);
	}

	public Date getFuelingDate() {
		return this.fuelingDate;
	}

	public void setFuelingDate(Date fuelingDate) {
		this.fuelingDate = fuelingDate;
	}

	public Integer getExpCurrId() {
		return this.expCurrId;
	}

	public void setExpCurrId(Integer expCurrId) {
		this.expCurrId = expCurrId;
	}

	public String getExpCurrCode() {
		return this.expCurrCode;
	}

	public void setExpCurrCode(String expCurrCode) {
		this.expCurrCode = expCurrCode;
	}

	public Integer getInvCurrId() {
		return this.invCurrId;
	}

	public void setInvCurrId(Integer invCurrId) {
		this.invCurrId = invCurrId;
	}

	public String getInvCurrCode() {
		return this.invCurrCode;
	}

	public void setInvCurrCode(String invCurrCode) {
		this.invCurrCode = invCurrCode;
	}

	public Integer getAccruelCurrId() {
		return this.accruelCurrId;
	}

	public void setAccruelCurrId(Integer accruelCurrId) {
		this.accruelCurrId = accruelCurrId;
	}

	public String getAccruelCurrCode() {
		return this.accruelCurrCode;
	}

	public void setAccruelCurrCode(String accruelCurrCode) {
		this.accruelCurrCode = accruelCurrCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Integer getEvents() {
		return this.events;
	}

	public void setEvents(Integer events) {
		this.events = events;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}
}
