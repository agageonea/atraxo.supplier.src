/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.receivableAccruals.service;

import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;
import atraxo.acc.business.api.receivableAccruals.IReceivableAccrualService;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.acc.presenter.ext.invoices.service.Invoice_DsService;
import atraxo.acc.presenter.impl.receivableAccruals.model.ReceivableAccrualGrouping_Ds;

public class ReceivableAccrualGrouping_DsService extends
		AbstractEntityDsService<ReceivableAccrualGrouping_Ds, ReceivableAccrualGrouping_Ds, Object, ReceivableAccrual> implements
		IDsService<ReceivableAccrualGrouping_Ds, ReceivableAccrualGrouping_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(Invoice_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<ReceivableAccrualGrouping_Ds, ReceivableAccrualGrouping_Ds, Object> builder,
			List<ReceivableAccrualGrouping_Ds> result) throws Exception {
		super.postFind(builder, result);
		IReceivableAccrualService receivableAccrualService = (IReceivableAccrualService) this.findEntityService(ReceivableAccrual.class);
		List<ReceivableAccrual> receivableAccruals = receivableAccrualService.findAll();
		HashMap<Date, ReceivableAccrualDsStore> hashMap = new HashMap<>();
		for (ReceivableAccrual pa : receivableAccruals) {
			Date usedDate = this.formatDate(pa.getFuelingDate());
			if (hashMap.containsKey(usedDate)) {
				ReceivableAccrualDsStore store = hashMap.get(usedDate);
				store.setEvents(store.getEvents() + 1);
				store.setNetAmount(store.getNetAmount().add(pa.getAccrualAmountSys(), MathContext.DECIMAL64));
				store.setVatAmount(store.getVatAmount().add(pa.getAccrualVATSys(), MathContext.DECIMAL64));
				store.setGrossAmount(store.getNetAmount().add(store.getVatAmount(), MathContext.DECIMAL64));
			} else {
				ReceivableAccrualDsStore store = new ReceivableAccrualDsStore();
				store.setDate(usedDate);
				store.setEvents(1);
				store.setNetAmount(pa.getAccrualAmountSys());
				store.setVatAmount(pa.getAccrualVATSys());
				store.setGrossAmount(store.getNetAmount().add(store.getVatAmount(), MathContext.DECIMAL64));
				hashMap.put(usedDate, store);
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
		List<ReceivableAccrualGrouping_Ds> retList = new ArrayList<>();
		for (ReceivableAccrualDsStore store : hashMap.values()) {
			ReceivableAccrualGrouping_Ds ds = new ReceivableAccrualGrouping_Ds();
			ds.setEvents(store.getEvents());
			ds.setMonth(sdf.format(store.getDate()));
			ds.setGrossAmount(store.getGrossAmount());
			ds.setVatAmount(store.getVatAmount());
			ds.setNetAmount(store.getNetAmount());
			retList.add(ds);
		}
		result.addAll(retList);
		result.retainAll(retList);
		Collections.sort(result, new Comparator<ReceivableAccrualGrouping_Ds>() {
			@Override
			public int compare(ReceivableAccrualGrouping_Ds arg0, ReceivableAccrualGrouping_Ds arg1) {
				SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
				try {
					Date date1 = sdf.parse(arg0.getMonth());
					Date date2 = sdf.parse(arg1.getMonth());
					return date2.compareTo(date1);
				} catch (ParseException e) {
					LOG.error("Error", e);
				}
				return 0;
			}
		});
	}

	@Override
	public Long count(IQueryBuilder<ReceivableAccrualGrouping_Ds, ReceivableAccrualGrouping_Ds, Object> builder) throws Exception {
		IReceivableAccrualService receivableAccrualService = (IReceivableAccrualService) this.findEntityService(ReceivableAccrual.class);
		List<ReceivableAccrual> receivableAccruals = receivableAccrualService.findAll();
		HashMap<Date, Integer> hashMap = new HashMap<>();
		for (ReceivableAccrual pa : receivableAccruals) {
			Date usedDate = this.formatDate(pa.getFuelingDate());
			if (hashMap.containsKey(usedDate)) {
				hashMap.put(usedDate, hashMap.get(usedDate) + 1);
			} else {
				hashMap.put(usedDate, 1);
			}
		}
		return new Long(hashMap.size());
	}

	private Date formatDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

}
