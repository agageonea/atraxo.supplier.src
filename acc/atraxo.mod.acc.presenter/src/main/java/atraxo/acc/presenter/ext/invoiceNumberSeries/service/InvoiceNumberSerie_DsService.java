/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.invoiceNumberSeries.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.presenter.impl.invoiceNumberSeries.model.InvoiceNumberSerie_Ds;
import atraxo.fmbas.business.api.documentNumberSeries.IDocumentNumberSeriesService;
import atraxo.fmbas.domain.impl.documentNumberSeries.DocumentNumberSeries;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class InvoiceNumberSerie_DsService extends AbstractEntityDsService<InvoiceNumberSerie_Ds, InvoiceNumberSerie_Ds, Object, DocumentNumberSeries>
		implements IDsService<InvoiceNumberSerie_Ds, InvoiceNumberSerie_Ds, Object> {

	@Override
	protected void preUpdate(InvoiceNumberSerie_Ds ds, Object params) throws Exception {
		super.preUpdate(ds, params);
		this.validateEndIndex(ds);
		this.enable(ds);
	}

	@Override
	protected void preInsert(InvoiceNumberSerie_Ds ds, Object params) throws Exception {
		super.preInsert(ds, params);
		if (ds.getCurrentIndex() == null) {
			ds.setCurrentIndex(ds.getStartIndex());
		}
		this.validateStartIndex(ds);
		this.validateEndIndex(ds);
		this.enable(ds);
	}

	private void validateEndIndex(InvoiceNumberSerie_Ds ds) throws BusinessException {
		if (ds.getStartIndex().compareTo(ds.getEndIndex()) >= 0) {
			throw new BusinessException(AccErrorCode.INVALID_INDEX, AccErrorCode.INVALID_INDEX.getErrMsg());
		}
	}

	private void validateStartIndex(InvoiceNumberSerie_Ds ds) throws BusinessException {
		if (ds.getCurrentIndex().compareTo(ds.getStartIndex()) > 0) {
			throw new BusinessException(AccErrorCode.INVALID_START_INDEX, AccErrorCode.INVALID_START_INDEX.getErrMsg());
		}
	}

	private void enable(InvoiceNumberSerie_Ds ds) throws Exception, BusinessException {
		if (ds.getEnabled()) {
			IDocumentNumberSeriesService srv = (IDocumentNumberSeriesService) this.findEntityService(DocumentNumberSeries.class);
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("enabled", Boolean.TRUE);
			List<DocumentNumberSeries> list = srv.findEntitiesByAttributes(paramMap);
			if (!list.isEmpty()) {
				Iterator<DocumentNumberSeries> iter = list.iterator();
				while (iter.hasNext()) {
					DocumentNumberSeries serie = iter.next();
					if (!serie.getId().equals(ds.getId()) && serie.getType().equals(ds.getType())) {
						serie.setEnabled(false);
					} else {
						iter.remove();
					}
				}
				srv.update(list);
			}
		}
	}
}
