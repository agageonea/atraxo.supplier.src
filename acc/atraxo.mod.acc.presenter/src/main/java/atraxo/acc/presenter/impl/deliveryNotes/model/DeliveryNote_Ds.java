/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.deliveryNotes.model;

import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperation;
import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DeliveryNote.class, sort = {@SortField(field = DeliveryNote_Ds.F_SOURCE)})
@RefLookups({
		@RefLookup(refId = DeliveryNote_Ds.F_AIRCRAFTID, namedQuery = Aircraft.NQ_FIND_BY_REG_PRIMITIVE, params = {@Param(name = "customerId", field = DeliveryNote_Ds.F_AIRCRAFTREGISTRATION)}),
		@RefLookup(refId = DeliveryNote_Ds.F_BILLCURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_BILLCURRENCYCODE)}),
		@RefLookup(refId = DeliveryNote_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = DeliveryNote_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_UNITCODE)}),
		@RefLookup(refId = DeliveryNote_Ds.F_NETUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_NETUNITCODE)}),
		@RefLookup(refId = DeliveryNote_Ds.F_DEPARTUREID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_DEPARTURECODE)}),
		@RefLookup(refId = DeliveryNote_Ds.F_SUPPLIERID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_SUPPLIERCODE)}),
		@RefLookup(refId = DeliveryNote_Ds.F_IPLAGENTID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_IPLAGENTCODE)}),
		@RefLookup(refId = DeliveryNote_Ds.F_PAYCURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_PAYCURRENCYCODE)}),
		@RefLookup(refId = DeliveryNote_Ds.F_FUELEVENTID),
		@RefLookup(refId = DeliveryNote_Ds.F_CONTRACTHOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_CONTRACTHOLDERCODE)}),
		@RefLookup(refId = DeliveryNote_Ds.F_SHIPTOID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = DeliveryNote_Ds.F_SHIPTOCODE)})})
public class DeliveryNote_Ds extends AbstractSubsidiaryDs_Ds<DeliveryNote> {

	public static final String ALIAS = "acc_DeliveryNote_Ds";

	public static final String F_AIRCRAFTID = "aircraftId";
	public static final String F_AIRCRAFTREGISTRATION = "aircraftRegistration";
	public static final String F_BILLCURRENCYID = "billCurrencyId";
	public static final String F_BILLCURRENCYCODE = "billCurrencyCode";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CONTRACTHOLDERID = "contractHolderID";
	public static final String F_CONTRACTHOLDERCODE = "contractHolderCode";
	public static final String F_DEPARTUREID = "departureId";
	public static final String F_DEPARTURECODE = "departureCode";
	public static final String F_SHIPTOID = "shipToID";
	public static final String F_SHIPTOCODE = "shipToCode";
	public static final String F_SUPPLIERID = "supplierId";
	public static final String F_SUPPLIERCODE = "supplierCode";
	public static final String F_IPLAGENTID = "iplAgentId";
	public static final String F_IPLAGENTCODE = "iplAgentCode";
	public static final String F_PAYCURRENCYID = "payCurrencyId";
	public static final String F_PAYCURRENCYCODE = "payCurrencyCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_NETUNITID = "netUnitId";
	public static final String F_NETUNITCODE = "netUnitCode";
	public static final String F_FUELEVENTID = "fuelEventId";
	public static final String F_FUELEVENTFLIGHTNUMBER = "fuelEventFlightNumber";
	public static final String F_FUELEVENTFUELINGDATE = "fuelEventFuelingDate";
	public static final String F_BILLSTATUS = "billStatus";
	public static final String F_INVOICESTATUS = "invoiceStatus";
	public static final String F_FUELEVENTDEPARTURECODE = "fuelEventDepartureCode";
	public static final String F_FUELEVENTAIRCRAFTREGISTRATION = "fueleventAircraftRegistration";
	public static final String F_FUELINGDATE = "fuelingDate";
	public static final String F_FLIGHTNUMBER = "flightNumber";
	public static final String F_SUFFIX = "suffix";
	public static final String F_TICKETNUMBER = "ticketNumber";
	public static final String F_QUANTITY = "quantity";
	public static final String F_SOURCE = "source";
	public static final String F_PAYABLECOST = "payableCost";
	public static final String F_BILLABECOST = "billabeCost";
	public static final String F_USED = "used";
	public static final String F_OBJECTID = "objectId";
	public static final String F_OBJECTTYPE = "objectType";
	public static final String F_NETUPLIFTQUANTITY = "netUpliftQuantity";
	public static final String F_FUELINGOPERATION = "fuelingOperation";
	public static final String F_TRANSPORT = "transport";
	public static final String F_OPERATIONTYPE = "operationType";
	public static final String F_ERPNO = "erpNo";
	public static final String F_PRODUCT = "product";
	public static final String F_FLIGHTID = "flightID";

	@DsField(join = "left", path = "aircraft.id")
	private Integer aircraftId;

	@DsField(join = "left", path = "aircraft.registration")
	private String aircraftRegistration;

	@DsField(join = "left", path = "billableCostCurrency.id")
	private Integer billCurrencyId;

	@DsField(join = "left", path = "billableCostCurrency.code")
	private String billCurrencyCode;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "contractHolder.id")
	private Integer contractHolderID;

	@DsField(join = "left", path = "contractHolder.code")
	private String contractHolderCode;

	@DsField(join = "left", path = "departure.id")
	private Integer departureId;

	@DsField(join = "left", path = "departure.code")
	private String departureCode;

	@DsField(join = "left", path = "shipTo.id")
	private Integer shipToID;

	@DsField(join = "left", path = "shipTo.code")
	private String shipToCode;

	@DsField(join = "left", path = "fuelSupplier.id")
	private Integer supplierId;

	@DsField(join = "left", path = "fuelSupplier.code")
	private String supplierCode;

	@DsField(join = "left", path = "iplAgent.id")
	private Integer iplAgentId;

	@DsField(join = "left", path = "iplAgent.code")
	private String iplAgentCode;

	@DsField(join = "left", path = "payableCostCurrency.id")
	private Integer payCurrencyId;

	@DsField(join = "left", path = "payableCostCurrency.code")
	private String payCurrencyCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "netUpliftUnit.id")
	private Integer netUnitId;

	@DsField(join = "left", path = "netUpliftUnit.code")
	private String netUnitCode;

	@DsField(join = "left", path = "fuelEvent.id")
	private Integer fuelEventId;

	@DsField(join = "left", path = "fuelEvent.flightNumber")
	private String fuelEventFlightNumber;

	@DsField(join = "left", path = "fuelEvent.fuelingDate")
	private Date fuelEventFuelingDate;

	@DsField(join = "left", path = "fuelEvent.billStatus")
	private BillStatus billStatus;

	@DsField(join = "left", path = "fuelEvent.invoiceStatus")
	private OutgoingInvoiceStatus invoiceStatus;

	@DsField(join = "left", path = "fuelEvent.departure.code")
	private String fuelEventDepartureCode;

	@DsField(join = "left", path = "fuelEvent.aircraft.registration")
	private String fueleventAircraftRegistration;

	@DsField
	private Date fuelingDate;

	@DsField
	private String flightNumber;

	@DsField
	private Suffix suffix;

	@DsField
	private String ticketNumber;

	@DsField
	private BigDecimal quantity;

	@DsField
	private QuantitySource source;

	@DsField
	private BigDecimal payableCost;

	@DsField(path = "billableCost")
	private BigDecimal billabeCost;

	@DsField
	private Boolean used;

	@DsField
	private Integer objectId;

	@DsField
	private String objectType;

	@DsField
	private BigDecimal netUpliftQuantity;

	@DsField
	private FuelTicketFuelingOperation fuelingOperation;

	@DsField
	private FuelingType transport;

	@DsField
	private OperationType operationType;

	@DsField
	private String erpNo;

	@DsField
	private Product product;

	@DsField
	private String flightID;

	/**
	 * Default constructor
	 */
	public DeliveryNote_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DeliveryNote_Ds(DeliveryNote e) {
		super(e);
	}

	public Integer getAircraftId() {
		return this.aircraftId;
	}

	public void setAircraftId(Integer aircraftId) {
		this.aircraftId = aircraftId;
	}

	public String getAircraftRegistration() {
		return this.aircraftRegistration;
	}

	public void setAircraftRegistration(String aircraftRegistration) {
		this.aircraftRegistration = aircraftRegistration;
	}

	public Integer getBillCurrencyId() {
		return this.billCurrencyId;
	}

	public void setBillCurrencyId(Integer billCurrencyId) {
		this.billCurrencyId = billCurrencyId;
	}

	public String getBillCurrencyCode() {
		return this.billCurrencyCode;
	}

	public void setBillCurrencyCode(String billCurrencyCode) {
		this.billCurrencyCode = billCurrencyCode;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getContractHolderID() {
		return this.contractHolderID;
	}

	public void setContractHolderID(Integer contractHolderID) {
		this.contractHolderID = contractHolderID;
	}

	public String getContractHolderCode() {
		return this.contractHolderCode;
	}

	public void setContractHolderCode(String contractHolderCode) {
		this.contractHolderCode = contractHolderCode;
	}

	public Integer getDepartureId() {
		return this.departureId;
	}

	public void setDepartureId(Integer departureId) {
		this.departureId = departureId;
	}

	public String getDepartureCode() {
		return this.departureCode;
	}

	public void setDepartureCode(String departureCode) {
		this.departureCode = departureCode;
	}

	public Integer getShipToID() {
		return this.shipToID;
	}

	public void setShipToID(Integer shipToID) {
		this.shipToID = shipToID;
	}

	public String getShipToCode() {
		return this.shipToCode;
	}

	public void setShipToCode(String shipToCode) {
		this.shipToCode = shipToCode;
	}

	public Integer getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierCode() {
		return this.supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public Integer getIplAgentId() {
		return this.iplAgentId;
	}

	public void setIplAgentId(Integer iplAgentId) {
		this.iplAgentId = iplAgentId;
	}

	public String getIplAgentCode() {
		return this.iplAgentCode;
	}

	public void setIplAgentCode(String iplAgentCode) {
		this.iplAgentCode = iplAgentCode;
	}

	public Integer getPayCurrencyId() {
		return this.payCurrencyId;
	}

	public void setPayCurrencyId(Integer payCurrencyId) {
		this.payCurrencyId = payCurrencyId;
	}

	public String getPayCurrencyCode() {
		return this.payCurrencyCode;
	}

	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getNetUnitId() {
		return this.netUnitId;
	}

	public void setNetUnitId(Integer netUnitId) {
		this.netUnitId = netUnitId;
	}

	public String getNetUnitCode() {
		return this.netUnitCode;
	}

	public void setNetUnitCode(String netUnitCode) {
		this.netUnitCode = netUnitCode;
	}

	public Integer getFuelEventId() {
		return this.fuelEventId;
	}

	public void setFuelEventId(Integer fuelEventId) {
		this.fuelEventId = fuelEventId;
	}

	public String getFuelEventFlightNumber() {
		return this.fuelEventFlightNumber;
	}

	public void setFuelEventFlightNumber(String fuelEventFlightNumber) {
		this.fuelEventFlightNumber = fuelEventFlightNumber;
	}

	public Date getFuelEventFuelingDate() {
		return this.fuelEventFuelingDate;
	}

	public void setFuelEventFuelingDate(Date fuelEventFuelingDate) {
		this.fuelEventFuelingDate = fuelEventFuelingDate;
	}

	public BillStatus getBillStatus() {
		return this.billStatus;
	}

	public void setBillStatus(BillStatus billStatus) {
		this.billStatus = billStatus;
	}

	public OutgoingInvoiceStatus getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(OutgoingInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getFuelEventDepartureCode() {
		return this.fuelEventDepartureCode;
	}

	public void setFuelEventDepartureCode(String fuelEventDepartureCode) {
		this.fuelEventDepartureCode = fuelEventDepartureCode;
	}

	public String getFueleventAircraftRegistration() {
		return this.fueleventAircraftRegistration;
	}

	public void setFueleventAircraftRegistration(
			String fueleventAircraftRegistration) {
		this.fueleventAircraftRegistration = fueleventAircraftRegistration;
	}

	public Date getFuelingDate() {
		return this.fuelingDate;
	}

	public void setFuelingDate(Date fuelingDate) {
		this.fuelingDate = fuelingDate;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public String getTicketNumber() {
		return this.ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public QuantitySource getSource() {
		return this.source;
	}

	public void setSource(QuantitySource source) {
		this.source = source;
	}

	public BigDecimal getPayableCost() {
		return this.payableCost;
	}

	public void setPayableCost(BigDecimal payableCost) {
		this.payableCost = payableCost;
	}

	public BigDecimal getBillabeCost() {
		return this.billabeCost;
	}

	public void setBillabeCost(BigDecimal billabeCost) {
		this.billabeCost = billabeCost;
	}

	public Boolean getUsed() {
		return this.used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public BigDecimal getNetUpliftQuantity() {
		return this.netUpliftQuantity;
	}

	public void setNetUpliftQuantity(BigDecimal netUpliftQuantity) {
		this.netUpliftQuantity = netUpliftQuantity;
	}

	public FuelTicketFuelingOperation getFuelingOperation() {
		return this.fuelingOperation;
	}

	public void setFuelingOperation(FuelTicketFuelingOperation fuelingOperation) {
		this.fuelingOperation = fuelingOperation;
	}

	public FuelingType getTransport() {
		return this.transport;
	}

	public void setTransport(FuelingType transport) {
		this.transport = transport;
	}

	public OperationType getOperationType() {
		return this.operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public String getErpNo() {
		return this.erpNo;
	}

	public void setErpNo(String erpNo) {
		this.erpNo = erpNo;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getFlightID() {
		return this.flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}
}
