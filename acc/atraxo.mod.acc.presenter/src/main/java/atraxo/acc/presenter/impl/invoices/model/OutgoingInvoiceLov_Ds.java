/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoices.model;

import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryLov_Ds;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = OutgoingInvoice.class)
public class OutgoingInvoiceLov_Ds
		extends
			AbstractSubsidiaryLov_Ds<OutgoingInvoice> {

	public static final String ALIAS = "acc_OutgoingInvoiceLov_Ds";

	public static final String F_INVOICEID = "invoiceId";
	public static final String F_INVOICENO = "invoiceNo";
	public static final String F_STATUS = "status";

	@DsField(path = "id")
	private Integer invoiceId;

	@DsField
	private String invoiceNo;

	@DsField
	private BillStatus status;

	/**
	 * Default constructor
	 */
	public OutgoingInvoiceLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public OutgoingInvoiceLov_Ds(OutgoingInvoice e) {
		super(e);
	}

	public Integer getInvoiceId() {
		return this.invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public BillStatus getStatus() {
		return this.status;
	}

	public void setStatus(BillStatus status) {
		this.status = status;
	}
}
