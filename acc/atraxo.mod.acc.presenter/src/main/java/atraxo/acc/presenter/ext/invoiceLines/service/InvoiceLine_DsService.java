/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.invoiceLines.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.invoices.IInvoiceService;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.presenter.impl.invoiceLines.model.InvoiceLine_Ds;
import atraxo.acc.presenter.impl.invoiceLines.model.InvoiceLine_DsParam;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.ext.contracts.CostVat;
import atraxo.cmm.domain.impl.contracts.Contract;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class InvoiceLine_DsService extends AbstractEntityDsService<InvoiceLine_Ds, InvoiceLine_Ds, Object, InvoiceLine>
		implements IDsService<InvoiceLine_Ds, InvoiceLine_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<InvoiceLine_Ds, InvoiceLine_Ds, Object> builder, List<InvoiceLine_Ds> result) throws Exception {

		super.postFind(builder, result);

		IInvoiceService invoiceService = (IInvoiceService) this.findEntityService(Invoice.class);
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);

		for (InvoiceLine_Ds ds : result) {
			Invoice invoice = invoiceService.findById(ds.getInvoiceId());
			if (invoice.getContract() == null) {
				return;
			}
			Date date = ds.getDeliveryDate() == null ? new Date() : ds.getDeliveryDate();
			CostVat priceVat = contractService.getPriceVat(invoice.getContract().getId(), invoice.getUnit().getCode(),
					invoice.getCurrency().getCode(), date, ds.getFlightType());
			ds.setContractPrice(priceVat.getCost());
			ds.setVatRate(priceVat.getVat());

		}

	}

	@Override
	protected void postSummaries(IQueryBuilder<InvoiceLine_Ds, InvoiceLine_Ds, Object> builder, List<InvoiceLine_Ds> findResult,
			Map<String, Object> sum) throws Exception {
		super.postSummaries(builder, findResult, sum);
		BigDecimal quantityTotal = BigDecimal.ZERO;
		BigDecimal amountTotal = BigDecimal.ZERO;
		BigDecimal vatAmountTotal = BigDecimal.ZERO;

		for (InvoiceLine_Ds ds : findResult) {
			quantityTotal = quantityTotal.add(ds.getQuantity() == null ? BigDecimal.ZERO : ds.getQuantity());
			amountTotal = amountTotal.add(ds.getAmount() == null ? BigDecimal.ZERO : ds.getAmount());
			vatAmountTotal = vatAmountTotal.add(ds.getVat() == null ? BigDecimal.ZERO : ds.getVat());
		}
		InvoiceLine_DsParam params = (InvoiceLine_DsParam) builder.getParams();
		if (builder.getFilter().getInvoiceId() != null) {
			this.setBalancedValues(findResult, params, builder.getFilter().getInvoiceId());
		}
		sum.put("quantity", quantityTotal);
		sum.put("amount", amountTotal);
		sum.put("vat", vatAmountTotal);
	}

	private void setBalancedValues(List<InvoiceLine_Ds> findResult, InvoiceLine_DsParam params, Integer invoiceId) throws Exception {
		IInvoiceService invoiceService = (IInvoiceService) this.findEntityService(Invoice.class);
		Invoice invoice = invoiceService.findById(invoiceId);
		if (invoice == null) {
			return;
		}
		if (findResult.isEmpty()) {
			params.setAmountBalance(invoice.getTotalAmount().negate().setScale(2, RoundingMode.HALF_UP));
			params.setQuantityBalance(invoice.getQuantity().negate().setScale(2, RoundingMode.HALF_UP));
			params.setOrigAmountBalance(invoice.getTotalAmount().negate());
			params.setOrigQuantityBalance(invoice.getQuantity().negate());
			return;
		}

		if (invoice.getTotalAmount() == null || invoice.getQuantity() == null) {
			return;
		}
		Collection<InvoiceLine> invoiceLines = invoice.getInvoiceLines();
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal totalQuantity = BigDecimal.ZERO;
		for (InvoiceLine il : invoiceLines) {
			totalAmount = totalAmount.add(il.getAmount() != null ? il.getAmount() : BigDecimal.ZERO);
			totalQuantity = totalQuantity.add(il.getQuantity() != null ? il.getQuantity() : BigDecimal.ZERO);
		}
		BigDecimal amountBalance = totalAmount.setScale(2, RoundingMode.HALF_UP).subtract(invoice.getTotalAmount().setScale(2, RoundingMode.HALF_UP));
		BigDecimal quantityBalance = totalQuantity.setScale(2, RoundingMode.HALF_UP)
				.subtract(invoice.getQuantity().setScale(2, RoundingMode.HALF_UP));
		params.setAmountBalance(amountBalance.setScale(2, RoundingMode.HALF_UP));
		params.setQuantityBalance(quantityBalance.setScale(2, RoundingMode.HALF_UP));
		params.setOrigAmountBalance(amountBalance);
		params.setOrigQuantityBalance(quantityBalance);
	}

}
