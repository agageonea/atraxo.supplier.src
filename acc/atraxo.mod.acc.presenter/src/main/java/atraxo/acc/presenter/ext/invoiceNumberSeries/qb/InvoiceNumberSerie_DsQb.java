/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.acc.presenter.ext.invoiceNumberSeries.qb;

import atraxo.acc.presenter.impl.invoiceNumberSeries.model.InvoiceNumberSerie_Ds;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class InvoiceNumberSerie_DsQb extends QueryBuilderWithJpql<InvoiceNumberSerie_Ds, InvoiceNumberSerie_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		this.addFilterCondition("e.type != :type");
		this.addCustomFilterItem("type", DocumentNumberSeriesType._CUSTOMER_);
	}

}
