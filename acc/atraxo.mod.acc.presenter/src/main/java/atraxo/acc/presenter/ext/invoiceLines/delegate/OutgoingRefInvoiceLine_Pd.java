package atraxo.acc.presenter.ext.invoiceLines.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IOutgoingInvoiceLineService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.creditMemo.CreditMemo_Service;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.presenter.impl.invoiceLines.model.OutgoingRefInvoiceLine_Ds;
import atraxo.acc.presenter.impl.invoiceLines.model.OutgoingRefInvoiceLine_DsParam;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class OutgoingRefInvoiceLine_Pd extends AbstractPresenterDelegate {

	public void addNewCreditMemoLine(List<OutgoingRefInvoiceLine_Ds> list, OutgoingRefInvoiceLine_DsParam params) throws Exception {
		InfoForCreditMemo ifcm = this.getInvoiceData(list, params);
		CreditMemo_Service creditMemoService = this.getApplicationContext().getBean(CreditMemo_Service.class);
		creditMemoService.addNewInvoiceLine(ifcm.outgoingInvoice, ifcm.oilList);
		this.updateFuelEvent(ifcm.outgoingInvoice, ifcm.oilList);
	}

	/**
	 * After adding back a Fuel Event on a Credit Memo, that Fuel Event is also updated
	 *
	 * @param creditMemo
	 * @param lines
	 * @throws Exception
	 */
	private void updateFuelEvent(OutgoingInvoice creditMemo, List<OutgoingInvoiceLine> lines) throws Exception {
		IFuelEventService fuelEventService = (IFuelEventService) this.findEntityService(FuelEvent.class);
		List<FuelEvent> fuelEvents = new ArrayList<>();

		OutgoingInvoiceStatus invStatus;
		for (OutgoingInvoiceLine line : lines) {
			FuelEvent fuelEvent = line.getFuelEvent();
			switch (creditMemo.getStatus()) {
			case _AWAITING_PAYMENT_:
				invStatus = OutgoingInvoiceStatus._AWAITING_PAYMENT_;
				break;
			case _AWAITING_APPROVAL_:
				invStatus = OutgoingInvoiceStatus._AWAITING_APPROVAL_;
				break;
			case _PAID_:
				invStatus = OutgoingInvoiceStatus._PAID_;
				break;
			case _CREDITED_:
				invStatus = OutgoingInvoiceStatus._CREDITED_;
				break;
			default:
				invStatus = OutgoingInvoiceStatus._NOT_INVOICED_;
				break;
			}

			fuelEvent.setCreditMemoNo(creditMemo.getInvoiceNo());
			fuelEvent.setInvoiceStatus(invStatus);
			fuelEvent.setRegenerateDetails(false);
			fuelEvents.add(fuelEvent);
		}
		fuelEventService.update(fuelEvents);
	}

	private InfoForCreditMemo getInvoiceData(List<OutgoingRefInvoiceLine_Ds> list, OutgoingRefInvoiceLine_DsParam params) throws Exception {
		IOutgoingInvoiceService oiService = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		IOutgoingInvoiceLineService oilService = (IOutgoingInvoiceLineService) this.findEntityService(OutgoingInvoiceLine.class);
		OutgoingInvoice oi = oiService.findById(params.getInvoiceId());
		List<OutgoingInvoiceLine> oilList = new ArrayList<>();
		for (OutgoingRefInvoiceLine_Ds ds : list) {
			OutgoingInvoiceLine oil = oilService.findById(ds.getId());
			oilList.add(oil);
		}
		return new InfoForCreditMemo(oi, oilList);
	}

	private class InfoForCreditMemo {
		OutgoingInvoice outgoingInvoice;
		List<OutgoingInvoiceLine> oilList;

		public InfoForCreditMemo(OutgoingInvoice outgoingInvoice, List<OutgoingInvoiceLine> oilList) {
			super();
			this.outgoingInvoice = outgoingInvoice;
			this.oilList = oilList;
		}

	}
}
