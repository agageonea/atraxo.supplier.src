/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.payableAccruals.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.payableAccruals.IPayableAccrualService;
import atraxo.acc.domain.impl.acc_type.InvoiceProcessStatus;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.acc.presenter.impl.payableAccruals.model.PayableAccrual_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class PayableAccrual_DsService extends AbstractEntityDsService<PayableAccrual_Ds, PayableAccrual_Ds, Object, PayableAccrual>
		implements IDsService<PayableAccrual_Ds, PayableAccrual_Ds, Object> {

	public BigDecimal getOpenAccruals() throws Exception {
		IPayableAccrualService srv = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		Map<String, Object> params = new HashMap<>();
		params.put("invProcessStatus", InvoiceProcessStatus._NOT_INVOICED_);
		int count = srv.findEntitiesByAttributes(params).size();
		params = new HashMap<>();
		params.put("invProcessStatus", InvoiceProcessStatus._PARTIAL_INVOICED_);
		count = count + srv.findEntitiesByAttributes(params).size();
		return new BigDecimal(count).setScale(2, RoundingMode.HALF_UP);
	}

	public BigDecimal getAccrualsAmount() throws Exception {
		IPayableAccrualService srv = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		Map<String, Object> params = new HashMap<>();
		params.put("isOpen", Boolean.TRUE);
		List<PayableAccrual> list = srv.findEntitiesByAttributes(params);
		BigDecimal amount = BigDecimal.ZERO;
		for (PayableAccrual accrual : list) {
			amount = amount.add(accrual.getAccrualAmountSys().add(accrual.getAccrualVATSys(), MathContext.DECIMAL64));
		}
		return amount.setScale(2, RoundingMode.HALF_UP);
	}

	@Override
	public List<PayableAccrual_Ds> find(IQueryBuilder<PayableAccrual_Ds, PayableAccrual_Ds, Object> builder, List<String> fieldNames)
			throws Exception {

		if (builder == null) {
			throw new Exception("Cannot run a query with null query builder.");
		}
		QueryBuilderWithJpql<PayableAccrual_Ds, PayableAccrual_Ds, Object> bld = (QueryBuilderWithJpql<PayableAccrual_Ds, PayableAccrual_Ds, Object>) builder;

		List<PayableAccrual> entities = bld.createQuery(this.getEntityClass()).getResultList();

		String month = builder.getFilter().getMonth();
		List<PayableAccrual> filterList = new ArrayList<>();
		for (PayableAccrual pa : entities) {
			if (pa.getMonth().equalsIgnoreCase(month)) {
				filterList.add(pa);
			}
		}
		return this.getConverter().entitiesToModels(filterList, bld.getEntityManager(), fieldNames);
	}

	@Override
	public Long count(IQueryBuilder<PayableAccrual_Ds, PayableAccrual_Ds, Object> builder) throws Exception {
		IPayableAccrualService payableAccrualService = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		List<PayableAccrual> accruals = payableAccrualService.findAll();
		String month = builder.getFilter().getMonth();
		List<PayableAccrual> tempList = new ArrayList<>();
		for (PayableAccrual ds : accruals) {
			if (ds.getMonth().equalsIgnoreCase(month)) {
				tempList.add(ds);
			}
		}
		accruals.retainAll(tempList);
		return new Long(accruals.size());
	}

}
