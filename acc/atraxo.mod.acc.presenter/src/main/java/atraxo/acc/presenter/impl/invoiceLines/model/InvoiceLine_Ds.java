/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoiceLines.model;

import atraxo.acc.domain.impl.acc_type.CheckResult;
import atraxo.acc.domain.impl.acc_type.CheckStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceLinesTaxType;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;
import seava.j4e.api.model.IModelWithId;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = InvoiceLine.class, sort = {@SortField(field = InvoiceLine_Ds.F_DELIVERYDATE)})
@RefLookups({
		@RefLookup(refId = InvoiceLine_Ds.F_GROSSQTYUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = InvoiceLine_Ds.F_GROSSQTYUNITCODE)}),
		@RefLookup(refId = InvoiceLine_Ds.F_NETQTYUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = InvoiceLine_Ds.F_NETQTYUNITCODE)}),
		@RefLookup(refId = InvoiceLine_Ds.F_INVOICEID),
		@RefLookup(refId = InvoiceLine_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = InvoiceLine_Ds.F_UNITCODE)}),
		@RefLookup(refId = InvoiceLine_Ds.F_AIRLINEDESIGNATORID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = InvoiceLine_Ds.F_AIRLINEDESIGNATORCODE)})})
public class InvoiceLine_Ds extends AbstractDs_Ds<InvoiceLine>
		implements
			IModelWithId<Integer> {

	public static final String ALIAS = "acc_InvoiceLine_Ds";

	public static final String F_INVOICEID = "invoiceId";
	public static final String F_INVOICENO = "invoiceNo";
	public static final String F_INVOICETOTALAMOUNT = "invoiceTotalAmount";
	public static final String F_INVOICETOTALQUANTITY = "invoiceTotalQuantity";
	public static final String F_CONTRACTID = "contractId";
	public static final String F_CONTRACTCODE = "contractCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_GROSSQTYUNITID = "grossQtyUnitId";
	public static final String F_GROSSQTYUNITCODE = "grossQtyUnitCode";
	public static final String F_NETQTYUNITID = "netQtyUnitId";
	public static final String F_NETQTYUNITCODE = "netQtyUnitCode";
	public static final String F_REFERENCEINVLINEID = "referenceInvLineId";
	public static final String F_AIRLINEDESIGNATORID = "airlineDesignatorId";
	public static final String F_AIRLINEDESIGNATORCODE = "airlineDesignatorCode";
	public static final String F_AIRCRAFTREGISTRATIONNO = "aircraftRegistrationNo";
	public static final String F_AMOUNT = "amount";
	public static final String F_CALCULATEDVAT = "calculatedVat";
	public static final String F_DELIVERYDATE = "deliveryDate";
	public static final String F_EVENTCHECKSTATUS = "eventCheckStatus";
	public static final String F_EVENTDATE = "eventDate";
	public static final String F_FLIGHTNO = "flightNo";
	public static final String F_FLIGHTSTRING = "flightString";
	public static final String F_FLIGHTTYPE = "flightType";
	public static final String F_GROSSQUANTITY = "grossQuantity";
	public static final String F_ID = "id";
	public static final String F_INVOICEDVAT = "invoicedVat";
	public static final String F_INVOICELINE = "invoiceLine";
	public static final String F_NETQUANTITY = "netQuantity";
	public static final String F_ORIGINALVATCHECKSTATUS = "originalVatCheckStatus";
	public static final String F_OVERALLCHECKSTATUS = "overAllCheckStatus";
	public static final String F_QUANTITY = "quantity";
	public static final String F_QUANTITYCHECKSTATUS = "quantityCheckStatus";
	public static final String F_SUFFIX = "suffix";
	public static final String F_TAXTYPE = "taxType";
	public static final String F_TICKETNO = "ticketNo";
	public static final String F_VAT = "vat";
	public static final String F_VATCHECKSTATUS = "vatCheckStatus";
	public static final String F_ORIGINALQUANTITYCHECKSTATUS = "originalQuantityCheckStatus";
	public static final String F_QUANTITYCHECKRESULT = "quantityCheckResult";
	public static final String F_VATCHECKRESULT = "vatCheckResult";
	public static final String F_CONTRACTPRICE = "contractPrice";
	public static final String F_VATRATE = "vatRate";

	@DsField(join = "left", path = "invoice.id")
	private Integer invoiceId;

	@DsField(join = "left", path = "invoice.invoiceNo")
	private String invoiceNo;

	@DsField(join = "left", path = "invoice.totalAmount")
	private BigDecimal invoiceTotalAmount;

	@DsField(join = "left", path = "invoice.quantity")
	private BigDecimal invoiceTotalQuantity;

	@DsField(join = "left", path = "contract.id")
	private Integer contractId;

	@DsField(join = "left", path = "contract.code")
	private String contractCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "grossQuantityUnit.id")
	private Integer grossQtyUnitId;

	@DsField(join = "left", path = "grossQuantityUnit.code")
	private String grossQtyUnitCode;

	@DsField(join = "left", path = "netQuantityUnit.id")
	private Integer netQtyUnitId;

	@DsField(join = "left", path = "netQuantityUnit.code")
	private String netQtyUnitCode;

	@DsField(join = "left", path = "referenceInvoiceLine.id")
	private Integer referenceInvLineId;

	@DsField(join = "left", path = "airlineDesignator.id")
	private Integer airlineDesignatorId;

	@DsField(join = "left", path = "airlineDesignator.code")
	private String airlineDesignatorCode;

	@DsField(path = "aircraftRegistrationNumber")
	private String aircraftRegistrationNo;

	@DsField
	private BigDecimal amount;

	@DsField
	private BigDecimal calculatedVat;

	@DsField
	private Date deliveryDate;

	@DsField
	private CheckStatus eventCheckStatus;

	@DsField
	private Date eventDate;

	@DsField
	private String flightNo;

	@DsField
	private String flightString;

	@DsField
	private FlightTypeIndicator flightType;

	@DsField
	private BigDecimal grossQuantity;

	@DsField
	private Integer id;

	@DsField
	private BigDecimal invoicedVat;

	@DsField
	private Integer invoiceLine;

	@DsField
	private BigDecimal netQuantity;

	@DsField
	private CheckStatus originalVatCheckStatus;

	@DsField
	private CheckStatus overAllCheckStatus;

	@DsField
	private BigDecimal quantity;

	@DsField
	private CheckStatus quantityCheckStatus;

	@DsField
	private Suffix suffix;

	@DsField
	private InvoiceLinesTaxType taxType;

	@DsField
	private String ticketNo;

	@DsField
	private BigDecimal vat;

	@DsField
	private CheckStatus vatCheckStatus;

	@DsField
	private CheckStatus originalQuantityCheckStatus;

	@DsField
	private CheckResult quantityCheckResult;

	@DsField
	private CheckResult vatCheckResult;

	@DsField(fetch = false)
	private BigDecimal contractPrice;

	@DsField(fetch = false)
	private BigDecimal vatRate;

	/**
	 * Default constructor
	 */
	public InvoiceLine_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public InvoiceLine_Ds(InvoiceLine e) {
		super(e);
	}

	public Integer getInvoiceId() {
		return this.invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public BigDecimal getInvoiceTotalAmount() {
		return this.invoiceTotalAmount;
	}

	public void setInvoiceTotalAmount(BigDecimal invoiceTotalAmount) {
		this.invoiceTotalAmount = invoiceTotalAmount;
	}

	public BigDecimal getInvoiceTotalQuantity() {
		return this.invoiceTotalQuantity;
	}

	public void setInvoiceTotalQuantity(BigDecimal invoiceTotalQuantity) {
		this.invoiceTotalQuantity = invoiceTotalQuantity;
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getGrossQtyUnitId() {
		return this.grossQtyUnitId;
	}

	public void setGrossQtyUnitId(Integer grossQtyUnitId) {
		this.grossQtyUnitId = grossQtyUnitId;
	}

	public String getGrossQtyUnitCode() {
		return this.grossQtyUnitCode;
	}

	public void setGrossQtyUnitCode(String grossQtyUnitCode) {
		this.grossQtyUnitCode = grossQtyUnitCode;
	}

	public Integer getNetQtyUnitId() {
		return this.netQtyUnitId;
	}

	public void setNetQtyUnitId(Integer netQtyUnitId) {
		this.netQtyUnitId = netQtyUnitId;
	}

	public String getNetQtyUnitCode() {
		return this.netQtyUnitCode;
	}

	public void setNetQtyUnitCode(String netQtyUnitCode) {
		this.netQtyUnitCode = netQtyUnitCode;
	}

	public Integer getReferenceInvLineId() {
		return this.referenceInvLineId;
	}

	public void setReferenceInvLineId(Integer referenceInvLineId) {
		this.referenceInvLineId = referenceInvLineId;
	}

	public Integer getAirlineDesignatorId() {
		return this.airlineDesignatorId;
	}

	public void setAirlineDesignatorId(Integer airlineDesignatorId) {
		this.airlineDesignatorId = airlineDesignatorId;
	}

	public String getAirlineDesignatorCode() {
		return this.airlineDesignatorCode;
	}

	public void setAirlineDesignatorCode(String airlineDesignatorCode) {
		this.airlineDesignatorCode = airlineDesignatorCode;
	}

	public String getAircraftRegistrationNo() {
		return this.aircraftRegistrationNo;
	}

	public void setAircraftRegistrationNo(String aircraftRegistrationNo) {
		this.aircraftRegistrationNo = aircraftRegistrationNo;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCalculatedVat() {
		return this.calculatedVat;
	}

	public void setCalculatedVat(BigDecimal calculatedVat) {
		this.calculatedVat = calculatedVat;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public CheckStatus getEventCheckStatus() {
		return this.eventCheckStatus;
	}

	public void setEventCheckStatus(CheckStatus eventCheckStatus) {
		this.eventCheckStatus = eventCheckStatus;
	}

	public Date getEventDate() {
		return this.eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getFlightString() {
		return this.flightString;
	}

	public void setFlightString(String flightString) {
		this.flightString = flightString;
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}

	public BigDecimal getGrossQuantity() {
		return this.grossQuantity;
	}

	public void setGrossQuantity(BigDecimal grossQuantity) {
		this.grossQuantity = grossQuantity;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getInvoicedVat() {
		return this.invoicedVat;
	}

	public void setInvoicedVat(BigDecimal invoicedVat) {
		this.invoicedVat = invoicedVat;
	}

	public Integer getInvoiceLine() {
		return this.invoiceLine;
	}

	public void setInvoiceLine(Integer invoiceLine) {
		this.invoiceLine = invoiceLine;
	}

	public BigDecimal getNetQuantity() {
		return this.netQuantity;
	}

	public void setNetQuantity(BigDecimal netQuantity) {
		this.netQuantity = netQuantity;
	}

	public CheckStatus getOriginalVatCheckStatus() {
		return this.originalVatCheckStatus;
	}

	public void setOriginalVatCheckStatus(CheckStatus originalVatCheckStatus) {
		this.originalVatCheckStatus = originalVatCheckStatus;
	}

	public CheckStatus getOverAllCheckStatus() {
		return this.overAllCheckStatus;
	}

	public void setOverAllCheckStatus(CheckStatus overAllCheckStatus) {
		this.overAllCheckStatus = overAllCheckStatus;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public CheckStatus getQuantityCheckStatus() {
		return this.quantityCheckStatus;
	}

	public void setQuantityCheckStatus(CheckStatus quantityCheckStatus) {
		this.quantityCheckStatus = quantityCheckStatus;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public InvoiceLinesTaxType getTaxType() {
		return this.taxType;
	}

	public void setTaxType(InvoiceLinesTaxType taxType) {
		this.taxType = taxType;
	}

	public String getTicketNo() {
		return this.ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public BigDecimal getVat() {
		return this.vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public CheckStatus getVatCheckStatus() {
		return this.vatCheckStatus;
	}

	public void setVatCheckStatus(CheckStatus vatCheckStatus) {
		this.vatCheckStatus = vatCheckStatus;
	}

	public CheckStatus getOriginalQuantityCheckStatus() {
		return this.originalQuantityCheckStatus;
	}

	public void setOriginalQuantityCheckStatus(
			CheckStatus originalQuantityCheckStatus) {
		this.originalQuantityCheckStatus = originalQuantityCheckStatus;
	}

	public CheckResult getQuantityCheckResult() {
		return this.quantityCheckResult;
	}

	public void setQuantityCheckResult(CheckResult quantityCheckResult) {
		this.quantityCheckResult = quantityCheckResult;
	}

	public CheckResult getVatCheckResult() {
		return this.vatCheckResult;
	}

	public void setVatCheckResult(CheckResult vatCheckResult) {
		this.vatCheckResult = vatCheckResult;
	}

	public BigDecimal getContractPrice() {
		return this.contractPrice;
	}

	public void setContractPrice(BigDecimal contractPrice) {
		this.contractPrice = contractPrice;
	}

	public BigDecimal getVatRate() {
		return this.vatRate;
	}

	public void setVatRate(BigDecimal vatRate) {
		this.vatRate = vatRate;
	}
}
