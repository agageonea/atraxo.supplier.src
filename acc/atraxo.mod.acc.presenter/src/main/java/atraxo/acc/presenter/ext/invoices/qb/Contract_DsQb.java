/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.acc.presenter.ext.invoices.qb;

import atraxo.acc.presenter.impl.invoices.model.Contract_Ds;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class Contract_DsQb extends QueryBuilderWithJpql<Contract_Ds, Contract_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();

		this.addFilterCondition("e.status = :status");

		this.addCustomFilterItem("status", ContractStatus._EFFECTIVE_);

	}
}
