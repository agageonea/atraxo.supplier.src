/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.fuelEvents.model;

import java.util.Date;
/**
 * Generated code. Do not modify in this file.
 */
public class FuelEvents_DsParam {

	public static final String f_outgoingInvoiceId = "outgoingInvoiceId";
	public static final String f_aircraftReg = "aircraftReg";
	public static final String f_flight = "flight";
	public static final String f_fuelTicket = "fuelTicket";
	public static final String f_supplier = "supplier";
	public static final String f_dateStart = "dateStart";
	public static final String f_dateEnd = "dateEnd";
	public static final String f_remark = "remark";
	public static final String f_exportFuelEventDescription = "exportFuelEventDescription";
	public static final String f_exportFuelEventResult = "exportFuelEventResult";
	public static final String f_exportFuelEventOption = "exportFuelEventOption";

	private Integer outgoingInvoiceId;

	private String aircraftReg;

	private String flight;

	private String fuelTicket;

	private String supplier;

	private Date dateStart;

	private Date dateEnd;

	private String remark;

	private String exportFuelEventDescription;

	private Boolean exportFuelEventResult;

	private String exportFuelEventOption;

	public Integer getOutgoingInvoiceId() {
		return this.outgoingInvoiceId;
	}

	public void setOutgoingInvoiceId(Integer outgoingInvoiceId) {
		this.outgoingInvoiceId = outgoingInvoiceId;
	}

	public String getAircraftReg() {
		return this.aircraftReg;
	}

	public void setAircraftReg(String aircraftReg) {
		this.aircraftReg = aircraftReg;
	}

	public String getFlight() {
		return this.flight;
	}

	public void setFlight(String flight) {
		this.flight = flight;
	}

	public String getFuelTicket() {
		return this.fuelTicket;
	}

	public void setFuelTicket(String fuelTicket) {
		this.fuelTicket = fuelTicket;
	}

	public String getSupplier() {
		return this.supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public Date getDateStart() {
		return this.dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return this.dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getExportFuelEventDescription() {
		return this.exportFuelEventDescription;
	}

	public void setExportFuelEventDescription(String exportFuelEventDescription) {
		this.exportFuelEventDescription = exportFuelEventDescription;
	}

	public Boolean getExportFuelEventResult() {
		return this.exportFuelEventResult;
	}

	public void setExportFuelEventResult(Boolean exportFuelEventResult) {
		this.exportFuelEventResult = exportFuelEventResult;
	}

	public String getExportFuelEventOption() {
		return this.exportFuelEventOption;
	}

	public void setExportFuelEventOption(String exportFuelEventOption) {
		this.exportFuelEventOption = exportFuelEventOption;
	}
}
