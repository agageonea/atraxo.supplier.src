/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoiceLines.model;

/**
 * Generated code. Do not modify in this file.
 */
public class OutgoingInvoiceLine_DsParam {

	public static final String f_invoiceId = "invoiceId";
	public static final String f_generatedCreditMemoId = "generatedCreditMemoId";
	public static final String f_creditMemoReason = "creditMemoReason";
	public static final String f_numberSeriesGeneratorMessage = "numberSeriesGeneratorMessage";

	private Integer invoiceId;

	private Integer generatedCreditMemoId;

	private String creditMemoReason;

	private String numberSeriesGeneratorMessage;

	public Integer getInvoiceId() {
		return this.invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Integer getGeneratedCreditMemoId() {
		return this.generatedCreditMemoId;
	}

	public void setGeneratedCreditMemoId(Integer generatedCreditMemoId) {
		this.generatedCreditMemoId = generatedCreditMemoId;
	}

	public String getCreditMemoReason() {
		return this.creditMemoReason;
	}

	public void setCreditMemoReason(String creditMemoReason) {
		this.creditMemoReason = creditMemoReason;
	}

	public String getNumberSeriesGeneratorMessage() {
		return this.numberSeriesGeneratorMessage;
	}

	public void setNumberSeriesGeneratorMessage(
			String numberSeriesGeneratorMessage) {
		this.numberSeriesGeneratorMessage = numberSeriesGeneratorMessage;
	}
}
