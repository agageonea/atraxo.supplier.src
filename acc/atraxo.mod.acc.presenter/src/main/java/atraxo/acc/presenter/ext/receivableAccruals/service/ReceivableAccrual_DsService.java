/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.receivableAccruals.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.receivableAccruals.IReceivableAccrualService;
import atraxo.acc.domain.impl.acc_type.InvoiceProcessStatus;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.acc.presenter.impl.receivableAccruals.model.ReceivableAccrual_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ReceivableAccrual_DsService extends AbstractEntityDsService<ReceivableAccrual_Ds, ReceivableAccrual_Ds, Object, ReceivableAccrual>
		implements IDsService<ReceivableAccrual_Ds, ReceivableAccrual_Ds, Object> {

	public BigDecimal getOpenAccruals() throws Exception {
		IReceivableAccrualService srv = (IReceivableAccrualService) this.findEntityService(ReceivableAccrual.class);
		Map<String, Object> params = new HashMap<>();
		params.put("invProcessStatus", InvoiceProcessStatus._NOT_INVOICED_);
		int count = srv.findEntitiesByAttributes(params).size();
		params = new HashMap<>();
		params.put("invProcessStatus", InvoiceProcessStatus._PARTIAL_INVOICED_);
		count = count + srv.findEntitiesByAttributes(params).size();
		return new BigDecimal(count).setScale(2, RoundingMode.HALF_UP);
	}

	public BigDecimal getAccrualsAmount() throws Exception {
		IReceivableAccrualService srv = (IReceivableAccrualService) this.findEntityService(ReceivableAccrual.class);
		Map<String, Object> params = new HashMap<>();
		params.put("isOpen", Boolean.TRUE);
		List<ReceivableAccrual> list = srv.findEntitiesByAttributes(params);
		BigDecimal amount = BigDecimal.ZERO;
		for (ReceivableAccrual accrual : list) {
			amount = amount.add(accrual.getAccrualAmountSys().add(accrual.getAccrualVATSys(), MathContext.DECIMAL64));
		}
		return amount.setScale(2, RoundingMode.HALF_UP);
	}

	@Override
	public List<ReceivableAccrual_Ds> find(IQueryBuilder<ReceivableAccrual_Ds, ReceivableAccrual_Ds, Object> builder, List<String> fieldNames)
			throws Exception {

		if (builder == null) {
			throw new Exception("Cannot run a query with null query builder.");
		}
		QueryBuilderWithJpql<ReceivableAccrual_Ds, ReceivableAccrual_Ds, Object> bld = (QueryBuilderWithJpql<ReceivableAccrual_Ds, ReceivableAccrual_Ds, Object>) builder;

		List<ReceivableAccrual> entities = bld.createQuery(this.getEntityClass()).getResultList();

		String month = builder.getFilter().getMonth();
		List<ReceivableAccrual> filterList = new ArrayList<>();
		for (ReceivableAccrual pa : entities) {
			if (pa.getMonth().equalsIgnoreCase(month)) {
				filterList.add(pa);
			}
		}
		return this.getConverter().entitiesToModels(filterList, bld.getEntityManager(), fieldNames);
	}

	@Override
	public Long count(IQueryBuilder<ReceivableAccrual_Ds, ReceivableAccrual_Ds, Object> builder) throws Exception {
		IReceivableAccrualService receivableAccrualService = (IReceivableAccrualService) this.findEntityService(ReceivableAccrual.class);
		List<ReceivableAccrual> accruals = receivableAccrualService.findAll();
		String month = builder.getFilter().getMonth();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
		Date startDate = sdf.parse(month);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		calendar.add(Calendar.MONTH, 1);
		Date endDate = calendar.getTime();
		List<ReceivableAccrual> tempList = new ArrayList<>();
		for (ReceivableAccrual ds : accruals) {
			if (ds.getFuelingDate().compareTo(startDate) >= 0 && ds.getFuelingDate().compareTo(endDate) < 0) {
				tempList.add(ds);
			}
		}
		accruals.retainAll(tempList);
		return new Long(accruals.size());
	}

}
