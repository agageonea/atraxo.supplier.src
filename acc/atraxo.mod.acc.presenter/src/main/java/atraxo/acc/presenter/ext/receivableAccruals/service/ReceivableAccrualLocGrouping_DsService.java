/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.receivableAccruals.service;

import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import atraxo.acc.business.api.receivableAccruals.IReceivableAccrualService;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.acc.presenter.impl.receivableAccruals.model.ReceivableAccrualLocGrouping_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ReceivableAccrualLocGrouping_DsService
		extends
			AbstractEntityDsService<ReceivableAccrualLocGrouping_Ds, ReceivableAccrualLocGrouping_Ds, Object, ReceivableAccrual>
		implements
			IDsService<ReceivableAccrualLocGrouping_Ds, ReceivableAccrualLocGrouping_Ds, Object> {

	@Override
	protected void postFind(IQueryBuilder<ReceivableAccrualLocGrouping_Ds, ReceivableAccrualLocGrouping_Ds, Object> builder,
			List<ReceivableAccrualLocGrouping_Ds> result) throws Exception {		
		super.postFind(builder, result);
		IReceivableAccrualService receivableAccrualService = (IReceivableAccrualService) this.findEntityService(ReceivableAccrual.class);
		List<ReceivableAccrual> receivableAccrual = receivableAccrualService.findAll();
		String filter = builder.getFilter().getMonth();
		HashMap<String, ReceivableAccrualDsStore> hashMap = new HashMap<String, ReceivableAccrualDsStore>();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
		for(ReceivableAccrual pa : receivableAccrual){
			if(filter.equalsIgnoreCase(sdf.format(this.formatDate(pa.getFuelingDate())))){
				String key = pa.getLocation().getCode();
				if(hashMap.containsKey(key)){
					ReceivableAccrualDsStore store = hashMap.get(key);
					store.setDate(pa.getFuelingDate());
					store.setLocationCode(key);
					store.setEvents(store.getEvents()+1);
					store.setNetAmount(store.getNetAmount().add(pa.getAccrualAmountSys(), MathContext.DECIMAL64));
					store.setVatAmount(store.getVatAmount().add(pa.getAccrualVATSys(), MathContext.DECIMAL64));
					store.setGrossAmount(store.getNetAmount().add(store.getVatAmount(), MathContext.DECIMAL64));
//					store.setInvoiceCurrencyCode(pa.getInvoicedCurrency().getCode());
//					store.setExpInvoiceCurrencyCode(pa.getExpectedCurrency().getCode());
				}else{
					ReceivableAccrualDsStore store = new ReceivableAccrualDsStore();
					store.setLocationCode(key);
					store.setDate(pa.getFuelingDate());
					store.setEvents(1);
					store.setNetAmount(pa.getAccrualAmountSys());
					store.setVatAmount(pa.getAccrualVATSys());
					store.setGrossAmount(store.getNetAmount().add(store.getVatAmount(), MathContext.DECIMAL64));
//					store.setInvoiceCurrencyCode(pa.getInvoicedCurrency().getCode());
//					store.setExpInvoiceCurrencyCode(pa.getExpectedCurrency().getCode());
					hashMap.put(key, store);
				}
			}
		}
		
		List<ReceivableAccrualLocGrouping_Ds> retList = new ArrayList<ReceivableAccrualLocGrouping_Ds>();
		for(ReceivableAccrualDsStore store : hashMap.values()){
			ReceivableAccrualLocGrouping_Ds ds = new ReceivableAccrualLocGrouping_Ds();
			ds.setEvents(store.getEvents());
			ds.setMonth(sdf.format(store.getDate()));
			ds.setGrossAmount(store.getGrossAmount());
			ds.setVatAmount(store.getVatAmount());
			ds.setNetAmount(store.getNetAmount());
			ds.setLocation(store.getLocationCode());
//			ds.setInvCurrCode(store.getInvoiceCurrencyCode());
//			ds.setExpCurrCode(store.getExpInvoiceCurrencyCode());
			retList.add(ds);
		}
		result.addAll(retList);
		result.retainAll(retList);
	}
	
	@Override
	public Long count(IQueryBuilder<ReceivableAccrualLocGrouping_Ds, ReceivableAccrualLocGrouping_Ds, Object> builder) throws Exception {		
		IReceivableAccrualService receivableAccrualService = (IReceivableAccrualService) this.findEntityService(ReceivableAccrual.class);
		List<ReceivableAccrual> receivableAccrual = receivableAccrualService.findAll();
		HashMap<Date, Integer> hashMap = new HashMap<Date, Integer>();
		for(ReceivableAccrual pa : receivableAccrual){
			Date usedDate = this.formatDate(pa.getFuelingDate());
			if(hashMap.containsKey(usedDate)){
				Integer count = hashMap.get(usedDate);
				count=count+1;
			}else{				
				hashMap.put(usedDate, 1);
			}
		}
		return new Long(hashMap.size());
	}
	
	private Date formatDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}

}
