/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.invoices.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_Ds;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.geo.ICountryService;
import atraxo.fmbas.business.api.vat.IVatService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class OutgoingInvoice_DsService extends AbstractEntityDsService<OutgoingInvoice_Ds, OutgoingInvoice_Ds, Object, OutgoingInvoice>
		implements IDsService<OutgoingInvoice_Ds, OutgoingInvoice_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(OutgoingInvoice_DsService.class);

	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntityService;
	@Autowired
	private IWorkflowBpmManager workflowBpmManager;

	@Override
	protected void postFind(IQueryBuilder<OutgoingInvoice_Ds, OutgoingInvoice_Ds, Object> builder, List<OutgoingInvoice_Ds> result) throws Exception {
		super.postFind(builder, result);
		Date now = GregorianCalendar.getInstance().getTime();
		for (OutgoingInvoice_Ds ds : result) {
			this.calculateDueDay(ds);
			this.setVat(ds);
			this.markInvoice(ds, now);

			ds.setCanBeCompleted(false);
			if (ds.getApprovalStatus().equals(BidApprovalStatus._AWAITING_APPROVAL_)) {
				try {
					WorkflowInstanceEntity wie = this.workflowInstanceEntityService.findByBusinessKey(
							WorkflowNames.CREDIT_MEMO_APPROVAL.getWorkflowName(), ds.getId(), ds._getEntity_().getClass().getSimpleName());
					ds.setCanBeCompleted(this.workflowBpmManager.canComplete(wie.getWorkflowInstance().getId()));
				} catch (Exception e) {
					// do nothing
					LOG.warn("Could not retrieve a WorkflowInstanceEntity for workflow CREDIT_MEMO_APPROVAL, will do nothing !", e);
				}
			}
		}
	}

	private void markInvoice(OutgoingInvoice_Ds ds, Date now) {
		Date anHourAgo = DateUtils.addHours(now, -1);
		if (ds.getCreatedAt().after(anHourAgo)) {
			ds.setMarked(true);
		} else {
			ds.setMarked(false);
		}
	}

	private void setVat(OutgoingInvoice_Ds ds) throws Exception {
		if (ds.getCountryId() == null) {
			return;
		}
		IVatService service = (IVatService) this.findEntityService(Vat.class);
		ICountryService countryservice = (ICountryService) this.findEntityService(Country.class);
		Country country = countryservice.findById(ds.getCountryId());
		BigDecimal vatRate = service.getVat(ds.getInvoiceDate(), country);
		ds.setVatRate(vatRate);
	}

	private void calculateDueDay(OutgoingInvoice_Ds ds) {
		Date limit = ds.getBaseLineDate();
		if (limit == null) {
			ds.setDueDays(null);
			return;
		}
		Date today = new Date();
		Long dueDay = (limit.getTime() - today.getTime()) / (1000 * 60 * 60 * 24);
		ds.setDueDays(dueDay.intValue());
	}

	@Override
	protected void preDelete(List<Object> ids) throws Exception {
		super.preDelete(ids);
		IOutgoingInvoiceService outgoingInvoiceService = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		List<OutgoingInvoice> invoices = outgoingInvoiceService.findByIds(ids);
		for (OutgoingInvoice oi : invoices) {
			if (BillStatus._AWAITING_PAYMENT_.equals(oi.getStatus()) || BillStatus._PAID_.equals(oi.getStatus())
					|| BillStatus._CREDITED_.equals(oi.getStatus())) {
				throw new BusinessException(AccErrorCode.INVOICE_LINE_DELETE_STATUS_NOT_DRAFT,
						AccErrorCode.INVOICE_LINE_DELETE_STATUS_NOT_DRAFT.getErrMsg());
			}
		}
	}

	@Override
	protected void preUpdateAfterEntity(OutgoingInvoice_Ds ds, OutgoingInvoice e, Object params) throws Exception {
		super.preUpdateAfterEntity(ds, e, params);
		OutgoingInvoice invoice = this.getEntityService().findById(e.getId());
		if (invoice != null && !ds.getBaseLineDate().equals(invoice.getBaseLineDate())) {
			e.setHistoryFlag(true);
		}
	}
}
