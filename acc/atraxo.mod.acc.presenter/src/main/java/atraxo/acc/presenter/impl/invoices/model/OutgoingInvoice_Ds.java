/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoices.model;

import atraxo.acc.domain.impl.acc_type.InvoiceCategory;
import atraxo.acc.domain.impl.acc_type.InvoiceExportStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceFormAcc;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.acc_type.InvoiceTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = OutgoingInvoice.class, sort = {@SortField(field = OutgoingInvoice_Ds.F_CREATEDAT, desc = true)})
@RefLookups({
		@RefLookup(refId = OutgoingInvoice_Ds.F_RECEIVERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoice_Ds.F_RECEIVERCODE)}),
		@RefLookup(refId = OutgoingInvoice_Ds.F_DELLOCID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoice_Ds.F_DELLOCCODE)}),
		@RefLookup(refId = OutgoingInvoice_Ds.F_COUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoice_Ds.F_COUNTRYCODE)}),
		@RefLookup(refId = OutgoingInvoice_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoice_Ds.F_UNITCODE)}),
		@RefLookup(refId = OutgoingInvoice_Ds.F_CURRID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoice_Ds.F_CURRCODE)}),
		@RefLookup(refId = OutgoingInvoice_Ds.F_ISSUERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoice_Ds.F_ISSUERCODE)})})
public class OutgoingInvoice_Ds
		extends
			AbstractSubsidiaryDs_Ds<OutgoingInvoice> {

	public static final String ALIAS = "acc_OutgoingInvoice_Ds";

	public static final String F_RECEIVERID = "receiverId";
	public static final String F_RECEIVERCODE = "receiverCode";
	public static final String F_RECEIVERNAME = "receiverName";
	public static final String F_DELLOCID = "delLocId";
	public static final String F_DELLOCCODE = "delLocCode";
	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYCODE = "countryCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CURRID = "currId";
	public static final String F_CURRCODE = "currCode";
	public static final String F_ISSUERID = "issuerId";
	public static final String F_ISSUERCODE = "issuerCode";
	public static final String F_INVOICETYPE = "invoiceType";
	public static final String F_INVOICENO = "invoiceNo";
	public static final String F_INVOICEDATE = "invoiceDate";
	public static final String F_ISSUEDATE = "issueDate";
	public static final String F_DELIVERYDATEFROM = "deliveryDateFrom";
	public static final String F_DELIVERYDATETO = "deliveryDateTo";
	public static final String F_TRANSACTIONTYPE = "transactionType";
	public static final String F_INVOICEFORM = "invoiceForm";
	public static final String F_CATEGORY = "category";
	public static final String F_REFERENCEDOCTYPE = "referenceDocType";
	public static final String F_REFERENCEDOCNO = "referenceDocNo";
	public static final String F_REFERENCEDOCID = "referenceDocId";
	public static final String F_TOTALAMOUNT = "totalAmount";
	public static final String F_VATAMOUNT = "vatAmount";
	public static final String F_VATTAXABLEAMOUNT = "vatTaxableAmount";
	public static final String F_QUANTITY = "quantity";
	public static final String F_BASELINEDATE = "baseLineDate";
	public static final String F_CLOSEDATE = "closeDate";
	public static final String F_ITEMSNUMBER = "itemsNumber";
	public static final String F_PAYMENTDATE = "paymentDate";
	public static final String F_TAXTYPE = "taxType";
	public static final String F_STATUS = "status";
	public static final String F_APPROVALSTATUS = "approvalStatus";
	public static final String F_PRODUCT = "product";
	public static final String F_CREDITMEMOREASON = "creditMemoReason";
	public static final String F_TRANSMISSIONSTATUS = "transmissionStatus";
	public static final String F_EXPORTSTATUS = "exportStatus";
	public static final String F_EXPORTDATE = "exportDate";
	public static final String F_CONTROLNO = "controlNo";
	public static final String F_INVOICEREFID = "invoiceRefId";
	public static final String F_INVOICEREFNO = "invoiceRefNo";
	public static final String F_DUMMYFIELD = "dummyField";
	public static final String F_DUEDAYS = "dueDays";
	public static final String F_VATRATE = "vatRate";
	public static final String F_PROCENT = "procent";
	public static final String F_TYPE = "type";
	public static final String F_SCOPE = "scope";
	public static final String F_MARKED = "marked";
	public static final String F_DELIVERYTYPE = "deliveryType";
	public static final String F_DELIVERYID = "deliveryId";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_CANBECOMPLETED = "canBeCompleted";

	@DsField(join = "left", path = "receiver.id")
	private Integer receiverId;

	@DsField(join = "left", path = "receiver.code")
	private String receiverCode;

	@DsField(join = "left", path = "receiver.name")
	private String receiverName;

	@DsField(join = "left", path = "deliveryLoc.id")
	private Integer delLocId;

	@DsField(join = "left", path = "deliveryLoc.code")
	private String delLocCode;

	@DsField(join = "left", path = "country.id")
	private Integer countryId;

	@DsField(join = "left", path = "country.code")
	private String countryCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "currency.id")
	private Integer currId;

	@DsField(join = "left", path = "currency.code")
	private String currCode;

	@DsField(join = "left", path = "issuer.id")
	private Integer issuerId;

	@DsField(join = "left", path = "issuer.code")
	private String issuerCode;

	@DsField
	private InvoiceTypeAcc invoiceType;

	@DsField
	private String invoiceNo;

	@DsField
	private Date invoiceDate;

	@DsField
	private Date issueDate;

	@DsField
	private Date deliveryDateFrom;

	@DsField(path = "deliverydateTo")
	private Date deliveryDateTo;

	@DsField
	private TransactionType transactionType;

	@DsField
	private InvoiceFormAcc invoiceForm;

	@DsField
	private InvoiceCategory category;

	@DsField
	private InvoiceReferenceDocType referenceDocType;

	@DsField
	private String referenceDocNo;

	@DsField
	private Integer referenceDocId;

	@DsField
	private BigDecimal totalAmount;

	@DsField
	private BigDecimal vatAmount;

	@DsField(path = "netAmount")
	private BigDecimal vatTaxableAmount;

	@DsField
	private BigDecimal quantity;

	@DsField
	private Date baseLineDate;

	@DsField
	private Date closeDate;

	@DsField
	private Integer itemsNumber;

	@DsField
	private Date paymentDate;

	@DsField
	private TaxType taxType;

	@DsField
	private BillStatus status;

	@DsField
	private BidApprovalStatus approvalStatus;

	@DsField
	private Product product;

	@DsField
	private RevocationReason creditMemoReason;

	@DsField
	private InvoiceTransmissionStatus transmissionStatus;

	@DsField
	private InvoiceExportStatus exportStatus;

	@DsField
	private Date exportDate;

	@DsField
	private String controlNo;

	@DsField(join = "left", path = "invoiceReference.id")
	private Integer invoiceRefId;

	@DsField(join = "left", path = "invoiceReference.invoiceNo")
	private String invoiceRefNo;

	@DsField(fetch = false)
	private String dummyField;

	@DsField(fetch = false)
	private Integer dueDays;

	@DsField(fetch = false)
	private BigDecimal vatRate;

	@DsField(fetch = false)
	private String procent;

	@DsField(fetch = false)
	private String type;

	@DsField(fetch = false)
	private String scope;

	@DsField(fetch = false)
	private Boolean marked;

	@DsField(fetch = false)
	private String deliveryType;

	@DsField(fetch = false)
	private Integer deliveryId;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private Boolean canBeCompleted;

	/**
	 * Default constructor
	 */
	public OutgoingInvoice_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public OutgoingInvoice_Ds(OutgoingInvoice e) {
		super(e);
	}

	public Integer getReceiverId() {
		return this.receiverId;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	public String getReceiverCode() {
		return this.receiverCode;
	}

	public void setReceiverCode(String receiverCode) {
		this.receiverCode = receiverCode;
	}

	public String getReceiverName() {
		return this.receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public Integer getDelLocId() {
		return this.delLocId;
	}

	public void setDelLocId(Integer delLocId) {
		this.delLocId = delLocId;
	}

	public String getDelLocCode() {
		return this.delLocCode;
	}

	public void setDelLocCode(String delLocCode) {
		this.delLocCode = delLocCode;
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getCurrId() {
		return this.currId;
	}

	public void setCurrId(Integer currId) {
		this.currId = currId;
	}

	public String getCurrCode() {
		return this.currCode;
	}

	public void setCurrCode(String currCode) {
		this.currCode = currCode;
	}

	public Integer getIssuerId() {
		return this.issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerCode() {
		return this.issuerCode;
	}

	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	public InvoiceTypeAcc getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceTypeAcc invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Date getIssueDate() {
		return this.issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getDeliveryDateFrom() {
		return this.deliveryDateFrom;
	}

	public void setDeliveryDateFrom(Date deliveryDateFrom) {
		this.deliveryDateFrom = deliveryDateFrom;
	}

	public Date getDeliveryDateTo() {
		return this.deliveryDateTo;
	}

	public void setDeliveryDateTo(Date deliveryDateTo) {
		this.deliveryDateTo = deliveryDateTo;
	}

	public TransactionType getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public InvoiceFormAcc getInvoiceForm() {
		return this.invoiceForm;
	}

	public void setInvoiceForm(InvoiceFormAcc invoiceForm) {
		this.invoiceForm = invoiceForm;
	}

	public InvoiceCategory getCategory() {
		return this.category;
	}

	public void setCategory(InvoiceCategory category) {
		this.category = category;
	}

	public InvoiceReferenceDocType getReferenceDocType() {
		return this.referenceDocType;
	}

	public void setReferenceDocType(InvoiceReferenceDocType referenceDocType) {
		this.referenceDocType = referenceDocType;
	}

	public String getReferenceDocNo() {
		return this.referenceDocNo;
	}

	public void setReferenceDocNo(String referenceDocNo) {
		this.referenceDocNo = referenceDocNo;
	}

	public Integer getReferenceDocId() {
		return this.referenceDocId;
	}

	public void setReferenceDocId(Integer referenceDocId) {
		this.referenceDocId = referenceDocId;
	}

	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getVatTaxableAmount() {
		return this.vatTaxableAmount;
	}

	public void setVatTaxableAmount(BigDecimal vatTaxableAmount) {
		this.vatTaxableAmount = vatTaxableAmount;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Date getBaseLineDate() {
		return this.baseLineDate;
	}

	public void setBaseLineDate(Date baseLineDate) {
		this.baseLineDate = baseLineDate;
	}

	public Date getCloseDate() {
		return this.closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public Integer getItemsNumber() {
		return this.itemsNumber;
	}

	public void setItemsNumber(Integer itemsNumber) {
		this.itemsNumber = itemsNumber;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public TaxType getTaxType() {
		return this.taxType;
	}

	public void setTaxType(TaxType taxType) {
		this.taxType = taxType;
	}

	public BillStatus getStatus() {
		return this.status;
	}

	public void setStatus(BillStatus status) {
		this.status = status;
	}

	public BidApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(BidApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public RevocationReason getCreditMemoReason() {
		return this.creditMemoReason;
	}

	public void setCreditMemoReason(RevocationReason creditMemoReason) {
		this.creditMemoReason = creditMemoReason;
	}

	public InvoiceTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			InvoiceTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public InvoiceExportStatus getExportStatus() {
		return this.exportStatus;
	}

	public void setExportStatus(InvoiceExportStatus exportStatus) {
		this.exportStatus = exportStatus;
	}

	public Date getExportDate() {
		return this.exportDate;
	}

	public void setExportDate(Date exportDate) {
		this.exportDate = exportDate;
	}

	public String getControlNo() {
		return this.controlNo;
	}

	public void setControlNo(String controlNo) {
		this.controlNo = controlNo;
	}

	public Integer getInvoiceRefId() {
		return this.invoiceRefId;
	}

	public void setInvoiceRefId(Integer invoiceRefId) {
		this.invoiceRefId = invoiceRefId;
	}

	public String getInvoiceRefNo() {
		return this.invoiceRefNo;
	}

	public void setInvoiceRefNo(String invoiceRefNo) {
		this.invoiceRefNo = invoiceRefNo;
	}

	public String getDummyField() {
		return this.dummyField;
	}

	public void setDummyField(String dummyField) {
		this.dummyField = dummyField;
	}

	public Integer getDueDays() {
		return this.dueDays;
	}

	public void setDueDays(Integer dueDays) {
		this.dueDays = dueDays;
	}

	public BigDecimal getVatRate() {
		return this.vatRate;
	}

	public void setVatRate(BigDecimal vatRate) {
		this.vatRate = vatRate;
	}

	public String getProcent() {
		return this.procent;
	}

	public void setProcent(String procent) {
		this.procent = procent;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public Boolean getMarked() {
		return this.marked;
	}

	public void setMarked(Boolean marked) {
		this.marked = marked;
	}

	public String getDeliveryType() {
		return this.deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public Integer getDeliveryId() {
		return this.deliveryId;
	}

	public void setDeliveryId(Integer deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public Boolean getCanBeCompleted() {
		return this.canBeCompleted;
	}

	public void setCanBeCompleted(Boolean canBeCompleted) {
		this.canBeCompleted = canBeCompleted;
	}
}
