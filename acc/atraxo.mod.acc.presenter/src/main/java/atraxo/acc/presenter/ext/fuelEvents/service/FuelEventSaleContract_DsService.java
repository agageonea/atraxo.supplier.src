/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.fuelEvents.service;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.presenter.impl.fuelEvents.model.FuelEventSaleContract_Ds;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelEventSaleContract_DsService extends AbstractEntityDsService<FuelEventSaleContract_Ds, FuelEventSaleContract_Ds, Object, FuelEvent>
		implements IDsService<FuelEventSaleContract_Ds, FuelEventSaleContract_Ds, Object> {

	@Autowired
	private IContractService contractService;
	@Autowired
	private ISystemParameterService paramSrv;
	@Autowired
	private IExchangeRateService exchSrv;
	@Autowired
	private ICurrenciesService curSrv;

	@Override
	protected void postFind(IQueryBuilder<FuelEventSaleContract_Ds, FuelEventSaleContract_Ds, Object> builder, List<FuelEventSaleContract_Ds> result)
			throws Exception {
		super.postFind(builder, result);
		String sysCurrencyCode = this.paramSrv.getSysCurrency();
		Currencies toCurrency = this.curSrv.findByCode(sysCurrencyCode);
		Map<Integer, Contract> contractMap = new HashMap<>();
		for (FuelEventSaleContract_Ds ds : result) {
			if (ds.getSourceType().equalsIgnoreCase(Contract.class.getSimpleName()) && ds.getContractId() != null) {
				Contract contract;
				if (contractMap.containsKey(ds.getContractId())) {
					contract = contractMap.get(ds.getContractId());
				} else {
					contract = this.contractService.findById(ds.getContractId());
					contractMap.put(ds.getContractId(), contract);
				}
				ds.setContractCode(contract.getCode());
				ds.setType(contract.getType().getName());
				ds.setDelivery(contract.getSubType().getName());
				ds.setScope(contract.getScope().getName());
				ds.setTermSpot(contract.getIsSpot().getName());
				this.calculateOriginalAmount(ds, contract, toCurrency);
			}
		}
	}

	private void calculateOriginalAmount(FuelEventSaleContract_Ds ds, Contract contract, Currencies toCurrency) throws BusinessException {
		BigDecimal cost = ds.getBillableCost();
		BigDecimal sysCost = this.exchSrv
				.convertExchangeRate(contract.getSettlementCurr(), toCurrency, GregorianCalendar.getInstance().getTime(), cost, false).getValue();

		ds.setConvertedAmount(sysCost);
		ds.setSystemCurrency(toCurrency.getCode());
	}
}
