/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoices.model;

import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = OutgoingInvoice.class, sort = {@SortField(field = OutgoingInvoiceMemo_Ds.F_INVOICEDATE, desc = true)})
@RefLookups({
		@RefLookup(refId = OutgoingInvoiceMemo_Ds.F_RECEIVERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoiceMemo_Ds.F_RECEIVERCODE)}),
		@RefLookup(refId = OutgoingInvoiceMemo_Ds.F_DELLOCID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoiceMemo_Ds.F_DELLOCCODE)})})
public class OutgoingInvoiceMemo_Ds
		extends
			AbstractSubsidiaryDs_Ds<OutgoingInvoice> {

	public static final String ALIAS = "acc_OutgoingInvoiceMemo_Ds";

	public static final String F_RECEIVERID = "receiverId";
	public static final String F_RECEIVERCODE = "receiverCode";
	public static final String F_DELLOCID = "delLocId";
	public static final String F_DELLOCCODE = "delLocCode";
	public static final String F_INVOICENO = "invoiceNo";
	public static final String F_INVOICEDATE = "invoiceDate";
	public static final String F_BASELINEDATE = "baseLineDate";
	public static final String F_STATUS = "status";
	public static final String F_REFERENCEINVOICEID = "referenceInvoiceId";
	public static final String F_REFERENCEINVOICENUMBER = "referenceInvoiceNumber";

	@DsField(join = "left", path = "receiver.id")
	private Integer receiverId;

	@DsField(join = "left", path = "receiver.code")
	private String receiverCode;

	@DsField(join = "left", path = "deliveryLoc.id")
	private Integer delLocId;

	@DsField(join = "left", path = "deliveryLoc.code")
	private String delLocCode;

	@DsField
	private String invoiceNo;

	@DsField
	private Date invoiceDate;

	@DsField
	private Date baseLineDate;

	@DsField
	private BillStatus status;

	@DsField(join = "left", path = "invoiceReference.id")
	private Integer referenceInvoiceId;

	@DsField(join = "left", path = "invoiceReference.invoiceNo")
	private String referenceInvoiceNumber;

	/**
	 * Default constructor
	 */
	public OutgoingInvoiceMemo_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public OutgoingInvoiceMemo_Ds(OutgoingInvoice e) {
		super(e);
	}

	public Integer getReceiverId() {
		return this.receiverId;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	public String getReceiverCode() {
		return this.receiverCode;
	}

	public void setReceiverCode(String receiverCode) {
		this.receiverCode = receiverCode;
	}

	public Integer getDelLocId() {
		return this.delLocId;
	}

	public void setDelLocId(Integer delLocId) {
		this.delLocId = delLocId;
	}

	public String getDelLocCode() {
		return this.delLocCode;
	}

	public void setDelLocCode(String delLocCode) {
		this.delLocCode = delLocCode;
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Date getBaseLineDate() {
		return this.baseLineDate;
	}

	public void setBaseLineDate(Date baseLineDate) {
		this.baseLineDate = baseLineDate;
	}

	public BillStatus getStatus() {
		return this.status;
	}

	public void setStatus(BillStatus status) {
		this.status = status;
	}

	public Integer getReferenceInvoiceId() {
		return this.referenceInvoiceId;
	}

	public void setReferenceInvoiceId(Integer referenceInvoiceId) {
		this.referenceInvoiceId = referenceInvoiceId;
	}

	public String getReferenceInvoiceNumber() {
		return this.referenceInvoiceNumber;
	}

	public void setReferenceInvoiceNumber(String referenceInvoiceNumber) {
		this.referenceInvoiceNumber = referenceInvoiceNumber;
	}
}
