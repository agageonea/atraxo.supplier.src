/**
 *
 */
package atraxo.acc.presenter.ext.invoices.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_Ds;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_DsParam;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.util.StringUtil;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * @author zspeter
 */
public class ApproveCreditMemoService extends AbstractCreditMemoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApproveCreditMemoService.class);

	private static final String DATE_FORMAT = "dd.MM.yyyy";

	@Autowired
	private ISystemParameterService paramSrv;
	@Autowired
	private IWorkflowBpmManager workflowBpmManager;
	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;
	@Autowired
	private IUserSuppService userService;

	public void approve(OutgoingInvoice_Ds ds, OutgoingInvoice_DsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START approve()");
		}
		if (Calendar.getInstance().getTime().before(ds.getInvoiceDate())) {
			throw new BusinessException(AccErrorCode.INVOICE_DATE_AFTER_TODAY, AccErrorCode.INVOICE_DATE_AFTER_TODAY.getErrMsg());
		}
		this.completeCurrentTask(ds.getId(), true, params.getApprovalNote());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END approve()");
		}
	}

	public void approveList(List<OutgoingInvoice_Ds> dsList, OutgoingInvoice_DsParam params) throws BusinessException {
		boolean useWorkflow = this.paramSrv.getCreditMemoApprovalWorkflow();
		if (!useWorkflow) {
			params.setApproveRejectResult(AccErrorCode.CREDIT_MEMO_APPROVAL_WORKFLOW_NO_SYS_PARAM_APPROVE_REJECT.getErrMsg());
			return;
		}

		List<String> failedToProcessInvoices = new ArrayList<>();
		for (OutgoingInvoice_Ds oiDs : dsList) {
			try {
				this.approve(oiDs, params);
			} catch (Exception e) {
				failedToProcessInvoices.add(oiDs.getInvoiceNo() + ":" + e.getMessage());
				LOGGER.error("Error", e);
			}
		}
		params.setApproveRejectResult(this.generateProcessingResultResponse(failedToProcessInvoices, dsList.size()));
	}

	private String generateProcessingResultResponse(List<String> failedToProcessInvoices, int totalProcessingAttempts) {
		int successfulProcesings = totalProcessingAttempts - failedToProcessInvoices.size();
		StringBuilder resultMessageBuilder = new StringBuilder();
		resultMessageBuilder
				.append(String.format(AccErrorCode.CREDIT_MEMO_APPROVE_TOTAL_PROCESSING.getErrMsg(), successfulProcesings, totalProcessingAttempts));
		if (!failedToProcessInvoices.isEmpty()) {
			resultMessageBuilder.append(AccErrorCode.CREDIT_MEMO_APPROVE_PARTIAL_PROCESSING.getErrMsg());
			resultMessageBuilder.append(UL_START);
			for (String failedInvoice : failedToProcessInvoices) {
				resultMessageBuilder.append(LI_START).append(failedInvoice).append(LI_END);
			}
			resultMessageBuilder.append(UL_END);
		}

		return resultMessageBuilder.toString();
	}

	/**
	 * @param dsList
	 * @param params
	 * @throws Exception
	 */
	public void submitForApproval(List<OutgoingInvoice_Ds> dsList, OutgoingInvoice_DsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApproval()");
		}
		StringBuilder validationMessages = new StringBuilder();

		// first, check the 'Use Credit Memo Approval Workflow' system parameter
		boolean useWorkflow = this.paramSrv.getCreditMemoApprovalWorkflow();
		if (!useWorkflow) {
			params.setSubmitForApprovalDescription(AccErrorCode.CREDIT_MEMO_APPROVAL_WORKFLOW_NO_SYS_PARAM.getErrMsg());
			params.setSubmitForApprovalResult(false);
			return;
		}

		// if ok, proceed forward

		List<String> dbWorkflowValidation = new ArrayList<>();

		int submittedOk = 0;
		int notFound = 0;
		for (OutgoingInvoice_Ds ds : dsList) {
			// Change Approval Status (if the case)
			OutgoingInvoice outInvoice = null;
			try {
				outInvoice = this.outgoingInvoiceService.findByBusiness(ds.getId());
			} catch (Exception e) {
				LOGGER.warn("WARNING:could not locate OutgoingInvoice for ID " + ds.getId(), e);
				notFound++;
			}
			if (outInvoice != null) {

				boolean canSubmitForApproval = this.canSubmitForApproval(outInvoice);

				if (canSubmitForApproval) {

					// submit for approval for the entity(change status, fill in History)
					this.outgoingInvoiceService.submitForApproval(outInvoice, BidApprovalStatus._AWAITING_APPROVAL_,
							WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_OK, params.getApprovalNote());

					// start workflow after adding workflow parameters
					Map<String, Object> vars = this.getWorkflowVariablesMap(params, outInvoice);
					WorkflowStartResult result = this.workflowBpmManager.startWorkflow(WorkflowNames.CREDIT_MEMO_APPROVAL.getWorkflowName(), vars,
							outInvoice.getSubsidiaryId(), outInvoice);
					if (result.isStarted()) {
						submittedOk++;
					} else {
						dbWorkflowValidation.add(this.getCreditMemoIdentificationMessage(ds, result.getReason()));
						// submit for approval for the entity(change status, fill in History)
						this.outgoingInvoiceService.submitForApproval(this.outgoingInvoiceService.findById(outInvoice.getId()),
								BidApprovalStatus._REJECTED_, WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_NOT_OK, result.getReason());
					}

				}
			}

		}

		if (!dbWorkflowValidation.isEmpty()) {
			validationMessages.append(
					String.format(AccErrorCode.CM_WKF_VALIDATION_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbWorkflowValidation.toArray())));
		}

		// fill in the result
		boolean submitOk = dsList.size() == submittedOk;
		String s1 = String.format(AccErrorCode.CREDIT_MEMO_APPROVAL_WORKFLOW_RESULT_SUBMIT.getErrMsg(), Integer.toString(submittedOk),
				Integer.toString(dsList.size()));
		String s2 = AccErrorCode.CREDIT_MEMO_APPROVAL_WORKFLOW_RESULT_CHECK.getErrMsg();
		String s3 = String.format(AccErrorCode.CREDIT_MEMO_APPROVAL_WORKFLOW_RESULT_NOT_FOUND.getErrMsg(), notFound);
		params.setSubmitForApprovalDescription(s1 + (submitOk ? "" : s2) + (notFound == 0 ? "" : s3) + "<br/><br/>" + validationMessages.toString());
		params.setSubmitForApprovalResult(submitOk);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApproval()");
		}
	}

	/**
	 * Re-check the conditions that allows a user to submit for approval a credit memo
	 *
	 * @param outInvoice
	 * @return
	 */
	private boolean canSubmitForApproval(OutgoingInvoice outInvoice) {
		boolean canSubmit = false;
		if (outInvoice.getInvoiceType().equals(InvoiceTypeAcc._CRN_) && outInvoice.getStatus().equals(BillStatus._AWAITING_APPROVAL_)
				&& (outInvoice.getApprovalStatus().equals(BidApprovalStatus._NEW_)
						|| outInvoice.getApprovalStatus().equals(BidApprovalStatus._REJECTED_))) {
			canSubmit = true;
		}
		return canSubmit;
	}

	/**
	 * @param params
	 * @param outInvoiceID
	 * @return
	 */
	private Map<String, Object> getWorkflowVariablesMap(OutgoingInvoice_DsParam params, OutgoingInvoice outInvoice) {
		Map<String, Object> vars = new HashMap<>();
		vars.put(WorkflowVariablesConstants.VAR_WKF_ENTITY_DESCRIPTION, "Credit memo approval request - " + outInvoice.getInvoiceNo() + " / "
				+ new SimpleDateFormat(DATE_FORMAT).format(outInvoice.getInvoiceDate()));
		vars.put(WorkflowVariablesConstants.INVOICE_ID_VAR, outInvoice.getId().toString());
		vars.put(WorkflowVariablesConstants.APPROVE_NOTE, params.getApprovalNote());
		vars.put(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR, this.getFullNameOfRequester());
		String attachmentsArray = params.getSelectedAttachments();

		if (!StringUtils.isEmpty(attachmentsArray)) {
			String attachTrimmed = attachmentsArray.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").trim();
			if (!StringUtils.isEmpty(attachTrimmed)) {
				String[] attachmentIdentifiers = attachTrimmed.split(",");
				vars.put(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS, Arrays.asList(attachmentIdentifiers));
			}
		}
		return vars;
	}

	/**
	 * @return
	 */
	private String getFullNameOfRequester() {

		UserSupp user = this.userService.findByLogin(Session.user.get().getLoginName());
		return user.getFirstName() + " " + user.getLastName();
	}

	private String getCreditMemoIdentificationMessage(OutgoingInvoice_Ds ds, String reason) {
		StringBuilder bidIdentification = new StringBuilder();
		bidIdentification.append("Invoice number : ").append(ds.getInvoiceNo());
		bidIdentification.append("<br/>");
		bidIdentification.append("<b>Error:</b> ").append(reason);
		return bidIdentification.toString();
	}

}
