/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.invoices.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.ext.invoices.InvoiceCategory;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.presenter.impl.invoices.model.Invoice_Ds;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.geo.ICountryService;
import atraxo.fmbas.business.api.vat.IVatService;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.vat.Vat;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class Invoice_DsService extends AbstractEntityDsService<Invoice_Ds, Invoice_Ds, Object, Invoice>
		implements IDsService<Invoice_Ds, Invoice_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(Invoice_DsService.class);

	@Override
	protected void preInsert(Invoice_Ds ds, Invoice e, Object params) throws Exception {
		super.preInsert(ds, e, params);
		try {
			this.setContract(e);
		} catch (BusinessException | RuntimeException e2) {
			LOG.warn("Set contract to invoice " + e.getInvoiceNo() + " " + e2.getMessage(), e2);
		}
	}

	@Override
	protected void postFind(IQueryBuilder<Invoice_Ds, Invoice_Ds, Object> builder, List<Invoice_Ds> result) throws Exception {
		super.postFind(builder, result);
		for (Invoice_Ds ds : result) {
			if (ds.getContractId() != null) {
				this.calculateDueDay(ds);
			}
			this.setVat(ds);
			this.setTypeScope(ds);
			Integer invoiceLinenUmber = ds._getEntity_() != null && ds._getEntity_().getInvoiceLines() != null
					? ds._getEntity_().getInvoiceLines().size() : 0;
			ds.setItemsNumber(invoiceLinenUmber);
		}
	}

	@Override
	protected void postInsert(List<Invoice_Ds> list, Object params) throws Exception {
		super.postInsert(list, params);
		for (Invoice_Ds ds : list) {
			this.setTypeScope(ds);
		}
	}

	private void setVat(Invoice_Ds ds) throws Exception {
		if (ds.getCountryId() == null) {
			return;
		}
		IVatService service = (IVatService) this.findEntityService(Vat.class);
		ICountryService countryservice = (ICountryService) this.findEntityService(Country.class);
		Country country = countryservice.findById(ds.getCountryId());
		BigDecimal vatRate = service.getVat(ds.getIssueDate(), country);
		ds.setVatRate(vatRate);
	}

	@Override
	protected void preDelete(List<Object> ids) throws Exception {
		Iterator<Object> iter = ids.iterator();
		boolean flag = false;
		while (iter.hasNext()) {
			Object id = iter.next();
			if (this.getEntityService().findById(id) == null) {
				flag = true;
			}
		}
		super.preDelete(ids);
		if (flag) {
			throw new BusinessException(AccErrorCode.INVOICE_DELETE_RPC_ERROR, AccErrorCode.INVOICE_DELETE_RPC_ERROR.getErrMsg());
		}
	}

	private void calculateDueDay(Invoice_Ds ds) {
		Date limit = ds.getBaseLineDate();
		if (limit == null) {
			ds.setDueDays(null);
			return;
		}
		Date today = new Date();
		Long dueDay = (limit.getTime() - today.getTime()) / (1000 * 60 * 60 * 24);
		ds.setDueDays(dueDay.intValue());
	}

	private void setTypeScope(Invoice_Ds ds) {
		String invoiceCategory = ds.getCategory().getName();
		if (invoiceCategory == null) {
			return;
		}
		InvoiceCategory invCategory = InvoiceCategory.getByName(invoiceCategory);
		ds.setScope(invCategory.getScope());
		ds.setType(invCategory.getType());
	}

	/**
	 * If there is only one contract this one is automatically pre-selected
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void setContract(Invoice e) throws Exception {
		if (e.getContract() != null) {
			return;
		}
		IContractService cService = (IContractService) this.findEntityService(Contract.class);
		InvoiceCategory invoiceCategory = InvoiceCategory.getByName(e.getCategory().getName());
		List<Contract> contracts = cService.findBySupplierStatusLocationScopeType(e.getIssuer(), ContractStatus._EFFECTIVE_, e.getDeliveryLoc(),
				ContractScope.getByName(invoiceCategory.getScope()), ContractType.getByName(invoiceCategory.getType()));
		if (contracts.size() == 1 && this.checkPeriod(e, contracts.get(0))) {
			e.setContract(contracts.get(0));
		}
	}

	private boolean checkPeriod(Invoice inv, Contract contract) {
		return contract.getValidFrom().compareTo(inv.getDeliveryDateFrom()) <= 0 && contract.getValidTo().compareTo(inv.getDeliverydateTo()) >= 0;
	}
}
