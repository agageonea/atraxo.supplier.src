package atraxo.acc.presenter.ext.invoices.delegate;

import java.util.List;

import atraxo.acc.presenter.ext.invoices.service.ApproveCreditMemoService;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_Ds;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class ApproveCreditMemo_Pd extends AbstractPresenterDelegate {

	public void approve(OutgoingInvoice_Ds ds, OutgoingInvoice_DsParam params) throws Exception {
		ApproveCreditMemoService service = this.getApplicationContext().getBean(ApproveCreditMemoService.class);
		service.approve(ds, params);
	}

	public void approveList(List<OutgoingInvoice_Ds> dsList, OutgoingInvoice_DsParam params) throws Exception {
		ApproveCreditMemoService service = this.getApplicationContext().getBean(ApproveCreditMemoService.class);
		service.approveList(dsList, params);
	}

	/**
	 * @param dsList
	 * @param params
	 * @throws Exception
	 */
	public void submitForApproval(List<OutgoingInvoice_Ds> dsList, OutgoingInvoice_DsParam params) throws Exception {
		ApproveCreditMemoService service = this.getApplicationContext().getBean(ApproveCreditMemoService.class);
		service.submitForApproval(dsList, params);
	}

}
