/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.acc.presenter.ext.outgoingInvoiceLineDetails.qb;

import atraxo.acc.presenter.impl.outgoingInvoiceLineDetails.model.OutgoingInvoiceLineDetails_Ds;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder OutgoingInvoiceLineDetails_DsQb
 */
public class OutgoingInvoiceLineDetails_DsQb extends QueryBuilderWithJpql<OutgoingInvoiceLineDetails_Ds, Object, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		this.addFilterCondition("e.calculateIndicator = :calculateIndicator");
		this.addCustomFilterItem("calculateIndicator", CalculateIndicator._INCLUDED_);
	}
}
