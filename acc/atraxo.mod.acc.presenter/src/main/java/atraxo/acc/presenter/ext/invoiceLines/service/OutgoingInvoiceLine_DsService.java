/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.invoiceLines.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.presenter.impl.invoiceLines.model.OutgoingInvoiceLine_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class OutgoingInvoiceLine_DsService
		extends AbstractEntityDsService<OutgoingInvoiceLine_Ds, OutgoingInvoiceLine_Ds, Object, OutgoingInvoiceLine>
		implements IDsService<OutgoingInvoiceLine_Ds, OutgoingInvoiceLine_Ds, Object> {

	@Override
	protected void postSummaries(IQueryBuilder<OutgoingInvoiceLine_Ds, OutgoingInvoiceLine_Ds, Object> builder,
			List<OutgoingInvoiceLine_Ds> findResult, Map<String, Object> sum) throws Exception {
		super.postSummaries(builder, findResult, sum);
		BigDecimal quantityTotal = BigDecimal.ZERO;
		BigDecimal amountTotal = BigDecimal.ZERO;
		BigDecimal vatAmountTotal = BigDecimal.ZERO;

		for (OutgoingInvoiceLine_Ds ds : findResult) {
			quantityTotal = quantityTotal.add(ds.getQuantity() == null ? BigDecimal.ZERO : ds.getQuantity());
			amountTotal = amountTotal.add(ds.getAmount() == null ? BigDecimal.ZERO : ds.getAmount());
			vatAmountTotal = vatAmountTotal.add(ds.getVat() == null ? BigDecimal.ZERO : ds.getVat());
		}
		sum.put("quantity", quantityTotal);
		sum.put("amount", amountTotal);
		sum.put("vat", vatAmountTotal);
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.presenter.service.ds.AbstractEntityDsReadService#postFind(seava.j4e.api.action.query.IQueryBuilder, java.util.List)
	 */
	@Override
	protected void postFind(IQueryBuilder<OutgoingInvoiceLine_Ds, OutgoingInvoiceLine_Ds, Object> builder, List<OutgoingInvoiceLine_Ds> result)
			throws Exception {
		super.postFind(builder, result);
		// set up the flight ID (IATA code (if existing)+flight number )
		for (OutgoingInvoiceLine_Ds ds : result) {
			ds.setFlightID((!StringUtils.isEmpty(ds.getIataCode()) ? ds.getIataCode() : "") + ds.getFlightNo());//$NON-NLS-1$
		}
	}

}
