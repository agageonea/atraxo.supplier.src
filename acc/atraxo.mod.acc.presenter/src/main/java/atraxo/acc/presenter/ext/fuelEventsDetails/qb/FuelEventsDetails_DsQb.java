/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.acc.presenter.ext.fuelEventsDetails.qb;

import atraxo.acc.presenter.impl.fuelEventsDetails.model.FuelEventsDetails_Ds;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

/**
 * Custom Query Builder FuelEventsDetails_DsQb
 */
public class FuelEventsDetails_DsQb extends QueryBuilderWithJpql<FuelEventsDetails_Ds, Object, Object> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		super.beforeBuildWhere();
		this.addFilterCondition("e.calculateIndicator = :calculateIndicator");
		this.addCustomFilterItem("calculateIndicator", CalculateIndicator._INCLUDED_);
	}
}
