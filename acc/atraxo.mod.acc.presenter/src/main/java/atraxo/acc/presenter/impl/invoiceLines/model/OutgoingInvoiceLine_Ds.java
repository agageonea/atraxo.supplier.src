/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoiceLines.model;

import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = OutgoingInvoiceLine.class)
@RefLookups({
		@RefLookup(refId = OutgoingInvoiceLine_Ds.F_FUELEVID),
		@RefLookup(refId = OutgoingInvoiceLine_Ds.F_OUTINVID),
		@RefLookup(refId = OutgoingInvoiceLine_Ds.F_REFINVID),
		@RefLookup(refId = OutgoingInvoiceLine_Ds.F_DESTID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoiceLine_Ds.F_DESTCODE)}),
		@RefLookup(refId = OutgoingInvoiceLine_Ds.F_CUSTID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingInvoiceLine_Ds.F_CUSTCODE)})})
public class OutgoingInvoiceLine_Ds extends AbstractDs_Ds<OutgoingInvoiceLine> {

	public static final String ALIAS = "acc_OutgoingInvoiceLine_Ds";

	public static final String F_FUELEVID = "fuelEvId";
	public static final String F_OUTINVID = "outInvId";
	public static final String F_OUTINVSTATUS = "outInvStatus";
	public static final String F_INVOICETYPE = "invoicetype";
	public static final String F_REFINVID = "refInvId";
	public static final String F_CUSTID = "custId";
	public static final String F_CUSTCODE = "custCode";
	public static final String F_IATACODE = "iataCode";
	public static final String F_DESTID = "destId";
	public static final String F_DESTCODE = "destCode";
	public static final String F_SHIPTOCODE = "shipToCode";
	public static final String F_SHIPTONAME = "shipToName";
	public static final String F_INVLINEID = "invLineId";
	public static final String F_DELIVERYDATE = "deliveryDate";
	public static final String F_AIRCRAFTREGISTRATIONNUMBER = "aircraftRegistrationNumber";
	public static final String F_FLIGHTSUFFIX = "flightSuffix";
	public static final String F_FLIGHTTYPE = "flightType";
	public static final String F_FLIGHTNO = "flightNo";
	public static final String F_TICKETNO = "ticketNo";
	public static final String F_QUANTITY = "quantity";
	public static final String F_AMOUNT = "amount";
	public static final String F_VAT = "vat";
	public static final String F_ISCREDITED = "isCredited";
	public static final String F_TRANSMISSIONSTATUS = "transmissionStatus";
	public static final String F_ERPREFERENCE = "erpReference";
	public static final String F_BOLNUMBER = "bolNumber";
	public static final String F_FLIGHTID = "flightID";
	public static final String F_LABELFIELD = "labelField";

	@DsField(join = "left", path = "fuelEvent.id")
	private Integer fuelEvId;

	@DsField(join = "left", path = "outgoingInvoice.id")
	private Integer outInvId;

	@DsField(join = "left", path = "outgoingInvoice.status")
	private BillStatus outInvStatus;

	@DsField(join = "left", path = "outgoingInvoice.invoiceType")
	private InvoiceTypeAcc invoicetype;

	@DsField(join = "left", path = "referenceInvItem.id")
	private Integer refInvId;

	@DsField(join = "left", path = "customer.id")
	private Integer custId;

	@DsField(join = "left", path = "customer.code")
	private String custCode;

	@DsField(join = "left", path = "customer.iataCode")
	private String iataCode;

	@DsField(join = "left", path = "destination.id")
	private Integer destId;

	@DsField(join = "left", path = "destination.code")
	private String destCode;

	@DsField(join = "left", path = "fuelEvent.shipTo.code")
	private String shipToCode;

	@DsField(join = "left", path = "fuelEvent.shipTo.name")
	private String shipToName;

	@DsField(path = "id")
	private Integer invLineId;

	@DsField
	private Date deliveryDate;

	@DsField
	private String aircraftRegistrationNumber;

	@DsField
	private Suffix flightSuffix;

	@DsField
	private FlightTypeIndicator flightType;

	@DsField
	private String flightNo;

	@DsField
	private String ticketNo;

	@DsField
	private BigDecimal quantity;

	@DsField
	private BigDecimal amount;

	@DsField
	private BigDecimal vat;

	@DsField
	private Boolean isCredited;

	@DsField
	private InvoiceLineTransmissionStatus transmissionStatus;

	@DsField
	private String erpReference;

	@DsField
	private String bolNumber;

	@DsField(fetch = false)
	private String flightID;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public OutgoingInvoiceLine_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public OutgoingInvoiceLine_Ds(OutgoingInvoiceLine e) {
		super(e);
	}

	public Integer getFuelEvId() {
		return this.fuelEvId;
	}

	public void setFuelEvId(Integer fuelEvId) {
		this.fuelEvId = fuelEvId;
	}

	public Integer getOutInvId() {
		return this.outInvId;
	}

	public void setOutInvId(Integer outInvId) {
		this.outInvId = outInvId;
	}

	public BillStatus getOutInvStatus() {
		return this.outInvStatus;
	}

	public void setOutInvStatus(BillStatus outInvStatus) {
		this.outInvStatus = outInvStatus;
	}

	public InvoiceTypeAcc getInvoicetype() {
		return this.invoicetype;
	}

	public void setInvoicetype(InvoiceTypeAcc invoicetype) {
		this.invoicetype = invoicetype;
	}

	public Integer getRefInvId() {
		return this.refInvId;
	}

	public void setRefInvId(Integer refInvId) {
		this.refInvId = refInvId;
	}

	public Integer getCustId() {
		return this.custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getIataCode() {
		return this.iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public Integer getDestId() {
		return this.destId;
	}

	public void setDestId(Integer destId) {
		this.destId = destId;
	}

	public String getDestCode() {
		return this.destCode;
	}

	public void setDestCode(String destCode) {
		this.destCode = destCode;
	}

	public String getShipToCode() {
		return this.shipToCode;
	}

	public void setShipToCode(String shipToCode) {
		this.shipToCode = shipToCode;
	}

	public String getShipToName() {
		return this.shipToName;
	}

	public void setShipToName(String shipToName) {
		this.shipToName = shipToName;
	}

	public Integer getInvLineId() {
		return this.invLineId;
	}

	public void setInvLineId(Integer invLineId) {
		this.invLineId = invLineId;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getAircraftRegistrationNumber() {
		return this.aircraftRegistrationNumber;
	}

	public void setAircraftRegistrationNumber(String aircraftRegistrationNumber) {
		this.aircraftRegistrationNumber = aircraftRegistrationNumber;
	}

	public Suffix getFlightSuffix() {
		return this.flightSuffix;
	}

	public void setFlightSuffix(Suffix flightSuffix) {
		this.flightSuffix = flightSuffix;
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getTicketNo() {
		return this.ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getVat() {
		return this.vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public Boolean getIsCredited() {
		return this.isCredited;
	}

	public void setIsCredited(Boolean isCredited) {
		this.isCredited = isCredited;
	}

	public InvoiceLineTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			InvoiceLineTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public String getErpReference() {
		return this.erpReference;
	}

	public void setErpReference(String erpReference) {
		this.erpReference = erpReference;
	}

	public String getBolNumber() {
		return this.bolNumber;
	}

	public void setBolNumber(String bolNumber) {
		this.bolNumber = bolNumber;
	}

	public String getFlightID() {
		return this.flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
