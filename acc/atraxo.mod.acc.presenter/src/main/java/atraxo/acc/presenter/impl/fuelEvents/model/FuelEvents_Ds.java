/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.fuelEvents.model;

import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.acc_type.ReceivedStatus;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperation;
import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import atraxo.ops.domain.impl.ops_type.Suffix;
import atraxo.ops.domain.impl.ops_type.TemperatureUnit;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelEvent.class, sort = {@SortField(field = FuelEvents_Ds.F_FUELINGDATE)})
@RefLookups({
		@RefLookup(refId = FuelEvents_Ds.F_AIRCRAFTID, namedQuery = Aircraft.NQ_FIND_BY_REG_PRIMITIVE, params = {@Param(name = "customerId", field = FuelEvents_Ds.F_AIRCRAFTREGISTRATION)}),
		@RefLookup(refId = FuelEvents_Ds.F_BILLCURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_BILLCURRENCYCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_UNITCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_NETUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_NETUNITCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_DEPARTUREID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_DEPARTURECODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_SUPPLIERID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_SUPPLIERCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_IPLAGENTID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_IPLAGENTCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_PAYCURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_PAYCURRENCYCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_CONTRACTHOLDERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_CONTRACTHOLDERCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_SHIPTOID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_SHIPTOCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_DENSITYUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_DENSITYUNITCODE)}),
		@RefLookup(refId = FuelEvents_Ds.F_DENSITYVOLUMEID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEvents_Ds.F_DENSITYVOLUMECODE)})})
public class FuelEvents_Ds extends AbstractSubsidiaryDs_Ds<FuelEvent> {

	public static final String ALIAS = "acc_FuelEvents_Ds";

	public static final String F_AIRCRAFTID = "aircraftId";
	public static final String F_AIRCRAFTREGISTRATION = "aircraftRegistration";
	public static final String F_BILLCURRENCYID = "billCurrencyId";
	public static final String F_BILLCURRENCYCODE = "billCurrencyCode";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_ACCOUNTNUMBER = "accountNumber";
	public static final String F_ISSUBSIDIARY = "isSubsidiary";
	public static final String F_CONTRACTHOLDERID = "contractHolderID";
	public static final String F_CONTRACTHOLDERCODE = "contractHolderCode";
	public static final String F_DEPARTUREID = "departureId";
	public static final String F_DEPARTURECODE = "departureCode";
	public static final String F_SHIPTOID = "shipToID";
	public static final String F_SHIPTONAME = "shipToName";
	public static final String F_SHIPTOCODE = "shipToCode";
	public static final String F_SUPPLIERID = "supplierId";
	public static final String F_SUPPLIERCODE = "supplierCode";
	public static final String F_SUPPLIERNAME = "supplierName";
	public static final String F_IPLAGENTID = "iplAgentId";
	public static final String F_IPLAGENTCODE = "iplAgentCode";
	public static final String F_PAYCURRENCYID = "payCurrencyId";
	public static final String F_PAYCURRENCYCODE = "payCurrencyCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_NETUNITID = "netUnitId";
	public static final String F_NETUNITCODE = "netUnitCode";
	public static final String F_DENSITYUNITID = "densityUnitId";
	public static final String F_DENSITYUNITCODE = "densityUnitCode";
	public static final String F_DENSITYVOLUMEID = "densityVolumeId";
	public static final String F_DENSITYVOLUMECODE = "densityVolumeCode";
	public static final String F_FUELINGDATE = "fuelingDate";
	public static final String F_FLIGHTNUMBER = "flightNumber";
	public static final String F_SUFFIX = "suffix";
	public static final String F_TICKETNUMBER = "ticketNumber";
	public static final String F_QUANTITY = "quantity";
	public static final String F_SYSQUANTITY = "sysQuantity";
	public static final String F_QUANTITYSOURCE = "quantitySource";
	public static final String F_PAYABLECOST = "payableCost";
	public static final String F_SYSPAYABLECOST = "sysPayableCost";
	public static final String F_BILLABECOST = "billabeCost";
	public static final String F_SYSBILLABLECOST = "sysBillableCost";
	public static final String F_INVOICESTATUS = "invoiceStatus";
	public static final String F_BILLSTATUS = "billStatus";
	public static final String F_RECEIVEDSTATUS = "receivedStatus";
	public static final String F_ISRESALE = "isResale";
	public static final String F_BILLPRICESOURCEID = "billPriceSourceId";
	public static final String F_BILLPRICESOURCETYPE = "billPriceSourceType";
	public static final String F_EVENTTYPE = "eventType";
	public static final String F_NETUPLIFTQUANTITY = "netUpliftQuantity";
	public static final String F_FUELINGOPERATION = "fuelingOperation";
	public static final String F_TRANSPORT = "transport";
	public static final String F_OPERATIONTYPE = "operationType";
	public static final String F_ERPSALESSHIPMENTNO = "erpSalesShipmentNo";
	public static final String F_SALESINVOICENO = "salesInvoiceNo";
	public static final String F_PURCHASEINVOICENO = "purchaseInvoiceNo";
	public static final String F_CREDITMEMONO = "creditMemoNo";
	public static final String F_PRODUCT = "product";
	public static final String F_FLIGHTID = "flightID";
	public static final String F_STATUS = "status";
	public static final String F_TRANSMISSIONSTATUSSALE = "transmissionStatusSale";
	public static final String F_TRANSMISSIONSTATUSPURCHASE = "transmissionStatusPurchase";
	public static final String F_ERPPURCHASERECEIPTNO = "erpPurchaseReceiptNo";
	public static final String F_ERPSALEINVOICENO = "erpSaleInvoiceNo";
	public static final String F_DENSITY = "density";
	public static final String F_TEMPERATURE = "temperature";
	public static final String F_TEMPERATUREUNIT = "temperatureUnit";
	public static final String F_REVOCATIONREASON = "revocationReason";
	public static final String F_BOLNUMBER = "bolNumber";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_RESALEDETAIL = "resaleDetail";
	public static final String F_PAYABLECOSTANDCURR = "payableCostAndCurr";
	public static final String F_BILLABLECOSTANDCURR = "billableCostAndCurr";
	public static final String F_SETTLEMENTUNITCODE = "settlementUnitCode";
	public static final String F_RESALEFUELEVENTID = "resaleFuelEventId";
	public static final String F_RESALEFUELEVENTRESALESTATUS = "resaleFuelEventResaleStatus";
	public static final String F_RESALEFUELEVENTINVOICESTATUS = "resaleFuelEventInvoiceStatus";

	@DsField(join = "left", path = "aircraft.id")
	private Integer aircraftId;

	@DsField(join = "left", path = "aircraft.registration")
	private String aircraftRegistration;

	@DsField(join = "left", path = "billableCostCurrency.id")
	private Integer billCurrencyId;

	@DsField(join = "left", path = "billableCostCurrency.code")
	private String billCurrencyCode;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "customer.accountNumber")
	private String accountNumber;

	@DsField(join = "left", path = "customer.isSubsidiary")
	private Boolean isSubsidiary;

	@DsField(join = "left", path = "contractHolder.id")
	private Integer contractHolderID;

	@DsField(join = "left", path = "contractHolder.code")
	private String contractHolderCode;

	@DsField(join = "left", path = "departure.id")
	private Integer departureId;

	@DsField(join = "left", path = "departure.code")
	private String departureCode;

	@DsField(join = "left", path = "shipTo.id")
	private Integer shipToID;

	@DsField(join = "left", path = "shipTo.name")
	private String shipToName;

	@DsField(join = "left", path = "shipTo.code")
	private String shipToCode;

	@DsField(join = "left", path = "fuelSupplier.id")
	private Integer supplierId;

	@DsField(join = "left", path = "fuelSupplier.code")
	private String supplierCode;

	@DsField(join = "left", path = "fuelSupplier.name")
	private String supplierName;

	@DsField(join = "left", path = "iplAgent.id")
	private Integer iplAgentId;

	@DsField(join = "left", path = "iplAgent.code")
	private String iplAgentCode;

	@DsField(join = "left", path = "payableCostCurrency.id")
	private Integer payCurrencyId;

	@DsField(join = "left", path = "payableCostCurrency.code")
	private String payCurrencyCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "netUpliftUnit.id")
	private Integer netUnitId;

	@DsField(join = "left", path = "netUpliftUnit.code")
	private String netUnitCode;

	@DsField(join = "left", path = "densityUnit.id")
	private Integer densityUnitId;

	@DsField(join = "left", path = "densityUnit.code")
	private String densityUnitCode;

	@DsField(join = "left", path = "densityVolume.id")
	private Integer densityVolumeId;

	@DsField(join = "left", path = "densityVolume.code")
	private String densityVolumeCode;

	@DsField
	private Date fuelingDate;

	@DsField
	private String flightNumber;

	@DsField
	private Suffix suffix;

	@DsField
	private String ticketNumber;

	@DsField
	private BigDecimal quantity;

	@DsField
	private BigDecimal sysQuantity;

	@DsField
	private QuantitySource quantitySource;

	@DsField
	private BigDecimal payableCost;

	@DsField
	private BigDecimal sysPayableCost;

	@DsField(path = "billableCost")
	private BigDecimal billabeCost;

	@DsField
	private BigDecimal sysBillableCost;

	@DsField
	private OutgoingInvoiceStatus invoiceStatus;

	@DsField
	private BillStatus billStatus;

	@DsField
	private ReceivedStatus receivedStatus;

	@DsField
	private Boolean isResale;

	@DsField
	private Integer billPriceSourceId;

	@DsField
	private String billPriceSourceType;

	@DsField
	private FlightTypeIndicator eventType;

	@DsField
	private BigDecimal netUpliftQuantity;

	@DsField
	private FuelTicketFuelingOperation fuelingOperation;

	@DsField
	private FuelingType transport;

	@DsField
	private OperationType operationType;

	@DsField(path = "erpSalesShipment")
	private String erpSalesShipmentNo;

	@DsField
	private String salesInvoiceNo;

	@DsField
	private String purchaseInvoiceNo;

	@DsField
	private String creditMemoNo;

	@DsField
	private Product product;

	@DsField
	private String flightID;

	@DsField
	private FuelEventStatus status;

	@DsField
	private FuelEventTransmissionStatus transmissionStatusSale;

	@DsField
	private FuelEventTransmissionStatus transmissionStatusPurchase;

	@DsField(path = "erpPurchaseReceipt")
	private String erpPurchaseReceiptNo;

	@DsField
	private String erpSaleInvoiceNo;

	@DsField
	private BigDecimal density;

	@DsField
	private Integer temperature;

	@DsField
	private TemperatureUnit temperatureUnit;

	@DsField
	private RevocationReason revocationReason;

	@DsField
	private String bolNumber;

	@DsField(fetch = false)
	private String formTitle;

	@DsField(fetch = false)
	private String resaleDetail;

	@DsField(fetch = false)
	private String payableCostAndCurr;

	@DsField(fetch = false)
	private String billableCostAndCurr;

	@DsField(fetch = false)
	private String settlementUnitCode;

	@DsField(fetch = false)
	private Integer resaleFuelEventId;

	@DsField(fetch = false)
	private String resaleFuelEventResaleStatus;

	@DsField(fetch = false)
	private String resaleFuelEventInvoiceStatus;

	/**
	 * Default constructor
	 */
	public FuelEvents_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelEvents_Ds(FuelEvent e) {
		super(e);
	}

	public Integer getAircraftId() {
		return this.aircraftId;
	}

	public void setAircraftId(Integer aircraftId) {
		this.aircraftId = aircraftId;
	}

	public String getAircraftRegistration() {
		return this.aircraftRegistration;
	}

	public void setAircraftRegistration(String aircraftRegistration) {
		this.aircraftRegistration = aircraftRegistration;
	}

	public Integer getBillCurrencyId() {
		return this.billCurrencyId;
	}

	public void setBillCurrencyId(Integer billCurrencyId) {
		this.billCurrencyId = billCurrencyId;
	}

	public String getBillCurrencyCode() {
		return this.billCurrencyCode;
	}

	public void setBillCurrencyCode(String billCurrencyCode) {
		this.billCurrencyCode = billCurrencyCode;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Boolean getIsSubsidiary() {
		return this.isSubsidiary;
	}

	public void setIsSubsidiary(Boolean isSubsidiary) {
		this.isSubsidiary = isSubsidiary;
	}

	public Integer getContractHolderID() {
		return this.contractHolderID;
	}

	public void setContractHolderID(Integer contractHolderID) {
		this.contractHolderID = contractHolderID;
	}

	public String getContractHolderCode() {
		return this.contractHolderCode;
	}

	public void setContractHolderCode(String contractHolderCode) {
		this.contractHolderCode = contractHolderCode;
	}

	public Integer getDepartureId() {
		return this.departureId;
	}

	public void setDepartureId(Integer departureId) {
		this.departureId = departureId;
	}

	public String getDepartureCode() {
		return this.departureCode;
	}

	public void setDepartureCode(String departureCode) {
		this.departureCode = departureCode;
	}

	public Integer getShipToID() {
		return this.shipToID;
	}

	public void setShipToID(Integer shipToID) {
		this.shipToID = shipToID;
	}

	public String getShipToName() {
		return this.shipToName;
	}

	public void setShipToName(String shipToName) {
		this.shipToName = shipToName;
	}

	public String getShipToCode() {
		return this.shipToCode;
	}

	public void setShipToCode(String shipToCode) {
		this.shipToCode = shipToCode;
	}

	public Integer getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierCode() {
		return this.supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Integer getIplAgentId() {
		return this.iplAgentId;
	}

	public void setIplAgentId(Integer iplAgentId) {
		this.iplAgentId = iplAgentId;
	}

	public String getIplAgentCode() {
		return this.iplAgentCode;
	}

	public void setIplAgentCode(String iplAgentCode) {
		this.iplAgentCode = iplAgentCode;
	}

	public Integer getPayCurrencyId() {
		return this.payCurrencyId;
	}

	public void setPayCurrencyId(Integer payCurrencyId) {
		this.payCurrencyId = payCurrencyId;
	}

	public String getPayCurrencyCode() {
		return this.payCurrencyCode;
	}

	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getNetUnitId() {
		return this.netUnitId;
	}

	public void setNetUnitId(Integer netUnitId) {
		this.netUnitId = netUnitId;
	}

	public String getNetUnitCode() {
		return this.netUnitCode;
	}

	public void setNetUnitCode(String netUnitCode) {
		this.netUnitCode = netUnitCode;
	}

	public Integer getDensityUnitId() {
		return this.densityUnitId;
	}

	public void setDensityUnitId(Integer densityUnitId) {
		this.densityUnitId = densityUnitId;
	}

	public String getDensityUnitCode() {
		return this.densityUnitCode;
	}

	public void setDensityUnitCode(String densityUnitCode) {
		this.densityUnitCode = densityUnitCode;
	}

	public Integer getDensityVolumeId() {
		return this.densityVolumeId;
	}

	public void setDensityVolumeId(Integer densityVolumeId) {
		this.densityVolumeId = densityVolumeId;
	}

	public String getDensityVolumeCode() {
		return this.densityVolumeCode;
	}

	public void setDensityVolumeCode(String densityVolumeCode) {
		this.densityVolumeCode = densityVolumeCode;
	}

	public Date getFuelingDate() {
		return this.fuelingDate;
	}

	public void setFuelingDate(Date fuelingDate) {
		this.fuelingDate = fuelingDate;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public String getTicketNumber() {
		return this.ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getSysQuantity() {
		return this.sysQuantity;
	}

	public void setSysQuantity(BigDecimal sysQuantity) {
		this.sysQuantity = sysQuantity;
	}

	public QuantitySource getQuantitySource() {
		return this.quantitySource;
	}

	public void setQuantitySource(QuantitySource quantitySource) {
		this.quantitySource = quantitySource;
	}

	public BigDecimal getPayableCost() {
		return this.payableCost;
	}

	public void setPayableCost(BigDecimal payableCost) {
		this.payableCost = payableCost;
	}

	public BigDecimal getSysPayableCost() {
		return this.sysPayableCost;
	}

	public void setSysPayableCost(BigDecimal sysPayableCost) {
		this.sysPayableCost = sysPayableCost;
	}

	public BigDecimal getBillabeCost() {
		return this.billabeCost;
	}

	public void setBillabeCost(BigDecimal billabeCost) {
		this.billabeCost = billabeCost;
	}

	public BigDecimal getSysBillableCost() {
		return this.sysBillableCost;
	}

	public void setSysBillableCost(BigDecimal sysBillableCost) {
		this.sysBillableCost = sysBillableCost;
	}

	public OutgoingInvoiceStatus getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(OutgoingInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public BillStatus getBillStatus() {
		return this.billStatus;
	}

	public void setBillStatus(BillStatus billStatus) {
		this.billStatus = billStatus;
	}

	public ReceivedStatus getReceivedStatus() {
		return this.receivedStatus;
	}

	public void setReceivedStatus(ReceivedStatus receivedStatus) {
		this.receivedStatus = receivedStatus;
	}

	public Boolean getIsResale() {
		return this.isResale;
	}

	public void setIsResale(Boolean isResale) {
		this.isResale = isResale;
	}

	public Integer getBillPriceSourceId() {
		return this.billPriceSourceId;
	}

	public void setBillPriceSourceId(Integer billPriceSourceId) {
		this.billPriceSourceId = billPriceSourceId;
	}

	public String getBillPriceSourceType() {
		return this.billPriceSourceType;
	}

	public void setBillPriceSourceType(String billPriceSourceType) {
		this.billPriceSourceType = billPriceSourceType;
	}

	public FlightTypeIndicator getEventType() {
		return this.eventType;
	}

	public void setEventType(FlightTypeIndicator eventType) {
		this.eventType = eventType;
	}

	public BigDecimal getNetUpliftQuantity() {
		return this.netUpliftQuantity;
	}

	public void setNetUpliftQuantity(BigDecimal netUpliftQuantity) {
		this.netUpliftQuantity = netUpliftQuantity;
	}

	public FuelTicketFuelingOperation getFuelingOperation() {
		return this.fuelingOperation;
	}

	public void setFuelingOperation(FuelTicketFuelingOperation fuelingOperation) {
		this.fuelingOperation = fuelingOperation;
	}

	public FuelingType getTransport() {
		return this.transport;
	}

	public void setTransport(FuelingType transport) {
		this.transport = transport;
	}

	public OperationType getOperationType() {
		return this.operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public String getErpSalesShipmentNo() {
		return this.erpSalesShipmentNo;
	}

	public void setErpSalesShipmentNo(String erpSalesShipmentNo) {
		this.erpSalesShipmentNo = erpSalesShipmentNo;
	}

	public String getSalesInvoiceNo() {
		return this.salesInvoiceNo;
	}

	public void setSalesInvoiceNo(String salesInvoiceNo) {
		this.salesInvoiceNo = salesInvoiceNo;
	}

	public String getPurchaseInvoiceNo() {
		return this.purchaseInvoiceNo;
	}

	public void setPurchaseInvoiceNo(String purchaseInvoiceNo) {
		this.purchaseInvoiceNo = purchaseInvoiceNo;
	}

	public String getCreditMemoNo() {
		return this.creditMemoNo;
	}

	public void setCreditMemoNo(String creditMemoNo) {
		this.creditMemoNo = creditMemoNo;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getFlightID() {
		return this.flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}

	public FuelEventStatus getStatus() {
		return this.status;
	}

	public void setStatus(FuelEventStatus status) {
		this.status = status;
	}

	public FuelEventTransmissionStatus getTransmissionStatusSale() {
		return this.transmissionStatusSale;
	}

	public void setTransmissionStatusSale(
			FuelEventTransmissionStatus transmissionStatusSale) {
		this.transmissionStatusSale = transmissionStatusSale;
	}

	public FuelEventTransmissionStatus getTransmissionStatusPurchase() {
		return this.transmissionStatusPurchase;
	}

	public void setTransmissionStatusPurchase(
			FuelEventTransmissionStatus transmissionStatusPurchase) {
		this.transmissionStatusPurchase = transmissionStatusPurchase;
	}

	public String getErpPurchaseReceiptNo() {
		return this.erpPurchaseReceiptNo;
	}

	public void setErpPurchaseReceiptNo(String erpPurchaseReceiptNo) {
		this.erpPurchaseReceiptNo = erpPurchaseReceiptNo;
	}

	public String getErpSaleInvoiceNo() {
		return this.erpSaleInvoiceNo;
	}

	public void setErpSaleInvoiceNo(String erpSaleInvoiceNo) {
		this.erpSaleInvoiceNo = erpSaleInvoiceNo;
	}

	public BigDecimal getDensity() {
		return this.density;
	}

	public void setDensity(BigDecimal density) {
		this.density = density;
	}

	public Integer getTemperature() {
		return this.temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public TemperatureUnit getTemperatureUnit() {
		return this.temperatureUnit;
	}

	public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
		this.temperatureUnit = temperatureUnit;
	}

	public RevocationReason getRevocationReason() {
		return this.revocationReason;
	}

	public void setRevocationReason(RevocationReason revocationReason) {
		this.revocationReason = revocationReason;
	}

	public String getBolNumber() {
		return this.bolNumber;
	}

	public void setBolNumber(String bolNumber) {
		this.bolNumber = bolNumber;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getResaleDetail() {
		return this.resaleDetail;
	}

	public void setResaleDetail(String resaleDetail) {
		this.resaleDetail = resaleDetail;
	}

	public String getPayableCostAndCurr() {
		return this.payableCostAndCurr;
	}

	public void setPayableCostAndCurr(String payableCostAndCurr) {
		this.payableCostAndCurr = payableCostAndCurr;
	}

	public String getBillableCostAndCurr() {
		return this.billableCostAndCurr;
	}

	public void setBillableCostAndCurr(String billableCostAndCurr) {
		this.billableCostAndCurr = billableCostAndCurr;
	}

	public String getSettlementUnitCode() {
		return this.settlementUnitCode;
	}

	public void setSettlementUnitCode(String settlementUnitCode) {
		this.settlementUnitCode = settlementUnitCode;
	}

	public Integer getResaleFuelEventId() {
		return this.resaleFuelEventId;
	}

	public void setResaleFuelEventId(Integer resaleFuelEventId) {
		this.resaleFuelEventId = resaleFuelEventId;
	}

	public String getResaleFuelEventResaleStatus() {
		return this.resaleFuelEventResaleStatus;
	}

	public void setResaleFuelEventResaleStatus(
			String resaleFuelEventResaleStatus) {
		this.resaleFuelEventResaleStatus = resaleFuelEventResaleStatus;
	}

	public String getResaleFuelEventInvoiceStatus() {
		return this.resaleFuelEventInvoiceStatus;
	}

	public void setResaleFuelEventInvoiceStatus(
			String resaleFuelEventInvoiceStatus) {
		this.resaleFuelEventInvoiceStatus = resaleFuelEventInvoiceStatus;
	}
}
