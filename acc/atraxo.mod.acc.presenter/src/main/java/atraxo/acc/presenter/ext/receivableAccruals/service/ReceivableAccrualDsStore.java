package atraxo.acc.presenter.ext.receivableAccruals.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;

class ReceivableAccrualDsStore {
	
	private Date date;
	private List<ReceivableAccrual> listOfElements;
	private Integer events;
	private BigDecimal netAmount;
	private BigDecimal vatAmount;
	private BigDecimal grossAmount;
	private String locationCode;
	private String invoiceCurrencyCode;
	private String expInvoiceCurrencyCode;
	private String accrualInvoiceCurrencyCode;
	
	public ReceivableAccrualDsStore(Date date, List<ReceivableAccrual> listOfElements, Integer events, BigDecimal netAmount, BigDecimal vatAmount,
			BigDecimal grossAmount) {
		super();
		this.date = date;
		this.listOfElements = listOfElements;
		this.events = events;
		this.netAmount = netAmount;
		this.vatAmount = vatAmount;
		this.grossAmount = grossAmount;
	}
	
	public ReceivableAccrualDsStore() {
		super();
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<ReceivableAccrual> getListOfElements() {
		return listOfElements;
	}

	public void setListOfElements(List<ReceivableAccrual> listOfElements) {
		this.listOfElements = listOfElements;
	}

	public Integer getEvents() {
		return events;
	}

	public void setEvents(Integer events) {
		this.events = events;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getVatAmount() {
		return vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getGrossAmount() {
		return grossAmount;
	}

	public void setGrossAmount(BigDecimal grossAmount) {
		this.grossAmount = grossAmount;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getInvoiceCurrencyCode() {
		return invoiceCurrencyCode;
	}
	public void setInvoiceCurrencyCode(String invoiceCurrencyCode) {
		this.invoiceCurrencyCode = invoiceCurrencyCode;
	}
	public String getExpInvoiceCurrencyCode() {
		return expInvoiceCurrencyCode;
	}
	public void setExpInvoiceCurrencyCode(String expInvoiceCurrencyCode) {
		this.expInvoiceCurrencyCode = expInvoiceCurrencyCode;
	}
	public String getAccrualInvoiceCurrencyCode() {
		return accrualInvoiceCurrencyCode;
	}
	public void setAccrualInvoiceCurrencyCode(String accrualInvoiceCurrencyCode) {
		this.accrualInvoiceCurrencyCode = accrualInvoiceCurrencyCode;
	}
	
	
	

	

}
