package atraxo.acc.presenter.ext.fuelEvents.delegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTimeComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IOutgoingInvoiceLineService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ws.ExportFuelEventOperationEnum;
import atraxo.acc.business.ws.ExportFuelEventOperationResult;
import atraxo.acc.business.ws.ExportFuelEventPumaRestClient;
import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.ReceivedStatus;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.presenter.impl.fuelEvents.model.FuelEvents_Ds;
import atraxo.acc.presenter.impl.fuelEvents.model.FuelEvents_DsParam;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * Presenter Delegate for Fuel Event entity
 */
public class FuelEvent_Pd extends AbstractPresenterDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(FuelEvent_Pd.class);

	private static final String EXPORT_OPTION_SELECTED = "selected";
	private static final String EXPORT_OPTION_FAILED = "failed";

	private static final String FUEL_EVENT_MODEL_PROPERY_TRANSMISSION_STATUS_SALE = "transmissionStatusSale";
	private static final String FUEL_EVENT_MODEL_PROPERY_TRANSMISSION_STATUS_PURCHASE = "transmissionStatusPurchase";

	/**
	 * @param dsList
	 * @param params
	 * @throws Exception
	 */
	public void addNewOutgoingInvoiceLine(List<FuelEvents_Ds> dsList, FuelEvents_DsParam params) throws Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("START addNewOutgoingInvoiceLine()");
		}

		List<FuelEvent> events = new ArrayList<>();
		IFuelEventService fuelEventService = this.getFuelEventService();
		IOutgoingInvoiceService invoiceService = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		OutgoingInvoice oi = invoiceService.findById(params.getOutgoingInvoiceId());
		IOutgoingInvoiceLineService invoiceLineService = (IOutgoingInvoiceLineService) this.findEntityService(OutgoingInvoiceLine.class);
		StringBuilder sb = new StringBuilder();
		for (FuelEvents_Ds ds : dsList) {
			if (DateTimeComparator.getDateOnlyInstance().compare(oi.getInvoiceDate(), ds.getFuelingDate()) < 0) {
				sb.append(ds.getFuelingDate()).append(",");
			} else {
				FuelEvent fuelEvent = fuelEventService.findById(ds.getId());
				if (StringUtils.isEmpty(fuelEvent.getSalesInvoiceNo())) {
					events.add(fuelEvent);
				} else {
					throw new BusinessException(AccErrorCode.THE_FUEL_EVENT_IS_ALREADY_ADDED_TO_THE_INVOICE,
							AccErrorCode.THE_FUEL_EVENT_IS_ALREADY_ADDED_TO_THE_INVOICE.getErrMsg());
				}
			}
		}

		if (sb.toString().isEmpty()) {
			invoiceLineService.insertNewInvoiceLines(events, oi);
		} else {

		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("END addNewOutgoingInvoiceLine()");
		}
	}

	/**
	 * @param dsList
	 * @param params
	 * @throws Exception
	 */
	public void putOnHoldList(List<FuelEvents_Ds> dsList, FuelEvents_DsParam params) throws Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("START putOnHoldList()");
		}

		List<Object> ids = new ArrayList<>();
		for (FuelEvents_Ds fd : dsList) {
			ids.add(fd.getId());
		}

		IFuelEventService fuelEventService = this.getFuelEventService();

		List<FuelEvent> fuelEvents = fuelEventService.findByIds(ids);

		Iterator<FuelEvent> iter = fuelEvents.iterator();
		StringBuilder sb = new StringBuilder("<ul>");
		while (iter.hasNext()) {
			FuelEvent fe = iter.next();
			if (!ReceivedStatus._RELEASED_.equals(fe.getReceivedStatus())) {
				sb.append("<li>").append(fe.getTicketNumber()).append("</li>");
				iter.remove();
			}
		}
		sb.append("</ul>");

		if (!CollectionUtils.isEmpty(fuelEvents)) {
			fuelEventService.putOnHoldFuelEvents(fuelEvents, params.getRemark());
		} else {
			LOG.warn("WARNING: could not retrieve any fuel events for the provided IDS ! Will do nothing since the input data is incorrect !");
		}

		String msg = sb.toString();
		if (msg.contains("<li>")) {
			throw new BusinessException(AccErrorCode.FUEL_EVENT_RPC_ACTION_ERROR,
					String.format(AccErrorCode.FUEL_EVENT_RPC_ACTION_ERROR.getErrMsg(), msg));
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("END putOnHoldList()");
		}
	}

	/**
	 * @param dsList
	 * @throws Exception
	 */
	public void releaseList(List<FuelEvents_Ds> dsList, FuelEvents_DsParam params) throws Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("START releaseList()");
		}

		List<Object> ids = new ArrayList<>();
		for (FuelEvents_Ds fd : dsList) {
			ids.add(fd.getId());
		}

		IFuelEventService fuelEventService = this.getFuelEventService();

		List<FuelEvent> fuelEvents = fuelEventService.findByIds(ids);

		Iterator<FuelEvent> iter = fuelEvents.iterator();
		StringBuilder sb = new StringBuilder("<ul>");
		while (iter.hasNext()) {
			FuelEvent fe = iter.next();
			if ((ReceivedStatus._ON_HOLD_.equals(fe.getReceivedStatus()) && FuelEventStatus._CANCELED_.equals(fe.getStatus()))
					|| ReceivedStatus._RELEASED_.equals(fe.getReceivedStatus())) {
				sb.append("<li>").append(fe.getTicketNumber()).append("</li>");
				iter.remove();
			}
		}
		sb.append("</ul>");
		if (!CollectionUtils.isEmpty(fuelEvents)) {
			fuelEventService.releaseFuelEvents(fuelEvents);
			this.reOrderResaleFuelEvents(fuelEvents);
			for (FuelEvent e : fuelEvents) {
				fuelEventService.export(e);
			}
		} else {
			LOG.warn("WARNING: could not retrieve any fuel events for the provided IDS ! Will do nothing since the input data is incorrect !");
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("END releaseList()");
		}

		String msg = sb.toString();
		if (msg.contains("<li>")) {
			throw new BusinessException(AccErrorCode.FUEL_EVENT_RPC_ACTION_ERROR,
					String.format(AccErrorCode.FUEL_EVENT_RPC_ACTION_ERROR.getErrMsg(), msg));
		}

	}

	/**
	 * Reorders the elements in the list of <code>FuelEvent</code> so that the resale fuel events(payable and receivable prices exist both)are in teh
	 * top of the list (at the beginning so they are processed first)
	 *
	 * @param fuelEventList
	 */
	private void reOrderResaleFuelEvents(List<FuelEvent> fuelEventList) {
		// make sure to put the resale fuel events(payable and receivable prices exist both) first
		List<FuelEvent> fuelEventListPayAndRec = new ArrayList<FuelEvent>();
		List<FuelEvent> fuelEventListRest = new ArrayList<FuelEvent>();
		for (FuelEvent e : fuelEventList) {
			if (e.getPayableCost() != null && e.getBillableCost() != null) {
				fuelEventListPayAndRec.add(e);
			} else {
				fuelEventListRest.add(e);
			}
		}
		fuelEventList.clear();
		fuelEventList.addAll(fuelEventListPayAndRec);
		fuelEventList.addAll(fuelEventListRest);
	}

	/**
	 * @param dsList
	 * @param param
	 * @throws Exception
	 */
	public void exportFuelPlusToNAVFuelEvent(List<FuelEvents_Ds> dsList, FuelEvents_DsParam param) throws Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("START exportFuelPlusToNAVFuelEvent()");
		}
		IFuelEventService fuelEventService = (IFuelEventService) this.findEntityService(FuelEvent.class);

		IExternalInterfaceService externalInterfaceService = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);

		ExternalInterface sendFuelEventsInterface = externalInterfaceService.findByName(ExtInterfaceNames.EXPORT_FUEL_EVENTS_TO_NAV.getValue());

		if (sendFuelEventsInterface != null) {

			if (sendFuelEventsInterface.getEnabled()) {
				// get the export option
				String option = param.getExportFuelEventOption();

				List<FuelEvent> fuelEvents = null;
				if (option.equals(EXPORT_OPTION_FAILED)) {
					// find only the failed ones
					fuelEvents = this.getFuelEventsFailed(fuelEventService);
				} else if (option.equals(EXPORT_OPTION_SELECTED)) {
					// find the ones that were selected
					fuelEvents = this.getFuelEventsForSelectedOption(fuelEventService, dsList);
				} else {
					LOG.error("ERROR:could not read export option for " + option + " !");
					param.setExportFuelEventResult(false);
					param.setExportFuelEventDescription(BusinessErrorCode.INTERFACE_EXPORT_CUSTOMERS_INCORRECT_OPTION.getErrMsg());
				}

				// check to see if the fuel events were brought OK from DB (even if none)
				if (fuelEvents != null) {
					// check to see if there actually are some fuel events to export
					if (!fuelEvents.isEmpty()) {

						// make sure to remove the ones that do not meet the assumed requirements (also create a message for the user)
						boolean allFuelEventsSynced = true;
						Iterator<FuelEvent> iter = fuelEvents.iterator();
						StringBuilder sb = new StringBuilder("<ul>");
						while (iter.hasNext()) {
							FuelEvent fe = iter.next();
							if (ReceivedStatus._ON_HOLD_.equals(fe.getReceivedStatus()) && FuelEventStatus._CANCELED_.equals(fe.getStatus())) {
								sb.append("<li>").append(fe.getTicketNumber()).append("</li>");
								iter.remove();
								allFuelEventsSynced = false;
							}
						}
						sb.append("</ul>");

						// export the fuel events that fulfill the export conditions
						ExportFuelEventPumaRestClient exportFuelEventPumaRestClient = this.getApplicationContext()
								.getBean(ExportFuelEventPumaRestClient.class);
						int nrEventsExportedOk = 0;
						int nrEventsExportedTotal = 0;
						for (FuelEvent fe : fuelEvents) {
							ExportFuelEventOperationResult result = exportFuelEventPumaRestClient.export(fe, this.getExportFuelEventOperation(fe),
									true, option.equals(EXPORT_OPTION_FAILED) ? true : false);
							if (result.attemptedExport()) {
								// update fuel event only if fuel event changed (was an attempt to export)
								fe.setRegenerateDetails(false);
								fuelEventService.update(fe);
							}
							nrEventsExportedOk += result.getNrEventsExported();
							nrEventsExportedTotal += result.getNrEventsTotal();
						}
						// set up the results for the user
						param.setExportFuelEventResult(nrEventsExportedOk == nrEventsExportedTotal);
						param.setExportFuelEventDescription(String.format(BusinessErrorCode.INTERFACE_EXPORT_FUEL_EVENTS_DETAILS.getErrMsg(),
								Integer.toString(nrEventsExportedOk), nrEventsExportedTotal, ExtInterfaceNames.EXPORT_FUEL_EVENTS_TO_NAV.getValue(),
								ExtInterfaceNames.EXPORT_FUEL_EVENTS_TO_NAV.getValue()));

						// if not all fuel events synced (and exported) inform the user
						if (!allFuelEventsSynced) {
							throw new BusinessException(AccErrorCode.FUEL_EVENT_RPC_ACTION_ERROR,
									String.format(AccErrorCode.FUEL_EVENT_RPC_ACTION_ERROR.getErrMsg(), sb.toString()));
						}

					} else {
						param.setExportFuelEventResult(true);
						param.setExportFuelEventDescription(BusinessErrorCode.INTERFACE_EXPORT_FUEL_EVENTS_NO_FUEL_EVENTS.getErrMsg());
					}
				} else {
					LOG.error("ERROR:could not find FuelEvents for " + fuelEvents + " fuel events and option " + option + "!");
					param.setExportFuelEventResult(false);
					param.setExportFuelEventDescription(BusinessErrorCode.INTERFACE_EXPORT_FUEL_EVENTS_INCORRECT_OPTION.getErrMsg());
				}
			} else {
				param.setExportFuelEventResult(false);
				param.setExportFuelEventDescription(BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
			}
		} else {
			LOG.error("ERROR:could not find External Interface for " + ExtInterfaceNames.EXPORT_FUEL_EVENTS_TO_NAV.getValue());
			param.setExportFuelEventResult(false);
			param.setExportFuelEventDescription(ErrorCode.G_RUNTIME_ERROR.getErrMsg());
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("END exportFuelPlusToNAVFuelEvent()");
		}
	}

	/**
	 * @param fe
	 * @return
	 */
	private ExportFuelEventOperationEnum getExportFuelEventOperation(FuelEvent fe) {
		return FuelEventStatus._CANCELED_.equals(fe.getStatus()) ? ExportFuelEventOperationEnum.CANCEL_FUEL_EVENT
				: ExportFuelEventOperationEnum.COST_UPDATE_FUEL_EVENT;
	}

	/**
	 * @param fuelEventService
	 * @return
	 */
	private List<FuelEvent> getFuelEventsFailed(IFuelEventService fuelEventService) {
		Map<String, Object> params = new HashMap<>();
		params.put(FUEL_EVENT_MODEL_PROPERY_TRANSMISSION_STATUS_SALE, FuelEventTransmissionStatus._FAILED_);
		List<FuelEvent> fuelEvents = fuelEventService.findEntitiesByAttributes(params);
		params.clear();
		params.put(FUEL_EVENT_MODEL_PROPERY_TRANSMISSION_STATUS_PURCHASE, FuelEventTransmissionStatus._FAILED_);
		List<FuelEvent> fuelEventsFailedPurchase = fuelEventService.findEntitiesByAttributes(params);
		for (FuelEvent fe : fuelEventsFailedPurchase) {
			if (!this.containedInFuelEventsList(fe, fuelEvents)) {
				fuelEvents.add(fe);
			}
		}
		return fuelEvents;
	}

	/**
	 * @param fe
	 * @param fuelEvents
	 * @return
	 */
	private boolean containedInFuelEventsList(FuelEvent fe, List<FuelEvent> fuelEvents) {
		boolean contained = false;
		for (FuelEvent feList : fuelEvents) {
			if (feList.getId().equals(fe.getId())) {
				contained = true;
				break;
			}
		}
		return contained;
	}

	/**
	 * @param fuelEventService
	 * @param fuelEventsDsList
	 * @return
	 */
	private List<FuelEvent> getFuelEventsForSelectedOption(IFuelEventService fuelEventService, List<FuelEvents_Ds> fuelEventsDsList) {
		List<FuelEvent> fuelEvents;
		if (fuelEventsDsList != null && !fuelEventsDsList.isEmpty()) {
			List<Object> fuelEventIds = fuelEventsDsList.stream().map(FuelEvents_Ds::getId).collect(Collectors.toList());
			fuelEvents = fuelEventService.findByIds(fuelEventIds);
		} else {
			fuelEvents = new ArrayList<>();
		}
		return fuelEvents;
	}

	/**
	 * @return
	 */
	private IFuelEventService getFuelEventService() throws Exception {
		return (IFuelEventService) this.findEntityService(FuelEvent.class);
	}

}
