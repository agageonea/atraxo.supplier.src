package atraxo.acc.presenter.ext.invoices.delegate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.ext.invoices.IIataInvoiceService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.invoices.service.OutgoingInvoiceToXMLTransformer;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.acc_type.InvoiceTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_Ds;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_DsParam;
import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.util.StringUtil;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.user.UserSupp;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.result.IDsMarshaller;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.Session;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceTransmission;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author vhojda
 */
public class OutgoingInvoice_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(OutgoingInvoice_Pd.class);

	private static final String EXPORT_OPTION_ALL = "all";
	private static final String EXPORT_OPTION_SELECTED = "selected";
	private static final String EXPORT_OPTION_FAILED = "failed";

	private static final String INVOICE_MODEL_PROPERY_TRANSMISSION_STATUS = "transmissionStatus";

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */

	/**
	 * @param dsList
	 * @param params
	 * @throws Exception
	 */
	public void bulkExportInvoices(List<OutgoingInvoice_Ds> dsList, OutgoingInvoice_DsParam params) throws Exception {
		params.setSubmitForApprovalResult(false);
		IUserSuppService srv = this.getApplicationContext().getBean(IUserSuppService.class);
		UserSupp user = srv.findByCode(Session.user.get().getLoginName());
		// to avoid problem lazy loading
		user.getNotifications().size();
		List<OutgoingInvoice> list = this.find(params);
		this.filterList(params, list);
		if (list.isEmpty()) {
			throw new BusinessException(AccErrorCode.EXPORT_INVOICE_LIST_EMPTY, AccErrorCode.EXPORT_INVOICE_LIST_EMPTY.getErrMsg());
		}

		Map<String, Object> vars = new HashMap<>();
		vars.put(WorkflowVariablesConstants.MAP_NOTIFICATIONS, list);
		vars.put(WorkflowVariablesConstants.REPORT_ID, params.getReportCode());
		vars.put(WorkflowVariablesConstants.REPORT_NAME, params.getReportName());
		vars.put(WorkflowVariablesConstants.USER, user);

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");
		WorkflowStartResult result = workflowBpmManager.startWorkflow(WorkflowNames.SEND_INVOICE_NOTIFICATIONS.getWorkflowName(), vars, null);

		if (result.isStarted()) {
			params.setSubmitForApprovalResult(true);
			params.setSubmitForApprovalDescription(AccErrorCode.INVOICE_BULK_EXPORT.getErrMsg());
		} else {
			params.setSubmitForApprovalDescription(AccErrorCode.INVOICE_BULK_EXPORT_ERROR.getErrMsg() + result.getReason());
		}
	}

	private void filterList(OutgoingInvoice_DsParam params, List<OutgoingInvoice> list) {

		List<String> idList;

		if (params.getSelectedAllInvoices()) {
			String[] arr = params.getUnselectedInvoices().split(",");
			idList = Arrays.asList(arr);
		} else {
			String[] arr = params.getSelectedInvoices().split(",");
			idList = Arrays.asList(arr);
		}

		for (Iterator<OutgoingInvoice> iterator = list.iterator(); iterator.hasNext();) {
			OutgoingInvoice outgoingInvoice = iterator.next();

			if ((params.getSelectedAllInvoices() && idList.contains(outgoingInvoice.getId().toString()))
					|| (!params.getSelectedAllInvoices() && !idList.contains(outgoingInvoice.getId().toString()))) {
				iterator.remove();
			}
		}
	}

	private List<OutgoingInvoice> find(OutgoingInvoice_DsParam params) throws Exception {
		IDsService<OutgoingInvoice_Ds, Object, Object> service = this.findDsService(OutgoingInvoice_Ds.class);
		IDsMarshaller<OutgoingInvoice_Ds, Object, Object> marshaller = service.createMarshaller(IDsMarshaller.JSON);

		Object stdFilter = marshaller.readFilterFromString(params.getStandardFilter());
		IQueryBuilder<OutgoingInvoice_Ds, Object, Object> builder = service.createQueryBuilder().addFilter(stdFilter);
		List<IFilterRule> filterRules = marshaller.readFilterRules(params.getFilter());

		if (!CollectionUtils.isEmpty(filterRules)) {
			builder.addFilterRules(filterRules);
		}

		QueryBuilderWithJpql<OutgoingInvoice_Ds, Object, Object> bld = (QueryBuilderWithJpql<OutgoingInvoice_Ds, Object, Object>) builder;

		List<OutgoingInvoice> list = bld.createQuery(OutgoingInvoice.class).getResultList();
		for (Iterator<OutgoingInvoice> iterator = list.iterator(); iterator.hasNext();) {
			OutgoingInvoice outgoingInvoice = iterator.next();
			if (!BillStatus._AWAITING_PAYMENT_.equals(outgoingInvoice.getStatus()) && !BillStatus._PAID_.equals(outgoingInvoice.getStatus())) {
				iterator.remove();
			}

		}
		return list;
	}

	/**
	 * Exports Invoices to NAV
	 *
	 * @param dsList
	 * @param param
	 * @throws Exception
	 */
	public void exportFuelPlusToNAVInvoice(List<OutgoingInvoice_Ds> dsList, OutgoingInvoice_DsParam param) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START exportFuelPlusToNAVInvoice()");
		}

		IExternalInterfaceService externalInterfaceService = (IExternalInterfaceService) this.findEntityService(ExternalInterface.class);

		// check export invoices to nav external interface existence
		ExternalInterface exportInvoicesToNAVInterface = externalInterfaceService.findByName(ExtInterfaceNames.EXPORT_INVOICES_TO_NAV.getValue());
		if (exportInvoicesToNAVInterface == null) {
			LOGGER.error("ERROR:could not find External Interface for " + ExtInterfaceNames.EXPORT_INVOICES_TO_NAV.getValue());
			param.setExportInvoiceResult(false);
			param.setExportInvoiceDescription(ErrorCode.G_RUNTIME_ERROR.getErrMsg());
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("END exportFuelPlusToNAVInvoice()");
			}
			return;
		}

		// check export credit memos to nav external interface existence
		ExternalInterface exportCreditMemosToNAVInterface = externalInterfaceService
				.findByName(ExtInterfaceNames.EXPORT_CREDIT_MEMO_TO_NAV.getValue());
		if (exportCreditMemosToNAVInterface == null) {
			LOGGER.error("ERROR:could not find External Interface for " + ExtInterfaceNames.EXPORT_CREDIT_MEMO_TO_NAV.getValue());
			param.setExportInvoiceResult(false);
			param.setExportInvoiceDescription(ErrorCode.G_RUNTIME_ERROR.getErrMsg());
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("END exportFuelPlusToNAVInvoice()");
			}
			return;
		}

		// if both interfaces are disabled break the cycle
		if (!exportInvoicesToNAVInterface.getEnabled() && !exportCreditMemosToNAVInterface.getEnabled()) {
			param.setExportInvoiceResult(false);
			param.setExportInvoiceDescription(BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("END exportFuelPlusToNAVInvoice()");
			}
			return;
		}

		IOutgoingInvoiceService outgoingInvoiceService = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);

		// get the export option
		String option = param.getExportInvoiceOption();

		List<OutgoingInvoice> outgoingInvoices = null;

		if (option.equals(EXPORT_OPTION_ALL)) {
			// get all the outgoing invoices
			outgoingInvoices = outgoingInvoiceService.findEntitiesByAttributes(Collections.<String, Object> emptyMap());
		} else if (option.equals(EXPORT_OPTION_FAILED)) {
			// find only the failed ones
			Map<String, Object> params = new HashMap<>();
			params.put(INVOICE_MODEL_PROPERY_TRANSMISSION_STATUS, InvoiceTransmissionStatus._FAILED_);
			outgoingInvoices = outgoingInvoiceService.findEntitiesByAttributes(params);
		} else if (option.equals(EXPORT_OPTION_SELECTED)) {
			// find the ones that were selected
			outgoingInvoices = this.getFuelEventsForSelectedOption(outgoingInvoiceService, dsList);
		} else {
			LOGGER.error("ERROR:could not read export option for " + option + " !");
			param.setExportInvoiceResult(false);
			param.setExportInvoiceDescription(BusinessErrorCode.INTERFACE_EXPORT_OUT_INVOICES_INCORRECT_OPTION.getErrMsg());
		}

		// check to see if the fuel events were brought OK from DB (even if none)
		if (outgoingInvoices != null) {

			// filter the results (remove the ones that are not awaiting payment & paid)
			for (Iterator<OutgoingInvoice> it = outgoingInvoices.iterator(); it.hasNext();) {
				OutgoingInvoice outInvoice = it.next();
				if (!(BillStatus._AWAITING_PAYMENT_.equals(outInvoice.getStatus()) || BillStatus._PAID_.equals(outInvoice.getStatus())
						|| BillStatus._CREDITED_.equals(outInvoice.getStatus()))) {
					it.remove();
				}
			}

			// check to see if there actually are some fuel events to export
			if (!outgoingInvoices.isEmpty()) {

				int nrOutgoingInvoicesExported = 0;
				int nrCreditMemosExported = 0;
				int nrOutgoingInvoicesExportedOk = 0;
				int nrCreditMemosExportedOk = 0;
				for (OutgoingInvoice invoice : outgoingInvoices) {
					boolean exported = true;
					try {
						exported = outgoingInvoiceService.exportToNAV(invoice);
					} catch (Exception e) {
						exported = false;
						LOGGER.error("ERROR:could not export OutgoingInvoice with ID " + invoice.getId(), e);
					}
					if (invoice.getInvoiceType().equals(InvoiceTypeAcc._INV_)) {
						nrOutgoingInvoicesExported++;
						if (exported) {
							nrOutgoingInvoicesExportedOk++;
						}
					} else if (invoice.getInvoiceType().equals(InvoiceTypeAcc._CRN_)) {
						nrCreditMemosExported++;
						if (exported) {
							nrCreditMemosExportedOk++;
						}
					}
				}

				param.setExportInvoiceResult((nrOutgoingInvoicesExportedOk + nrCreditMemosExportedOk) == outgoingInvoices.size());
				StringBuilder sb = new StringBuilder();
				if (exportInvoicesToNAVInterface.getEnabled()) {
					sb.append(String.format(BusinessErrorCode.INTERFACE_EXPORT_OUT_INVOICES_DETAILS_OUT_INVOICE.getErrMsg(),
							ExtInterfaceNames.EXPORT_INVOICES_TO_NAV.getValue(), nrOutgoingInvoicesExportedOk, nrOutgoingInvoicesExported,
							ExtInterfaceNames.EXPORT_INVOICES_TO_NAV.getValue()));
				} else {
					sb.append(String.format(BusinessErrorCode.INTERFACE_EXPORT_OUT_INVOICES_DISABLED.getErrMsg(),
							ExtInterfaceNames.EXPORT_INVOICES_TO_NAV.getValue()));
				}
				sb.append("<br/><br/>");
				if (exportCreditMemosToNAVInterface.getEnabled()) {
					sb.append(String.format(BusinessErrorCode.INTERFACE_EXPORT_OUT_INVOICES_DETAILS_CREDIT_MEMO.getErrMsg(),
							ExtInterfaceNames.EXPORT_CREDIT_MEMO_TO_NAV.getValue(), nrCreditMemosExportedOk, nrCreditMemosExported,
							ExtInterfaceNames.EXPORT_CREDIT_MEMO_TO_NAV.getValue()));
				} else {
					sb.append(String.format(BusinessErrorCode.INTERFACE_EXPORT_OUT_INVOICES_DISABLED.getErrMsg(),
							ExtInterfaceNames.EXPORT_CREDIT_MEMO_TO_NAV.getValue()));
				}
				param.setExportInvoiceDescription(sb.toString());

			} else {
				param.setExportInvoiceResult(true);
				param.setExportInvoiceDescription(BusinessErrorCode.INTERFACE_EXPORT_OUT_INVOICES_NO_INVOICES.getErrMsg());
			}
		} else {
			LOGGER.error("ERROR:could not find outgoing invoices/credit memos for " + outgoingInvoices + " items and option " + option + "!");
			param.setExportInvoiceResult(false);
			param.setExportInvoiceDescription(BusinessErrorCode.INTERFACE_EXPORT_OUT_INVOICES_INCORRECT_OPTION.getErrMsg());
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END exportFuelPlusToNAVInvoice()");
		}
	}

	/**
	 * @param outgoingInvoiceService
	 * @param dsList
	 * @return
	 */
	private List<OutgoingInvoice> getFuelEventsForSelectedOption(IOutgoingInvoiceService outgoingInvoiceService, List<OutgoingInvoice_Ds> dsList) {
		List<OutgoingInvoice> outgoingInvoices = null;
		if ((dsList != null) && !dsList.isEmpty()) {
			// get the selected ones from the DB
			List<Object> outgoingInvoiceIds = new ArrayList<>();
			for (OutgoingInvoice_Ds outgoingInvoice_Ds : dsList) {
				outgoingInvoiceIds.add(outgoingInvoice_Ds.getId());
			}
			outgoingInvoices = outgoingInvoiceService.findByIds(outgoingInvoiceIds);
		} else {
			outgoingInvoices = new ArrayList<>();
		}
		return outgoingInvoices;
	}

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void exportAsIataXML(OutgoingInvoice_Ds ds, OutgoingInvoice_DsParam params) throws Exception {
		System.setProperty("jdk.xml.maxOccurLimit", "200000");
		IIataInvoiceService iataSrv = this.getApplicationContext().getBean(IIataInvoiceService.class);
		IOutgoingInvoiceService invSrv = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		OutgoingInvoiceToXMLTransformer transformer = this.getApplicationContext().getBean(OutgoingInvoiceToXMLTransformer.class);
		OutgoingInvoice invoice = invSrv.findById(ds.getId());
		String fileName = UUID.randomUUID().toString().toUpperCase() + ".xml";
		String filePath = Session.user.get().getWorkspace().getTempPath() + File.separator + fileName;
		params.setIataXmlFileName(fileName);
		File file = new File(filePath);
		FileOutputStream fos = new FileOutputStream(file);
		try {
			InvoiceTransmission transmission = transformer.generateInvoiceTransmission(invoice, invoice.getIssuer().getCode(),
					invoice.getReceiver().getCode());
			iataSrv.generateIataXml(transmission, fos);
			fos.flush();
		} finally {
			try {
				fos.close();
			} catch (IOException ioe) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(ioe.getMessage(), ioe);
				}
			}
		}
	}

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void generateReport(OutgoingInvoice_Ds ds, OutgoingInvoice_DsParam params) throws Exception {
		IExternalReportService reportService = (IExternalReportService) this.findEntityService(ExternalReport.class);
		reportService.runReport(ds, params.getReportName(), "PDF", ds.getReceiverCode() + "_" + ds.getDelLocCode() + "_" + ds.getInvoiceNo());
	}

	public void exportToRicoh(OutgoingInvoice_Ds ds, OutgoingInvoice_DsParam params) throws Exception {
		this.exportToRicohBulk(Arrays.asList(ds), params);
	}

	public void exportToRicohBulk(List<OutgoingInvoice_Ds> dsList, OutgoingInvoice_DsParam param) throws Exception {
		IOutgoingInvoiceService outgoingInvoiceService = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);

		StringBuilder validationMessages = new StringBuilder();
		List<String> dbSyncValidation = new ArrayList<>();
		List<String> businessValidation = new ArrayList<>();
		List<String> invoiceTypeValidation = new ArrayList<>();
		List<String> workflowValidation = new ArrayList<>();

		for (OutgoingInvoice_Ds ds : dsList) {
			OutgoingInvoice invoice = outgoingInvoiceService.findById(ds.getId());
			if (!invoice.getControlNo().equals(ds.getControlNo())) {
				dbSyncValidation.add(invoice.getInvoiceNo());
				continue;
			}

			if (!this.canInvoiceBePrinted(invoice)) {
				businessValidation.add(invoice.getInvoiceNo());
				continue;
			}

			if (!this.corectInvoiceTypeForPrinting(invoice)) {
				invoiceTypeValidation.add(invoice.getInvoiceNo());
				continue;
			}

			WorkflowStartResult result = this.startUploadToRicohWorkflow(invoice);

			if (!result.isStarted()) {
				workflowValidation.add("Invoice " + invoice.getInvoiceNo() + ": " + result.getReason());
			}
		}

		if (!dbSyncValidation.isEmpty()) {
			validationMessages
					.append(String.format(AccErrorCode.INVOICE_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbSyncValidation.toArray())));
		}

		if (!businessValidation.isEmpty()) {
			validationMessages.append(
					String.format(AccErrorCode.EXPORT_INVOICE_TO_RICOH_FAILED.getErrMsg(), StringUtil.unorderedList(businessValidation.toArray())));
		}

		if (!invoiceTypeValidation.isEmpty()) {
			validationMessages.append(String.format(AccErrorCode.EXPORT_INVOICE_TO_RICOH_WRONG_INVOICE_TYPE.getErrMsg(),
					StringUtil.unorderedList(invoiceTypeValidation.toArray())));
		}

		if (!workflowValidation.isEmpty()) {
			validationMessages.append(String.format(AccErrorCode.UPLOAD_RICOH_INVOICE_WORKFLOW_NOT_START.getErrMsg(),
					StringUtil.unorderedList(workflowValidation.toArray())));
		}

		if (validationMessages.length() > 0) {
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS, validationMessages.toString());
		}
	}

	private boolean corectInvoiceTypeForPrinting(OutgoingInvoice invoice) throws Exception {
		boolean correctInvoiceType = false;
		if (invoice.getReferenceDocType().equals(InvoiceReferenceDocType._CONTRACT_)) {
			IContractService service = (IContractService) this.findEntityService(Contract.class);
			Contract contract = service.findById(invoice.getReferenceDocId());
			if (contract.getInvoiceType().equals(InvoiceType._FISCAL_) || contract.getInvoiceType().equals(InvoiceType._EXPORT_)
					|| contract.getInvoiceType().equals(InvoiceType._REGULAR_)) {
				correctInvoiceType = true;
			}
		} else {
			// TODO if conditions for delivery notes arise, modify here!
			correctInvoiceType = true;
		}
		return correctInvoiceType;
	}

	private boolean canInvoiceBePrinted(OutgoingInvoice entity) {
		boolean canBePrinted = true;
		for (OutgoingInvoiceLine il : entity.getInvoiceLines()) {
			if (il.getErpReference() == null || il.getErpReference().isEmpty()) {
				canBePrinted = false;
				break;
			}
		}
		return canBePrinted;
	}

	private WorkflowStartResult startUploadToRicohWorkflow(OutgoingInvoice invoice) throws BusinessException {
		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");

		Map<String, Object> vars = new HashMap<>();
		vars.put(WorkflowVariablesConstants.NOTIFICATION_DETAILS, true);
		vars.put(WorkflowVariablesConstants.NOTIFICATION_DETAILS_ENTITY_CODE, invoice.getInvoiceNo());
		vars.put(WorkflowVariablesConstants.NOTIFICATION_DETAILS_ENTITY_NAME, invoice.getClass().getSimpleName());

		vars.put(WorkflowVariablesConstants.INVOICE_ID_VAR, invoice.getId());
		vars.put(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME, Session.user.get().getCode());

		return workflowBpmManager.startWorkflow(WorkflowNames.UPLOAD_INVOICE_RICOH_XML.getWorkflowName(), vars, invoice.getSubsidiaryId(), invoice);
	}

}
