package atraxo.acc.presenter.ext.invoices.delegate;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.acc.presenter.ext.invoices.service.RejectCreditMemoService;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_Ds;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class RejectCreditMemo_Pd extends AbstractPresenterDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(RejectCreditMemo_Pd.class);

	public void reject(OutgoingInvoice_Ds ds, OutgoingInvoice_DsParam params) throws Exception {
		RejectCreditMemoService service = this.getApplicationContext().getBean(RejectCreditMemoService.class);
		service.reject(ds, params);
	}

	public void rejectList(List<OutgoingInvoice_Ds> dsList, OutgoingInvoice_DsParam params) throws Exception {
		RejectCreditMemoService service = this.getApplicationContext().getBean(RejectCreditMemoService.class);
		service.rejectList(dsList, params);
	}
}
