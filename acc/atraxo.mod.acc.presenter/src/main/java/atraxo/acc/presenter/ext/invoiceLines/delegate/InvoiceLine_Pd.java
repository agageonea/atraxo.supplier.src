package atraxo.acc.presenter.ext.invoiceLines.delegate;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;
import java.util.List;

import atraxo.acc.business.api.invoiceLines.IInvoiceLineService;
import atraxo.acc.business.api.invoices.IInvoiceService;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.presenter.impl.invoiceLines.model.InvoiceLine_Ds;
import atraxo.acc.presenter.impl.invoiceLines.model.InvoiceLine_DsParam;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.ext.contracts.CostVat;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.vat.IVatService;
import atraxo.fmbas.domain.impl.vat.Vat;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 * @author zspeter
 */
public class InvoiceLine_Pd extends AbstractPresenterDelegate {

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void calculateAmount(InvoiceLine_Ds ds, InvoiceLine_DsParam params) throws Exception {
		if (ds.getQuantity() == null) {
			return;
		}
		IInvoiceService invoiceService = (IInvoiceService) this.findEntityService(Invoice.class);
		Invoice invoice = invoiceService.findById(ds.getInvoiceId());
		if (invoice.getContract() == null) {
			return;
		}
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Date date = ds.getDeliveryDate() == null ? new Date() : ds.getDeliveryDate();
		BigDecimal price = contractService.getPrice(invoice.getContract().getId(), invoice.getUnit().getCode(), invoice.getCurrency().getCode(),
				date);

		BigDecimal amount = price.multiply(ds.getQuantity());
		ds.setAmount(amount);
		if (invoice.getCountry() == null) {
			return;
		}
		IVatService service = (IVatService) this.findEntityService(Vat.class);
		BigDecimal vatRate = service.getVat(ds.getDeliveryDate(), invoice.getCountry());
		BigDecimal totalVat = amount.multiply(vatRate.divide(new BigDecimal(100), MathContext.DECIMAL64));
		ds.setVat(totalVat);
		ds.setCalculatedVat(totalVat);

		BigDecimal amountBalance = params.getAmountBalance();
		BigDecimal oldAmount = ds.getAmount();

		if (oldAmount != null) {
			amountBalance = amountBalance.subtract(oldAmount, MathContext.DECIMAL64);
		}
		params.setAmountBalance(amountBalance.add(amount, MathContext.DECIMAL64));
	}

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void setContractPriceAndVat(InvoiceLine_Ds ds, InvoiceLine_DsParam params) throws Exception {

		IInvoiceService invoiceService = (IInvoiceService) this.findEntityService(Invoice.class);
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);

		Invoice invoice = invoiceService.findById(ds.getInvoiceId());
		if (invoice.getContract() == null) {
			return;
		}
		Date date = ds.getDeliveryDate() == null ? new Date() : ds.getDeliveryDate();
		CostVat priceVat = contractService.getPriceVat(invoice.getContract().getId(), invoice.getUnit().getCode(), invoice.getCurrency().getCode(),
				date, ds.getFlightType());
		ds.setContractPrice(priceVat.getCost());
		ds.setVatRate(priceVat.getVat());

	}

	/**
	 * @param invoiceLines
	 * @throws Exception
	 */
	public void autoBalance(List<InvoiceLine_Ds> invoiceLines) throws Exception {
		if (invoiceLines.isEmpty()) {
			return;
		}
		IInvoiceLineService invoiceLineService = (IInvoiceLineService) this.findEntityService(InvoiceLine.class);
		InvoiceLine il = invoiceLineService.findById(invoiceLines.get(0).getId());
		invoiceLineService.balanceInvoice(il);
	}
}
