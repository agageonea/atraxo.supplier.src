/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.deliveryNotes.model;

import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.IsSpot;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = DeliveryNoteContract.class)
@RefLookups({@RefLookup(refId = DeliveryNoteContract_Ds.F_CONTRACTID),
		@RefLookup(refId = DeliveryNoteContract_Ds.F_DELIVERYNOTEID)})
public class DeliveryNoteContract_Ds
		extends
			AbstractDs_Ds<DeliveryNoteContract> {

	public static final String ALIAS = "acc_DeliveryNoteContract_Ds";

	public static final String F_CONTRACTID = "contractId";
	public static final String F_CONTRACTCODE = "contractCode";
	public static final String F_CONTRATTYPE = "contratType";
	public static final String F_CONTRACTSUBTYPE = "contractSubType";
	public static final String F_CONTRACTSCOPE = "contractScope";
	public static final String F_CONTRACTISSPOT = "contractIsSpot";
	public static final String F_SETTCURRCODE = "settCurrCode";
	public static final String F_SETTUNITCODE = "settUnitCode";
	public static final String F_CONTRACTSUPPID = "contractSuppId";
	public static final String F_CONTRACTSUPPLIER = "contractSupplier";
	public static final String F_DELIVERYNOTEID = "deliveryNoteId";
	public static final String F_PAYABLECOST = "payableCost";
	public static final String F_DNFUELINGDATE = "dnFuelingDate";
	public static final String F_DNBILLABLECOST = "dnBillableCost";
	public static final String F_DNQUANTITY = "dnQuantity";
	public static final String F_DNUNITID = "dnUnitId";
	public static final String F_DNUNITCODE = "dnUnitCode";
	public static final String F_DNCURRID = "dnCurrId";
	public static final String F_DNCURR = "dnCurr";

	@DsField(join = "left", path = "contracts.id")
	private Integer contractId;

	@DsField(join = "left", path = "contracts.code")
	private String contractCode;

	@DsField(join = "left", path = "contracts.type")
	private ContractType contratType;

	@DsField(join = "left", path = "contracts.subType")
	private ContractSubType contractSubType;

	@DsField(join = "left", path = "contracts.scope")
	private ContractScope contractScope;

	@DsField(join = "left", path = "contracts.isSpot")
	private IsSpot contractIsSpot;

	@DsField(join = "left", path = "contracts.settlementCurr.code")
	private String settCurrCode;

	@DsField(join = "left", path = "contracts.settlementUnit.code")
	private String settUnitCode;

	@DsField(join = "left", path = "contracts.supplier.id")
	private Integer contractSuppId;

	@DsField(join = "left", path = "contracts.supplier.code")
	private String contractSupplier;

	@DsField(join = "left", path = "deliveryNotes.id")
	private Integer deliveryNoteId;

	@DsField(join = "left", path = "deliveryNotes.payableCost")
	private BigDecimal payableCost;

	@DsField(join = "left", path = "deliveryNotes.fuelingDate")
	private Date dnFuelingDate;

	@DsField(join = "left", path = "deliveryNotes.billableCost")
	private BigDecimal dnBillableCost;

	@DsField(join = "left", path = "deliveryNotes.quantity")
	private BigDecimal dnQuantity;

	@DsField(join = "left", path = "deliveryNotes.unit.id")
	private Integer dnUnitId;

	@DsField(join = "left", path = "deliveryNotes.unit.code")
	private String dnUnitCode;

	@DsField(join = "left", path = "deliveryNotes.payableCostCurrency.id")
	private Integer dnCurrId;

	@DsField(join = "left", path = "deliveryNotes.payableCostCurrency.code")
	private String dnCurr;

	/**
	 * Default constructor
	 */
	public DeliveryNoteContract_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public DeliveryNoteContract_Ds(DeliveryNoteContract e) {
		super(e);
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public ContractType getContratType() {
		return this.contratType;
	}

	public void setContratType(ContractType contratType) {
		this.contratType = contratType;
	}

	public ContractSubType getContractSubType() {
		return this.contractSubType;
	}

	public void setContractSubType(ContractSubType contractSubType) {
		this.contractSubType = contractSubType;
	}

	public ContractScope getContractScope() {
		return this.contractScope;
	}

	public void setContractScope(ContractScope contractScope) {
		this.contractScope = contractScope;
	}

	public IsSpot getContractIsSpot() {
		return this.contractIsSpot;
	}

	public void setContractIsSpot(IsSpot contractIsSpot) {
		this.contractIsSpot = contractIsSpot;
	}

	public String getSettCurrCode() {
		return this.settCurrCode;
	}

	public void setSettCurrCode(String settCurrCode) {
		this.settCurrCode = settCurrCode;
	}

	public String getSettUnitCode() {
		return this.settUnitCode;
	}

	public void setSettUnitCode(String settUnitCode) {
		this.settUnitCode = settUnitCode;
	}

	public Integer getContractSuppId() {
		return this.contractSuppId;
	}

	public void setContractSuppId(Integer contractSuppId) {
		this.contractSuppId = contractSuppId;
	}

	public String getContractSupplier() {
		return this.contractSupplier;
	}

	public void setContractSupplier(String contractSupplier) {
		this.contractSupplier = contractSupplier;
	}

	public Integer getDeliveryNoteId() {
		return this.deliveryNoteId;
	}

	public void setDeliveryNoteId(Integer deliveryNoteId) {
		this.deliveryNoteId = deliveryNoteId;
	}

	public BigDecimal getPayableCost() {
		return this.payableCost;
	}

	public void setPayableCost(BigDecimal payableCost) {
		this.payableCost = payableCost;
	}

	public Date getDnFuelingDate() {
		return this.dnFuelingDate;
	}

	public void setDnFuelingDate(Date dnFuelingDate) {
		this.dnFuelingDate = dnFuelingDate;
	}

	public BigDecimal getDnBillableCost() {
		return this.dnBillableCost;
	}

	public void setDnBillableCost(BigDecimal dnBillableCost) {
		this.dnBillableCost = dnBillableCost;
	}

	public BigDecimal getDnQuantity() {
		return this.dnQuantity;
	}

	public void setDnQuantity(BigDecimal dnQuantity) {
		this.dnQuantity = dnQuantity;
	}

	public Integer getDnUnitId() {
		return this.dnUnitId;
	}

	public void setDnUnitId(Integer dnUnitId) {
		this.dnUnitId = dnUnitId;
	}

	public String getDnUnitCode() {
		return this.dnUnitCode;
	}

	public void setDnUnitCode(String dnUnitCode) {
		this.dnUnitCode = dnUnitCode;
	}

	public Integer getDnCurrId() {
		return this.dnCurrId;
	}

	public void setDnCurrId(Integer dnCurrId) {
		this.dnCurrId = dnCurrId;
	}

	public String getDnCurr() {
		return this.dnCurr;
	}

	public void setDnCurr(String dnCurr) {
		this.dnCurr = dnCurr;
	}
}
