package atraxo.acc.presenter.ext.invoiceNumberSeries.delegate;

import atraxo.acc.presenter.impl.invoiceNumberSeries.model.InvoiceNumberSerie_Ds;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class UpdateSeriesStatus_Pd extends AbstractPresenterDelegate {

	public void enable(InvoiceNumberSerie_Ds ds) throws Exception {
		ds.setEnabled(true);
		this.findDsService(InvoiceNumberSerie_Ds.class).update(ds, null);
	}

	public void disable(InvoiceNumberSerie_Ds ds) throws Exception {
		ds.setEnabled(false);
		this.findDsService(InvoiceNumberSerie_Ds.class).update(ds, null);
	}

}
