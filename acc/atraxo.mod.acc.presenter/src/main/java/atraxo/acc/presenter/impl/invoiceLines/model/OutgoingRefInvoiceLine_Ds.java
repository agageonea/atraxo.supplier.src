/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoiceLines.model;

import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = OutgoingInvoiceLine.class)
@RefLookups({
		@RefLookup(refId = OutgoingRefInvoiceLine_Ds.F_FUELEVID),
		@RefLookup(refId = OutgoingRefInvoiceLine_Ds.F_OUTINVID),
		@RefLookup(refId = OutgoingRefInvoiceLine_Ds.F_REFINVID),
		@RefLookup(refId = OutgoingRefInvoiceLine_Ds.F_DESTID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingRefInvoiceLine_Ds.F_DESTCODE)}),
		@RefLookup(refId = OutgoingRefInvoiceLine_Ds.F_CUSTID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = OutgoingRefInvoiceLine_Ds.F_CUSTCODE)})})
public class OutgoingRefInvoiceLine_Ds
		extends
			AbstractDs_Ds<OutgoingInvoiceLine> {

	public static final String ALIAS = "acc_OutgoingRefInvoiceLine_Ds";

	public static final String F_FUELEVID = "fuelEvId";
	public static final String F_OUTINVID = "outInvId";
	public static final String F_REFINVID = "refInvId";
	public static final String F_CUSTID = "custId";
	public static final String F_CUSTCODE = "custCode";
	public static final String F_DESTID = "destId";
	public static final String F_DESTCODE = "destCode";
	public static final String F_INVLINEID = "invLineId";
	public static final String F_DELIVERYDATE = "deliveryDate";
	public static final String F_AIRCRAFTREGISTRATIONNUMBER = "aircraftRegistrationNumber";
	public static final String F_FLIGHTSUFFIX = "flightSuffix";
	public static final String F_FLIGHTTYPE = "flightType";
	public static final String F_FLIGHTNO = "flightNo";
	public static final String F_TICKETNO = "ticketNo";
	public static final String F_QUANTITY = "quantity";
	public static final String F_AMOUNT = "amount";
	public static final String F_VAT = "vat";
	public static final String F_ISCREDITED = "isCredited";

	@DsField(join = "left", path = "fuelEvent.id")
	private Integer fuelEvId;

	@DsField(join = "left", path = "outgoingInvoice.id")
	private Integer outInvId;

	@DsField(join = "left", path = "referenceInvItem.id")
	private Integer refInvId;

	@DsField(join = "left", path = "customer.id")
	private Integer custId;

	@DsField(join = "left", path = "customer.code")
	private String custCode;

	@DsField(join = "left", path = "destination.id")
	private Integer destId;

	@DsField(join = "left", path = "destination.code")
	private String destCode;

	@DsField(path = "id")
	private Integer invLineId;

	@DsField
	private Date deliveryDate;

	@DsField
	private String aircraftRegistrationNumber;

	@DsField
	private Suffix flightSuffix;

	@DsField
	private FlightTypeIndicator flightType;

	@DsField
	private String flightNo;

	@DsField
	private String ticketNo;

	@DsField
	private BigDecimal quantity;

	@DsField
	private BigDecimal amount;

	@DsField
	private BigDecimal vat;

	@DsField
	private Boolean isCredited;

	/**
	 * Default constructor
	 */
	public OutgoingRefInvoiceLine_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public OutgoingRefInvoiceLine_Ds(OutgoingInvoiceLine e) {
		super(e);
	}

	public Integer getFuelEvId() {
		return this.fuelEvId;
	}

	public void setFuelEvId(Integer fuelEvId) {
		this.fuelEvId = fuelEvId;
	}

	public Integer getOutInvId() {
		return this.outInvId;
	}

	public void setOutInvId(Integer outInvId) {
		this.outInvId = outInvId;
	}

	public Integer getRefInvId() {
		return this.refInvId;
	}

	public void setRefInvId(Integer refInvId) {
		this.refInvId = refInvId;
	}

	public Integer getCustId() {
		return this.custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public Integer getDestId() {
		return this.destId;
	}

	public void setDestId(Integer destId) {
		this.destId = destId;
	}

	public String getDestCode() {
		return this.destCode;
	}

	public void setDestCode(String destCode) {
		this.destCode = destCode;
	}

	public Integer getInvLineId() {
		return this.invLineId;
	}

	public void setInvLineId(Integer invLineId) {
		this.invLineId = invLineId;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getAircraftRegistrationNumber() {
		return this.aircraftRegistrationNumber;
	}

	public void setAircraftRegistrationNumber(String aircraftRegistrationNumber) {
		this.aircraftRegistrationNumber = aircraftRegistrationNumber;
	}

	public Suffix getFlightSuffix() {
		return this.flightSuffix;
	}

	public void setFlightSuffix(Suffix flightSuffix) {
		this.flightSuffix = flightSuffix;
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getTicketNo() {
		return this.ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getVat() {
		return this.vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public Boolean getIsCredited() {
		return this.isCredited;
	}

	public void setIsCredited(Boolean isCredited) {
		this.isCredited = isCredited;
	}
}
