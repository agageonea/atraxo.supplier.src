/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.fuelEvents.service;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.ext.fuelEvents.service.CostCalculatorService;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.presenter.impl.fuelEvents.model.FuelEventPurchaseOrder_Ds;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelEventPurchaseOrder_DsService extends AbstractEntityDsService<FuelEventPurchaseOrder_Ds, FuelEventPurchaseOrder_Ds, Object, FuelEvent>
		implements IDsService<FuelEventPurchaseOrder_Ds, FuelEventPurchaseOrder_Ds, Object> {

	@Autowired
	private CostCalculatorService calcSrv;
	@Autowired
	private IFuelOrderLocationService folService;
	@Autowired
	private ISystemParameterService paramSrv;
	@Autowired
	private IExchangeRateService exchSrv;
	@Autowired
	private ICurrenciesService curSrv;

	@Override
	protected void postFind(IQueryBuilder<FuelEventPurchaseOrder_Ds, FuelEventPurchaseOrder_Ds, Object> builder,
			List<FuelEventPurchaseOrder_Ds> result) throws Exception {
		super.postFind(builder, result);
		String sysCurrencyCode = this.paramSrv.getSysCurrency();
		Currencies toCurrency = this.curSrv.findByCode(sysCurrencyCode);
		for (FuelEventPurchaseOrder_Ds ds : result) {
			FuelOrderLocation fol = this.folService.findById(ds.getOrderLocationId());
			ds.setOrderCode(fol.getFuelOrder().getCode());
			ds.setEventType(fol.getFlightType());
			ds.setOperationalType(fol.getOperationalType().getName());
			ds.setProduct(fol.getProduct().getName());
			FuelEvent event = this.folService.findById(ds._getEntity_().getId(), FuelEvent.class);
			this.calculateOriginalAmount(ds, fol, event, toCurrency);
		}
	}

	private void calculateOriginalAmount(FuelEventPurchaseOrder_Ds ds, FuelOrderLocation fol, FuelEvent event, Currencies toCurrency)
			throws BusinessException {
		BigDecimal cost = ds.getBillableCost();
		BigDecimal sysCost = this.exchSrv
				.convertExchangeRate(fol.getPaymentCurrency(), toCurrency, GregorianCalendar.getInstance().getTime(), cost, false).getValue();
		ds.setConvertedAmount(sysCost);
		ds.setSystemCurrency(toCurrency.getCode());
	}

}
