/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.fuelEventsDetails.model;

import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelEventsDetails.class, sort = {@SortField(field = FuelEventsDetails_Ds.F_CONTRACTCODE, desc = true)})
@RefLookups({
		@RefLookup(refId = FuelEventsDetails_Ds.F_MAINCATEGORYID, namedQuery = MainCategory.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEventsDetails_Ds.F_MAINCATEGORYCODE)}),
		@RefLookup(refId = FuelEventsDetails_Ds.F_ORIGINALPRICECURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEventsDetails_Ds.F_ORIGINALPRICECURRENCYCODE)}),
		@RefLookup(refId = FuelEventsDetails_Ds.F_ORIGINALPRICEUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelEventsDetails_Ds.F_ORIGINALPRICEUNITCODE)}),
		@RefLookup(refId = FuelEventsDetails_Ds.F_PRICECATEGORYID),
		@RefLookup(refId = FuelEventsDetails_Ds.F_FUELEVENTID)})
public class FuelEventsDetails_Ds extends AbstractDs_Ds<FuelEventsDetails> {

	public static final String ALIAS = "acc_FuelEventsDetails_Ds";

	public static final String F_FUELEVENTID = "fuelEventId";
	public static final String F_MAINCATEGORYID = "mainCategoryId";
	public static final String F_MAINCATEGORYCODE = "mainCategoryCode";
	public static final String F_MAINCATEGORYNAME = "mainCategoryName";
	public static final String F_PRICECATEGORYID = "priceCategoryId";
	public static final String F_PRICECATEGORYNAME = "priceCategoryName";
	public static final String F_ORIGINALPRICECURRENCYID = "originalPriceCurrencyId";
	public static final String F_ORIGINALPRICECURRENCYCODE = "originalPriceCurrencyCode";
	public static final String F_ORIGINALPRICECURRENCYNAME = "originalPriceCurrencyName";
	public static final String F_ORIGINALPRICEUNITID = "originalPriceUnitId";
	public static final String F_ORIGINALPRICEUNITCODE = "originalPriceUnitCode";
	public static final String F_ORIGINALPRICEUNITNAME = "originalPriceUnitName";
	public static final String F_CONTRACTCODE = "contractCode";
	public static final String F_PRICENAME = "priceName";
	public static final String F_ORIGINALPRICE = "originalPrice";
	public static final String F_SETTLEMENTPRICE = "settlementPrice";
	public static final String F_EXCHANGERATE = "exchangeRate";
	public static final String F_VATAMOUNT = "vatAmount";
	public static final String F_CONTRACTAMOUNT = "contractAmount";

	@DsField(join = "left", path = "fuelEvent.id")
	private Integer fuelEventId;

	@DsField(join = "left", path = "mainCategory.id")
	private Integer mainCategoryId;

	@DsField(join = "left", path = "mainCategory.code")
	private String mainCategoryCode;

	@DsField(join = "left", path = "mainCategory.name")
	private String mainCategoryName;

	@DsField(join = "left", path = "priceCategory.id")
	private Integer priceCategoryId;

	@DsField(join = "left", path = "priceCategory.name")
	private String priceCategoryName;

	@DsField(join = "left", path = "originalPriceCurrency.id")
	private Integer originalPriceCurrencyId;

	@DsField(join = "left", path = "originalPriceCurrency.code")
	private String originalPriceCurrencyCode;

	@DsField(join = "left", path = "originalPriceCurrency.name")
	private String originalPriceCurrencyName;

	@DsField(join = "left", path = "originalPriceUnit.id")
	private Integer originalPriceUnitId;

	@DsField(join = "left", path = "originalPriceUnit.code")
	private String originalPriceUnitCode;

	@DsField(join = "left", path = "originalPriceUnit.name")
	private String originalPriceUnitName;

	@DsField
	private String contractCode;

	@DsField
	private String priceName;

	@DsField
	private BigDecimal originalPrice;

	@DsField
	private BigDecimal settlementPrice;

	@DsField
	private BigDecimal exchangeRate;

	@DsField
	private BigDecimal vatAmount;

	@DsField(path = "settlementAmount")
	private BigDecimal contractAmount;

	/**
	 * Default constructor
	 */
	public FuelEventsDetails_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelEventsDetails_Ds(FuelEventsDetails e) {
		super(e);
	}

	public Integer getFuelEventId() {
		return this.fuelEventId;
	}

	public void setFuelEventId(Integer fuelEventId) {
		this.fuelEventId = fuelEventId;
	}

	public Integer getMainCategoryId() {
		return this.mainCategoryId;
	}

	public void setMainCategoryId(Integer mainCategoryId) {
		this.mainCategoryId = mainCategoryId;
	}

	public String getMainCategoryCode() {
		return this.mainCategoryCode;
	}

	public void setMainCategoryCode(String mainCategoryCode) {
		this.mainCategoryCode = mainCategoryCode;
	}

	public String getMainCategoryName() {
		return this.mainCategoryName;
	}

	public void setMainCategoryName(String mainCategoryName) {
		this.mainCategoryName = mainCategoryName;
	}

	public Integer getPriceCategoryId() {
		return this.priceCategoryId;
	}

	public void setPriceCategoryId(Integer priceCategoryId) {
		this.priceCategoryId = priceCategoryId;
	}

	public String getPriceCategoryName() {
		return this.priceCategoryName;
	}

	public void setPriceCategoryName(String priceCategoryName) {
		this.priceCategoryName = priceCategoryName;
	}

	public Integer getOriginalPriceCurrencyId() {
		return this.originalPriceCurrencyId;
	}

	public void setOriginalPriceCurrencyId(Integer originalPriceCurrencyId) {
		this.originalPriceCurrencyId = originalPriceCurrencyId;
	}

	public String getOriginalPriceCurrencyCode() {
		return this.originalPriceCurrencyCode;
	}

	public void setOriginalPriceCurrencyCode(String originalPriceCurrencyCode) {
		this.originalPriceCurrencyCode = originalPriceCurrencyCode;
	}

	public String getOriginalPriceCurrencyName() {
		return this.originalPriceCurrencyName;
	}

	public void setOriginalPriceCurrencyName(String originalPriceCurrencyName) {
		this.originalPriceCurrencyName = originalPriceCurrencyName;
	}

	public Integer getOriginalPriceUnitId() {
		return this.originalPriceUnitId;
	}

	public void setOriginalPriceUnitId(Integer originalPriceUnitId) {
		this.originalPriceUnitId = originalPriceUnitId;
	}

	public String getOriginalPriceUnitCode() {
		return this.originalPriceUnitCode;
	}

	public void setOriginalPriceUnitCode(String originalPriceUnitCode) {
		this.originalPriceUnitCode = originalPriceUnitCode;
	}

	public String getOriginalPriceUnitName() {
		return this.originalPriceUnitName;
	}

	public void setOriginalPriceUnitName(String originalPriceUnitName) {
		this.originalPriceUnitName = originalPriceUnitName;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public String getPriceName() {
		return this.priceName;
	}

	public void setPriceName(String priceName) {
		this.priceName = priceName;
	}

	public BigDecimal getOriginalPrice() {
		return this.originalPrice;
	}

	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}

	public BigDecimal getSettlementPrice() {
		return this.settlementPrice;
	}

	public void setSettlementPrice(BigDecimal settlementPrice) {
		this.settlementPrice = settlementPrice;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getContractAmount() {
		return this.contractAmount;
	}

	public void setContractAmount(BigDecimal contractAmount) {
		this.contractAmount = contractAmount;
	}
}
