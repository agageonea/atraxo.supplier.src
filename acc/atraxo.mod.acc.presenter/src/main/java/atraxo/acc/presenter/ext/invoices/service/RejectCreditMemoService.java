/**
 *
 */
package atraxo.acc.presenter.ext.invoices.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_Ds;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_DsParam;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class RejectCreditMemoService extends AbstractCreditMemoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RejectCreditMemoService.class);

	@Autowired
	private ISystemParameterService paramSrv;

	/**
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void reject(OutgoingInvoice_Ds ds, OutgoingInvoice_DsParam params) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START reject()");
		}
		this.completeCurrentTask(ds.getId(), false, params.getApprovalNote());

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END reject()");
		}
	}

	public void rejectList(List<OutgoingInvoice_Ds> dsList, OutgoingInvoice_DsParam params) throws Exception {
		boolean useWorkflow = this.paramSrv.getCreditMemoApprovalWorkflow();
		if (!useWorkflow) {
			params.setApproveRejectResult(AccErrorCode.CREDIT_MEMO_APPROVAL_WORKFLOW_NO_SYS_PARAM_APPROVE_REJECT.getErrMsg());
			return;
		}

		List<String> failedToProcessInvoices = new ArrayList<>();
		for (OutgoingInvoice_Ds oiDs : dsList) {
			try {
				this.reject(oiDs, params);
			} catch (Exception e) {
				failedToProcessInvoices.add(oiDs.getInvoiceNo() + ":" + e.getMessage());
				LOGGER.error("Error", e);
			}
		}
		params.setApproveRejectResult(this.generateProcessingResultResponse(failedToProcessInvoices, dsList.size()));
	}

	private String generateProcessingResultResponse(List<String> failedToProcessInvoices, int totalProcessingAttempts) {
		int successfulProcesings = totalProcessingAttempts - failedToProcessInvoices.size();
		StringBuilder resultMessageBuilder = new StringBuilder();
		resultMessageBuilder
				.append(String.format(AccErrorCode.CREDIT_MEMO_REJECT_TOTAL_PROCESSING.getErrMsg(), successfulProcesings, totalProcessingAttempts));
		if (!failedToProcessInvoices.isEmpty()) {
			resultMessageBuilder.append(AccErrorCode.CREDIT_MEMO_REJECT_PARTIAL_PROCESSING.getErrMsg());
			resultMessageBuilder.append(UL_START);
			for (String failedInvoice : failedToProcessInvoices) {
				resultMessageBuilder.append(LI_START).append(failedInvoice).append(LI_END);
			}
			resultMessageBuilder.append(UL_END);
		}

		return resultMessageBuilder.toString();
	}

}
