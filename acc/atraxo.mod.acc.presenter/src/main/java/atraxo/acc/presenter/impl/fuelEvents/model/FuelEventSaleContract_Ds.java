/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.fuelEvents.model;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelEvent.class, jpqlWhere = "e.billPriceSourceType = 'Contract'")
public class FuelEventSaleContract_Ds
		extends
			AbstractSubsidiaryDs_Ds<FuelEvent> {

	public static final String ALIAS = "acc_FuelEventSaleContract_Ds";

	public static final String F_CONTRACTCODE = "contractCode";
	public static final String F_TYPE = "type";
	public static final String F_DELIVERY = "delivery";
	public static final String F_SCOPE = "scope";
	public static final String F_TERMSPOT = "termSpot";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_BILLABLECOST = "billableCost";
	public static final String F_BILLABELCOSTCURRENCY = "billabelCostCurrency";
	public static final String F_CONVERTEDAMOUNT = "convertedAmount";
	public static final String F_SYSTEMCURRENCY = "systemCurrency";
	public static final String F_CONTRACTID = "contractId";
	public static final String F_SOURCETYPE = "sourceType";
	public static final String F_FUELINGDATE = "fuelingDate";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_QUANTITY = "quantity";

	@DsField(fetch = false)
	private String contractCode;

	@DsField(fetch = false)
	private String type;

	@DsField(fetch = false)
	private String delivery;

	@DsField(fetch = false)
	private String scope;

	@DsField(fetch = false)
	private String termSpot;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField
	private BigDecimal billableCost;

	@DsField(join = "left", path = "billableCostCurrency.code")
	private String billabelCostCurrency;

	@DsField(fetch = false)
	private BigDecimal convertedAmount;

	@DsField(fetch = false)
	private String systemCurrency;

	@DsField(path = "billPriceSourceId")
	private Integer contractId;

	@DsField(path = "billPriceSourceType")
	private String sourceType;

	@DsField
	private Date fuelingDate;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField
	private BigDecimal quantity;

	/**
	 * Default constructor
	 */
	public FuelEventSaleContract_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelEventSaleContract_Ds(FuelEvent e) {
		super(e);
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDelivery() {
		return this.delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getTermSpot() {
		return this.termSpot;
	}

	public void setTermSpot(String termSpot) {
		this.termSpot = termSpot;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public BigDecimal getBillableCost() {
		return this.billableCost;
	}

	public void setBillableCost(BigDecimal billableCost) {
		this.billableCost = billableCost;
	}

	public String getBillabelCostCurrency() {
		return this.billabelCostCurrency;
	}

	public void setBillabelCostCurrency(String billabelCostCurrency) {
		this.billabelCostCurrency = billabelCostCurrency;
	}

	public BigDecimal getConvertedAmount() {
		return this.convertedAmount;
	}

	public void setConvertedAmount(BigDecimal convertedAmount) {
		this.convertedAmount = convertedAmount;
	}

	public String getSystemCurrency() {
		return this.systemCurrency;
	}

	public void setSystemCurrency(String systemCurrency) {
		this.systemCurrency = systemCurrency;
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public String getSourceType() {
		return this.sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public Date getFuelingDate() {
		return this.fuelingDate;
	}

	public void setFuelingDate(Date fuelingDate) {
		this.fuelingDate = fuelingDate;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
}
