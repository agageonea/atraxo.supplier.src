/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.ext.deliveryNotes.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.presenter.impl.deliveryNotes.model.DeliveryNoteContract_Ds;
import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class DeliveryNoteContract_DsService
		extends AbstractEntityDsService<DeliveryNoteContract_Ds, DeliveryNoteContract_Ds, Object, DeliveryNoteContract>
		implements IDsService<DeliveryNoteContract_Ds, DeliveryNoteContract_Ds, Object> {

	@Autowired
	private ISystemParameterService sysParamService;
	@Autowired
	private ICurrenciesService currencyService;
	@Autowired
	private IFinancialSourcesService finSrcSrv;
	@Autowired
	IAverageMethodService avgMthSrv;
	@Autowired
	private IUnitService unitSrv;
	@Autowired
	private IFuelEventService feSrv;

	@Override
	protected void postFind(IQueryBuilder<DeliveryNoteContract_Ds, DeliveryNoteContract_Ds, Object> builder, List<DeliveryNoteContract_Ds> result)
			throws Exception {
		super.postFind(builder, result);
		String sysCurrCode = this.sysParamService.getSysCurrency();
		Currencies sysCurrency = this.currencyService.findByCode(sysCurrCode);
		PriceConverterService priceConverterService = this.getApplicationContext().getBean(PriceConverterService.class);
		FinancialSources stdFinSrc = this.getStdFinancialSource();
		AverageMethod stdAvgMth = this.avgMthSrv.findByName(this.sysParamService.getSysAverageMethod());

		Unit sysVol = this.unitSrv.findByCode(this.sysParamService.getSysVol());
		Unit sysUnit = this.unitSrv.findByCode(this.sysParamService.getSysWeight());
		Density density = new Density(sysUnit, sysVol, Double.parseDouble(this.sysParamService.getDensity()));

		for (DeliveryNoteContract_Ds dnc : result) {
			DeliveryNote dn = dnc._getEntity_().getDeliveryNotes();
			List<FuelEvent> list = this.feSrv.findByDeliveryNotes(dn);
			if (list.isEmpty()) {
				continue;
			}

			FuelEvent event = list.get(0);

			density = this.getDensity(density, event);
			BigDecimal convertedPrice = priceConverterService.convert(event.getUnit(), event.getUnit(), event.getPayableCostCurrency(), sysCurrency,
					event.getPayableCost(), event.getFuelingDate(), stdFinSrc, stdAvgMth, density, false, MasterAgreementsPeriod._CURRENT_)
					.getValue();
			dnc.setPayableCost(convertedPrice);
			dnc.setDnCurr(sysCurrCode);
			dnc.setDnCurrId(sysCurrency.getId());
		}
	}

	public FinancialSources getStdFinancialSource() {
		Map<String, Object> params = new HashMap<>();
		params.put("isStdFlg", true);
		params.put("active", true);
		return this.finSrcSrv.findEntityByAttributes(params);
	}

	private Density getDensity(Density density, FuelEvent fe) {
		if (fe.getDensity() != null) {
			return new Density(fe.getDensityUnit(), fe.getDensityVolume(), fe.getDensity().doubleValue());
		}
		return density;
	}
}
