/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoiceLines.model;

import java.math.BigDecimal;
/**
 * Generated code. Do not modify in this file.
 */
public class InvoiceLine_DsParam {

	public static final String f_amountBalance = "amountBalance";
	public static final String f_quantityBalance = "quantityBalance";
	public static final String f_origAmountBalance = "origAmountBalance";
	public static final String f_origQuantityBalance = "origQuantityBalance";

	private BigDecimal amountBalance;

	private BigDecimal quantityBalance;

	private BigDecimal origAmountBalance;

	private BigDecimal origQuantityBalance;

	public BigDecimal getAmountBalance() {
		return this.amountBalance;
	}

	public void setAmountBalance(BigDecimal amountBalance) {
		this.amountBalance = amountBalance;
	}

	public BigDecimal getQuantityBalance() {
		return this.quantityBalance;
	}

	public void setQuantityBalance(BigDecimal quantityBalance) {
		this.quantityBalance = quantityBalance;
	}

	public BigDecimal getOrigAmountBalance() {
		return this.origAmountBalance;
	}

	public void setOrigAmountBalance(BigDecimal origAmountBalance) {
		this.origAmountBalance = origAmountBalance;
	}

	public BigDecimal getOrigQuantityBalance() {
		return this.origQuantityBalance;
	}

	public void setOrigQuantityBalance(BigDecimal origQuantityBalance) {
		this.origQuantityBalance = origQuantityBalance;
	}
}
