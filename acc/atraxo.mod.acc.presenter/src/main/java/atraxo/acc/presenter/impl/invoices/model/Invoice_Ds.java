/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoices.model;

import atraxo.acc.domain.impl.acc_type.InvoiceCategory;
import atraxo.acc.domain.impl.acc_type.InvoiceFormAcc;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceType;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Invoice.class, sort = {@SortField(field = Invoice_Ds.F_CREATEDAT, desc = true)})
@RefLookups({
		@RefLookup(refId = Invoice_Ds.F_RECEIVERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Invoice_Ds.F_RECEIVERCODE)}),
		@RefLookup(refId = Invoice_Ds.F_DELLOCID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Invoice_Ds.F_DELLOCCODE)}),
		@RefLookup(refId = Invoice_Ds.F_COUNTRYID, namedQuery = Country.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Invoice_Ds.F_COUNTRYCODE)}),
		@RefLookup(refId = Invoice_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Invoice_Ds.F_UNITCODE)}),
		@RefLookup(refId = Invoice_Ds.F_CONTRACTID, namedQuery = Contract.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Invoice_Ds.F_CONTRACTCODE)}),
		@RefLookup(refId = Invoice_Ds.F_CURRID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Invoice_Ds.F_CURRCODE)}),
		@RefLookup(refId = Invoice_Ds.F_ISSUERID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = Invoice_Ds.F_ISSUERCODE)})})
public class Invoice_Ds extends AbstractSubsidiaryDs_Ds<Invoice> {

	public static final String ALIAS = "acc_Invoice_Ds";

	public static final String F_RECEIVERID = "receiverId";
	public static final String F_RECEIVERCODE = "receiverCode";
	public static final String F_DELLOCID = "delLocId";
	public static final String F_DELLOCCODE = "delLocCode";
	public static final String F_DUMMYFIELD = "dummyField";
	public static final String F_COUNTRYID = "countryId";
	public static final String F_COUNTRYCODE = "countryCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CONTRACTID = "contractId";
	public static final String F_CONTRACTCODE = "contractCode";
	public static final String F_CURRID = "currId";
	public static final String F_CURRCODE = "currCode";
	public static final String F_ISSUERID = "issuerId";
	public static final String F_ISSUERCODE = "issuerCode";
	public static final String F_DEALTYPE = "dealtype";
	public static final String F_INVOICETYPE = "invoiceType";
	public static final String F_INVOICENO = "invoiceNo";
	public static final String F_ISSUEDATE = "issueDate";
	public static final String F_RECEIVINGDATE = "receivingDate";
	public static final String F_DELIVERYDATEFROM = "deliveryDateFrom";
	public static final String F_DELIVERYDATETO = "deliverydateTo";
	public static final String F_TRANSACTIONTYPE = "transactionType";
	public static final String F_INVOICEFORM = "invoiceForm";
	public static final String F_CATEGORY = "category";
	public static final String F_REFERENCETYPE = "referenceType";
	public static final String F_REFERENCEVALUE = "referenceValue";
	public static final String F_TOTALAMOUNT = "totalAmount";
	public static final String F_VATAMOUNT = "vatAmount";
	public static final String F_NETAMOUNT = "netAmount";
	public static final String F_QUANTITY = "quantity";
	public static final String F_ISSUERBASELINEDATE = "issuerBaselinedate";
	public static final String F_BASELINEDATE = "baseLineDate";
	public static final String F_CALCULATEDBASELINEDATE = "calculatedBaseLineDate";
	public static final String F_ITEMSNUMBER = "itemsNumber";
	public static final String F_CLOSINGDATE = "closingDate";
	public static final String F_TAXTYPE = "taxType";
	public static final String F_STATUS = "status";
	public static final String F_ERPREFERENCE = "erpReference";
	public static final String F_CHECKRESULT = "checkResult";
	public static final String F_DUEDAYS = "dueDays";
	public static final String F_VATTAXABLEAMOUNT = "vatTaxableAmount";
	public static final String F_VATRATE = "vatRate";
	public static final String F_PROCENT = "procent";
	public static final String F_TYPE = "type";
	public static final String F_SCOPE = "scope";

	@DsField(join = "left", path = "receiver.id")
	private Integer receiverId;

	@DsField(join = "left", path = "receiver.code")
	private String receiverCode;

	@DsField(join = "left", path = "deliveryLoc.id")
	private Integer delLocId;

	@DsField(join = "left", path = "deliveryLoc.code")
	private String delLocCode;

	@DsField(fetch = false)
	private String dummyField;

	@DsField(join = "left", path = "country.id")
	private Integer countryId;

	@DsField(join = "left", path = "country.code")
	private String countryCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "contract.id")
	private Integer contractId;

	@DsField(join = "left", path = "contract.code")
	private String contractCode;

	@DsField(join = "left", path = "currency.id")
	private Integer currId;

	@DsField(join = "left", path = "currency.code")
	private String currCode;

	@DsField(join = "left", path = "issuer.id")
	private Integer issuerId;

	@DsField(join = "left", path = "issuer.code")
	private String issuerCode;

	@DsField(path = "dealType")
	private DealType dealtype;

	@DsField
	private InvoiceTypeAcc invoiceType;

	@DsField
	private String invoiceNo;

	@DsField
	private Date issueDate;

	@DsField
	private Date receivingDate;

	@DsField
	private Date deliveryDateFrom;

	@DsField
	private Date deliverydateTo;

	@DsField
	private TransactionType transactionType;

	@DsField
	private InvoiceFormAcc invoiceForm;

	@DsField
	private InvoiceCategory category;

	@DsField
	private InvoiceReferenceType referenceType;

	@DsField
	private String referenceValue;

	@DsField
	private BigDecimal totalAmount;

	@DsField
	private BigDecimal vatAmount;

	@DsField
	private BigDecimal netAmount;

	@DsField
	private BigDecimal quantity;

	@DsField
	private Date issuerBaselinedate;

	@DsField
	private Date baseLineDate;

	@DsField
	private Date calculatedBaseLineDate;

	@DsField
	private Integer itemsNumber;

	@DsField
	private Date closingDate;

	@DsField
	private TaxType taxType;

	@DsField
	private BillStatus status;

	@DsField
	private String erpReference;

	@DsField
	private Boolean checkResult;

	@DsField(fetch = false)
	private Integer dueDays;

	@DsField(fetch = false)
	private BigDecimal vatTaxableAmount;

	@DsField(fetch = false)
	private BigDecimal vatRate;

	@DsField(fetch = false)
	private String procent;

	@DsField(fetch = false)
	private String type;

	@DsField(fetch = false)
	private String scope;

	/**
	 * Default constructor
	 */
	public Invoice_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public Invoice_Ds(Invoice e) {
		super(e);
	}

	public Integer getReceiverId() {
		return this.receiverId;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	public String getReceiverCode() {
		return this.receiverCode;
	}

	public void setReceiverCode(String receiverCode) {
		this.receiverCode = receiverCode;
	}

	public Integer getDelLocId() {
		return this.delLocId;
	}

	public void setDelLocId(Integer delLocId) {
		this.delLocId = delLocId;
	}

	public String getDelLocCode() {
		return this.delLocCode;
	}

	public void setDelLocCode(String delLocCode) {
		this.delLocCode = delLocCode;
	}

	public String getDummyField() {
		return this.dummyField;
	}

	public void setDummyField(String dummyField) {
		this.dummyField = dummyField;
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public Integer getCurrId() {
		return this.currId;
	}

	public void setCurrId(Integer currId) {
		this.currId = currId;
	}

	public String getCurrCode() {
		return this.currCode;
	}

	public void setCurrCode(String currCode) {
		this.currCode = currCode;
	}

	public Integer getIssuerId() {
		return this.issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public String getIssuerCode() {
		return this.issuerCode;
	}

	public void setIssuerCode(String issuerCode) {
		this.issuerCode = issuerCode;
	}

	public DealType getDealtype() {
		return this.dealtype;
	}

	public void setDealtype(DealType dealtype) {
		this.dealtype = dealtype;
	}

	public InvoiceTypeAcc getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceTypeAcc invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getIssueDate() {
		return this.issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getReceivingDate() {
		return this.receivingDate;
	}

	public void setReceivingDate(Date receivingDate) {
		this.receivingDate = receivingDate;
	}

	public Date getDeliveryDateFrom() {
		return this.deliveryDateFrom;
	}

	public void setDeliveryDateFrom(Date deliveryDateFrom) {
		this.deliveryDateFrom = deliveryDateFrom;
	}

	public Date getDeliverydateTo() {
		return this.deliverydateTo;
	}

	public void setDeliverydateTo(Date deliverydateTo) {
		this.deliverydateTo = deliverydateTo;
	}

	public TransactionType getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public InvoiceFormAcc getInvoiceForm() {
		return this.invoiceForm;
	}

	public void setInvoiceForm(InvoiceFormAcc invoiceForm) {
		this.invoiceForm = invoiceForm;
	}

	public InvoiceCategory getCategory() {
		return this.category;
	}

	public void setCategory(InvoiceCategory category) {
		this.category = category;
	}

	public InvoiceReferenceType getReferenceType() {
		return this.referenceType;
	}

	public void setReferenceType(InvoiceReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceValue() {
		return this.referenceValue;
	}

	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}

	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Date getIssuerBaselinedate() {
		return this.issuerBaselinedate;
	}

	public void setIssuerBaselinedate(Date issuerBaselinedate) {
		this.issuerBaselinedate = issuerBaselinedate;
	}

	public Date getBaseLineDate() {
		return this.baseLineDate;
	}

	public void setBaseLineDate(Date baseLineDate) {
		this.baseLineDate = baseLineDate;
	}

	public Date getCalculatedBaseLineDate() {
		return this.calculatedBaseLineDate;
	}

	public void setCalculatedBaseLineDate(Date calculatedBaseLineDate) {
		this.calculatedBaseLineDate = calculatedBaseLineDate;
	}

	public Integer getItemsNumber() {
		return this.itemsNumber;
	}

	public void setItemsNumber(Integer itemsNumber) {
		this.itemsNumber = itemsNumber;
	}

	public Date getClosingDate() {
		return this.closingDate;
	}

	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	public TaxType getTaxType() {
		return this.taxType;
	}

	public void setTaxType(TaxType taxType) {
		this.taxType = taxType;
	}

	public BillStatus getStatus() {
		return this.status;
	}

	public void setStatus(BillStatus status) {
		this.status = status;
	}

	public String getErpReference() {
		return this.erpReference;
	}

	public void setErpReference(String erpReference) {
		this.erpReference = erpReference;
	}

	public Boolean getCheckResult() {
		return this.checkResult;
	}

	public void setCheckResult(Boolean checkResult) {
		this.checkResult = checkResult;
	}

	public Integer getDueDays() {
		return this.dueDays;
	}

	public void setDueDays(Integer dueDays) {
		this.dueDays = dueDays;
	}

	public BigDecimal getVatTaxableAmount() {
		return this.vatTaxableAmount;
	}

	public void setVatTaxableAmount(BigDecimal vatTaxableAmount) {
		this.vatTaxableAmount = vatTaxableAmount;
	}

	public BigDecimal getVatRate() {
		return this.vatRate;
	}

	public void setVatRate(BigDecimal vatRate) {
		this.vatRate = vatRate;
	}

	public String getProcent() {
		return this.procent;
	}

	public void setProcent(String procent) {
		this.procent = procent;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}
}
