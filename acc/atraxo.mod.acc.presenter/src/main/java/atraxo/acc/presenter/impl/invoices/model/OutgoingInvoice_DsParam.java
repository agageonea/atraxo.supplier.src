/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.presenter.impl.invoices.model;

import atraxo.acc.domain.impl.acc_type.ExportTypeInvoice;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import java.util.Date;
/**
 * Generated code. Do not modify in this file.
 */
public class OutgoingInvoice_DsParam {

	public static final String f_dateFrom = "dateFrom";
	public static final String f_dateTo = "dateTo";
	public static final String f_closingDate = "closingDate";
	public static final String f_invoiceGenerationDate = "invoiceGenerationDate";
	public static final String f_transactionTypeParam = "transactionTypeParam";
	public static final String f_receiverIdParam = "receiverIdParam";
	public static final String f_deliveryLocIdParam = "deliveryLocIdParam";
	public static final String f_deliveryAreaIdParam = "deliveryAreaIdParam";
	public static final String f_nrGeneratedInvoices = "nrGeneratedInvoices";
	public static final String f_invGeneratorErrorMsg = "invGeneratorErrorMsg";
	public static final String f_separatePerShipTo = "separatePerShipTo";
	public static final String f_exportInvoiceDescription = "exportInvoiceDescription";
	public static final String f_exportInvoiceResult = "exportInvoiceResult";
	public static final String f_submitForApprovalDescription = "submitForApprovalDescription";
	public static final String f_submitForApprovalResult = "submitForApprovalResult";
	public static final String f_exportInvoiceOption = "exportInvoiceOption";
	public static final String f_iataXmlFileName = "iataXmlFileName";
	public static final String f_exportType = "exportType";
	public static final String f_reportName = "reportName";
	public static final String f_reportCode = "reportCode";
	public static final String f_approvalNote = "approvalNote";
	public static final String f_approveRejectResult = "approveRejectResult";
	public static final String f_selectedAttachments = "selectedAttachments";
	public static final String f_selectedInvoices = "selectedInvoices";
	public static final String f_selectedAllInvoices = "selectedAllInvoices";
	public static final String f_unselectedInvoices = "unselectedInvoices";
	public static final String f_filter = "filter";
	public static final String f_standardFilter = "standardFilter";

	private Date dateFrom;

	private Date dateTo;

	private Date closingDate;

	private Date invoiceGenerationDate;

	private TransactionType transactionTypeParam;

	private Integer receiverIdParam;

	private Integer deliveryLocIdParam;

	private Integer deliveryAreaIdParam;

	private Integer nrGeneratedInvoices;

	private String invGeneratorErrorMsg;

	private Boolean separatePerShipTo;

	private String exportInvoiceDescription;

	private Boolean exportInvoiceResult;

	private String submitForApprovalDescription;

	private Boolean submitForApprovalResult;

	private String exportInvoiceOption;

	private String iataXmlFileName;

	private ExportTypeInvoice exportType;

	private String reportName;

	private String reportCode;

	private String approvalNote;

	private String approveRejectResult;

	private String selectedAttachments;

	private String selectedInvoices;

	private Boolean selectedAllInvoices;

	private String unselectedInvoices;

	private String filter;

	private String standardFilter;

	public Date getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return this.dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Date getClosingDate() {
		return this.closingDate;
	}

	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	public Date getInvoiceGenerationDate() {
		return this.invoiceGenerationDate;
	}

	public void setInvoiceGenerationDate(Date invoiceGenerationDate) {
		this.invoiceGenerationDate = invoiceGenerationDate;
	}

	public TransactionType getTransactionTypeParam() {
		return this.transactionTypeParam;
	}

	public void setTransactionTypeParam(TransactionType transactionTypeParam) {
		this.transactionTypeParam = transactionTypeParam;
	}

	public Integer getReceiverIdParam() {
		return this.receiverIdParam;
	}

	public void setReceiverIdParam(Integer receiverIdParam) {
		this.receiverIdParam = receiverIdParam;
	}

	public Integer getDeliveryLocIdParam() {
		return this.deliveryLocIdParam;
	}

	public void setDeliveryLocIdParam(Integer deliveryLocIdParam) {
		this.deliveryLocIdParam = deliveryLocIdParam;
	}

	public Integer getDeliveryAreaIdParam() {
		return this.deliveryAreaIdParam;
	}

	public void setDeliveryAreaIdParam(Integer deliveryAreaIdParam) {
		this.deliveryAreaIdParam = deliveryAreaIdParam;
	}

	public Integer getNrGeneratedInvoices() {
		return this.nrGeneratedInvoices;
	}

	public void setNrGeneratedInvoices(Integer nrGeneratedInvoices) {
		this.nrGeneratedInvoices = nrGeneratedInvoices;
	}

	public String getInvGeneratorErrorMsg() {
		return this.invGeneratorErrorMsg;
	}

	public void setInvGeneratorErrorMsg(String invGeneratorErrorMsg) {
		this.invGeneratorErrorMsg = invGeneratorErrorMsg;
	}

	public Boolean getSeparatePerShipTo() {
		return this.separatePerShipTo;
	}

	public void setSeparatePerShipTo(Boolean separatePerShipTo) {
		this.separatePerShipTo = separatePerShipTo;
	}

	public String getExportInvoiceDescription() {
		return this.exportInvoiceDescription;
	}

	public void setExportInvoiceDescription(String exportInvoiceDescription) {
		this.exportInvoiceDescription = exportInvoiceDescription;
	}

	public Boolean getExportInvoiceResult() {
		return this.exportInvoiceResult;
	}

	public void setExportInvoiceResult(Boolean exportInvoiceResult) {
		this.exportInvoiceResult = exportInvoiceResult;
	}

	public String getSubmitForApprovalDescription() {
		return this.submitForApprovalDescription;
	}

	public void setSubmitForApprovalDescription(
			String submitForApprovalDescription) {
		this.submitForApprovalDescription = submitForApprovalDescription;
	}

	public Boolean getSubmitForApprovalResult() {
		return this.submitForApprovalResult;
	}

	public void setSubmitForApprovalResult(Boolean submitForApprovalResult) {
		this.submitForApprovalResult = submitForApprovalResult;
	}

	public String getExportInvoiceOption() {
		return this.exportInvoiceOption;
	}

	public void setExportInvoiceOption(String exportInvoiceOption) {
		this.exportInvoiceOption = exportInvoiceOption;
	}

	public String getIataXmlFileName() {
		return this.iataXmlFileName;
	}

	public void setIataXmlFileName(String iataXmlFileName) {
		this.iataXmlFileName = iataXmlFileName;
	}

	public ExportTypeInvoice getExportType() {
		return this.exportType;
	}

	public void setExportType(ExportTypeInvoice exportType) {
		this.exportType = exportType;
	}

	public String getReportName() {
		return this.reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportCode() {
		return this.reportCode;
	}

	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}

	public String getApprovalNote() {
		return this.approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}

	public String getApproveRejectResult() {
		return this.approveRejectResult;
	}

	public void setApproveRejectResult(String approveRejectResult) {
		this.approveRejectResult = approveRejectResult;
	}

	public String getSelectedAttachments() {
		return this.selectedAttachments;
	}

	public void setSelectedAttachments(String selectedAttachments) {
		this.selectedAttachments = selectedAttachments;
	}

	public String getSelectedInvoices() {
		return this.selectedInvoices;
	}

	public void setSelectedInvoices(String selectedInvoices) {
		this.selectedInvoices = selectedInvoices;
	}

	public Boolean getSelectedAllInvoices() {
		return this.selectedAllInvoices;
	}

	public void setSelectedAllInvoices(Boolean selectedAllInvoices) {
		this.selectedAllInvoices = selectedAllInvoices;
	}

	public String getUnselectedInvoices() {
		return this.unselectedInvoices;
	}

	public void setUnselectedInvoices(String unselectedInvoices) {
		this.unselectedInvoices = unselectedInvoices;
	}

	public String getFilter() {
		return this.filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getStandardFilter() {
		return this.standardFilter;
	}

	public void setStandardFilter(String standardFilter) {
		this.standardFilter = standardFilter;
	}
}
