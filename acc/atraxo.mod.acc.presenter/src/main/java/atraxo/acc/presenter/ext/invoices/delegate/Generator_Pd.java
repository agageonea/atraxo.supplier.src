package atraxo.acc.presenter.ext.invoices.delegate;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTimeComparator;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.ext.invoices.GeneratorResponse;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_Ds;
import atraxo.acc.presenter.impl.invoices.model.OutgoingInvoice_DsParam;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Locations;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class Generator_Pd extends AbstractPresenterDelegate {

	public void generate(OutgoingInvoice_Ds ds, OutgoingInvoice_DsParam dsParam) throws Exception {
		this.validate(dsParam);
		IOutgoingInvoiceService srv = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		Customer customer = null;
		if (dsParam.getReceiverIdParam() != null) {
			customer = srv.findById(dsParam.getReceiverIdParam(), Customer.class);
		}
		GeneratorResponse response;
		Date dateTo = DateUtils.addHours(dsParam.getDateTo(), 23);
		dateTo = DateUtils.addMinutes(dateTo, 59);
		dateTo = DateUtils.addSeconds(dateTo, 59);

		if (dsParam.getDeliveryLocIdParam() != null) {
			Locations location = srv.findById(dsParam.getDeliveryLocIdParam(), Locations.class);
			response = srv.generate(dsParam.getInvoiceGenerationDate(), dateTo, dsParam.getClosingDate(), dsParam.getTransactionTypeParam(), customer,
					dsParam.getSeparatePerShipTo(), location);
		} else if (dsParam.getDeliveryAreaIdParam() != null) {
			Area area = srv.findById(dsParam.getDeliveryAreaIdParam(), Area.class);
			response = srv.generate(dsParam.getInvoiceGenerationDate(), dateTo, dsParam.getClosingDate(), dsParam.getTransactionTypeParam(), customer,
					dsParam.getSeparatePerShipTo(), area);
		} else {
			response = srv.generate(dsParam.getInvoiceGenerationDate(), dateTo, dsParam.getClosingDate(), dsParam.getTransactionTypeParam(), customer,
					dsParam.getSeparatePerShipTo());
		}

		dsParam.setNrGeneratedInvoices(response.getNrInvoicesGenerated());
		dsParam.setInvGeneratorErrorMsg(response.getErrorMsg());

	}

	private void validate(OutgoingInvoice_DsParam dsParam) throws BusinessException {
		if (dsParam.getDateTo() == null || dsParam.getTransactionTypeParam() == null) {
			throw new BusinessException(AccErrorCode.INVALID_INV_GEN_PARAM, AccErrorCode.INVALID_INV_GEN_PARAM.getErrMsg());
		}
		if (dsParam.getInvoiceGenerationDate() != null) {
			int invoiceDateDateToComparison = DateTimeComparator.getDateOnlyInstance().compare(dsParam.getInvoiceGenerationDate(),
					dsParam.getDateTo());
			if (invoiceDateDateToComparison < 0) {
				throw new BusinessException(AccErrorCode.INVOICE_DATE_GRATER_THAN_DATE_TO, AccErrorCode.INVOICE_DATE_GRATER_THAN_DATE_TO.getErrMsg());
			}
		}

	}
}
