
Ext.override(atraxo.acc.i18n.dc.OutgoingInvoice_Dc$ApprovalNote, {
	remarks__lbl: "Add a note to be sent with your request to the approvers"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoice_Dc$Export, {
	exportTypeLabel__lbl: "Document type",
	reportNameLabel__lbl: "Template",
	reportName__lbl: "Report name"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoice_Dc$Filter, {
	customerName__lbl: "Invoice receiver name",
	customer__lbl: "Invoice receiver",
	exportDate__lbl: "Export date",
	exportStatus__lbl: "Export status",
	issuerCode__lbl: "Invoice issuer",
	issuerCode__tlp: "Invoice issuer",
	product__lbl: "Product"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoice_Dc$Generate, {
	closeDate__lbl: "Closing date",
	deliveryTo__lbl: "Deliveries up to",
	invoiceGenerationDate__lbl: "Invoice date",
	separatePerShipTo__lbl: "Separate invoice per ship-to"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoice_Dc$InvoiceHeader, {
	deliveryDateFrom__lbl: "Delivery period from",
	deliveryLocCode__lbl: "For location",
	deliverydateTo__lbl: "Delivery period until",
	dueIn__lbl: "Due in [days]",
	invDate__lbl: "Invoice date",
	invoiceForm__lbl: "Invoice form",
	invoiceNo__lbl: "Invoice #",
	invoiceRefNo__lbl: "Reference invoice",
	issueDate__lbl: "Issued on",
	issuer__lbl: "Invoice issuer ",
	product__lbl: "Product",
	quantity__lbl: "Total quantity",
	receiver__lbl: "Invoice receiver",
	refDocNo__lbl: "Document #",
	refDoc__lbl: "Reference document",
	status__lbl: "Invoice status",
	totalAmount__lbl: "Total amount",
	transactionType__lbl: "Transaction type"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoice_Dc$InvoiceList, {
	amount__lbl: "Gross amount",
	approvalStatus__lbl: "Approval status",
	controlNo__lbl: "Ricoh control number",
	currency__lbl: "Currency",
	deliveryFrom__lbl: "Deliveries from",
	deliveryTo__lbl: "Deliveries up to",
	exportDate__lbl: "Export date",
	exportStatus__lbl: "Export status",
	invoiceNo__lbl: "Invoice #",
	invoiceRefNo__lbl: "Reference invoice",
	issuerCode__lbl: "Invoice issuer ",
	items__lbl: "Invoice lines #",
	location__lbl: "Location",
	product__lbl: "Product",
	quantity__lbl: "Quantity",
	status__lbl: "Invoice status",
	type__lbl: "Invoice type",
	unitCode__lbl: "Unit",
	vatAmount__lbl: "VAT amount"
});
