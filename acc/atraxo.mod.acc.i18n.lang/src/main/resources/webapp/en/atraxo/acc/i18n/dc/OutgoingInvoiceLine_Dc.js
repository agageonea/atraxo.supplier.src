
Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$AddList, {
	aircraftRegistration__lbl: "Aircraft",
	customer__lbl: "Ship-to",
	flightNo__lbl: "Flight #",
	ticketNo__lbl: "Fuel ticket #"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$EditList, {
	amount__lbl: "Amount",
	bolNumber__lbl: "Bill of lading #",
	erpReference__lbl: "External sales invoice #",
	flightID__lbl: "Flight #",
	flightType__lbl: "Flight type",
	quantity__lbl: "Quantity",
	shipToName__lbl: "Ship-to name",
	shipTo__lbl: "Ship-to",
	suffix__lbl: "Flight suffix",
	ticketNo__lbl: "Fuel ticket #",
	transmissionStatus__lbl: "Transmission status",
	vatAmount__lbl: "VAT amount"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$Filter, {
	bolNumber__lbl: "Bill of lading #"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$MemoList, {
	aircraftRegistration__lbl: "Aircraft",
	customer__lbl: "Ship-to",
	flightNo__lbl: "Flight #",
	ticketNo__lbl: "Fuel ticket #"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$creditMemoReason, {
	creditMemoReasonLabel__lbl: "Reason for creating the credit memo"
});
