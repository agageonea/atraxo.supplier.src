
Ext.override(atraxo.acc.i18n.dc.DeliveryNote_Dc$Filter, {
	billableCostCurrency__lbl: "Currency",
	billableCost__lbl: "Receivable amount",
	fuelingQuantity__lbl: "Fueling quantity",
	iplAgent__lbl: "IPL agent",
	payableCost__lbl: "Payable amount",
	source__lbl: "Source",
	supplier__lbl: "Fuel supplier",
	unit__lbl: "Unit",
	used__lbl: "Used?"
});

Ext.override(atraxo.acc.i18n.dc.DeliveryNote_Dc$List, {
	aircraftRegistration__lbl: "Aircraft",
	customer__lbl: "Customer",
	flightNumber__lbl: "Flight #",
	fuelingDate__lbl: "Fueling date",
	iplAgent__lbl: "IPL agent",
	location__lbl: "Location",
	quantity__lbl: "Fueling quantity",
	source__lbl: "Source",
	supplier__lbl: "Fuel supplier",
	ticketNumber__lbl: "Fuel ticket #",
	unit__lbl: "Unit",
	used__lbl: "Used?"
});
