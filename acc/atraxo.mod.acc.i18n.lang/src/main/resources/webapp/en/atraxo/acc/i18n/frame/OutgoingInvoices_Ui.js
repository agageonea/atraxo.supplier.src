Ext.override(atraxo.acc.i18n.frame.OutgoingInvoices_Ui, {
	/* view */
	History__ttl: "History",
	InvoiceLineList__ttl: "Invoice lines",
	NotesList__ttl: "Notes",
	addFuelEventWdw__ttl: "",
	addInvoiceLineWdw__ttl: "Add Invoice Lines",
	attachmentList__ttl: "Documents",
	documentsAssignList__ttl: "Select documents to be attached to the request sent to approvers",
	exportInvoiceWdw__ttl: "Export Invoice",
	newCreditMemoWdw__ttl: "New Credit Memo",
	noteWdw__ttl: "Note",
	wdwApprovalNote__ttl: "Remark",
	wdwApproveNote__ttl: "Remark",
	wdwBulkExportRicohWizard__ttl: "Send Invoices to Ricoh Printer in Bulk",
	wdwBulkExportWizard__ttl: "Bulk Invoice Export",
	wdwGenerate__ttl: "New Sales Invoice",
	wdwRejectNote__ttl: "Remark",
	/* menu */
	/* button */
	btAddInvoiceLine__lbl: "Add",
	btAddInvoiceLine__tlp: "Add",
	btnAddFeulEv__lbl: "Add fuel event",
	btnAddFeulEv__tlp: "Add fuel event",
	btnApplyFuelEventFilter__lbl: "Apply filter",
	btnApplyFuelEventFilter__tlp: "Apply filter",
	btnApproveEdit__lbl: "Approve",
	btnApproveEdit__tlp: "Approve ",
	btnApproveList__lbl: "Approve",
	btnApproveList__tlp: "Approve",
	btnApproveListRegular__lbl: "Approve invoice",
	btnApproveListRegular__tlp: "Approve invoice",
	btnApproveSingleRegular__lbl: "Approve invoice",
	btnApproveSingleRegular__tlp: "Approve invoice",
	btnBackMemo__lbl: "Back",
	btnBackMemo__tlp: "Back",
	btnBackToConditions__lbl: "Back to conditions",
	btnBackToConditions__tlp: "Back to conditions",
	btnBackToRicohConditions__lbl: "Back to conditions",
	btnBackToRicohConditions__tlp: "Back to conditions",
	btnCancel__lbl: "Cancel",
	btnCancel__tlp: "Cancel",
	btnCancelApprovalWdw__lbl: "Cancel",
	btnCancelApprovalWdw__tlp: "Cancel",
	btnCancelApproveWdw__lbl: "Cancel",
	btnCancelApproveWdw__tlp: "Cancel",
	btnCancelExport__lbl: "Cancel",
	btnCancelExport__tlp: "Cancel",
	btnCancelFuelEvent__lbl: "Cancel",
	btnCancelFuelEvent__tlp: "Cancel",
	btnCancelInvoiceLine__lbl: "Cancel",
	btnCancelInvoiceLine__tlp: "Cancel",
	btnCancelMemo__lbl: "Cancel",
	btnCancelMemo__tlp: "Cancel",
	btnCancelRejectWdw__lbl: "Cancel",
	btnCancelRejectWdw__tlp: "Cancel",
	btnCloseNoteWdw__lbl: "Cancel",
	btnCloseNoteWdw__tlp: "Cancel",
	btnContinueMemo__lbl: "Continue",
	btnContinueMemo__tlp: "Continue",
	btnContinueMemo2__lbl: "Continue",
	btnContinueMemo2__tlp: "Continue",
	btnContinueRicohStep1__lbl: "Continue with invoice selection",
	btnContinueRicohStep1__tlp: "Continue with invoice selection",
	btnContinueStep1__lbl: "Continue with invoice selection",
	btnContinueStep1__tlp: "Continue with invoice selection",
	btnDeleteAttach__lbl: "Delete",
	btnDeleteAttach__tlp: "Delete",
	btnDeleteSalesInvoice__lbl: "Delete",
	btnDeleteSalesInvoice__tlp: "Delete",
	btnDiscardRicohStep1__lbl: "Cancel",
	btnDiscardRicohStep1__tlp: "Cancel send action",
	btnDiscardStep1__lbl: "Cancel export",
	btnDiscardStep1__tlp: "Cancel export",
	btnExportBulkInvoices__lbl: "Export",
	btnExportBulkInvoices__tlp: "Export",
	btnExportBulkRicohInvoices__lbl: "Send",
	btnExportBulkRicohInvoices__tlp: "Send invoices",
	btnExportFailed__lbl: "Failed",
	btnExportFailed__tlp: "Resend sales invoices that have failed to be transmitted to financial system",
	btnExportInvoice__lbl: "Export invoice",
	btnExportInvoice__tlp: "Export invoice",
	btnExportInvoiceList__lbl: "Export invoice",
	btnExportInvoiceList__tlp: "Export invoice",
	btnExportInvoicesBulk__lbl: "Bulk invoice export",
	btnExportInvoicesBulk__tlp: "Bulk invoice export",
	btnExportInvoicesMenu__lbl: "Export invoice",
	btnExportInvoicesMenu__tlp: "Export invoice",
	btnExportSelected__lbl: "Selected",
	btnExportSelected__tlp: "Resend selected sales invoices to financial system",
	btnExportToRicoh__lbl: "Send to Ricoh printer",
	btnExportToRicoh__tlp: "Send to Ricoh printer",
	btnExportToRicohBulk__lbl: "Bulk mode",
	btnExportToRicohBulk__tlp: "Send invoices in bulk",
	btnExportToRicohMenu__lbl: "Send to Ricoh printer",
	btnExportToRicohMenu__tlp: "Send invoices to Ricoh printer",
	btnExportToRicohSelected__lbl: "Selected invoice",
	btnExportToRicohSelected__tlp: "Send selected invoice",
	btnExportWithMenu__lbl: "Resend",
	btnExportWithMenu__tlp: "Resend sales invoices to financial system",
	btnGenerate__lbl: "New invoice",
	btnGenerate__tlp: "Generate new invoice",
	btnGenerateFunction__lbl: "Generate",
	btnGenerateFunction__tlp: "Generate new invoice(s)",
	btnInvAddFeulEv__lbl: "Add fuel event",
	btnInvAddFeulEv__tlp: "Add fuel event",
	btnNewAttach__lbl: "New",
	btnNewAttach__tlp: "New",
	btnNewCreditNote__lbl: "New credit memo",
	btnNewCreditNote__tlp: "Create new credit memo",
	btnNewInvoice__lbl: "Invoice",
	btnNewInvoice__tlp: "Invoice",
	btnNewWithMenu__lbl: "New invoice",
	btnNewWithMenu__tlp: "New invoice",
	btnOkExport__lbl: "Ok",
	btnOkExport__tlp: "Ok",
	btnPaid__lbl: "Paid",
	btnPaid__tlp: "Paid",
	btnPaidList__lbl: "Paid",
	btnPaidList__tlp: "Paid",
	btnRejectEdit__lbl: "Reject",
	btnRejectEdit__tlp: "Reject",
	btnRejectList__lbl: "Reject",
	btnRejectList__tlp: "Reject",
	btnRemove__lbl: "Remove",
	btnRemove__tlp: "Remove fuel event",
	btnResetFuelEventFilter__lbl: "",
	btnResetFuelEventFilter__tlp: "",
	btnSaveCloseApprovalWdw__lbl: "Submit",
	btnSaveCloseApprovalWdw__tlp: "Submit",
	btnSaveCloseApproveWdw__lbl: "Save & Close",
	btnSaveCloseApproveWdw__tlp: "Approve price update",
	btnSaveCloseNoteWdw__lbl: "Save & Close",
	btnSaveCloseNoteWdw__tlp: "Save & Close",
	btnSaveCloseRejectWdw__lbl: "Save & Close",
	btnSaveCloseRejectWdw__tlp: "Reject price update",
	btnSelectFuelEvent__lbl: "Select",
	btnSelectFuelEvent__tlp: "Select",
	btnSubmitForApproval__lbl: "Submit for approval",
	btnSubmitForApproval__tlp: "Submit for approval",
	btnSubmitForApprovalList__lbl: "Submit for approval",
	btnSubmitForApprovalList__tlp: "Submit for approval",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	helpWdwEdit__lbl: "Help",
	helpWdwEdit__tlp: "Help",
	
	title: "Sales invoices"
});
