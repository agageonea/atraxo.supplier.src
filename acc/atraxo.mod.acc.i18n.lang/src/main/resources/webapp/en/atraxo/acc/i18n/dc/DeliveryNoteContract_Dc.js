
Ext.override(atraxo.acc.i18n.dc.DeliveryNoteContract_Dc$pcList, {
	amount__lbl: "Amount",
	contracType__lbl: "Contract type",
	contractCode__lbl: "Contract #",
	contractSubType__lbl: "Delivery point",
	convAmount__lbl: "Converted amount",
	convCurr__lbl: "System currency",
	currency__lbl: "Currency",
	isSpot__lbl: "Term/Spot",
	scope__lbl: "Contract scope",
	supplier__lbl: "Supplier"
});
