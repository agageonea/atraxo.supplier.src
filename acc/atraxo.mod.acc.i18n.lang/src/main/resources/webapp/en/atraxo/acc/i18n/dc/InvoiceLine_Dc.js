
Ext.override(atraxo.acc.i18n.dc.InvoiceLine_Dc$Details, {
	airlineDesignator__lbl: "Ship-to"
});

Ext.override(atraxo.acc.i18n.dc.InvoiceLine_Dc$EditList, {
	airlineDesignator__lbl: "Ship-to",
	amount__lbl: "Amount",
	eventDate__lbl: "Flight date",
	flightNo__lbl: "Flight #",
	flightType__lbl: "Flight type",
	invoiceLine__lbl: "Invoice line",
	overAllCheckStatus__lbl: "Check status",
	quantity__lbl: "Quantity",
	suffix__lbl: "Flight suffix",
	ticketNo__lbl: "Fuel ticket #",
	vatAmount__lbl: "VAT amount"
});
