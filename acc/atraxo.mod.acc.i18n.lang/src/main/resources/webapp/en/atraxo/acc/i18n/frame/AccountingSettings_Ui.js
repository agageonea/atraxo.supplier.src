Ext.override(atraxo.acc.i18n.frame.AccountingSettings_Ui, {
	/* view */
	invNumSerieList__ttl: "Invoice Number Series",
	vatPanel__ttl: "VAT Rates",
	/* menu */
	/* button */
	disableInvNumSerie__lbl: "Disable",
	disableInvNumSerie__tlp: "Disable",
	enableInvNumSerie__lbl: "Enable",
	enableInvNumSerie__tlp: "Enable",
	helpInvWdw__lbl: "Help",
	helpInvWdw__tlp: "Help",
	helpWdw__lbl: "Help",
	helpWdw__tlp: "Help",
	
	title: "Accounting Settings"
});
