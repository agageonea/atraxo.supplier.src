
Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceBulkExportRicohWizard_Dc$bulkInvoiceExportStep1, {
	delLocCode__lbl: "Location ",
	periodLabel__lbl: "Invoice date between",
	periodSeparator__lbl: "-",
	receiverCode__lbl: "Invoice receiver",
	step1DescriptionLabel__lbl: "Indicate criteria for selecting invoices to be sent:"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceBulkExportRicohWizard_Dc$wizardHeader, {
	phase1__lbl: "Set conditions",
	phase2__lbl: "Select invoices"
});
