
Ext.override(atraxo.acc.i18n.dc.ReceivableAccrual_Dc$Filter, {
	invoiceStatus__lbl: "Invoice status"
});

Ext.override(atraxo.acc.i18n.dc.ReceivableAccrual_Dc$List, {
	accruedCost__lbl: "Accrued cost",
	calculatedCost__lbl: "Calculated cost",
	invoiceCost__lbl: "Invoice cost",
	invoiceStatus__lbl: "Invoicing status"
});

Ext.override(atraxo.acc.i18n.dc.ReceivableAccrual_Dc$mainList, {
	fuelingDate__lbl: "Month",
	grossAmount__lbl: "Gross amount",
	netAmount__lbl: "Net amount",
	vatAmount__lbl: "VAT amount"
});
