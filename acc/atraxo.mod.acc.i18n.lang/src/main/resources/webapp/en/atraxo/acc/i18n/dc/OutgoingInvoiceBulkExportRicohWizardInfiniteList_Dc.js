
Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceBulkExportRicohWizardInfiniteList_Dc$bulkInvoiceExportRicohStep2, {
	delLocCode__lbl: "Location",
	invoiceDate__lbl: "Invoice date",
	invoiceNo__lbl: "Invoice #",
	issuerCode__lbl: "Invoice issuer",
	receiverCode__lbl: "Invoice receiver"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceBulkExportRicohWizardInfiniteList_Dc$bulkInvoiceExportRicohStep2TopLabel, {
	step2Description__lbl: "Select invoices to be sent. The invoice list is filtered by the previously indicated criteria."
});
