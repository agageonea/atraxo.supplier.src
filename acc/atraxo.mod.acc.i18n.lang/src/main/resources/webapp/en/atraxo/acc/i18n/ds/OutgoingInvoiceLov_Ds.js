Ext.override(atraxo.acc.i18n.ds.OutgoingInvoiceLov_Ds, {
	invoiceId__lbl: "ID",
	invoiceNo__lbl: "Invoice #",
	invoiceNo__tlp: "Invoice number",
	status__lbl: "Invoice status",
	status__tlp: "Invoice status indicator"
});
