
Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceBulkExportWizard_Dc$bulkInvoiceExportStep1, {
	delLocCode__lbl: "Location ",
	periodLabel__lbl: "Invoice date between",
	periodSeparator__lbl: "-",
	receiverCode__lbl: "Invoice receiver",
	reportName__lbl: "Invoice template",
	step1DescriptionLabel__lbl: "Indicate criteria for selecting invoices to be exported:"
});

Ext.override(atraxo.acc.i18n.dc.OutgoingInvoiceBulkExportWizard_Dc$wizardHeader, {
	phase1__lbl: "Set conditions",
	phase2__lbl: "Select invoices"
});
