Ext.override(atraxo.acc.i18n.ds.OutgoingInvoiceMemo_Ds, {
	baseLineDate__lbl: "Baseline date",
	baseLineDate__tlp: "Date for payment deadline calculated by the system based on the contract payment terms. Can be overwritten by the user.",
	delLocCode__lbl: "Delivery location",
	delLocCode__tlp: "Code",
	delLocId__lbl: "Delivery loc (ID)",
	invoiceDate__lbl: "Invoice date",
	invoiceDate__tlp: "Invoice date",
	invoiceNo__lbl: "Invoice #",
	invoiceNo__tlp: "Invoice number",
	receiverCode__lbl: "Invoice receiver",
	receiverCode__tlp: "Invoice receiver's company code",
	receiverId__lbl: "Receiver (ID)",
	referenceInvoiceId__lbl: "Invoice reference (ID)",
	referenceInvoiceNumber__lbl: "Invoice #",
	referenceInvoiceNumber__tlp: "Invoice number",
	status__lbl: "Invoice status",
	status__tlp: "Invoice status indicator"
});
