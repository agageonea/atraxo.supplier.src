
Ext.override(atraxo.acc.i18n.dc.Invoice_Dc$Edit, {
	btn__lbl: "...",
	btn__tlp: "...",
	contractCode__lbl: "Contract #",
	deliveryDateFrom__lbl: "Delivery period from",
	deliveryLocCode__lbl: "For location",
	deliverydateTo__lbl: "Delivery period until",
	dueIn__lbl: "Due in [days]",
	invoiceForm__lbl: "Invoice form",
	invoiceNo__lbl: "Invoice #",
	issueDate__lbl: "Issued on",
	issuerBaseLine__lbl: "Issuer bas eline date",
	issuerCode__lbl: "Invoice issuer",
	quantity__lbl: "Total quantity",
	receiverCode__lbl: "Invoice receiver",
	receivingDate__lbl: "Received on",
	status__lbl: "Invoice status",
	totalAmount__lbl: "Total amount",
	transactionType__lbl: "Transaction type"
});

Ext.override(atraxo.acc.i18n.dc.Invoice_Dc$List, {
	amount__lbl: "Total amount",
	currency__lbl: "Currency",
	date__lbl: "Date",
	invoiceNumber__lbl: "Invoice #",
	issuer__lbl: "Invoice issuer",
	location__lbl: "Location",
	quantity__lbl: "Quantity",
	status__lbl: "Invoice status",
	type__lbl: "Invoice type",
	unitCode__lbl: "Unit"
});

Ext.override(atraxo.acc.i18n.dc.Invoice_Dc$New, {
	deliveryDateFrom__lbl: "Delivery period from",
	deliveryLocCode__lbl: "For location",
	deliverydateTo__lbl: "until",
	invoiceNo__lbl: "Invoice #",
	issueDate__lbl: "Issued on",
	issuerCode__lbl: "Received from",
	quantity__lbl: "Total quantity",
	totalAmount__lbl: "Total amount"
});
