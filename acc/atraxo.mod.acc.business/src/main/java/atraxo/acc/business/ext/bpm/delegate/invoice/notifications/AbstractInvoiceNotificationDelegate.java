/**
 *
 */
package atraxo.acc.business.ext.bpm.delegate.invoice.notifications;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;

/**
 * @author vhojda
 */
public abstract class AbstractInvoiceNotificationDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractInvoiceNotificationDelegate.class);

	/**
	 * Performs all the operations of the current delegate.
	 */
	public abstract void executeDelegate() throws Exception;

	/**
	 * Perform operations afetr execution of delegate.
	 */
	public abstract void afterExecuteDelegate();

	/**
	 * @returns the Error message from current Workflow Delegate
	 */
	public abstract String getDelegateErrorCode();

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		try {
			this.execution.setVariable(WorkflowVariablesConstants.SKIP_TO_NOTIFICATIONS, true);
			this.executeDelegate();
			this.execution.setVariable(WorkflowVariablesConstants.SKIP_TO_NOTIFICATIONS, false);
		} catch (Exception e) {
			LOGGER.warn("WARNING:Caught Exception when executing Invoice Notification Workflow, skipping to sending notification!", e);
			this.execution.setVariable(WorkflowVariablesConstants.SKIP_TO_NOTIFICATIONS, true);
			this.execution.setVariable(WorkflowVariablesConstants.ERROR_NOTIFICATION,
					this.getDelegateErrorCode() + "[" + e.getLocalizedMessage() + "]");
		} finally {
			this.afterExecuteDelegate();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return null;
	}

}
