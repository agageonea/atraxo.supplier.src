package atraxo.acc.business.ext.invoices.delegate;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.payableAccruals.IPayableAccrualService;
import atraxo.acc.business.ext.fuelEvents.delegate.DensityDelegate;
import atraxo.acc.business.ext.payableAccruals.delegate.PayableAccrualUpdate_Bd;
import atraxo.acc.domain.impl.acc_type.InvoiceProcessStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.tolerance.IToleranceService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.tolerance.ToleranceResult;
import atraxo.fmbas.business.ext.tolerance.ToleranceVerifier;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author apetho
 */
public class ApproveIncommingInvoice_Bd extends AbstractBusinessDelegate {

	/**
	 * Recalculate the payable accruals invoiced and the accrued costs.
	 *
	 * @param invoice
	 * @throws BusinessException
	 */
	public void approveInvoice(Invoice invoice) throws BusinessException {
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		IPayableAccrualService payableAccrualService = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		ICurrenciesService currencieService = (ICurrenciesService) this.findEntityService(Currencies.class);
		PayableAccrualUpdate_Bd paUpdateBd = this.getBusinessDelegate(PayableAccrualUpdate_Bd.class);
		PriceConverterService priceConverterService = this.getApplicationContext().getBean(PriceConverterService.class);
		UnitConverterService unitConverterService = this.getApplicationContext().getBean(UnitConverterService.class);
		for (InvoiceLine invoiceLine : invoice.getInvoiceLines()) {
			List<PayableAccrual> payableAccruals = this.getPayableAccrual(invoiceLine, payableAccrualService);
			for (PayableAccrual payableAccrual : payableAccruals) {
				this.setInvoicedFields(payableAccrual, invoiceLine, paramSrv, currencieService, priceConverterService, unitConverterService);
				paUpdateBd.updatePayableAccrual(payableAccrual);
				this.closeAccrual(payableAccrual, unitConverterService);
				payableAccrualService.update(payableAccrual);
			}
		}
	}

	/**
	 * Set up the payable accrual quantity and cost.
	 *
	 * @param payableAccrual
	 * @param invoiceLine
	 * @param paramSrv
	 * @param exchangeRateService
	 * @param currencieService
	 * @throws BusinessException
	 */
	private void setInvoicedFields(PayableAccrual payableAccrual, InvoiceLine invoiceLine, ISystemParameterService paramSrv,
			ICurrenciesService currencieService, PriceConverterService priceConverterService, UnitConverterService unitConverterService)
			throws BusinessException {
		payableAccrual.setInvoicedQuantity(payableAccrual.getInvoicedQuantity()
				.add(this.getInSystemQuantity(invoiceLine, paramSrv, unitConverterService), MathContext.DECIMAL64));
		payableAccrual.setInvoicedCurrency(invoiceLine.getInvoice().getCurrency());
		payableAccrual.setInvoicedAmountSet(invoiceLine.getAmount());
		payableAccrual.setInvoicedVATSet(invoiceLine.getVat());
		String sysCurrencieStr = paramSrv.getSysCurrency();
		Currencies sysCurrencie = currencieService.findByCode(sysCurrencieStr);

		IFinancialSourcesService finSrcSrv = (IFinancialSourcesService) this.findEntityService(FinancialSources.class);
		Map<String, Object> params = new HashMap<>();
		params.put("isStdFlg", true);
		params.put("active", true);
		FinancialSources stdFinSrc = finSrcSrv.findEntityByAttributes(params);
		IAverageMethodService avgMthSrv = (IAverageMethodService) this.findEntityService(AverageMethod.class);
		AverageMethod stdAvgMth = avgMthSrv.findByName(paramSrv.getSysAverageMethod());

		ConversionResult sysAmount = priceConverterService.convert(invoiceLine.getInvoice().getUnit(), invoiceLine.getInvoice().getUnit(),
				payableAccrual.getInvoicedCurrency(), sysCurrencie, payableAccrual.getInvoicedAmountSet(),
				this.getAccrualEvaluationDate(paramSrv, payableAccrual.getFuelEvent()), stdFinSrc, stdAvgMth, this.getDensity(invoiceLine), false,
				MasterAgreementsPeriod._CURRENT_);
		ConversionResult sysVAT = priceConverterService.convert(invoiceLine.getInvoice().getUnit(), this.getSysVol(paramSrv),
				payableAccrual.getInvoicedCurrency(), sysCurrencie, payableAccrual.getInvoicedVATSet(),
				this.getAccrualEvaluationDate(paramSrv, payableAccrual.getFuelEvent()), stdFinSrc, stdAvgMth, this.getDensity(invoiceLine), false,
				MasterAgreementsPeriod._CURRENT_);
		payableAccrual.setInvoicedAmountSys(sysAmount.getValue());
		payableAccrual.setInvoicedVATSys(sysVAT.getValue());
	}

	/**
	 * Convert the settlement quantity in system quantity.
	 *
	 * @param invoiceLine
	 * @param paramSrv
	 * @return
	 * @throws BusinessException
	 */
	private BigDecimal getInSystemQuantity(InvoiceLine invoiceLine, ISystemParameterService paramSrv, UnitConverterService unitConverterService)
			throws BusinessException {
		Density density = this.getDensity(invoiceLine);
		return unitConverterService.convert(invoiceLine.getInvoice().getUnit(), this.getSysVol(paramSrv), invoiceLine.getQuantity(), density);
	}

	/**
	 * If the delivery note is generated from a fuel ticket we get the density from the fuel ticket. If the density is null or we dont have delivery
	 * note generated from fuel ticket we use the system density.
	 *
	 * @param paramSrv
	 * @param invoiceLine
	 * @return
	 * @throws BusinessException
	 */
	private Density getDensity(InvoiceLine invoiceLine) throws BusinessException {
		IDeliveryNoteService deliveryNoteService = (IDeliveryNoteService) this.findEntityService(DeliveryNote.class);
		List<DeliveryNote> list = deliveryNoteService.findByObjectIdAndType(invoiceLine.getId(), InvoiceLine.class.getSimpleName());
		DensityDelegate bd = this.getBusinessDelegate(DensityDelegate.class);
		// system density
		Density density = bd.getDensity(null);
		for (DeliveryNote dn : list) {
			density = bd.getDensity(dn.getFuelEvent());
		}
		return density;
	}

	/**
	 * Get the system volume unit.
	 *
	 * @param paramSrv
	 * @return
	 * @throws BusinessException
	 */
	private Unit getSysVol(ISystemParameterService paramSrv) throws BusinessException {
		String sysVolume = paramSrv.getSysVol();
		IUnitService unitService = (IUnitService) this.findEntityService(Unit.class);
		return unitService.findByCode(sysVolume);
	}

	/**
	 * Return the generated payable accruals.
	 *
	 * @param invoiceLine
	 * @param payableAccrualService
	 * @return
	 * @throws BusinessException
	 */
	private List<PayableAccrual> getPayableAccrual(InvoiceLine invoiceLine, IPayableAccrualService payableAccrualService) throws BusinessException {
		IDeliveryNoteService deliveryNoteService = (IDeliveryNoteService) this.findEntityService(DeliveryNote.class);
		List<DeliveryNote> list = deliveryNoteService.findByObjectIdAndType(invoiceLine.getId(), InvoiceLine.class.getSimpleName());
		List<PayableAccrual> retList = new ArrayList<>();
		for (DeliveryNote dn : list) {
			for (DeliveryNoteContract dnc : dn.getDeliveryNoteContracts()) {
				PayableAccrual pa = payableAccrualService.findByFuelEventContract(dnc.getContracts(), dn.getFuelEvent());
				retList.add(pa);
			}

		}
		return retList;
	}

	/**
	 * Get the accrual evaluational date.
	 *
	 * @param paramSrv
	 * @param fe
	 * @return If the system parameter is event date return the fuel event's fueling date, else the current date.
	 * @throws BusinessException
	 */
	private Date getAccrualEvaluationDate(ISystemParameterService paramSrv, FuelEvent fe) throws BusinessException {
		String accrualEvaluationDate = paramSrv.getAccrualEvaluationDate();
		Date retDate = GregorianCalendar.getInstance().getTime();
		if ("Event Date".equalsIgnoreCase(accrualEvaluationDate)) {
			retDate = fe.getFuelingDate();
		}
		return retDate;
	}

	/**
	 * If the accruad amount is in tolerance set the accrual status to completely invoiced, else partial invoiced.
	 *
	 * @param accrual
	 * @throws BusinessException
	 */
	private void closeAccrual(PayableAccrual accrual, UnitConverterService unitConverterService) throws BusinessException {
		IToleranceService toleranceSrv = (IToleranceService) this.findEntityService(Tolerance.class);
		Tolerance tolerance = toleranceSrv.getAccrualAutoCloseTolerance();
		ToleranceVerifier verifier = new ToleranceVerifier(unitConverterService, this.getBusinessDelegate(ExchangeRate_Bd.class),
				this.getBusinessDelegate(DensityDelegate.class).getDensity(null));
		ToleranceResult result = verifier.checkTolerance(accrual.getExpectedAmountSys(), accrual.getInvoicedAmountSys(), accrual.getAccrualCurrency(),
				accrual.getAccrualCurrency(), tolerance, GregorianCalendar.getInstance().getTime());
		switch (result.getType()) {
		case MARGIN:
		case WITHIN:
			accrual.setIsOpen(false);
			accrual.setInvProcessStatus(InvoiceProcessStatus._COMPLETELY_INVOICED_);
			break;
		case NOT_WITHIN:
			accrual.setInvProcessStatus(InvoiceProcessStatus._PARTIAL_INVOICED_);
			break;
		default:
			break;
		}
	}

}
