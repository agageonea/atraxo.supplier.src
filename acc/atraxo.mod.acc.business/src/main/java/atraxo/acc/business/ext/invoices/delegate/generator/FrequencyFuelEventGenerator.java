package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;

/**
 * @author vhojda
 *
 */
public interface FrequencyFuelEventGenerator {

	/**
	 * Depending on frequency, this method removes from the received collection
	 * of events the ones that are not in the present or future period of time
	 * (eg: for the DAILY frequency, the ones from present and future are
	 * removed; for the MONTHLY frequency, the ones that are not in a past month
	 * are removed, and so on )
	 *
	 * @param events
	 */
	public void filterByFrequency(Collection<FuelEvent> events);

	/**
	 * This method splits the received collection of events in another
	 * collections, based on the invoicing period of time (eg.:for the DAILY
	 * frequency, the events are split in collections, each collection for a
	 * unique day; for MONTHLY, the collections are split per month, where a
	 * month is represented by the start day of the month(1) and the last day of
	 * the month(28-31))
	 *
	 * @param events
	 * @return
	 */
	public Map<InvoicingDatePeriod, List<FuelEvent>> splitByFrequency(List<FuelEvent> events);

}
