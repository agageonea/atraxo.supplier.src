package atraxo.acc.business.ext.fuelEvents.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.ext.fuelEvents.delegate.DensityDelegate;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.domain.ext.contracts.CostVat;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.business.api.vat.IVatService;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * Business service to calculate fuel event cost.
 *
 * @author zspeter
 */
public class CostCalculatorService extends AbstractBusinessBaseService {

	/**
	 * @author zspeter
	 */
	private final class ContractPriceCategoryComparator implements Comparator<ContractPriceCategory> {
		@Override
		public int compare(ContractPriceCategory o1, ContractPriceCategory o2) {
			PriceInd ind1 = o1.getPriceCategory().getPricePer();
			PriceInd ind2 = o2.getPriceCategory().getPricePer();
			if (ind1.equals(ind2)) {
				return 0;
			} else if (ind1.equals(PriceInd._VOLUME_)) {
				return -1;
			} else if (ind1.equals(PriceInd._EVENT_) && (ind2.equals(PriceInd._PERCENT_) || ind2.equals(PriceInd._COMPOSITE_))) {
				return -1;
			} else if (ind1.equals(PriceInd._PERCENT_) && ind2.equals(PriceInd._COMPOSITE_)) {
				return -1;
			} else {
				return 1;
			}

		}
	}

	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

	@Autowired
	private PriceRestriction_Service restrictionSrv;
	@Autowired
	private IVatService vatService;
	@Autowired
	private UnitConverterService unitConverterService;
	@Autowired
	private IContractPriceCategoryService cpcService;

	/**
	 * Calculate cost for a fuel event based on a location order.
	 *
	 * @param orderLocation
	 * @param event
	 * @return
	 * @throws BusinessException
	 */
	public CostVat calculate(FuelOrderLocation orderLocation, FuelEvent event) throws BusinessException {
		CostVat costVat = this.calculate(orderLocation.getContract(), event);
		BigDecimal diff = orderLocation.getDifferential();
		BigDecimal qty = this.getQuantity(orderLocation.getPaymentUnit(), event);
		costVat = this.convert(orderLocation, orderLocation.getPaymentCurrency(), costVat, event.getFuelingDate());
		if (diff != null) {
			costVat.setCost(costVat.getCost().add(qty.multiply(diff)));
		}
		return costVat;
	}

	/**
	 * @param c
	 * @param event
	 * @return
	 * @throws BusinessException
	 */
	public CostVat calculate(ContractPriceCategory c, FuelEvent event) throws BusinessException {
		Contract contract = c.getContract();
		BigDecimal qty = this.getQuantity(contract.getSettlementUnit(), event);
		BigDecimal cost = BigDecimal.ZERO;
		switch (c.getPriceCategory().getPricePer()) {
		case _VOLUME_:
			cost = this.getSettlementPricePerCategory(event, c).multiply(qty);
			break;
		case _EVENT_:
			cost = this.getSettlementPricePerCategory(event, c);
			break;
		case _PERCENT_:
			Map<ContractPriceCategory, BigDecimal> priceMap = new HashMap<>();
			cost = this.getPercentageCost(event, priceMap, c, qty);
			break;
		case _COMPOSITE_:
			Map<ContractPriceCategory, BigDecimal> priceMap1 = new HashMap<>();
			cost = this.getCompositeCost(event, priceMap1, c, qty);
			break;
		default:
			// do nothing
			break;
		}
		BigDecimal vat = BigDecimal.ZERO;
		BigDecimal vatValue = this.vatService.getVat(event.getFuelingDate(), event.getDeparture().getCountry());

		if (this.schouldCalculateVatSum(event.getEventType(), contract, contract.getVat(), true)
				|| (this.schouldCalculateVatSum(event.getEventType(), contract, c.getVat(), false))) {
			vat = vat.add(this.calculateVat(vatValue, cost));
		}
		return new CostVat(cost, vat);

	}

	/**
	 * Calculate cost for a fuel event based on a contract.
	 *
	 * @param contract
	 * @param event
	 * @return
	 * @throws BusinessException
	 */
	public CostVat calculate(Contract contract, FuelEvent event) throws BusinessException {
		BigDecimal qty = this.getQuantity(contract.getSettlementUnit(), event);
		List<ContractPriceCategory> list = this.sortPriceCategories(contract);
		Map<ContractPriceCategory, BigDecimal> priceMap = new HashMap<>();
		for (ContractPriceCategory c : list) {
			if (c.getRestriction() && !this.restrictionSrv.applyRestriction(c, event)) {
				continue;
			}
			PriceInd pricePer = c.getPriceCategory().getPricePer();
			switch (pricePer) {
			case _VOLUME_:
				priceMap.put(c, this.getSettlementPricePerCategory(event, c).multiply(qty));
				break;
			case _EVENT_:
				priceMap.put(c, this.getSettlementPricePerCategory(event, c));
				break;
			case _PERCENT_:
				if (!priceMap.containsKey(c)) {
					BigDecimal cost = this.getPercentageCost(event, priceMap, c, qty);
					priceMap.put(c, cost);
				}
				break;
			case _COMPOSITE_:
				if (!priceMap.containsKey(c)) {
					BigDecimal cost = this.getCompositeCost(event, priceMap, c, qty);
					if (BigDecimal.ZERO.compareTo(cost) != 0) {
						priceMap.put(c, cost);
					}
				}
				break;
			default:
				// do nothing
				break;
			}
		}
		return this.buildCostVat(contract, event, priceMap);
	}

	private CostVat buildCostVat(Contract contract, FuelEvent event, Map<ContractPriceCategory, BigDecimal> priceMap) throws BusinessException {
		BigDecimal cost = BigDecimal.ZERO;
		BigDecimal vat = BigDecimal.ZERO;
		BigDecimal vatValue = this.vatService.getVat(event.getFuelingDate(), event.getDeparture().getCountry());
		for (Entry<ContractPriceCategory, BigDecimal> entry : priceMap.entrySet()) {
			ContractPriceCategory c = entry.getKey();
			if (CalculateIndicator._CALCULATE_ONLY_.equals(c.getCalculateIndicator())) {
				continue;
			}
			cost = cost.add(priceMap.get(c));
			if (this.schouldCalculateVatSum(event.getEventType(), contract, c.getVat(), false)) {
				vat = vat.add(this.calculateVat(vatValue, priceMap.get(c)));
			}
		}
		if (this.schouldCalculateVatSum(event.getEventType(), contract, contract.getVat(), true)) {
			vat = this.calculateVat(vatValue, cost);

		}
		return new CostVat(cost, vat);
	}

	private BigDecimal calculateVat(BigDecimal vatNum, BigDecimal value) {
		return value.multiply(vatNum.divide(BigDecimal.valueOf(100), MathContext.DECIMAL64), MathContext.DECIMAL64);
	}

	private List<ContractPriceCategory> sortPriceCategories(Contract contract) {
		List<ContractPriceCategory> list = new ArrayList<>(contract.getPriceCategories());
		Collections.sort(list, new ContractPriceCategoryComparator());
		return list;
	}

	private BigDecimal getPercentageCost(FuelEvent event, Map<ContractPriceCategory, BigDecimal> priceMap, ContractPriceCategory c, BigDecimal qty)
			throws BusinessException {
		BigDecimal percentage = this.getPercent(event, c);
		BigDecimal sum = BigDecimal.ZERO;
		for (ContractPriceCategory parent : c.getParentPriceCategory()) {
			if (parent.equals(c)) {
				// to not enter in infinite cycle
				continue;
			}
			if (!priceMap.containsKey(parent)) {
				switch (parent.getPriceCategory().getPricePer()) {
				case _VOLUME_:
					priceMap.put(parent, this.getSettlementPricePerCategory(event, parent).multiply(qty));
					break;
				case _EVENT_:
					priceMap.put(parent, this.getSettlementPricePerCategory(event, parent));
					break;
				case _PERCENT_:
					if (!priceMap.containsKey(c)) {
						BigDecimal cost = this.getPercentageCost(event, priceMap, parent, qty);
						priceMap.put(parent, cost);
					}
					break;
				case _COMPOSITE_:
					priceMap.put(parent, this.getSettlementPricePerCategory(event, parent).multiply(qty, MathContext.DECIMAL64));
					break;
				default:
					// do nothing
					break;
				}
			}
			sum = sum.add(priceMap.get(parent));
		}
		return sum.multiply(percentage.divide(ONE_HUNDRED, MathContext.DECIMAL64), MathContext.DECIMAL64);
	}

	private BigDecimal getCompositeCost(FuelEvent event, Map<ContractPriceCategory, BigDecimal> priceMap, ContractPriceCategory c, BigDecimal qty)
			throws BusinessException {
		BigDecimal sum = BigDecimal.ZERO;
		for (ContractPriceCategory parent : c.getParentPriceCategory()) {
			if (parent.equals(c) || PriceInd._COMPOSITE_.equals(parent.getPriceCategory().getPricePer())
					|| (parent.getRestriction() && !this.restrictionSrv.applyRestriction(parent, event))) {
				continue;
			}
			if (!priceMap.containsKey(parent)) {
				switch (parent.getPriceCategory().getPricePer()) {
				case _VOLUME_:
					priceMap.put(parent, this.getSettlementPricePerCategory(event, parent).multiply(qty));
					break;
				case _EVENT_:
					priceMap.put(parent, this.getSettlementPricePerCategory(event, parent));
					break;
				case _PERCENT_:
					priceMap.put(parent, this.getPercentageCost(event, priceMap, parent, qty));
					break;
				case _COMPOSITE_:
				default:
					// do nothing
					break;
				}
			}
			sum = sum.add(priceMap.get(parent));
		}
		return sum;
	}

	/**
	 * Returns the settlement price from contract
	 *
	 * @param event
	 * @param c
	 * @return
	 * @throws BusinessException
	 */
	public BigDecimal getSettlementPricePerCategory(FuelEvent event, ContractPriceCategory c) throws BusinessException {
		BigDecimal price = BigDecimal.ZERO;
		BigDecimal percent = BigDecimal.ZERO;
		BigDecimal finalPrice;

		Date fuelingDate = DateUtils.truncate(event.getFuelingDate(), Calendar.DAY_OF_MONTH);
		List<ContractPriceComponent> components = new ArrayList<>();
		if (PriceInd._COMPOSITE_.equals(c.getPriceCategory().getPricePer())) {
			for (ContractPriceCategory cpc : c.getParentPriceCategory()) {
				if (!cpc.getRestriction() || this.restrictionSrv.applyRestriction(cpc, event)) {
					components.addAll(cpc.getPriceComponents());
				}
			}
		} else {
			components.addAll(c.getPriceComponents());
		}
		for (ContractPriceComponent comp : components) {
			if (comp.getValidFrom().compareTo(fuelingDate) <= 0 && comp.getValidTo().compareTo(fuelingDate) >= 0) {

				if (PriceInd._PERCENT_.equals(comp.getContrPriceCtgry().getPriceCategory().getPricePer())) {
					percent = this.getPercentage(event, price, comp);
					continue;
				}
				price = price.add(this.getPriceFromConverted(fuelingDate, comp));
			}
		}
		finalPrice = price.add(percent);
		return finalPrice;
	}

	private BigDecimal getPercentage(FuelEvent event, BigDecimal price, ContractPriceComponent comp) throws BusinessException {
		BigDecimal percent;
		if (!comp.getContrPriceCtgry().getChildPriceCategory().isEmpty()) {
			percent = this.cpcService.getPriceInCurrencyUnit(comp.getContrPriceCtgry(), comp.getUnit().getCode(), comp.getCurrency().getCode(),
					event.getFuelingDate());
		} else {
			percent = price.add(comp.getPrice());
		}
		return percent;
	}

	private BigDecimal getPercent(FuelEvent event, ContractPriceCategory c) {
		BigDecimal percent = BigDecimal.ZERO;
		Date fuelingDate = DateUtils.truncate(event.getFuelingDate(), Calendar.DAY_OF_MONTH);
		for (ContractPriceComponent comp : c.getPriceComponents()) {
			if (comp.getValidFrom().compareTo(fuelingDate) <= 0 && comp.getValidTo().compareTo(fuelingDate) >= 0
					&& PriceInd._PERCENT_.equals(comp.getContrPriceCtgry().getPriceCategory().getPricePer())) {
				percent = percent.add(comp.getPrice());
			}
		}
		return percent;

	}

	private BigDecimal getPriceFromConverted(Date fuelingDate, ContractPriceComponent comp) {
		for (ContractPriceComponentConv conv : comp.getConvertedPrices()) {
			if (conv.getValidFrom().compareTo(fuelingDate) <= 0 && conv.getValidTo().compareTo(fuelingDate) >= 0) {
				return conv.getEquivalent();
			}
		}
		return BigDecimal.ZERO;
	}

	private BigDecimal getQuantity(Unit unit, FuelEvent event) throws BusinessException {
		Density density = this.getBusinessDelegate(DensityDelegate.class).getDensity(event);
		return this.unitConverterService.convert(event.getUnit(), unit, event.getQuantity(), density);
	}

	private boolean schouldCalculateVatSum(FlightTypeIndicator eventType, Contract contract, VatApplicability vat, boolean isContract) {
		if (eventType == null) {
			return false;
		}
		boolean contractVatNot = (!isContract && contract.getVat().equals(VatApplicability._NOT_APPLICABLE_))
				|| (isContract && !contract.getVat().equals(VatApplicability._NOT_APPLICABLE_));
		boolean allEvents = vat.equals(VatApplicability._ALL_EVENTS_) && FlightTypeIndicator._UNSPECIFIED_.equals(eventType);
		boolean domestic = FlightTypeIndicator._DOMESTIC_.equals(eventType)
				&& (vat.equals(VatApplicability._ALL_EVENTS_) || vat.equals(VatApplicability._DOMESTIC_EVENTS_));
		boolean international = FlightTypeIndicator._INTERNATIONAL_.equals(eventType)
				&& (vat.equals(VatApplicability._ALL_EVENTS_) || vat.equals(VatApplicability._INTERNATIONAL_EVENTS_));
		return contractVatNot && (allEvents || domestic || international);
	}

	private CostVat convert(FuelOrderLocation orderLocation, Currencies paymentCurrency, CostVat costVat, Date date) throws BusinessException {
		Contract contract = orderLocation.getContract();
		FinancialSources finSrc = orderLocation.getFinancialSource();
		AverageMethod avgMth = orderLocation.getAverageMethod();
		ExchangeRate_Bd bd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		BigDecimal cost = bd.convert(contract.getSettlementCurr(), paymentCurrency, date, costVat.getCost(), false, avgMth, finSrc).getValue();
		BigDecimal vat = bd.convert(contract.getSettlementCurr(), paymentCurrency, date, costVat.getVat(), false, avgMth, finSrc).getValue();
		return new CostVat(cost, vat);
	}

}
