package atraxo.acc.business.ws;

public enum FuelEventDealTypeAliasEnum {

	SALE("FuelEvent-Sale"), PURCHASE("FuelEvent-Purchase");

	private String customEntityAlias;

	private FuelEventDealTypeAliasEnum(String customEntityAlias) {
		this.customEntityAlias = customEntityAlias;
	}

	public String getCustomEntityAlias() {
		return this.customEntityAlias;
	}

}
