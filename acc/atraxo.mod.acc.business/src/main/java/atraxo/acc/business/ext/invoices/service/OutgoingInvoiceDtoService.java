package atraxo.acc.business.ext.invoices.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.efaps.number2words.Converter;
import org.efaps.number2words.IConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.domain.ext.mailmerge.dto.OutgoingInvoiceDto;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateValueService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.profile.IBankAccountService;
import atraxo.fmbas.business.api.timeserie.ITimeSerieService;
import atraxo.fmbas.business.api.vat.IVatService;
import atraxo.fmbas.business.ext.dto.AbstractDataDtoService;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRateValue;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.profile.BankAccount;
import atraxo.fmbas.domain.impl.timeserie.TimeSerie;
import atraxo.fmbas.domain.impl.vat.Vat;
import atraxo.fmbas.domain.impl.vat.VatRate;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;

public class OutgoingInvoiceDtoService extends AbstractDataDtoService<OutgoingInvoice, OutgoingInvoiceDto> {

	private static final Logger LOGGER = LoggerFactory.getLogger(OutgoingInvoiceDtoService.class);

	@Autowired
	private IContractService contractService;

	@Autowired
	private IFuelOrderLocationService fuelOrderLocationService;

	@Autowired
	private IExchangeRateService exchangeRateService;

	@Autowired
	private ITimeSerieService timeSerieService;

	@Autowired
	private IExchangeRateValueService exchangeRateValueService;

	@Autowired
	private IBankAccountService bankAccountService;

	@Autowired
	private IVatService vatService;

	@Autowired
	private ICurrenciesService currencyService;

	@Autowired
	private IFinancialSourcesService financialSourceService;

	@Override
	public void addCustomValues(OutgoingInvoice invoice, OutgoingInvoiceDto dto) {
		// Set header vat rate
		this.setHeaderVatRate(invoice, dto);

		// Set issuer bank account
		this.setIssuerBankAccount(invoice, dto);

		// Set receiver office details
		this.setReceiverOfficeDetails(invoice, dto);

		// Set payment terms
		this.setPaymentTerms(invoice, dto);

		// Set TZS USD echange rate value
		this.setExchangeRateValue(invoice, dto);

		// Set total amount in words
		this.setTotalAmountInWords(dto);
	}

	private void setHeaderVatRate(OutgoingInvoice invoice, OutgoingInvoiceDto dto) {
		try {
			Vat vat = this.vatService.findByCountry(invoice.getCountry()).get(0);
			for (VatRate rate : vat.getVatRate()) {
				if (rate.getValidFrom().before(invoice.getInvoiceDate()) && rate.getValidTo().after(invoice.getInvoiceDate())) {
					dto.getHeader().setVatRate(rate.getRate());
				}
			}
		} catch (Exception e) {
			LOGGER.info("No vat for country " + invoice.getCountry().getCode(), e);
		}
	}

	private void setIssuerBankAccount(OutgoingInvoice invoice, OutgoingInvoiceDto dto) {
		Map<String, Object> params = new HashMap<>();
		params.put("subsidiary", invoice.getIssuer());
		params.put("currency", invoice.getCurrency());

		try {
			BankAccount bankAccount = this.bankAccountService.findEntityByAttributes(params);
			if (dto.getIssuer() != null) {
				dto.getIssuer().setBankName(bankAccount.getBankName());
				dto.getIssuer().setBankAddress(bankAccount.getBankAdress());
				dto.getIssuer().setBankAccountHolder(bankAccount.getAccountHolder());
				dto.getIssuer().setBankAccountNr(bankAccount.getAccountNumber());
				dto.getIssuer().setBankIbanNr(bankAccount.getIbanCode());
				dto.getIssuer().setBankSwiftNr(bankAccount.getSwiftCode());
				dto.getIssuer().setBankAccountCurrency(bankAccount.getCurrency().getCode());
			}
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOGGER.info("No bank account found: subsidiary " + invoice.getIssuer().getCode() + " - currency " + invoice.getCurrency().getCode(),
						ex);
			} else {
				throw ex;
			}
		}
	}

	private void setReceiverOfficeDetails(OutgoingInvoice invoice, OutgoingInvoiceDto dto) {
		if (invoice.getReceiver() != null && dto.getReceiver() != null && invoice.getReceiver().getUseForBilling()) {
			dto.getReceiver().setOfficeStreet(invoice.getReceiver().getOfficeStreet());
			dto.getReceiver().setOfficeCity(invoice.getReceiver().getOfficeCity());
			dto.getReceiver().setOfficePostalCode(invoice.getReceiver().getOfficePostalCode());
			dto.getReceiver().setOfficeCountryName(invoice.getReceiver().getOfficeCountry().getName());
		}
	}

	private void setPaymentTerms(OutgoingInvoice invoice, OutgoingInvoiceDto dto) {
		switch (invoice.getReferenceDocType()) {
		case _CONTRACT_:
			this.setContractPaymentTerms(invoice, dto);
			break;
		case _PURCHASE_ORDER_:
			this.setOrderPaymentTerms(invoice, dto);
			break;
		default:
			break;
		}
	}

	private void setExchangeRateValue(OutgoingInvoice invoice, OutgoingInvoiceDto dto) {
		try {
			Currencies tzs = this.currencyService.findByCode("TZS");
			Currencies usd = this.currencyService.findByCode("USD");
			FinancialSources nav = this.financialSourceService.findByCode("NAV");

			// Find time serie
			Map<String, Object> timeSerieParams = new HashMap<>();
			timeSerieParams.put("currency1Id", tzs);
			timeSerieParams.put("currency2Id", usd);
			timeSerieParams.put("financialSource", nav);

			TimeSerie timeSerie = this.timeSerieService.findEntityByAttributes(timeSerieParams);

			// Find exchange rate
			Map<String, Object> exchangeRateParams = new HashMap<>();
			exchangeRateParams.put("currency1", tzs);
			exchangeRateParams.put("currency2", usd);
			exchangeRateParams.put("finsource", nav);
			exchangeRateParams.put("tserie", timeSerie);

			ExchangeRate exchangeRate = this.exchangeRateService.findEntityByAttributes(exchangeRateParams);
			ExchangeRateValue exchangeRateValue = this.exchangeRateValueService.findByBusinessKey(exchangeRate, invoice.getInvoiceDate(),
					invoice.getInvoiceDate());

			if (dto.getHeader() != null) {
				dto.getHeader().setTzsUsdRate(exchangeRateValue.getRate());
			}
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOGGER.info("No exchange rate found: currency1 = TZS - currency2 = USD - financialSource = NAV ", ex);
			} else if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(ex.getErrorCode())) {
				LOGGER.info("Multiple exchange rates found: currency1 = TZS - currency2 = USD - financialSource = NAV ", ex);
			}
		}
	}

	private void setContractPaymentTerms(OutgoingInvoice invoice, OutgoingInvoiceDto dto) {
		try {
			Contract contract = this.contractService.findById(invoice.getReferenceDocId());
			if (dto.getHeader() != null) {
				dto.getHeader().setPaymentTerms(contract.getPaymentTerms());
				dto.getHeader().setPaymentTermsReference(contract.getPaymentRefDay());
			}
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOGGER.info("No contract found: nr " + invoice.getReferenceDocNo(), ex);
			} else {
				throw ex;
			}

		}
	}

	private void setOrderPaymentTerms(OutgoingInvoice invoice, OutgoingInvoiceDto dto) {
		try {
			FuelOrderLocation fuelOrderLocation = this.fuelOrderLocationService.findById(invoice.getReferenceDocId());
			if (dto.getHeader() != null) {
				dto.getHeader().setPaymentTerms(fuelOrderLocation.getPaymentTerms());
				dto.getHeader().setPaymentTermsReference(fuelOrderLocation.getPaymentRefDay());
			}
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOGGER.info("No fuel order location found: nr " + invoice.getReferenceDocNo(), ex);
			} else {
				throw ex;
			}

		}
	}

	private void setTotalAmountInWords(OutgoingInvoiceDto dto) {
		Locale locale = new Locale("es");
		IConverter converter = Converter.getMaleConverter(locale);

		BigDecimal totalAmount = dto.getHeader().getTotalAmount();
		StringBuilder totalAmountInWords = new StringBuilder();

		totalAmountInWords.append(converter.convert(totalAmount.longValue()));
		totalAmountInWords.append(" US DOLARES CON ");
		totalAmountInWords.append(converter.convert(totalAmount.remainder(BigDecimal.ONE).movePointRight(2).abs().longValue()));
		totalAmountInWords.append(" CENTAVOS");

		dto.getHeader().setTotalAmountInWords(totalAmountInWords.toString().toUpperCase());
	}

}