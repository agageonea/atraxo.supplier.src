package atraxo.acc.business.ws.acknowledgment.services;

import java.io.IOException;
import java.util.concurrent.RejectedExecutionException;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.xml.sax.SAXException;

import atraxo.acc.business.api.ext.fuelEvents.IFuelEventAccepter;
import atraxo.acc.business.api.ext.invoices.service.IInvoiceAccepter;
import atraxo.acc.business.api.ext.invoices.service.IRicohAccepter;
import atraxo.fmbas.domain.impl.fmbas_type.ProcessStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TransportStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
@WebService
public class AcknowledgeAccountInfoWsImpl implements AcknowledgeAccountInfoWs {

	private static final Logger LOG = LoggerFactory.getLogger(AcknowledgeAccountInfoWsImpl.class);

	@Autowired
	private IFuelEventAccepter feAccepter;
	@Autowired
	private IInvoiceAccepter invAccepter;
	@Autowired
	private IRicohAccepter ricohAccepter;

	@Override
	public MSG acknowledgeFuelEventsNav(MSG request) throws JAXBException, SAXException, IOException, BusinessException {
		if (LOG.isInfoEnabled()) {
			LOG.info("START acknowledgeFuelEventsNav()");
		}

		try {
			this.feAccepter.process(request, "FuelEventAccepter", "acknoledge");
		} catch (RejectedExecutionException e) {
			LOG.warn("Request Rejected (Acknowledge Fuel Event Nav) - Bandwidth Exceeded : MessageId = " + request.getHeaderCommon().getMsgID(), e);

			request.getHeaderCommon().setTransportStatus(TransportStatus._FAILURE_.getName());
			request.getHeaderCommon().setProcessStatus(ProcessStatus._FAILURE_.getName());
			request.getHeaderCommon().setErrorCode(Integer.toString(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value()));
			request.getHeaderCommon().setErrorDescription(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.getReasonPhrase());

			return request;
		}

		request.getHeaderCommon().setTransportStatus(TransportStatus._PROCESSED_.getName());
		request.getHeaderCommon().setProcessStatus(ProcessStatus._SUCCESS_.getName());

		if (LOG.isInfoEnabled()) {
			LOG.info("STOP acknowledgeFuelEventsNav()");
		}
		return request;
	}

	@Override
	public MSG acknowledgeInvoicesNav(MSG request) throws JAXBException, SAXException, IOException, BusinessException {
		if (LOG.isInfoEnabled()) {
			LOG.info("START acknowledgeInvoicesNav()");
		}

		try {
			this.invAccepter.process(request, "InvoiceAccepter", "acknoledgeInvoice");
		} catch (RejectedExecutionException e) {
			LOG.warn("Request Rejected (Acknowledge Invoices Nav) - Bandwidth Exceeded : MessageId = " + request.getHeaderCommon().getMsgID(), e);

			request.getHeaderCommon().setTransportStatus(TransportStatus._FAILURE_.getName());
			request.getHeaderCommon().setProcessStatus(ProcessStatus._FAILURE_.getName());
			request.getHeaderCommon().setErrorCode(Integer.toString(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value()));
			request.getHeaderCommon().setErrorDescription(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.getReasonPhrase());

			return request;
		}

		request.getHeaderCommon().setTransportStatus(TransportStatus._PROCESSED_.getName());
		request.getHeaderCommon().setProcessStatus(ProcessStatus._SUCCESS_.getName());

		if (LOG.isInfoEnabled()) {
			LOG.info("STOP acknowledgeInvoicesNav()");
		}

		return request;
	}

	@Override
	public MSG acknowledgeCreditMemosNav(MSG request) throws JAXBException, SAXException, IOException, BusinessException {
		if (LOG.isInfoEnabled()) {
			LOG.info("START acknowledgeCreditMemosNav()");
		}

		try {
			this.invAccepter.process(request, "InvoiceAccepter", "acknoledgeCreditMemo");
		} catch (RejectedExecutionException e) {
			LOG.warn("Request Rejected (Acknowledge Credit Memo Nav) - Bandwidth Exceeded : MessageId = " + request.getHeaderCommon().getMsgID(), e);

			request.getHeaderCommon().setTransportStatus(TransportStatus._FAILURE_.getName());
			request.getHeaderCommon().setProcessStatus(ProcessStatus._FAILURE_.getName());
			request.getHeaderCommon().setErrorCode(Integer.toString(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value()));
			request.getHeaderCommon().setErrorDescription(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.getReasonPhrase());

			return request;
		}

		request.getHeaderCommon().setTransportStatus(TransportStatus._PROCESSED_.getName());
		request.getHeaderCommon().setProcessStatus(ProcessStatus._SUCCESS_.getName());

		if (LOG.isInfoEnabled()) {
			LOG.info("STOP acknowledgeCreditMemosNav()");
		}
		return request;
	}

	@Override
	public MSG acknowledgeRicoh(MSG request) throws JAXBException, SAXException, IOException, BusinessException {
		if (LOG.isInfoEnabled()) {
			LOG.info("START acknowledgeInvoicesNavResponse()");
		}

		try {
			this.ricohAccepter.process(request, "RicohAccepter", "acknowledgeRicohSentFiles");
		} catch (RejectedExecutionException e) {
			LOG.warn(
					"Request Rejected (Acknowledge Invoice Sent to Ricoh) - Bandwidth Exceeded : MessageId = " + request.getHeaderCommon().getMsgID(),
					e);

			request.getHeaderCommon().setTransportStatus(TransportStatus._FAILURE_.getName());
			request.getHeaderCommon().setProcessStatus(ProcessStatus._FAILURE_.getName());
			request.getHeaderCommon().setErrorCode(Integer.toString(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value()));
			request.getHeaderCommon().setErrorDescription(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.getReasonPhrase());

			return request;
		}

		request.getHeaderCommon().setTransportStatus(TransportStatus._PROCESSED_.getName());
		request.getHeaderCommon().setProcessStatus(ProcessStatus._SUCCESS_.getName());

		if (LOG.isInfoEnabled()) {
			LOG.info("STOP acknowledgeInvoicesNavResponse()");
		}
		return request;
	}
}
