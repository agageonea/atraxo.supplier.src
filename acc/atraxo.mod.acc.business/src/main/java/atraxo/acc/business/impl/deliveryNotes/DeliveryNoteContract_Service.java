/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.deliveryNotes;

import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.cmm.domain.impl.contracts.Contract;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DeliveryNoteContract} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DeliveryNoteContract_Service
		extends
			AbstractEntityService<DeliveryNoteContract> {

	/**
	 * Public constructor for DeliveryNoteContract_Service
	 */
	public DeliveryNoteContract_Service() {
		super();
	}

	/**
	 * Public constructor for DeliveryNoteContract_Service
	 * 
	 * @param em
	 */
	public DeliveryNoteContract_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DeliveryNoteContract> getEntityClass() {
		return DeliveryNoteContract.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DeliveryNoteContract
	 */
	public DeliveryNoteContract findByDeliveryNotesContracts(
			Contract contracts, DeliveryNote deliveryNotes) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							DeliveryNoteContract.NQ_FIND_BY_DELIVERYNOTESCONTRACTS,
							DeliveryNoteContract.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contracts", contracts)
					.setParameter("deliveryNotes", deliveryNotes)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DeliveryNoteContract", "contracts, deliveryNotes"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DeliveryNoteContract", "contracts, deliveryNotes"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DeliveryNoteContract
	 */
	public DeliveryNoteContract findByDeliveryNotesContracts(Long contractsId,
			Long deliveryNotesId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							DeliveryNoteContract.NQ_FIND_BY_DELIVERYNOTESCONTRACTS_PRIMITIVE,
							DeliveryNoteContract.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contractsId", contractsId)
					.setParameter("deliveryNotesId", deliveryNotesId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DeliveryNoteContract",
							"contractsId, deliveryNotesId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DeliveryNoteContract",
							"contractsId, deliveryNotesId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DeliveryNoteContract
	 */
	public DeliveryNoteContract findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DeliveryNoteContract.NQ_FIND_BY_BUSINESS,
							DeliveryNoteContract.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DeliveryNoteContract", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DeliveryNoteContract", "id"), nure);
		}
	}

	/**
	 * Find by reference: contracts
	 *
	 * @param contracts
	 * @return List<DeliveryNoteContract>
	 */
	public List<DeliveryNoteContract> findByContracts(Contract contracts) {
		return this.findByContractsId(contracts.getId());
	}
	/**
	 * Find by ID of reference: contracts.id
	 *
	 * @param contractsId
	 * @return List<DeliveryNoteContract>
	 */
	public List<DeliveryNoteContract> findByContractsId(Integer contractsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNoteContract e where e.clientId = :clientId and e.contracts.id = :contractsId",
						DeliveryNoteContract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractsId", contractsId).getResultList();
	}
	/**
	 * Find by reference: deliveryNotes
	 *
	 * @param deliveryNotes
	 * @return List<DeliveryNoteContract>
	 */
	public List<DeliveryNoteContract> findByDeliveryNotes(
			DeliveryNote deliveryNotes) {
		return this.findByDeliveryNotesId(deliveryNotes.getId());
	}
	/**
	 * Find by ID of reference: deliveryNotes.id
	 *
	 * @param deliveryNotesId
	 * @return List<DeliveryNoteContract>
	 */
	public List<DeliveryNoteContract> findByDeliveryNotesId(
			Integer deliveryNotesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNoteContract e where e.clientId = :clientId and e.deliveryNotes.id = :deliveryNotesId",
						DeliveryNoteContract.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("deliveryNotesId", deliveryNotesId)
				.getResultList();
	}
}
