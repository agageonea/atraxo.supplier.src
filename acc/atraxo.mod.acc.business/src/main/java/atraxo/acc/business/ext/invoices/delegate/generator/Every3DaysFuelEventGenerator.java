/**
 *
 */
package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.utils.DateUtils;

/**
 * @author vhojda
 */
public class Every3DaysFuelEventGenerator extends AbstractFuelEventGenerator {

	/**
	 *
	 */
	public Every3DaysFuelEventGenerator() {
		// The explicit default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#filterByFrequency(java.util.Collection)
	 */
	@Override
	public void filterByFrequency(Collection<FuelEvent> events) {
		Calendar presentCal = DateUtils.getPresentIgnoreHours();

		int comparissonDay = -1;
		int presentDay = presentCal.get(Calendar.DAY_OF_MONTH);

		if (presentDay % 3 == 0) {// 3,6,9,12,....
			comparissonDay = presentDay - 2;// substract 2 days
		} else if (presentDay % 3 == 2) {// 2,5,8,11,14,...
			comparissonDay = presentDay - 1;// substract 1 day
		} else if (presentDay % 3 == 1) {// 1,4,7,10,...
			comparissonDay = presentDay;// do not substract any days
		}

		Calendar comparissonCal = new GregorianCalendar(presentCal.get(Calendar.YEAR), presentCal.get(Calendar.MONTH), comparissonDay);

		for (Iterator<FuelEvent> it = events.iterator(); it.hasNext();) {
			FuelEvent event = it.next();

			Calendar eventCal = DateUtils.getCalendarFromDateIgnoreHours(event.getFuelingDate());

			// if NOT in the correct interval then remove it
			if (DateUtils.compareDates(eventCal, comparissonCal, true) >= 0) {
				it.remove();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#splitByFrequency(java.util.List)
	 */
	@Override
	public Map<InvoicingDatePeriod, List<FuelEvent>> splitByFrequency(List<FuelEvent> events) {
		Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap = new HashMap<>();

		if (!events.isEmpty()) {
			FuelEvent firstFuelEvent = events.get(0);
			FuelEvent lastFuelEvent = events.get(events.size() - 1);

			Calendar firstFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(firstFuelEvent.getFuelingDate());
			Calendar lastFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(lastFuelEvent.getFuelingDate());

			int firstMonth = firstFuelEventCal.get(Calendar.YEAR) * 12 + firstFuelEventCal.get(Calendar.MONTH);
			int lastMonth = lastFuelEventCal.get(Calendar.YEAR) * 12 + lastFuelEventCal.get(Calendar.MONTH);

			// for each month put collections of events
			int diff = lastMonth - firstMonth;
			for (int i = 0; i <= diff; i++) {
				// the start day of the first period calendar is 1st of the month
				Calendar startDayMonthCal = DateUtils.getCalendarFromCalendarIgnoreHours(firstFuelEventCal);
				startDayMonthCal.add(Calendar.MONTH, i);
				startDayMonthCal.set(Calendar.DAY_OF_MONTH, 1);

				// the end day of the first period calendar is the 3rd of the month
				Calendar endDayMonthCal = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal);
				endDayMonthCal.add(Calendar.DAY_OF_MONTH, 2);

				// calculate the maximum date for this month
				Calendar maxMonthDate = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal);
				maxMonthDate.set(Calendar.DAY_OF_MONTH, maxMonthDate.getActualMaximum(Calendar.DAY_OF_MONTH));

				// add the first period
				eventsMap.put(new InvoicingDatePeriod(startDayMonthCal, endDayMonthCal), null);

				boolean inNextMonth = false;
				while (!inNextMonth) {
					// save the old calendars because the new one will change
					Calendar oldStartCal = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal);
					Calendar oldEndCal = DateUtils.getCalendarFromCalendarIgnoreHours(endDayMonthCal);

					// go to the next interval of 3 days
					startDayMonthCal.set(Calendar.DAY_OF_MONTH, endDayMonthCal.get(Calendar.DAY_OF_MONTH) + 1);
					endDayMonthCal.add(Calendar.DAY_OF_MONTH, 3);

					// check if maybe we passed the current month
					inNextMonth = endDayMonthCal.after(maxMonthDate) || startDayMonthCal.after(maxMonthDate);

					if (!inNextMonth) {
						// add new invoice period
						eventsMap.put(new InvoicingDatePeriod(startDayMonthCal, endDayMonthCal), null);

					} else {
						// treat only the cases when only one of the date is in the new month,
						// because otherwise all the periods were treated so far
						if (!(endDayMonthCal.after(maxMonthDate) && startDayMonthCal.after(maxMonthDate))) {
							// get the old values for the calendars
							startDayMonthCal = oldStartCal;
							endDayMonthCal = oldEndCal;

							// new start=old end+1 ; new end=maximum of the month
							startDayMonthCal.set(Calendar.DAY_OF_MONTH, endDayMonthCal.get(Calendar.DAY_OF_MONTH) + 1);
							endDayMonthCal.set(Calendar.DAY_OF_MONTH, endDayMonthCal.getActualMaximum(Calendar.DAY_OF_MONTH));

							// add new invoice period
							eventsMap.put(new InvoicingDatePeriod(startDayMonthCal, endDayMonthCal), null);
						}
					}

				}

				// fill in the events map depending on the period with events
				this.fillFuelEventsPeriodMap(eventsMap, events);

			}
		}
		return eventsMap;
	}

}
