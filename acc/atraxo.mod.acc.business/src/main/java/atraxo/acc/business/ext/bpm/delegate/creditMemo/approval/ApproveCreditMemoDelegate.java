/**
 *
 */
package atraxo.acc.business.ext.bpm.delegate.creditMemo.approval;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;

/**
 * @author vhojda
 */
public class ApproveCreditMemoDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApproveCreditMemoDelegate.class);

	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceSrv;

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.bpm.delegate.AbstractJavaDelegate#execute()
	 */
	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		// update the BID and the history
		Object outInvoiceIdString = this.execution.getVariable(WorkflowVariablesConstants.INVOICE_ID_VAR);
		if (outInvoiceIdString != null) {
			Integer outInvoiceId = Integer.parseInt(outInvoiceIdString.toString());
			OutgoingInvoice outInvoice = null;
			try {
				outInvoice = this.outgoingInvoiceSrv.findByBusiness(outInvoiceId);
			} catch (Exception e) {
				LOGGER.error("ERROR:could not find an outgoing invoice for ID " + outInvoiceId, e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						"Could not find an outgoing invoice for ID " + outInvoiceId);
			}
			if (outInvoice != null) {
				// approve the credit memo
				this.outgoingInvoiceSrv.approve(Arrays.asList(outInvoice), this.getNote());
			}

		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}

}
