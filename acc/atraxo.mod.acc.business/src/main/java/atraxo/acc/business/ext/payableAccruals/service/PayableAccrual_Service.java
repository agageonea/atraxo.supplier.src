/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.ext.payableAccruals.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.payableAccruals.IPayableAccrualService;
import atraxo.acc.business.ext.payableAccruals.delegate.PayableAccrualGenerate_Bd;
import atraxo.acc.business.ext.payableAccruals.delegate.PayableAccrualUpdate_Bd;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.cmm.domain.impl.contracts.Contract;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link PayableAccrual} domain entity.
 */
public class PayableAccrual_Service extends atraxo.acc.business.impl.payableAccruals.PayableAccrual_Service implements IPayableAccrualService {

	@Override
	public void generatePayableAccruals(DeliveryNote deliveryNote, Contract contract) throws BusinessException {
		PayableAccrualGenerate_Bd generateBd = this.getBusinessDelegate(PayableAccrualGenerate_Bd.class);
		generateBd.generatePayableAccrual(deliveryNote, contract);
	}

	@Override
	public void updatePayableAccruals(PayableAccrual pa) throws BusinessException {
		PayableAccrualUpdate_Bd paUpdateBd = this.getBusinessDelegate(PayableAccrualUpdate_Bd.class);
		paUpdateBd.updatePayableAccrual(pa);
	}

	@Override
	public List<PayableAccrual> findAll() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		return this.findEntitiesByAttributes(params);
	}
}
