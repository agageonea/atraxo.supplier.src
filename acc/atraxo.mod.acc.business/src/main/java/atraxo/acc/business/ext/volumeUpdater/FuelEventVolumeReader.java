package atraxo.acc.business.ext.volumeUpdater;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteContractService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.business.utils.DateUtils;

public class FuelEventVolumeReader implements ItemReader<Map<FuelEvent, List<Contract>>> {

	private static final String STATUS = "status";
	@Autowired
	private IContractService contractService;
	@Autowired
	private IFuelEventService fuelEventService;
	@Autowired
	private IDeliveryNoteContractService deliveryNoteContractService;
	@Autowired
	ISystemParameterService sysParamService;
	@Autowired
	IUnitService unitService;

	private StepExecution stepExecution;
	private Integer offset;

	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	@Override
	public Map<FuelEvent, List<Contract>> read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		String offsetStr = this.stepExecution.getJobExecution().getJobParameters().getString("Expiring date offset");
		Object firstRun = this.stepExecution.getJobExecution().getExecutionContext().get("run");
		if (firstRun != null) {
			return null;
		}
		this.offset = Integer.parseInt(offsetStr);
		List<Contract> contracts = this.getAllEfectiveContracts();
		List<Contract> expiredContracts = this.getAllExpiredContracts(this.offset);
		contracts.addAll(expiredContracts);
		this.stepExecution.getJobExecution().getExecutionContext().put("run", 1);
		return this.collectFuelEvents(contracts);
	}

	private Map<FuelEvent, List<Contract>> collectFuelEvents(List<Contract> contracts) throws BusinessException {
		Map<FuelEvent, List<Contract>> retList = new HashMap<>();
		for (Contract contract : contracts) {
			List<FuelEvent> events = new ArrayList<>();
			if (DealType._SELL_.equals(contract.getDealType())) {
				events.addAll(this.getBySellContract(contract));
			} else {
				events.addAll(this.getByBuyContract(contract));
			}
			for (FuelEvent event : events) {
				if (retList.containsKey(event)) {
					retList.get(event).add(contract);
				} else {
					List<Contract> contractList = new ArrayList<>();
					contractList.add(contract);
					retList.put(event, contractList);
				}
			}
			if (events.isEmpty()) {
				this.addEmptyFuelEvent(retList, contract);
			}
		}
		return retList.isEmpty() ? null : retList;
	}

	private void addEmptyFuelEvent(Map<FuelEvent, List<Contract>> map, Contract contract) throws BusinessException {
		boolean found = false;
		for (Entry<FuelEvent, List<Contract>> entry : map.entrySet()) {
			if (entry.getKey().getId() == null) {
				found = entry.getValue().add(contract);
			}
		}
		if (!found) {
			FuelEvent fuelEvent = new FuelEvent();
			fuelEvent.setQuantity(BigDecimal.ZERO);
			String unitStr = this.sysParamService.getSysVol();
			Unit unit = this.unitService.findByCode(unitStr);
			fuelEvent.setUnit(unit);
			List<Contract> contracts = new ArrayList<>();
			contracts.add(contract);
			map.put(fuelEvent, contracts);
		}
	}

	private List<FuelEvent> getByBuyContract(Contract contract) throws BusinessException {
		List<DeliveryNoteContract> noteContracts = this.deliveryNoteContractService.findByContract(contract);
		List<FuelEvent> events = new ArrayList<>();
		for (DeliveryNoteContract dnc : noteContracts) {
			DeliveryNote dn = dnc.getDeliveryNotes();
			events.add(dn.getFuelEvent());
		}
		this.filterFuelEvent(events);
		return events;
	}

	private List<FuelEvent> getBySellContract(Contract contract) {
		Map<String, Object> params = new HashMap<>();
		params.put("billPriceSourceId", contract.getId());
		params.put("billPriceSourceType", Contract.class.getSimpleName());
		List<FuelEvent> events = this.fuelEventService.findEntitiesByAttributes(params);
		this.filterFuelEvent(events);
		return events;
	}

	private void filterFuelEvent(List<FuelEvent> events) {
		Iterator<FuelEvent> iter = events.iterator();
		while (iter.hasNext()) {
			FuelEvent event = iter.next();
			if (FuelEventStatus._CANCELED_.equals(event.getStatus())) {
				iter.remove();
			}
		}
	}

	private List<Contract> getAllEfectiveContracts() {
		Map<String, Object> params = new HashMap<>();
		params.put(STATUS, ContractStatus._EFFECTIVE_);
		List<Contract> retList = this.contractService.findEntitiesByAttributes(params);
		this.filterContracts(retList);
		return retList;
	}

	private void filterContracts(List<Contract> oldList) {
		if (CollectionUtils.isEmpty(oldList)) {
			return;
		}
		Iterator<Contract> iter = oldList.iterator();
		while (iter.hasNext()) {
			Contract temp = iter.next();
			if (CollectionUtils.isEmpty(temp.getShipTo())) {
				iter.remove();
			}
		}
	}

	private List<Contract> getAllExpiredContracts(int offset) {
		Date offsetDate = DateUtils.modifyDate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_YEAR, offset * (-1));
		String hql = "select e from " + Contract.class.getSimpleName()
				+ " e where e.clientId =:clientId and e.status =:status and e.validTo >=:validTo";
		List<Contract> contracts = this.contractService.getEntityManager().createQuery(hql, Contract.class)
				.setParameter("clientId", Session.user.get().getClient().getId()).setParameter(STATUS, ContractStatus._EXPIRED_)
				.setParameter("validTo", offsetDate).getResultList();
		this.filterContracts(contracts);
		return contracts;
	}
}
