/**
 *
 */
package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.utils.DateUtils;

/**
 * @author vhojda
 */
public class Every20DaysFuelEventGenerator extends AbstractFuelEventGenerator {

	/**
	 *
	 */
	public Every20DaysFuelEventGenerator() {
		// The explicit default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#filterByFrequency(java.util.Collection)
	 */
	@Override
	public void filterByFrequency(Collection<FuelEvent> events) {
		Calendar presentCal = DateUtils.getPresentIgnoreHours();

		// the first day of the last month
		Calendar startDayCal = Calendar.getInstance();
		startDayCal.setTime(presentCal.getTime());
		startDayCal.add(Calendar.MONTH, -1);
		startDayCal.set(Calendar.DAY_OF_MONTH, 1);

		// 1st of the last month
		Calendar first2MonthsCalPeriod = Calendar.getInstance();
		first2MonthsCalPeriod.setTime(startDayCal.getTime());

		// 21th of the last month
		Calendar second2MonthsCalPeriod = Calendar.getInstance();
		second2MonthsCalPeriod.setTime(first2MonthsCalPeriod.getTime());
		second2MonthsCalPeriod.add(Calendar.DAY_OF_MONTH, 20);

		// 8-11 th of the current month
		Calendar third2MonthsCalPeriod = Calendar.getInstance();
		third2MonthsCalPeriod.setTime(second2MonthsCalPeriod.getTime());
		third2MonthsCalPeriod.add(Calendar.DAY_OF_MONTH, 20);

		// find out the comparisson calendar day
		Calendar comparissonCal = DateUtils.compareDates(presentCal, third2MonthsCalPeriod, true) >= 0 ? third2MonthsCalPeriod
				: second2MonthsCalPeriod;

		// exclude the ones that are not in the proper period
		for (Iterator<FuelEvent> it = events.iterator(); it.hasNext();) {
			FuelEvent event = it.next();

			Calendar eventCal = DateUtils.getCalendarFromDateIgnoreHours(event.getFuelingDate());

			// if NOT in the correct interval then remove it
			if (DateUtils.compareDates(eventCal, comparissonCal, true) >= 0) {
				it.remove();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#splitByFrequency(java.util.List)
	 */
	@Override
	public Map<InvoicingDatePeriod, List<FuelEvent>> splitByFrequency(List<FuelEvent> events) {
		Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap = new HashMap<>();

		if (!events.isEmpty()) {
			FuelEvent firstFuelEvent = events.get(0);
			FuelEvent lastFuelEvent = events.get(events.size() - 1);

			Calendar firstFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(firstFuelEvent.getFuelingDate());
			Calendar lastFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(lastFuelEvent.getFuelingDate());

			int firstMonth = firstFuelEventCal.get(Calendar.YEAR) * 12 + firstFuelEventCal.get(Calendar.MONTH);
			int lastMonth = lastFuelEventCal.get(Calendar.YEAR) * 12 + lastFuelEventCal.get(Calendar.MONTH);

			// for each month put 3 collections of events
			int diff = lastMonth - firstMonth;
			for (int i = 0; i <= diff; i = i + 2) {
				Calendar startDayMonthCal1 = DateUtils.getCalendarFromCalendarIgnoreHours(firstFuelEventCal);
				startDayMonthCal1.add(Calendar.MONTH, i);
				startDayMonthCal1.set(Calendar.DAY_OF_MONTH, 1);
				Calendar endDayMonthCal1 = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal1);
				endDayMonthCal1.set(Calendar.DAY_OF_MONTH, 20);

				Calendar startDayMonthCal2 = DateUtils.getCalendarFromCalendarIgnoreHours(endDayMonthCal1);
				startDayMonthCal2.set(Calendar.DAY_OF_MONTH, 21);
				Calendar endDayMonthCal2 = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal2);
				endDayMonthCal2.add(Calendar.DAY_OF_MONTH, 19);

				Calendar startDayMonthCal3 = DateUtils.getCalendarFromCalendarIgnoreHours(endDayMonthCal2);
				startDayMonthCal3.add(Calendar.DAY_OF_MONTH, 1);
				Calendar endDayMonthCal3 = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal3);
				endDayMonthCal3.set(Calendar.DAY_OF_MONTH, startDayMonthCal3.getActualMaximum(Calendar.DAY_OF_MONTH));

				eventsMap.put(new InvoicingDatePeriod(startDayMonthCal1, endDayMonthCal1), null);

				if (!(startDayMonthCal2.after(lastFuelEventCal) || endDayMonthCal2.after(lastFuelEventCal))) {
					eventsMap.put(new InvoicingDatePeriod(startDayMonthCal2, endDayMonthCal2), null);
				}
				if (!(startDayMonthCal3.after(lastFuelEventCal) || endDayMonthCal3.after(lastFuelEventCal))) {
					eventsMap.put(new InvoicingDatePeriod(startDayMonthCal3, endDayMonthCal3), null);
				}
			}

			// fill in the events map depending on the period with events
			this.fillFuelEventsPeriodMap(eventsMap, events);
		}

		return eventsMap;
	}

}
