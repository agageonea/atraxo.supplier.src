package atraxo.acc.business.ext.bpm.delegate.creditMemo.approval;

import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.ext.mailmerge.dto.CreditMemoEmailDto;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.ApprovalDelegate;
import atraxo.fmbas.business.ext.dto.DataTransferObjectManager;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.user.UserSupp;

/**
 * @author vhojda
 */
public class CreditMemoApprovalDelegate extends ApprovalDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditMemoApprovalDelegate.class);

	private static final String DATE_FORMAT = "dd.MM.yyyy";

	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceSrv;

	@Autowired
	private DataTransferObjectManager dataTransferObjectManager;

	@Autowired
	private ISystemParameterService systemParameterService;

	@Override
	public void execute() throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START execute()");
		}

		List<UserSupp> users = this.extractApprovers(WorkflowVariablesConstants.APPROVER_ROLE);
		if (users != null) {
			OutgoingInvoice outInvoice = this.extractOutgoingInvoice();
			if (outInvoice != null) {
				EmailDto emailDTO = this.extractEmailDto(outInvoice, users);
				this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDTO);
			}
		}

		// make sure to reset the mail components
		this.resetMailComponentVariables();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END execute()");
		}
	}

	/**
	 * @param outInvoice
	 * @param users
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private EmailDto extractEmailDto(OutgoingInvoice outInvoice, List<UserSupp> users) throws Exception {

		EmailDto emailDTO = new EmailDto();

		// set the subject
		emailDTO.setSubject(String.format(AccErrorCode.CREDIT_MEMO_APPROVAL_WORKFLOW_SUBJECT.getErrMsg(), outInvoice.getInvoiceNo(),
				new SimpleDateFormat(DATE_FORMAT).format(outInvoice.getInvoiceDate())));

		// set attachments
		Object emailAttachments = this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS);
		if (emailAttachments != null && emailAttachments instanceof List) {
			emailDTO.setAttachments((List<String>) emailAttachments);
		}

		Integer nrDecimals = this.systemParameterService.getDecimalsForAmount();

		// set the email content
		List<EmailAddressesDto> emailAddressesDtoList = new ArrayList<>();
		for (final UserSupp user : users) {
			// extract email dto data from invoice and user
			CreditMemoEmailDto dto = this.dataTransferObjectManager.objectToDto(outInvoice, CreditMemoEmailDto.class);
			dto.setName(user.getFirstName() + " " + user.getLastName());
			dto.setApproveNote(this.getVariableFromWorkflow(WorkflowVariablesConstants.APPROVE_NOTE));
			dto.setFullnameOfRequester(this.getVariableFromWorkflow(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR));

			// make sure to set the number of decimals to amounts
			dto.setInvoiceQuantity(dto.getInvoiceQuantity().setScale(nrDecimals, RoundingMode.HALF_UP));
			dto.setInvoiceNetAmount(dto.getInvoiceNetAmount().setScale(nrDecimals, RoundingMode.HALF_UP));

			EmailAddressesDto emailAddressesDto = new EmailAddressesDto();
			emailAddressesDto.setAddress(user.getEmail());
			emailAddressesDto.setData(dto);
			emailAddressesDtoList.add(emailAddressesDto);
		}
		emailDTO.setTo(emailAddressesDtoList);

		return emailDTO;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	private OutgoingInvoice extractOutgoingInvoice() throws Exception {
		OutgoingInvoice outInvoice = null;
		Object outInvoiceIdObj = this.execution.getVariable(WorkflowVariablesConstants.INVOICE_ID_VAR);
		if (outInvoiceIdObj != null) {
			String outInvoiceIdString = outInvoiceIdObj.toString();
			if (!StringUtils.isEmpty(outInvoiceIdString)) {
				try {
					Integer outInvoiceId = Integer.parseInt(outInvoiceIdString);
					try {
						outInvoice = this.outgoingInvoiceSrv.findByBusiness(outInvoiceId);
					} catch (Exception e) {
						LOGGER.error("ERROR:could not find an outgoing invoice for ID " + outInvoiceId, e);
						this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
								"Could not find an outgoing invoice for ID " + outInvoiceId);
					}
				} catch (NumberFormatException e) {
					LOGGER.error("ERROR:could not parse the received Outgoing Invoice ID ", outInvoiceIdObj);
					this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
							"Could not parse the received invoice ID " + outInvoiceIdString, e);
				}
			} else {
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						"The id for the outgoing invoice is empty !" + outInvoiceIdString);
			}
		} else {
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					"The variable for the id of the outgoing invoice could not be found !");
		}
		return outInvoice;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate#getSessionUserCode()
	 */
	@Override
	public String getSessionUserCode() {
		return null;
	}

}
