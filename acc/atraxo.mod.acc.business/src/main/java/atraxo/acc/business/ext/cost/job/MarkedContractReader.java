package atraxo.acc.business.ext.cost.job;

import java.util.Arrays;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IMarkedContractService;
import atraxo.cmm.domain.impl.contracts.MarkedContract;

public class MarkedContractReader implements ItemReader<MarkedContract> {

	private static final String NR_CONTRACTS = "nrContracts";

	@Autowired
	private IMarkedContractService srv;
	private ExecutionContext executionContext;

	/**
	 * @param stepExecution
	 */
	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.executionContext = stepExecution.getExecutionContext();
	}

	@Override
	public MarkedContract read() throws Exception {
		MarkedContract mc = this.srv.findAnEntity();
		int nrContracts = 0;
		if (this.executionContext.containsKey(NR_CONTRACTS)) {
			nrContracts = (Integer) this.executionContext.get(NR_CONTRACTS);
		}
		if (mc != null) {
			this.srv.delete(Arrays.asList(mc));
			nrContracts++;
		}
		this.executionContext.put(NR_CONTRACTS, nrContracts);
		return mc;
	}
}
