/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.invoices;

import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link Invoice} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class Invoice_Service extends AbstractEntityService<Invoice> {

	/**
	 * Public constructor for Invoice_Service
	 */
	public Invoice_Service() {
		super();
	}

	/**
	 * Public constructor for Invoice_Service
	 * 
	 * @param em
	 */
	public Invoice_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<Invoice> getEntityClass() {
		return Invoice.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return Invoice
	 */
	public Invoice findByKey(Suppliers issuer, Date issueDate, String invoiceNo) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Invoice.NQ_FIND_BY_KEY, Invoice.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("issuer", issuer)
					.setParameter("issueDate", issueDate)
					.setParameter("invoiceNo", invoiceNo).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Invoice", "issuer, issueDate, invoiceNo"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Invoice", "issuer, issueDate, invoiceNo"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Invoice
	 */
	public Invoice findByKey(Long issuerId, Date issueDate, String invoiceNo) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Invoice.NQ_FIND_BY_KEY_PRIMITIVE,
							Invoice.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("issuerId", issuerId)
					.setParameter("issueDate", issueDate)
					.setParameter("invoiceNo", invoiceNo).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Invoice", "issuerId, issueDate, invoiceNo"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Invoice", "issuerId, issueDate, invoiceNo"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return Invoice
	 */
	public Invoice findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(Invoice.NQ_FIND_BY_BUSINESS,
							Invoice.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"Invoice", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"Invoice", "id"), nure);
		}
	}

	/**
	 * Find by reference: receiver
	 *
	 * @param receiver
	 * @return List<Invoice>
	 */
	public List<Invoice> findByReceiver(Customer receiver) {
		return this.findByReceiverId(receiver.getId());
	}
	/**
	 * Find by ID of reference: receiver.id
	 *
	 * @param receiverId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByReceiverId(Integer receiverId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Invoice e where e.clientId = :clientId and e.receiver.id = :receiverId",
						Invoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("receiverId", receiverId).getResultList();
	}
	/**
	 * Find by reference: deliveryLoc
	 *
	 * @param deliveryLoc
	 * @return List<Invoice>
	 */
	public List<Invoice> findByDeliveryLoc(Locations deliveryLoc) {
		return this.findByDeliveryLocId(deliveryLoc.getId());
	}
	/**
	 * Find by ID of reference: deliveryLoc.id
	 *
	 * @param deliveryLocId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByDeliveryLocId(Integer deliveryLocId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Invoice e where e.clientId = :clientId and e.deliveryLoc.id = :deliveryLocId",
						Invoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("deliveryLocId", deliveryLocId).getResultList();
	}
	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Invoice>
	 */
	public List<Invoice> findByCountry(Country country) {
		return this.findByCountryId(country.getId());
	}
	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByCountryId(Integer countryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Invoice e where e.clientId = :clientId and e.country.id = :countryId",
						Invoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("countryId", countryId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<Invoice>
	 */
	public List<Invoice> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Invoice e where e.clientId = :clientId and e.unit.id = :unitId",
						Invoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<Invoice>
	 */
	public List<Invoice> findByContract(Contract contract) {
		return this.findByContractId(contract.getId());
	}
	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByContractId(Integer contractId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Invoice e where e.clientId = :clientId and e.contract.id = :contractId",
						Invoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractId", contractId).getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<Invoice>
	 */
	public List<Invoice> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Invoice e where e.clientId = :clientId and e.currency.id = :currencyId",
						Invoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: issuer
	 *
	 * @param issuer
	 * @return List<Invoice>
	 */
	public List<Invoice> findByIssuer(Suppliers issuer) {
		return this.findByIssuerId(issuer.getId());
	}
	/**
	 * Find by ID of reference: issuer.id
	 *
	 * @param issuerId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByIssuerId(Integer issuerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from Invoice e where e.clientId = :clientId and e.issuer.id = :issuerId",
						Invoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("issuerId", issuerId).getResultList();
	}
	/**
	 * Find by reference: invoiceLines
	 *
	 * @param invoiceLines
	 * @return List<Invoice>
	 */
	public List<Invoice> findByInvoiceLines(InvoiceLine invoiceLines) {
		return this.findByInvoiceLinesId(invoiceLines.getId());
	}
	/**
	 * Find by ID of reference: invoiceLines.id
	 *
	 * @param invoiceLinesId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByInvoiceLinesId(Integer invoiceLinesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from Invoice e, IN (e.invoiceLines) c where e.clientId = :clientId and c.id = :invoiceLinesId",
						Invoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("invoiceLinesId", invoiceLinesId).getResultList();
	}
}
