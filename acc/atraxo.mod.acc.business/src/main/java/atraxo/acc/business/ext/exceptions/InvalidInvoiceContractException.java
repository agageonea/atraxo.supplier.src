package atraxo.acc.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.IErrorCode;

/**
 * Exception used for contrac
 *
 * @author zspeter
 */
public class InvalidInvoiceContractException extends BusinessException {

	/**
	 *
	 */
	private static final long serialVersionUID = 5725041351121257545L;

	public InvalidInvoiceContractException(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}

}
