package atraxo.acc.business.ext.receivableAccruals.delegate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.fuelEvents.service.CostCalculatorService;
import atraxo.acc.domain.ext.deliveryNotes.AccrualDateParameter;
import atraxo.acc.domain.impl.acc_type.InvoiceProcessStatus;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.ext.contracts.CostVat;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelQuoteType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class ReceivableGenerator_Bd extends AbstractBusinessDelegate {

	public void generate(FuelEvent event) throws BusinessException {
		List<ReceivableAccrual> list = new ArrayList<>();
		if (event.getReceivableAccruals() != null) {
			list.addAll(event.getReceivableAccruals());
		}
		ReceivableAccrual accrual = new ReceivableAccrual();
		if (!list.isEmpty()) {
			accrual = list.get(0);
		} else {
			this.initializeInvoicesCost(accrual, event);
		}
		this.generate(accrual, event);
	}

	private void initializeInvoicesCost(ReceivableAccrual accrual, FuelEvent event) {
		accrual.setInvoicedAmountSet(BigDecimal.ZERO);
		accrual.setInvoicedAmountSys(BigDecimal.ZERO);
		accrual.setInvoicedQuantity(BigDecimal.ZERO);
		accrual.setInvoicedVATSet(BigDecimal.ZERO);
		accrual.setInvoicedVATSys(BigDecimal.ZERO);
		accrual.setInvoicedCurrency(event.getBillableCostCurrency());
		accrual.setBusinessType(FuelQuoteType._SCHEDULED_);
	}

	private void generate(ReceivableAccrual accrual, FuelEvent event) throws BusinessException {
		event.addToReceivableAccruals(accrual);
		accrual.setLocation(event.getDeparture());
		accrual.setCustomer(event.getCustomer());
		accrual.setFuelingDate(event.getFuelingDate());
		accrual.setIsOpen(accrual.getIsOpen() != null ? accrual.getIsOpen() : true);
		accrual.setSupplier(event.getFuelSupplier());
		accrual.setAccrualCurrency(event.getBillableCostCurrency());
		accrual.setExpectedCurrency(event.getBillableCostCurrency());
		accrual.setInvoicedCurrency(event.getBillableCostCurrency());
		accrual.setInvProcessStatus(InvoiceProcessStatus._EMPTY_);
		this.calculateExpectedCost(accrual);
		if (accrual.getIsOpen()) {
			this.setInvStatus(accrual);
			this.calculateAccruedCost(accrual);
		}
	}

	private void setInvStatus(ReceivableAccrual accrual) {
		if (accrual.getInvProcessStatus() == null) {
			accrual.setInvProcessStatus(InvoiceProcessStatus._NOT_INVOICED_);
		}
		if (accrual.getInvProcessStatus().equals(InvoiceProcessStatus._COMPLETELY_INVOICED_) && accrual.getAccrualQuantity() != null
				&& !accrual.getAccrualQuantity().equals(BigDecimal.ZERO)) {

			accrual.setInvProcessStatus(InvoiceProcessStatus._PARTIAL_INVOICED_);
		}
	}

	public void calculateAccruedCost(ReceivableAccrual accrual) {
		accrual.setAccrualAmountSet(accrual.getExpectedAmountSet().subtract(accrual.getInvoicedAmountSet()));
		accrual.setAccrualAmountSys(accrual.getExpectedAmountSys().subtract(accrual.getInvoicedAmountSys()));
		accrual.setAccrualQuantity(accrual.getExpectedQuantity().subtract(accrual.getInvoicedQuantity()));
		accrual.setAccrualVATSet(accrual.getExpectedVATSet().subtract(accrual.getInvoicedVATSet()));
		accrual.setAccrualVATSys(accrual.getExpectedVATSys().subtract(accrual.getInvoicedVATSys()));
		accrual.setAccrualCurrency(accrual.getInvoicedCurrency());
	}

	private void calculateExpectedCost(ReceivableAccrual accrual) throws BusinessException {
		FuelEvent event = accrual.getFuelEvent();
		IFinancialSourcesService finSrcSrv = (IFinancialSourcesService) this.findEntityService(FinancialSources.class);
		Map<String, Object> params = new HashMap<>();
		params.put("isStdFlg", true);
		params.put("active", true);
		FinancialSources stdFinSrc = finSrcSrv.findEntityByAttributes(params);
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		String sysCurrencyCode = paramSrv.getSysCurrency();
		ICurrenciesService currencySrv = (ICurrenciesService) this.findEntityService(Currencies.class);
		Currencies sysCurrency = currencySrv.findByCode(sysCurrencyCode);
		String sysAvgMthCode = paramSrv.getSysAverageMethod();
		IAverageMethodService avgMthSrv = (IAverageMethodService) this.findEntityService(AverageMethod.class);
		AverageMethod sysAvgMth = avgMthSrv.findByName(sysAvgMthCode);
		String sysAccrualEvalDate = paramSrv.getAccrualEvaluationDate();
		Date eventDate = GregorianCalendar.getInstance().getTime();
		if (sysAccrualEvalDate.equalsIgnoreCase(AccrualDateParameter.EVENT_DATE.getValue())) {
			eventDate = event.getFuelingDate();
		}
		BigDecimal density = this.getAccrualDensity(event, paramSrv);

		this.setExpectedValues(event, accrual, sysCurrency, sysAvgMth, eventDate, stdFinSrc, density);
	}

	private void setExpectedValues(FuelEvent event, ReceivableAccrual accrual, Currencies sysCurrency, AverageMethod sysAvgMth, Date eventDate,
			FinancialSources stdFinSrc, BigDecimal density) throws BusinessException {
		IContractService contractSrv = (IContractService) this.findEntityService(Contract.class);
		CostCalculatorService calcSrv = (CostCalculatorService) this.getApplicationContext().getBean("costCalculator");
		BigDecimal sysQuantity = event.getSysQuantity();
		ExchangeRate_Bd exchangeRateBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		switch (event.getBillPriceSourceType()) {
		case "Contract":
			Contract contract = contractSrv.findById(event.getBillPriceSourceId());
			CostVat setCurSetUnitCostVat = calcSrv.calculate(contract, event);

			ConversionResult sysCurSysUnitPrice = exchangeRateBd.convert(contract.getSettlementCurr(), sysCurrency, eventDate,
					setCurSetUnitCostVat.getCost(), false, sysAvgMth, stdFinSrc);

			ConversionResult sysCurSysUnitVat = exchangeRateBd.convert(contract.getSettlementCurr(), sysCurrency, eventDate,
					setCurSetUnitCostVat.getVat(), false, sysAvgMth, stdFinSrc);
			accrual.setExpectedAmountSet(setCurSetUnitCostVat.getCost());
			accrual.setExpectedAmountSys(sysCurSysUnitPrice.getValue());
			accrual.setExchangeRate(sysCurSysUnitPrice.getFactor());
			accrual.setExpectedQuantity(sysQuantity);
			accrual.setExpectedVATSet(setCurSetUnitCostVat.getVat());
			accrual.setExpectedVATSys(sysCurSysUnitVat.getValue());
			accrual.setDensity(density);
			accrual.setBusinessType(FuelQuoteType._SCHEDULED_);
			break;
		case "FuelOrderLocation":
			IFuelOrderLocationService locSrv = (IFuelOrderLocationService) this.findEntityService(FuelOrderLocation.class);
			FuelOrderLocation locOrder = locSrv.findById(event.getBillPriceSourceId());
			CostVat locOrderCostVat = calcSrv.calculate(locOrder, event);

			ConversionResult sysCurSysUnitLocOrderCost = exchangeRateBd.convert(locOrder.getPaymentCurrency(), sysCurrency, eventDate,
					locOrderCostVat.getCost(), false, sysAvgMth, stdFinSrc);

			ConversionResult sysCurSysUnitLocOrderVat = exchangeRateBd.convert(locOrder.getPaymentCurrency(), sysCurrency, eventDate,
					locOrderCostVat.getVat(), false, sysAvgMth, stdFinSrc);

			accrual.setExpectedAmountSet(locOrderCostVat.getCost());
			accrual.setExpectedAmountSys(sysCurSysUnitLocOrderCost.getValue());
			accrual.setExchangeRate(sysCurSysUnitLocOrderCost.getFactor());
			accrual.setExpectedQuantity(sysQuantity);
			accrual.setExpectedVATSet(locOrderCostVat.getVat());
			accrual.setExpectedVATSys(sysCurSysUnitLocOrderVat.getValue());
			accrual.setDensity(density);
			accrual.setBusinessType(locOrder.getFuelOrder().getContractType());
			break;
		default:
			throw new BusinessException(AccErrorCode.INVALID_FUEL_EVENT, AccErrorCode.INVALID_FUEL_EVENT.getErrMsg());
		}
	}

	private BigDecimal getAccrualDensity(FuelEvent event, ISystemParameterService paramSrv) throws BusinessException {
		BigDecimal density = new BigDecimal(paramSrv.getDensity());
		IDeliveryNoteService dnService = (IDeliveryNoteService) this.findEntityService(DeliveryNote.class);
		List<DeliveryNote> notes = dnService.findByFuelEventId(event.getId());
		for (DeliveryNote dn : notes) {
			if (dn.getSource().equals(QuantitySource._FUEL_TICKET_)) {
				IFuelTicketService ticketSrv = (IFuelTicketService) this.findEntityService(FuelTicket.class);
				FuelTicket ticket = ticketSrv.findById(dn.getObjectId());
				if (ticket.getDensity() != null && !ticket.getDensity().equals(BigDecimal.ZERO)) {
					density = ticket.getDensity();
				}
			}
		}
		return density;
	}

}
