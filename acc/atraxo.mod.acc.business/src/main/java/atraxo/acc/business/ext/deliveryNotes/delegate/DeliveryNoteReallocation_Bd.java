package atraxo.acc.business.ext.deliveryNotes.delegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class DeliveryNoteReallocation_Bd extends AbstractBusinessDelegate {

	class LocSupplier {
		private Locations location;
		private Suppliers supplier;

		public LocSupplier(Contract contract) {
			this.location = contract.getLocation();
			this.supplier = contract.getSupplier();
		}

		public Locations getLocation() {
			return this.location;
		}

		public Suppliers getSupplier() {
			return this.supplier;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + ((this.location == null) ? 0 : this.location.hashCode());
			result = prime * result + ((this.supplier == null) ? 0 : this.supplier.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (this.getClass() != obj.getClass()) {
				return false;
			}
			LocSupplier other = (LocSupplier) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (this.location == null) {
				if (other.location != null) {
					return false;
				}
			} else if (!this.location.getId().equals(other.location.getId())) {
				return false;
			}
			if (this.supplier == null) {
				if (other.supplier != null) {
					return false;
				}
			} else if (!this.supplier.getId().equals(other.supplier.getId())) {
				return false;
			}
			return true;
		}

		private DeliveryNoteReallocation_Bd getOuterType() {
			return DeliveryNoteReallocation_Bd.this;
		}
	}

	public void reallocate(List<Contract> contracts) throws BusinessException {
		IDeliveryNoteService srv = (IDeliveryNoteService) this.findEntityService(DeliveryNote.class);
		List<DeliveryNote> deliveryNotes = new ArrayList<>();
		for (Contract contract : contracts) {

			Map<String, Object> params = new HashMap<>();
			params.put("fuelSupplier", contract.getSupplier());
			params.put("departure", contract.getLocation());
			List<DeliveryNote> dnList = srv.findEntitiesByAttributes(params);
			this.filterDeliveryNotes(contract, dnList);
			for (DeliveryNote deliveryNote : dnList) {
				if (deliveryNote.getDeliveryNoteContracts().isEmpty()) {
					deliveryNote.setReallocate(true);
					deliveryNotes.add(deliveryNote);
				}
			}
		}
		srv.update(deliveryNotes);
	}

	private void filterDeliveryNotes(Contract contract, List<DeliveryNote> deliveryNotes) throws BusinessException {
		Iterator<DeliveryNote> iter = deliveryNotes.iterator();
		IContractService srv = (IContractService) this.findEntityService(Contract.class);
		Map<String, Object> params = new HashMap<>();
		params.put("dealType", DealType._SELL_);
		params.put("resaleRef", contract);
		List<Contract> scList = srv.findEntitiesByAttributes(params);
		while (iter.hasNext()) {
			DeliveryNote dn = iter.next();
			if (dn.getFuelingDate().compareTo(contract.getValidFrom()) < 0 || dn.getFuelingDate().compareTo(contract.getValidTo()) > 0) {
				iter.remove();
			} else if (!"Contract".equals(dn.getFuelEvent().getBillPriceSourceType()) || !this.hasResaleReference(dn.getFuelEvent(), scList)) {
				iter.remove();
			}
		}
	}

	private boolean hasResaleReference(FuelEvent fuelEvent, List<Contract> scList) {
		for (Contract c : scList) {
			if (c.getId().equals(fuelEvent.getBillPriceSourceId())) {
				return true;
			}
		}

		return false;
	}

}
