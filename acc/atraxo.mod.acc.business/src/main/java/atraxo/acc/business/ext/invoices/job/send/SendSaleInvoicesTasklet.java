package atraxo.acc.business.ext.invoices.job.send;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.fuelplus.mailmerge.ws.FileAttachment;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.invoices.service.OutgoingInvoiceDtoService;
import atraxo.acc.domain.ext.invoices.SourceForTheTemplate;
import atraxo.acc.domain.ext.mailmerge.dto.OutgoingInvoiceDto;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.business.api.system.IDateFormatMaskService;
import atraxo.ad.business.api.system.IDateFormatService;
import atraxo.ad.domain.impl.ad.TAttachmentType;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ad.domain.impl.system.DateFormat;
import atraxo.ad.domain.impl.system.DateFormatMask;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.business.api.customer.ICustomerNotificationService;
import atraxo.fmbas.business.api.geo.IAreaService;
import atraxo.fmbas.business.api.notificationEvent.INotificationEventService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.dto.DataTransferObjectManager;
import atraxo.fmbas.business.ext.exceptions.mailMerge.MailMergeApplicationFaulExeption;
import atraxo.fmbas.business.ext.mailMerge.MailMergeManager;
import atraxo.fmbas.business.ext.mailMerge.service.MailMerge_Service;
import atraxo.fmbas.business.ext.util.StreamUtil;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.notificationEvent.NotificationEvent;
import atraxo.fmbas.domain.impl.templates.Template;
import seava.j4e.api.exceptions.BusinessException;

public class SendSaleInvoicesTasklet implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendSaleInvoicesTasklet.class);

	private static final String ATTACHMENT_ZIP_NAME = "Invoices.zip";

	private static final String AREA = "AREA";
	private static final String OFFSET = "OFFSET";
	private static final String RESEND = "RESEND";
	private static final String CUSTOMERS = "CUSTOMER(S)";
	private static final String SUBSIDIARIES = "SUBSIDIARY(S)";
	private static final String INVOICE_FORMAT = "INVOICE FORMAT";
	private static final String BCC = "BLIND CARBON COPY (BCC)";

	private static final String NOTIFICATION_NAME = "Sales invoice";

	protected static final String EXECUTION_LOG = "executionLog";
	protected static final String TOTALS_LOG = "Totals";
	protected static final String CUSTOMER_LOG = "Customer ";

	private static final String INVOICES_SENT = "Number of invoices sent: %s ";
	private static final String INVOICES_TO_BE_SENT = "Number of invoices to be sent: %s ";

	private static final String NOTIFIED_CUSTOMER_CONTACTS = "Notified contacts: %s";
	private static final String NOTIFICATIONS_SENT = "Number of generated notifications: %s ";

	private static final String MISSING_TEMPLATE = "Missing template exception";
	private static final String MISSING_EMAIL_RECEPIENTS_TEMPLATE = "Missing email recepients";

	private static final String CUSTOMER_WARNING = "Warning: %s";
	private static final String CUSTOMER_EXCEPTION = "Exception occured: %s";

	private static final String WARNING_ACCOUNT_MANAGER_NOT_DEFINED = "Account Manager not defined!";

	private static final String SOURCE_FOR_THE_TEMPLATE = "SOURCE FOR THE TEMPLATE";

	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;
	@Autowired
	private OutgoingInvoiceDtoService outgoingInvoiceDtoService;
	@Autowired
	private ICustomerNotificationService customerNotificationService;
	@Autowired
	private INotificationEventService notificationEventService;
	@Autowired
	private IContactsService contactsService;
	@Autowired
	private DataTransferObjectManager dtoManager;
	@Autowired
	private IAreaService areaService;
	@Autowired
	private MailMerge_Service mailService;
	@Autowired
	private ISystemParameterService sysParamSrv;
	@Autowired
	private IDateFormatService dateFormatService;
	@Autowired
	private IDateFormatMaskService dateFormatMaskService;
	@Autowired
	private IContractService contractService;
	@Autowired
	private IExternalReportService externalReportService;
	@Autowired
	private IAttachmentService attachmentService;

	private String areaParam;
	private String offsetParam;
	private String customersParam;
	private String subsidiariesParam;

	private String bccParam;
	private String resendParam;
	private String invoiceFormatParam;

	private DateFormat df;
	private DateFormatMask dateFormatMask;

	private String sourceForTheTemplate;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		ExecutionContext context = chunkContext.getStepContext().getStepExecution().getExecutionContext();

		// Set initial parameters
		this.setParams(chunkContext);

		// Log map results
		Map<String, List<String>> logsMap = new HashMap<>();

		// Find the filtered invoices
		List<OutgoingInvoice> outgoingInvoiceList = this.getOutgoingInvoices();
		Map<Customer, List<OutgoingInvoice>> customerInvoicesMap = SendInvoicesUtil.groupInvoicesByReceiver(outgoingInvoiceList);

		// Totals results log
		List<String> totalsLogs = new ArrayList<>();
		Integer totalNotificationsSent = 0;
		Integer totalInvoicesSent = 0;

		for (Map.Entry<Customer, List<OutgoingInvoice>> receiverEntry : customerInvoicesMap.entrySet()) {
			// Customer results log
			List<String> customerLogs = new ArrayList<>();
			List<String> notifiedCustomerContacts = new ArrayList<>();
			Integer customerNotificationsSent = 0;
			Integer customerInvoicesSent = 0;
			String customerWarning = "";
			String customerException = "";

			Customer receiver = receiverEntry.getKey();
			List<OutgoingInvoice> receiverInvoices = receiverEntry.getValue();

			customerLogs.add(String.format(INVOICES_TO_BE_SENT, receiverInvoices.size()));

			// Find customer contacts with sales invoice notification enabled
			List<CustomerNotification> notifications = this.getNotifications(receiver);
			List<Contacts> contacts = this.getNotificationContacts(receiver);

			// Check conditions
			if (notifications.isEmpty() || contacts.isEmpty()) {
				customerLogs.add(String.format(INVOICES_SENT, 0));
				if (notifications.isEmpty()) {
					customerLogs.add(MISSING_TEMPLATE);
				} else {
					customerLogs.add(MISSING_EMAIL_RECEPIENTS_TEMPLATE);
				}
				logsMap.put(CUSTOMER_LOG + receiver.getCode() + " - " + receiver.getName(), customerLogs);
				continue;
			}

			Template emailBody = notifications.get(0).getEmailBody();

			// Send emails (on from each issuer to all customer contacts with all the invoices)
			Map<Customer, List<OutgoingInvoice>> issuerInvoicesMap = SendInvoicesUtil.groupInvoicesByIssuer(receiverInvoices);
			for (Map.Entry<Customer, List<OutgoingInvoice>> issuerEntry : issuerInvoicesMap.entrySet()) {
				Customer issuer = issuerEntry.getKey();
				List<OutgoingInvoice> issuerInvoices = issuerEntry.getValue();

				try {
					List<FileAttachment> attachments = this.getAttachmentFile(issuer, receiver, notifications, issuerInvoices);

					if (!CollectionUtils.isEmpty(attachments)) {
						EmailDto emailDto = EmailDtoFactory.getEmailDto(issuer, contacts, this.bccParam, this.sysParamSrv.getNoReplyAddress());
						String emailJson = MailMergeManager.getDataJson(null, "", emailDto, this.dateFormatMask.getValue());
						this.mailService.sendMail(emailBody.getFileReference(), emailJson, attachments);

						customerInvoicesSent = customerInvoicesSent + issuerInvoices.size();
						customerNotificationsSent = customerNotificationsSent + contacts.size();

						contacts.stream().forEach(c -> notifiedCustomerContacts.add(c.getFullNameField()));

						if (issuer != null && issuer.getResponsibleBuyer() == null) {
							customerWarning = WARNING_ACCOUNT_MANAGER_NOT_DEFINED;
						}
					}
				} catch (MailMergeApplicationFaulExeption mmafe) {
					customerException = "MailMerge " + mmafe.getMessage();
					LOGGER.error("MailMerge Application Fault Exception", mmafe);
				} catch (Exception e) {
					customerException = e.getMessage();
					LOGGER.error(e.getMessage(), e);
				}
			}

			totalInvoicesSent = totalInvoicesSent + customerInvoicesSent;
			totalNotificationsSent = totalNotificationsSent + customerNotificationsSent;

			// Add customer log details
			customerLogs.add(String.format(INVOICES_SENT, customerInvoicesSent));

			if (customerNotificationsSent > 0) {
				customerLogs.add(String.format(NOTIFICATIONS_SENT, customerNotificationsSent));
			}

			if (!notifiedCustomerContacts.isEmpty()) {
				customerLogs.add(String.format(NOTIFIED_CUSTOMER_CONTACTS, StringUtils.join(notifiedCustomerContacts, ", ")));
			}

			if (!customerWarning.isEmpty()) {
				customerLogs.add(String.format(CUSTOMER_WARNING, customerWarning));
			}

			if (!customerException.isEmpty()) {
				customerLogs.add(String.format(CUSTOMER_EXCEPTION, customerException));
			}

			// Add customer log to logs map
			logsMap.put(CUSTOMER_LOG + receiver.getCode() + " - " + receiver.getName(), customerLogs);
		}

		// Add total log details
		totalsLogs.add(String.format(INVOICES_TO_BE_SENT, outgoingInvoiceList.size()));
		totalsLogs.add(String.format(INVOICES_SENT, totalInvoicesSent));
		totalsLogs.add(String.format(NOTIFICATIONS_SENT, totalNotificationsSent));

		// Add totals log to logs map
		logsMap.put(TOTALS_LOG, totalsLogs);

		// Add execution log to context
		context.put(EXECUTION_LOG, logsMap);

		this.outgoingInvoiceService.update(outgoingInvoiceList);

		return null;
	}

	private void setParams(ChunkContext chunkContext) {
		LOGGER.info("Collecting paramteres.");
		// Filter invoice params
		this.areaParam = (String) chunkContext.getStepContext().getJobParameters().get(AREA);
		this.offsetParam = chunkContext.getStepContext().getJobParameters().get(OFFSET).toString();
		this.customersParam = chunkContext.getStepContext().getJobParameters().get(CUSTOMERS).toString();
		this.subsidiariesParam = (String) chunkContext.getStepContext().getJobParameters().get(SUBSIDIARIES);

		// Send invoice params
		this.bccParam = (String) chunkContext.getStepContext().getJobParameters().get(BCC);
		this.resendParam = (String) chunkContext.getStepContext().getJobParameters().get(RESEND);
		this.invoiceFormatParam = (String) chunkContext.getStepContext().getJobParameters().get(INVOICE_FORMAT);

		// Set formats
		this.df = this.dateFormatService.findByName("dd-mm-yyyy");
		this.dateFormatMask = this.dateFormatMaskService.findByName(this.df, "JAVA_DATE_FORMAT");
		this.sourceForTheTemplate = (String) chunkContext.getStepContext().getJobParameters().get(SOURCE_FOR_THE_TEMPLATE);
	}

	/**
	 * @param resendParam
	 * @param dateFormatMask
	 * @param invoiceFormat
	 * @param notification
	 * @param issuerInvoices
	 * @return
	 * @throws IOException
	 */
	private List<FileAttachment> getAttachmentFile(Customer issuer, Customer receiver, List<CustomerNotification> notifications,
			List<OutgoingInvoice> issuerInvoices) throws BusinessException, IOException {
		LOGGER.info("Get attachement file, for customer:" + issuer.getAccountNumber());
		List<FileAttachment> attachments = new ArrayList<>();

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(out);

		byte[] fileContent = null;
		boolean hasAttachment = false;
		String generatedFileName = null;
		boolean createArchive = issuerInvoices.size() > 1;

		CustomerNotification customerNotification;

		try {
			for (OutgoingInvoice invoice : issuerInvoices) {
				customerNotification = SendInvoicesUtil.getCustomerNotification(notifications, invoice);

				LOGGER.debug("Processing invoice:" + invoice.getInvoiceNo());
				if (customerNotification != null) {
					String invoiceFormat = SendInvoicesUtil.getInvoiceFormat(customerNotification.getTemplate().getFileName(),
							this.invoiceFormatParam);

					generatedFileName = invoice.getReceiver().getCode() + "_" + invoice.getDeliveryLoc().getCode() + "_" + invoice.getInvoiceNo();

					InputStream in = null;
					if (SourceForTheTemplate.REPORT_CENTER.getName().equalsIgnoreCase(this.sourceForTheTemplate)) {
						generatedFileName += ".pdf";
						InputStream is = this.generateFileWithReportCenter(invoice, generatedFileName, "pdf");
						if (is != null) {
							fileContent = IOUtils.toByteArray(is);
							is.close();
							in = new ByteArrayInputStream(fileContent);
						}
					} else {
						generatedFileName += "." + invoiceFormat.toLowerCase();
						fileContent = this.generateFileWithMailMerge(customerNotification, invoice, invoiceFormat);
						in = new ByteArrayInputStream(fileContent);
					}
					if (in != null) {
						StreamUtil.addToZip(in, zos, generatedFileName);
						in.close();
					}

					hasAttachment = true;
					invoice.setInvoiceSent(true);
				}
			}
			if (zos != null) {
				zos.close();
			}
			if (out != null) {
				out.close();
			}
			if (hasAttachment) {
				LOGGER.debug("Build attachement.");
				FileAttachment attachment = new FileAttachment();
				attachment.setFilecontent(createArchive ? out.toByteArray() : fileContent);
				attachment.setFilename(createArchive ? ATTACHMENT_ZIP_NAME : generatedFileName);
				attachments.add(attachment);
			}

		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			if (zos != null) {
				zos.close();
			}
			if (out != null) {
				out.close();
			}
			throw new BusinessException(AccErrorCode.INVOICE_JOB_MAIL_DELIVERY_ERROR,
					String.format(AccErrorCode.INVOICE_JOB_MAIL_DELIVERY_ERROR.getErrMsg(), issuer.getCode(), receiver.getCode()), e);
		}
		return attachments;
	}

	private byte[] generateFileWithMailMerge(CustomerNotification customerNotification, OutgoingInvoice invoice, String invoiceFormat)
			throws Exception {
		OutgoingInvoiceDto invoiceDto = this.dtoManager.objectToCustomDto(invoice, OutgoingInvoiceDto.class, this.outgoingInvoiceDtoService);
		String json = MailMergeManager.getDataJson(invoiceDto, invoiceFormat, null, this.dateFormatMask.getValue());
		return this.mailService.generateDocument(customerNotification.getTemplate().getFileReference(), json);
	}

	private InputStream generateFileWithReportCenter(OutgoingInvoice invoice, String fileName, String invoiceFormat) throws BusinessException {
		if (InvoiceReferenceDocType._CONTRACT_.equals(invoice.getReferenceDocType())) {
			Contract contract = this.contractService.findById(invoice.getReferenceDocId());
			if (InvoiceType._PDF_FORMAT_.equals(contract.getInvoiceType()) && contract.getInvoiceTemplate() != null) {
				ExternalReport externalReport = contract.getInvoiceTemplate();
				List<Attachment> attach = this.attachmentService.getDocumentsByType(invoice.getId(), OutgoingInvoice.class.getSimpleName(),
						TAttachmentType._UPLOAD_);
				if (!CollectionUtils.isEmpty(attach)) {
					for (Attachment a : attach) {
						if (a.getFileName().equalsIgnoreCase(fileName)) {
							return this.attachmentService.download(a.getRefid());
						}
					}
				}
				Attachment attachement = this.externalReportService.generateSaleInvoiceReportSync(invoice.getId().toString(), externalReport.getId(),
						invoiceFormat, OutgoingInvoice.class.getSimpleName(), fileName);
				return this.attachmentService.download(attachement.getRefid());
			}
		}
		return null;
	}

	/**
	 * @param notification
	 * @return
	 */
	private List<Contacts> getNotificationContacts(Customer customer) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("objectId", customer.getId());
		params.put("objectType", Customer.class.getSimpleName());
		List<Contacts> customerContacts = this.contactsService.findEntitiesByAttributes(params);

		List<Contacts> notificationContacts = new ArrayList<>();
		for (Contacts c : customerContacts) {
			for (NotificationEvent event : c.getNotification()) {
				if (event.getName().equals(NOTIFICATION_NAME)) {
					notificationContacts.add(c);
				}
			}
		}
		return notificationContacts;
	}

	/**
	 * @param customer
	 * @param areaParam
	 * @return
	 */
	private List<CustomerNotification> getNotifications(Customer customer) {
		List<CustomerNotification> customerNotifications = new ArrayList<>();

		try {
			NotificationEvent notification = this.notificationEventService.findByName(NOTIFICATION_NAME);

			Map<String, Object> params = new HashMap<>();
			params.put("customer", customer);
			params.put("notificationEvent", notification);
			customerNotifications = this.customerNotificationService.findEntitiesByAttributes(params);
		} catch (Exception e) {
			LOGGER.info("ERROR:could not find '" + NOTIFICATION_NAME + "' notification for customer '" + customer.getCode() + "' and area '"
					+ this.areaParam + "'", e);
		}

		return customerNotifications;
	}

	/**
	 * @param areaParam
	 * @param offsetParam
	 * @param customersParam
	 * @param subsidiariesParam
	 * @return
	 * @throws BusinessException
	 * @throws NumberFormatException
	 */
	private List<OutgoingInvoice> getOutgoingInvoices() throws BusinessException {
		List<String> issuers = new ArrayList<>();
		List<String> receivers = new ArrayList<>();
		List<Locations> locations = new ArrayList<>();

		if (!this.subsidiariesParam.isEmpty()) {
			issuers = Arrays.asList(this.subsidiariesParam.split(","));
		}
		if (!this.customersParam.isEmpty()) {
			receivers = Arrays.asList(this.customersParam.split(","));
		}
		if (!this.areaParam.isEmpty()) {
			Area area = this.areaService.findByCode(this.areaParam);
			locations = (List<Locations>) area.getLocations();
		}

		Date offsetDate = SendInvoicesUtil.getDate(new Integer(this.offsetParam));
		return this.outgoingInvoiceService.exportToMail(issuers, receivers, locations, offsetDate, this.resendParam);
	}

}
