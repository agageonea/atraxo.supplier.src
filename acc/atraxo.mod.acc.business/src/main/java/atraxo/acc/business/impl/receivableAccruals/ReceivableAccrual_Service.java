/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.receivableAccruals;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ReceivableAccrual} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ReceivableAccrual_Service
		extends
			AbstractEntityService<ReceivableAccrual> {

	/**
	 * Public constructor for ReceivableAccrual_Service
	 */
	public ReceivableAccrual_Service() {
		super();
	}

	/**
	 * Public constructor for ReceivableAccrual_Service
	 * 
	 * @param em
	 */
	public ReceivableAccrual_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ReceivableAccrual> getEntityClass() {
		return ReceivableAccrual.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ReceivableAccrual
	 */
	public ReceivableAccrual findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ReceivableAccrual.NQ_FIND_BY_BUSINESS,
							ReceivableAccrual.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ReceivableAccrual", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ReceivableAccrual", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByFuelEvent(FuelEvent fuelEvent) {
		return this.findByFuelEventId(fuelEvent.getId());
	}
	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByFuelEventId(Integer fuelEventId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ReceivableAccrual e where e.clientId = :clientId and e.fuelEvent.id = :fuelEventId",
						ReceivableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelEventId", fuelEventId).getResultList();
	}
	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ReceivableAccrual e where e.clientId = :clientId and e.location.id = :locationId",
						ReceivableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ReceivableAccrual e where e.clientId = :clientId and e.customer.id = :customerId",
						ReceivableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findBySupplier(Suppliers supplier) {
		return this.findBySupplierId(supplier.getId());
	}
	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findBySupplierId(Integer supplierId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ReceivableAccrual e where e.clientId = :clientId and e.supplier.id = :supplierId",
						ReceivableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("supplierId", supplierId).getResultList();
	}
	/**
	 * Find by reference: expectedCurrency
	 *
	 * @param expectedCurrency
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByExpectedCurrency(
			Currencies expectedCurrency) {
		return this.findByExpectedCurrencyId(expectedCurrency.getId());
	}
	/**
	 * Find by ID of reference: expectedCurrency.id
	 *
	 * @param expectedCurrencyId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByExpectedCurrencyId(
			Integer expectedCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ReceivableAccrual e where e.clientId = :clientId and e.expectedCurrency.id = :expectedCurrencyId",
						ReceivableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("expectedCurrencyId", expectedCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: invoicedCurrency
	 *
	 * @param invoicedCurrency
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByInvoicedCurrency(
			Currencies invoicedCurrency) {
		return this.findByInvoicedCurrencyId(invoicedCurrency.getId());
	}
	/**
	 * Find by ID of reference: invoicedCurrency.id
	 *
	 * @param invoicedCurrencyId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByInvoicedCurrencyId(
			Integer invoicedCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ReceivableAccrual e where e.clientId = :clientId and e.invoicedCurrency.id = :invoicedCurrencyId",
						ReceivableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("invoicedCurrencyId", invoicedCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: accrualCurrency
	 *
	 * @param accrualCurrency
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByAccrualCurrency(
			Currencies accrualCurrency) {
		return this.findByAccrualCurrencyId(accrualCurrency.getId());
	}
	/**
	 * Find by ID of reference: accrualCurrency.id
	 *
	 * @param accrualCurrencyId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByAccrualCurrencyId(
			Integer accrualCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from ReceivableAccrual e where e.clientId = :clientId and e.accrualCurrency.id = :accrualCurrencyId",
						ReceivableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("accrualCurrencyId", accrualCurrencyId)
				.getResultList();
	}
}
