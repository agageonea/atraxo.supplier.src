package atraxo.acc.business.ext.invoices.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.invoiceLines.IInvoiceLineService;
import atraxo.acc.business.api.invoices.IInvoiceService;
import atraxo.acc.domain.impl.acc_type.CheckResult;
import atraxo.acc.domain.impl.acc_type.CheckStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceLinesTaxType;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceType;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.suppliers.ISuppliersService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class IncomingInvoiceGenerator_Bd extends AbstractBusinessDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(IncomingInvoiceGenerator_Bd.class);
	private static final String FINISH_INPUT = "Finish input";

	public List<String> generate(List<OutgoingInvoice> outgoingList) throws BusinessException {
		IInvoiceService invoiceService = (IInvoiceService) this.findEntityService(Invoice.class);
		IInvoiceLineService invoiceLineService = (IInvoiceLineService) this.findEntityService(InvoiceLine.class);
		List<String> invoiceNumbers = new ArrayList<>();
		for (OutgoingInvoice oi : outgoingList) {
			if (this.canGenerateIncomingInvoice(oi)) {
				Invoice invoice = this.generateIncomingInvoice(oi, invoiceService, invoiceLineService);
				invoiceNumbers.add(invoice.getInvoiceNo());
			} else {
				continue;
			}
		}
		return invoiceNumbers;
	}

	private Invoice generateIncomingInvoice(OutgoingInvoice outgoingInvoice, IInvoiceService invoiceService, IInvoiceLineService invoiceLineService)
			throws BusinessException {
		IContractService service = (IContractService) this.findEntityService(Contract.class);
		ISuppliersService supplierService = (ISuppliersService) this.findEntityService(Suppliers.class);
		Invoice invoice = new Invoice();
		invoice.setStatus(BillStatus._DRAFT_);
		invoice.setTransactionType(TransactionType._ORIGINAL_);
		invoice.setReceivingDate(GregorianCalendar.getInstance().getTime());
		invoice.setCountry(outgoingInvoice.getCountry());
		invoice.setDeliveryLoc(outgoingInvoice.getDeliveryLoc());
		invoice.setUnit(outgoingInvoice.getUnit());
		invoice.setCurrency(outgoingInvoice.getCurrency());
		invoice.setDeliveryDateFrom(outgoingInvoice.getDeliveryDateFrom());
		invoice.setDeliverydateTo(outgoingInvoice.getDeliverydateTo());
		invoice.setInvoiceForm(outgoingInvoice.getInvoiceForm());
		invoice.setCategory(outgoingInvoice.getCategory());
		invoice.setTotalAmount(outgoingInvoice.getTotalAmount());
		invoice.setVatAmount(outgoingInvoice.getVatAmount());
		invoice.setNetAmount(outgoingInvoice.getNetAmount());
		invoice.setQuantity(outgoingInvoice.getQuantity());
		invoice.setInvoiceType(InvoiceTypeAcc._INV_);
		invoice.setIssueDate(outgoingInvoice.getInvoiceDate());
		Contract contract = service.findById(outgoingInvoice.getReferenceDocId());
		invoice.setContract(contract.getResaleRef());
		invoice.setReceiver(outgoingInvoice.getReceiver());
		Suppliers issuer = supplierService.findById(outgoingInvoice.getIssuer().getId());
		invoice.setIssuer(issuer);
		invoice.setDealType(DealType._BUY_);
		invoice.setReferenceType(InvoiceReferenceType._EMPTY_);
		invoice.setTaxType(outgoingInvoice.getTaxType());

		invoice.setInvoiceNo(outgoingInvoice.getInvoiceNo());
		invoice.setSubsidiaryId(invoice.getContract().getSubsidiaryId());
		this.generateInvoiceLine(outgoingInvoice, invoice);
		invoiceService.insert(invoice);
		try {
			if (!CollectionUtils.isEmpty(invoice.getInvoiceLines())) {
				InvoiceLine il = invoice.getInvoiceLines().iterator().next();
				invoiceLineService.balanceInvoice(il);
			}
			invoiceService.finishInput(invoice, FINISH_INPUT);
			if (outgoingInvoice.getReceiver().getIsSubsidiary() && outgoingInvoice.getIssuer().getIsSubsidiary()) {
				invoiceService.modifyStatus(Arrays.asList(invoice), outgoingInvoice.getStatus().getName(), outgoingInvoice.getStatus());
			} else {
				invoiceService.modifyStatus(Arrays.asList(invoice), BillStatus._AWAITING_APPROVAL_.getName(), BillStatus._AWAITING_APPROVAL_);
			}
		} catch (BusinessException e) {
			LOG.warn("Generater incoming invoice cannot be finished", e);
		}
		return invoice;
	}

	private void generateInvoiceLine(OutgoingInvoice outgoingInvoice, Invoice invoice) {
		for (OutgoingInvoiceLine oil : outgoingInvoice.getInvoiceLines()) {
			InvoiceLine il = new InvoiceLine();
			il.setInvoice(invoice);
			il.setContract(invoice.getContract());
			il.setAirlineDesignator(oil.getCustomer());
			il.setDeliveryDate(oil.getDeliveryDate());
			il.setEventDate(oil.getDeliveryDate());
			il.setUnit(invoice.getUnit());
			il.setNetQuantityUnit(invoice.getUnit());
			il.setGrossQuantityUnit(invoice.getUnit());
			il.setDestination(oil.getDestination());
			il.setTicketNo(oil.getTicketNo());
			il.setQuantity(oil.getQuantity());
			il.setAmount(oil.getAmount());
			il.setVat(oil.getVat());
			il.setCalculatedVat(oil.getVat());
			il.setNetQuantity(oil.getQuantity());
			il.setGrossQuantity(oil.getQuantity());
			il.setCalculatedVat(oil.getVat());
			il.setInvoicedVat(oil.getVat());
			il.setTaxType(InvoiceLinesTaxType._EMPTY_);
			il.setFlightNo(oil.getFlightNo());
			il.setSuffix(oil.getFlightSuffix());
			il.setEventCheckStatus(CheckStatus._NOT_CHECKED_);
			il.setOriginalQuantityCheckStatus(CheckStatus._NOT_CHECKED_);
			il.setOriginalVatCheckStatus(CheckStatus._NOT_CHECKED_);
			il.setOverAllCheckStatus(CheckStatus._NOT_CHECKED_);
			il.setQuantityCheckResult(CheckResult._EMPTY_);
			il.setQuantityCheckStatus(CheckStatus._NOT_CHECKED_);
			il.setVatCheckResult(CheckResult._EMPTY_);
			il.setVatCheckStatus(CheckStatus._NOT_CHECKED_);
			il.setAircraftRegistrationNumber(oil.getAircraftRegistrationNumber());
			il.setFlightType(oil.getFlightType());
			invoice.addToInvoiceLines(il);
		}
	}

	private boolean canGenerateIncomingInvoice(OutgoingInvoice oi) throws BusinessException {
		if (InvoiceReferenceDocType._PURCHASE_ORDER_.equals(oi.getReferenceDocType()) || InvoiceTypeAcc._CRN_.equals(oi.getInvoiceType())) {
			return false;
		}
		IContractService service = (IContractService) this.findEntityService(Contract.class);
		Contract contract = service.findById(oi.getReferenceDocId());
		Customer customer = contract.getCustomer();
		return customer != null && customer.getIsSubsidiary() && contract.getResaleRef() != null;
	}

}
