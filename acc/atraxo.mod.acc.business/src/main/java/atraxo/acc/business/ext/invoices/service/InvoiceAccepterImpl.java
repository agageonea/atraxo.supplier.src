package atraxo.acc.business.ext.invoices.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import atraxo.acc.business.api.ext.invoices.IAccountInfoAcknoledgmentService;
import atraxo.acc.business.api.ext.invoices.service.IInvoiceAccepter;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceHistoryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.ext.externalInterfaces.processor.AbstractIncomingMessageProcessor;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class InvoiceAccepterImpl extends AbstractIncomingMessageProcessor implements IInvoiceAccepter {

	private static final Logger LOG = LoggerFactory.getLogger(InvoiceAccepterImpl.class);

	@Autowired
	@Qualifier("accountInfoAcknoledgmentService")
	private IAccountInfoAcknoledgmentService srv;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageHistoryFacade;
	@Autowired
	private IExternalInterfaceHistoryService historyService;
	@Autowired
	private IExternalInterfaceService externalInterfaceService;

	@Override
	public MSG acknoledgeCreditMemo(MSG request) throws BusinessException {
		boolean success = false;
		try {
			success = this.srv.processCreditMemo(request);
			LOG.info("Credit memo acknowledgement processed success!");
		} catch (Exception e) {
			LOG.warn("Credit memo acknowledgement processed failure!", e);
			request.getHeaderCommon().setErrorCode(e.getMessage());
			request.getHeaderCommon().setErrorDescription(e.getMessage());
		}
		this.updateMessageHistory(request, success);
		this.historyService.updateHistoryOutboundFromACK(success, request.getHeaderCommon().getCorrelationID(), true);
		return request;
	}

	@Override
	public MSG acknoledgeInvoice(MSG request) throws BusinessException {
		boolean success = false;
		try {
			success = this.srv.processInvoice(request);
			LOG.info("Invoice acknowledgement processed success!");
		} catch (Exception e) {
			LOG.warn("Invoice acknowledgement processed failure!", e);
			request.getHeaderCommon().setErrorCode(e.getMessage());
			request.getHeaderCommon().setErrorDescription(e.getMessage());
		}

		this.updateMessageHistory(request, success);
		this.historyService.updateHistoryOutboundFromACK(success, request.getHeaderCommon().getCorrelationID(), true);
		return request;
	}

	private void updateMessageHistory(MSG message, boolean success) throws BusinessException {
		ExternalInterfaceMessageHistoryStatus status = success ? ExternalInterfaceMessageHistoryStatus._ACKNOWLEDGED_
				: ExternalInterfaceMessageHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_;
		this.messageHistoryFacade.updateMessageHistory(message, status);
	}

	@Override
	protected ExternalInterface getExternalInterface() {
		return this.externalInterfaceService.findByName(ExtInterfaceNames.ACK_INVOICE_INTERFACE.getValue());
	}
}
