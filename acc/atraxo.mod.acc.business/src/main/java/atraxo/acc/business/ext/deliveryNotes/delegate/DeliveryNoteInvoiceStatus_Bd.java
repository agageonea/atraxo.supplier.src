package atraxo.acc.business.ext.deliveryNotes.delegate;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author apetho
 */
public class DeliveryNoteInvoiceStatus_Bd extends AbstractBusinessDelegate {
	private static final Logger LOG = LoggerFactory.getLogger(Invoice.class);

	public void modifyDeliveryNotesSources(BillStatus status, InvoiceLine invoiceLine) throws BusinessException {
		IDeliveryNoteService dnService = (IDeliveryNoteService) this.findEntityService(DeliveryNote.class);
		List<DeliveryNote> list = dnService.findByObjectIdAndType(invoiceLine.getId(), invoiceLine.getClass().getSimpleName());
		for (DeliveryNote dn : list) {
			this.modifyChildBillStatus(dn, status);
		}
	}

	private void modifyFuelEventStatus(DeliveryNote dn, BillStatus status) throws BusinessException {
		IFuelEventService fuelEventService = (IFuelEventService) this.findEntityService(FuelEvent.class);
		FuelEvent fuelEvent = dn.getFuelEvent();
		fuelEvent.setBillStatus(status);
		fuelEvent.setRegenerateDetails(false);
		fuelEventService.update(fuelEvent);
	}

	private void modifyChildBillStatus(DeliveryNote deliveryNote, BillStatus status) throws BusinessException {
		for (DeliveryNote dn : deliveryNote.getFuelEvent().getDeliveryNotes()) {
			if (QuantitySource._FUEL_TICKET_.equals(dn.getSource())) {
				this.modifyFuelTicket(dn, status);
			}
			if (QuantitySource._FUEL_ORDER_.equals(dn.getSource())) {
				this.modifyFuelOrder(dn, status);
			}
		}
		this.modifyFuelEventStatus(deliveryNote, status);
	}

	private void modifyFuelTicket(DeliveryNote dn, BillStatus status) throws BusinessException {
		IFuelTicketService ticketService = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		Integer fuelTicketId = dn.getObjectId();
		FuelTicket fuelTicket = ticketService.findById(fuelTicketId);
		fuelTicket.setBillStatus(status);
		ticketService.updateWithoutBL(Arrays.asList(fuelTicket));
	}

	private void modifyFuelOrder(DeliveryNote dn, BillStatus status) throws BusinessException {
		IFlightEventService flightService = (IFlightEventService) this.findEntityService(FlightEvent.class);
		Integer flightEventId = dn.getObjectId();
		FlightEvent flightEvent = flightService.findById(flightEventId);
		if (flightEvent.getLocOrder() != null) {
			flightEvent.setBillStatus(status);
			flightService.updateWithoutBusiness(flightEvent);
		}
	}

}
