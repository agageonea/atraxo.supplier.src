package atraxo.acc.business.ext.invoices.job.send;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.domain.ext.mailmerge.dto.CustomerDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDataDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.SubsidiaryDto;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.api.exceptions.BusinessException;

class EmailDtoFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailDtoFactory.class);

	@SuppressWarnings("unused")
	private void EmailDto() {
	}

	protected static final EmailDto getEmailDto(Customer issuer, List<Contacts> receivers, String bccParam, String replyAddresses)
			throws BusinessException {
		LOGGER.info("Build customer data transfer object.");
		CustomerDto customerDto = new CustomerDto();
		SubsidiaryDto subsidiaryDto = new SubsidiaryDto();
		if (issuer != null && issuer.getResponsibleBuyer() != null) {
			customerDto.setAccountManager(issuer.getResponsibleBuyer().getFirstName() + " " + issuer.getResponsibleBuyer().getLastName());
			subsidiaryDto.setName(issuer.getName());
		}

		List<EmailAddressesDto> emails = new ArrayList<>();
		for (Contacts receiver : receivers) {
			EmailDataDto emailData = new EmailDataDto();
			emailData.setEmail(receiver.getEmail());
			emailData.setFirstName(receiver.getFirstName());
			emailData.setLastName(receiver.getLastName());
			emailData.setTitle(receiver.getTitle().getName());
			emailData.setFullName(receiver.getFullNameField());
			emailData.setCustomer(customerDto);
			emailData.setSubsidiary(subsidiaryDto);

			EmailAddressesDto emailAddress = new EmailAddressesDto();
			emailAddress.setAddress(emailData.getEmail());
			emailAddress.setName(emailData.getFullName());
			emailAddress.setTitle(emailData.getTitle());
			emailAddress.setData(emailData);

			emails.add(emailAddress);
		}

		EmailDto emailDto = new EmailDto();
		emailDto.setFrom(replyAddresses);// this.sysParamSrv.getNoReplyAddress()
		emailDto.setSubject("Your invoice(s) have been issued");
		emailDto.setTo(emails);
		if (!bccParam.isEmpty()) {
			emailDto.setBcc(new HashSet<>(Arrays.asList(bccParam.split(","))));
		}
		return emailDto;
	}
}
