/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.payableAccruals;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link PayableAccrual} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class PayableAccrual_Service
		extends
			AbstractEntityService<PayableAccrual> {

	/**
	 * Public constructor for PayableAccrual_Service
	 */
	public PayableAccrual_Service() {
		super();
	}

	/**
	 * Public constructor for PayableAccrual_Service
	 * 
	 * @param em
	 */
	public PayableAccrual_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<PayableAccrual> getEntityClass() {
		return PayableAccrual.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return PayableAccrual
	 */
	public PayableAccrual findByFuelEventContract(Contract contract,
			FuelEvent fuelEvent) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							PayableAccrual.NQ_FIND_BY_FUELEVENTCONTRACT,
							PayableAccrual.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contract", contract)
					.setParameter("fuelEvent", fuelEvent).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PayableAccrual", "contract, fuelEvent"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PayableAccrual", "contract, fuelEvent"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return PayableAccrual
	 */
	public PayableAccrual findByFuelEventContract(Long contractId,
			Long fuelEventId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							PayableAccrual.NQ_FIND_BY_FUELEVENTCONTRACT_PRIMITIVE,
							PayableAccrual.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("contractId", contractId)
					.setParameter("fuelEventId", fuelEventId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PayableAccrual", "contractId, fuelEventId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PayableAccrual", "contractId, fuelEventId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return PayableAccrual
	 */
	public PayableAccrual findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(PayableAccrual.NQ_FIND_BY_BUSINESS,
							PayableAccrual.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PayableAccrual", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PayableAccrual", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByFuelEvent(FuelEvent fuelEvent) {
		return this.findByFuelEventId(fuelEvent.getId());
	}
	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByFuelEventId(Integer fuelEventId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PayableAccrual e where e.clientId = :clientId and e.fuelEvent.id = :fuelEventId",
						PayableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelEventId", fuelEventId).getResultList();
	}
	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PayableAccrual e where e.clientId = :clientId and e.location.id = :locationId",
						PayableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PayableAccrual e where e.clientId = :clientId and e.customer.id = :customerId",
						PayableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findBySupplier(Suppliers supplier) {
		return this.findBySupplierId(supplier.getId());
	}
	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findBySupplierId(Integer supplierId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PayableAccrual e where e.clientId = :clientId and e.supplier.id = :supplierId",
						PayableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("supplierId", supplierId).getResultList();
	}
	/**
	 * Find by reference: expectedCurrency
	 *
	 * @param expectedCurrency
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByExpectedCurrency(
			Currencies expectedCurrency) {
		return this.findByExpectedCurrencyId(expectedCurrency.getId());
	}
	/**
	 * Find by ID of reference: expectedCurrency.id
	 *
	 * @param expectedCurrencyId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByExpectedCurrencyId(
			Integer expectedCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PayableAccrual e where e.clientId = :clientId and e.expectedCurrency.id = :expectedCurrencyId",
						PayableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("expectedCurrencyId", expectedCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: invoicedCurrency
	 *
	 * @param invoicedCurrency
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByInvoicedCurrency(
			Currencies invoicedCurrency) {
		return this.findByInvoicedCurrencyId(invoicedCurrency.getId());
	}
	/**
	 * Find by ID of reference: invoicedCurrency.id
	 *
	 * @param invoicedCurrencyId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByInvoicedCurrencyId(
			Integer invoicedCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PayableAccrual e where e.clientId = :clientId and e.invoicedCurrency.id = :invoicedCurrencyId",
						PayableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("invoicedCurrencyId", invoicedCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: accrualCurrency
	 *
	 * @param accrualCurrency
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByAccrualCurrency(Currencies accrualCurrency) {
		return this.findByAccrualCurrencyId(accrualCurrency.getId());
	}
	/**
	 * Find by ID of reference: accrualCurrency.id
	 *
	 * @param accrualCurrencyId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByAccrualCurrencyId(
			Integer accrualCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PayableAccrual e where e.clientId = :clientId and e.accrualCurrency.id = :accrualCurrencyId",
						PayableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("accrualCurrencyId", accrualCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByContract(Contract contract) {
		return this.findByContractId(contract.getId());
	}
	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByContractId(Integer contractId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PayableAccrual e where e.clientId = :clientId and e.contract.id = :contractId",
						PayableAccrual.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractId", contractId).getResultList();
	}
}
