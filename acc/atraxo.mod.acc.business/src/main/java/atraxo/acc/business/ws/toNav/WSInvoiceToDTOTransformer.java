package atraxo.acc.business.ws.toNav;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.profile.IAccountingRulesService;
import atraxo.fmbas.business.api.ws.WSModelToDTOTransformer;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.AccountingRulesType;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.profile.AccountingRules;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.ws.dto.WSPayloadDataDTO;
import atraxo.fmbas.domain.ws.dto.WSTransportDataType;
import atraxo.fmbas.domain.ws.fuelEvent.Any;
import atraxo.fmbas.domain.ws.fuelEvent.Any.FuelPlusFuelEvent;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.business.utils.DateUtils;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author abolindu
 */
public class WSInvoiceToDTOTransformer extends AbstractBusinessBaseService implements WSModelToDTOTransformer<OutgoingInvoiceLine> {

	static final Logger logger = LoggerFactory.getLogger(WSInvoiceToDTOTransformer.class);

	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IContractService contractService;
	@Autowired
	private IAccountingRulesService accountingRulesService;

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#transformModelToDTO(java.util.Collection, java.util.Map)
	 */
	@Override
	public WSPayloadDataDTO transformModelToDTO(Collection<OutgoingInvoiceLine> modelEntities, Map<String, String> paramMap) throws JAXBException {
		if (logger.isDebugEnabled()) {
			logger.debug("START transformModelToDTO()");
		}

		WSPayloadDataDTO dto = new WSPayloadDataDTO();

		if (!modelEntities.isEmpty()) {
			AbstractEntity entity = modelEntities.iterator().next();
			OutgoingInvoiceLine invoiceLineModel = null;
			try {
				invoiceLineModel = (OutgoingInvoiceLine) entity;
			} catch (ClassCastException e) {
				logger.error("ERROR: could not cast " + entity.getClass() + " to " + OutgoingInvoiceLine.class + " !", e);
				throw e;
			}

			if (invoiceLineModel != null) {
				Any customerAccountAny = this.toFuelEvents(invoiceLineModel,
						paramMap.containsKey(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_PAYABLE),
						paramMap.containsKey(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_CREDIT_MEMO));
				dto.setPayload(XMLUtils.toXML(customerAccountAny, customerAccountAny.getClass()));
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("END transformModelToDTO()");
		}
		return dto;
	}

	/**
	 * @param invoiceLine
	 * @param hasPayableCost
	 * @param isCreditMemo
	 * @return
	 */
	private Any toFuelEvents(OutgoingInvoiceLine invoiceLine, boolean hasPayableCost, boolean isCreditMemo) {
		Any any = new Any();
		FuelPlusFuelEvent fuelPlusFuelEvent = new FuelPlusFuelEvent();
		FuelPlusFuelEvent.FuelEvents fuelEvents = new FuelPlusFuelEvent.FuelEvents();

		boolean splitComposite = false;
		fuelEvents.setInvoiceNumber(invoiceLine.getOutgoingInvoice().getInvoiceNo());
		if (hasPayableCost) {
			fuelEvents.setContractHolder(this.getCustomerCode(invoiceLine.getOutgoingInvoice().getReceiver(), invoiceLine));
			fuelEvents.setPayableCost(invoiceLine.getFuelEvent().getPayableCost());
			fuelEvents.setPayableCostCurrency(
					invoiceLine.getFuelEvent().getPayableCostCurrency() != null ? invoiceLine.getFuelEvent().getPayableCostCurrency().getCode() : "");
			fuelEvents.setReceivableCost(BigDecimal.valueOf(0));
			fuelEvents.setReceivableCostCurrency("");

			splitComposite = this.splitComposite(invoiceLine.getOutgoingInvoice(), true);

		} else {
			fuelEvents.setContractHolder(this.getCustomerCode(invoiceLine.getOutgoingInvoice().getIssuer(), invoiceLine));
			fuelEvents.setPayableCost(BigDecimal.valueOf(0));
			fuelEvents.setPayableCostCurrency("");
			fuelEvents.setReceivableCost(this.checkAmount(invoiceLine.getAmount(), isCreditMemo));
			fuelEvents.setReceivableCostCurrency(
					invoiceLine.getOutgoingInvoice().getCurrency() != null ? invoiceLine.getOutgoingInvoice().getCurrency().getCode() : "");

			splitComposite = this.splitComposite(invoiceLine.getOutgoingInvoice(), false);
		}

		Customer customer = this.getCustomer(invoiceLine);

		fuelEvents.setCustomerAccountNumber(customer.getCode());
		fuelEvents.setCustomerErpNo(customer.getAccountNumber());
		fuelEvents.setFuelEventID(BigInteger.valueOf(invoiceLine.getFuelEvent().getId()));
		fuelEvents.setFuelEventType(isCreditMemo ? "CreditMemo" : "Invoice");
		if (isCreditMemo) {
			fuelEvents.setReasonCode(invoiceLine.getOutgoingInvoice().getCreditMemoReason().getCode());
		}
		fuelEvents.setFuelingDate(DateUtils.toXMLGregorianCalendar(invoiceLine.getDeliveryDate()));
		fuelEvents.setPostingDate(DateUtils.toXMLGregorianCalendar(invoiceLine.getOutgoingInvoice().getInvoiceDate()));
		fuelEvents.setSettlementDate(DateUtils.toXMLGregorianCalendar(invoiceLine.getOutgoingInvoice().getBaseLineDate()));
		fuelEvents.setShipTo(invoiceLine.getCustomer() != null ? invoiceLine.getCustomer().getAccountNumber() : "");
		fuelEvents.setDepartureLocation(this.determineDepartureLocation(invoiceLine));
		fuelEvents.setDeliveryTicketNumber(invoiceLine.getTicketNo());
		fuelEvents.setAircraftRegistrationNo(invoiceLine.getAircraftRegistrationNumber());
		fuelEvents.setFlightID(invoiceLine.getFuelEvent().getFlightID());
		fuelEvents.setFuelSupplier(this.getAccountNumber(invoiceLine.getFuelEvent().getFuelSupplier(), invoiceLine));
		fuelEvents.setIPLAgent(this.getAccountNumber(invoiceLine.getFuelEvent().getIplAgent(), invoiceLine));
		fuelEvents.setProduct(invoiceLine.getFuelEvent().getProduct().getIataName());
		fuelEvents.setQuantity(invoiceLine.getQuantity() != null ? invoiceLine.getQuantity() : BigDecimal.ZERO);
		fuelEvents.setUOM(invoiceLine.getOutgoingInvoice().getUnit() != null ? invoiceLine.getOutgoingInvoice().getUnit().getCode() : "");
		fuelEvents.setPaymentTerms(
				this.getPaymentTerms(invoiceLine.getOutgoingInvoice().getReferenceDocType(), invoiceLine.getOutgoingInvoice().getReferenceDocNo()));
		fuelEvents.setGrossQuantity(invoiceLine.getQuantity() != null ? invoiceLine.getQuantity() : BigDecimal.ZERO);
		fuelEvents.setNetQuantity(invoiceLine.getFuelEvent() != null ? invoiceLine.getFuelEvent().getNetUpliftQuantity() : BigDecimal.ZERO);
		fuelEvents.setNetQuantityUOM(
				invoiceLine.getFuelEvent().getNetUpliftUnit() != null ? invoiceLine.getFuelEvent().getNetUpliftUnit().getCode() : "");
		fuelEvents.setVATAmountTotal(invoiceLine.getVat() != null ? this.checkAmount(invoiceLine.getVat(), isCreditMemo) : BigDecimal.ZERO);
		fuelEvents.setReasonCode(isCreditMemo ? invoiceLine.getOutgoingInvoice().getCreditMemoReason().getCode() : "");
		fuelEvents.setIsInternationalFlight(invoiceLine.getFuelEvent().getEventType().equals(FlightTypeIndicator._INTERNATIONAL_));

		fuelEvents.setTemperature(BigDecimal.valueOf(invoiceLine.getFuelEvent() != null
				? (invoiceLine.getFuelEvent().getTemperature() != null ? invoiceLine.getFuelEvent().getTemperature() : 15) : 15));
		fuelEvents.setDensity(invoiceLine.getFuelEvent() != null
				? (invoiceLine.getFuelEvent().getDensity() != null ? invoiceLine.getFuelEvent().getDensity() : BigDecimal.valueOf(0.8))
				: BigDecimal.valueOf(0.8));

		FuelPlusFuelEvent.FuelEvents.FuelEventLines fuelEventLines = new FuelPlusFuelEvent.FuelEvents.FuelEventLines();
		List<FuelPlusFuelEvent.FuelEvents.FuelEventLines.FuelEventLine> fuelEventLineList = fuelEventLines.getFuelEventLine();
		for (OutgoingInvoiceLineDetails invoiceLineDetails : invoiceLine.getDetails()) {

			// SONE-6005
			if (splitComposite && invoiceLineDetails.getPriceCategory().getPricePer().equals(PriceInd._COMPOSITE_)) {
				continue;
			}
			if (!splitComposite && invoiceLineDetails.getCalculateIndicator().equals(CalculateIndicator._CALCULATE_ONLY_)) {
				continue;
			}

			FuelPlusFuelEvent.FuelEvents.FuelEventLines.FuelEventLine fuelEventLine = new FuelPlusFuelEvent.FuelEvents.FuelEventLines.FuelEventLine();
			fuelEventLine.setPriceName(invoiceLineDetails.getPriceName());
			String priceCategoryStr = "Product".equalsIgnoreCase(invoiceLineDetails.getMainCategory().getCode())
					? invoiceLineDetails.getOutgoingInvoice().getProduct().getIataName()
					: invoiceLineDetails.getPriceCategory() != null ? (invoiceLineDetails.getPriceCategory().getIata() != null
							? invoiceLineDetails.getPriceCategory().getIata().getCode() : "") : "";
			fuelEventLine.setPriceCategory(priceCategoryStr);
			fuelEventLine.setMainCategory(invoiceLineDetails.getMainCategory() != null ? invoiceLineDetails.getMainCategory().getCode() : "");
			fuelEventLine.setOriginalPrice(invoiceLineDetails.getOriginalPrice() != null ? invoiceLineDetails.getOriginalPrice() : BigDecimal.ZERO);
			fuelEventLine.setOriginalPriceCurrency(
					invoiceLineDetails.getOriginalPriceCurrency() != null ? invoiceLineDetails.getOriginalPriceCurrency().getCode() : "");
			fuelEventLine.setOriginalPriceUOM(
					invoiceLineDetails.getOriginalPriceUnit() != null ? invoiceLineDetails.getOriginalPriceUnit().getCode() : "");
			fuelEventLine
					.setSettlementPrice(invoiceLineDetails.getSettlementPrice() != null ? invoiceLineDetails.getSettlementPrice() : BigDecimal.ZERO);
			fuelEventLine.setSettlementAmount(invoiceLineDetails.getSettlementAmount() != null
					? this.checkAmount(invoiceLineDetails.getSettlementAmount(), isCreditMemo) : BigDecimal.ZERO);
			fuelEventLine.setSettlementPriceCurrency(invoiceLineDetails.getOutgoingInvoice().getCurrency() != null
					? invoiceLineDetails.getOutgoingInvoice().getCurrency().getCode() : "");
			fuelEventLine.setExchangeRate(invoiceLineDetails.getExchangeRate() != null ? invoiceLineDetails.getExchangeRate() : BigDecimal.ONE);
			fuelEventLine.setVatAmount(
					invoiceLineDetails.getVatAmount() != null ? this.checkAmount(invoiceLineDetails.getVatAmount(), isCreditMemo) : BigDecimal.ZERO);
			fuelEventLineList.add(fuelEventLine);
		}
		fuelEvents.setFuelEventLines(fuelEventLines);
		fuelPlusFuelEvent.setFuelEvents(fuelEvents);
		any.setFuelPlusFuelEvent(fuelPlusFuelEvent);

		return any;
	}

	private String determineDepartureLocation(OutgoingInvoiceLine invoiceLine) {
		if (invoiceLine.getOutgoingInvoice().getDeliveryLoc() != null) {
			String iataCode = invoiceLine.getOutgoingInvoice().getDeliveryLoc().getIataCode();
			String icaoCode = invoiceLine.getOutgoingInvoice().getDeliveryLoc().getIcaoCode();

			if (!StringUtils.isEmpty(iataCode)) {
				return iataCode;
			} else if (!StringUtils.isEmpty(icaoCode)) {
				return icaoCode;
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	private Customer getCustomer(OutgoingInvoiceLine outgoingInvoiceLine) {
		try {
			Customer subsidiary = this.customerService.findByRefid(outgoingInvoiceLine.getOutgoingInvoice().getSubsidiaryId());
			AccountingRules accountingRules = this.accountingRulesService
					.findByAccountingRules(AccountingRulesType._REPLACE_ON_INVOICE_EXPORT_THE_INVOICE_RECEIVER_WITH_SHIP_TO_, subsidiary);
			if (accountingRules.getSubsidiary().getRefid().equalsIgnoreCase(subsidiary.getRefid())) {
				return outgoingInvoiceLine.getCustomer();
			}
		} catch (ApplicationException e) {
			logger.info("Acounting rules not found!", e);
			return outgoingInvoiceLine.getOutgoingInvoice().getReceiver();
		}
		return outgoingInvoiceLine.getOutgoingInvoice().getReceiver();
	}

	/**
	 * @param outgoingInvoice
	 * @param isPayableCost
	 * @return
	 */
	private boolean splitComposite(OutgoingInvoice outgoingInvoice, boolean isPayableCost) {
		try {
			Contract contract;
			if (isPayableCost) {
				contract = this.contractService.findByCode(outgoingInvoice.getReferenceDocNo()).getResaleRef();
			} else {
				contract = this.contractService.findByCode(outgoingInvoice.getReferenceDocNo());
			}

			this.accountingRulesService.findByAccountingRules(AccountingRulesType._EXPORT_DETAILED_COMPOSITE_PRICE_, contract.getHolder());

		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug(e.getMessage(), e);
			}
			return false;
		}
		return true;
	}

	private String getCustomerCode(Customer customer, OutgoingInvoiceLine invoiceLine) {
		if (customer == null) {
			return "";
		}
		boolean isResale = invoiceLine.getFuelEvent().getIsResale();
		return isResale ? customer.getCode() : customer.getAccountNumber();
	}

	/**
	 * @param cost
	 * @param isCreditMemo
	 * @return
	 */
	private BigDecimal checkAmount(BigDecimal cost, boolean isCreditMemo) {
		if (isCreditMemo) {
			return cost.negate();
		} else {
			return cost;
		}
	}

	/**
	 * @param suppliers
	 * @return
	 */
	private String getAccountNumber(Suppliers suppliers, OutgoingInvoiceLine outgoingInvoiceLIne) {
		if (suppliers != null) {
			Customer customer = this.customerService.findById(suppliers.getId());
			return this.getCustomerCode(customer, outgoingInvoiceLIne);
		}
		return "";
	}

	/**
	 * @param invoiceReferenceDocType
	 * @param referenceDocNo
	 * @return
	 */
	private String getPaymentTerms(InvoiceReferenceDocType invoiceReferenceDocType, String referenceDocNo) {
		StringBuilder paymentTerms = new StringBuilder();
		if (invoiceReferenceDocType == InvoiceReferenceDocType._CONTRACT_ && referenceDocNo != null) {
			Contract contract = this.contractService.findByCode(referenceDocNo);
			paymentTerms.append(contract.getPaymentTerms());
		}
		return paymentTerms.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#getTransportDataType()
	 */
	@Override
	public WSTransportDataType getTransportDataType() {
		return WSTransportDataType.SALES_INVOICES;
	}

}
