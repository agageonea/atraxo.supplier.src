package atraxo.acc.business.ext.deliveryNotes.service;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperation;
import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.fmbas.business.api.aircraft.IAircraftService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.Suffix;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class DeliveryNoteBuilderService extends AbstractBusinessBaseService {

	@Autowired
	private IAircraftService aircraftSrv;

	/**
	 * @param il
	 * @return
	 * @throws BusinessException
	 */
	public DeliveryNote build(InvoiceLine il) throws BusinessException {
		Aircraft aircraft = this.getAircraft(il.getAirlineDesignator(), il.getAircraftRegistrationNumber());
		DeliveryNote dn = new DeliveryNote();
		dn.setAircraft(aircraft);
		dn.setCustomer(il.getInvoice().getReceiver());
		dn.setFlightNumber(il.getFlightNo());
		dn.setFuelingDate(il.getDeliveryDate());
		dn.setDeparture(il.getInvoice().getDeliveryLoc());
		dn.setSuffix(il.getSuffix());
		dn.setSource(QuantitySource._INVOICE_);
		dn.setQuantity(il.getQuantity());
		dn.setUnit(il.getInvoice().getUnit());
		dn.setFuelSupplier(il.getInvoice().getIssuer());
		dn.setIplAgent(null);
		dn.setObjectId(il.getId());
		dn.setObjectType(il.getClass().getSimpleName());
		dn.setTicketNumber(il.getTicketNo());
		dn.setEventType(il.getFlightType());
		dn.setFuelingOperation(FuelTicketFuelingOperation._EMPTY_);
		dn.setTransport(FuelingType._EMPTY_);
		dn.setOperationType(OperationType._EMPTY_);
		dn.setProduct(Product._EMPTY_);
		dn.setShipTo(il.getAirlineDesignator());
		dn.setFlightID(il.getAirlineDesignator().getIataCode() + il.getFlightNo());
		return dn;

	}

	/**
	 * @param ft
	 * @param dn
	 * @param customer
	 * @param shipTo
	 * @return
	 */
	public DeliveryNote build(FuelTicket ft, DeliveryNote dn, Customer customer, Customer shipTo) {
		if (dn == null) {
			dn = new DeliveryNote();
		}
		dn.setAircraft(ft.getRegistration());
		dn.setCustomer(customer != null ? customer : ft.getCustomer());
		dn.setFlightNumber(ft.getFlightNo());
		dn.setFuelingDate(ft.getDeliveryDate());
		dn.setDeparture(ft.getDeparture());
		dn.setSuffix(ft.getSuffix());
		dn.setSource(QuantitySource._FUEL_TICKET_);
		dn.setQuantity(ft.getUpliftVolume());
		dn.setUnit(ft.getUpliftUnit());
		dn.setFuelSupplier(ft.getSupplier());
		dn.setIplAgent(ft.getFueller());
		dn.setObjectId(ft.getId());
		dn.setObjectType(ft.getClass().getSimpleName());
		dn.setTicketNumber(ft.getTicketNo());
		dn.setEventType(ft.getIndicator());
		dn.setNetUpliftQuantity(ft.getNetUpliftQuantity());
		dn.setNetUpliftUnit(ft.getNetUpliftUnit());
		dn.setFuelingOperation(ft.getFuelingOperation());
		dn.setTransport(ft.getTransport());
		dn.setOperationType(OperationType._EMPTY_);
		dn.setProduct(ft.getProductType());
		dn.setShipTo(shipTo);
		dn.setFlightID(ft.getAirlineDesignator() + ft.getFlightNo());
		dn.setTicketStatus(ft.getTicketStatus());
		return dn;
	}

	/**
	 * @param fe
	 * @return
	 * @throws BusinessException
	 */
	public DeliveryNote build(FlightEvent fe) throws BusinessException {
		DeliveryNote dn = new DeliveryNote();
		dn.setAircraft(
				!"".equals(fe.getRegistration()) ? this.getAircraft(fe.getLocOrder().getFuelOrder().getCustomer(), fe.getRegistration()) : null);
		dn.setCustomer(fe.getLocOrder().getFuelOrder().getCustomer());
		dn.setFlightNumber(fe.getFlighNo());
		dn.setFuelingDate(fe.getUpliftDate());
		dn.setDeparture(fe.getLocOrder().getLocation());
		dn.setSuffix(Suffix._EMPTY_);
		dn.setSource(QuantitySource._FUEL_ORDER_);
		dn.setQuantity(fe.getTotalQuantity());
		dn.setUnit(fe.getUnit());
		dn.setFuelSupplier(fe.getLocOrder().getSupplier());
		dn.setIplAgent(fe.getLocOrder().getIplAgent());
		dn.setObjectId(fe.getId());
		dn.setObjectType(fe.getClass().getSimpleName());
		dn.setTicketNumber(null);
		dn.setEventType(fe.getEventType());
		dn.setFuelingOperation(FuelTicketFuelingOperation._EMPTY_);
		dn.setTransport(FuelingType._EMPTY_);
		dn.setOperationType(fe.getOperationType());
		dn.setShipTo(fe.getLocOrder().getFuelOrder().getCustomer());
		dn.setProduct(fe.getProduct());
		dn.setFlightID(fe.getFlighNo());
		return dn;
	}

	private Aircraft getAircraft(Customer customer, String registration) throws BusinessException {
		try {
			return this.aircraftSrv.findByReg(customer, registration);
		} catch (ApplicationException e) {
			throw new BusinessException(BusinessErrorCode.INVALID_AIRCRAFT_REGISTRATION_NUMBER,
					BusinessErrorCode.INVALID_AIRCRAFT_REGISTRATION_NUMBER.getErrMsg(), e);
		}
	}
}
