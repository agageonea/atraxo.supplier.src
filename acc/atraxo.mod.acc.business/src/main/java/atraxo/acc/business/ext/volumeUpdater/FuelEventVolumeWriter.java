package atraxo.acc.business.ext.volumeUpdater;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;

public class FuelEventVolumeWriter implements ItemWriter<Map<Contract, Map<ShipTo, BigDecimal>>> {

	@Autowired
	private IContractService contractService;

	@Override
	public void write(List<? extends Map<Contract, Map<ShipTo, BigDecimal>>> items) throws Exception {
		if (items == null || items.isEmpty()) {
			return;
		}
		for (Map<Contract, Map<ShipTo, BigDecimal>> item : items) {
			List<Contract> updateList = new ArrayList<>();
			for (Entry<Contract, Map<ShipTo, BigDecimal>> entry : item.entrySet()) {
				Contract contract = this.contractService.findById(entry.getKey().getId());
				for (Entry<ShipTo, BigDecimal> shiToEntry : entry.getValue().entrySet()) {
					ShipTo oldShipTo = shiToEntry.getKey();
					ShipTo shipTo = this.getShipTo(contract, oldShipTo);
					if (shipTo != null) {
						shipTo.setActualVolume(shiToEntry.getValue());
					} else {
						for (ShipTo temp : contract.getShipTo()) {
							temp.setActualVolume(null);
						}
					}
				}
				updateList.add(contract);
			}
			this.contractService.updateWithoutBusinessLogic(updateList);
		}
	}

	private ShipTo getShipTo(Contract contract, ShipTo shipTo) {
		for (ShipTo st : contract.getShipTo()) {
			if (st.equals(shipTo)) {
				return st;
			}
		}
		return null;
	}

}
