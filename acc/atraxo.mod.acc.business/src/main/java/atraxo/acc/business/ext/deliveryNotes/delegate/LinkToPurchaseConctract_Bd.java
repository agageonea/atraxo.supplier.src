package atraxo.acc.business.ext.deliveryNotes.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.invoiceLines.IInvoiceLineService;
import atraxo.acc.domain.ext.deliveryNotes.DeliveryNoteSource;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;
import seava.j4e.business.utils.DateUtils;

public class LinkToPurchaseConctract_Bd extends AbstractBusinessDelegate {

	public void linkToPurchaseContract(DeliveryNote dn) throws BusinessException {
		QuantitySource source = dn.getSource();
		Collection<Contract> contracts = new ArrayList<>();
		switch (source) {
		case _FUEL_TICKET_:
			contracts = this.findFuelTicketContracts(dn);
			break;
		case _FUEL_ORDER_:
			contracts = this.findFuelOrderContracts(dn);
			break;
		case _INVOICE_:
			contracts = this.findInvoiceContracts(dn);
			break;
		default:
			break;
		}

		for (Contract c : contracts) {
			boolean find = false;
			if (dn.getDeliveryNoteContracts() != null) {
				for (DeliveryNoteContract rel : dn.getDeliveryNoteContracts()) {
					if (rel.getContracts().equals(c)) {
						find = true;
					}
				}
			}
			if (!find) {
				DeliveryNoteContract dnc = new DeliveryNoteContract();
				dnc.setContracts(c);
				dn.addToDeliveryNoteContracts(dnc);
				dn.setContractHolder(c.getHolder());
			}
		}
	}

	private List<Contract> findInvoiceContracts(DeliveryNote dn) throws BusinessException {
		IInvoiceLineService srv = (IInvoiceLineService) this.findEntityService(DeliveryNoteSource.getByObjectType(dn.getObjectType()).getClazz());
		InvoiceLine invLine = srv.findById(dn.getObjectId());
		return Arrays.asList(invLine.getInvoice().getContract());
	}

	private List<Contract> findFuelOrderContracts(DeliveryNote dn) throws BusinessException {
		IFlightEventService srv = (IFlightEventService) this.findEntityService(DeliveryNoteSource.getByObjectType(dn.getObjectType()).getClazz());
		FlightEvent event = srv.findById(dn.getObjectId());
		return Arrays.asList(event.getLocOrder().getContract());
	}

	/**
	 * Get Purchase contracts.
	 *
	 * @param dn
	 * @return
	 * @throws BusinessException
	 */
	private Collection<Contract> findFuelTicketContracts(DeliveryNote dn) throws BusinessException {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Map<ContractType, Contract> map = new EnumMap<>(ContractType.class);
		if (!Contract.class.getSimpleName().equalsIgnoreCase(dn.getFuelEvent().getBillPriceSourceType())
				|| dn.getFuelEvent().getBillPriceSourceId() == null) {
			return map.values();
		}

		Map<String, Object> params = new HashMap<>();
		params.put("supplier", dn.getFuelSupplier());
		params.put("location", dn.getDeparture());
		params.put("holder", dn.getCustomer());

		List<Contract> contracts = contractService.findEntitiesByAttributes(params);
		Collections.sort(contracts, new Comparator<Contract>() {
			@Override
			public int compare(Contract o1, Contract o2) {
				// order by status DESC (first EXP after EFF)
				int result = o2.getStatus().compareTo(o1.getStatus());
				if (result == 0) {
					return o1.getLimitedTo().compareTo(o2.getLimitedTo());
				}
				return result;
			}
		});
		Iterator<Contract> iter = contracts.iterator();
		Contract saleContract = contractService.findById(dn.getFuelEvent().getBillPriceSourceId());
		if (saleContract.getResaleRef() == null) {
			return map.values();
		}
		while (iter.hasNext()) {
			Contract c = iter.next();
			if (!saleContract.getResaleRef().equals(c)) {
				continue;
			}
			ContractStatus status = c.getStatus();
			Date validTo = DateUtils.modifyDate(c.getValidTo(), Calendar.DAY_OF_MONTH, 1);
			validTo = DateUtils.modifyDate(validTo, Calendar.MILLISECOND, -1);

			if ((ContractStatus._EFFECTIVE_.equals(status) || ContractStatus._EXPIRED_.equals(status))
					&& (c.getValidFrom().compareTo(dn.getFuelingDate()) <= 0 && validTo.compareTo(dn.getFuelingDate()) >= 0)) {
				FlightTypeIndicator dnEventType = dn.getEventType();
				FlightTypeIndicator cLimitedTo = c.getLimitedTo();
				if (dnEventType.equals(c.getLimitedTo())
						|| (map.get(c.getType()) == null) && (cLimitedTo.equals(FlightTypeIndicator._UNSPECIFIED_))) {
					map.put(c.getType(), c);
				}
			}
		}
		return map.values();
	}

}
