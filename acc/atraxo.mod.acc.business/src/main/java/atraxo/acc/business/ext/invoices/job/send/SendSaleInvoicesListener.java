package atraxo.acc.business.ext.invoices.job.send;

import java.util.List;
import java.util.Map;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;

import seava.j4e.scheduler.batch.listener.DefaultActionListener;

public class SendSaleInvoicesListener extends DefaultActionListener {

	@Override
	@SuppressWarnings("unchecked")
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);

		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder();
		List<StepExecution> stepExecution = (List<StepExecution>) jobExecution.getStepExecutions();

		for (StepExecution se : stepExecution) {
			ExecutionContext executionContext = se.getExecutionContext();
			Map<String, List<String>> executionLog = (Map<String, List<String>>) executionContext.get(SendSaleInvoicesTasklet.EXECUTION_LOG);

			this.printLogEntry(sb, executionLog, SendSaleInvoicesTasklet.TOTALS_LOG);
			this.printLogEntry(sb, executionLog, SendSaleInvoicesTasklet.CUSTOMER_LOG);

			status = new ExitStatus(status.getExitCode(), sb.toString());
			jobExecution.setExitStatus(status);
		}
	}

	/**
	 * @param sb
	 * @param executionLog
	 * @param entryKey
	 */
	private void printLogEntry(StringBuilder sb, Map<String, List<String>> executionLog, String entryKey) {
		for (Map.Entry<String, List<String>> logEntry : executionLog.entrySet()) {
			if (logEntry.getKey().contains(entryKey)) {
				sb.append(logEntry.getKey()).append(System.lineSeparator());

				for (String detail : logEntry.getValue()) {
					sb.append("\t").append(detail).append(System.lineSeparator());
				}

				sb.append(System.lineSeparator());
			}
		}
	}

}
