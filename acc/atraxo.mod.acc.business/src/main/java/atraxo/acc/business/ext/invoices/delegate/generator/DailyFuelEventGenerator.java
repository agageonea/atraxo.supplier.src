/**
 *
 */
package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.utils.DateUtils;

/**
 * @author vhojda
 */
public class DailyFuelEventGenerator extends AbstractFuelEventGenerator {

	/**
	 *
	 */
	public DailyFuelEventGenerator() {
		// The explicit default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#filterByFrequency(java.util.Collection)
	 */
	@Override
	public void filterByFrequency(Collection<FuelEvent> events) {
		Date presentDate = new Date();
		for (Iterator<FuelEvent> it = events.iterator(); it.hasNext();) {
			FuelEvent event = it.next();
			// if in the future of in present day then remove it
			if (DateUtils.compareDates(event.getFuelingDate(), presentDate, true) >= 0) {
				it.remove();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#splitByFrequency(java.util.List)
	 */
	@Override
	public Map<InvoicingDatePeriod, List<FuelEvent>> splitByFrequency(List<FuelEvent> events) {
		Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap = new HashMap<>();
		for (FuelEvent event : events) {
			InvoicingDatePeriod period = new InvoicingDatePeriod(event.getFuelingDate());
			if (!eventsMap.containsKey(period)) {
				eventsMap.put(period, new ArrayList<FuelEvent>());
			}
			eventsMap.get(period).add(event);
		}
		return eventsMap;
	}

}
