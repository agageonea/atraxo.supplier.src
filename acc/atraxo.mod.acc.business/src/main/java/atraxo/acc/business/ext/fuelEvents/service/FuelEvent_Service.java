/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.ext.fuelEvents.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IOutgoingInvoiceLineService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.api.receivableAccruals.IReceivableAccrualService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.exposure.ExposureUpdater_Service;
import atraxo.acc.business.ext.receivableAccruals.delegate.ReceivableGenerator_Bd;
import atraxo.acc.business.ws.ExportFuelEventOperationEnum;
import atraxo.acc.business.ws.ExportFuelEventPumaRestClient;
import atraxo.acc.domain.ext.deliveryNotes.AccrualDateParameter;
import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.acc_type.ReceivedStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.api.ws.IRestClientCallbackService;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link FuelEvent} domain entity.
 */
public class FuelEvent_Service extends atraxo.acc.business.impl.fuelEvents.FuelEvent_Service
		implements IFuelEventService, IRestClientCallbackService<FuelEvent> {

	private static final Logger LOG = LoggerFactory.getLogger(FuelEvent_Service.class);

	@Autowired
	private CostCalculatorService calculatorSrv;
	@Autowired
	private ExposureUpdater_Service expSrv;
	@Autowired
	private IOutgoingInvoiceLineService invLineSrv;
	@Autowired
	private IOutgoingInvoiceService invSrv;
	@Autowired
	private IReceivableAccrualService receivableAccrualSrv;
	@Autowired
	private FuelEventModifierService feSrv;
	@Autowired
	private ExportFuelEventPumaRestClient exportFuelEventPumaRestClient;
	@Autowired
	private IExternalInterfaceService externalInterfaceService;
	@Autowired
	private IDeliveryNoteService deliveryNoteService;
	@Autowired
	private ISystemParameterService paramSrv;
	@Autowired
	private ICurrenciesService currenciesService;
	@Autowired
	private IFuelOrderLocationService orderLocationService;
	@Autowired
	private IContractService contractService;
	@Autowired
	private FuelEventManagerService fuelEventManagerService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private IFuelTicketService fuelTicketService;
	@Autowired
	private IChangeHistoryService historyService;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void export(List<FuelTicket> fuelTicketList) throws BusinessException {
		for (FuelTicket ticket : fuelTicketList) {
			List<FuelEvent> fuelEventList = this.findByFuelTicket(ticket);
			this.fuelEventManagerService.reOrderResaleFuelEvents(fuelEventList);
			for (FuelEvent fe : fuelEventList) {
				this.export(fe);
			}
		}
	}

	@Override
	@Transactional
	public void export(FuelEvent e) throws BusinessException {
		FuelEvent event = this.findById(e.getId());
		// if interface enabled make sure to set the transmission progress as in progress
		ExternalInterface exportFuelEventsInterface = this.externalInterfaceService
				.findByName(ExtInterfaceNames.EXPORT_FUEL_EVENTS_TO_NAV.getValue());
		Boolean exportSale = false;
		Boolean exportPurchase = false;
		if (exportFuelEventsInterface != null && exportFuelEventsInterface.getEnabled()
				&& FuelEventTransmissionStatus._NEW_.equals(event.getTransmissionStatusSale()) && event.getBillableCost() != null
				&& ReceivedStatus._RELEASED_.equals(event.getReceivedStatus())) {
			// event.setTransmissionStatusSale(FuelEventTransmissionStatus._IN_PROGRESS_);
			exportSale = true;
		}
		if (exportFuelEventsInterface != null && exportFuelEventsInterface.getEnabled()
				&& FuelEventTransmissionStatus._NEW_.equals(event.getTransmissionStatusPurchase()) && event.getPayableCost() != null
				&& ReceivedStatus._RELEASED_.equals(event.getReceivedStatus())) {
			// event.setTransmissionStatusPurchase(FuelEventTransmissionStatus._IN_PROGRESS_);
			exportPurchase = true;
		}
		// export
		if (exportSale || exportPurchase) {
			this.exportFuelEventPumaRestClient.export(event, this.getExportFuelEventOperation(e), false, false);
			this.onUpdate(event);
		}
	}

	/**
	 * @param e
	 * @return
	 */
	private ExportFuelEventOperationEnum getExportFuelEventOperation(FuelEvent e) {
		return FuelEventStatus._CANCELED_.equals(e.getStatus()) ? ExportFuelEventOperationEnum.CANCEL_FUEL_EVENT
				: ExportFuelEventOperationEnum.NEW_FUEL_EVENT;
	}

	@Override
	protected void postInsert(FuelEvent e) throws BusinessException {
		super.postInsert(e);
		if ((e.getBillableCost() != null) && (e.getBillableCost().compareTo(BigDecimal.ZERO) > 0)) {
			try {
				this.expSrv.addToUtilization(e);
			} catch (BusinessException be) {
				LOG.warn(be.getMessage(), be);
			}
		}
	}

	@Override
	protected void preUpdate(FuelEvent e) throws BusinessException {
		if (e.getRegenerateDetails() != null && e.getRegenerateDetails()) {
			this.feSrv.generateDetails(e);
		}
		if (e.getOldBillableCost() != null) {
			e.setStatus(FuelEventStatus._UPDATED_);
			try {
				this.expSrv.updateUtilization(e);
			} catch (BusinessException be) {
				LOG.warn(be.getMessage(), be);
			}
			// commented because of #3921
			// boolean exported = this.exportFuelEventPumaRestClient.export(e, ExportFuelEventOperationEnum.COST_UPDATE_FUEL_EVENT);
			// e.setTransmissionStatus(exported ? FuelEventTransmissionStatus._IN_PROGRESS_ : FuelEventTransmissionStatus._NEW_);

			this.updateInvoice(e);
		}
	}

	@Override
	@Transactional
	public void updateWithoutBusiness(FuelEvent e) throws BusinessException {
		this.onUpdate(e);
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	private void updateInvoice(FuelEvent e) throws BusinessException {
		List<OutgoingInvoiceLine> list = this.invLineSrv.findByFuelEvent(e);
		for (OutgoingInvoiceLine invLine : list) {
			if (InvoiceTypeAcc._CRN_.equals(invLine.getOutgoingInvoice().getInvoiceType()) || invLine.getIsCredited()
					|| (e.getBillableCost().compareTo(invLine.getAmount()) == 0
							&& e.getBillableCostCurrency().equals(invLine.getOutgoingInvoice().getCurrency()))) {
				continue;
			}
			switch (invLine.getOutgoingInvoice().getStatus()) {
			case _AWAITING_APPROVAL_:
				this.invSrv.updatePrice(invLine.getOutgoingInvoice().getId(), e);
				break;
			case _AWAITING_PAYMENT_:
			case _PAID_:
				// commented because of #3921
				// this.managePaidInvoice(e, invLine);
				break;
			default:
				break;
			}
		}
	}

	private void modifySourceStatus(List<FuelEvent> fuelEvents) throws BusinessException {
		for (FuelEvent fe : fuelEvents) {
			switch (fe.getQuantitySource()) {
			case _EMPTY_:
				break;
			case _FUEL_ORDER_:
				break;
			case _FUEL_TICKET_:
				Integer fuelTicketId = null;
				for (DeliveryNote dn : fe.getDeliveryNotes()) {
					if (dn.getObjectType().equalsIgnoreCase(FuelTicket.class.getSimpleName())) {
						fuelTicketId = dn.getObjectId();
					}
				}
				if (fuelTicketId != null) {
					FuelTicket ft = this.fuelTicketService.findByBusiness(fuelTicketId);
					ft.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
					this.fuelTicketService.update(ft);
					ChangeHistory ch = new ChangeHistory();
					ch.setObjectId(ft.getId());
					ch.setObjectType(ft.getClass().getSimpleName());
					ch.setObjectValue("Fuel event deleted");
					this.historyService.insert(ch);
				}
				break;
			case _INVOICE_:
				break;
			default:
				break;
			}
		}
	}

	@Override
	protected void preDelete(List<FuelEvent> list) throws BusinessException {
		super.preDelete(list);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Start delete fuel event!");
		}
		StringBuilder sb = new StringBuilder();
		List<FuelEvent> resaleEvents = new ArrayList<>();
		for (FuelEvent fe : list) {
			if (!ReceivedStatus._ON_HOLD_.equals(fe.getReceivedStatus())) {
				sb.append(sb.toString().equals("") ? fe.getTicketNumber() : (", " + fe.getTicketNumber()));
			}
			if (fe.getIsResale()) {
				for (DeliveryNote dn : fe.getDeliveryNotes()) {
					if (QuantitySource._FUEL_TICKET_.equals(dn.getSource())) {
						try {
							FuelTicket ft = this.fuelTicketService.findByBusiness(dn.getObjectId());
							List<FuelEvent> tempEvents = this.findByFuelTicket(ft);
							for (FuelEvent tempEvent : tempEvents) {
								if (!ReceivedStatus._ON_HOLD_.equals(tempEvent.getReceivedStatus())) {
									sb.append(sb.toString().equals("") ? fe.getTicketNumber() : (", " + fe.getTicketNumber()));
								}
								if (!list.contains(tempEvent)) {
									resaleEvents.add(tempEvent);
								}
							}
						} catch (ApplicationException ex) {
							if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
								throw new BusinessException(AccErrorCode.FUEL_EVENT_YOU_HAVE_NO_RIGHTS,
										AccErrorCode.FUEL_EVENT_YOU_HAVE_NO_RIGHTS.getErrMsg(), ex);
							} else if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(ex.getErrorCode())) {
								throw new BusinessException(AccErrorCode.FUEL_TICKET_DUPLICATE_BUSINESS_KEY,
										AccErrorCode.FUEL_TICKET_DUPLICATE_BUSINESS_KEY.getErrMsg(), ex);
							}
						}
					}
				}
			}
		}
		list.addAll(resaleEvents);
		if (!"".equals(sb.toString())) {
			throw new BusinessException(AccErrorCode.FUEL_EVENT_RELEASE_STATUS_NOT_ON_HOLD,
					String.format(AccErrorCode.FUEL_EVENT_RELEASE_STATUS_NOT_ON_HOLD.getErrMsg(), sb.toString()));
		}
		this.modifySourceStatus(list);

		if (LOG.isDebugEnabled()) {
			LOG.debug("End delete fuel event!");
		}
	}

	@Override
	protected void postDelete(FuelEvent e) throws BusinessException {
		super.postDelete(e);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Modify exposure!");
		}
		this.expSrv.removeFromUtilization(e);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.api.fuelEvents.IFuelEventService#getForOutgoingInvoices(java.util.Date, java.util.Date,
	 * atraxo.fmbas.domain.impl.customer.Customer, atraxo.fmbas.domain.impl.geo.Locations)
	 */
	@Override
	public List<FuelEvent> getForOutgoingInvoices(Date startDate, Date endDate, Customer customer, Locations location) throws BusinessException {
		List<FuelEvent> list = this.getFuelEvents(customer, location);
		for (Iterator<FuelEvent> iterator = list.iterator(); iterator.hasNext();) {
			boolean remove = false;
			FuelEvent fuelEvent = iterator.next();
			if (!fuelEvent.getInvoiceStatus().equals(OutgoingInvoiceStatus._NOT_INVOICED_)
					&& !fuelEvent.getInvoiceStatus().equals(OutgoingInvoiceStatus._CREDITED_)) {
				remove = true;
			}
			if (!this.isInPeriod(endDate, fuelEvent) || this.isCanceled(fuelEvent)) {
				remove = true;
			}
			if (!this.hasAccruals(fuelEvent)) {
				remove = true;
			} else {
				boolean flag = this.hasOpenAccruals(fuelEvent);
				if (flag) {
					remove = true;
				}
			}
			if ((fuelEvent.getBillPriceSourceType() == null) || (fuelEvent.getBillPriceSourceId() == null)) {
				remove = true;
			}
			if (remove) {
				iterator.remove();
			}
		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.api.fuelEvents.IFuelEventService#getForOutgoingInvoicesIgnoreStart(java.util.Date,
	 * atraxo.fmbas.domain.impl.customer.Customer, atraxo.fmbas.domain.impl.geo.Locations) calls getForOutgoingInvoices but the start date is set to
	 * the beginning of time ( if you consider 1970 the beginning of time )
	 */
	@Override
	public List<FuelEvent> getForOutgoingInvoicesIgnoreStart(Date endDate, Customer customer, Locations location) throws BusinessException {
		return this.getForOutgoingInvoices(new Date(0), endDate, customer, location);
	}

	/**
	 * @param customer
	 * @param location
	 * @return
	 */
	private List<FuelEvent> getFuelEvents(Customer customer, Locations location) {
		Map<String, Object> params = new HashMap<>();
		params.put("receivedStatus", ReceivedStatus._RELEASED_);
		if (customer != null) {
			params.put("customer", customer);
		}
		if (location != null) {
			params.put("departure", location);
		}
		return this.findEntitiesByAttributes(params);
	}

	/**
	 * @param fuelEvent
	 * @return
	 */
	private boolean hasOpenAccruals(FuelEvent fuelEvent) {
		for (ReceivableAccrual accrual : fuelEvent.getReceivableAccruals()) {
			if (accrual.getIsOpen()) {
				return false;
			}
		}
		return true;
	}

	private boolean hasAccruals(FuelEvent fuelEvent) {
		return !fuelEvent.getReceivableAccruals().isEmpty();
	}

	private boolean isCanceled(FuelEvent fuelEvent) {
		return FuelEventStatus._CANCELED_.equals(fuelEvent.getStatus());
	}

	private boolean isInPeriod(Date endDate, FuelEvent fuelEvent) {
		return fuelEvent.getFuelingDate().compareTo(endDate) <= 0;
	}

	@Override
	public void calculateReceivableCost(FuelEvent fuelEvent) throws BusinessException {
		if ((fuelEvent.getBillPriceSourceId() == null) || (fuelEvent.getBillPriceSourceType() == null)) {
			return;
		}
		BigDecimal cost = this.getCostInSysMeasurment(fuelEvent);
		fuelEvent.setSysBillableCost(cost);
		cost = this.getCostInSettMeasurment(fuelEvent);
		fuelEvent.setBillableCost(cost);
		this.setDeliveryNotesBillPrice(fuelEvent);
		this.getBusinessDelegate(ReceivableGenerator_Bd.class).generate(fuelEvent);
	}

	/**
	 * Get cost in settlement currency(orderLocation or contract) from fuel event bill source and set billable cost currency on fuel event.
	 *
	 * @param fuelEvent
	 * @return
	 * @throws BusinessException
	 */
	private BigDecimal getCostInSettMeasurment(FuelEvent fuelEvent) throws BusinessException {
		String accrualDateParameter = this.paramSrv.getAccrualEvaluationDate();
		Date date = GregorianCalendar.getInstance().getTime();
		ExchangeRate_Bd exchBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		if (accrualDateParameter.equalsIgnoreCase(AccrualDateParameter.EVENT_DATE.getValue())) {
			date = fuelEvent.getFuelingDate();
		}
		if (fuelEvent.getBillPriceSourceType().equalsIgnoreCase(FuelOrderLocation.class.getSimpleName())) {
			FuelOrderLocation orderLocation = this.orderLocationService.findById(fuelEvent.getBillPriceSourceId());
			fuelEvent.setBillableCostCurrency(orderLocation.getPaymentCurrency());
			BigDecimal cost = this.calculatorSrv.calculate(orderLocation, fuelEvent).getCost();
			return exchBd.convert(orderLocation.getPaymentCurrency(), fuelEvent.getBillableCostCurrency(), date, cost, false).getValue();
		} else if (fuelEvent.getBillPriceSourceType().equalsIgnoreCase(Contract.class.getSimpleName())) {
			Contract contract = this.contractService.findById(fuelEvent.getBillPriceSourceId());
			fuelEvent.setBillableCostCurrency(contract.getSettlementCurr());
			BigDecimal cost = this.calculatorSrv.calculate(contract, fuelEvent).getCost();
			return exchBd.convert(contract.getSettlementCurr(), fuelEvent.getBillableCostCurrency(), date, cost, false).getValue();
		} else {
			return BigDecimal.ZERO;
		}
	}

	private BigDecimal getCostInSysMeasurment(FuelEvent fuelEvent) throws BusinessException {
		String systemCurrencyCode = this.paramSrv.getSysCurrency();
		Currencies sysCurrency = this.currenciesService.findByCode(systemCurrencyCode);
		ExchangeRate_Bd exchBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		String accrualDateParameter = this.paramSrv.getAccrualEvaluationDate();
		Date date = new Date();
		if (accrualDateParameter.equalsIgnoreCase(AccrualDateParameter.EVENT_DATE.getValue())) {
			date = fuelEvent.getFuelingDate();
		}
		if (fuelEvent.getBillPriceSourceType().equalsIgnoreCase(FuelOrderLocation.class.getSimpleName())) {
			FuelOrderLocation orderLocation = this.orderLocationService.findById(fuelEvent.getBillPriceSourceId());
			BigDecimal cost = this.calculatorSrv.calculate(orderLocation, fuelEvent).getCost();
			return exchBd.convert(orderLocation.getPaymentCurrency(), sysCurrency, date, cost, false).getValue();
		} else if (fuelEvent.getBillPriceSourceType().equalsIgnoreCase(Contract.class.getSimpleName())) {
			Contract contract = this.contractService.findById(fuelEvent.getBillPriceSourceId());
			BigDecimal cost = this.calculatorSrv.calculate(contract, fuelEvent).getCost();
			return exchBd.convert(contract.getSettlementCurr(), sysCurrency, date, cost, false).getValue();
		} else {
			return BigDecimal.ZERO;
		}
	}

	private void setDeliveryNotesBillPrice(FuelEvent fuelEvent) throws BusinessException {
		for (DeliveryNote dn : fuelEvent.getDeliveryNotes()) {
			dn.setBillableCost(fuelEvent.getBillableCost());
			dn.setBillableCostCurrency(fuelEvent.getBillableCostCurrency());
			this.deliveryNoteService.update(dn);
		}
	}

	/*
	 * @Override protected void postDelete(FuelEvent e) throws BusinessException { super.postDelete(e); List<PayableAccrual> payableAccruals =
	 * this.payableAccrualSrv.findByFuelEvent(e); this.payableAccrualSrv.delete(payableAccruals); if ((e.getBillableCost() != null) &&
	 * (e.getBillableCost().compareTo(BigDecimal.ZERO) > 0)) { this.expSrv.removeFromUtilization(e); } }
	 */

	@Override
	public void resetSaleContract(Contract contract) throws BusinessException {
		Map<String, Object> paramsContract = new HashMap<>();
		paramsContract.put("billPriceSourceId", contract.getId());
		paramsContract.put("billPriceSourceType", Contract.class.getSimpleName());
		List<FuelEvent> oldList = this.findEntitiesByAttributes(paramsContract);
		List<ReceivableAccrual> deleteList = new ArrayList<>();
		for (FuelEvent fe : oldList) {
			if (fe.getInvoiceStatus().equals(OutgoingInvoiceStatus._AWAITING_PAYMENT_)
					|| fe.getInvoiceStatus().equals(OutgoingInvoiceStatus._PAID_)) {
				throw new BusinessException(AccErrorCode.INVOICE_STATUS_PAY, AccErrorCode.INVOICE_STATUS_PAY.getErrMsg());
			}
			fe.setBillableCost(null);
			fe.setBillableCostCurrency(null);
			fe.setBillPriceSourceId(null);
			fe.setBillPriceSourceType(null);
			fe.setBillStatus(BillStatus._NOT_BILLED_);
			deleteList.addAll(new ArrayList<ReceivableAccrual>(fe.getReceivableAccruals()));
		}
		this.update(oldList);
		this.receivableAccrualSrv.delete(deleteList);
	}

	@Override
	@Transactional
	@History(value = "Set on hold", type = FuelEvent.class)
	public void putOnHoldFuelEvents(List<FuelEvent> fuelEvents, @Reason String reason) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("START putOnHoldFuelEvents()");
		}

		// change the received status
		for (FuelEvent event : fuelEvents) {
			event.setReceivedStatus(ReceivedStatus._ON_HOLD_);
		}

		// persist
		this.update(fuelEvents);

		if (LOG.isDebugEnabled()) {
			LOG.debug("END putOnHoldFuelEvents()");
		}
	}

	@Override
	@Transactional
	@History(value = "Released", type = FuelEvent.class)
	public void releaseFuelEvents(List<FuelEvent> fuelEvents) throws BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("START releaseFuelEvents()");
		}

		// change the received status
		for (FuelEvent event : fuelEvents) {
			event.setReceivedStatus(ReceivedStatus._RELEASED_);
			this.onUpdate(event);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("END releaseFuelEvents()");
		}
	}

	@Override
	public List<FuelEvent> findByFuelTicket(FuelTicket ticket) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("objectType", FuelTicket.class.getSimpleName());
		params.put("objectId", ticket.getId());
		List<DeliveryNote> deliveryNoteList = this.deliveryNoteService.findEntitiesByAttributes(params);
		List<FuelEvent> retList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(deliveryNoteList)) {
			for (DeliveryNote dn : deliveryNoteList) {
				retList.add(dn.getFuelEvent());
			}
		}
		return retList;
	}

	@Override
	public BigDecimal calculateVolume(Date from, Date to, Unit unit) throws BusinessException {
		String hql = "select e from " + FuelEvent.class.getSimpleName()
				+ " e where e.clientId=:clientId and e.subsidiaryId in :subsidiaryIds and e.fuelingDate>=:from and e.fuelingDate<=:to";
		List<FuelEvent> fuelEvents = this.getEntityManager().createQuery(hql, FuelEvent.class)
				.setParameter("clientId", Session.user.get().getClient().getId())
				.setParameter("subsidiaryIds", Session.user.get().getProfile().getOrganizationIds()).setParameter("from", from).setParameter("to", to)
				.getResultList();
		Iterator<FuelEvent> iter = fuelEvents.iterator();
		while (iter.hasNext()) {
			FuelEvent fuelEvent = iter.next();
			if (FuelEventStatus._CANCELED_.equals(fuelEvent.getStatus())) {
				iter.remove();
			}
		}
		BigDecimal value = BigDecimal.ZERO;
		for (FuelEvent fe : fuelEvents) {
			BigDecimal density = this.getDensity(fe);
			BigDecimal convertedValue = this.unitService.convert(fe.getUnit(), unit, fe.getQuantity(), density.doubleValue());
			value = value.add(convertedValue, MathContext.DECIMAL64);
		}
		return value.setScale(0, RoundingMode.HALF_UP);
	}

	private BigDecimal getDensity(FuelEvent fe) throws BusinessException {
		if (fe.getDensity() != null) {
			return fe.getDensity();
		} else {
			String densityStr = this.paramSrv.getDensity();
			return new BigDecimal(densityStr);
		}
	}

	@Override
	@Transactional
	public void finish(WSExecutionResult result, Collection<FuelEvent> list) throws BusinessException {
		if (!result.isSuccess()) {
			List<FuelEvent> toUpdate = new ArrayList<>();
			for (FuelEvent fe : list) {
				try {
					FuelEvent fuelEvent = this.findById(fe.getId());

					if (fuelEvent.getBillableCost() != null) {
						fuelEvent.setTransmissionStatusSale(FuelEventTransmissionStatus._FAILED_);
					}
					if (fuelEvent.getPayableCost() != null) {
						fuelEvent.setTransmissionStatusPurchase(FuelEventTransmissionStatus._FAILED_);
					}

					toUpdate.add(fuelEvent);
				} catch (ApplicationException e) {
					if (J4eErrorCode.DB_NO_RESULT.equals(e.getErrorCode())) {
						// cutomer not found do nothing
						LOG.warn("Could not retrieve a fuel event by id " + fe.getId() + " ! Will do nothing !", e);
					} else {
						throw e;
					}
				}
			}
			this.update(toUpdate);
		}
	}

	/**
	 * Updates the Bill of Lading value from Fuel Event and propagates it to linked Outgoing Invoice Line The method proceeds the update of both
	 * entities
	 *
	 * @param bolNumber
	 * @param fuelEvent
	 * @throws BusinessException
	 */
	@Override
	@Transactional
	public void setBillOfLading(String bolNumber, FuelEvent fuelEvent) throws BusinessException {
		fuelEvent.setBolNumber(bolNumber);
		this.update(fuelEvent);

		List<OutgoingInvoiceLine> lines = this.invLineSrv.findByFuelEvent(fuelEvent);
		for (OutgoingInvoiceLine line : lines) {
			line.setBolNumber(bolNumber);
		}
		this.invLineSrv.update(lines);
	}
}
