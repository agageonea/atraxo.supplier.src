/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.ext.invoiceLines.service;

import java.math.MathContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IOutgoingInvoiceLineService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.api.outgoingInvoiceLineDetails.IOutgoingInvoiceLineDetailsService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.invoices.delegate.GeneratorBd;
import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.business.api.ws.IRestClientTimeoutService;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link OutgoingInvoiceLine} domain entity.
 */
public class OutgoingInvoiceLine_Service extends atraxo.acc.business.impl.invoiceLines.OutgoingInvoiceLine_Service
		implements IOutgoingInvoiceLineService, IRestClientTimeoutService<OutgoingInvoiceLine> {

	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;
	@Autowired
	private IOutgoingInvoiceLineDetailsService invoiceLineDetailsService;
	@Autowired
	private IFuelTicketService ticketSrv;
	@Autowired
	private IFuelEventService fuelEventService;

	@Override
	public boolean afterTimeout(OutgoingInvoiceLine entity) throws BusinessException {
		entity.setTransmissionStatus(InvoiceLineTransmissionStatus._FAILED_);
		return OutgoingInvoiceLine_Service.this.checkLines(entity);
	}

	private synchronized boolean checkLines(OutgoingInvoiceLine outgoingInvoiceLine) throws BusinessException {
		OutgoingInvoice outgoingInvoice = outgoingInvoiceLine.getOutgoingInvoice();
		int failedLines = this.getFailedInvoiceLines(outgoingInvoice).size();

		if ((failedLines + 1) == outgoingInvoice.getInvoiceLines().size()) {
			outgoingInvoice.setTransmissionStatus(InvoiceTransmissionStatus._FAILED_);
			this.outgoingInvoiceService.update(outgoingInvoice);
			return false;
		}
		return true;
	}

	@Override
	@Transactional
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		List<OutgoingInvoiceLine> invoiceLines = this.findByIds(ids);
		List<Object> invoiceLineDetails = new ArrayList<>();
		for (OutgoingInvoiceLine oil : invoiceLines) {
			for (OutgoingInvoiceLineDetails oild : oil.getDetails()) {
				invoiceLineDetails.add(oild.getId());
			}
			FuelEvent event = this.fuelEventService.findById(oil.getFuelEvent().getId());
			event.setCreditMemoNo("");
			event.setRegenerateDetails(false);
			this.fuelEventService.update(event);
		}
		this.updateHeaderOnDeleteInvoiceLine(invoiceLines);
		this.invoiceLineDetailsService.deleteByIds(invoiceLineDetails);
	}

	@Override
	public void updateHeaderOnDeleteInvoiceLine(List<OutgoingInvoiceLine> oils) throws BusinessException {
		OutgoingInvoice outInvoice = null;
		for (OutgoingInvoiceLine oil : oils) {
			if (BillStatus._AWAITING_PAYMENT_.equals(oil.getOutgoingInvoice().getStatus())
					|| BillStatus._PAID_.equals(oil.getOutgoingInvoice().getStatus())) {
				throw new BusinessException(AccErrorCode.CREDIT_NOTE_STATUS_DELETE, AccErrorCode.CREDIT_NOTE_STATUS_DELETE.getErrMsg());
			}
			outInvoice = oil.getOutgoingInvoice();
			outInvoice.setQuantity(outInvoice.getQuantity().subtract(oil.getQuantity(), MathContext.DECIMAL64));
			outInvoice.setNetAmount(outInvoice.getNetAmount().subtract(oil.getAmount(), MathContext.DECIMAL64));
			outInvoice.setVatAmount(outInvoice.getVatAmount().subtract(oil.getVat(), MathContext.DECIMAL64));
			outInvoice.setTotalAmount(outInvoice.getNetAmount().add(outInvoice.getVatAmount(), MathContext.DECIMAL64));
			if (oil.getReferenceInvoiceLine() != null) {
				oil.getReferenceInvoiceLine().setIsCredited(false);
				this.updateWithoutBusiness(Arrays.asList(oil.getReferenceInvoiceLine()));
			}

			OutgoingInvoiceStatus invStatus;
			if (InvoiceTypeAcc._CRN_.equals(outInvoice.getInvoiceType())) {
				OutgoingInvoice creditedInvoice = outInvoice.getInvoiceReference();

				switch (creditedInvoice.getStatus()) {
				case _AWAITING_PAYMENT_:
					invStatus = OutgoingInvoiceStatus._AWAITING_PAYMENT_;
					break;
				case _AWAITING_APPROVAL_:
					invStatus = OutgoingInvoiceStatus._AWAITING_APPROVAL_;
					break;
				case _PAID_:
					invStatus = OutgoingInvoiceStatus._PAID_;
					break;
				default:
					invStatus = OutgoingInvoiceStatus._NOT_INVOICED_;
					break;
				}

			} else {
				invStatus = OutgoingInvoiceStatus._NOT_INVOICED_;
			}

			this.updateSourcesInvoicedStatus(oil, invStatus);
		}
		this.adjustHeaderDates(outInvoice, oils);
		this.outgoingInvoiceService.update(outInvoice);
	}

	@Override
	protected void postUpdate(OutgoingInvoiceLine e) throws BusinessException {
		super.postUpdate(e);
		OutgoingInvoice oi = this.outgoingInvoiceService.findByBusiness(e.getOutgoingInvoice().getId());
		this.adjustHeaderDates(oi, new ArrayList<OutgoingInvoiceLine>());
		this.outgoingInvoiceService.update(oi);
	}

	/**
	 * @param outgoingInvoice
	 * @param excludeList
	 */
	public void adjustHeaderDates(OutgoingInvoice outgoingInvoice, List<OutgoingInvoiceLine> excludeList) {
		List<OutgoingInvoiceLine> invoiceLines = new ArrayList<>(outgoingInvoice.getInvoiceLines());
		invoiceLines.removeAll(excludeList);
		outgoingInvoice.setItemsNumber(invoiceLines.size());
		if (CollectionUtils.isEmpty(invoiceLines)) {
			return;
		}
		invoiceLines.sort((o1, o2) -> o1.getDeliveryDate().compareTo(o2.getDeliveryDate()));
		Date validFrom = invoiceLines.get(0).getDeliveryDate();
		Date validTo = invoiceLines.get(invoiceLines.size() - 1).getDeliveryDate();
		outgoingInvoice.setDeliveryDateFrom(validFrom);
		outgoingInvoice.setDeliverydateTo(validTo);
	}

	@Override
	@Transactional
	public void updateWithoutBusiness(List<OutgoingInvoiceLine> invoiceLines) throws BusinessException {
		for (OutgoingInvoiceLine oil : invoiceLines) {
			this.onUpdate(oil);
		}
	}

	private void updateSourcesInvoicedStatus(OutgoingInvoiceLine invLine, OutgoingInvoiceStatus invStatus) throws BusinessException {
		@SuppressWarnings("unchecked")
		GeneratorBd<AbstractEntity> bd = this.getBusinessDelegate(GeneratorBd.class);
		FuelEvent event = this.fuelEventService.findById(invLine.getFuelEvent().getId());
		if (QuantitySource._FUEL_TICKET_.equals(event.getQuantitySource())) {
			for (DeliveryNote dn : event.getDeliveryNotes()) {
				if (dn.getUsed()) {
					FuelTicket ticket = this.ticketSrv.findById(dn.getObjectId());
					ticket.setInvoiceStatus(invStatus);
					this.ticketSrv.updateWithoutBL(Arrays.asList(ticket));
				}
			}
		}
		switch (invLine.getOutgoingInvoice().getReferenceDocType()) {
		case _PURCHASE_ORDER_:
			bd.adjustFlightEventStatus(event, invStatus);
			break;
		case _CONTRACT_:
			bd.adjustFlightEventStatus(event, invStatus);
			break;
		default:
			break;
		}
		event.setInvoiceStatus(invStatus);
		if (invStatus.equals(OutgoingInvoiceStatus._NOT_INVOICED_)) {
			event.setSalesInvoiceNo("");
		}
		event.setRegenerateDetails(false);
		this.fuelEventService.update(event);
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public void insertNewInvoiceLines(List<FuelEvent> fuelEvents, OutgoingInvoice outgoingInvoice) throws BusinessException {
		this.getBusinessDelegate(GeneratorBd.class).addInvoiceLines(fuelEvents, outgoingInvoice);
	}

	@Override
	public List<OutgoingInvoiceLine> getFailedInvoiceLines(OutgoingInvoice outgoingInvoice) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("outgoingInvoice", outgoingInvoice);
		params.put("transmissionStatus", InvoiceLineTransmissionStatus._FAILED_);
		return this.findEntitiesByAttributes(params);

	}
}
