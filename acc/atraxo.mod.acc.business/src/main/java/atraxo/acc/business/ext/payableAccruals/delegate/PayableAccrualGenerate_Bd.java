package atraxo.acc.business.ext.payableAccruals.delegate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.payableAccruals.IPayableAccrualService;
import atraxo.acc.business.ext.fuelEvents.delegate.DensityDelegate;
import atraxo.acc.business.ext.fuelEvents.service.CostCalculatorService;
import atraxo.acc.domain.ext.deliveryNotes.AccrualDateParameter;
import atraxo.acc.domain.impl.acc_type.InvoiceProcessStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.cmm.domain.ext.contracts.CostVat;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class PayableAccrualGenerate_Bd extends AbstractBusinessDelegate {

	public void generate(FuelEvent fe) throws BusinessException {
		List<PayableAccrual> list = new ArrayList<>();
		if (fe.getPayableAccruals() != null) {
			list.addAll(fe.getPayableAccruals());
		}
		if (list.isEmpty()) {
			for (DeliveryNote dn : fe.getDeliveryNotes()) {
				if (dn.getPayableCost() == null) {
					continue;
				}
				Collection<DeliveryNoteContract> dnContracts = dn.getDeliveryNoteContracts() == null ? new ArrayList<>()
						: dn.getDeliveryNoteContracts();
				for (DeliveryNoteContract dnc : dnContracts) {
					PayableAccrual pa = this.getPayableAccrual(dn, dnc.getContracts());
					fe.addToPayableAccruals(pa);
				}
			}
		} else {
			PayableAccrual pa = list.get(0);
			DeliveryNote dn = null;
			for (DeliveryNote note : fe.getDeliveryNotes()) {
				if (note.getPayableCost() != null) {
					dn = note;
					break;
				}
			}
			if (dn != null) {
				this.setPricesAndAmounts(fe, pa.getContract(), pa);
			}

		}
	}

	public void generatePayableAccrual(DeliveryNote dn, Contract contract) throws BusinessException {
		if (dn == null || dn.getDeliveryNoteContracts() == null) {
			return;
		}
		IPayableAccrualService payableAccrualService = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		dn.getFuelEvent().setPayableCostCurrency(dn.getPayableCostCurrency());
		PayableAccrual pa = this.getPayableAccrual(dn, contract);
		payableAccrualService.insert(pa);
	}

	public PayableAccrual getPayableAccrual(DeliveryNote dn, Contract contract) throws BusinessException {
		FuelEvent fuelEvent = dn.getFuelEvent();
		PayableAccrual pa = new PayableAccrual();
		pa.setContract(contract);
		pa.setFuelEvent(fuelEvent);
		pa.setLocation(fuelEvent.getDeparture());
		pa.setCustomer(fuelEvent.getCustomer());
		pa.setSupplier(fuelEvent.getFuelSupplier());
		pa.setFuelingDate(fuelEvent.getFuelingDate());
		pa.setContractType(contract.getType());
		pa.setContractSubType(contract.getSubType());
		pa.setContractScope(contract.getScope());
		pa.setInvProcessStatus(InvoiceProcessStatus._NOT_INVOICED_);
		pa.setIsOpen(true);
		this.setPricesAndAmounts(fuelEvent, contract, pa);
		return pa;
	}

	private void setPricesAndAmounts(FuelEvent fuelEvent, Contract contract, PayableAccrual pa) throws BusinessException {
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		DensityDelegate densityDelegate = this.getBusinessDelegate(DensityDelegate.class);
		BigDecimal expectedQuantity = densityDelegate.getInSystemQuantity(fuelEvent);
		pa.setExpectedQuantity(expectedQuantity);
		this.setExpectedAmountInSettlementCurrency(fuelEvent, contract, pa);
		this.setExpectedAmountSystemCurrency(contract, pa, paramSrv, fuelEvent, pa.getExpectedAmountSet(), pa.getExpectedVATSet());
		pa.setInvoicedQuantity(pa.getInvoicedQuantity() != null ? pa.getInvoicedQuantity() : BigDecimal.ZERO);
		pa.setInvoicedAmountSet(pa.getInvoicedAmountSet() != null ? pa.getInvoicedAmountSet() : BigDecimal.ZERO);
		pa.setInvoicedAmountSys(pa.getInvoicedAmountSys() != null ? pa.getInvoicedAmountSys() : BigDecimal.ZERO);
		pa.setInvoicedCurrency(contract.getSettlementCurr());
		pa.setInvoicedVATSet(pa.getInvoicedVATSet() != null ? pa.getInvoicedVATSet() : BigDecimal.ZERO);
		pa.setInvoicedVATSys(pa.getInvoicedVATSys() != null ? pa.getInvoicedVATSys() : BigDecimal.ZERO);
		pa.setExpectedCurrency(contract.getSettlementCurr());
		if (pa.getIsOpen()) {
			pa.setAccrualCurrency(contract.getSettlementCurr());
			pa.setAccrualQuantity(expectedQuantity);
			pa.setAccrualAmountSet(pa.getExpectedAmountSet().subtract(pa.getInvoicedAmountSet()));
			pa.setAccrualAmountSys(pa.getExpectedAmountSys().subtract(pa.getInvoicedAmountSys()));
			pa.setAccrualVATSet(pa.getExpectedVATSet().subtract(pa.getInvoicedVATSet()));
			pa.setAccrualVATSys(pa.getExpectedVATSys().subtract(pa.getInvoicedVATSys()));
			this.setInvStatus(pa);
		}
	}

	private void setInvStatus(PayableAccrual accrual) {
		if (accrual.getInvProcessStatus() == null) {
			accrual.setInvProcessStatus(InvoiceProcessStatus._NOT_INVOICED_);
		}
		if (accrual.getInvProcessStatus().equals(InvoiceProcessStatus._COMPLETELY_INVOICED_) && accrual.getAccrualAmountSet() != null
				&& !accrual.getAccrualAmountSet().equals(BigDecimal.ZERO)) {

			accrual.setInvProcessStatus(InvoiceProcessStatus._PARTIAL_INVOICED_);
		}
	}

	private void setExpectedAmountInSettlementCurrency(FuelEvent fuelEvent, Contract contract, PayableAccrual pa) throws BusinessException {
		CostCalculatorService calcSrv = (CostCalculatorService) this.getApplicationContext().getBean("costCalculator");
		CostVat settlementPrice = calcSrv.calculate(contract, fuelEvent);
		BigDecimal expectedAmount = settlementPrice.getCost();
		BigDecimal setVatPrice = settlementPrice.getVat();
		pa.setExpectedVATSet(setVatPrice);
		pa.setExpectedAmountSet(expectedAmount);
	}

	private void setExpectedAmountSystemCurrency(Contract contract, PayableAccrual pa, ISystemParameterService paramSrv, FuelEvent fuelEvent,
			BigDecimal settlementCost, BigDecimal vat) throws BusinessException {
		ICurrenciesService currService = (ICurrenciesService) this.findEntityService(Currencies.class);
		PriceConverterService priceConverterService = this.getApplicationContext().getBean(PriceConverterService.class);
		String sysCurrCode = paramSrv.getSysCurrency();
		Currencies toCurrency = currService.findByCode(sysCurrCode);
		IFinancialSourcesService finSrcSrv = (IFinancialSourcesService) this.findEntityService(FinancialSources.class);
		Map<String, Object> params = new HashMap<>();
		params.put("isStdFlg", true);
		params.put("active", true);
		FinancialSources stdFinSrc = finSrcSrv.findEntityByAttributes(params);
		IAverageMethodService avgMthSrv = (IAverageMethodService) this.findEntityService(AverageMethod.class);
		AverageMethod stdAvgMth = avgMthSrv.findByName(paramSrv.getSysAverageMethod());
		Density density = this.getBusinessDelegate(DensityDelegate.class).getDensity(fuelEvent);
		ConversionResult sysCost = priceConverterService.convert(this.getSysVol(paramSrv), this.getSysVol(paramSrv), contract.getSettlementCurr(),
				toCurrency, settlementCost, this.getConversionDate(fuelEvent), stdFinSrc, stdAvgMth, density, false,
				MasterAgreementsPeriod._CURRENT_);
		ConversionResult sysVat = priceConverterService.convert(this.getSysVol(paramSrv), this.getSysVol(paramSrv), contract.getSettlementCurr(),
				toCurrency, vat, this.getConversionDate(fuelEvent), stdFinSrc, stdAvgMth, density, false, MasterAgreementsPeriod._CURRENT_);
		pa.setExpectedVATSys(sysVat.getValue());
		pa.setExpectedAmountSys(sysCost.getValue());
		pa.setExchangeRate(sysCost.getFactor());
	}

	private Unit getSysVol(ISystemParameterService paramSrv) throws BusinessException {
		IUnitService unitService = (IUnitService) this.findEntityService(Unit.class);
		String sysVolume = paramSrv.getSysVol();
		return unitService.findByCode(sysVolume);
	}

	private Date getConversionDate(FuelEvent fuelEvent) throws BusinessException {
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		String accrualDateParameter = paramSrv.getAccrualEvaluationDate();
		Date date = new Date();
		if (accrualDateParameter.equalsIgnoreCase(AccrualDateParameter.EVENT_DATE.getValue())) {
			date = fuelEvent.getFuelingDate();
		}
		return date;
	}
}
