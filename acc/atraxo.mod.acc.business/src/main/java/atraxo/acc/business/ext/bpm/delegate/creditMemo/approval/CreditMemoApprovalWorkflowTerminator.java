package atraxo.acc.business.ext.bpm.delegate.creditMemo.approval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author abolindu
 */
public class CreditMemoApprovalWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditMemoApprovalWorkflowTerminator.class);

	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceSrv;

	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {

		List<WorkflowInstanceEntity> entities = new ArrayList<>(workflowInstance.getEntitites());

		if (!CollectionUtils.isEmpty(entities)) {
			for (WorkflowInstanceEntity entity : entities) {
				// set the credit memo status to rejected (if found)
				OutgoingInvoice outInvoiceCreditMemo = null;
				try {
					outInvoiceCreditMemo = this.outgoingInvoiceSrv.findByBusiness(entity.getObjectId());
				} catch (Exception e) {
					LOGGER.warn("WARNING: could not retrieve a credit memo for ID " + entity.getObjectId()
							+ " ! Will not update the entity since it doesn't exist !", e);
				}
				if (outInvoiceCreditMemo != null) {
					this.outgoingInvoiceSrv.reject(Arrays.asList(outInvoiceCreditMemo), WorkflowMsgConstants.WKF_RESULT_REJECTED,
							StringUtils.isEmpty(reason) ? WorkflowMsgConstants.APPROVAL_WORKFLOW_TERMINATED_COMMENT : reason);
				}
			}
		}
	}
}
