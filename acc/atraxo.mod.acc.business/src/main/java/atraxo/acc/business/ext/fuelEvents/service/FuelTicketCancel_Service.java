package atraxo.acc.business.ext.fuelEvents.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IOutgoingInvoiceLineService;
import atraxo.acc.business.api.receivableAccruals.IReceivableAccrualService;
import atraxo.acc.business.ext.creditMemo.CreditMemo_Service;
import atraxo.acc.business.ws.ExportFuelEventOperationEnum;
import atraxo.acc.business.ws.ExportFuelEventPumaRestClient;
import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.ReceivedStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.ops.business.api.ext.fuelTicket.ICancelFuelTicket;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.ext.fuelTicket.CancelFuelTicketResult;
import atraxo.ops.domain.ext.fuelTicket.FuelTicketInvoice;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * Cancel and approved fuel ticket.
 *
 * @author apetho
 */
public class FuelTicketCancel_Service extends AbstractBusinessBaseService implements ICancelFuelTicket {

	private static final Logger LOG = LoggerFactory.getLogger(FuelTicketCancel_Service.class);

	@Autowired
	private IDeliveryNoteService deliveryNoteService;
	@Autowired
	private IOutgoingInvoiceLineService outgoingInvoiceLineService;
	@Autowired
	private IReceivableAccrualService receivableAccrualService;
	@Autowired
	private CreditMemo_Service creditMemoService;
	@Autowired
	private IFuelEventService fuelEventService;
	@Autowired
	private IFuelTicketService fuelTicketService;
	@Autowired
	private ExportFuelEventPumaRestClient exportFuelEventPumaRestClient;
	@Autowired
	private ISystemParameterService systemParameterService;

	@Override
	public CancelFuelTicketResult cancelFuelTickets(List<FuelTicket> fuelTickets) throws BusinessException {
		CancelFuelTicketResult cancelFuelTicketResult = new CancelFuelTicketResult();
		for (FuelTicket fuelTicket : fuelTickets) {
			List<DeliveryNote> deliveryNotes = this.deliveryNoteService.findByObjectIdAndType(fuelTicket.getId(), FuelTicket.class.getSimpleName());
			for (DeliveryNote dn : deliveryNotes) {
				dn.getFuelEvent().setRevocationReason(fuelTicket.getRevocationRason());
				this.processFuelEvents(dn.getFuelEvent(), fuelTicket, cancelFuelTicketResult);
			}
		}
		return cancelFuelTicketResult;
	}

	/**
	 * Process the fuel event when the fuel ticket is cancelled.
	 *
	 * @param dn - {@link DeliveryNote}
	 * @throws BusinessException
	 */
	private void processFuelEvents(FuelEvent fuelEvent, FuelTicket fuelTicket, CancelFuelTicketResult cancelFuelTicketResult)
			throws BusinessException {
		switch (fuelEvent.getInvoiceStatus()) {
		case _AWAITING_APPROVAL_:
			List<String> invoiceNumbers = this.processAwaitingApproval(fuelEvent);
			for (String invoiceNumber : invoiceNumbers) {
				cancelFuelTicketResult.addInvoice(new FuelTicketInvoice(fuelTicket.getTicketNo(), invoiceNumber));
			}
			break;
		case _NOT_INVOICED_:
			this.processNotInvoiced(fuelEvent);
			break;
		case _AWAITING_PAYMENT_:
		case _PAID_:
			this.processPaid(fuelEvent, fuelTicket, cancelFuelTicketResult);
			break;
		case _CREDITED_:
			this.processCredited(fuelEvent);
			break;
		default:
			break;
		}
	}

	/**
	 * Process the fuel event when the fuel ticket is cancelled.
	 *
	 * @param fuelEvent - {@link FuelEvent}
	 * @throws BusinessException
	 */
	private List<String> processAwaitingApproval(FuelEvent fuelEvent) throws BusinessException {
		List<String> outgoingInvoiceNumbers = new ArrayList<>();
		List<OutgoingInvoiceLine> lines = this.outgoingInvoiceLineService.findByFuelEvent(fuelEvent);
		List<Object> invoiceLineDeleteIds = new ArrayList<>();
		for (OutgoingInvoiceLine oil : lines) {
			if (oil.getIsCredited() || oil.getOutgoingInvoice().getInvoiceType().equals(InvoiceTypeAcc._CRN_)) {
				continue;
			}
			outgoingInvoiceNumbers.add(oil.getOutgoingInvoice().getInvoiceNo());
			invoiceLineDeleteIds.add(oil.getId());
		}
		this.outgoingInvoiceLineService.deleteByIds(invoiceLineDeleteIds);

		List<ReceivableAccrual> accruals = this.receivableAccrualService.findByFuelEvent(fuelEvent);
		this.receivableAccrualService.delete(accruals);

		fuelEvent.setStatus(FuelEventStatus._CANCELED_);
		this.exportFuelEventPumaRestClient.export(fuelEvent, ExportFuelEventOperationEnum.CANCEL_FUEL_EVENT, true, false);
		fuelEvent.setRegenerateDetails(false);
		this.fuelEventService.update(fuelEvent);
		return outgoingInvoiceNumbers;
	}

	/**
	 * Process the paid fuel event when the fuel ticket is cancelled.
	 *
	 * @param fuelEvent - {@link FuelEvent}
	 * @throws BusinessException
	 */
	private void processPaid(FuelEvent fuelEvent, FuelTicket fuelTicket, CancelFuelTicketResult cancelFuelTicketResult) throws BusinessException {
		FuelTicket ft = this.fuelTicketService.findByBusiness(fuelTicket.getId());
		List<OutgoingInvoiceLine> lines = this.outgoingInvoiceLineService.findByFuelEvent(fuelEvent);
		for (OutgoingInvoiceLine oil : lines) {
			if (InvoiceTypeAcc._CRN_.equals(oil.getOutgoingInvoice().getInvoiceType()) || oil.getIsCredited()) {
				continue;
			}
			OutgoingInvoice creditMemo = this.creditMemoService.createAndInsertCreditMemo(oil.getOutgoingInvoice(), Arrays.asList(oil),
					ft.getRevocationRason());
			cancelFuelTicketResult.addCreditMemo(new FuelTicketInvoice(fuelTicket.getTicketNo(), creditMemo.getInvoiceNo()));
		}
		List<ReceivableAccrual> accruals = this.receivableAccrualService.findByFuelEvent(fuelEvent);
		this.receivableAccrualService.delete(accruals);

		Boolean isCreditMemoActivalWorkflowActive = this.systemParameterService.getCreditMemoApprovalWorkflow();
		if (!isCreditMemoActivalWorkflowActive) {
			fuelEvent.setInvoiceStatus(OutgoingInvoiceStatus._CREDITED_);
			ft.setInvoiceStatus(OutgoingInvoiceStatus._CREDITED_);
		}
		fuelEvent.setStatus(FuelEventStatus._CANCELED_);
		fuelEvent.setRegenerateDetails(false);
		this.fuelEventService.update(fuelEvent);

		this.fuelTicketService.updateWithoutBL(Arrays.asList(ft));
	}

	/**
	 * Update Fuel Event if Ticket was cancelled, but the Invoices have been already credited.
	 *
	 * @param fuelEvent
	 * @throws BusinessException
	 */
	private void processCredited(FuelEvent fuelEvent) throws BusinessException {
		fuelEvent.setStatus(FuelEventStatus._CANCELED_);
		fuelEvent.setRegenerateDetails(false);
		this.fuelEventService.update(fuelEvent);
	}

	/**
	 * Process the not invoiced fuel event when the fuel ticket is cancelled.
	 *
	 * @param dn - {@link DeliveryNote}
	 * @throws BusinessException
	 */
	private void processNotInvoiced(FuelEvent fuelEvent) throws BusinessException {
		try {
			fuelEvent.setStatus(FuelEventStatus._CANCELED_);
			if (fuelEvent.getReceivedStatus().equals(ReceivedStatus._RELEASED_)) {
				this.exportFuelEventPumaRestClient.export(fuelEvent, ExportFuelEventOperationEnum.CANCEL_FUEL_EVENT, true, false);
			}
			fuelEvent.setRegenerateDetails(false);
			this.fuelEventService.update(fuelEvent);
		} catch (ApplicationException e) {
			LOG.debug("The fuel event is deleted. The update is not neccesary.", e);
		}
	}

}
