package atraxo.acc.business.ext.bpm.delegate.invoice.notifications;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.workflow.IWorkflowInstanceService;
import atraxo.fmbas.business.api.workflow.IWorkflowNotificationService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;

/**
 * @author abolindu
 */
public class InvoiceBulkExportListener implements ExecutionListener {

	private static final long serialVersionUID = 3971162279196246854L;
	@Autowired
	private IWorkflowInstanceService workflowInstanceService;
	@Autowired
	private IWorkflowNotificationService workflowNotificationService;

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		Object wkfInstanceId = execution.getVariable(WorkflowVariablesConstants.VAR_WKF_INSTANCE_ID);
		WorkflowInstance wkfInstance = this.workflowInstanceService.findByBusiness((Integer) wkfInstanceId);

		String message = null;
		if (execution.hasVariable(WorkflowVariablesConstants.ERROR_NOTIFICATION)) {
			message = (String) execution.getVariable(WorkflowVariablesConstants.ERROR_NOTIFICATION);
			wkfInstance.setStatus(WorkflowInstanceStatus._FAILURE_);
		} else if (execution.hasVariable(WorkflowVariablesConstants.TASK_NOTE_VAR)) {
			message = (String) execution.getVariable(WorkflowVariablesConstants.TASK_NOTE_VAR);
			wkfInstance.setStatus(WorkflowInstanceStatus._SUCCESS_);
		}

		wkfInstance.setResult(message);
		wkfInstance.setEntitites(null);
		this.workflowInstanceService.update(wkfInstance);

		// send notification
		this.workflowNotificationService.sendNotification(wkfInstance, wkfInstance.getStatus(), message);
	}

}
