package atraxo.acc.business.ext.invoices.service;

import java.io.OutputStream;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.xml.sax.SAXException;

import atraxo.acc.business.api.ext.invoices.IIataInvoiceService;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceTransmission;

public class IataInvoiceService implements IIataInvoiceService {

	private static final Logger LOG = LoggerFactory.getLogger(IataInvoiceService.class);

	/**
	 * @param invoice
	 * @param out
	 * @throws BusinessException
	 */
	@Override
	public void generateIataXml(InvoiceTransmission invoiceTransmission, OutputStream out) throws BusinessException {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(InvoiceTransmission.class);
			Marshaller marshaller = jaxbContext.createMarshaller();

			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(ClassPathResource.class.getResource("/xsd/iata/IATA_Fuel_Invoice_Standard_3_1_0.xsd"));
			marshaller.setSchema(schema);
			if (LOG.isDebugEnabled()) {
				StringWriter writer = new StringWriter();
				marshaller.marshal(invoiceTransmission, writer);
				LOG.debug(writer.toString());
			}
			marshaller.marshal(invoiceTransmission, out);

		} catch (JAXBException | SAXException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(e.getMessage(), e);
			}
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}
}
