/**
 *
 */
package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.utils.DateUtils;

/**
 * @author vhojda
 */
public abstract class AbstractFuelEventGenerator implements FrequencyFuelEventGenerator {

	/**
	 * @param eventsMap
	 * @param events
	 */
	protected void fillFuelEventsPeriodMap(Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap, List<FuelEvent> events) {
		// add fuel events correspondent to the correct period
		for (Iterator<InvoicingDatePeriod> it = eventsMap.keySet().iterator(); it.hasNext();) {
			InvoicingDatePeriod period = it.next();
			List<FuelEvent> eventsForPeriod = this.getFuelEventsForPeriod(events, period);
			if (eventsForPeriod.isEmpty()) {
				it.remove();
			} else {
				eventsMap.put(period, eventsForPeriod);
			}
		}
	}

	/**
	 * @param events
	 * @param period
	 * @return
	 */
	private List<FuelEvent> getFuelEventsForPeriod(List<FuelEvent> events, InvoicingDatePeriod period) {
		List<FuelEvent> monthlyEvents = new ArrayList<>();
		for (FuelEvent event : events) {
			// if the event fueling date is in the period interval then add it
			if ((DateUtils.compareDates(event.getFuelingDate(), period.getStartPeriod().getTime(), true) >= 0)
					&& (DateUtils.compareDates(event.getFuelingDate(), period.getEndPeriod().getTime(), true) <= 0)) {
				monthlyEvents.add(event);
			}
		}
		return monthlyEvents;
	}

}
