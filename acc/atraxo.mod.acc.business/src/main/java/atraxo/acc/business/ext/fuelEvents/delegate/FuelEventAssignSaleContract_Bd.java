package atraxo.acc.business.ext.fuelEvents.delegate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.ops.domain.ext.fuelQuoteLocation.FlightType;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class FuelEventAssignSaleContract_Bd extends AbstractBusinessDelegate {

	public void assignContract(FuelEvent fe, DeliveryNote dn) throws BusinessException {
		if (dn == null || dn.getSource().equals(QuantitySource._FUEL_ORDER_)) {
			return;
		}
		Contract contract = this.getContract(dn);
		if (contract == null) {
			return;
		}
		if (fe.getBillPriceSourceType() == null || !fe.getBillPriceSourceType().equalsIgnoreCase(FuelOrderLocation.class.getSimpleName())) {
			fe.setBillPriceSourceId(contract.getId());
			fe.setBillPriceSourceType(contract.getClass().getSimpleName());
			fe.setSubsidiaryId(contract.getSubsidiaryId());
		}

	}

	private Contract getContract(DeliveryNote dn) throws BusinessException {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		List<Contract> contracts = contractService.findContractByDeliveryDateCustomerLocation(dn.getCustomer().getId(), dn.getFuelingDate(),
				DealType._SELL_, dn.getDeparture());
		FlightTypeIndicator flightType = dn.getEventType();
		return this.selectContract(contracts, flightType.getName());
	}

	private Contract selectContract(List<Contract> contracts, String flightType) {
		List<Contract> contractList = this.filterContract(contracts, flightType, ContractStatus._EFFECTIVE_);
		if (contractList.size() == 1) {
			return contractList.get(0);
		}
		contractList = this.filterContract(contracts, flightType, ContractStatus._EXPIRED_);
		if (contractList.size() == 1) {
			return contractList.get(0);
		}
		contractList = this.filterContract(contracts, FlightType.UNSPECIFIED.getDisplayName(), ContractStatus._EFFECTIVE_);
		if (contractList.size() == 1) {
			return contractList.get(0);
		}
		contractList = this.filterContract(contracts, FlightType.UNSPECIFIED.getDisplayName(), ContractStatus._EXPIRED_);
		if (contractList.size() == 1) {
			return contractList.get(0);
		}
		return !CollectionUtils.isEmpty(contracts) ? contracts.get(0) : null;
	}

	private List<Contract> filterContract(List<Contract> contracts, String flightType, ContractStatus status) {
		List<Contract> retList = new ArrayList<>();
		for (Contract contract : contracts) {
			if (contract.getStatus().equals(status) && contract.getLimitedTo().getName().equals(flightType)) {
				retList.add(contract);
			}
		}
		return retList;
	}

}
