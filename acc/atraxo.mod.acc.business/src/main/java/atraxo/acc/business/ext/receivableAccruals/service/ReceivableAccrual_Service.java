/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.ext.receivableAccruals.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.acc.business.api.receivableAccruals.IReceivableAccrualService;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link ReceivableAccrual} domain entity.
 */
public class ReceivableAccrual_Service extends atraxo.acc.business.impl.receivableAccruals.ReceivableAccrual_Service
		implements IReceivableAccrualService {

	@Override
	public List<ReceivableAccrual> findAll() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		return this.findEntitiesByAttributes(params);
	}

}
