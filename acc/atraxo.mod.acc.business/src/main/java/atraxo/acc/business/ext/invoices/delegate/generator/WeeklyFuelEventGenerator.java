/**
 *
 */
package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.utils.DateUtils;

/**
 * @author vhojda
 */
public class WeeklyFuelEventGenerator extends AbstractFuelEventGenerator {

	/**
	 *
	 */
	public WeeklyFuelEventGenerator() {
		// The explicit default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#filterByFrequency(java.util.Collection)
	 */
	@Override
	public void filterByFrequency(Collection<FuelEvent> events) {
		Calendar presentCal = DateUtils.getPresentIgnoreHours();

		Calendar comparissonCal = Calendar.getInstance();
		comparissonCal.setTime(presentCal.getTime());

		if (presentCal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
			comparissonCal.add(Calendar.DAY_OF_MONTH, -1);
		} else if (presentCal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
			comparissonCal.add(Calendar.DAY_OF_MONTH, -7);
		} else {
			comparissonCal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		}

		for (Iterator<FuelEvent> it = events.iterator(); it.hasNext();) {
			FuelEvent event = it.next();
			Calendar eventCal = DateUtils.getCalendarFromDateIgnoreHours(event.getFuelingDate());
			if (eventCal.after(comparissonCal)) {
				it.remove();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#splitByFrequency(java.util.List)
	 */
	@Override
	public Map<InvoicingDatePeriod, List<FuelEvent>> splitByFrequency(List<FuelEvent> events) {
		Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap = new HashMap<>();

		FuelEvent firstFuelEvent = events.get(0);
		FuelEvent lastFuelEvent = events.get(events.size() - 1);

		Calendar firstFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(firstFuelEvent.getFuelingDate());
		Calendar lastFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(lastFuelEvent.getFuelingDate());

		Calendar weekCal = DateUtils.getCalendarFromCalendarIgnoreHours(firstFuelEventCal);

		boolean pastLastEndMonthCal = weekCal.after(lastFuelEventCal);
		while (!pastLastEndMonthCal) {
			Calendar oldWeekCal = DateUtils.getCalendarFromCalendarIgnoreHours(weekCal);

			Calendar nextMondayCal = DateUtils.getNextCalendarDayOfTheWeek(weekCal, Calendar.MONDAY);

			weekCal = DateUtils.getCalendarFromCalendarIgnoreHours(nextMondayCal);
			weekCal.add(Calendar.DAY_OF_MONTH, 1);

			pastLastEndMonthCal = DateUtils.compareDates(weekCal, lastFuelEventCal, true) > 0;

			InvoicingDatePeriod period = new InvoicingDatePeriod(oldWeekCal, pastLastEndMonthCal ? lastFuelEventCal : nextMondayCal);
			eventsMap.put(period, null);
		}

		// fill in the events map depending on the period with events
		this.fillFuelEventsPeriodMap(eventsMap, events);

		return eventsMap;
	}

}
