package atraxo.acc.business.ext.invoices.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.acc.business.api.outgoingInvoiceLineDetails.IOutgoingInvoiceLineDetailsService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.fuelEvents.service.CostCalculatorService;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.business.api.vat.IVatService;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.business.utils.DateUtils;
import seava.j4e.iata.formatter.D182BaseFormatter;
import seava.j4e.iata.fuelplus.iata.invoice.Invoice;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceHeader;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceLine;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceSummary;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceTransmission;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceTransmissionHeader;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceTransmissionSmry;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceType;
import seava.j4e.iata.fuelplus.iata.invoice.ItemDeliveryReferenceValue;
import seava.j4e.iata.fuelplus.iata.invoice.ItemQuantity;
import seava.j4e.iata.fuelplus.iata.invoice.ItemReferenceLocalDate;
import seava.j4e.iata.fuelplus.iata.invoice.SubInvoiceHeader;
import seava.j4e.iata.fuelplus.iata.invoice.SubItem;
import seava.j4e.iata.fuelplus.iata.invoice.SubItemProduct;
import seava.j4e.iata.fuelplus.iata.invoice.SubItemProductID;
import seava.j4e.iata.fuelplus.iata.invoice.SubItemQuantity;
import seava.j4e.iata.fuelplus.iata.invoice.SubItemTax;
import seava.j4e.iata.fuelplus.iata.invoice.SubItemTaxQuantity;
import seava.j4e.iata.fuelplus.iata.invoice.SubTax;
import seava.j4e.iata.fuelplus.iata.invoice.SubTaxQuantity;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.BaseBasis;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.CountryCode;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.CurrencyCodeBase;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.InvoiceTransactionType;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.InvoiceTypeBase;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.ItemReferenceCode;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.ItemReferenceDateType;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.PUOMBase;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.PaymentTermsDateBasis;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.ProductIDBase;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.RateTypeBase;
import seava.j4e.iata.fuelplus.iata.invoice.codedirectory.TaxTypeBase;

public class OutgoingInvoiceToXMLTransformer extends AbstractBusinessBaseService {

	private static final String PRICE_CATEGORY = "priceCategory";
	private static final Logger LOG = LoggerFactory.getLogger(OutgoingInvoiceToXMLTransformer.class);
	private static final String QUANTITY_TYPE = "DL";
	private static final String SCHEMA_VERSION = "3.1.0";
	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

	@Autowired
	private IContractService contractSrv;
	@Autowired
	private IVatService vatSrv;
	@Autowired
	private IContractPriceCategoryService contractPriceCatSrv;
	@Autowired
	private IOutgoingInvoiceLineDetailsService invoiceLineDetailSrv;

	/**
	 * @param invoice
	 * @return
	 * @throws BusinessException
	 */
	public InvoiceTransmission generateInvoiceTransmission(OutgoingInvoice invoice, String invoiceIssuer, String invoiceReceiver)
			throws BusinessException {
		InvoiceTransmission transmission = new InvoiceTransmission();

		InvoiceTransmissionHeader transmissionHeader = new InvoiceTransmissionHeader();
		transmissionHeader.setInvoiceTransmissionId(invoice.getRefid());
		transmissionHeader.setInvoiceCreationDate(DateUtils.toXMLGregorianCalendar(invoice.getCreatedAt()));
		transmissionHeader.setVersion(SCHEMA_VERSION);

		Invoice resultInvoice = new Invoice();
		this.generateInvoiceHeader(resultInvoice, invoice, invoiceIssuer, invoiceReceiver);
		BigDecimal[] total = this.generateSubInvoiceHeader(resultInvoice, invoice);
		this.generateInvoiceSummary(resultInvoice, invoice, total);

		transmission.setInvoiceTransmissionHeader(transmissionHeader);
		transmission.getInvoice().add(resultInvoice);

		InvoiceTransmissionSmry transmissionSummary = new InvoiceTransmissionSmry();
		transmissionSummary.setInvoiceMessageCount(1);
		transmission.setInvoiceTransmissionSmry(transmissionSummary);

		return transmission;
	}

	/**
	 * @param resultInvoice
	 * @param invoice
	 * @throws BusinessException
	 */
	private void generateInvoiceHeader(Invoice resultInvoice, OutgoingInvoice invoice, String invoiceIssuer, String invoiceReceiver)
			throws BusinessException {
		InvoiceHeader ih = new InvoiceHeader();
		if (invoice.getReceiver().getIataCode() != null && !invoice.getReceiver().getIataCode().isEmpty()) {
			ih.setCustomerEntityID(invoice.getReceiver().getIataCode());
		} else {
			ih.setCustomerEntityID(invoice.getReceiver().getCode());
		}
		if (invoice.getIssuer().getIataCode() != null && !invoice.getIssuer().getIataCode().isEmpty()) {
			ih.setIssuingEntityID(invoice.getIssuer().getIataCode());
		} else {
			ih.setIssuingEntityID(invoice.getIssuer().getCode());
		}
		ih.setIssuingEntityName(invoice.getIssuer().getName());
		ih.setInvoiceNumber(invoice.getInvoiceNo());
		ih.setInvoiceIssueDate(DateUtils.toXMLGregorianCalendar(invoice.getInvoiceDate()));

		InvoiceType invType = new InvoiceType();
		if (invoice.getInvoiceType().equals(InvoiceTypeAcc._CRN_)) {
			invType.setValue(InvoiceTypeBase.CRN);
		} else {
			invType.setValue(InvoiceTypeBase.INV);
		}
		switch (invoice.getTransactionType()) {
		case _ORIGINAL_:
			invType.setInvoiceTransactionType(InvoiceTransactionType.OR);
			break;
		case _CASH_:
			invType.setInvoiceTransactionType(InvoiceTransactionType.CA);
			break;
		case _CORRECTED_:
			invType.setInvoiceTransactionType(InvoiceTransactionType.CO);
			break;
		case _FINAL_:
			invType.setInvoiceTransactionType(InvoiceTransactionType.FI);
			break;
		case _PREPAID_INVOICE_:
			invType.setInvoiceTransactionType(InvoiceTransactionType.PP);
			break;
		case _PREPAID_INVOICE_CORRECTION_:
			invType.setInvoiceTransactionType(InvoiceTransactionType.PC);
			break;
		case _PROVISIONAL_:
			invType.setInvoiceTransactionType(InvoiceTransactionType.PR);
			break;
		default:
			break;
		}
		ih.setInvoiceType(invType);

		if (StringUtils.isEmpty(invoice.getDeliveryLoc().getIataCode())) {
			throw new BusinessException(AccErrorCode.LOCATION_DOES_NOT_HAVE_IATA_CODE, AccErrorCode.LOCATION_DOES_NOT_HAVE_IATA_CODE.getErrMsg());
		}
		ih.setInvoiceDeliveryLocation(invoice.getDeliveryLoc().getIataCode());
		ih.setInvoiceDeliveryCountryCode(CountryCode.fromValue(invoice.getDeliveryLoc().getCountry().getCode()));

		Contract contract = this.contractSrv.findById(invoice.getReferenceDocId());
		ih.setInvoicePaymentTermsType(contract.getPaymentTerms().toString());
		ih.setInvoicePaymentTermsDateBasis(PaymentTermsDateBasis.fromValue(contract.getPaymentRefDay().getCode()));
		ih.setTaxInvoiceNumber(invoice.getInvoiceNo());
		ih.setInvoiceCurrencyCode(CurrencyCodeBase.fromValue(invoice.getCurrency().getCode()));
		ih.setInvoiceTotalAmount(invoice.getTotalAmount());

		resultInvoice.setInvoiceHeader(ih);
	}

	private BigDecimal[] generateSubInvoiceHeader(Invoice resultInvoice, OutgoingInvoice invoice) throws BusinessException {
		SubInvoiceHeader subInvoiceHeader = new SubInvoiceHeader();
		BigDecimal vatRate = this.vatSrv.getVat(invoice.getInvoiceDate(), invoice.getCountry());
		vatRate = vatRate.divide(new BigDecimal(100), 6, RoundingMode.HALF_UP);

		subInvoiceHeader.setSubInvoiceNumber(invoice.getInvoiceNo());
		subInvoiceHeader.setSubInvoiceIssueDate(DateUtils.toXMLGregorianCalendar(invoice.getInvoiceDate()));
		subInvoiceHeader.setSubInvoiceCurrencyCode(CurrencyCodeBase.fromValue(invoice.getCurrency().getCode()));
		subInvoiceHeader.setSubInvoiceTotalAmount(invoice.getTotalAmount());
		BigDecimal[] total = new BigDecimal[2];
		BigDecimal totalTax = BigDecimal.ZERO;
		BigDecimal totalAmount = BigDecimal.ZERO;
		Arrays.fill(total, BigDecimal.ZERO);
		Integer counter = 1;
		BigDecimal multiplier = invoice.getInvoiceType().equals(InvoiceTypeAcc._CRN_) ? BigDecimal.valueOf(-1) : BigDecimal.valueOf(1);
		for (OutgoingInvoiceLine line : invoice.getInvoiceLines()) {
			if (line.getAmount().compareTo(BigDecimal.ZERO) == 0) {
				continue;
			}
			total = this.addInvoiceLine(line, subInvoiceHeader, vatRate, counter++, multiplier);
			totalTax = totalTax.add(total[0]);
			totalAmount = totalAmount.add(total[1]);
		}
		resultInvoice.getSubInvoiceHeader().add(subInvoiceHeader);
		total[0] = totalTax;
		total[1] = totalAmount;
		return total;
	}

	private BigDecimal[] addInvoiceLine(OutgoingInvoiceLine line, SubInvoiceHeader subInvoiceHeader, BigDecimal vatRate, Integer number,
			BigDecimal multiplier) throws BusinessException {
		InvoiceLine invoiceLine = new InvoiceLine();
		invoiceLine.setItemNumber(number);

		ItemQuantity quantity = new ItemQuantity();
		quantity.setItemQuantityType(QUANTITY_TYPE);
		Contract contract = this.contractSrv.findById(line.getOutgoingInvoice().getReferenceDocId());
		if (contract.getQuantityType().equals(QuantityType._GROSS_VOLUME_)) {
			quantity.setItemQuantityFlag(BaseBasis.GR);
		} else {
			quantity.setItemQuantityFlag(BaseBasis.NT);
		}
		quantity.setItemQuantityQty(line.getQuantity().multiply(multiplier));

		quantity.setItemQuantityUOM(PUOMBase.fromValue(contract.getSettlementUnit().getIataCode()));
		invoiceLine.getItemQuantity().add(quantity);

		ItemDeliveryReferenceValue fNORefValue = new ItemDeliveryReferenceValue();
		fNORefValue.setItemDeliveryReferenceType(ItemReferenceCode.FNO);
		fNORefValue.setValue(line.getFlightNo());
		ItemDeliveryReferenceValue dTNRefValue = new ItemDeliveryReferenceValue();
		dTNRefValue.setItemDeliveryReferenceType(ItemReferenceCode.DTN);
		dTNRefValue.setValue(line.getTicketNo());
		ItemDeliveryReferenceValue aRNRefValue = new ItemDeliveryReferenceValue();
		aRNRefValue.setItemDeliveryReferenceType(ItemReferenceCode.ARN);
		aRNRefValue.setValue(line.getAircraftRegistrationNumber());
		if (!StringUtils.isEmpty(line.getBolNumber())) {
			ItemDeliveryReferenceValue bOLRefValue = new ItemDeliveryReferenceValue();
			bOLRefValue.setItemDeliveryReferenceType(ItemReferenceCode.BOL);
			bOLRefValue.setValue(line.getBolNumber());
			invoiceLine.getItemDeliveryReferenceValue().add(bOLRefValue);
		}
		invoiceLine.getItemDeliveryReferenceValue().add(fNORefValue);
		invoiceLine.getItemDeliveryReferenceValue().add(dTNRefValue);
		invoiceLine.getItemDeliveryReferenceValue().add(aRNRefValue);

		ItemReferenceLocalDate refDate = new ItemReferenceLocalDate();
		refDate.setItemReferenceDateTypes(ItemReferenceDateType.DTA);
		refDate.setValue(DateUtils.toXMLGregorianCalendar(line.getDeliveryDate()));
		invoiceLine.getItemReferenceLocalDate().add(refDate);
		BigDecimal[] total = new BigDecimal[3];
		Arrays.fill(total, BigDecimal.ZERO);
		SubItem subItem = new SubItem();
		List<OutgoingInvoiceLineDetails> list = new ArrayList<>(line.getDetails());

		Collections.sort(list, new Comparator<OutgoingInvoiceLineDetails>() {
			@Override
			public int compare(OutgoingInvoiceLineDetails arg0, OutgoingInvoiceLineDetails arg1) {
				if (Integer.compare(arg0.getPriceCategory().getIata().getUse().getOrder(),
						arg1.getPriceCategory().getIata().getUse().getOrder()) == 0) {
					if (arg0.getPriceCategory().getPricePer().equals(PriceInd._PERCENT_)
							&& arg1.getPriceCategory().getPricePer().equals(PriceInd._PERCENT_)) {
						return 0;
					}
					if (arg0.getPriceCategory().getPricePer().equals(PriceInd._PERCENT_)
							&& !arg1.getPriceCategory().getPricePer().equals(PriceInd._PERCENT_)) {
						return -1;
					}
					if (!arg0.getPriceCategory().getPricePer().equals(PriceInd._PERCENT_)
							&& arg1.getPriceCategory().getPricePer().equals(PriceInd._PERCENT_)) {
						return 1;
					}
				}
				return Integer.compare(arg0.getPriceCategory().getIata().getUse().getOrder(), arg1.getPriceCategory().getIata().getUse().getOrder());

			}
		});
		for (OutgoingInvoiceLineDetails lineDetail : list) {
			if (lineDetail.getCalculateIndicator().equals(CalculateIndicator._CALCULATE_ONLY_)) {
				continue;
			}
			switch (lineDetail.getPriceCategory().getIata().getUse()) {
			case _PRODUCT_:
				SubItemProduct product = this.createSubItemProduct(lineDetail, vatRate, total, multiplier);
				subItem.getSubItemProduct().add(product);
				break;
			case _FEE_:
				subItem.getSubItemProduct().add(this.createSubItemProduct(lineDetail, vatRate, total, multiplier));
				break;
			case _TAX_:
				this.createSubItemTax(lineDetail, subItem.getSubItemProduct(), vatRate, total, multiplier);
				break;
			default:
				break;
			}
		}
		invoiceLine.setItemInvoiceAmount(total[2]);
		total[2] = BigDecimal.ZERO;
		invoiceLine.setSubItem(subItem);
		subInvoiceHeader.getInvoiceLine().add(invoiceLine);
		return total;
	}

	private void createSubItemTax(OutgoingInvoiceLineDetails lineDetail, List<SubItemProduct> products, BigDecimal vatRate, BigDecimal[] total,
			BigDecimal multiplier) throws BusinessException {
		SubItemTax subItemTax = new SubItemTax();
		subItemTax.setSubItemTaxType(TaxTypeBase.fromValue(lineDetail.getPriceCategory().getIata().getCode()));
		subItemTax.setSubItemTaxDescription(lineDetail.getPriceCategory().getIata().getName());
		switch (lineDetail.getPriceCategory().getPricePer()) {
		case _PERCENT_:
			this.processPercentSubItemTax(lineDetail, products, vatRate, total, multiplier);
			return;
		case _EVENT_:
			subItemTax.setSubItemTaxRateType(RateTypeBase.FF);
			break;
		default:
			subItemTax.setSubItemTaxRateType(RateTypeBase.UR);
			break;
		}
		subItemTax.setSubItemTaxPricingCurrencyCode(CurrencyCodeBase.fromValue(lineDetail.getOutgoingInvoice().getCurrency().getCode()));
		subItemTax.setSubItemTaxPricingUOM(PUOMBase.fromValue(lineDetail.getOutgoingInvoice().getUnit().getIataCode()));
		subItemTax.setSubItemTaxPricingUOMFactor(BigDecimal.valueOf(1));
		subItemTax.setSubItemTaxPricingRate(lineDetail.getSettlementPrice());
		subItemTax.setSubItemTaxPricingAmount(lineDetail.getSettlementAmount());
		subItemTax.setSubItemTaxInvoiceUOM(PUOMBase.fromValue(lineDetail.getOutgoingInvoice().getUnit().getIataCode()));
		subItemTax.setSubItemTaxInvoiceUnitRate(lineDetail.getSettlementPrice());

		SubItemTaxQuantity subItemTaxQuantity = new SubItemTaxQuantity();
		subItemTaxQuantity.setSubItemTaxInvoiceQuantity(lineDetail.getOutgoingInvoiceLine().getQuantity().multiply(multiplier));
		subItemTaxQuantity.setSubItemTaxQuantityType(seava.j4e.iata.fuelplus.iata.invoice.codedirectory.QuantityType.IN);
		subItemTax.getSubItemTaxQuantity().add(subItemTaxQuantity);
		subItemTax.setSubItemTaxInvoiceAmount(lineDetail.getSettlementAmount());

		BigDecimal taxAmount = D182BaseFormatter.parseBigDecimal(D182BaseFormatter.printBigDecimal(lineDetail.getSettlementAmount()));
		total[0] = total[0].add(taxAmount);
		total[2] = total[2].add(taxAmount);

		PriceCategory priceCat = lineDetail.getPriceCategory();
		Contract contract = this.contractSrv.findById(lineDetail.getOutgoingInvoiceLine().getOutgoingInvoice().getReferenceDocId());
		Map<String, Object> params = new HashMap<>();
		params.put("contract", contract);
		params.put(PRICE_CATEGORY, priceCat);
		ContractPriceCategory contractPriceCat = this.contractPriceCatSrv.findEntityByAttributes(params);
		for (ContractPriceCategory childPriceCat : contractPriceCat.getChildPriceCategory()) {
			this.createSubTax(lineDetail, subItemTax, contractPriceCat, childPriceCat, taxAmount, total, multiplier);
		}

		subItemTax.setSubItemTaxIndicator("O");
		if (lineDetail.getVatAmount().compareTo(BigDecimal.ZERO) != 0) {
			this.createVATAsSubTax(lineDetail, subItemTax, vatRate, total, multiplier);
		}
		for (SubItemProduct product : products) {
			if ("PRDT".equals(product.getSubItemProductID().getSubItemProductIDQualifier())) {
				product.getSubItemTax().add(subItemTax);
			}
		}

	}

	private void createSubTax(OutgoingInvoiceLineDetails lineDetail, SubItemTax subItemTax, ContractPriceCategory parentPriceCat,
			ContractPriceCategory childPriceCat, BigDecimal taxAmount, BigDecimal[] total, BigDecimal multiplier) throws BusinessException {
		CostCalculatorService costCalcService = new CostCalculatorService();
		BigDecimal percentage = costCalcService.getSettlementPricePerCategory(lineDetail.getOutgoingInvoiceLine().getFuelEvent(), childPriceCat);

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("outgoingInvoiceLine", lineDetail.getOutgoingInvoiceLine());
		parameters.put(PRICE_CATEGORY, childPriceCat.getPriceCategory());
		OutgoingInvoiceLineDetails childLineDetail = this.invoiceLineDetailSrv.findEntityByAttributes(parameters);

		SubTax subTax = new SubTax();
		subTax.setSubTaxType(TaxTypeBase.fromValue(childPriceCat.getPriceCategory().getIata().getCode()));
		subTax.setSubTaxDescription(childPriceCat.getPriceCategory().getIata().getName());
		subTax.setSubTaxRateType(RateTypeBase.P);

		BigDecimal percentageTaxValue = D182BaseFormatter.parseBigDecimal(
				D182BaseFormatter.printBigDecimal(taxAmount.multiply(percentage.divide(ONE_HUNDRED, MathContext.DECIMAL64), MathContext.DECIMAL64)));
		subTax.setSubTaxPricingCurrencyCode(CurrencyCodeBase.fromValue(lineDetail.getOutgoingInvoice().getCurrency().getCode()));
		subTax.setSubTaxPricingUOM(PUOMBase.PCT);
		subTax.setSubTaxPricingUOMFactor(BigDecimal.valueOf(1));
		subTax.setSubTaxPricingRate(childLineDetail.getSettlementPrice());
		subTax.setSubTaxPricingAmount(percentageTaxValue);
		subTax.setSubTaxInvoiceUOM(PUOMBase.fromValue(lineDetail.getOutgoingInvoice().getUnit().getIataCode()));
		subTax.setSubTaxInvoiceUnitRate(childLineDetail.getSettlementPrice());

		SubTaxQuantity subTaxQuantity = new SubTaxQuantity();
		subTaxQuantity.setSubTaxInvoiceQuantity(lineDetail.getOutgoingInvoiceLine().getQuantity().multiply(multiplier));
		subTaxQuantity.setSubTaxQuantityType(seava.j4e.iata.fuelplus.iata.invoice.codedirectory.QuantityType.IN);
		subTax.getSubTaxQuantity().add(subTaxQuantity);
		subTax.setSubTaxInvoiceAmount(percentageTaxValue);

		BigDecimal subTaxAmount = D182BaseFormatter.parseBigDecimal(D182BaseFormatter.printBigDecimal(percentageTaxValue));
		total[0] = total[0].add(subTaxAmount);
		total[2] = total[2].add(subTaxAmount);

		subItemTax.getSubTax().add(subTax);

	}

	private void processPercentSubItemTax(OutgoingInvoiceLineDetails lineDetail, List<SubItemProduct> products, BigDecimal vatRate,
			BigDecimal[] total, BigDecimal multiplier) throws BusinessException {
		PriceCategory priceCat = lineDetail.getPriceCategory();
		Contract contract = this.contractSrv.findById(lineDetail.getOutgoingInvoiceLine().getOutgoingInvoice().getReferenceDocId());
		Map<String, Object> params = new HashMap<>();
		params.put("contract", contract);
		params.put(PRICE_CATEGORY, priceCat);
		ContractPriceCategory contractPriceCat = this.contractPriceCatSrv.findEntityByAttributes(params);
		CostCalculatorService costCalcService = new CostCalculatorService();
		BigDecimal percentage = costCalcService.getSettlementPricePerCategory(lineDetail.getOutgoingInvoiceLine().getFuelEvent(), contractPriceCat);
		for (ContractPriceCategory parentPriceCat : contractPriceCat.getParentPriceCategory()) {
			for (SubItemProduct product : products) {
				try {
					if (product.getSubItemProductID().getValue()
							.equals(ProductIDBase.fromValue(parentPriceCat.getPriceCategory().getIata().getCode()))) {
						SubItemTax subItemTax = new SubItemTax();
						subItemTax.setSubItemTaxType(TaxTypeBase.fromValue(lineDetail.getPriceCategory().getIata().getCode()));
						subItemTax.setSubItemTaxDescription(lineDetail.getPriceCategory().getIata().getName());
						subItemTax.setSubItemTaxRateType(RateTypeBase.P);
						BigDecimal parentPriceCatCost = costCalcService
								.getSettlementPricePerCategory(lineDetail.getOutgoingInvoiceLine().getFuelEvent(), parentPriceCat);
						if (parentPriceCat.getPriceCategory().getPricePer().equals(PriceInd._VOLUME_)) {
							parentPriceCatCost = parentPriceCatCost.multiply(lineDetail.getOutgoingInvoiceLine().getQuantity());
						}
						if (lineDetail.getOutgoingInvoice().getInvoiceType().equals(InvoiceTypeAcc._CRN_)) {
							parentPriceCatCost = parentPriceCatCost.multiply(BigDecimal.valueOf(-1));
						}
						BigDecimal percentageTaxValue = D182BaseFormatter.parseBigDecimal(D182BaseFormatter.printBigDecimal(
								parentPriceCatCost.multiply(percentage.divide(ONE_HUNDRED, MathContext.DECIMAL64), MathContext.DECIMAL64)));
						subItemTax.setSubItemTaxPricingCurrencyCode(
								CurrencyCodeBase.fromValue(lineDetail.getOutgoingInvoice().getCurrency().getCode()));
						subItemTax.setSubItemTaxPricingUOM(PUOMBase.PCT);
						subItemTax.setSubItemTaxPricingUOMFactor(BigDecimal.valueOf(1));
						subItemTax.setSubItemTaxPricingRate(lineDetail.getSettlementPrice());
						subItemTax.setSubItemTaxPricingAmount(percentageTaxValue);
						subItemTax.setSubItemTaxInvoiceUOM(PUOMBase.fromValue(lineDetail.getOutgoingInvoice().getUnit().getIataCode()));
						subItemTax.setSubItemTaxInvoiceUnitRate(lineDetail.getSettlementPrice());

						SubItemTaxQuantity subItemTaxQuantity = new SubItemTaxQuantity();
						subItemTaxQuantity.setSubItemTaxInvoiceQuantity(lineDetail.getOutgoingInvoiceLine().getQuantity().multiply(multiplier));
						subItemTaxQuantity.setSubItemTaxQuantityType(seava.j4e.iata.fuelplus.iata.invoice.codedirectory.QuantityType.IN);
						subItemTax.getSubItemTaxQuantity().add(subItemTaxQuantity);
						subItemTax.setSubItemTaxInvoiceAmount(percentageTaxValue);

						BigDecimal taxAmount = D182BaseFormatter.parseBigDecimal(D182BaseFormatter.printBigDecimal(percentageTaxValue));
						total[0] = total[0].add(taxAmount);
						total[2] = total[2].add(taxAmount);

						for (ContractPriceCategory childPriceCat : contractPriceCat.getChildPriceCategory()) {
							this.createSubTax(lineDetail, subItemTax, contractPriceCat, childPriceCat, taxAmount, total, multiplier);
						}

						subItemTax.setSubItemTaxIndicator("O");
						if (lineDetail.getVatAmount().compareTo(BigDecimal.ZERO) != 0) {
							this.createVATAsSubTax(lineDetail, subItemTax, vatRate, total, taxAmount, multiplier);
						}
						product.getSubItemTax().add(subItemTax);
						break;
					}
				} catch (IllegalArgumentException e) {
					if (LOG.isDebugEnabled()) {
						LOG.debug(e.getMessage(), e);
					}
					continue;
				}
			}
		}
	}

	private SubItemProduct createSubItemProduct(OutgoingInvoiceLineDetails lineDetail, BigDecimal vatRate, BigDecimal[] total, BigDecimal multiplier)
			throws BusinessException {
		SubItemProduct product = new SubItemProduct();
		SubItemProductID productID = new SubItemProductID();
		try {
			if (lineDetail.getPriceCategory().getType().equals(PriceType._PRODUCT_)) {
				productID.setValue(ProductIDBase.fromValue(lineDetail.getOutgoingInvoice().getProduct().getIataName()));
			} else {
				productID.setValue(ProductIDBase.fromValue(lineDetail.getPriceCategory().getIata().getCode()));
			}
		} catch (IllegalArgumentException e) {
			throw new BusinessException(AccErrorCode.IATA_INVOICE_WRONG_PRODUCT_OR_FEE_CODE,
					String.format(AccErrorCode.IATA_INVOICE_WRONG_PRODUCT_OR_FEE_CODE.getErrMsg(), lineDetail.getPriceCategory().getIata().getCode()),
					e);
		}
		switch (lineDetail.getPriceCategory().getIata().getUse()) {
		case _FEE_:
			productID.setSubItemProductIDQualifier("FEE");
			break;
		case _PRODUCT_:
			productID.setSubItemProductIDQualifier("PRDT");
			break;
		case _TAX_:
		default:
			productID.setSubItemProductIDQualifier("OTHR");
		}
		product.setSubItemProductID(productID);
		if (lineDetail.getPriceCategory().getType().equals(PriceType._PRODUCT_)) {
			product.setSubItemDescription(lineDetail.getOutgoingInvoice().getProduct().getName());
		} else {
			product.setSubItemDescription(lineDetail.getPriceCategory().getName());
		}
		switch (lineDetail.getPriceCategory().getPricePer()) {
		case _EVENT_:
		case _PERCENT_:
			product.setSubItemPricingUnitRateType("FF");
			break;
		default:
			product.setSubItemPricingUnitRateType("UR");
		}
		product.setSubItemPricingUnitRate(lineDetail.getSettlementPrice());
		product.setSubItemPricingUOM(PUOMBase.fromValue(lineDetail.getOutgoingInvoice().getUnit().getIataCode()));
		product.setSubItemPricingUOMFactor(BigDecimal.valueOf(1));
		product.setSubItemPricingCurrencyCode(CurrencyCodeBase.fromValue(lineDetail.getOutgoingInvoice().getCurrency().getCode()));
		product.setSubItemPricingAmount(lineDetail.getSettlementAmount());
		product.setSubItemInvoiceUOM(PUOMBase.fromValue(lineDetail.getOutgoingInvoice().getUnit().getIataCode()));

		BigDecimal productAmount = D182BaseFormatter.parseBigDecimal(D182BaseFormatter.printBigDecimal(lineDetail.getSettlementAmount()));
		total[1] = total[1].add(productAmount);
		total[2] = total[2].add(productAmount);

		SubItemQuantity subItemQuantity = new SubItemQuantity();
		subItemQuantity.setSubItemInvoiceQuantity(lineDetail.getOutgoingInvoiceLine().getQuantity().multiply(multiplier));
		subItemQuantity.setSubItemQuantityType(seava.j4e.iata.fuelplus.iata.invoice.codedirectory.QuantityType.IN);
		subItemQuantity.setSubItemQuantityFlag(BaseBasis.GR);
		product.getSubItemQuantity().add(subItemQuantity);

		product.setSubItemInvoiceUnitRate(lineDetail.getSettlementPrice());
		product.setSubItemInvoiceAmount(lineDetail.getSettlementAmount());
		if (lineDetail.getVatAmount().compareTo(BigDecimal.ZERO) != 0) {
			SubItemTax subItemTax = this.buildVatAsSubItemTax(lineDetail, vatRate, total, multiplier);
			product.getSubItemTax().add(subItemTax);
		}

		return product;
	}

	private void createVATAsSubTax(OutgoingInvoiceLineDetails lineDetail, SubItemTax subItemTax, BigDecimal vatRate, BigDecimal[] total,
			BigDecimal multiplier) {
		this.createVATAsSubTax(lineDetail, subItemTax, vatRate, total, null, multiplier);
	}

	private void createVATAsSubTax(OutgoingInvoiceLineDetails lineDetail, SubItemTax subItemTax, BigDecimal vatRate, BigDecimal[] total,
			BigDecimal taxableAmount, BigDecimal multiplier) {
		SubTax subTax = new SubTax();

		BigDecimal vatAmount;
		if (taxableAmount != null) {
			vatAmount = D182BaseFormatter.parseBigDecimal(D182BaseFormatter.printBigDecimal(taxableAmount.multiply(vatRate, MathContext.DECIMAL64)));
		} else {
			vatAmount = lineDetail.getVatAmount();
		}
		subTax.setSubTaxType(TaxTypeBase.VAT);
		subTax.setSubTaxDescription("Standard rated VAT");
		subTax.setSubTaxRateType(RateTypeBase.P);
		subTax.setSubTaxPricingCurrencyCode(CurrencyCodeBase.fromValue(lineDetail.getOutgoingInvoice().getCurrency().getCode()));
		subTax.setSubTaxPricingUOM(PUOMBase.PCT);
		subTax.setSubTaxPricingUOMFactor(BigDecimal.valueOf(1));
		subTax.setSubTaxPricingRate(vatRate.multiply(BigDecimal.valueOf(100)));
		subTax.setSubTaxPricingAmount(vatAmount);
		subTax.setSubTaxInvoiceUOM(PUOMBase.fromValue(lineDetail.getOutgoingInvoice().getUnit().getIataCode()));
		subTax.setSubTaxInvoiceUnitRate(vatRate);

		SubTaxQuantity subTaxQuantity = new SubTaxQuantity();
		subTaxQuantity.setSubTaxInvoiceQuantity(lineDetail.getOutgoingInvoiceLine().getQuantity().multiply(multiplier));
		subTaxQuantity.setSubTaxQuantityType(seava.j4e.iata.fuelplus.iata.invoice.codedirectory.QuantityType.IN);
		subTax.getSubTaxQuantity().add(subTaxQuantity);
		subTax.setSubTaxInvoiceAmount(vatAmount);

		BigDecimal taxAmount = D182BaseFormatter.parseBigDecimal(D182BaseFormatter.printBigDecimal(vatAmount));
		total[0] = total[0].add(taxAmount);
		total[2] = total[2].add(taxAmount);

		subItemTax.getSubTax().add(subTax);

	}

	private SubItemTax buildVatAsSubItemTax(OutgoingInvoiceLineDetails lineDetail, BigDecimal vatRate, BigDecimal[] taxTotal, BigDecimal multiplier) {
		SubItemTax subItemTax = new SubItemTax();
		subItemTax.setSubItemTaxType(TaxTypeBase.VAT);
		subItemTax.setSubItemTaxDescription("Standard rated VAT");
		subItemTax.setSubItemTaxRateType(RateTypeBase.P);
		subItemTax.setSubItemTaxPricingCurrencyCode(CurrencyCodeBase.fromValue(lineDetail.getOutgoingInvoice().getCurrency().getCode()));
		subItemTax.setSubItemTaxPricingUOM(PUOMBase.PCT);
		subItemTax.setSubItemTaxPricingUOMFactor(BigDecimal.valueOf(1));
		subItemTax.setSubItemTaxPricingRate(vatRate.multiply(BigDecimal.valueOf(100)));
		subItemTax.setSubItemTaxPricingAmount(lineDetail.getVatAmount());
		subItemTax.setSubItemTaxInvoiceUOM(PUOMBase.fromValue(lineDetail.getOutgoingInvoice().getUnit().getIataCode()));
		subItemTax.setSubItemTaxInvoiceUnitRate(vatRate);

		SubItemTaxQuantity subItemTaxQuantity = new SubItemTaxQuantity();
		subItemTaxQuantity.setSubItemTaxInvoiceQuantity(lineDetail.getOutgoingInvoiceLine().getQuantity().multiply(multiplier));
		subItemTaxQuantity.setSubItemTaxQuantityType(seava.j4e.iata.fuelplus.iata.invoice.codedirectory.QuantityType.IN);
		subItemTax.getSubItemTaxQuantity().add(subItemTaxQuantity);
		subItemTax.setSubItemTaxInvoiceAmount(lineDetail.getVatAmount());

		BigDecimal taxAmount = D182BaseFormatter.parseBigDecimal(D182BaseFormatter.printBigDecimal(lineDetail.getVatAmount()));
		taxTotal[0] = taxTotal[0].add(taxAmount);
		taxTotal[2] = taxTotal[2].add(taxAmount);

		subItemTax.setSubItemTaxIndicator("O");
		return subItemTax;
	}

	private void generateInvoiceSummary(Invoice resultInvoice, OutgoingInvoice invoice, BigDecimal[] total) {
		InvoiceSummary summary = new InvoiceSummary();
		// summary.setInvoiceLineCount(BigInteger.valueOf(invoice.getInvoiceLines().size()));
		summary.setInvoiceLineCount(BigInteger.valueOf(resultInvoice.getSubInvoiceHeader().iterator().next().getInvoiceLine().size()));
		summary.setTotalInvoiceLineAmount(total[1].add(total[0]));
		summary.setTotalInvoiceTaxAmount(total[0]);
		resultInvoice.getInvoiceHeader().setInvoiceTotalAmount(total[1].add(total[0]));
		if (!CollectionUtils.isEmpty(resultInvoice.getSubInvoiceHeader())) {
			resultInvoice.getSubInvoiceHeader().get(0).setSubInvoiceTotalAmount(total[1].add(total[0]));
		}
		resultInvoice.setInvoiceSummary(summary);
	}

}
