package atraxo.acc.business.ext.payableAccruals.delegate;

import java.math.BigDecimal;

/**
 * @author apetho
 */
public class PayableCostVat {

	private BigDecimal sysCost;
	private BigDecimal setCost;
	private BigDecimal sysVat;
	private BigDecimal setVat;
	private BigDecimal factor;

	/**
	 * @param sysCost
	 * @param setCost
	 * @param sysVat
	 * @param setVat
	 * @param factor
	 */
	public PayableCostVat(BigDecimal sysCost, BigDecimal setCost, BigDecimal sysVat, BigDecimal setVat, BigDecimal factor) {
		super();
		this.sysCost = sysCost;
		this.setCost = setCost;
		this.sysVat = sysVat;
		this.setVat = setVat;
		this.factor = factor;
	}

	public BigDecimal getSysCost() {
		return this.sysCost;
	}

	public void setSysCost(BigDecimal sysCost) {
		this.sysCost = sysCost;
	}

	public BigDecimal getSetCost() {
		return this.setCost;
	}

	public void setSetCost(BigDecimal setCost) {
		this.setCost = setCost;
	}

	public BigDecimal getSysVat() {
		return this.sysVat;
	}

	public void setSysVat(BigDecimal sysVat) {
		this.sysVat = sysVat;
	}

	public BigDecimal getSetVat() {
		return this.setVat;
	}

	public void setSetVat(BigDecimal setVat) {
		this.setVat = setVat;
	}

	public BigDecimal getFactor() {
		return this.factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

}
