/**
 *
 */
package atraxo.acc.business.ext.bpm.delegate.invoice.notifications;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.business.api.attachment.IAttachmentTypeService;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.storage.StorageService;

/**
 * @author vhojda
 */
public class CheckInvoicesDelegate extends AbstractInvoiceNotificationDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(CheckInvoicesDelegate.class);

	private static final String NAME = "report";

	@Autowired
	IAttachmentService attachmentService;
	@Autowired
	IAttachmentTypeService attachmentTypeService;
	@Autowired
	private StorageService storageService;

	@Override
	@SuppressWarnings("unchecked")
	public void executeDelegate() throws BusinessException {

		List<Attachment> reports = new ArrayList<>();
		List<Attachment> tobeDeleted = new ArrayList<>();
		List<OutgoingInvoice> invoices = (List<OutgoingInvoice>) this.execution.getVariable(WorkflowVariablesConstants.MAP_NOTIFICATIONS);

		Iterator<OutgoingInvoice> iterator = invoices.iterator();
		while (iterator.hasNext()) {
			OutgoingInvoice invoice = iterator.next();
			Attachment report = this.getInvoiceReport(invoice);
			if (report != null) {
				if (this.storageService.existsResource(report.getRefid())) {
					reports.add(report);
					iterator.remove();
				} else {
					tobeDeleted.add(report);
				}

			}
		}
		this.attachmentService.delete(tobeDeleted);

		this.execution.setVariable(WorkflowVariablesConstants.SKIP_TO_ZIP, invoices.isEmpty());

		if (!reports.isEmpty()) {
			this.execution.setVariable(WorkflowVariablesConstants.MAP_REPORTS, reports);
		}

	}

	/**
	 * @param invoice
	 * @param reports
	 * @return
	 */
	private Attachment getInvoiceReport(OutgoingInvoice invoice) {
		AttachmentType type = this.attachmentTypeService.findByName(NAME);
		this.execution.setVariable(WorkflowVariablesConstants.ATTACHMENT_TYPE, type);

		Map<String, Object> params = new HashMap<>();
		params.put("targetRefid", invoice.getId());
		params.put("type", type);

		Attachment attachment = null;
		try {
			attachment = this.attachmentService.findEntityByAttributes(params);
		} catch (Exception e) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(e.getMessage(), e);
			}
		}
		return attachment;
	}

	@Override
	public void afterExecuteDelegate() {
		// do nothing
	}

	@Override
	public String getDelegateErrorCode() {
		return "";
	}
}
