package atraxo.acc.business.ext.exceptions;

import java.util.List;

import seava.j4e.api.exceptions.BusinessException;

public class FinishInputException extends BusinessException {

	private static final long serialVersionUID = 4946773327506854982L;
	private final List<BusinessException> exceptions;

	public FinishInputException(List<BusinessException> exceptions) {
		super(AccErrorCode.INVOICE_FINISH_INPUT, "");
		this.setWithIcon(false);
		this.exceptions = exceptions;
	}

	public List<BusinessException> getExceptions() {
		return this.exceptions;
	}

	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("<p style='margin-top:0px; margin-bottom:20px'>" + AccErrorCode.INVOICE_CHECK_FAILED_SUBTITLE.getErrMsg() + "</p>");
		sb.append("<ul style='margin-left:-10px'>");
		for (BusinessException exception : this.exceptions) {
			sb.append("<li style='padding-bottom:1px'>").append(exception.getMessage()).append("</li>");
		}
		sb.append("</ul>");
		sb.append("<p style='margin-top:20px'>" + AccErrorCode.INVOICE_CHECK_FOOTER.getErrMsg() + "</p>");
		return sb.toString();
	}

}
