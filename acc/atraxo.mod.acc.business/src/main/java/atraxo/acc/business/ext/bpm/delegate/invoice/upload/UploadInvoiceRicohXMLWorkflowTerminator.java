package atraxo.acc.business.ext.bpm.delegate.invoice.upload;

import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author vhojda
 */
public class UploadInvoiceRicohXMLWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {
		// this workflow has nothing to do on terminate
	}
}
