package atraxo.acc.business.ext.invoices.job.upload;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.util.StringUtils;

import seava.j4e.scheduler.batch.listener.DefaultActionListener;

/**
 *
 * @author zspeter
 *
 */
public class FTPUploadFilesListener extends DefaultActionListener {

	private static final String LOG_FILE_NAME_AND_MESSAGE = "logFileNameAndMessage";

	@Override
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);

		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder();
		List<StepExecution> stepExecution = (List<StepExecution>) jobExecution.getStepExecutions();

		for (StepExecution se : stepExecution) {
			ExecutionContext executionContext = se.getExecutionContext();
			if (executionContext.containsKey("successfullyTransfered") || executionContext.containsKey("unsuccessfullyTransfered")) {
				Integer success = (Integer) executionContext.get("successfullyTransfered");
				Integer failed = (Integer) executionContext.get("unsuccessfullyTransfered");
				String message = (String) executionContext.get(LOG_FILE_NAME_AND_MESSAGE);
				if (success != 0) {
					sb.append("SUCCESSFULLY UPLOADED: ").append(success);
				} else {
					sb.append("SUCCESSFULLY UPLOADED: 0");
				}
				if (failed != 0) {
					sb.append(" ; FAILED TO UPLOAD: ").append(failed).append("| \n");
				} else {
					sb.append(" ; FAILED TO UPLOAD: ").append("0 | \n");
				}
				if (!StringUtils.isEmpty(message)) {
					sb.append("Detailed info about each invoice: \n");
					sb.append(message);
				}
			} else if (executionContext.containsKey(LOG_FILE_NAME_AND_MESSAGE)) {
				sb.append((String) executionContext.get(LOG_FILE_NAME_AND_MESSAGE));
			}
		}
		status = new ExitStatus(status.getExitCode(), StringUtils.isEmpty(sb.toString()) ? status.getExitDescription() : sb.toString());
		jobExecution.setExitStatus(status);
	}

}
