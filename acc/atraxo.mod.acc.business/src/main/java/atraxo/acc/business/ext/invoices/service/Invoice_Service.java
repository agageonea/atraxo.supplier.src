/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.ext.invoices.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.joda.time.DateTimeComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IInvoiceLineService;
import atraxo.acc.business.api.invoices.IInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.exceptions.FinishInputException;
import atraxo.acc.business.ext.exceptions.InvalidInvoiceContractException;
import atraxo.acc.business.ext.fuelEvents.service.FuelEventManagerService;
import atraxo.acc.business.ext.invoices.delegate.ApproveIncommingInvoice_Bd;
import atraxo.acc.domain.ext.invoiceLines.InvLine;
import atraxo.acc.domain.ext.invoiceLines.InvoiceLineTolerance;
import atraxo.acc.domain.ext.invoices.InvoiceCategory;
import atraxo.acc.domain.impl.acc_type.CheckResult;
import atraxo.acc.domain.impl.acc_type.CheckStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.business.api.contracts.IContractInvoiceService;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.contracts.messageobject.MessageContractObject;
import atraxo.cmm.domain.ext.invoice.InvoiceStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.tolerance.IToleranceService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.business.ext.tolerance.ToleranceResult;
import atraxo.fmbas.business.ext.tolerance.ToleranceVerifier;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceCostToAccrual;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link Invoice} domain entity.
 *
 * @author apetho
 */
public class Invoice_Service extends atraxo.acc.business.impl.invoices.Invoice_Service implements IInvoiceService {

	private static final Logger LOG = LoggerFactory.getLogger(Invoice_Service.class);

	@Autowired
	private FuelEventManagerService feManagerServ;
	@Autowired
	private IFuelEventService feServ;
	@Autowired
	private UnitConverterService unitConverterService;
	@Autowired
	private IDeliveryNoteService deliveryNoteService;
	@Autowired
	private IContractInvoiceService ciService;
	@Autowired
	private IInvoiceLineService invoiceLineService;
	@Autowired
	private IContractService cService;
	@Autowired
	private ISystemParameterService sysParamService;
	@Autowired
	private IToleranceService toleranceService;
	@Autowired
	private IChangeHistoryService historyService;

	@Override
	protected void preInsert(Invoice e) throws BusinessException {
		super.preInsert(e);
		this.checkPeriods(e);
		e.setCountry(e.getDeliveryLoc().getCountry());
		e.setReceivingDate(GregorianCalendar.getInstance().getTime());
		this.checkInvoiceBusinessKey(e);
		this.calculateDueDay(e);
		try {
			this.setContract(e);
		} catch (BusinessException | RuntimeException be) {
			LOG.warn("Set contract to invoice " + e.getInvoiceNo() + " ", be);
		}
		this.setSubsidiary(e);
		this.checkValidity(e);
	}

	private void setSubsidiary(Invoice e) {
		if (e.getContract() != null) {
			e.setSubsidiaryId(e.getContract().getSubsidiaryId());
		}
	}

	@Override
	@Transactional
	@History(value = "Insert", type = Invoice.class)
	public void insert(List<Invoice> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	protected void postInsert(Invoice e) throws BusinessException {
		super.postInsert(e);
		if (e.getContract() != null) {
			this.ciService.insertUpdateLink(e.getContract(), e.getId(), e.getInvoiceNo(), e.getStatus().getName(), e.getDeliveryDateFrom(),
					e.getDeliverydateTo(), DealType._BUY_);
		}
	}

	@Override
	protected void preUpdate(Invoice e) throws BusinessException {
		super.preUpdate(e);
		this.checkPeriods(e);
		e.setCountry(e.getDeliveryLoc().getCountry());
		this.checkInvoiceBusinessKey(e);
		this.calculateDueDay(e);
		this.setSubsidiary(e);
		this.checkValidity(e);
	}

	@Override
	protected void postUpdate(Invoice e) throws BusinessException {
		super.postUpdate(e);
		if (e.getContract() != null) {
			this.ciService.insertUpdateLink(e.getContract(), e.getId(), e.getInvoiceNo(), e.getStatus().getName(), e.getDeliveryDateFrom(),
					e.getDeliverydateTo(), DealType._BUY_);
		}
	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		for (Object id : ids) {
			this.ciService.removeLink((Integer) id, DealType._BUY_);
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		List<Object> invoiceLineIds = new ArrayList<>();
		for (Object obj : ids) {
			Invoice invoice = this.findById(obj);
			Map<String, Object> params = new HashMap<>();
			params.put("purchaseInvoiceNo", invoice.getInvoiceNo());
			List<FuelEvent> feList = this.feServ.findEntitiesByAttributes(params);
			List<FuelEvent> feUpdatedList = new ArrayList<>();
			for (FuelEvent fe : feList) {
				if (OutgoingInvoiceStatus._AWAITING_PAYMENT_.equals(fe.getInvoiceStatus())
						|| OutgoingInvoiceStatus._PAID_.equals(fe.getInvoiceStatus())) {
					throw new BusinessException(AccErrorCode.INV_LINE_DEL_DELIVERY_NOTE_USED,
							AccErrorCode.INV_LINE_DEL_DELIVERY_NOTE_USED.getErrMsg());
				} else {
					fe.setPurchaseInvoiceNo("");
					feUpdatedList.add(fe);
				}
			}
			this.feServ.update(feUpdatedList);
			for (InvoiceLine il : invoice.getInvoiceLines()) {
				invoiceLineIds.add(il.getId());
			}
		}
		this.invoiceLineService.deleteByIds(invoiceLineIds);
	}

	private void checkValidity(Invoice invoice) throws BusinessException {
		if (invoice.getIssueDate() != null && invoice.getDeliverydateTo() != null
				&& DateTimeComparator.getDateOnlyInstance().compare(invoice.getIssueDate(), invoice.getDeliverydateTo()) < 0) {
			throw new BusinessException(AccErrorCode.INVOICE_DATE_GRATER_THAN_DATE_TO, AccErrorCode.INVOICE_DATE_GRATER_THAN_DATE_TO.getErrMsg());
		}
	}

	/**
	 * If there is only one contract this one is automatically pre-selected
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void setContract(Invoice e) throws BusinessException {
		if (e.getContract() != null) {
			return;
		}
		InvoiceCategory invoiceCategory = InvoiceCategory.getByName(e.getCategory().getName());
		List<Contract> contracts = this.cService.findBySupplierStatusLocationScopeType(e.getIssuer(), ContractStatus._EFFECTIVE_, e.getDeliveryLoc(),
				ContractScope.getByName(invoiceCategory.getScope()), ContractType.getByName(invoiceCategory.getType()));
		if (contracts.size() == 1) {
			e.setContract(contracts.get(0));
		}
	}

	private void checkPeriods(Invoice e) throws BusinessException {
		if (e.getDeliveryDateFrom().after(e.getDeliverydateTo())) {
			throw new BusinessException(AccErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					AccErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
	}

	private void checkInvoiceBusinessKey(Invoice invoice) throws BusinessException {
		try {
			Invoice i = this.findByKey(invoice.getIssuer(), invoice.getIssueDate(), invoice.getInvoiceNo());
			if ((invoice.getId() == null) || (!invoice.getId().equals(i.getId()))) {
				throw new BusinessException(AccErrorCode.DUPLICATE_ENTITY,
						String.format(AccErrorCode.DUPLICATE_ENTITY.getErrMsg(), Invoice.class.getSimpleName(), "Issuer-Invoice date-Invoice #"));
			}
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOG.warn("Invoice check business key ", ex);
			} else if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(ex.getErrorCode())) {
				throw new BusinessException(AccErrorCode.INCOMING_INVOICE_DUPLICATE_BUSINESS_KEY,
						String.format(AccErrorCode.INCOMING_INVOICE_DUPLICATE_BUSINESS_KEY.getErrMsg(), invoice.getInvoiceNo(),
								invoice.getIssuer().getCode(), invoice.getIssueDate()),
						ex);
			}
		}
	}

	private void calculateDueDay(Invoice ds) {
		Contract contract = ds.getContract();

		if (contract == null) {
			return;
		}
		PaymentDay referenceTo = contract.getPaymentRefDay();
		Date deliveryDate = null;
		if (referenceTo.equals(PaymentDay._INVOICE_DATE_)) {
			deliveryDate = ds.getIssueDate();
		}
		if (referenceTo.equals(PaymentDay._INVOICE_RECEIVING_DATE_)) {
			deliveryDate = ds.getReceivingDate();
		}
		if (referenceTo.equals(PaymentDay._LAST_DELIVERY_DATE_)) {
			deliveryDate = ds.getDeliverydateTo();
		}
		if (deliveryDate == null) {
			return;
		}
		Date limit = this.getLimit(contract, deliveryDate);
		Invoice oldInvoice = ds.getId() == null ? null : this.findById(ds.getId());
		if (this.shouldSetBaseLineDate(ds, limit, oldInvoice)) {
			ds.setBaseLineDate(limit);
		}
		ds.setCalculatedBaseLineDate(limit);
	}

	private boolean shouldSetBaseLineDate(Invoice ds, Date limit, Invoice oldInvoice) {
		boolean isNull = ds.getBaseLineDate() == null || oldInvoice == null || oldInvoice.getContract() == null;
		return isNull || !oldInvoice.getContract().getId().equals(ds.getContract().getId())
				|| (oldInvoice.getCalculatedBaseLineDate() != null && !oldInvoice.getCalculatedBaseLineDate().equals(limit));
	}

	private Date getLimit(Contract contract, Date deliveryDate) {
		Integer paymentTerm = contract.getPaymentTerms();
		Calendar cal = Calendar.getInstance();
		cal.setTime(deliveryDate);
		cal.add(Calendar.DAY_OF_YEAR, paymentTerm);
		return cal.getTime();
	}

	@Override
	@History(value = "Reseted", type = Invoice.class)
	@Transactional
	public void resetInvoice(List<Invoice> invoices) throws BusinessException {

		for (Invoice invoice : invoices) {
			if (!BillStatus._PAID_.equals(invoice.getStatus()) && !BillStatus._DRAFT_.equals(invoice.getStatus())) {
				invoice.setStatus(BillStatus._DRAFT_);
				this.propagateIncomingInvoiceStatus(invoices);
				for (InvoiceLine il : invoice.getInvoiceLines()) {
					this.deliveryNoteService.deleteDeliveryNote(il.getId(), InvoiceLine.class.getSimpleName());
				}
			} else {
				throw new BusinessException(AccErrorCode.INVOICE_DRAFT_PAID, AccErrorCode.INVOICE_DRAFT_PAID.getErrMsg());
			}
		}
		this.update(invoices);
	}

	@Override
	@Transactional(noRollbackFor = { FinishInputException.class, InvalidInvoiceContractException.class })
	@History(type = Invoice.class)
	public void finishInput(Invoice invoice, @Action String value) throws BusinessException {
		if (!BillStatus._DRAFT_.equals(invoice.getStatus())) {
			throw new BusinessException(AccErrorCode.INVOICE_FINISH_INPUT_DRAFT, AccErrorCode.INVOICE_FINISH_INPUT_DRAFT.getErrMsg());
		}
		List<BusinessException> exceptions = new ArrayList<>();
		this.updateDeliveryDatesOnInvoice(invoice, exceptions);
		if (!this.checkContract(invoice)) {
			invoice.setContract(null);
			this.update(invoice);
			throw new InvalidInvoiceContractException(AccErrorCode.INVALID_CONTRACT,
					"Delivery period does not fit in contract period.<br/> Please recheck the contract.");
		}
		Map<Integer, BigDecimal> priceMap = this.getPrices(invoice);
		boolean valid = this.validateInputData(invoice, exceptions);
		valid = this.validateInvoiceLines(invoice, priceMap, exceptions) && valid;
		if (valid) {
			this.generateFuelEvent(invoice);
		}
		this.updateStatus(invoice, valid);
		if (!exceptions.isEmpty()) {
			this.saveToHistory(invoice, exceptions);
			throw new FinishInputException(exceptions);
		}
		this.propagateIncomingInvoiceStatus(Arrays.asList(invoice));
	}

	private void saveToHistory(Invoice invoice, List<BusinessException> exceptions) throws BusinessException {
		ChangeHistory ch = new ChangeHistory();
		ch.setObjectId(invoice.getId());
		ch.setObjectType(invoice.getClass().getSimpleName());
		StringBuilder sb = new StringBuilder();
		for (BusinessException be : exceptions) {
			sb.append(be.getMessage()).append("|");
		}
		ch.setRemarks(sb.toString());
		ch.setObjectValue("Finish input");
		this.historyService.insert(ch);
	}

	private boolean checkContract(Invoice inv) {
		Contract contract = inv.getContract();
		return contract != null && contract.getValidFrom().compareTo(inv.getDeliveryDateFrom()) <= 0
				&& contract.getValidTo().compareTo(inv.getDeliverydateTo()) >= 0;
	}

	private Map<Integer, BigDecimal> getPrices(Invoice invoice) throws BusinessException {
		Map<Integer, BigDecimal> prices = new HashMap<>();
		for (InvoiceLine il : invoice.getInvoiceLines()) {
			prices.put(il.getId(), this.cService.getPrice(invoice.getContract().getId(), invoice.getUnit().getCode(), invoice.getCurrency().getCode(),
					il.getDeliveryDate()));
		}
		return prices;
	}

	private void updateDeliveryDatesOnInvoice(Invoice invoice, List<BusinessException> exceptions) {
		List<InvoiceLine> invoiceLines = new ArrayList<>(invoice.getInvoiceLines());
		if (!invoiceLines.isEmpty()) {
			invoiceLines.sort((o1, o2) -> o1.getDeliveryDate().compareTo(o2.getDeliveryDate()));
			invoice.setDeliveryDateFrom(invoiceLines.get(0).getDeliveryDate());
			invoice.setDeliverydateTo(invoiceLines.get(invoiceLines.size() - 1).getDeliveryDate());
		} else {
			exceptions.add(new BusinessException(AccErrorCode.INVOICE_NO_INVOICE_LINE, AccErrorCode.INVOICE_NO_INVOICE_LINE.getErrMsg()));
		}
	}

	private void updateStatus(Invoice invoice, boolean valid) throws BusinessException {
		if (valid) {
			if (Boolean.parseBoolean(this.sysParamService.getInvoiceApprovalWorkflow())) {
				invoice.setStatus(BillStatus._CHECK_PASSED_);
			} else {
				invoice.setStatus(BillStatus._AWAITING_PAYMENT_);
				this.updateAccrualInvoicedAmount(invoice);
			}
		} else {
			invoice.setStatus(BillStatus._CHECK_FAILED_);
		}
		this.onUpdate(invoice);
	}

	private boolean validateInputData(Invoice invoice, List<BusinessException> exceptions) throws BusinessException {
		Integer decimalsForCurrency = this.sysParamService.getDecimalsForCurrency();
		boolean valid = true;
		BigDecimal amount = this.getInvLineAmount(invoice);
		if (!invoice.getTotalAmount().setScale(decimalsForCurrency, RoundingMode.HALF_UP)
				.equals(amount.setScale(decimalsForCurrency, RoundingMode.HALF_UP))) {
			valid = false;
			exceptions.add(new BusinessException(AccErrorCode.INVOICE_PRICE_DIFFERENT, AccErrorCode.INVOICE_PRICE_DIFFERENT.getErrMsg()));
		}
		if (!this.checkQuantityAmountBalanced(invoice, decimalsForCurrency)) {
			valid = false;
			exceptions.add(new BusinessException(AccErrorCode.INVOICE_BALANCED_VALUES_DIFFERENCE,
					AccErrorCode.INVOICE_BALANCED_VALUES_DIFFERENCE.getErrMsg()));
		}
		if (!VatApplicability._NOT_APPLICABLE_.equals(invoice.getContract().getVat())) {
			// VAT is not applicable for the contract
			BigDecimal invLineVatSum = BigDecimal.ZERO;
			for (Iterator<InvoiceLine> iter = invoice.getInvoiceLines().iterator(); iter.hasNext();) {
				InvoiceLine line = iter.next();
				invLineVatSum = invLineVatSum.add(line.getVat() != null ? line.getVat() : BigDecimal.ZERO);
			}
			BigDecimal vatAmount = invoice.getVatAmount() != null ? invoice.getVatAmount() : BigDecimal.ZERO;
			if (!invLineVatSum.setScale(decimalsForCurrency, RoundingMode.HALF_UP)
					.equals(vatAmount.setScale(decimalsForCurrency, RoundingMode.HALF_UP))) {
				valid = false;
				exceptions.add(new BusinessException(AccErrorCode.INVOICE_PRICE_VAT_DIFFERENT, AccErrorCode.INVOICE_PRICE_VAT_DIFFERENT.getErrMsg()));
			}
		}
		valid = !this.checkDuplicatedInvoiceLines(invoice, exceptions) && valid;
		return valid;
	}

	private BigDecimal getInvLineAmount(Invoice invoice) {
		BigDecimal amount = BigDecimal.ZERO;
		for (InvoiceLine il : invoice.getInvoiceLines()) {
			amount = amount.add(il.getAmount());

		}
		return amount;
	}

	private boolean checkQuantityAmountBalanced(Invoice invoice, Integer decimalsForCurrency) {
		Collection<InvoiceLine> invoiceLines = invoice.getInvoiceLines();
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal totalQuantity = BigDecimal.ZERO;
		for (InvoiceLine il : invoiceLines) {
			totalAmount = totalAmount.add(il.getAmount(), MathContext.DECIMAL64);
			totalQuantity = totalQuantity.add(il.getQuantity(), MathContext.DECIMAL64);
		}
		BigDecimal amountBalance = totalAmount.setScale(decimalsForCurrency, RoundingMode.HALF_UP)
				.subtract(invoice.getTotalAmount().setScale(decimalsForCurrency, RoundingMode.HALF_UP), MathContext.DECIMAL64)
				.setScale(decimalsForCurrency, RoundingMode.HALF_UP);
		BigDecimal quantityBalance = totalQuantity.setScale(decimalsForCurrency, RoundingMode.HALF_UP)
				.subtract(invoice.getQuantity().setScale(decimalsForCurrency, RoundingMode.HALF_UP), MathContext.DECIMAL64)
				.setScale(decimalsForCurrency, RoundingMode.HALF_UP);
		return amountBalance.compareTo(BigDecimal.ZERO) == 0 && quantityBalance.compareTo(BigDecimal.ZERO) == 0;
	}

	/**
	 * @param invoice
	 * @param exceptionList
	 * @return
	 */
	public boolean checkDuplicatedInvoiceLines(Invoice invoice, List<BusinessException> exceptionList) {
		Map<String, Object> invParams = new HashMap<>();
		invParams.put("deliveryLoc", invoice.getDeliveryLoc());
		invParams.put("receiver", invoice.getReceiver());
		List<Invoice> invoices = this.findEntitiesByAttributes(invParams);
		for (Iterator<Invoice> iterator = invoices.iterator(); iterator.hasNext();) {
			Invoice inv = iterator.next();
			if (!inv.getId().equals(invoice.getId()) && (invoice.getDeliveryDateFrom().after(inv.getDeliverydateTo())
					|| invoice.getDeliverydateTo().before(inv.getDeliveryDateFrom()))) {
				iterator.remove();
			}
		}

		Set<InvLine> existingInvLines = new HashSet<>();
		Set<InvLine> duplicatedInvLines = new HashSet<>();

		for (Invoice inv : invoices) {
			if (!inv.getId().equals(invoice.getId())) {
				for (InvoiceLine il : inv.getInvoiceLines()) {
					InvLine invLine = new InvLine(il);
					existingInvLines.add(invLine);
				}
			}
		}

		for (InvoiceLine invoiceLine : invoice.getInvoiceLines()) {
			invoiceLine.setEventCheckStatus(CheckStatus._OK_);
			invoiceLine.setOverAllCheckStatus(CheckStatus._OK_);// If the finish input was not success, on the recheck the status was not
																// modified on the success. Alpar
			InvLine invLine = new InvLine(invoiceLine);
			if (!existingInvLines.add(invLine)) {
				duplicatedInvLines.add(invLine);
			}
		}

		return this.buildExceptionList(exceptionList, duplicatedInvLines);
	}

	private boolean buildExceptionList(List<BusinessException> exceptionList, Set<InvLine> duplicatedInvLines) {
		if (!duplicatedInvLines.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			sb.append("<ul>");
			for (Iterator<InvLine> iterator = duplicatedInvLines.iterator(); iterator.hasNext();) {
				InvLine invLine = iterator.next();
				sb.append("<li>").append(invLine.toString()).append("</li>");
				invLine.getInvLine().setEventCheckStatus(CheckStatus._FAILED_);
				invLine.getInvLine().setOverAllCheckStatus(CheckStatus._FAILED_);
			}
			sb.append("</ul>");
			exceptionList.add(new BusinessException(AccErrorCode.DUPLICATE_INV_LINE_EXISTS,
					AccErrorCode.DUPLICATE_INV_LINE_EXISTS.getErrMsg() + sb.toString()));
			return true;

		}
		return false;
	}

	private boolean validateInvoiceLines(Invoice invoice, Map<Integer, BigDecimal> priceMap, List<BusinessException> exceptions)
			throws BusinessException {
		boolean valid = true;
		for (InvoiceLine invoiceLine : invoice.getInvoiceLines()) {
			BigDecimal calcAmount = invoiceLine.getQuantity().multiply(priceMap.get(invoiceLine.getId()));
			BigDecimal vat = invoiceLine.getVat() != null ? invoiceLine.getVat() : BigDecimal.ZERO;
			BigDecimal calcVAT = invoiceLine.getCalculatedVat() != null ? invoiceLine.getCalculatedVat().setScale(4, RoundingMode.HALF_UP)
					: BigDecimal.ZERO;
			if (vat.setScale(4, RoundingMode.HALF_UP).equals(calcVAT)) {
				invoiceLine.setVatCheckResult(CheckResult._NO_DISCREPANCIES_);
				invoiceLine.setVatCheckStatus(CheckStatus._OK_);
			}
			valid = this.checkTolerance(invoiceLine.getAmount(), vat.setScale(4, RoundingMode.HALF_UP), calcAmount, calcVAT, invoice.getCurrency(),
					GregorianCalendar.getInstance().getTime(), exceptions, invoiceLine);
		}
		return valid;
	}

	private boolean checkTolerance(BigDecimal amount, BigDecimal vat, BigDecimal calcAmount, BigDecimal calcVAT, Currencies currency, Date date,
			List<BusinessException> exceptions, InvoiceLine invoiceLine) throws BusinessException {
		boolean valid = true;

		ToleranceVerifier verifier = new ToleranceVerifier(this.unitConverterService, this.getBusinessDelegate(ExchangeRate_Bd.class),
				this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity());
		Tolerance toleranceVAT = this.toleranceService.getTolerance(InvoiceLineTolerance.CHECK_BILL_VAT.name());
		Tolerance toleranceAmount = this.toleranceService.getTolerance(InvoiceLineTolerance.CHECK_BILL_AMOUNT.name());
		ToleranceResult toleranceResultAmount = verifier.checkTolerance(amount, calcAmount, currency, currency, toleranceAmount, date);
		ToleranceResult toleranceResultVAT = verifier.checkTolerance(vat, calcVAT, currency, currency, toleranceVAT, date);
		switch (toleranceResultAmount.getType()) {
		case NOT_WITHIN:
			this.addToExceptionList(AccErrorCode.INVOICE_LINE_BILL_AMOUNT_OUT_OF_TOLERANCE, exceptions);
			invoiceLine.setOverAllCheckStatus(CheckStatus._FAILED_);
			valid = false;
			break;
		case WITHIN:
		case MARGIN:
		default:
			break;
		}
		switch (toleranceResultVAT.getType()) {
		case NOT_WITHIN:
			this.addToExceptionList(AccErrorCode.INVOICE_LINE_BILL_VAT_OUT_OF_TOLERANCE, exceptions);
			invoiceLine.setVatCheckStatus(CheckStatus._FAILED_);
			invoiceLine.setVatCheckResult(CheckResult._OUT_TOLERANCE_);
			invoiceLine.setOverAllCheckStatus(CheckStatus._FAILED_);
			valid = false;
			break;
		case WITHIN:
		case MARGIN:
		default:
			if (valid) {
				invoiceLine.setVatCheckStatus(CheckStatus._OK_);
				invoiceLine.setVatCheckResult(CheckResult._IN_TOLERANCE_);
			}
			break;
		}
		return valid;
	}

	private void addToExceptionList(AccErrorCode accErrorCode, List<BusinessException> exceptions) {
		for (BusinessException be : exceptions) {
			if (be.getErrorCode().getErrNo() == accErrorCode.getErrNo()) {
				return;
			}
		}
		exceptions.add(new BusinessException(accErrorCode, accErrorCode.getErrMsg()));
	}

	@Override
	public void generateFuelEvent(Invoice invoice) throws BusinessException {
		this.feManagerServ.manage(invoice);
	}

	@Override
	@History(type = Invoice.class)
	@Transactional
	public void modifyStatus(List<Invoice> invoices, @Action String action, BillStatus status) throws BusinessException {
		for (Invoice invoice : invoices) {
			invoice.setStatus(status);
			if (invoice.getContract() == null) {
				throw new BusinessException(AccErrorCode.INVOICE_NO_CONTRACT, AccErrorCode.INVOICE_NO_CONTRACT.getErrMsg());
			}
			this.updateAccrualInvoicedAmount(invoice);
		}
		this.update(invoices);
		this.propagateIncomingInvoiceStatus(invoices);
	}

	private void updateAccrualInvoicedAmount(Invoice invoice) throws BusinessException {
		BillStatus invStatus = invoice.getStatus();
		InvoiceCostToAccrual invoiceCostToAccrual = this.sysParamService.getInvoiceCostToAccruals();
		if ((invStatus.equals(BillStatus._AWAITING_PAYMENT_) && !InvoiceCostToAccrual._ON_PAYMENT_.equals(invoiceCostToAccrual))
				|| (invStatus.equals(BillStatus._PAID_) && InvoiceCostToAccrual._ON_PAYMENT_.equals(invoiceCostToAccrual))) {
			ApproveIncommingInvoice_Bd bDelegate = this.getBusinessDelegate(ApproveIncommingInvoice_Bd.class);
			bDelegate.approveInvoice(invoice);
		}
	}

	private void propagateIncomingInvoiceStatus(List<Invoice> invoices) throws BusinessException {
		for (Invoice invoice : invoices) {
			for (InvoiceLine il : invoice.getInvoiceLines()) {
				this.deliveryNoteService.propagateIncInvoiceStatus(il);
			}
		}
	}

	private void propagateIncomingInvoiceStatus(Invoice invoice) throws BusinessException {
		this.propagateIncomingInvoiceStatus(Arrays.asList(invoice));
	}

	@Override
	@Transactional
	public void updateWithoutBusiness(Invoice invice) throws BusinessException {
		this.onUpdate(invice);

	}

	@Override
	public void modifyStatus(Object object) throws BusinessException {
		if (object instanceof MessageContractObject) {
			MessageContractObject mco = (MessageContractObject) object;
			Invoice invoice = this.findById(mco.getInvoiceId());
			invoice.setStatus(BillStatus.getByName(mco.getStatus()));
			if (mco.getStatus().equalsIgnoreCase(InvoiceStatus.NO_CONTRACT.getName())) {
				invoice.setContract(null);
			}
			this.update(invoice);
			this.propagateIncomingInvoiceStatus(invoice);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.api.invoices.IInvoiceService#findByInvoiceNumbers(java.util.List)
	 */
	@Override
	public List<Invoice> findByInvoiceNumbers(List<String> list) throws BusinessException {
		String qlString = "SELECT e FROM Invoice e WHERE e.invoiceNo IN :numbers AND e.clientId = :clientId";
		return this.getEntityManager().createQuery(qlString, Invoice.class).setParameter("numbers", list)
				.setParameter("clientId", Session.user.get().getClientId()).getResultList();
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.api.invoices.IInvoiceService#deleteByOutgoingInvoice(java.util.List)
	 */
	@Override
	@Transactional
	public void deleteByOutgoingInvoice(List<OutgoingInvoice> list) throws BusinessException {
		List<String> invoiceNumbers = list.stream().map(OutgoingInvoice::getInvoiceNo).collect(Collectors.toList());
		List<Invoice> invoices = this.findByInvoiceNumbers(invoiceNumbers);
		List<Object> ids = invoices.stream().map(Invoice::getId).collect(Collectors.toList());
		this.deleteByIds(ids);

	}

	@Override
	@Transactional
	public void markAsCredited(OutgoingInvoice outgoingInvoice) throws BusinessException {
		List<Invoice> incomingInvoices = this.findByInvoiceNumbers(Arrays.asList(outgoingInvoice.getInvoiceNo()));
		for (Invoice incomingInvoice : incomingInvoices) {
			incomingInvoice.setStatus(BillStatus._CREDITED_);
			this.update(incomingInvoice);

			List<InvoiceLine> incomingInvoiceLines = (List<InvoiceLine>) incomingInvoice.getInvoiceLines();
			for (InvoiceLine incomingInvoiceLine : incomingInvoiceLines) {
				List<DeliveryNote> deliveryNotes = this.deliveryNoteService.findByObjectIdAndType(incomingInvoiceLine.getId(),
						InvoiceLine.class.getSimpleName());
				for (DeliveryNote deliveryNote : deliveryNotes) {
					deliveryNote.getFuelEvent().setBillStatus(incomingInvoice.getStatus());
				}
			}
		}
	}
}
