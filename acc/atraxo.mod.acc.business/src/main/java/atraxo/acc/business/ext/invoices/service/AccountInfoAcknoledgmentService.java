package atraxo.acc.business.ext.invoices.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.ext.invoices.IAccountInfoAcknoledgmentService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IOutgoingInvoiceLineService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ws.FuelEventDealTypeAliasEnum;
import atraxo.acc.domain.ext.ricoh.EntityTypeName;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceMessageHistoryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceMessageHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.ws.commonResponse.Any;
import atraxo.fmbas.domain.ws.commonResponse.Any.Acknowledgement.DataSets.DataSet;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author apetho
 */

public class AccountInfoAcknoledgmentService extends AbstractBusinessBaseService implements IAccountInfoAcknoledgmentService {

	private static final String SUCCESS2 = "Success";

	private static final Logger LOG = LoggerFactory.getLogger(AccountInfoAcknoledgmentService.class);

	@Autowired
	private IExternalInterfaceService externalInterfaceService;
	@Autowired
	private ExternalInterfaceHistoryFacade externalInterfaceHistoryFacade;
	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;
	@Autowired
	private IOutgoingInvoiceLineService oilService;
	@Autowired
	private IFuelEventService feService;
	@Autowired
	private IExternalInterfaceMessageHistoryService messageHistoryService;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageHistoryFacade;

	protected void verifyInterfaceEnabbled(ExternalInterface externalInterface) throws BusinessException {
		if (!externalInterface.getEnabled()) {
			throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
		}
	}

	@Override
	@Transactional
	public boolean processFuelEvent(MSG msg) throws BusinessException, JAXBException {
		LOG.info("Started Fuel Event acknowledgement processing");
		ExternalInterface externalInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.ACK_FUEL_EVENT_INTERFACE.getValue());
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		boolean success = false;
		StringBuilder historyMessage = new StringBuilder();
		try {
			this.verifyInterfaceEnabbled(externalInterface);
			ExternalInterfaceMessageHistory historyMsg = this.getHistoryMessage(msg.getHeaderCommon().getCorrelationID());
			if (!this.canUpdate(historyMsg)) {
				throw new BusinessException(AccErrorCode.IS_NOT_THE_LAST_EXPORT, AccErrorCode.IS_NOT_THE_LAST_EXPORT.getErrMsg());
			}
			success = this.updateFuelEvent(msg, historyMsg);
			historyMessage.append(SUCCESS2);
			LOG.info("Fuel Event acknowledgement was successfully processed");
		} catch (BusinessException | JAXBException | XMLStreamException | FactoryConfigurationError e) {
			LOG.warn("Problem occured on Fuel Event acknowledgement processing!", e);
			historyMessage.append(e.getMessage());
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, this.getExceptionMessageChain(e), e);
		} finally {
			stopWatch.stop();
			historyMessage.append("\nMessage:").append(XMLUtils.toString(msg, MSG.class, true, true));
			this.saveToHistory(externalInterface, stopWatch, success, historyMessage.toString());
		}
		return success;
	}

	/**
	 * @param msg
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	@Override
	public boolean processInvoice(MSG msg) throws BusinessException, JAXBException {
		ExternalInterface externalInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.ACK_INVOICE_INTERFACE.getValue());
		return this.processInvoiceLine(msg, externalInterface);
	}

	/**
	 * @param msg
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	@Override
	@Transactional
	public boolean processCreditMemo(MSG msg) throws BusinessException, JAXBException {
		ExternalInterface externalInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.ACK_CREDIT_MEMO_INTERFACE.getValue());
		return this.processInvoiceLine(msg, externalInterface);
	}

	@Override
	@Transactional
	public boolean processRicohAcknoledgment(MSG msg) throws BusinessException, JAXBException {
		LOG.info("Started Ricoh acknowledgement processing");
		ExternalInterface externalInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.ACK_FROM_RICOH_PRINTER.getValue());
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		boolean success = false;
		StringBuilder historyMessage = new StringBuilder();
		try {
			this.verifyInterfaceEnabbled(externalInterface);
			Any acknowledgementAny = (Any) XMLUtils.toObject(msg.getPayload().getAny(), Any.class);

			for (DataSet dataSet : acknowledgementAny.getAcknowledgement().getDataSets().getDataSet()) {
				String transactioID = dataSet.getTransactionID();
				String name = dataSet.getName();

				String[] info = name.split(":");
				String entityType = info[0];
				String entityCode = info[1];

				if (entityType.equals(EntityTypeName.NDR.toString())) {
					this.processFuelEvent(entityCode, transactioID);
				} else if (entityType.equals(EntityTypeName.FCF.toString()) || (entityType.equals(EntityTypeName.CCF.toString()))
						|| (entityType.equals(EntityTypeName.FEX.toString()) || (entityType.equals(EntityTypeName.NDC.toString())))) {
					this.processInvoice(entityCode, transactioID);
				} else {
					throw new BusinessException(AccErrorCode.TAG_NOT_DEFINED_CORRECTLY,
							String.format(AccErrorCode.TAG_NOT_DEFINED_CORRECTLY.getErrMsg(), "<Name>"));
				}
			}

			success = true;
			LOG.info("Ricoh acknowledgement was successfully processed");
		} catch (Exception e) {
			LOG.warn("Problem occured on Ricoh acknowledgement processing!", e);
			historyMessage.append(e.getMessage());
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, this.getExceptionMessageChain(e), e);
		} finally {
			stopWatch.stop();
			historyMessage.append("\nMessage:").append(XMLUtils.toString(msg, MSG.class, true, true));
			this.saveToHistory(externalInterface, stopWatch, success, historyMessage.toString());
		}
		return success;
	}

	private void processInvoice(String invoiceNumber, String transactioID) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("invoiceNo", invoiceNumber);
		OutgoingInvoice invoice = this.outgoingInvoiceService.findEntityByAttributes(params);
		invoice.setControlNo(transactioID);
		this.outgoingInvoiceService.update(invoice);
	}

	private void processFuelEvent(String id, String transactioID) throws BusinessException {
		FuelEvent fuelEvent = this.feService.findById(Integer.valueOf(id));
		this.feService.setBillOfLading(transactioID, fuelEvent);
	}

	private boolean processInvoiceLine(MSG msg, ExternalInterface externalInterface) throws BusinessException, JAXBException {
		LOG.info("Started Invoice Line acknowledgement processing");
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		boolean success = false;
		StringBuilder historyMessage = new StringBuilder();
		try {
			this.verifyInterfaceEnabbled(externalInterface);
			ExternalInterfaceMessageHistory historyMsg = this.getHistoryMessage(msg.getHeaderCommon().getCorrelationID());
			if (!this.canUpdate(historyMsg)) {
				this.updateMessageHistory(msg, false);
				throw new BusinessException(AccErrorCode.IS_NOT_THE_LAST_EXPORT, AccErrorCode.IS_NOT_THE_LAST_EXPORT.getErrMsg());
			}
			if (historyMsg.getObjectType().equals(OutgoingInvoiceLine.class.getSimpleName())) {
				success = this.updateInvoice(msg);
			}
			historyMessage.append(SUCCESS2);
			LOG.info("Invoice Line acknowledgement was successfully processed");
		} catch (BusinessException | JAXBException | XMLStreamException | FactoryConfigurationError e) {
			LOG.warn("Problem occured on Invoice Line acknowledgement processing!", e);
			historyMessage.append(e.getMessage());
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, this.getExceptionMessageChain(e));
		} finally {
			stopWatch.stop();
			historyMessage.append("\nMessage:").append(XMLUtils.toString(msg, MSG.class, true, true));
			this.updateMessageHistory(msg, success);
			this.saveToHistory(externalInterface, stopWatch, success, historyMessage.toString());
		}
		return success;
	}

	private boolean canUpdate(ExternalInterfaceMessageHistory messageHistory) throws BusinessException {
		return this.messageHistoryService.isLastMessage(messageHistory.getObjectId(), messageHistory.getObjectType(), messageHistory.getMessageId());

	}

	private void saveToHistory(ExternalInterface externalInterface, StopWatch stopWatch, boolean success, String message) throws BusinessException {
		ExternalInterfaceHistory history = new ExternalInterfaceHistory();
		history.setExternalInterface(externalInterface);
		history.setRequester(Session.user.get().getClientCode());
		history.setRequestReceived(new Date(stopWatch.getStartTime()));
		history.setResponseSent(new Date(stopWatch.getStartTime() + stopWatch.getTime()));
		history.setStatus(success ? ExternalInterfacesHistoryStatus._SUCCESS_ : ExternalInterfacesHistoryStatus._FAILURE_);
		history.setResult(message);
		this.externalInterfaceHistoryFacade.insert(history);
		LOG.debug("Inserted succesfully the message");
	}

	private boolean updateFuelEvent(MSG msg, ExternalInterfaceMessageHistory sentMessageHistory)
			throws BusinessException, JAXBException, XMLStreamException, FactoryConfigurationError {
		Any acknowledgementAny = (Any) XMLUtils.toObject(msg.getPayload().getAny(), Any.class);
		FuelEvent fe = this.getFuelEvent(msg);
		if (sentMessageHistory.getObjectType().equals(FuelEventDealTypeAliasEnum.SALE.getCustomEntityAlias())) {
			fe.setTransmissionStatusSale(FuelEventTransmissionStatus._FAILED_);
		}
		if (sentMessageHistory.getObjectType().equals(FuelEventDealTypeAliasEnum.PURCHASE.getCustomEntityAlias())) {
			fe.setTransmissionStatusPurchase(FuelEventTransmissionStatus._FAILED_);
		}
		List<DataSet> list = acknowledgementAny.getAcknowledgement().getDataSets().getDataSet();
		if (CollectionUtils.isEmpty(list)) {
			throw new BusinessException(AccErrorCode.NO_FUEL_EVENT_FOUND, AccErrorCode.NO_FUEL_EVENT_FOUND.getErrMsg());
		}
		boolean processStatus = list.get(0).isProcessed();
		if (processStatus) {
			String transactionId = list.get(0).getTransactionID();
			if (sentMessageHistory.getObjectType().equals(FuelEventDealTypeAliasEnum.PURCHASE.getCustomEntityAlias())) {
				fe.setErpPurchaseReceipt(transactionId);
				fe.setTransmissionStatusPurchase(FuelEventTransmissionStatus._TRANSMITTED_);
			} else if (sentMessageHistory.getObjectType().equals(FuelEventDealTypeAliasEnum.SALE.getCustomEntityAlias())) {
				fe.setErpSalesShipment(transactionId);
				fe.setTransmissionStatusSale(FuelEventTransmissionStatus._TRANSMITTED_);
			}
		}
		this.feService.updateWithoutBusiness(fe);
		return processStatus;
	}

	private boolean updateInvoice(MSG msg) throws BusinessException, JAXBException, XMLStreamException, FactoryConfigurationError {
		Any acknowledgementAny = (Any) XMLUtils.toObject(msg.getPayload().getAny(), Any.class);
		String transactioID = acknowledgementAny.getAcknowledgement().getDataSets().getDataSet().get(0).getTransactionID();
		OutgoingInvoiceLine oil = this.getInvoiceLine(msg);
		boolean processStatus = this.getProcessStatus(acknowledgementAny);
		InvoiceLineTransmissionStatus status = processStatus ? InvoiceLineTransmissionStatus._TRANSMITTED_ : InvoiceLineTransmissionStatus._FAILED_;
		oil.setTransmissionStatus(processStatus ? InvoiceLineTransmissionStatus._TRANSMITTED_ : InvoiceLineTransmissionStatus._FAILED_);
		synchronized (this.outgoingInvoiceService) {
			this.outgoingInvoiceService.updateTransmissionStatus(oil, status, transactioID);
		}
		return processStatus;
	}

	private OutgoingInvoiceLine getInvoiceLine(MSG msg) throws BusinessException {
		String messageId = msg.getHeaderCommon().getCorrelationID();
		List<ExternalInterfaceMessageHistory> list = this.messageHistoryService.findByMessageId(messageId);
		if (!CollectionUtils.isEmpty(list) && list.size() == 1) {
			ExternalInterfaceMessageHistory messageHistory = list.get(0);
			return this.oilService.findById(messageHistory.getObjectId());
		} else {
			LOG.warn(AccErrorCode.INVALID_CORRELATION_ID.getErrMsg());
			throw new BusinessException(AccErrorCode.INVALID_CORRELATION_ID, AccErrorCode.INVALID_CORRELATION_ID.getErrMsg());
		}
	}

	private FuelEvent getFuelEvent(MSG msg) throws BusinessException {
		String messageId = msg.getHeaderCommon().getCorrelationID();
		List<ExternalInterfaceMessageHistory> list = this.messageHistoryService.findByMessageId(messageId);
		if (!CollectionUtils.isEmpty(list) && list.size() == 1) {
			ExternalInterfaceMessageHistory messageHistory = list.get(0);
			return this.feService.findById(messageHistory.getObjectId());
		} else {
			LOG.warn(AccErrorCode.INVALID_CORRELATION_ID.getErrMsg());
			throw new BusinessException(AccErrorCode.INVALID_CORRELATION_ID, AccErrorCode.INVALID_CORRELATION_ID.getErrMsg());
		}

	}

	private boolean getProcessStatus(Any acknowledgementAny) throws BusinessException {
		List<DataSet> list = acknowledgementAny.getAcknowledgement().getDataSets().getDataSet();
		if (CollectionUtils.isEmpty(list)) {
			throw new BusinessException(AccErrorCode.NO_FUEL_EVENT_FOUND, AccErrorCode.NO_FUEL_EVENT_FOUND.getErrMsg());
		}
		return list.get(0).isProcessed();
	}

	private void updateMessageHistory(MSG message, boolean success) throws BusinessException {
		ExternalInterfaceMessageHistoryStatus status = success ? ExternalInterfaceMessageHistoryStatus._ACKNOWLEDGED_
				: ExternalInterfaceMessageHistoryStatus._FAILED_;
		this.messageHistoryFacade.updateMessageHistory(message, status);
	}

	private String getExceptionMessageChain(Throwable throwable) {
		StringBuilder sb = new StringBuilder();
		Throwable t = throwable;
		while (t != null) {
			sb.append(throwable.getMessage()).append("|");
			t = t.getCause();
		}
		return sb.toString();
	}

	private ExternalInterfaceMessageHistory getHistoryMessage(String correlationID) throws BusinessException {
		List<ExternalInterfaceMessageHistory> list = this.messageHistoryService.findByMessageId(correlationID);
		if (CollectionUtils.isEmpty(list) || list.size() != 1) {
			LOG.warn(AccErrorCode.INVALID_CORRELATION_ID.getErrMsg());
			throw new BusinessException(AccErrorCode.INVALID_CORRELATION_ID, AccErrorCode.INVALID_CORRELATION_ID.getErrMsg());
		}
		return list.get(0);
	}

}
