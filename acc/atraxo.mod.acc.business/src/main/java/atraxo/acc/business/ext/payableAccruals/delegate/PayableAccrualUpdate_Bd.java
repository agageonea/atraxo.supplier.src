package atraxo.acc.business.ext.payableAccruals.delegate;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import atraxo.acc.business.ext.fuelEvents.delegate.DensityDelegate;
import atraxo.acc.business.ext.fuelEvents.service.CostCalculatorService;
import atraxo.acc.domain.ext.deliveryNotes.AccrualDateParameter;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.cmm.domain.ext.contracts.CostVat;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author apetho
 */
public class PayableAccrualUpdate_Bd extends AbstractBusinessDelegate {

	/**
	 * Update the payable accrual expected cost and accrued quantity and costs.
	 *
	 * @param pa {@link PayableAccrual}
	 * @throws BusinessException
	 */
	public void updatePayableAccrual(PayableAccrual pa) throws BusinessException {
		this.setPricesAndAmounts(pa.getFuelEvent(), pa.getContract(), pa);
	}

	/**
	 * Update the payable accrual expected cost and accrued quantity and costs.
	 *
	 * @param fuelEvent
	 * @param contract
	 * @param pa
	 * @throws BusinessException
	 */
	private void setPricesAndAmounts(FuelEvent fuelEvent, Contract contract, PayableAccrual pa) throws BusinessException {
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		DensityDelegate densityDelegate = this.getBusinessDelegate(DensityDelegate.class);
		BigDecimal expectedSysQuantity = densityDelegate.getInSystemQuantity(fuelEvent);
		pa.setExpectedQuantity(expectedSysQuantity);
		IUnitService unitService = (IUnitService) this.findEntityService(Unit.class);
		Unit sysUnit = unitService.findByCode(paramSrv.getSysVol());
		PayableCostVat ppc = this.getPriceAndVat(fuelEvent, contract, paramSrv, pa, sysUnit);
		this.setExpectedAmount(ppc, pa);
		this.setAccrualAmount(pa, sysUnit, this.getConversionDate(fuelEvent));
	}

	/**
	 * Calculate the price in settlement and in system currency.
	 *
	 * @param fuelEvent
	 * @param contract
	 * @param contractService
	 * @param paramSrv
	 * @param pa
	 * @return {@link PayableCostVat}
	 * @throws BusinessException
	 */
	private PayableCostVat getPriceAndVat(FuelEvent fuelEvent, Contract contract, ISystemParameterService paramSrv, PayableAccrual pa, Unit sysUnit)
			throws BusinessException {

		CostCalculatorService calcSrv = (CostCalculatorService) this.getApplicationContext().getBean("costCalculator");
		CostVat settlementPrice = calcSrv.calculate(contract, fuelEvent);

		ICurrenciesService currService = (ICurrenciesService) this.findEntityService(Currencies.class);
		Currencies sysCurrency = currService.findByCode(paramSrv.getSysCurrency());
		PayableCostVat sysPrice = this.convert(sysUnit, sysUnit, contract.getSettlementCurr(), sysCurrency, paramSrv, settlementPrice,
				this.getConversionDate(fuelEvent));
		pa.setExchangeRate(sysPrice.getFactor());
		return sysPrice;
	}

	/**
	 * Convert price using standard financial source average method.
	 *
	 * @param fromUnit
	 * @param toUnit
	 * @param fromCurrency
	 * @param toCurrency
	 * @param paramSrv
	 * @param price
	 * @param date
	 * @return
	 * @throws BusinessException
	 */
	private PayableCostVat convert(Unit fromUnit, Unit toUnit, Currencies fromCurrency, Currencies toCurrency, ISystemParameterService paramSrv,
			CostVat price, Date date) throws BusinessException {
		IFinancialSourcesService finSrcSrv = (IFinancialSourcesService) this.findEntityService(FinancialSources.class);
		PriceConverterService priceConverterService = this.getApplicationContext().getBean(PriceConverterService.class);
		Map<String, Object> params = new HashMap<>();
		params.put("isStdFlg", true);
		params.put("active", true);
		FinancialSources stdFinSrc = finSrcSrv.findEntityByAttributes(params);
		IAverageMethodService avgMthSrv = (IAverageMethodService) this.findEntityService(AverageMethod.class);
		AverageMethod stdAvgMth = avgMthSrv.findByName(paramSrv.getSysAverageMethod());
		Density density = this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity();
		ConversionResult convPrice = priceConverterService.convert(fromUnit, toUnit, fromCurrency, toCurrency, price.getCost(), date, stdFinSrc,
				stdAvgMth, density, false, MasterAgreementsPeriod._CURRENT_);

		ConversionResult convVat = priceConverterService.convert(fromUnit, toUnit, fromCurrency, toCurrency, price.getVat(), date, stdFinSrc,
				stdAvgMth, density, false, MasterAgreementsPeriod._CURRENT_);

		return new PayableCostVat(convPrice.getValue(), price.getCost(), convVat.getValue(), price.getVat(), convPrice.getFactor());
	}

	/**
	 * Calculate and set the expected costs and vat.
	 *
	 * @param ppc
	 * @param pa
	 * @param expectedQuantity
	 */
	private void setExpectedAmount(PayableCostVat ppc, PayableAccrual pa) {
		pa.setExpectedAmountSet(ppc.getSetCost());
		pa.setExpectedAmountSys(ppc.getSysCost());
		pa.setExpectedVATSet(ppc.getSetVat());
		pa.setExpectedVATSys(ppc.getSysVat());
	}

	/**
	 * Set up the accrued quantity and amount.
	 *
	 * @param pa
	 * @param sysUnit
	 * @param date
	 * @throws BusinessException
	 */
	private void setAccrualAmount(PayableAccrual pa, Unit sysUnit, Date date) throws BusinessException {
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		CostVat price = new CostVat(pa.getInvoicedAmountSet(), pa.getInvoicedVATSet());
		PayableCostVat invSetPriceAndVat = this.convert(sysUnit, sysUnit, pa.getInvoicedCurrency(), pa.getAccrualCurrency(), paramSrv, price, date);
		pa.setAccrualQuantity(pa.getExpectedQuantity().subtract(pa.getInvoicedQuantity(), MathContext.DECIMAL64));
		pa.setAccrualAmountSet(pa.getExpectedAmountSet().subtract(invSetPriceAndVat.getSysCost(), MathContext.DECIMAL64));
		pa.setAccrualAmountSys(pa.getExpectedAmountSys().subtract(pa.getInvoicedAmountSys(), MathContext.DECIMAL64));
		pa.setAccrualVATSet(pa.getExpectedVATSet().subtract(invSetPriceAndVat.getSysVat(), MathContext.DECIMAL64));
		pa.setAccrualVATSys(pa.getExpectedVATSys().subtract(pa.getInvoicedVATSys(), MathContext.DECIMAL64));
	}

	/**
	 * Get the accrual evaluation date.
	 *
	 * @param paramSrv
	 * @param fe
	 * @return If the system parameter is event date return the fuel event's fueling date, else the current date.
	 * @throws BusinessException
	 */
	private Date getConversionDate(FuelEvent fuelEvent) throws BusinessException {
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		String accrualDateParameter = paramSrv.getAccrualEvaluationDate();
		Date date = GregorianCalendar.getInstance().getTime();
		if (accrualDateParameter.equalsIgnoreCase(AccrualDateParameter.EVENT_DATE.getValue())) {
			date = fuelEvent.getFuelingDate();
		}
		return date;
	}
}
