package atraxo.acc.business.ext.cost.job;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.ext.cost.FuelEventCostUpdaterService;
import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.contracts.MarkedContract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
public class MarkedContractProcessor implements ItemProcessor<MarkedContract, List<FuelEvent>> {

	private static final String SUCCESS = "success";
	private static final String FAILED = "failed";
	private static final String STRUCTURE = "structure";

	private static final Logger LOG = LoggerFactory.getLogger(MarkedContractProcessor.class);

	@Autowired
	private FuelEventCostUpdaterService costUpdSrv;
	@Autowired
	private IContractService contractSrv;
	private ExecutionContext executionContext;

	/**
	 * @param stepExecution
	 */
	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.executionContext = stepExecution.getExecutionContext();
	}

	@Override
	public List<FuelEvent> process(MarkedContract mc) throws Exception {
		List<FuelEvent> feList = new ArrayList<>();
		int successCounter = 0;
		int structureCounter = 0;
		StringBuilder sb = new StringBuilder();
		StringBuilder eventsList = new StringBuilder();
		try {
			List<FuelEvent> payables = this.costUpdSrv.updatePayableCost(mc.getContractId(), eventsList);
			successCounter += payables.size();
			feList.addAll(payables);
			List<FuelEvent> receivables = this.costUpdSrv.updateReceivable(mc.getContractId(), eventsList);
			successCounter += receivables.size();
			feList.addAll(receivables);
			this.costUpdSrv.updateAccruals(feList);
			List<FuelEvent> modified = this.checkPriceStructure(mc.getContractId());
			structureCounter = modified.size();
			feList.addAll(modified);
		} catch (Exception e) {
			if (sb.length() != 0) {
				sb.append(" | ");
			}
			if (!eventsList.toString().isEmpty()) {
				sb.append("refId+ticket number:").append(eventsList.toString()).append(e.getMessage()).append("\n");
			}
			LOG.warn("Fuel event cost updater exception.", e);
		}
		this.executionContext.put(FAILED, sb.toString());
		this.executionContext.put(SUCCESS, successCounter);
		this.executionContext.put(STRUCTURE, structureCounter);
		return feList;
	}

	private List<FuelEvent> checkPriceStructure(Integer contractId) throws BusinessException {
		Contract c = this.contractSrv.findById(contractId);
		List<FuelEvent> returnList = new ArrayList<>();
		List<String> includedList = new ArrayList<>();
		List<String> calculateList = new ArrayList<>();
		int count = c.getPriceCategories().size();
		for (ContractPriceCategory cat : c.getPriceCategories()) {
			if (CalculateIndicator._INCLUDED_.equals(cat.getCalculateIndicator())) {
				includedList.add(cat.getPriceCategory().getName());
			}
			if (CalculateIndicator._CALCULATE_ONLY_.equals(cat.getCalculateIndicator())) {
				calculateList.add(cat.getPriceCategory().getName());
			}
		}

		List<FuelEvent> fuelEvents = this.costUpdSrv.getFuelEventWithPayableCost(c);
		returnList.addAll(this.checkFuelEventsPriceStructure(fuelEvents, includedList, calculateList, count));

		fuelEvents = this.costUpdSrv.getFuelEventsWithReceivableCost(Contract.class, contractId);
		returnList.addAll(this.checkFuelEventsPriceStructure(fuelEvents, includedList, calculateList, count));

		return returnList;
	}

	private List<FuelEvent> checkFuelEventsPriceStructure(List<FuelEvent> list, List<String> includedList, List<String> calculateList, int count) {
		List<FuelEvent> returnList = new ArrayList<>();
		for (Iterator<FuelEvent> iterator = list.iterator(); iterator.hasNext();) {
			FuelEvent fuelEvent = iterator.next();
			if (FuelEventStatus._CANCELED_.equals(fuelEvent.getStatus())) {
				continue;
			}
			if (count != fuelEvent.getDetails().size()) {
				returnList.add(fuelEvent);
			}

			for (FuelEventsDetails details : fuelEvent.getDetails()) {
				if (CalculateIndicator._INCLUDED_.equals(details.getCalculateIndicator())
						&& !includedList.contains(details.getPriceCategory().getName())) {
					returnList.add(fuelEvent);
				}
				if (CalculateIndicator._CALCULATE_ONLY_.equals(details.getCalculateIndicator())
						&& !calculateList.contains(details.getPriceCategory().getName())) {
					returnList.add(fuelEvent);
				}
			}
		}
		return returnList;
	}

}
