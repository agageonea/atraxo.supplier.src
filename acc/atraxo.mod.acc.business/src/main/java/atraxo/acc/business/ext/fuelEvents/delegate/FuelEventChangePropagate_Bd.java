package atraxo.acc.business.ext.fuelEvents.delegate;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IInvoiceLineService;
import atraxo.acc.business.api.invoices.IInvoiceService;
import atraxo.acc.domain.impl.acc_type.CheckStatus;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author apetho
 */
public class FuelEventChangePropagate_Bd extends AbstractBusinessDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(FuelEventChangePropagate_Bd.class);

	public void propagateChange(FuelEvent fuelEvent) throws BusinessException {
		Collection<DeliveryNote> notes = fuelEvent.getDeliveryNotes();
		DeliveryNote dn = this.getInvoiceDeliveryNote(notes);
		if (dn == null) {
			return;
		}
		this.modifyInvoiceStatus(dn);
	}

	public void propagateChange(Integer fuelEventId) throws BusinessException {
		IFuelEventService fuelEventService = (IFuelEventService) this.findEntityService(FuelEvent.class);
		FuelEvent fuelEvent = fuelEventService.findById(fuelEventId);
		this.propagateChange(fuelEvent);
	}

	public void propagateChange(DeliveryNote deliveryNote) throws BusinessException {
		if (QuantitySource._INVOICE_.equals(deliveryNote.getSource())) {
			return;
		}
		this.propagateChange(deliveryNote.getFuelEvent());
	}

	/**
	 * @param objectId - fuel ticket or flight event id
	 * @param objectType
	 * @throws BusinessException
	 */
	public void propagateChange(Integer objectId, String objectType) throws BusinessException {
		IDeliveryNoteService dnService = (IDeliveryNoteService) this.findEntityService(DeliveryNote.class);
		List<DeliveryNote> list = dnService.findByObjectIdAndType(objectId, objectType);
		for (DeliveryNote dn : list) {
			this.propagateChange(dn);
		}

	}

	private DeliveryNote getInvoiceDeliveryNote(Collection<DeliveryNote> notes) {
		for (DeliveryNote dn : notes) {
			if (QuantitySource._INVOICE_.equals(dn.getSource())) {
				return dn;
			}
		}
		return null;
	}

	private void modifyInvoiceStatus(DeliveryNote dn) throws BusinessException {
		if (dn == null || !QuantitySource._INVOICE_.equals(dn.getSource())) {
			return;
		}
		IInvoiceLineService invoiceLineService = (IInvoiceLineService) this.findEntityService(InvoiceLine.class);
		IInvoiceService invoiceService = (IInvoiceService) this.findEntityService(Invoice.class);
		InvoiceLine invoiceLine = invoiceLineService.findById(dn.getObjectId());
		if (CheckStatus._NEEDS_RECHECK_.equals(invoiceLine.getOverAllCheckStatus())) {
			return;
		}
		Invoice invoice = invoiceLine.getInvoice();
		invoiceLine.setOverAllCheckStatus(CheckStatus._NEEDS_RECHECK_);
		invoiceLineService.updateWithoutBusiness(invoiceLine);
		invoice.setStatus(BillStatus._NEEDS_RECHECK_);
		invoiceService.updateWithoutBusiness(invoice);

	}

}
