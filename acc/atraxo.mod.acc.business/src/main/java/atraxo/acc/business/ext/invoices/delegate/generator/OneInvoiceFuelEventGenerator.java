/**
 *
 */
package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.utils.DateUtils;

/**
 * @author vhojda
 */
public class OneInvoiceFuelEventGenerator extends AbstractFuelEventGenerator {

	/**
	 *
	 */
	public OneInvoiceFuelEventGenerator() {
		// The explicit default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#filterByFrequency(java.util.Collection)
	 */
	@Override
	public void filterByFrequency(Collection<FuelEvent> events) {
		Date presentDate = new Date();
		for (Iterator<FuelEvent> it = events.iterator(); it.hasNext();) {
			FuelEvent event = it.next();
			// if in the future of in present day then remove it
			if (DateUtils.compareDates(event.getFuelingDate(), presentDate, true) >= 0) {
				it.remove();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#splitByFrequency(java.util.List)
	 */
	@Override
	public Map<InvoicingDatePeriod, List<FuelEvent>> splitByFrequency(List<FuelEvent> events) {
		Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap = new HashMap<>();
		FuelEvent firstFuelEvent = events.get(0);
		FuelEvent lastFuelEvent = events.get(events.size() - 1);
		eventsMap.put(new InvoicingDatePeriod(firstFuelEvent.getFuelingDate(), lastFuelEvent.getFuelingDate(), false),events );
		return eventsMap;
	}

}
