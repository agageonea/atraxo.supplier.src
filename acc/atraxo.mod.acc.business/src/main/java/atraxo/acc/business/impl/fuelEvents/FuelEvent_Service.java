/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.fuelEvents;

import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelEvent} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelEvent_Service extends AbstractEntityService<FuelEvent> {

	/**
	 * Public constructor for FuelEvent_Service
	 */
	public FuelEvent_Service() {
		super();
	}

	/**
	 * Public constructor for FuelEvent_Service
	 * 
	 * @param em
	 */
	public FuelEvent_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelEvent> getEntityClass() {
		return FuelEvent.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelEvent
	 */
	public FuelEvent findByKey(Date fuelingDate, Customer customer,
			Locations departure, Aircraft aircraft, String flightNumber,
			Suffix suffix, String ticketNumber) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelEvent.NQ_FIND_BY_KEY, FuelEvent.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("fuelingDate", fuelingDate)
					.setParameter("customer", customer)
					.setParameter("departure", departure)
					.setParameter("aircraft", aircraft)
					.setParameter("flightNumber", flightNumber)
					.setParameter("suffix", suffix)
					.setParameter("ticketNumber", ticketNumber)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(
							J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelEvent",
							"fuelingDate, customer, departure, aircraft, flightNumber, suffix, ticketNumber"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelEvent",
							"fuelingDate, customer, departure, aircraft, flightNumber, suffix, ticketNumber"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelEvent
	 */
	public FuelEvent findByKey(Date fuelingDate, Long customerId,
			Long departureId, Long aircraftId, String flightNumber,
			Suffix suffix, String ticketNumber) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelEvent.NQ_FIND_BY_KEY_PRIMITIVE,
							FuelEvent.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("fuelingDate", fuelingDate)
					.setParameter("customerId", customerId)
					.setParameter("departureId", departureId)
					.setParameter("aircraftId", aircraftId)
					.setParameter("flightNumber", flightNumber)
					.setParameter("suffix", suffix)
					.setParameter("ticketNumber", ticketNumber)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(
							J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelEvent",
							"fuelingDate, customerId, departureId, aircraftId, flightNumber, suffix, ticketNumber"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelEvent",
							"fuelingDate, customerId, departureId, aircraftId, flightNumber, suffix, ticketNumber"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelEvent
	 */
	public FuelEvent findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelEvent.NQ_FIND_BY_BUSINESS,
							FuelEvent.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelEvent", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelEvent", "id"), nure);
		}
	}

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.customer.id = :customerId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: departure
	 *
	 * @param departure
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDeparture(Locations departure) {
		return this.findByDepartureId(departure.getId());
	}
	/**
	 * Find by ID of reference: departure.id
	 *
	 * @param departureId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDepartureId(Integer departureId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.departure.id = :departureId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("departureId", departureId).getResultList();
	}
	/**
	 * Find by reference: aircraft
	 *
	 * @param aircraft
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByAircraft(Aircraft aircraft) {
		return this.findByAircraftId(aircraft.getId());
	}
	/**
	 * Find by ID of reference: aircraft.id
	 *
	 * @param aircraftId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByAircraftId(Integer aircraftId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.aircraft.id = :aircraftId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("aircraftId", aircraftId).getResultList();
	}
	/**
	 * Find by reference: fuelSupplier
	 *
	 * @param fuelSupplier
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByFuelSupplier(Suppliers fuelSupplier) {
		return this.findByFuelSupplierId(fuelSupplier.getId());
	}
	/**
	 * Find by ID of reference: fuelSupplier.id
	 *
	 * @param fuelSupplierId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByFuelSupplierId(Integer fuelSupplierId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.fuelSupplier.id = :fuelSupplierId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelSupplierId", fuelSupplierId).getResultList();
	}
	/**
	 * Find by reference: iplAgent
	 *
	 * @param iplAgent
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByIplAgent(Suppliers iplAgent) {
		return this.findByIplAgentId(iplAgent.getId());
	}
	/**
	 * Find by ID of reference: iplAgent.id
	 *
	 * @param iplAgentId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByIplAgentId(Integer iplAgentId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.iplAgent.id = :iplAgentId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("iplAgentId", iplAgentId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.unit.id = :unitId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: payableCostCurrency
	 *
	 * @param payableCostCurrency
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByPayableCostCurrency(
			Currencies payableCostCurrency) {
		return this.findByPayableCostCurrencyId(payableCostCurrency.getId());
	}
	/**
	 * Find by ID of reference: payableCostCurrency.id
	 *
	 * @param payableCostCurrencyId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByPayableCostCurrencyId(
			Integer payableCostCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.payableCostCurrency.id = :payableCostCurrencyId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("payableCostCurrencyId", payableCostCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: billableCostCurrency
	 *
	 * @param billableCostCurrency
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByBillableCostCurrency(
			Currencies billableCostCurrency) {
		return this.findByBillableCostCurrencyId(billableCostCurrency.getId());
	}
	/**
	 * Find by ID of reference: billableCostCurrency.id
	 *
	 * @param billableCostCurrencyId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByBillableCostCurrencyId(
			Integer billableCostCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.billableCostCurrency.id = :billableCostCurrencyId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("billableCostCurrencyId", billableCostCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: netUpliftUnit
	 *
	 * @param netUpliftUnit
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByNetUpliftUnit(Unit netUpliftUnit) {
		return this.findByNetUpliftUnitId(netUpliftUnit.getId());
	}
	/**
	 * Find by ID of reference: netUpliftUnit.id
	 *
	 * @param netUpliftUnitId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByNetUpliftUnitId(Integer netUpliftUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.netUpliftUnit.id = :netUpliftUnitId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("netUpliftUnitId", netUpliftUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: contractHolder
	 *
	 * @param contractHolder
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByContractHolder(Customer contractHolder) {
		return this.findByContractHolderId(contractHolder.getId());
	}
	/**
	 * Find by ID of reference: contractHolder.id
	 *
	 * @param contractHolderId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByContractHolderId(Integer contractHolderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.contractHolder.id = :contractHolderId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractHolderId", contractHolderId)
				.getResultList();
	}
	/**
	 * Find by reference: shipTo
	 *
	 * @param shipTo
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByShipTo(Customer shipTo) {
		return this.findByShipToId(shipTo.getId());
	}
	/**
	 * Find by ID of reference: shipTo.id
	 *
	 * @param shipToId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByShipToId(Integer shipToId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.shipTo.id = :shipToId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("shipToId", shipToId).getResultList();
	}
	/**
	 * Find by reference: densityUnit
	 *
	 * @param densityUnit
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDensityUnit(Unit densityUnit) {
		return this.findByDensityUnitId(densityUnit.getId());
	}
	/**
	 * Find by ID of reference: densityUnit.id
	 *
	 * @param densityUnitId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDensityUnitId(Integer densityUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.densityUnit.id = :densityUnitId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("densityUnitId", densityUnitId).getResultList();
	}
	/**
	 * Find by reference: densityVolume
	 *
	 * @param densityVolume
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDensityVolume(Unit densityVolume) {
		return this.findByDensityVolumeId(densityVolume.getId());
	}
	/**
	 * Find by ID of reference: densityVolume.id
	 *
	 * @param densityVolumeId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDensityVolumeId(Integer densityVolumeId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEvent e where e.clientId = :clientId and e.densityVolume.id = :densityVolumeId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("densityVolumeId", densityVolumeId)
				.getResultList();
	}
	/**
	 * Find by reference: deliveryNotes
	 *
	 * @param deliveryNotes
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDeliveryNotes(DeliveryNote deliveryNotes) {
		return this.findByDeliveryNotesId(deliveryNotes.getId());
	}
	/**
	 * Find by ID of reference: deliveryNotes.id
	 *
	 * @param deliveryNotesId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDeliveryNotesId(Integer deliveryNotesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelEvent e, IN (e.deliveryNotes) c where e.clientId = :clientId and c.id = :deliveryNotesId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("deliveryNotesId", deliveryNotesId)
				.getResultList();
	}
	/**
	 * Find by reference: payableAccruals
	 *
	 * @param payableAccruals
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByPayableAccruals(PayableAccrual payableAccruals) {
		return this.findByPayableAccrualsId(payableAccruals.getId());
	}
	/**
	 * Find by ID of reference: payableAccruals.id
	 *
	 * @param payableAccrualsId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByPayableAccrualsId(Integer payableAccrualsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelEvent e, IN (e.payableAccruals) c where e.clientId = :clientId and c.id = :payableAccrualsId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("payableAccrualsId", payableAccrualsId)
				.getResultList();
	}
	/**
	 * Find by reference: receivableAccruals
	 *
	 * @param receivableAccruals
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByReceivableAccruals(
			ReceivableAccrual receivableAccruals) {
		return this.findByReceivableAccrualsId(receivableAccruals.getId());
	}
	/**
	 * Find by ID of reference: receivableAccruals.id
	 *
	 * @param receivableAccrualsId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByReceivableAccrualsId(
			Integer receivableAccrualsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelEvent e, IN (e.receivableAccruals) c where e.clientId = :clientId and c.id = :receivableAccrualsId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("receivableAccrualsId", receivableAccrualsId)
				.getResultList();
	}
	/**
	 * Find by reference: details
	 *
	 * @param details
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDetails(FuelEventsDetails details) {
		return this.findByDetailsId(details.getId());
	}
	/**
	 * Find by ID of reference: details.id
	 *
	 * @param detailsId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDetailsId(Integer detailsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelEvent e, IN (e.details) c where e.clientId = :clientId and c.id = :detailsId",
						FuelEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("detailsId", detailsId).getResultList();
	}
}
