package atraxo.acc.business.ext.invoices.job.upload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.domain.impl.acc_type.InvoiceExportStatus;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.fmbas.business.api.job.IActionService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.ftp.FtpCommunicationResult;
import atraxo.fmbas.business.ext.ftp.SessionFactoryBuilder;
import atraxo.fmbas.business.ext.ftp.job.bean.FtpPutFilesBean;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.job.Action;
import atraxo.fmbas.domain.impl.job.ActionParameter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.business.service.storage.StorageService;
import seava.j4e.domain.batchjob.JobParameter;

/**
 * Spring batch tasklet to put files to a remote ftp/sftp server.
 *
 * @author aradu
 */
public class FTPUploadFilesTasklet implements Tasklet, ApplicationContextAware {

	private static final String SLASH = "/";

	private static final Logger LOGGER = LoggerFactory.getLogger(FTPUploadFilesTasklet.class);

	@Autowired
	private IActionService actionSrv;
	@Autowired
	private StorageService storageService;
	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;

	private ApplicationContext appContext;

	private enum ParamName {
		HOST("HOST"),
		USERNAME("USERNAME"),
		PASSWORD("PASSWORD"),
		CUSTOMERS("CUSTOMER(S)"),
		FTPDESTINATION("FTPDESTINATION"),
		SECURED("SECURED"),
		FILEPATTERN("FILEPATTERN"),
		KEYNAME("KEYNAME"),
		PORT("PORT");

		private String value;

		private ParamName(String value) {
			this.value = value;
		}

		private String getValue() {
			return this.value;
		}
	}

	@Override
	@Transactional
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		Map<ParamName, String> paramMap = this.initializeParameters(chunkContext); // get's the parameters for this job
		this.exportInvoices(chunkContext, paramMap); // the method that does all the work. It "eats" a list of Customers ID's separated by
		return RepeatStatus.FINISHED;
	}

	/**
	 * @param customers
	 * @throws BusinessException
	 * @throws IOException
	 * @throws Exception
	 */

	private void exportInvoices(ChunkContext chunkContext, Map<ParamName, String> paramMap) throws BusinessException, IOException {
		ExecutionContext context = chunkContext.getStepContext().getStepExecution().getExecutionContext();
		int successfullyTransfered = 0;
		int unsuccessfullyTransfered = 0;
		StringBuilder sbLog = new StringBuilder();

		Resource resource = null;
		boolean secured = "yes".equalsIgnoreCase(paramMap.get(ParamName.SECURED));
		if (secured && paramMap.containsKey(ParamName.KEYNAME) && !StringUtils.isEmpty(paramMap.get(ParamName.KEYNAME))) {
			String refId = this.getActionParameterRefId(chunkContext, ParamName.KEYNAME);
			resource = this.createPrivateKeyResource(refId);
		}

		List<OutgoingInvoice> invoiceList = this.getOutgoingInvoices(paramMap.get(ParamName.CUSTOMERS));

		String destination = paramMap.get(ParamName.FTPDESTINATION);
		if (destination.startsWith(SLASH)) {
			destination = destination.substring(1);
		}
		if (!destination.endsWith(SLASH)) {
			destination = destination.concat(SLASH);
		}
		FtpPutFilesBean bean = (FtpPutFilesBean) this.appContext.getBean("ftpPutFiles",
				SessionFactoryBuilder.getInstance().buildSessionFactory(paramMap.get(ParamName.HOST), paramMap.get(ParamName.USERNAME),
						paramMap.get(ParamName.PASSWORD), paramMap.get(ParamName.PORT), secured, resource),
				destination, paramMap.get(ParamName.FILEPATTERN));

		OutgoingInvoice[] array = invoiceList.toArray(new OutgoingInvoice[invoiceList.size()]);
		FtpCommunicationResult result = bean.execute(array);
		sbLog.append(result.getMessage());
		for (Entry<String, List<AbstractEntity>> entry : result.getMap().entrySet()) {
			switch (entry.getKey()) {
			case "SUCCESS":
				for (AbstractEntity entity : entry.getValue()) {
					OutgoingInvoice inv = (OutgoingInvoice) entity;
					this.archiveStoredInvoice(inv.getRefid());
					this.updateInvoiceExportStatus(inv, true);
					successfullyTransfered++;
				}
				break;
			case "FAILURE":
				for (AbstractEntity entity : entry.getValue()) {
					OutgoingInvoice inv = (OutgoingInvoice) entity;
					this.updateInvoiceExportStatus(inv, false);
					unsuccessfullyTransfered++;
				}
				break;
			default:
				break;
			}
		}
		if (successfullyTransfered != 0 || unsuccessfullyTransfered != 0) {
			context.put("successfullyTransfered", successfullyTransfered);
			context.put("unsuccessfullyTransfered", unsuccessfullyTransfered);
			context.put("logFileNameAndMessage", sbLog.toString());
		} else {
			context.put("logFileNameAndMessage", "No invoice was uploaded on the ftp server");
		}
		this.outgoingInvoiceService.update(invoiceList); // updates in database the Invoices
	}

	/**
	 * @param refId
	 * @param inputStreamForWebDavMovement
	 * @param folderPath
	 * @throws BusinessException
	 */
	private void archiveStoredInvoice(String refId) throws BusinessException {
		String archivePath = new StringBuilder("archive/").append(refId).toString();
		this.storageService.moveFile(refId, archivePath);
	}

	/**
	 * @param salesInvoice
	 * @param successfull
	 * @throws BusinessException
	 */
	private void updateInvoiceExportStatus(OutgoingInvoice salesInvoice, boolean successfull) throws BusinessException {
		if (successfull) {
			this.outgoingInvoiceService.markAsUploaded(salesInvoice);
		} else {
			salesInvoice.setExportStatus(InvoiceExportStatus._UPLOAD_FAILED_);
			salesInvoice.setExportDate(Calendar.getInstance().getTime());
		}
	}

	/**
	 * special method that converts wildCard pattern to Regex pattern
	 *
	 * @param wildcard
	 * @return
	 */
	public static String wildcardToRegex(String wildcard) {
		StringBuilder s = new StringBuilder(wildcard.length());
		s.append('^');
		for (int i = 0, is = wildcard.length(); i < is; i++) {
			char c = wildcard.charAt(i);
			switch (c) {
			case '*':
				s.append(".*");
				break;
			case '?':
				s.append(".");
				break;
			// escape special regexp-characters
			case '(':
			case ')':
			case '[':
			case ']':
			case '$':
			case '^':
			case '.':
			case '{':
			case '}':
			case '|':
			case '\\':
				s.append("\\");
				s.append(c);
				break;
			default:
				s.append(c);
				break;
			}
		}
		s.append('$');
		return s.toString();
	}

	/**
	 * Based on a list of customers ID's, returns the Customers who have Invoices with the status Exported If no customers are given, it will return
	 * all the invoices with status exported
	 *
	 * @param customers
	 * @return
	 * @throws BusinessException
	 */
	public List<OutgoingInvoice> getOutgoingInvoices(String customers) {
		Map<String, Object> params = new HashMap<>();
		params.put("exportStatus", Arrays.asList(InvoiceExportStatus._EXPORTED_, InvoiceExportStatus._UPLOAD_FAILED_));
		if (!customers.isEmpty()) {
			params.put("receiver", Arrays.asList(customers.split(",")));
		}
		return this.outgoingInvoiceService.findEntitiesByAttributes(params);
	}

	/**
	 * Retrieves the parameters and puts them in a EnumMap.
	 *
	 * @param chunkContext
	 * @return Map - Containing all the parameters
	 */
	private Map<ParamName, String> initializeParameters(ChunkContext chunkContext) {

		Map<ParamName, String> paramMap = new EnumMap<>(ParamName.class);
		paramMap.put(ParamName.CUSTOMERS, (String) chunkContext.getStepContext().getJobParameters().get(ParamName.CUSTOMERS.getValue()));
		paramMap.put(ParamName.FILEPATTERN, (String) chunkContext.getStepContext().getJobParameters().get(ParamName.FILEPATTERN.getValue()));
		paramMap.put(ParamName.FTPDESTINATION, (String) chunkContext.getStepContext().getJobParameters().get(ParamName.FTPDESTINATION.getValue()));
		paramMap.put(ParamName.USERNAME, (String) chunkContext.getStepContext().getJobParameters().get(ParamName.USERNAME.getValue()));
		paramMap.put(ParamName.HOST, (String) chunkContext.getStepContext().getJobParameters().get(ParamName.HOST.getValue()));
		paramMap.put(ParamName.PASSWORD, (String) chunkContext.getStepContext().getJobParameters().get(ParamName.PASSWORD.getValue()));
		paramMap.put(ParamName.SECURED, (String) chunkContext.getStepContext().getJobParameters().get(ParamName.SECURED.getValue()));
		paramMap.put(ParamName.PORT, (String) chunkContext.getStepContext().getJobParameters().get(ParamName.PORT.getValue()));
		paramMap.put(ParamName.KEYNAME, (String) chunkContext.getStepContext().getJobParameters().get(ParamName.KEYNAME.getValue()));
		return paramMap;
	}

	private Resource createPrivateKeyResource(String refId) throws BusinessException, IOException {
		final Path destination = Paths.get(
				seava.j4e.api.session.Session.user.get().getWorkspace().getTempPath() + File.separator + GUIDMessageGenerator.generateMessageID());
		try (final InputStream io = this.storageService.downloadFile(refId);) {
			if (destination.toFile().exists()) {
				Files.delete(destination);
			}
			Files.copy(io, destination);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
		return new FileSystemResource(destination.toFile());
	}

	private String getActionParameterRefId(ChunkContext chunkContext, ParamName name) throws BusinessException {
		Long actionId = (Long) chunkContext.getStepContext().getJobParameters().get(JobParameter.ACTION.name());
		Action action = this.actionSrv.findById(actionId.intValue());
		for (ActionParameter param : action.getActionParameters()) {
			if (name.toString().equals(param.getName())) {
				return param.getRefid();
			}
		}
		throw new BusinessException(BusinessErrorCode.FTP_KEY_NOT_FOUND, BusinessErrorCode.FTP_KEY_NOT_FOUND.getErrMsg());
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.appContext = applicationContext;

	}

}
