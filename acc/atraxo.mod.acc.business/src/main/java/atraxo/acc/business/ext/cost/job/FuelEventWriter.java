package atraxo.acc.business.ext.cost.job;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;

public class FuelEventWriter implements ItemWriter<List<FuelEvent>> {

	@Autowired
	private IFuelEventService srv;

	@Override
	public void write(List<? extends List<FuelEvent>> items) throws Exception {
		for (List<FuelEvent> list : items) {
			this.srv.update(list);
		}
	}
}
