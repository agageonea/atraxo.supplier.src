/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.ext.invoiceLines.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.invoiceLines.IInvoiceLineService;
import atraxo.acc.business.api.invoices.IInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.ext.invoiceLines.InvoiceLineTolerance;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.business.api.tolerance.IToleranceService;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.business.ext.tolerance.ToleranceResult;
import atraxo.fmbas.business.ext.tolerance.ToleranceVerifier;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link InvoiceLine} domain entity.
 */
public class InvoiceLine_Service extends atraxo.acc.business.impl.invoiceLines.InvoiceLine_Service implements IInvoiceLineService {

	@Autowired
	private UnitConverterService unitConverterService;
	@Autowired
	private IToleranceService toleranceService;
	@Autowired
	private IDeliveryNoteService deliveryNoteService;
	@Autowired
	private IInvoiceService invoiceService;

	@Override
	protected void postUpdate(InvoiceLine e) throws BusinessException {
		super.postUpdate(e);
		this.extendInvoicePeriod(e);
	}

	@Override
	protected void postInsert(InvoiceLine e) throws BusinessException {
		super.postInsert(e);
		this.extendInvoicePeriod(e);
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		for (Object id : ids) {
			this.canDelete(id);
		}
	}

	private void canDelete(Object id) throws BusinessException {
		InvoiceLine il = this.findById(id);
		if (BillStatus._AWAITING_PAYMENT_.equals(il.getInvoice().getStatus()) || BillStatus._PAID_.equals(il.getInvoice().getStatus())
				|| BillStatus._CREDITED_.equals(il.getInvoice().getStatus())) {
			throw new BusinessException(AccErrorCode.INVOICE_LINE_DELETE_STATUS_NOT_DRAFT,
					AccErrorCode.INVOICE_LINE_DELETE_STATUS_NOT_DRAFT.getErrMsg());
		}
		List<DeliveryNote> list = this.deliveryNoteService.findByObjectIdAndType((Integer) id, InvoiceLine.class.getSimpleName());
		for (DeliveryNote dn : list) {
			if (OutgoingInvoiceStatus._AWAITING_PAYMENT_.equals(dn.getFuelEvent().getInvoiceStatus())
					|| OutgoingInvoiceStatus._PAID_.equals(dn.getFuelEvent().getInvoiceStatus())) {
				throw new BusinessException(AccErrorCode.INV_LINE_DEL_DELIVERY_NOTE_USED, AccErrorCode.INV_LINE_DEL_DELIVERY_NOTE_USED.getErrMsg());
			}
		}
	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		for (Object id : ids) {
			this.deliveryNoteService.deleteDeliveryNote((Integer) id, InvoiceLine.class.getSimpleName());
		}
	}

	/**
	 * @param e - InvoiceLine
	 * @throws BusinessException
	 */
	private void extendInvoicePeriod(InvoiceLine e) throws BusinessException {
		Invoice invoice = e.getInvoice();
		boolean isModified = false;
		if (invoice.getDeliveryDateFrom().after(e.getDeliveryDate())) {
			invoice.setDeliveryDateFrom(e.getDeliveryDate());
			isModified = true;
		}
		if (invoice.getDeliverydateTo().before(e.getDeliveryDate())) {
			invoice.setDeliverydateTo(e.getDeliveryDate());
			isModified = true;
		}
		if (isModified) {
			IInvoiceService invoiceService = (IInvoiceService) this.findEntityService(Invoice.class);
			invoiceService.update(invoice);
		}
	}

	@Override
	@Transactional
	@History(value = "Balance", type = InvoiceLine.class)
	public void balanceInvoice(InvoiceLine invoiceLine) throws BusinessException {
		Invoice invoice = invoiceLine.getInvoice();
		Collection<InvoiceLine> invoiceLines = invoice.getInvoiceLines();
		if (invoice.getTotalAmount() == null || invoice.getQuantity() == null) {
			return;
		}
		if (BigDecimal.ZERO.compareTo(invoice.getTotalAmount()) == 0 || BigDecimal.ZERO.compareTo(invoice.getQuantity()) == 0) {
			for (InvoiceLine il : invoiceLines) {
				InvoiceLine updatedInvoiceLine = this.findByRefid(il.getRefid());
				if (updatedInvoiceLine.getAmount() == null) {
					updatedInvoiceLine.setAmount(BigDecimal.ZERO);
				}
				updatedInvoiceLine.setQuantity(BigDecimal.ZERO);
				this.update(updatedInvoiceLine);
			}
		} else {
			BigDecimal totalAmount = BigDecimal.ZERO;
			BigDecimal totalQuantity = BigDecimal.ZERO;
			for (InvoiceLine il : invoiceLines) {
				totalAmount = totalAmount.add(il.getAmount() != null ? il.getAmount() : BigDecimal.ZERO);
				totalQuantity = totalQuantity.add(il.getQuantity() != null ? il.getQuantity() : BigDecimal.ZERO);
			}

			BigDecimal amountBalance = totalAmount.subtract(invoice.getTotalAmount());
			BigDecimal quantityBalance = totalQuantity.subtract(invoice.getQuantity());

			this.checkTolerance(invoice.getTotalAmount(), invoice.getQuantity(), totalAmount, totalQuantity, invoice.getUnit(), invoice.getCurrency(),
					invoice.getReceivingDate());

			BigDecimal amountUnit = amountBalance.divide(totalAmount, MathContext.DECIMAL64);
			BigDecimal quantityUnit = quantityBalance.divide(totalQuantity, MathContext.DECIMAL64);

			for (InvoiceLine il : invoiceLines) {
				BigDecimal amount = il.getAmount() != null ? il.getAmount() : BigDecimal.ZERO;
				BigDecimal quantity = il.getQuantity() != null ? il.getQuantity() : BigDecimal.ZERO;
				BigDecimal amountTemp = amount.subtract(amountUnit.multiply(amount));
				BigDecimal quantityTemp = quantity.subtract(quantityUnit.multiply(quantity));
				InvoiceLine updatedInvoiceLine = this.findByRefid(il.getRefid());
				updatedInvoiceLine.setAmount(amountTemp);
				updatedInvoiceLine.setQuantity(quantityTemp);
				this.update(updatedInvoiceLine);
			}
		}
	}

	private void checkTolerance(BigDecimal invoiceTotalAmount, BigDecimal invoiceTotalQuantity, BigDecimal totalAmount, BigDecimal totalQuantity,
			Unit unit, Currencies currency, Date date) throws BusinessException {
		ToleranceVerifier verifier = new ToleranceVerifier(this.unitConverterService, this.getBusinessDelegate(ExchangeRate_Bd.class),
				this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity());
		Tolerance toleranceQuantity = this.toleranceService.getTolerance(InvoiceLineTolerance.CHECK_INVOICE_QUANTITY_AUTO_BALANCE.name());
		Tolerance toleranceAmount = this.toleranceService.getTolerance(InvoiceLineTolerance.CHECK_INVOICE_AMOUNT_AUTO_BALANCE.name());
		ToleranceResult toleranceResultAmount = verifier.checkTolerance(invoiceTotalAmount, totalAmount, currency, currency, toleranceAmount, date);
		ToleranceResult toleranceResultQuantity = verifier.checkTolerance(invoiceTotalQuantity, totalQuantity, unit, unit, toleranceQuantity, date);
		switch (toleranceResultAmount.getType()) {
		case NOT_WITHIN:
			throw new BusinessException(AccErrorCode.INVOICE_LINE_AMOUNT_BALANCE_OUT_OF_TOLERANCE,
					AccErrorCode.INVOICE_LINE_AMOUNT_BALANCE_OUT_OF_TOLERANCE.getErrMsg());
		case WITHIN:
		case MARGIN:
		default:
			break;
		}
		switch (toleranceResultQuantity.getType()) {
		case NOT_WITHIN:
			throw new BusinessException(AccErrorCode.INVOICE_LINE_QUANTITY_BALANCE_OUT_OF_TOLERANCE,
					AccErrorCode.INVOICE_LINE_QUANTITY_BALANCE_OUT_OF_TOLERANCE.getErrMsg());
		case WITHIN:
		case MARGIN:
		default:
			break;
		}
	}

	@Override
	@Transactional
	public void updateWithoutBusiness(InvoiceLine invoiceLine) throws BusinessException {
		this.onUpdate(invoiceLine);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.api.invoiceLines.IInvoiceLineService#removeInvoiceLines(java.util.List)
	 */
	@Override
	@Transactional
	public void removeInvoiceLines(List<OutgoingInvoiceLine> outgoingInvoiceLines) throws BusinessException {
		List<String> ticketNumbers = outgoingInvoiceLines.stream().map(OutgoingInvoiceLine::getTicketNo).collect(Collectors.toList());
		List<String> invoiceCodes = outgoingInvoiceLines.stream().map(OutgoingInvoiceLine::getOutgoingInvoice).map(OutgoingInvoice::getInvoiceNo)
				.collect(Collectors.toList());
		List<Invoice> invoices = this.invoiceService.findByInvoiceNumbers(invoiceCodes);
		List<Object> ids = new ArrayList<>();
		for (Invoice invoice : invoices) {
			List<Integer> list = invoice.getInvoiceLines().stream().filter(s -> ticketNumbers.contains(s.getTicketNo())).map(InvoiceLine::getId)
					.collect(Collectors.toList());
			ids.addAll(list);
		}
		this.deleteByIds(ids);
	}

}
