package atraxo.acc.business.ext.fuelEvents.delegate;

import java.math.BigDecimal;

import org.springframework.util.CollectionUtils;

import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author zspeter
 */
public class DensityDelegate extends AbstractBusinessDelegate {

	/**
	 * @param fuelEvent
	 * @return density from the associated fuel ticket if any otherwise system density.
	 * @throws BusinessException
	 */
	public Density getDensity(FuelEvent fuelEvent) throws BusinessException {
		Density density = null;
		if (fuelEvent != null && !CollectionUtils.isEmpty(fuelEvent.getDeliveryNotes())) {
			density = this.getDensityFromFuelTicket(fuelEvent);
		}
		if (density == null) {
			ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
			IUnitService unitSrv = (IUnitService) this.findEntityService(Unit.class);
			String densityStr = paramSrv.getDensity();
			Unit dFromUnit = unitSrv.findByCode(paramSrv.getSysWeight());
			Unit dToUnit = unitSrv.findByCode(paramSrv.getSysVol());
			Double d = Double.valueOf(densityStr);
			density = new Density(dFromUnit, dToUnit, d);
		}
		return density;
	}

	private Density getDensityFromFuelTicket(FuelEvent fuelEvent) throws BusinessException {
		Density density = null;
		IFuelTicketService fuelTicketService = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		for (DeliveryNote dn : fuelEvent.getDeliveryNotes()) {
			if (dn != null && dn.getSource().equals(QuantitySource._FUEL_TICKET_)) {
				FuelTicket ft = fuelTicketService.findById(dn.getObjectId());
				if (ft != null && ft.getDensity() != null) {
					density = new Density(ft.getDensityUnit(), ft.getDensityVolume(), ft.getDensity().doubleValue());
				}
			}
		}
		return density;
	}

	public BigDecimal getInSystemQuantity(FuelEvent fuelEvent) throws BusinessException {
		return this.getInSystemQuantity(fuelEvent.getQuantity(), fuelEvent.getUnit(), this.getDensity(fuelEvent));
	}

	public BigDecimal getInSystemQuantity(BigDecimal quantity, Unit fromUnit, Density density) throws BusinessException {
		UnitConverterService unitConverterService = this.getApplicationContext().getBean(UnitConverterService.class);
		Unit toUnit = this.getSysVol();
		return unitConverterService.convert(fromUnit, toUnit, quantity, density);
	}

	public Unit getSysVol() throws BusinessException {
		IUnitService unitService = (IUnitService) this.findEntityService(Unit.class);
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		String sysVolume = paramSrv.getSysVol();
		return unitService.findByCode(sysVolume);
	}
}
