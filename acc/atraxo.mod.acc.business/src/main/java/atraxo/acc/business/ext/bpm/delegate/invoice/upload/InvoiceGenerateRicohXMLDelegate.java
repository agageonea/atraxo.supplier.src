package atraxo.acc.business.ext.bpm.delegate.invoice.upload;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.invoices.service.InvoiceToRicohXMLTransformer;
import atraxo.acc.domain.ftp.invoice.Document;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;
import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.commons.utils.XMLUtils;

public class InvoiceGenerateRicohXMLDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceGenerateRicohXMLDelegate.class);

	// Pattern: COMPANY_AVI_SAL_DocumentNumber_DocumentFormatCode
	private static final String DOCUMENT_NAME_PATTERN = "%s_AVI_%s_%s";

	@Autowired
	private InvoiceToRicohXMLTransformer transformer;

	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;

	@Autowired
	private ICustomerService customerService;

	@Autowired
	private IContractService contractService;

	@Override
	public void execute() throws Exception {
		LOGGER.info("Start xml generation");

		Integer invoiceId = (Integer) this.execution.getVariable(WorkflowVariablesConstants.INVOICE_ID_VAR);
		OutgoingInvoice invoice = this.outgoingInvoiceService.findById(invoiceId);
		Customer subsidiary = this.customerService.findByRefid(invoice.getSubsidiaryId());
		Contract contract = this.contractService.findById(invoice.getReferenceDocId());

		Document ricohDocument = this.transformer.generate(invoice);
		String ricohXML = XMLUtils.toString(ricohDocument, Document.class, true, true);

		String documentFormatCode = InvoiceTypeAcc._INV_.equals(invoice.getInvoiceType()) ? contract.getInvoiceType().getCode() : "NDC";
		String fileName = String.format(DOCUMENT_NAME_PATTERN, subsidiary.getCode(), invoice.getInvoiceNo(), documentFormatCode);

		this.execution.setVariable(WorkflowVariablesConstants.INVOICE_RICOH_XML, ricohXML);
		this.execution.setVariable(WorkflowVariablesConstants.INVOICE_RICOH_FILE_NAME, fileName);
		LOGGER.info("End xml generation");
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

}
