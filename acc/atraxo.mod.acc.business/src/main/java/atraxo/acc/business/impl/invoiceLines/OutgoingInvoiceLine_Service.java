/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.invoiceLines;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link OutgoingInvoiceLine} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class OutgoingInvoiceLine_Service
		extends
			AbstractEntityService<OutgoingInvoiceLine> {

	/**
	 * Public constructor for OutgoingInvoiceLine_Service
	 */
	public OutgoingInvoiceLine_Service() {
		super();
	}

	/**
	 * Public constructor for OutgoingInvoiceLine_Service
	 * 
	 * @param em
	 */
	public OutgoingInvoiceLine_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<OutgoingInvoiceLine> getEntityClass() {
		return OutgoingInvoiceLine.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoiceLine
	 */
	public OutgoingInvoiceLine findByKey(OutgoingInvoice outgoingInvoice,
			FuelEvent fuelEvent) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(OutgoingInvoiceLine.NQ_FIND_BY_KEY,
							OutgoingInvoiceLine.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("outgoingInvoice", outgoingInvoice)
					.setParameter("fuelEvent", fuelEvent).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"OutgoingInvoiceLine", "outgoingInvoice, fuelEvent"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"OutgoingInvoiceLine", "outgoingInvoice, fuelEvent"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoiceLine
	 */
	public OutgoingInvoiceLine findByKey(Long outgoingInvoiceId,
			Long fuelEventId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							OutgoingInvoiceLine.NQ_FIND_BY_KEY_PRIMITIVE,
							OutgoingInvoiceLine.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("outgoingInvoiceId", outgoingInvoiceId)
					.setParameter("fuelEventId", fuelEventId).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"OutgoingInvoiceLine",
							"outgoingInvoiceId, fuelEventId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"OutgoingInvoiceLine",
							"outgoingInvoiceId, fuelEventId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoiceLine
	 */
	public OutgoingInvoiceLine findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(OutgoingInvoiceLine.NQ_FIND_BY_BUSINESS,
							OutgoingInvoiceLine.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"OutgoingInvoiceLine", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"OutgoingInvoiceLine", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByFuelEvent(FuelEvent fuelEvent) {
		return this.findByFuelEventId(fuelEvent.getId());
	}
	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByFuelEventId(Integer fuelEventId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLine e where e.clientId = :clientId and e.fuelEvent.id = :fuelEventId",
						OutgoingInvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelEventId", fuelEventId).getResultList();
	}
	/**
	 * Find by reference: outgoingInvoice
	 *
	 * @param outgoingInvoice
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByOutgoingInvoice(
			OutgoingInvoice outgoingInvoice) {
		return this.findByOutgoingInvoiceId(outgoingInvoice.getId());
	}
	/**
	 * Find by ID of reference: outgoingInvoice.id
	 *
	 * @param outgoingInvoiceId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByOutgoingInvoiceId(
			Integer outgoingInvoiceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLine e where e.clientId = :clientId and e.outgoingInvoice.id = :outgoingInvoiceId",
						OutgoingInvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("outgoingInvoiceId", outgoingInvoiceId)
				.getResultList();
	}
	/**
	 * Find by reference: referenceInvItem
	 *
	 * @param referenceInvItem
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByReferenceInvItem(
			OutgoingInvoice referenceInvItem) {
		return this.findByReferenceInvItemId(referenceInvItem.getId());
	}
	/**
	 * Find by ID of reference: referenceInvItem.id
	 *
	 * @param referenceInvItemId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByReferenceInvItemId(
			Integer referenceInvItemId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLine e where e.clientId = :clientId and e.referenceInvItem.id = :referenceInvItemId",
						OutgoingInvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("referenceInvItemId", referenceInvItemId)
				.getResultList();
	}
	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLine e where e.clientId = :clientId and e.customer.id = :customerId",
						OutgoingInvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: destination
	 *
	 * @param destination
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByDestination(Locations destination) {
		return this.findByDestinationId(destination.getId());
	}
	/**
	 * Find by ID of reference: destination.id
	 *
	 * @param destinationId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByDestinationId(Integer destinationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLine e where e.clientId = :clientId and e.destination.id = :destinationId",
						OutgoingInvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("destinationId", destinationId).getResultList();
	}
	/**
	 * Find by reference: referenceInvoiceLine
	 *
	 * @param referenceInvoiceLine
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByReferenceInvoiceLine(
			OutgoingInvoiceLine referenceInvoiceLine) {
		return this.findByReferenceInvoiceLineId(referenceInvoiceLine.getId());
	}
	/**
	 * Find by ID of reference: referenceInvoiceLine.id
	 *
	 * @param referenceInvoiceLineId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByReferenceInvoiceLineId(
			Integer referenceInvoiceLineId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLine e where e.clientId = :clientId and e.referenceInvoiceLine.id = :referenceInvoiceLineId",
						OutgoingInvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("referenceInvoiceLineId", referenceInvoiceLineId)
				.getResultList();
	}
	/**
	 * Find by reference: details
	 *
	 * @param details
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByDetails(
			OutgoingInvoiceLineDetails details) {
		return this.findByDetailsId(details.getId());
	}
	/**
	 * Find by ID of reference: details.id
	 *
	 * @param detailsId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByDetailsId(Integer detailsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from OutgoingInvoiceLine e, IN (e.details) c where e.clientId = :clientId and c.id = :detailsId",
						OutgoingInvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("detailsId", detailsId).getResultList();
	}
}
