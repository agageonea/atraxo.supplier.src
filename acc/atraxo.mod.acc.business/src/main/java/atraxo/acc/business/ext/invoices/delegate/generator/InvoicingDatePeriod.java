package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.RandomStringUtils;

/**
 * Class representing a period of days in which the invoices are created
 *
 * @author vhojda
 */
public class InvoicingDatePeriod {

	private Calendar startPeriod;

	private Calendar endPeriod;

	private String timestamp;

	/**
	 *
	 */
	public InvoicingDatePeriod() {

	}

	/**
	 * @param startPeriodDate
	 * @param endPeriodDate
	 * @param timestamp
	 */
	public InvoicingDatePeriod(Date startPeriodDate, Date endPeriodDate, boolean timestamp) {
		this.startPeriod = Calendar.getInstance();
		this.startPeriod.setTime(startPeriodDate);
		this.endPeriod = Calendar.getInstance();
		this.endPeriod.setTime(endPeriodDate);
		if (timestamp) {
			this.timestamp = this.generateUniqueRandomKey();
		}
	}

	/**
	 * @param day
	 */
	public InvoicingDatePeriod(Date day) {
		this(day, day, false);
	}

	/**
	 * @param day
	 * @param timestamp
	 */
	public InvoicingDatePeriod(Date day, boolean timestamp) {
		this(day, day, timestamp);
	}

	/**
	 * @param startPeriod
	 * @param endPeriod
	 */
	public InvoicingDatePeriod(Calendar startPeriod, Calendar endPeriod) {
		this(startPeriod, endPeriod, false);

	}

	/**
	 * @param startPeriod
	 * @param endPeriod
	 * @param timestamp
	 */
	public InvoicingDatePeriod(Calendar startPeriod, Calendar endPeriod, boolean timestamp) {
		this.startPeriod = new GregorianCalendar(startPeriod.get(Calendar.YEAR), startPeriod.get(Calendar.MONTH),
				startPeriod.get(Calendar.DAY_OF_MONTH));
		this.endPeriod = new GregorianCalendar(endPeriod.get(Calendar.YEAR), endPeriod.get(Calendar.MONTH), endPeriod.get(Calendar.DAY_OF_MONTH));
		if (timestamp) {
			this.timestamp = this.generateUniqueRandomKey();
		}
	}

	/**
	 * @param day
	 */
	public InvoicingDatePeriod(Calendar day) {
		this(day, day);
	}

	public Calendar getStartPeriod() {
		return this.startPeriod;
	}

	public void setStartPeriod(Calendar startPeriod) {
		this.startPeriod = startPeriod;
	}

	public Calendar getEndPeriod() {
		return this.endPeriod;
	}

	public void setEndPeriod(Calendar endPeriod) {
		this.endPeriod = endPeriod;
	}

	public Calendar getUniqueDay() {
		Calendar cal = null;
		if (this.startPeriod.equals(this.endPeriod)) {
			cal = this.startPeriod;
		}
		return cal;
	}

	public String getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.endPeriod == null) ? 0 : this.endPeriod.hashCode());
		result = prime * result + ((this.startPeriod == null) ? 0 : this.startPeriod.hashCode());
		result = prime * result + ((this.timestamp == null) ? 0 : this.timestamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		InvoicingDatePeriod other = (InvoicingDatePeriod) obj;
		if (this.endPeriod == null) {
			if (other.endPeriod != null) {
				return false;
			}
		} else if (!this.endPeriod.equals(other.endPeriod)) {
			return false;
		}
		if (this.startPeriod == null) {
			if (other.startPeriod != null) {
				return false;
			}
		} else if (!this.startPeriod.equals(other.startPeriod)) {
			return false;
		}
		if (this.timestamp == null) {
			if (other.timestamp != null) {
				return false;
			}
		} else if (!this.timestamp.equals(other.timestamp)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "InvoicingDatePeriod [startPeriod=" + this.startPeriod + ", endPeriod=" + this.endPeriod + ", timestamp=" + this.timestamp + "]";
	}

	/**
	 * @return
	 */
	private String generateUniqueRandomKey() {
		return System.currentTimeMillis() + RandomStringUtils.randomAlphanumeric(50);
	}

}
