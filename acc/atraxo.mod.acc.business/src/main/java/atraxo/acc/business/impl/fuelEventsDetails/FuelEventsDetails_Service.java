/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.fuelEventsDetails;

import atraxo.acc.business.api.fuelEventsDetails.IFuelEventsDetailsService;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelEventsDetails} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelEventsDetails_Service
		extends
			AbstractEntityService<FuelEventsDetails>
		implements
			IFuelEventsDetailsService {

	/**
	 * Public constructor for FuelEventsDetails_Service
	 */
	public FuelEventsDetails_Service() {
		super();
	}

	/**
	 * Public constructor for FuelEventsDetails_Service
	 * 
	 * @param em
	 */
	public FuelEventsDetails_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelEventsDetails> getEntityClass() {
		return FuelEventsDetails.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelEventsDetails
	 */
	public FuelEventsDetails findByKey(FuelEvent fuelEvent,
			String contractCode, PriceCategory priceCategory) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelEventsDetails.NQ_FIND_BY_KEY,
							FuelEventsDetails.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("fuelEvent", fuelEvent)
					.setParameter("contractCode", contractCode)
					.setParameter("priceCategory", priceCategory)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelEventsDetails",
							"fuelEvent, contractCode, priceCategory"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelEventsDetails",
							"fuelEvent, contractCode, priceCategory"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelEventsDetails
	 */
	public FuelEventsDetails findByKey(Long fuelEventId, String contractCode,
			Long priceCategoryId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							FuelEventsDetails.NQ_FIND_BY_KEY_PRIMITIVE,
							FuelEventsDetails.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("fuelEventId", fuelEventId)
					.setParameter("contractCode", contractCode)
					.setParameter("priceCategoryId", priceCategoryId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelEventsDetails",
							"fuelEventId, contractCode, priceCategoryId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelEventsDetails",
							"fuelEventId, contractCode, priceCategoryId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelEventsDetails
	 */
	public FuelEventsDetails findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelEventsDetails.NQ_FIND_BY_BUSINESS,
							FuelEventsDetails.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelEventsDetails", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelEventsDetails", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByFuelEvent(FuelEvent fuelEvent) {
		return this.findByFuelEventId(fuelEvent.getId());
	}
	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByFuelEventId(Integer fuelEventId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEventsDetails e where e.clientId = :clientId and e.fuelEvent.id = :fuelEventId",
						FuelEventsDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelEventId", fuelEventId).getResultList();
	}
	/**
	 * Find by reference: priceCategory
	 *
	 * @param priceCategory
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByPriceCategory(
			PriceCategory priceCategory) {
		return this.findByPriceCategoryId(priceCategory.getId());
	}
	/**
	 * Find by ID of reference: priceCategory.id
	 *
	 * @param priceCategoryId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByPriceCategoryId(Integer priceCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEventsDetails e where e.clientId = :clientId and e.priceCategory.id = :priceCategoryId",
						FuelEventsDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceCategoryId", priceCategoryId)
				.getResultList();
	}
	/**
	 * Find by reference: mainCategory
	 *
	 * @param mainCategory
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByMainCategory(MainCategory mainCategory) {
		return this.findByMainCategoryId(mainCategory.getId());
	}
	/**
	 * Find by ID of reference: mainCategory.id
	 *
	 * @param mainCategoryId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByMainCategoryId(Integer mainCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEventsDetails e where e.clientId = :clientId and e.mainCategory.id = :mainCategoryId",
						FuelEventsDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("mainCategoryId", mainCategoryId).getResultList();
	}
	/**
	 * Find by reference: originalPriceCurrency
	 *
	 * @param originalPriceCurrency
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByOriginalPriceCurrency(
			Currencies originalPriceCurrency) {
		return this
				.findByOriginalPriceCurrencyId(originalPriceCurrency.getId());
	}
	/**
	 * Find by ID of reference: originalPriceCurrency.id
	 *
	 * @param originalPriceCurrencyId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByOriginalPriceCurrencyId(
			Integer originalPriceCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEventsDetails e where e.clientId = :clientId and e.originalPriceCurrency.id = :originalPriceCurrencyId",
						FuelEventsDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("originalPriceCurrencyId",
						originalPriceCurrencyId).getResultList();
	}
	/**
	 * Find by reference: originalPriceUnit
	 *
	 * @param originalPriceUnit
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByOriginalPriceUnit(
			Unit originalPriceUnit) {
		return this.findByOriginalPriceUnitId(originalPriceUnit.getId());
	}
	/**
	 * Find by ID of reference: originalPriceUnit.id
	 *
	 * @param originalPriceUnitId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByOriginalPriceUnitId(
			Integer originalPriceUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelEventsDetails e where e.clientId = :clientId and e.originalPriceUnit.id = :originalPriceUnitId",
						FuelEventsDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("originalPriceUnitId", originalPriceUnitId)
				.getResultList();
	}
}
