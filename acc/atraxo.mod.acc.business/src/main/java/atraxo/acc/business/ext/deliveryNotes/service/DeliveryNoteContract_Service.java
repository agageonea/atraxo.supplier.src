/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.ext.deliveryNotes.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteContractService;
import atraxo.acc.business.api.payableAccruals.IPayableAccrualService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.cmm.domain.impl.contracts.Contract;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;

/**
 * Business extensions specific for {@link DeliveryNoteContract} domain entity.
 */
public class DeliveryNoteContract_Service extends atraxo.acc.business.impl.deliveryNotes.DeliveryNoteContract_Service
		implements IDeliveryNoteContractService {

	private static final Logger LOG = LoggerFactory.getLogger(DeliveryNoteContract_Service.class);

	@Override
	public List<DeliveryNoteContract> findByContract(Contract c) throws BusinessException {

		Map<String, Object> params = new HashMap<>();
		params.put("contracts", c);
		return this.findEntitiesByAttributes(params);
	}

	@Override
	protected void postDelete(DeliveryNoteContract e) throws BusinessException {
		super.postDelete(e);
		try {
			IPayableAccrualService payableAccrualService = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
			PayableAccrual pa = payableAccrualService.findByFuelEventContract(e.getContracts(), e.getDeliveryNotes().getFuelEvent());
			payableAccrualService.deleteById(pa.getId());
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOG.warn("No accrual payable found.", ex);
			} else if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(ex.getErrorCode())) {
				throw new BusinessException(AccErrorCode.PAYABLE_ACCRUALS_DUPLICATE_BUSINESS_KEY,
						AccErrorCode.PAYABLE_ACCRUALS_DUPLICATE_BUSINESS_KEY.getErrMsg(), ex);
			}
		}
	}

}
