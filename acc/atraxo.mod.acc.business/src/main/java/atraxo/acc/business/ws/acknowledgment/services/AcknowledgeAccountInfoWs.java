package atraxo.acc.business.ws.acknowledgment.services;

import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
@WebService(targetNamespace = "http://services.acknowledgment.ws.business.acc.atraxo/", endpointInterface = "AcknowledgeAccountInfo")
public interface AcknowledgeAccountInfoWs {

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BusinessException
	 */
	@WebMethod(operationName = "acknowledgeFuelEventsNav", action = "acknowledgeFuelEventsNav")
	@WebResult(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse")
	MSG acknowledgeFuelEventsNav(
			@WebParam(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse") MSG request)
			throws JAXBException, SAXException, IOException, BusinessException;

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BusinessException
	 */
	@WebMethod(operationName = "acknowledgeInvoicesNav", action = "acknowledgeInvoicesNav")
	@WebResult(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse")
	MSG acknowledgeInvoicesNav(
			@WebParam(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse") MSG request)
			throws JAXBException, SAXException, IOException, BusinessException;

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BusinessException
	 */
	@WebMethod(operationName = "acknowledgeCreditMemosNav", action = "acknowledgeCreditMemosNav")
	@WebResult(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse")
	MSG acknowledgeCreditMemosNav(
			@WebParam(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse") MSG request)
			throws JAXBException, SAXException, IOException, BusinessException;

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BusinessException
	 */
	@WebMethod(operationName = "acknowledgeRicoh", action = "acknowledgeRicoh")
	@WebResult(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse")
	MSG acknowledgeRicoh(
			@WebParam(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse") MSG request)
			throws JAXBException, SAXException, IOException, BusinessException;
}
