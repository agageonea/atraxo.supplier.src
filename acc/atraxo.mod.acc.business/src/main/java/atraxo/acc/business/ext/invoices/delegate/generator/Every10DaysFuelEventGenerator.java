/**
 *
 */
package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.utils.DateUtils;

/**
 * @author vhojda
 */
public class Every10DaysFuelEventGenerator extends AbstractFuelEventGenerator {

	/**
	 *
	 */
	public Every10DaysFuelEventGenerator() {
		// The explicit default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#filterByFrequency(java.util.Collection)
	 */
	@Override
	public void filterByFrequency(Collection<FuelEvent> events) {
		Calendar presentCal = DateUtils.getPresentIgnoreHours();

		boolean firstPartOfTheMonth = presentCal.get(Calendar.DAY_OF_MONTH) <= 10;
		boolean secondPartOfTheMonth = !firstPartOfTheMonth && presentCal.get(Calendar.DAY_OF_MONTH) <= 20;

		int comparissonDay = 0;
		if (firstPartOfTheMonth) {
			comparissonDay = 1;
		} else if (secondPartOfTheMonth) {
			comparissonDay = 10;
		} else {
			comparissonDay = 20;
		}

		Calendar comparissonCal = new GregorianCalendar(presentCal.get(Calendar.YEAR), presentCal.get(Calendar.MONTH), comparissonDay);

		for (Iterator<FuelEvent> it = events.iterator(); it.hasNext();) {
			FuelEvent event = it.next();

			Calendar eventCal = DateUtils.getCalendarFromDateIgnoreHours(event.getFuelingDate());

			// if NOT in the correct interval then remove it
			if (eventCal.after(comparissonCal)) {
				it.remove();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#splitByFrequency(java.util.List)
	 */
	@Override
	public Map<InvoicingDatePeriod, List<FuelEvent>> splitByFrequency(List<FuelEvent> events) {
		Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap = new HashMap<>();

		if (!events.isEmpty()) {
			FuelEvent firstFuelEvent = events.get(0);
			FuelEvent lastFuelEvent = events.get(events.size() - 1);

			Calendar firstFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(firstFuelEvent.getFuelingDate());
			Calendar lastFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(lastFuelEvent.getFuelingDate());

			int firstMonth = firstFuelEventCal.get(Calendar.YEAR) * 12 + firstFuelEventCal.get(Calendar.MONTH);
			int lastMonth = lastFuelEventCal.get(Calendar.YEAR) * 12 + lastFuelEventCal.get(Calendar.MONTH);

			// for each month put 3 collections of events
			int diff = lastMonth - firstMonth;
			for (int i = 0; i <= diff; i++) {
				Calendar startDayMonthCal1 = DateUtils.getCalendarFromCalendarIgnoreHours(firstFuelEventCal);
				startDayMonthCal1.add(Calendar.MONTH, i);
				startDayMonthCal1.set(Calendar.DAY_OF_MONTH, 1);

				Calendar endDayMonthCal1 = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal1);
				endDayMonthCal1.set(Calendar.DAY_OF_MONTH, 10);

				Calendar startDayMonthCal2 = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal1);
				startDayMonthCal2.set(Calendar.DAY_OF_MONTH, 11);

				Calendar endDayMonthCal2 = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal1);
				endDayMonthCal2.set(Calendar.DAY_OF_MONTH, 20);

				Calendar startDayMonthCal3 = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal1);
				startDayMonthCal3.set(Calendar.DAY_OF_MONTH, 21);

				Calendar endDayMonthCal3 = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal1);
				endDayMonthCal3.set(Calendar.DAY_OF_MONTH, startDayMonthCal1.getActualMaximum(Calendar.DAY_OF_MONTH));

				eventsMap.put(new InvoicingDatePeriod(startDayMonthCal1, endDayMonthCal1), null);
				eventsMap.put(new InvoicingDatePeriod(startDayMonthCal2, endDayMonthCal2), null);
				eventsMap.put(new InvoicingDatePeriod(startDayMonthCal3, endDayMonthCal3), null);
			}
			// fill in the events map depending on the period with events
			this.fillFuelEventsPeriodMap(eventsMap, events);

		}
		return eventsMap;
	}

}
