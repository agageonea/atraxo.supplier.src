package atraxo.acc.business.ext.volumeUpdater;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.api.shipTo.IShipToService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;

public class FuelEventVolumeProcessor implements ItemProcessor<Map<FuelEvent, List<Contract>>, Map<Contract, Map<ShipTo, BigDecimal>>> {

	@Autowired
	ISystemParameterService sysParamService;
	@Autowired
	IFuelTicketService fuelTicketService;
	@Autowired
	IUnitService unitService;
	@Autowired
	IShipToService shipToService;

	private static final Logger LOG = LoggerFactory.getLogger(FuelEventVolumeProcessor.class);

	@Override
	public Map<Contract, Map<ShipTo, BigDecimal>> process(Map<FuelEvent, List<Contract>> item) throws Exception {
		Map<Contract, Map<ShipTo, BigDecimal>> finalMap = new HashMap<>();

		for (Entry<FuelEvent, List<Contract>> entry : item.entrySet()) {
			FuelEvent fuelEvent = entry.getKey();
			BigDecimal density = this.getDensity(fuelEvent);
			Unit fromUnit = fuelEvent.getUnit();
			Customer fuelEventShipTo = fuelEvent.getShipTo();
			for (Contract contract : entry.getValue()) {
				Unit toUnit = contract.getSettlementUnit();
				BigDecimal convertedQuantity = this.unitService.convert(fromUnit, toUnit, fuelEvent.getQuantity(), density.doubleValue());
				ShipTo shipTo = this.getShipTo(fuelEventShipTo, contract);
				this.addVolume(finalMap, contract, convertedQuantity, shipTo);
			}
		}
		return finalMap;
	}

	private void addVolume(Map<Contract, Map<ShipTo, BigDecimal>> finalMap, Contract contract, BigDecimal convertedQuantity, ShipTo shipTo) {
		if (finalMap.containsKey(contract)) {
			Map<ShipTo, BigDecimal> shipToMap = finalMap.get(contract);
			if (shipTo != null) {
				if (shipToMap.containsKey(shipTo)) {
					BigDecimal oldValue = shipToMap.get(shipTo);
					BigDecimal newValue = oldValue.add(convertedQuantity, MathContext.DECIMAL64);
					shipToMap.put(shipTo, newValue);
				} else {
					shipToMap.put(shipTo, convertedQuantity);
				}
			}
		} else {
			Map<ShipTo, BigDecimal> shipToMap = new HashMap<>();
			shipToMap.put(shipTo, convertedQuantity);
			finalMap.put(contract, shipToMap);
		}
	}

	private ShipTo getShipTo(Customer fuelEventShipTo, Contract contract) {
		ShipTo shipTo = null;
		for (ShipTo st : contract.getShipTo()) {
			if (st.getCustomer().equals(fuelEventShipTo)) {
				shipTo = st;
			}
		}
		return shipTo;
	}

	private BigDecimal getDensity(FuelEvent fuelEvent) throws BusinessException {
		DeliveryNote dn = this.getUsedDeliveryNote(fuelEvent);
		if (dn == null || !QuantitySource._FUEL_TICKET_.equals(dn.getSource())) {
			return new BigDecimal(this.sysParamService.getDensity());
		}
		BigDecimal density = null;
		try {
			FuelTicket fuelTicket = this.fuelTicketService.findByBusiness(dn.getObjectId());
			density = fuelTicket.getDensity();
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOG.warn("The referenced fuel ticket is missing!", ex);
			} else if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(ex.getErrorCode())) {
				throw new BusinessException(AccErrorCode.FUEL_TICKET_DUPLICATE_BUSINESS_KEY,
						AccErrorCode.FUEL_TICKET_DUPLICATE_BUSINESS_KEY.getErrMsg(), ex);

			}
		}
		return density == null ? new BigDecimal(this.sysParamService.getDensity()) : density;
	}

	private DeliveryNote getUsedDeliveryNote(FuelEvent fuelEvent) {
		if (CollectionUtils.isEmpty(fuelEvent.getDeliveryNotes())) {
			return null;
		}
		for (DeliveryNote dn : fuelEvent.getDeliveryNotes()) {
			if (dn.getUsed()) {
				return dn;
			}
		}
		return null;
	}
}
