package atraxo.acc.business.ws;

/**
 * Incapsulates the result of the "export fuel events" operation. It contains data like number of sucessfully exported events, total number of events
 * and so on.
 *
 * @author vhojda
 */
public class ExportFuelEventOperationResult {

	private int nrEventsExported;

	private int nrEventsTotal;

	/**
	 * Default constructor, assume nothing exported so far.
	 */
	public ExportFuelEventOperationResult() {
		this.nrEventsExported = 0;
		this.nrEventsTotal = 0;
	}

	public int getNrEventsExported() {
		return this.nrEventsExported;
	}

	public void setNrEventsExported(int nrEventsExported) {
		this.nrEventsExported = nrEventsExported;
	}

	public int getNrEventsTotal() {
		return this.nrEventsTotal;
	}

	public void setNrEventsTotal(int nrEventsTotal) {
		this.nrEventsTotal = nrEventsTotal;
	}

	public void incrementNrExported() {
		this.nrEventsExported++;
	}

	public void incrementNrTotal() {
		this.nrEventsTotal++;
	}

	public boolean attemptedExport() {
		return this.nrEventsTotal != 0;
	}

}
