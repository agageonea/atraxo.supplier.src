package atraxo.acc.business.ext.invoices.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.efaps.number2words.Converter;
import org.efaps.number2words.IConverter;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.domain.ftp.invoice.Document;
import atraxo.acc.domain.ftp.invoice.Document.Data;
import atraxo.acc.domain.ftp.invoice.Document.Data.Footer;
import atraxo.acc.domain.ftp.invoice.Document.Data.Header;
import atraxo.acc.domain.ftp.invoice.Document.Data.Lines;
import atraxo.acc.domain.ftp.invoice.Document.Data.Lines.Line;
import atraxo.acc.domain.ftp.invoice.Document.Data.Lines.Line.AdditionalCharges;
import atraxo.acc.domain.ftp.invoice.Document.Data.Lines.Line.AdditionalCharges.AdditionalCharge;
import atraxo.acc.domain.ftp.invoice.Document.DocumentInformation;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class InvoiceToRicohXMLTransformer extends AbstractBusinessBaseService {

	@Autowired
	private IContractService contractService;

	@Autowired
	private ICustomerService customerService;

	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	private DecimalFormat volumeDecimalFormat = new DecimalFormat("#");
	private DecimalFormat amountDecimalFormat = new DecimalFormat(".00");
	private DecimalFormat priceDecimalFormat = new DecimalFormat(".0000");

	private static final String NIT = "9447-231209-102-7";
	private static final String BUSINESS_SEGMENT = "AVI";
	private static final String CREDIT_MEMO_FORMAT_CODE = "NDC";
	private static final String LINE_OF_BUSINESS = "Otras Actividades de Servicios";

	public Document generate(OutgoingInvoice invoice) {
		Document doc = new Document();
		Contract contract = this.contractService.findById(invoice.getReferenceDocId());
		Boolean priceIncludesVAT = InvoiceType._EXPORT_.equals(contract.getInvoiceType()) || InvoiceType._REGULAR_.equals(contract.getInvoiceType());

		// Build document data
		Data docData = new Data();
		docData.setHeader(this.buildHeader(invoice, contract.getCreditTerms()));
		docData.setLines(this.builLines(invoice, priceIncludesVAT));
		docData.setFooter(this.buildFooter(invoice, priceIncludesVAT));

		// Build document information
		String documentFormatCode = InvoiceTypeAcc._INV_.equals(invoice.getInvoiceType()) ? contract.getInvoiceType().getCode()
				: CREDIT_MEMO_FORMAT_CODE;

		doc.setDocumentInformation(this.buildInformation(invoice, documentFormatCode));
		doc.setData(docData);

		return doc;
	}

	private DocumentInformation buildInformation(OutgoingInvoice invoice, String documentType) {
		DocumentInformation docInfo = new DocumentInformation();
		docInfo.setDocumentType(documentType);

		Customer subsidiary = this.customerService.findByRefid(invoice.getSubsidiaryId());
		docInfo.setCompany(subsidiary.getCode());
		return docInfo;
	}

	private Header buildHeader(OutgoingInvoice invoice, CreditTerm creditTerm) {
		Header docHeader = new Header();
		docHeader.setDocumentNumber(invoice.getInvoiceNo());
		docHeader.setDateOfIssue(this.sdf.format(invoice.getInvoiceDate()));
		docHeader.setDueDate(this.sdf.format(invoice.getBaseLineDate()));
		docHeader.setSalesPersonCode(StringUtils.EMPTY);
		docHeader.setBusinessSegment(BUSINESS_SEGMENT);
		docHeader.setAccountNumber(invoice.getReceiver().getAccountNumber());
		docHeader.setCustomerName(invoice.getReceiver().getName());
		docHeader.setCustomerAddress(invoice.getReceiver().getOfficeStreet());
		docHeader.setCity(invoice.getReceiver().getOfficeCity());

		if (invoice.getReceiver().getOfficeCountry() != null) {
			docHeader.setCounty(invoice.getReceiver().getOfficeCountry().getName());
		}
		docHeader.setBOLNumber(StringUtils.EMPTY);
		docHeader.setVatRegistrationNumber(invoice.getReceiver().getVatRegNumber());
		docHeader.setLineOfBusiness(LINE_OF_BUSINESS);
		docHeader.setNIT(NIT);
		docHeader.setNIToDUI(StringUtils.EMPTY);

		docHeader.setIsCredit(CreditTerm._CREDIT_LINE_.equals(creditTerm) ? "1" : "0");
		docHeader.setIsPrepayment(CreditTerm._PREPAYMENT_.equals(creditTerm) ? "1" : "0");
		docHeader.setTransportationCompany(StringUtils.EMPTY);
		return docHeader;
	}

	private Lines builLines(OutgoingInvoice invoice, Boolean priceIncludesVAT) {
		Lines docLines = new Lines();
		List<Line> lines = new ArrayList<>();
		BigInteger lineNumber = BigInteger.ONE;
		for (OutgoingInvoiceLine invoiceLine : invoice.getInvoiceLines()) {

			Line line = new Line();
			line.setLineNumber(lineNumber);
			line.setUpliftDate(this.sdf.format(invoiceLine.getDeliveryDate()));
			line.setAircraftModel(invoiceLine.getAircraftRegistrationNumber());
			line.setFlightNumber(invoiceLine.getFlightNo());

			// Return only the first 3 characters (fizical paper limitations)
			if (invoiceLine.getFlightType() != null) {
				line.setFlightType(invoiceLine.getFlightType().getName().substring(0, 3).toUpperCase());
			}
			line.setTicketNumber(invoiceLine.getTicketNo());
			line.setBolNumber(invoiceLine.getBolNumber() != null ? invoiceLine.getBolNumber() : StringUtils.EMPTY);

			if (invoice.getProduct() != null) {
				line.setProductName(invoice.getProduct().getName());
			}

			if (invoiceLine.getQuantity() != null) {
				Double quantity = Double.parseDouble(this.volumeDecimalFormat.format(invoiceLine.getQuantity()));
				line.setProductQuantity(quantity);
			}
			line.setAdditionalCharges(this.buildAdditionalCharges(invoiceLine, priceIncludesVAT));
			lines.add(line);

			lineNumber = lineNumber.add(BigInteger.ONE);
		}
		docLines.getLine().addAll(lines);
		return docLines;
	}

	private AdditionalCharges buildAdditionalCharges(OutgoingInvoiceLine invoiceLine, Boolean priceIncludesVAT) {
		AdditionalCharges lineAdditionalCharges = new AdditionalCharges();

		// Convert collection to list for filtering with lambdas
		List<OutgoingInvoiceLineDetails> details = new ArrayList<>(invoiceLine.getDetails());

		// Build product predicate
		Predicate<OutgoingInvoiceLineDetails> products = d -> PriceType._PRODUCT_.equals(d.getPriceCategory().getType());

		// Add product charges
		List<OutgoingInvoiceLineDetails> productDetails = details.stream().filter(products).collect(Collectors.toList());
		lineAdditionalCharges.getAdditionalCharge().addAll(this.buildAdditionalCharges(productDetails, priceIncludesVAT));

		// Add other charges
		List<OutgoingInvoiceLineDetails> otherDetails = details.stream().filter(products.negate()).collect(Collectors.toList());
		lineAdditionalCharges.getAdditionalCharge().addAll(this.buildAdditionalCharges(otherDetails, priceIncludesVAT));

		return lineAdditionalCharges;
	}

	private List<AdditionalCharge> buildAdditionalCharges(List<OutgoingInvoiceLineDetails> details, Boolean priceIncludesVAT) {
		List<AdditionalCharge> additionalCharges = new ArrayList<>();
		for (OutgoingInvoiceLineDetails detail : details) {
			if (CalculateIndicator._INCLUDED_.equals(detail.getCalculateIndicator())) {
				AdditionalCharge additionalCharge = new AdditionalCharge();
				additionalCharge.setChargeType(detail.getPriceName());

				BigDecimal vatAmount = detail.getVatAmount();
				BigDecimal unitPrice = detail.getSettlementPrice();
				BigDecimal totalAmount = detail.getSettlementAmount();

				if (priceIncludesVAT && BigDecimal.ZERO.compareTo(vatAmount) != 0) {
					BigDecimal vat = totalAmount.divide(vatAmount, MathContext.DECIMAL64);

					unitPrice = unitPrice.add(unitPrice.divide(vat, MathContext.DECIMAL64));
					totalAmount = totalAmount.add(vatAmount);
				}

				additionalCharge.setUnitPrice(Double.parseDouble(this.priceDecimalFormat.format(unitPrice)));
				additionalCharge.setTotalCharge(Double.parseDouble(this.amountDecimalFormat.format(totalAmount)));
				additionalCharges.add(additionalCharge);
			}
		}

		return additionalCharges;
	}

	private Footer buildFooter(OutgoingInvoice invoice, Boolean priceIncludesVAT) {
		Footer docFooter = new Footer();

		if (invoice.getTotalAmount() != null) {
			Double totalAmount = Double.parseDouble(this.amountDecimalFormat.format(invoice.getTotalAmount()));
			docFooter.setTotalValueInWords(this.totalAmountInWords(invoice.getTotalAmount()).toUpperCase());
			docFooter.setSubTotal(totalAmount);
			docFooter.setTotal(totalAmount);
		}

		BigDecimal amount = priceIncludesVAT ? invoice.getTotalAmount() : invoice.getNetAmount();
		if (invoice.getNetAmount() != null) {
			Double totalBeforeVAT = Double.parseDouble(this.amountDecimalFormat.format(amount));
			docFooter.setTotalValueBeforeVat(totalBeforeVAT);
		}

		if (!priceIncludesVAT && invoice.getVatAmount() != null) {
			Double vatAmount = Double.parseDouble(this.amountDecimalFormat.format(invoice.getVatAmount()));
			docFooter.setVat(vatAmount);
		}

		return docFooter;
	}

	private String totalAmountInWords(BigDecimal amount) {
		Locale locale = new Locale("es");
		IConverter converter = Converter.getMaleConverter(locale);

		StringBuilder totalAmountInWords = new StringBuilder();

		totalAmountInWords.append(converter.convert(amount.longValue()));
		totalAmountInWords.append(" US DOLARES CON ");

		long decimalValue = amount.setScale(2, BigDecimal.ROUND_HALF_UP).remainder(BigDecimal.ONE).movePointRight(2).abs().longValue();
		totalAmountInWords.append(converter.convert(decimalValue));
		totalAmountInWords.append(" CENTAVOS");

		return totalAmountInWords.toString();
	}
}
