package atraxo.acc.business.ws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.ReceivedStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.ws.IRestClientCallbackService;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ext.util.EntityCloner;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.client.PumaRestClient;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.ws.dto.WSExecutionResult;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;

/**
 * @author vhojda
 */
public class ExportFuelEventPumaRestClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExportFuelEventPumaRestClient.class);

	@Autowired
	private IExternalInterfaceService externalInterfaceService;

	@Autowired
	private ExternalInterfaceHistoryFacade historyFacade;

	@Autowired
	private WSFuelEventToDTOTransformer wsFuelEventToDTOTransformer;

	@Autowired
	private IFuelEventService fuelEventService;

	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageFacade;
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	/**
	 * @param fuelEvent
	 */
	public ExportFuelEventPumaRestClient() {
		// The explicit default constructor
	}

	/**
	 * Calls the Puma Rest Client associated with this operation and exports the Fuel Event; also, depending on the result of the operation, it also
	 * sets the fuel event transmission status accordingly.
	 *
	 * @param fuelEvent
	 * @param fuelEventOperation
	 * @param checkOnHoldStatus true if the system should check the released status before attempting export the fuel events
	 * @param exportOnlyFailed true if the system should export only FAILED fuel tickets, false otherwise (normal behaviour)
	 * @returns the result of the export operation
	 * @throws BusinessException
	 */
	public ExportFuelEventOperationResult export(FuelEvent fuelEvent, ExportFuelEventOperationEnum fuelEventOperation, boolean checkOnHoldStatus,
			boolean exportOnlyFailed) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START export()");
		}

		ExportFuelEventOperationResult exportedResult = new ExportFuelEventOperationResult();

		ExternalInterface extInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.EXPORT_FUEL_EVENTS_TO_NAV.getValue());
		if (extInterface.getEnabled()) {
			// if payable and billable cost exist, "create" two fuel events and attempt to export them (if the case)
			if (fuelEvent.getPayableCost() != null && fuelEvent.getBillableCost() != null) {

				// check if system should export only the ones that are not NEW OR if the system should export only FAILED ones
				boolean proceedWithPayableFuelEventExport = true;
				if ((checkOnHoldStatus && fuelEvent.getReceivedStatus().equals(ReceivedStatus._ON_HOLD_))
						|| (exportOnlyFailed && !fuelEvent.getTransmissionStatusPurchase().equals(FuelEventTransmissionStatus._FAILED_))) {
					proceedWithPayableFuelEventExport = false;
				}

				if (proceedWithPayableFuelEventExport) {
					exportedResult.incrementNrTotal();

					FuelEvent payableFuelEvent = this.extractPayableFuelEvent(fuelEvent);
					boolean exportedPayable = this.callExportInterface(payableFuelEvent, extInterface, fuelEventOperation, true);
					fuelEvent.setTransmissionStatusPurchase(
							exportedPayable ? FuelEventTransmissionStatus._IN_PROGRESS_ : FuelEventTransmissionStatus._FAILED_);
					if (exportedPayable) {
						exportedResult.incrementNrExported();
					}
				}

				// check if system should export only the ones that are not NEW OR if the system should export only FAILED ones
				boolean proceedWithReceivableFuelEventExport = true;
				if ((checkOnHoldStatus && fuelEvent.getReceivedStatus().equals(ReceivedStatus._ON_HOLD_))
						|| (exportOnlyFailed && !fuelEvent.getTransmissionStatusSale().equals(FuelEventTransmissionStatus._FAILED_))) {
					proceedWithReceivableFuelEventExport = false;
				}
				if (proceedWithReceivableFuelEventExport) {
					exportedResult.incrementNrTotal();

					FuelEvent receivableFuelEvent = this.extractReceivableFuelEvent(fuelEvent);
					boolean exportedReceivable = this.callExportInterface(receivableFuelEvent, extInterface, fuelEventOperation, false);
					fuelEvent.setTransmissionStatusSale(
							exportedReceivable ? FuelEventTransmissionStatus._IN_PROGRESS_ : FuelEventTransmissionStatus._FAILED_);
					if (exportedReceivable) {
						exportedResult.incrementNrExported();
					}
				}

			} else {
				// attempt to export only one fuel event (if the case)
				boolean proceedWithFuelEventExport = true;
				if ((checkOnHoldStatus && fuelEvent.getReceivedStatus().equals(ReceivedStatus._ON_HOLD_))
						|| (exportOnlyFailed && !fuelEvent.getTransmissionStatusSale().equals(FuelEventTransmissionStatus._FAILED_))) {
					proceedWithFuelEventExport = false;
				}
				if (proceedWithFuelEventExport) {
					exportedResult.incrementNrTotal();
					fuelEvent.setEntityAlias(fuelEvent.getPayableCost() != null ? FuelEventDealTypeAliasEnum.PURCHASE.getCustomEntityAlias()
							: FuelEventDealTypeAliasEnum.SALE.getCustomEntityAlias());
					boolean exported = this.callExportInterface(fuelEvent, extInterface, fuelEventOperation, false);
					fuelEvent.setTransmissionStatusSale(exported ? FuelEventTransmissionStatus._IN_PROGRESS_ : FuelEventTransmissionStatus._FAILED_);
					if (exported) {
						exportedResult.incrementNrExported();
					}
				}
			}
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END export()");
		}

		return exportedResult;
	}

	/**
	 * @param fuelEvent
	 * @return
	 * @throws BusinessException
	 */
	private FuelEvent extractReceivableFuelEvent(FuelEvent fuelEvent) throws BusinessException {
		FuelEvent fe = null;
		try {
			fe = EntityCloner.cloneEntityWithAbstractFields(fuelEvent);
			fe.setDetails(new ArrayList<FuelEventsDetails>());

			fe.setPayableCost(null);
			fe.setPayableCostCurrency(null);
			for (Iterator<FuelEventsDetails> it = fuelEvent.getDetails().iterator(); it.hasNext();) {
				FuelEventsDetails feDetails = it.next();
				if (feDetails.getTransaction().equals(DealType._SELL_)) {
					fe.addToDetails(EntityCloner.cloneEntityWithAbstractFields(feDetails));
				}
			}

			fe.setEntityAlias(FuelEventDealTypeAliasEnum.SALE.getCustomEntityAlias());
		} catch (BeansException e) {
			LOGGER.error("ERROR : could not clone FuelEvent !", e);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}

		return fe;
	}

	private void setPayableHolder(FuelEvent fe) {
		if (fe.getIsResale()) {
			for (DeliveryNote dn : fe.getDeliveryNotes()) {
				if (dn.getUsed()) {
					if (!CollectionUtils.isEmpty(dn.getDeliveryNoteContracts())) {
						Contract contract = dn.getDeliveryNoteContracts().iterator().next().getContracts();
						fe.setContractHolder(contract.getHolder());
					}
				}
			}
		}
	}

	/**
	 * @param fuelEvent
	 * @return
	 * @throws BusinessException
	 */
	private FuelEvent extractPayableFuelEvent(FuelEvent fuelEvent) throws BusinessException {
		FuelEvent fe = null;
		try {
			fe = EntityCloner.cloneEntityWithAbstractFields(fuelEvent);
			fe.setDetails(new ArrayList<FuelEventsDetails>());

			fe.setBillableCost(null);
			// fe.setBillableCostCurrency(null);

			this.setPayableHolder(fe);

			for (Iterator<FuelEventsDetails> it = fuelEvent.getDetails().iterator(); it.hasNext();) {
				FuelEventsDetails feDetails = it.next();
				if (feDetails.getTransaction().equals(DealType._BUY_)) {
					fe.addToDetails(EntityCloner.cloneEntityWithAbstractFields(feDetails));
				}
			}

			fe.setEntityAlias(FuelEventDealTypeAliasEnum.PURCHASE.getCustomEntityAlias());
		} catch (BeansException e) {
			LOGGER.error("ERROR : could not clone FuelEvent !", e);
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}

		return fe;
	}

	/**
	 * Instantiates a new REST Client and exports the fuel event received as parameter
	 *
	 * @param fuelEvent
	 * @param extInterface
	 * @throws BusinessException
	 * @returns true if exported, false otherwise
	 */
	@SuppressWarnings("unchecked")
	private boolean callExportInterface(FuelEvent fuelEvent, ExternalInterface extInterface, ExportFuelEventOperationEnum fuelEventOperation,
			final boolean isPayableCost) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START callExportInterface()");
		}

		boolean exported = true;

		this.wsFuelEventToDTOTransformer.setFuelEventOperationType(fuelEventOperation);

		ExtInterfaceParameters params = new ExtInterfaceParameters(extInterface.getParams());
		params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS, "");
		params.put(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS, "FALSE");
		params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_OBJECT_TYPE, fuelEvent.getEntityAlias());

		if (isPayableCost) {
			params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_PAYABLE, "TRUE");
		} else {
			params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_PAYABLE, "FALSE");
		}

		PumaRestClient<FuelEvent> client = new PumaRestClient<>(this.taskExecutor, extInterface, params, Arrays.asList(fuelEvent), this.historyFacade,
				this.wsFuelEventToDTOTransformer, this.messageFacade);

		client.setSourceCompany(fuelEvent.getContractHolder().getCode());

		client.setCallbackSrv((IRestClientCallbackService<FuelEvent>) this.fuelEventService);

		WSExecutionResult result = client.start(3);
		if (result != null) {
			exported = result.isSuccess();
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END callExportInterface()");
		}

		return exported;
	}

}
