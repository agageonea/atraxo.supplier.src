package atraxo.acc.business.ext.cost;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteContractService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.ext.deliveryNotes.delegate.DeliveryNoteCostCalculatorDelegate;
import atraxo.acc.business.ext.fuelEvents.service.FuelEventModifierService;
import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author zspeter
 */
public class FuelEventCostUpdaterService extends AbstractBusinessBaseService {

	@Autowired
	private IFuelEventService feSrv;
	@Autowired
	private IFuelOrderLocationService folSrv;
	@Autowired
	private FuelEventModifierService manager;
	@Autowired
	private IDeliveryNoteContractService dncSrv;

	/**
	 * @param list
	 * @throws BusinessException
	 */
	public void updateAccruals(List<FuelEvent> list) throws BusinessException {
		for (FuelEvent fe : list) {
			this.manager.generateAccruals(fe);
		}
	}

	/**
	 * Update fuel event receivable cost based on contract.
	 *
	 * @param c Contract.
	 * @param eventsList
	 * @return
	 * @throws BusinessException
	 */
	public List<FuelEvent> updateReceivable(Contract c, StringBuilder eventsList) throws BusinessException {
		List<FuelEvent> list = new ArrayList<>();
		if (c.getDealType().equals(DealType._SELL_)) {
			List<FuelEvent> feList = this.getFuelEventsWithReceivableCost(c.getClass(), c.getId());
			list.addAll(this.updateReceivable(feList, eventsList));
		} else {
			List<FuelOrderLocation> folList = this.folSrv.findByContract(c);
			for (FuelOrderLocation fol : folList) {
				List<FuelEvent> feList = this.getFuelEventsWithReceivableCost(fol.getClass(), fol.getId());
				list.addAll(this.updateReceivable(feList, eventsList));
			}
		}
		return list;
	}

	/**
	 * Update fuel event receivable cost based on contract id.
	 *
	 * @param contractId Contract id.
	 * @param eventsList
	 * @return
	 * @throws BusinessException
	 */
	public List<FuelEvent> updateReceivable(Integer contractId, StringBuilder eventsList) throws BusinessException {
		Contract contract = this.feSrv.findById(contractId, Contract.class);
		if (contract != null) {
			return this.updateReceivable(contract, eventsList);
		}
		return Collections.emptyList();
	}

	/**
	 * Update Fuel event payable cost based on contract.
	 *
	 * @param c
	 * @param eventsList
	 * @return
	 * @throws BusinessException
	 */
	public List<FuelEvent> updatePayableCost(Contract c, StringBuilder eventsList) throws BusinessException {
		List<FuelEvent> feList = this.getFuelEventWithPayableCost(c);
		DeliveryNoteCostCalculatorDelegate bd = this.getBusinessDelegate(DeliveryNoteCostCalculatorDelegate.class);
		Iterator<FuelEvent> iter = feList.iterator();
		while (iter.hasNext()) {
			FuelEvent fe = iter.next();
			if (FuelEventStatus._CANCELED_.equals(fe.getStatus())) {
				iter.remove();
				continue;
			}
			eventsList.append(fe.getRefid() + "  " + fe.getTicketNumber());
			boolean updated = false;
			for (DeliveryNote dn : fe.getDeliveryNotes()) {
				if (bd.calculatePayableCost(dn)) {
					updated = true;
					this.manager.updateFuelEvent(dn);
				}
			}
			if (!updated) {
				iter.remove();
			}

		}
		return feList;
	}

	public List<FuelEvent> getFuelEventWithPayableCost(Contract c) throws BusinessException {
		List<DeliveryNoteContract> list = this.dncSrv.findByContract(c);
		List<Object> ids = new ArrayList<>();
		for (DeliveryNoteContract dnc : list) {
			FuelEvent fe = dnc.getDeliveryNotes().getFuelEvent();
			if (!FuelEventStatus._CANCELED_.equals(fe.getStatus())) {
				ids.add(dnc.getDeliveryNotes().getFuelEvent().getId());
			}
		}
		if (ids.isEmpty()) {
			return Collections.emptyList();
		}
		return this.feSrv.findByIds(ids);
	}

	/**
	 * Update fuel event payable cost based on contract id.
	 *
	 * @param contractId
	 * @param eventsList
	 * @return
	 * @throws BusinessException
	 */
	public List<FuelEvent> updatePayableCost(Integer contractId, StringBuilder eventsList) throws BusinessException {
		Contract contract = this.feSrv.findById(contractId, Contract.class);
		if (contract != null) {
			return this.updatePayableCost(contract, eventsList);
		}
		return Collections.emptyList();
	}

	/**
	 * Update fuel event receivable cost based on bill price ource type and bill price source id.
	 *
	 * @param clazz - Bill price source Type Contract.class or FuelOrderLocation.class
	 * @param id - Contract or fuel order location id.
	 * @return - A list with fuel events that has been updated.
	 * @throws BusinessException
	 */
	private List<FuelEvent> updateReceivable(List<FuelEvent> list, StringBuilder eventsList) throws BusinessException {
		Iterator<FuelEvent> iter = list.iterator();
		while (iter.hasNext()) {
			FuelEvent fe = iter.next();
			if (FuelEventStatus._CANCELED_.equals(fe.getStatus())) {
				iter.remove();
				continue;
			}
			eventsList.append(fe.getRefid() + "  " + fe.getTicketNumber());
			boolean updated = false;
			for (DeliveryNote dn : fe.getDeliveryNotes()) {
				if (dn.getUsed()) {
					updated = this.manager.calculateReceivableCost(fe, dn);
				}
			}
			if (!updated) {
				iter.remove();
			}
		}
		return list;
	}

	public List<FuelEvent> getFuelEventsWithReceivableCost(Class<?> clazz, Integer id) {
		Map<String, Object> params = new HashMap<>();
		params.put("billPriceSourceType", clazz.getSimpleName());
		params.put("billPriceSourceId", id);

		List<FuelEvent> list = this.feSrv.findEntitiesByAttributes(params);
		if (list == null || list.isEmpty()) {
			return Collections.emptyList();
		}
		return list;
	}

}
