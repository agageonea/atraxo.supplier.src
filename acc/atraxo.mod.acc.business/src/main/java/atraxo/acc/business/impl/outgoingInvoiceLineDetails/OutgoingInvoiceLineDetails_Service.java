/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.outgoingInvoiceLineDetails;

import atraxo.acc.business.api.outgoingInvoiceLineDetails.IOutgoingInvoiceLineDetailsService;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link OutgoingInvoiceLineDetails} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class OutgoingInvoiceLineDetails_Service
		extends
			AbstractEntityService<OutgoingInvoiceLineDetails>
		implements
			IOutgoingInvoiceLineDetailsService {

	/**
	 * Public constructor for OutgoingInvoiceLineDetails_Service
	 */
	public OutgoingInvoiceLineDetails_Service() {
		super();
	}

	/**
	 * Public constructor for OutgoingInvoiceLineDetails_Service
	 * 
	 * @param em
	 */
	public OutgoingInvoiceLineDetails_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<OutgoingInvoiceLineDetails> getEntityClass() {
		return OutgoingInvoiceLineDetails.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoiceLineDetails
	 */
	public OutgoingInvoiceLineDetails findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							OutgoingInvoiceLineDetails.NQ_FIND_BY_BUSINESS,
							OutgoingInvoiceLineDetails.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"OutgoingInvoiceLineDetails", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"OutgoingInvoiceLineDetails", "id"), nure);
		}
	}

	/**
	 * Find by reference: outgoingInvoiceLine
	 *
	 * @param outgoingInvoiceLine
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOutgoingInvoiceLine(
			OutgoingInvoiceLine outgoingInvoiceLine) {
		return this.findByOutgoingInvoiceLineId(outgoingInvoiceLine.getId());
	}
	/**
	 * Find by ID of reference: outgoingInvoiceLine.id
	 *
	 * @param outgoingInvoiceLineId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOutgoingInvoiceLineId(
			Integer outgoingInvoiceLineId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLineDetails e where e.clientId = :clientId and e.outgoingInvoiceLine.id = :outgoingInvoiceLineId",
						OutgoingInvoiceLineDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("outgoingInvoiceLineId", outgoingInvoiceLineId)
				.getResultList();
	}
	/**
	 * Find by reference: outgoingInvoice
	 *
	 * @param outgoingInvoice
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOutgoingInvoice(
			OutgoingInvoice outgoingInvoice) {
		return this.findByOutgoingInvoiceId(outgoingInvoice.getId());
	}
	/**
	 * Find by ID of reference: outgoingInvoice.id
	 *
	 * @param outgoingInvoiceId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOutgoingInvoiceId(
			Integer outgoingInvoiceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLineDetails e where e.clientId = :clientId and e.outgoingInvoice.id = :outgoingInvoiceId",
						OutgoingInvoiceLineDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("outgoingInvoiceId", outgoingInvoiceId)
				.getResultList();
	}
	/**
	 * Find by reference: priceCategory
	 *
	 * @param priceCategory
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByPriceCategory(
			PriceCategory priceCategory) {
		return this.findByPriceCategoryId(priceCategory.getId());
	}
	/**
	 * Find by ID of reference: priceCategory.id
	 *
	 * @param priceCategoryId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByPriceCategoryId(
			Integer priceCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLineDetails e where e.clientId = :clientId and e.priceCategory.id = :priceCategoryId",
						OutgoingInvoiceLineDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("priceCategoryId", priceCategoryId)
				.getResultList();
	}
	/**
	 * Find by reference: mainCategory
	 *
	 * @param mainCategory
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByMainCategory(
			MainCategory mainCategory) {
		return this.findByMainCategoryId(mainCategory.getId());
	}
	/**
	 * Find by ID of reference: mainCategory.id
	 *
	 * @param mainCategoryId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByMainCategoryId(
			Integer mainCategoryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLineDetails e where e.clientId = :clientId and e.mainCategory.id = :mainCategoryId",
						OutgoingInvoiceLineDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("mainCategoryId", mainCategoryId).getResultList();
	}
	/**
	 * Find by reference: originalPriceCurrency
	 *
	 * @param originalPriceCurrency
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOriginalPriceCurrency(
			Currencies originalPriceCurrency) {
		return this
				.findByOriginalPriceCurrencyId(originalPriceCurrency.getId());
	}
	/**
	 * Find by ID of reference: originalPriceCurrency.id
	 *
	 * @param originalPriceCurrencyId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOriginalPriceCurrencyId(
			Integer originalPriceCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLineDetails e where e.clientId = :clientId and e.originalPriceCurrency.id = :originalPriceCurrencyId",
						OutgoingInvoiceLineDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("originalPriceCurrencyId",
						originalPriceCurrencyId).getResultList();
	}
	/**
	 * Find by reference: originalPriceUnit
	 *
	 * @param originalPriceUnit
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOriginalPriceUnit(
			Unit originalPriceUnit) {
		return this.findByOriginalPriceUnitId(originalPriceUnit.getId());
	}
	/**
	 * Find by ID of reference: originalPriceUnit.id
	 *
	 * @param originalPriceUnitId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOriginalPriceUnitId(
			Integer originalPriceUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoiceLineDetails e where e.clientId = :clientId and e.originalPriceUnit.id = :originalPriceUnitId",
						OutgoingInvoiceLineDetails.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("originalPriceUnitId", originalPriceUnitId)
				.getResultList();
	}
}
