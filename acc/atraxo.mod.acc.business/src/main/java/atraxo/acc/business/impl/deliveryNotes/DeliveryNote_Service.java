/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.deliveryNotes;

import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link DeliveryNote} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class DeliveryNote_Service extends AbstractEntityService<DeliveryNote> {

	/**
	 * Public constructor for DeliveryNote_Service
	 */
	public DeliveryNote_Service() {
		super();
	}

	/**
	 * Public constructor for DeliveryNote_Service
	 * 
	 * @param em
	 */
	public DeliveryNote_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<DeliveryNote> getEntityClass() {
		return DeliveryNote.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return DeliveryNote
	 */
	public DeliveryNote findByKey(QuantitySource source, Date fuelingDate,
			Locations departure, Customer customer, Aircraft aircraft,
			String flightNumber, Suffix suffix, String ticketNumber) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DeliveryNote.NQ_FIND_BY_KEY,
							DeliveryNote.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("source", source)
					.setParameter("fuelingDate", fuelingDate)
					.setParameter("departure", departure)
					.setParameter("customer", customer)
					.setParameter("aircraft", aircraft)
					.setParameter("flightNumber", flightNumber)
					.setParameter("suffix", suffix)
					.setParameter("ticketNumber", ticketNumber)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(
							J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DeliveryNote",
							"source, fuelingDate, departure, customer, aircraft, flightNumber, suffix, ticketNumber"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DeliveryNote",
							"source, fuelingDate, departure, customer, aircraft, flightNumber, suffix, ticketNumber"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DeliveryNote
	 */
	public DeliveryNote findByKey(QuantitySource source, Date fuelingDate,
			Long departureId, Long customerId, Long aircraftId,
			String flightNumber, Suffix suffix, String ticketNumber) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DeliveryNote.NQ_FIND_BY_KEY_PRIMITIVE,
							DeliveryNote.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("source", source)
					.setParameter("fuelingDate", fuelingDate)
					.setParameter("departureId", departureId)
					.setParameter("customerId", customerId)
					.setParameter("aircraftId", aircraftId)
					.setParameter("flightNumber", flightNumber)
					.setParameter("suffix", suffix)
					.setParameter("ticketNumber", ticketNumber)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(
							J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DeliveryNote",
							"source, fuelingDate, departureId, customerId, aircraftId, flightNumber, suffix, ticketNumber"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DeliveryNote",
							"source, fuelingDate, departureId, customerId, aircraftId, flightNumber, suffix, ticketNumber"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return DeliveryNote
	 */
	public DeliveryNote findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(DeliveryNote.NQ_FIND_BY_BUSINESS,
							DeliveryNote.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"DeliveryNote", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"DeliveryNote", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByFuelEvent(FuelEvent fuelEvent) {
		return this.findByFuelEventId(fuelEvent.getId());
	}
	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByFuelEventId(Integer fuelEventId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.fuelEvent.id = :fuelEventId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelEventId", fuelEventId).getResultList();
	}
	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.customer.id = :customerId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: departure
	 *
	 * @param departure
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByDeparture(Locations departure) {
		return this.findByDepartureId(departure.getId());
	}
	/**
	 * Find by ID of reference: departure.id
	 *
	 * @param departureId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByDepartureId(Integer departureId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.departure.id = :departureId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("departureId", departureId).getResultList();
	}
	/**
	 * Find by reference: aircraft
	 *
	 * @param aircraft
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByAircraft(Aircraft aircraft) {
		return this.findByAircraftId(aircraft.getId());
	}
	/**
	 * Find by ID of reference: aircraft.id
	 *
	 * @param aircraftId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByAircraftId(Integer aircraftId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.aircraft.id = :aircraftId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("aircraftId", aircraftId).getResultList();
	}
	/**
	 * Find by reference: fuelSupplier
	 *
	 * @param fuelSupplier
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByFuelSupplier(Suppliers fuelSupplier) {
		return this.findByFuelSupplierId(fuelSupplier.getId());
	}
	/**
	 * Find by ID of reference: fuelSupplier.id
	 *
	 * @param fuelSupplierId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByFuelSupplierId(Integer fuelSupplierId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.fuelSupplier.id = :fuelSupplierId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelSupplierId", fuelSupplierId).getResultList();
	}
	/**
	 * Find by reference: iplAgent
	 *
	 * @param iplAgent
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByIplAgent(Suppliers iplAgent) {
		return this.findByIplAgentId(iplAgent.getId());
	}
	/**
	 * Find by ID of reference: iplAgent.id
	 *
	 * @param iplAgentId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByIplAgentId(Integer iplAgentId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.iplAgent.id = :iplAgentId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("iplAgentId", iplAgentId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.unit.id = :unitId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: payableCostCurrency
	 *
	 * @param payableCostCurrency
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByPayableCostCurrency(
			Currencies payableCostCurrency) {
		return this.findByPayableCostCurrencyId(payableCostCurrency.getId());
	}
	/**
	 * Find by ID of reference: payableCostCurrency.id
	 *
	 * @param payableCostCurrencyId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByPayableCostCurrencyId(
			Integer payableCostCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.payableCostCurrency.id = :payableCostCurrencyId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("payableCostCurrencyId", payableCostCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: billableCostCurrency
	 *
	 * @param billableCostCurrency
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByBillableCostCurrency(
			Currencies billableCostCurrency) {
		return this.findByBillableCostCurrencyId(billableCostCurrency.getId());
	}
	/**
	 * Find by ID of reference: billableCostCurrency.id
	 *
	 * @param billableCostCurrencyId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByBillableCostCurrencyId(
			Integer billableCostCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.billableCostCurrency.id = :billableCostCurrencyId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("billableCostCurrencyId", billableCostCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: netUpliftUnit
	 *
	 * @param netUpliftUnit
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByNetUpliftUnit(Unit netUpliftUnit) {
		return this.findByNetUpliftUnitId(netUpliftUnit.getId());
	}
	/**
	 * Find by ID of reference: netUpliftUnit.id
	 *
	 * @param netUpliftUnitId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByNetUpliftUnitId(Integer netUpliftUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.netUpliftUnit.id = :netUpliftUnitId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("netUpliftUnitId", netUpliftUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: contractHolder
	 *
	 * @param contractHolder
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByContractHolder(Customer contractHolder) {
		return this.findByContractHolderId(contractHolder.getId());
	}
	/**
	 * Find by ID of reference: contractHolder.id
	 *
	 * @param contractHolderId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByContractHolderId(Integer contractHolderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.contractHolder.id = :contractHolderId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractHolderId", contractHolderId)
				.getResultList();
	}
	/**
	 * Find by reference: shipTo
	 *
	 * @param shipTo
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByShipTo(Customer shipTo) {
		return this.findByShipToId(shipTo.getId());
	}
	/**
	 * Find by ID of reference: shipTo.id
	 *
	 * @param shipToId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByShipToId(Integer shipToId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from DeliveryNote e where e.clientId = :clientId and e.shipTo.id = :shipToId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("shipToId", shipToId).getResultList();
	}
	/**
	 * Find by reference: deliveryNoteContracts
	 *
	 * @param deliveryNoteContracts
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByDeliveryNoteContracts(
			DeliveryNoteContract deliveryNoteContracts) {
		return this
				.findByDeliveryNoteContractsId(deliveryNoteContracts.getId());
	}
	/**
	 * Find by ID of reference: deliveryNoteContracts.id
	 *
	 * @param deliveryNoteContractsId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByDeliveryNoteContractsId(
			Integer deliveryNoteContractsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from DeliveryNote e, IN (e.deliveryNoteContracts) c where e.clientId = :clientId and c.id = :deliveryNoteContractsId",
						DeliveryNote.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("deliveryNoteContractsId",
						deliveryNoteContractsId).getResultList();
	}
}
