/**
 *
 */
package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.utils.DateUtils;

/**
 * @author vhojda
 */
public class PerDeliveryFuelEventGenerator extends AbstractFuelEventGenerator {

	/**
	 *
	 */
	public PerDeliveryFuelEventGenerator() {

	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#filterByFrequency(java.util.Collection)
	 */
	@Override
	public void filterByFrequency(Collection<FuelEvent> events) {
		Calendar presentCal = DateUtils.getPresentIgnoreHours();

		for (Iterator<FuelEvent> it = events.iterator(); it.hasNext();) {
			FuelEvent event = it.next();
			Calendar eventCal = DateUtils.getCalendarFromDateIgnoreHours(event.getFuelingDate());
			// if ONLY in the future then remove it
			if (DateUtils.compareDates(eventCal, presentCal, true) > 0) {
				it.remove();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#splitByFrequency(java.util.List)
	 */
	@Override
	public Map<InvoicingDatePeriod, List<FuelEvent>> splitByFrequency(List<FuelEvent> events) {
		Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap = new HashMap<>();

		if (!events.isEmpty()) {
			// put one entry for each of the invoices
			for (FuelEvent ev : events) {
				List<FuelEvent> eventsList = new ArrayList<>();
				eventsList.add(ev);
				eventsMap.put(new InvoicingDatePeriod(ev.getFuelingDate(), true), eventsList);
			}
		}
		return eventsMap;
	}

}
