package atraxo.acc.business.ext.fuelEvents.delegate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.domain.ext.deliveryNotes.AccrualDateParameter;
import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.financialSources.IFinancialSourcesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.timeserie.IAverageMethodService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class FuelEvent_Bd extends AbstractBusinessDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(FuelEvent_Bd.class);

	public FuelEvent generateFuelEvent(DeliveryNote dn) throws BusinessException {
		IFuelEventService fuelEventService = (IFuelEventService) this.findEntityService(FuelEvent.class);
		FuelEvent fe = new FuelEvent();
		fe.setFuelingDate(dn.getFuelingDate());
		fe.setCustomer(dn.getCustomer());
		fe.setDeparture(dn.getDeparture());
		fe.setAircraft(dn.getAircraft());
		fe.setFlightNumber(dn.getFlightNumber());
		fe.setSuffix(dn.getSuffix());
		fe.setTicketNumber(dn.getTicketNumber() != null ? dn.getTicketNumber() : "");
		fe.setFuelSupplier(dn.getFuelSupplier());
		fe.setIplAgent(dn.getIplAgent());
		fe.setQuantity(dn.getQuantity() != null ? dn.getQuantity() : BigDecimal.ZERO);
		fe.setUnit(dn.getUnit());
		this.setSystemQuantity(fe, dn);
		fe.setQuantitySource(dn.getSource());
		fe.setInvoiceStatus(OutgoingInvoiceStatus._NOT_INVOICED_);
		fe.setBillStatus(BillStatus._NOT_BILLED_);
		fe.setEventType(dn.getEventType());
		fe.setPayableCost(dn.getPayableCost());
		fe.setPayableCostCurrency(dn.getPayableCostCurrency());
		fe.setSysPayableCost(this.calculateSysPayableCost(fe));
		fe.setNetUpliftQuantity(dn.getNetUpliftQuantity());
		fe.setNetUpliftUnit(dn.getNetUpliftUnit());
		fe.setFuelingOperation(dn.getFuelingOperation());
		fe.setTransport(dn.getTransport());
		fe.setOperationType(dn.getOperationType());
		fe.setStatus(FuelEventStatus._ORIGINAL_);
		fe.setTransmissionStatusSale(FuelEventTransmissionStatus._NEW_);
		fe.setTransmissionStatusPurchase(FuelEventTransmissionStatus._NEW_);
		fe.setRevocationReason(RevocationReason._EMPTY_);
		fuelEventService.insert(fe);
		return fe;
	}

	public BigDecimal calculateSysPayableCost(FuelEvent fe) throws BusinessException {
		if (fe.getPayableCostCurrency() == null) {
			return null;
		}
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		Density sysDensity = this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity();
		ICurrenciesService currencySrv = (ICurrenciesService) this.findEntityService(Currencies.class);
		Currencies sysCurrency = currencySrv.findByCode(paramSrv.getSysCurrency());
		IAverageMethodService avgMthSrv = (IAverageMethodService) this.findEntityService(AverageMethod.class);
		AverageMethod stdAvgMth = avgMthSrv.findByName(paramSrv.getSysAverageMethod());
		FinancialSources stdFinSrc = this.getStdFinancialSource();
		PriceConverterService priceConverterService = this.getApplicationContext().getBean(PriceConverterService.class);
		String accrualDateParameter = paramSrv.getAccrualEvaluationDate();
		Date date = GregorianCalendar.getInstance().getTime();
		if (accrualDateParameter.equalsIgnoreCase(AccrualDateParameter.EVENT_DATE.getValue())) {
			date = fe.getFuelingDate();
		}
		return priceConverterService.convert(fe.getUnit(), fe.getUnit(), fe.getPayableCostCurrency(), sysCurrency, fe.getPayableCost(), date,
				stdFinSrc, stdAvgMth, sysDensity, false, MasterAgreementsPeriod._CURRENT_).getValue();
	}

	/**
	 * @param fe
	 * @param dn
	 * @throws BusinessException
	 */
	public void setSystemQuantity(FuelEvent fe, DeliveryNote dn) throws BusinessException {
		ISystemParameterService srv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		IUnitService unitSrv = (IUnitService) this.findEntityService(Unit.class);
		UnitConverterService unitConverterService = this.getApplicationContext().getBean(UnitConverterService.class);
		Unit sysVol = unitSrv.findByCode(srv.getSysVol());
		Unit sysUnit = unitSrv.findByCode(srv.getSysWeight());
		Density density = new Density(sysUnit, sysVol, Double.parseDouble(srv.getDensity()));
		density = this.getDensity(density, dn);
		fe.setSysQuantity(unitConverterService.convert(fe.getUnit(), sysVol, fe.getQuantity(), density));
	}

	private Density getDensity(Density density, DeliveryNote dn) throws BusinessException {
		if (dn.getSource().equals(QuantitySource._FUEL_TICKET_)) {
			IFuelTicketService fuelTicketService = (IFuelTicketService) this.findEntityService(FuelTicket.class);
			FuelTicket ft = fuelTicketService.findById(dn.getObjectId());
			if (ft.getDensity() != null) {
				return new Density(ft.getDensityUnit(), ft.getDensityVolume(), ft.getDensity().doubleValue());
			}
		}
		return density;
	}

	public FinancialSources getStdFinancialSource() throws BusinessException {
		IFinancialSourcesService finSrcSrv = (IFinancialSourcesService) this.findEntityService(FinancialSources.class);
		Map<String, Object> params = new HashMap<>();
		params.put("isStdFlg", true);
		params.put("active", true);
		return finSrcSrv.findEntityByAttributes(params);
	}

}
