/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.ext.deliveryNotes.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteContractService;
import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IInvoiceLineService;
import atraxo.acc.business.api.receivableAccruals.IReceivableAccrualService;
import atraxo.acc.business.ext.deliveryNotes.delegate.DeliveryNoteCostCalculatorDelegate;
import atraxo.acc.business.ext.deliveryNotes.delegate.DeliveryNoteInvoiceStatus_Bd;
import atraxo.acc.business.ext.deliveryNotes.delegate.DeliveryNoteReallocation_Bd;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.fuelEvents.delegate.FuelEvent_Bd;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.business.ext.exceptions.OutRangedFuelEventsExceptions;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.ContractStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import seava.j4e.api.enums.DateFormatAttribute;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link DeliveryNote} domain entity.
 */
public class DeliveryNote_Service extends atraxo.acc.business.impl.deliveryNotes.DeliveryNote_Service implements IDeliveryNoteService {

	private static final Logger LOG = LoggerFactory.getLogger(DeliveryNote_Service.class);

	@Autowired
	private ISystemParameterService sysParamSrv;
	@Autowired
	private IDeliveryNoteContractService dncService;
	@Autowired
	private IFuelEventService feService;
	@Autowired
	private IFuelTicketService fuelTicketService;
	@Autowired
	private IContractService cSrv;
	@Autowired
	private IReceivableAccrualService accSrv;
	@Autowired
	private IInvoiceLineService invoiceLineService;

	@SuppressWarnings("unchecked")
	@Override
	public void reallocate(Object object) throws BusinessException {
		DeliveryNoteReallocation_Bd bd = this.getBusinessDelegate(DeliveryNoteReallocation_Bd.class);
		List<Contract> list = (List<Contract>) object;
		bd.reallocate(list);
	}

	/**
	 * @param dn
	 * @return System density or in case that delivery note is generated from fuel ticket than the density from fuel ticket.
	 * @throws BusinessException
	 */
	@Override
	public double getDensity(DeliveryNote dn) throws BusinessException {
		double density = Double.parseDouble(this.sysParamSrv.getDensity());
		if (QuantitySource._FUEL_TICKET_.equals(dn.getSource())) {
			FuelTicket ft = this.fuelTicketService.findById(dn.getObjectId());
			if (ft.getDensity() != null) {
				density = ft.getDensity().doubleValue();
			}
		}
		return density;
	}

	@Override
	protected void postUpdate(DeliveryNote e) throws BusinessException {
		super.postUpdate(e);
		if (e.getUsed()) {
			FuelEvent fuelEvent = e.getFuelEvent();
			fuelEvent.setPayableCost(e.getPayableCost());
			fuelEvent.setPayableCostCurrency(e.getPayableCostCurrency());
			fuelEvent.setSysPayableCost(this.getBusinessDelegate(FuelEvent_Bd.class).calculateSysPayableCost(fuelEvent));
			this.feService.update(fuelEvent);
		}
	}

	@Override
	public void propagateIncInvoiceStatus(InvoiceLine invoiceLine) throws BusinessException {
		DeliveryNoteInvoiceStatus_Bd dnis = this.getBusinessDelegate(DeliveryNoteInvoiceStatus_Bd.class);
		BillStatus billStatus = invoiceLine.getInvoice().getStatus();
		if (billStatus.equals(BillStatus._DRAFT_)) {
			billStatus = BillStatus._NOT_BILLED_;
		}
		dnis.modifyDeliveryNotesSources(billStatus, invoiceLine);
	}

	@Override
	public List<DeliveryNote> findByObjectIdAndType(Integer objectId, String objectType) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("objectId", objectId);
		params.put("objectType", objectType);
		return this.findEntitiesByAttributes(params);
	}

	@Override
	public void deleteDeliveryNote(Integer objectId, String objectType) throws BusinessException {
		List<DeliveryNote> list = this.findByObjectIdAndType(objectId, objectType);
		for (DeliveryNote dn : list) {
			this.deleteDeliveryNote(dn);
		}
	}

	@Override
	public void resetSupplierContract(Contract contract) throws BusinessException {
		DeliveryNoteCostCalculatorDelegate bd = this.getBusinessDelegate(DeliveryNoteCostCalculatorDelegate.class);

		List<DeliveryNote> deliveryNotes = new ArrayList<>();
		List<DeliveryNoteContract> deliveryNoteContracts = this.dncService.findByContracts(contract);
		for (DeliveryNoteContract dnc : deliveryNoteContracts) {
			deliveryNotes.add(dnc.getDeliveryNotes());
		}
		this.filterDeliveryNotes(contract, deliveryNotes);
		for (DeliveryNote dn : deliveryNotes) {
			this.addRemoveDeliveryNoteContract(dn, contract, false);
			bd.recalculatePayableCost(dn);
		}
		this.update(deliveryNotes);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService#addRemoveDeliveryNoteContract(atraxo.acc.domain.impl.deliveryNotes.DeliveryNote,
	 * atraxo.cmm.domain.impl.contracts.Contract, boolean) addRemove - true add new DeliveryNoteContract, false remove.
	 */
	@Override
	public void addRemoveDeliveryNoteContract(DeliveryNote deliveryNote, Contract contract, boolean addRemove) throws BusinessException {
		if (addRemove) {
			this.addDeliveryNoteContract(deliveryNote, contract);
		} else {
			this.removeDeliveryNoteContract(deliveryNote, contract);
		}
	}

	/**
	 * @param deliveryNote
	 * @param contract
	 */
	private void removeDeliveryNoteContract(DeliveryNote deliveryNote, Contract contract) {
		Collection<DeliveryNoteContract> deliveryNoteContracts = deliveryNote.getDeliveryNoteContracts();
		Iterator<DeliveryNoteContract> iterator = deliveryNoteContracts.iterator();
		while (iterator.hasNext()) {
			DeliveryNoteContract dnc = iterator.next();
			if (dnc.getContracts().getId() == contract.getId()) {
				iterator.remove();
			}
		}
	}

	private void addDeliveryNoteContract(DeliveryNote deliveryNote, Contract contract) {
		boolean isAdded = false;
		for (DeliveryNoteContract dnc : deliveryNote.getDeliveryNoteContracts()) {
			if (dnc.getContracts().getId() == contract.getId()) {
				isAdded = true;
			}
		}
		if (!isAdded) {
			DeliveryNoteContract dnc = new DeliveryNoteContract();
			dnc.setContracts(contract);
			dnc.setDeliveryNotes(deliveryNote);
			deliveryNote.addToDeliveryNoteContracts(dnc);
		}
	}

	@Override
	public void removeDeliveryNoteContract(Contract contract) throws BusinessException {
		List<DeliveryNoteContract> dnContracts = this.dncService.findByContracts(contract);
		List<DeliveryNote> updateList = new ArrayList<>();
		DeliveryNoteCostCalculatorDelegate bd = this.getBusinessDelegate(DeliveryNoteCostCalculatorDelegate.class);
		for (DeliveryNoteContract dnc : dnContracts) {
			DeliveryNote dn = dnc.getDeliveryNotes();
			if (dn.getFuelingDate().before(contract.getValidFrom()) || dn.getFuelingDate().after(contract.getValidTo())) {
				updateList.add(dn);
				dn.getDeliveryNoteContracts().remove(dnc);
				bd.recalculatePayableCost(dn);
			}
		}
		this.update(updateList);
	}

	@Override
	public void maintainDeliveryNotesContracts(Contract contract) throws BusinessException {
		if (contract.getStatus().equals(ContractStatus._DRAFT_) || contract.getStatus().equals(ContractStatus._ACTIVE_)) {
			return;
		}
		switch (contract.getDealType()) {
		case _BUY_:
			this.assignBuyContract(contract);
			break;
		case _SELL_:
			this.assignSaleContract(contract);
			break;
		case _EMPTY_:
		default:
			break;
		}
	}

	/**
	 * @param contract
	 * @return a list of all the fuel events that have the same clientId, customer departure location as the contract
	 */
	private List<FuelEvent> findFuelEventsBasedOnClientCustomerDeparture(Contract contract) {
		Map<String, Object> paramsFuelEvent = new HashMap<>();
		paramsFuelEvent.put("clientId", contract.getClientId());
		paramsFuelEvent.put("customer", contract.getCustomer());
		paramsFuelEvent.put("departure", contract.getLocation());
		return this.feService.findEntitiesByAttributes(paramsFuelEvent);
	}

	/**
	 * Verifies fuel events
	 *
	 * @param contract
	 * @throws BusinessException
	 */
	@Override
	public void checkOutRangedFuelEvents(Contract contract) throws BusinessException {
		List<FuelEvent> list = new ArrayList<>();
		if (DealType._BUY_.equals(contract.getDealType())) {
			List<DeliveryNoteContract> dncList = this.dncService.findByContract(contract);
			for (DeliveryNoteContract dnc : dncList) {
				list.add(dnc.getDeliveryNotes().getFuelEvent());
			}
		} else {
			list = this.findFuelEventsBasedOnBillPrice(contract);
		}
		Iterator<FuelEvent> iter = list.iterator();
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = Session.user.get().getSettings().getDateFormat(DateFormatAttribute.JAVA_DATE_FORMAT);
		while (iter.hasNext()) {
			FuelEvent fuelEvent = iter.next();
			Date fuelingDate = DateUtils.truncate(fuelEvent.getFuelingDate(), Calendar.DAY_OF_MONTH);
			if (!fuelingDate.before(contract.getValidFrom()) && !fuelingDate.after(contract.getValidTo())) {
				iter.remove();
			} else {
				sb.append(fuelEvent.getFuelSupplier().getName()).append(" - ").append(fuelEvent.getShipTo().getName()).append(" - ")
						.append(sdf.format(fuelingDate)).append(", ");
			}
		}
		if (!CollectionUtils.isEmpty(list)) {
			if (contract.getValidFrom().equals(contract.getValidTo())) {
				throw new BusinessException(CmmErrorCode.RESET_CONTRACTS_ERROR, CmmErrorCode.RESET_CONTRACTS_ERROR.getErrMsg());
			} else {
				throw new OutRangedFuelEventsExceptions(CmmErrorCode.OUT_RANGED_FUEL_EVENTS,
						String.format(CmmErrorCode.OUT_RANGED_FUEL_EVENTS.getErrMsg(), sb.toString().substring(0, sb.lastIndexOf(","))));
			}
		}
	}

	/**
	 * @param contract
	 * @return a list of all the fuel events that have the the billPriceSource the contract and has the same billPriceSourceType
	 */
	private List<FuelEvent> findFuelEventsBasedOnBillPrice(Contract contract) {
		Map<String, Object> paramsContract = new HashMap<>();
		paramsContract.put("billPriceSourceId", contract.getId());
		paramsContract.put("billPriceSourceType", Contract.class.getSimpleName());
		return this.feService.findEntitiesByAttributes(paramsContract);
	}

	private void assignSaleContract(Contract contract) throws BusinessException {
		List<FuelEvent> fuelEvents = this.findFuelEventsBasedOnClientCustomerDeparture(contract);
		List<FuelEvent> oldList = this.findFuelEventsBasedOnBillPrice(contract);
		List<FuelEvent> updateList = new ArrayList<>();
		for (FuelEvent fe : fuelEvents) {
			if (this.isContractAssignable(fe, contract)) {
				updateList.add(fe);
				fe.setBillPriceSourceId(contract.getId());
				fe.setBillPriceSourceType(Contract.class.getSimpleName());
				fe.setSubsidiaryId(contract.getSubsidiaryId());
				this.feService.calculateReceivableCost(fe);
			}
		}
		List<FuelEvent> removeLink = this.getDifferenceFuelEvent(oldList, updateList);
		List<ReceivableAccrual> deleteList = new ArrayList<>();
		for (FuelEvent fe : removeLink) {
			if (this.isAwaitingPaymentOrPaid(fe) && !this.isBetweenContractBoundaries(fe, contract)) {
				throw new BusinessException(AccErrorCode.INVOICE_STATUS_PAY, AccErrorCode.INVOICE_STATUS_PAY.getErrMsg());
			}
			fe.setBillableCost(null);
			fe.setBillableCostCurrency(null);
			fe.setBillPriceSourceId(null);
			fe.setBillPriceSourceType(null);
			fe.setBillStatus(BillStatus._NOT_BILLED_);
			deleteList.addAll(new ArrayList<>(fe.getReceivableAccruals()));
		}
		this.feService.update(updateList);
		this.feService.update(removeLink);
		this.accSrv.delete(deleteList);
	}

	private boolean isAwaitingPaymentOrPaid(FuelEvent fe) {
		return (fe.getInvoiceStatus().equals(OutgoingInvoiceStatus._AWAITING_PAYMENT_) || fe.getInvoiceStatus().equals(OutgoingInvoiceStatus._PAID_))
				? true : false;
	}

	private void assignBuyContract(Contract contract) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("fuelSupplier", contract.getSupplier());
		params.put("departure", contract.getLocation());
		List<DeliveryNote> deliveryNotes = this.findEntitiesByAttributes(params);
		this.filterDeliveryNotes(contract, deliveryNotes);
		List<DeliveryNoteContract> actualList = new ArrayList<>();
		List<DeliveryNoteContract> oldList = this.dncService.findByContracts(contract);
		List<DeliveryNoteContract> deleteList = new ArrayList<>();
		for (DeliveryNote dn : deliveryNotes) {
			if (this.isContractAssignable(dn, contract, deleteList)) {
				DeliveryNoteContract dnc = new DeliveryNoteContract();
				dnc.setContracts(contract);
				dnc.setDeliveryNotes(dn);
				actualList.add(dnc);
			}
		}
		List<DeliveryNoteContract> insertList = this.getDifference(actualList, oldList);
		deleteList.addAll(this.getDifference(oldList, actualList));
		this.deleteDeliveryNoteContract(deleteList);
		this.dncService.insert(insertList);
		this.getBusinessDelegate(DeliveryNoteCostCalculatorDelegate.class).recalculatePayableCost(deliveryNotes);
	}

	private void filterDeliveryNotes(Contract contract, List<DeliveryNote> deliveryNotes) {
		Map<String, Object> params = new HashMap<>();
		params.put("dealType", DealType._SELL_);
		params.put("resaleRef", contract);
		List<Contract> scList = this.cSrv.findEntitiesByAttributes(params);
		Iterator<DeliveryNote> iter = deliveryNotes.iterator();
		while (iter.hasNext()) {
			DeliveryNote dn = iter.next();
			if (dn.getFuelingDate().compareTo(contract.getValidFrom()) < 0 || dn.getFuelingDate().compareTo(contract.getValidTo()) > 0) {
				iter.remove();
			} else if (!"Contract".equals(dn.getFuelEvent().getBillPriceSourceType()) || !this.hasResaleReference(dn.getFuelEvent(), scList)) {
				iter.remove();
			}
		}
	}

	private boolean hasResaleReference(FuelEvent fuelEvent, List<Contract> scList) {
		for (Contract c : scList) {
			if (c.getId().equals(fuelEvent.getBillPriceSourceId())) {
				return true;
			}
		}

		return false;
	}

	private void deleteDeliveryNoteContract(List<DeliveryNoteContract> deleteList) throws BusinessException {
		if (CollectionUtils.isEmpty(deleteList)) {
			return;
		}
		for (DeliveryNoteContract dnc : deleteList) {
			dnc.getDeliveryNotes().getDeliveryNoteContracts().remove(dnc);
		}
		this.dncService.delete(deleteList);
	}

	private boolean isBetweenContractBoundaries(FuelEvent fe, Contract contract) {
		return !(fe.getFuelingDate().compareTo(contract.getValidFrom()) < 0 || fe.getFuelingDate().compareTo(contract.getValidTo()) > 0);
	}

	private boolean isContractAssignable(FuelEvent fe, Contract contract) {
		if (!this.isBetweenContractBoundaries(fe, contract)) {
			return false;
		}
		if (fe.getBillPriceSourceType() != null && fe.getBillPriceSourceType().equalsIgnoreCase(FuelOrder.class.getSimpleName())) {
			return false;
		}
		switch (fe.getEventType()) {
		case _DOMESTIC_:
			if (contract.getLimitedTo().equals(FlightTypeIndicator._DOMESTIC_)
					|| (contract.getLimitedTo().equals(FlightTypeIndicator._UNSPECIFIED_) && fe.getBillPriceSourceId() == null)) {
				return true;
			}
			break;
		case _EMPTY_:
			return false;
		case _INTERNATIONAL_:
			if (contract.getLimitedTo().equals(FlightTypeIndicator._INTERNATIONAL_)
					|| (contract.getLimitedTo().equals(FlightTypeIndicator._UNSPECIFIED_) && fe.getBillPriceSourceId() == null)) {
				return true;
			}
			break;
		case _UNSPECIFIED_:
			if (contract.getLimitedTo().equals(FlightTypeIndicator._UNSPECIFIED_)) {
				return true;
			}
			break;
		default:
			break;
		}
		return false;
	}

	private boolean isContractAssignable(DeliveryNote dn, Contract contract, List<DeliveryNoteContract> deleteList) {
		if (dn.getFuelingDate().compareTo(contract.getValidFrom()) < 0 || dn.getFuelingDate().compareTo(contract.getValidTo()) > 0) {
			return false;
		}
		switch (dn.getEventType()) {
		case _DOMESTIC_:
			if (contract.getLimitedTo().equals(FlightTypeIndicator._DOMESTIC_)) {
				for (DeliveryNoteContract dnc : dn.getDeliveryNoteContracts()) {
					if (dnc.getContracts().getLimitedTo().equals(FlightTypeIndicator._UNSPECIFIED_)) {
						this.addToDeleteList(deleteList, dnc);
					}
					if (ContractStatus._DRAFT_.equals(dnc.getContracts().getStatus())) {
						this.addToDeleteList(deleteList, dnc);
					}
				}
				return true;
			}
			if (contract.getLimitedTo().equals(FlightTypeIndicator._UNSPECIFIED_)) {
				for (DeliveryNoteContract dnc : dn.getDeliveryNoteContracts()) {
					if (dnc.getContracts().getLimitedTo().equals(FlightTypeIndicator._DOMESTIC_)) {
						return false;
					}
				}
				return true;
			}
			break;
		case _EMPTY_:
			return false;
		case _INTERNATIONAL_:
			if (contract.getLimitedTo().equals(FlightTypeIndicator._INTERNATIONAL_)) {
				for (DeliveryNoteContract dnc : dn.getDeliveryNoteContracts()) {
					if (dnc.getContracts().getLimitedTo().equals(FlightTypeIndicator._UNSPECIFIED_)) {
						this.addToDeleteList(deleteList, dnc);
					}
					if (ContractStatus._DRAFT_.equals(dnc.getContracts().getStatus())) {
						this.addToDeleteList(deleteList, dnc);
					}
				}
				return true;
			}
			if (contract.getLimitedTo().equals(FlightTypeIndicator._UNSPECIFIED_)) {
				for (DeliveryNoteContract dnc : dn.getDeliveryNoteContracts()) {
					if (dnc.getContracts().getLimitedTo().equals(FlightTypeIndicator._INTERNATIONAL_)) {
						return false;
					}
				}
				return true;
			}
			break;
		case _UNSPECIFIED_:
			if (contract.getLimitedTo().equals(FlightTypeIndicator._UNSPECIFIED_)) {
				return true;
			}
			break;
		default:
			break;
		}
		return false;
	}

	private void addToDeleteList(List<DeliveryNoteContract> deleteList, DeliveryNoteContract dnc) {
		if (!deleteList.contains(dnc)) {
			deleteList.add(dnc);
		}
	}

	private List<DeliveryNoteContract> getDifference(List<DeliveryNoteContract> fromList, List<DeliveryNoteContract> referenceList) {
		List<DeliveryNoteContract> retList = new ArrayList<>();
		for (DeliveryNoteContract dncFrom : fromList) {
			boolean is = true;
			for (DeliveryNoteContract dncReference : referenceList) {
				if (dncFrom.equals(dncReference)) {
					is = false;
				}
			}
			if (is) {
				retList.add(dncFrom);
			}
		}
		return retList;
	}

	/**
	 * @param fromList - old list
	 * @param referenceList - update list
	 * @return
	 */
	private List<FuelEvent> getDifferenceFuelEvent(List<FuelEvent> fromList, List<FuelEvent> referenceList) {
		List<FuelEvent> retList = new ArrayList<>();
		for (FuelEvent from : fromList) {
			boolean is = true;
			for (FuelEvent reference : referenceList) {
				if (from.getId().equals(reference.getId())) {
					is = false;
				}
			}
			if (is) {
				retList.add(from);
			}
		}
		return retList;
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService#deleteDeliveryNote(atraxo.acc.domain.impl.deliveryNotes.DeliveryNote)
	 */
	@Override
	@Transactional
	public void deleteDeliveryNote(DeliveryNote deliveryNote) throws BusinessException {
		if (this.canDelete(deliveryNote)) {
			FuelEvent fe = deliveryNote.getFuelEvent();
			Collection<DeliveryNote> notes = fe.getDeliveryNotes();
			if (notes.size() <= 1) {
				this.feService.delete(Arrays.asList(fe));
			} else {
				notes.remove(deliveryNote);
				this.setUsed(notes);

				boolean hasTicketSource = this.checkDeliveryNotesSouces(notes);
				if (!hasTicketSource) {
					this.feService.calculateReceivableCost(fe);
					this.removeBillableCost(deliveryNote);
				}
				this.feService.update(fe);
			}
		} else {
			throw new BusinessException(AccErrorCode.CAN_NOT_DELETE_DELIVERY_NOTE, AccErrorCode.CAN_NOT_DELETE_DELIVERY_NOTE.getErrMsg());
		}
	}

	/**
	 * Check if i can delete the note.
	 *
	 * @param dn - Delivery note
	 * @return
	 */
	public boolean canDelete(DeliveryNote dn) {
		return !OutgoingInvoiceStatus._PAID_.equals(dn.getFuelEvent().getInvoiceStatus())
				&& !OutgoingInvoiceStatus._AWAITING_PAYMENT_.equals(dn.getFuelEvent().getInvoiceStatus())
				&& !BillStatus._AWAITING_PAYMENT_.equals(dn.getFuelEvent().getBillStatus())
				&& !BillStatus._PAID_.equals(dn.getFuelEvent().getBillStatus());
	}

	private boolean checkDeliveryNotesSouces(Collection<DeliveryNote> notes) {
		boolean hasTicketSource = false;
		for (DeliveryNote delivNote : notes) {
			if (QuantitySource._FUEL_TICKET_.equals(delivNote.getSource())) {
				hasTicketSource = true;
				break;
			}
		}
		return hasTicketSource;
	}

	private void removeBillableCost(DeliveryNote dn) throws BusinessException {
		if (QuantitySource._FUEL_ORDER_.equals(dn.getSource())) {
			FuelEvent fe = dn.getFuelEvent();
			fe.setBillableCost(null);
			fe.setBillableCostCurrency(null);
			fe.setBillPriceSourceId(null);
			fe.setBillPriceSourceType(null);
		}
	}

	private void setUsed(Collection<DeliveryNote> notes) throws BusinessException {
		if (!this.isUsed(notes) && !this.setDNFuelTicketSourceUsed(notes, QuantitySource._FUEL_TICKET_)
				&& !this.setDNFuelTicketSourceUsed(notes, QuantitySource._INVOICE_)) {
			this.setDNFuelTicketSourceUsed(notes, QuantitySource._FUEL_ORDER_);
		}
	}

	/**
	 * Return true if one from the collection is marked as used.
	 *
	 * @param notes
	 * @return
	 */
	private boolean isUsed(Collection<DeliveryNote> notes) {
		for (DeliveryNote dn : notes) {
			if (dn.getUsed()) {
				return true;
			}
		}
		return false;
	}

	private boolean setDNFuelTicketSourceUsed(Collection<DeliveryNote> notes, QuantitySource source) throws BusinessException {
		for (DeliveryNote dn : notes) {
			if (dn.getSource().equals(source)) {
				dn.setUsed(true);

				FuelEvent fe = dn.getFuelEvent();
				if (fe.getSubsidiaryId() == null && CollectionUtils.isEmpty(dn.getDeliveryNoteContracts())) {
					fe.setSubsidiaryId(dn.getDeliveryNoteContracts().iterator().next().getContracts().getSubsidiaryId());
				}
				fe.setQuantity(dn.getQuantity());
				this.getBusinessDelegate(FuelEvent_Bd.class).setSystemQuantity(fe, dn);
				fe.setUnit(dn.getUnit());
				fe.setQuantitySource(source);
				fe.setRegenerateDetails(false);
				this.feService.update(fe);
				this.update(dn);
				return true;
			}
		}
		return false;
	}

	@Override
	@Transactional
	public void removeByOutgoingInvoiceLines(List<OutgoingInvoiceLine> outgoingInvoiceLines) throws BusinessException {
		for (OutgoingInvoiceLine line : outgoingInvoiceLines) {
			Map<String, Object> params = new HashMap<>();
			params.put("fuelEvent", line.getFuelEvent());
			params.put("objectType", InvoiceLine.class.getSimpleName());

			DeliveryNote note = null;
			try {
				note = this.findEntityByAttributes(params);
			} catch (ApplicationException e) {
				LOG.warn(e.getMessage(), e);
			}

			if (note != null) {
				InvoiceLine invoiceLine = this.invoiceLineService.findById(note.getObjectId());
				if (line.getOutgoingInvoice().getInvoiceNo().equals(invoiceLine.getInvoice().getInvoiceNo())) {
					this.delete(note);
				} else {
					throw new BusinessException(AccErrorCode.DELIVERY_NOTE_NOT_FOUND,
							String.format(AccErrorCode.DELIVERY_NOTE_NOT_FOUND.getErrMsg(), line.getOutgoingInvoice().getInvoiceNo()));
				}
			}
		}
	}

}
