package atraxo.acc.business.ext.creditMemo;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.invoiceLines.IOutgoingInvoiceLineService;
import atraxo.acc.business.api.invoices.IInvoiceService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.domain.impl.acc_type.ControlNo;
import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.business.api.documentNumberSeries.IDocumentNumberSeriesService;
import atraxo.fmbas.business.ext.exceptions.DocumentSeriesOutOfBoundException;
import atraxo.fmbas.business.ext.util.EntityCloner;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author apetho
 */
public class CreditMemo_Service extends AbstractBusinessBaseService {

	private static final String ENTITY_CLONE = "Entity clone.";

	private static final Logger LOG = LoggerFactory.getLogger(CreditMemo_Service.class);

	@Autowired
	private IInvoiceService incomingInvoiceService;
	@Autowired
	private IOutgoingInvoiceLineService oilService;
	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;
	@Autowired
	private IDocumentNumberSeriesService documentNumberSeriesService;
	@Autowired
	private IDeliveryNoteService deliveryNoteService;

	/**
	 * Create a credit memo from invoice lines.
	 *
	 * @param invoice - The invoice header.
	 * @param invoiceLines - List of invoice lines.
	 * @param reason - Reason, why is cancelled the invoice.
	 * @return - The generated credit memo.
	 * @throws BusinessException
	 */
	public OutgoingInvoice createAndInsertCreditMemo(OutgoingInvoice invoice, List<OutgoingInvoiceLine> invoiceLines, RevocationReason reason)
			throws BusinessException {
		if (!BillStatus._AWAITING_PAYMENT_.equals(invoice.getStatus()) && !BillStatus._PAID_.equals(invoice.getStatus())) {
			throw new BusinessException(AccErrorCode.INVOICE_STATUS_GENERATE_CREDIT_NOTE,
					AccErrorCode.INVOICE_STATUS_GENERATE_CREDIT_NOTE.getErrMsg());
		}

		OutgoingInvoice creditMemo = this.generateCreditMemo(invoice, invoiceLines);
		creditMemo.setCreditMemoReason(reason);
		this.outgoingInvoiceService.insert(creditMemo);
		OutgoingInvoice generatedCreditMemo = this.outgoingInvoiceService.findByRefid(creditMemo.getRefid());

		// update the Outgoing Invoice who was just credited
		this.oilService.updateWithoutBusiness(invoiceLines);
		this.outgoingInvoiceService.update(invoice);

		if (BillStatus._AWAITING_PAYMENT_.equals(generatedCreditMemo.getStatus())) {
			this.outgoingInvoiceService.createInvoiceReport(generatedCreditMemo);
		}

		if (BillStatus._AWAITING_PAYMENT_.equals(generatedCreditMemo.getStatus()) && this.outgoingInvoiceService.hasAllLinesCredited(invoice)) {
			this.outgoingInvoiceService.markAsCredited(invoice);
			this.incomingInvoiceService.markAsCredited(invoice);
		}

		return generatedCreditMemo;
	}

	/**
	 * Add new invoice line to an existing credit memo.
	 *
	 * @param invoice - The invoice header.
	 * @param invoiceLines - List of invoice lines.
	 * @return - Collection of the new invoice lines.
	 * @throws BusinessException
	 */
	public Collection<OutgoingInvoiceLine> addNewInvoiceLine(OutgoingInvoice invoice, List<OutgoingInvoiceLine> invoiceLines)
			throws BusinessException {
		Collection<OutgoingInvoiceLine> newCreditMemoLines = this.getCreditMemoLine(invoiceLines, invoice);
		invoice.getInvoiceLines().addAll(newCreditMemoLines);
		this.calculateCosts(invoice);
		this.adjustHeaderDates(invoice);
		this.outgoingInvoiceService.update(invoice);
		this.oilService.updateWithoutBusiness(invoiceLines);
		invoice.setStatus(BillStatus._CREDITED_);
		return newCreditMemoLines;
	}

	protected OutgoingInvoice generateCreditMemo(OutgoingInvoice invoice, List<OutgoingInvoiceLine> invoiceLines) throws BusinessException {
		OutgoingInvoice newOi = this.generateHeader(invoice);
		Collection<OutgoingInvoiceLine> newInvoiceLines = this.getCreditMemoLine(invoiceLines, newOi);
		newOi.setInvoiceLines(newInvoiceLines);
		newOi.setItemsNumber(newInvoiceLines.size());
		this.calculateCosts(newOi);
		this.adjustHeaderDates(newOi);
		// status will be changed at postInsert()
		newOi.setStatus(BillStatus._EMPTY_);
		newOi.setTransactionType(TransactionType._CORRECTED_);
		newOi.setTransmissionStatus(InvoiceTransmissionStatus._NEW_);
		newOi.setApprovalStatus(BidApprovalStatus._NEW_);
		return newOi;
	}

	private void calculateCosts(OutgoingInvoice outgoingInvoice) {
		BigDecimal quantity = BigDecimal.ZERO;
		BigDecimal netAmount = BigDecimal.ZERO;
		BigDecimal vatAmount = BigDecimal.ZERO;
		for (OutgoingInvoiceLine oil : outgoingInvoice.getInvoiceLines()) {
			quantity = quantity.add(oil.getQuantity(), MathContext.DECIMAL64);
			netAmount = netAmount.add(oil.getAmount(), MathContext.DECIMAL64);
			vatAmount = vatAmount.add(oil.getVat(), MathContext.DECIMAL64);
		}
		outgoingInvoice.setQuantity(quantity);
		outgoingInvoice.setNetAmount(netAmount);
		outgoingInvoice.setVatAmount(vatAmount);
		outgoingInvoice.setTotalAmount(netAmount.add(vatAmount, MathContext.DECIMAL64));
	}

	protected OutgoingInvoice generateHeader(OutgoingInvoice sourceInvoice) throws BusinessException {
		try {
			OutgoingInvoice newInvoice = EntityCloner.cloneEntity(sourceInvoice, new String[] { "invoiceReference", "invoiceLines" });
			newInvoice.setInvoiceReference(sourceInvoice);
			newInvoice.setTotalAmount(newInvoice.getTotalAmount().negate());
			newInvoice.setNetAmount(newInvoice.getNetAmount().negate());
			newInvoice.setVatAmount(newInvoice.getVatAmount().negate());
			newInvoice.setIssueDate(Calendar.getInstance().getTime());
			newInvoice.setInvoiceDate(Calendar.getInstance().getTime());
			newInvoice.setInvoiceType(InvoiceTypeAcc._CRN_);
			newInvoice.setInvoiceSent(false);
			newInvoice.setInvoiceNo(this.getInvoiceNumberSeries());
			newInvoice.setControlNo(ControlNo._NOT_AVAILABLE_.getName());

			return newInvoice;
		} catch (BeansException e) {
			LOG.warn(ENTITY_CLONE, e);
		}
		return null;
	}

	protected Collection<OutgoingInvoiceLine> getCreditMemoLine(List<OutgoingInvoiceLine> invoiceLines, OutgoingInvoice oi) throws BusinessException {
		Collection<OutgoingInvoiceLine> retList = new ArrayList<>();
		for (OutgoingInvoiceLine oil : invoiceLines) {
			try {
				OutgoingInvoiceLine newOil = EntityCloner.cloneEntity(oil, new String[] { "outgoingInvoice" });
				newOil.setReferenceInvoiceLine(oil);
				newOil.setIsCredited(false);
				newOil.setAmount(newOil.getAmount().negate());
				newOil.setVat(newOil.getVat().negate());
				newOil.setErpReference(StringUtils.EMPTY);
				newOil.setOutgoingInvoice(oi);
				newOil.setDetails(this.getInvoiceLineDetails(oil.getDetails(), newOil));
				newOil.setTransmissionStatus(InvoiceLineTransmissionStatus._NEW_);
				oil.setIsCredited(true);
				retList.add(newOil);
			} catch (BeansException e) {
				LOG.warn(ENTITY_CLONE, e);
			}

			this.deliveryNoteService.removeByOutgoingInvoiceLines(invoiceLines);
		}
		return retList;
	}

	private Collection<OutgoingInvoiceLineDetails> getInvoiceLineDetails(Collection<OutgoingInvoiceLineDetails> details, OutgoingInvoiceLine newOil) {
		Collection<OutgoingInvoiceLineDetails> retList = new ArrayList<>();
		for (OutgoingInvoiceLineDetails oild : details) {
			try {
				OutgoingInvoiceLineDetails newOild = EntityCloner.cloneEntity(oild, new String[] { "outgoingInvoice", "outgoingInvoiceLine" });
				newOild.setOutgoingInvoiceLine(newOil);
				newOild.setOutgoingInvoice(newOil.getOutgoingInvoice());
				newOild.setSettlementAmount(oild.getSettlementAmount().negate());
				newOild.setVatAmount(oild.getVatAmount() != null ? oild.getVatAmount().negate() : BigDecimal.ZERO);
				retList.add(newOild);
			} catch (BeansException e) {
				LOG.warn(ENTITY_CLONE, e);
			}

		}
		return retList;
	}

	/**
	 * Delete the credit memo and it's invoice lines.
	 *
	 * @param outgoingInvoice
	 * @throws BusinessException
	 */
	public void deleteCreditMemo(OutgoingInvoice outgoingInvoice) throws BusinessException {
		if (BillStatus._AWAITING_PAYMENT_.equals(outgoingInvoice.getStatus()) || BillStatus._PAID_.equals(outgoingInvoice.getStatus())) {
			throw new BusinessException(AccErrorCode.CREDIT_NOTE_STATUS_DELETE, AccErrorCode.CREDIT_NOTE_STATUS_DELETE.getErrMsg());
		}
		List<OutgoingInvoiceLine> updateList = new ArrayList<>();
		for (OutgoingInvoiceLine oilTemp : outgoingInvoice.getInvoiceLines()) {
			oilTemp.getReferenceInvoiceLine().setIsCredited(false);
			updateList.add(oilTemp.getReferenceInvoiceLine());
		}
		this.oilService.updateWithoutBusiness(updateList);
		this.outgoingInvoiceService.deleteById(outgoingInvoice.getId());
	}

	/**
	 * After an add or remove invoice line recalculate the header values.
	 *
	 * @param outgoingInvoice
	 * @throws BusinessException
	 */
	public void adjustHeaderDates(OutgoingInvoice outgoingInvoice) throws BusinessException {
		List<OutgoingInvoiceLine> invoiceLines = new ArrayList(outgoingInvoice.getInvoiceLines());
		if (CollectionUtils.isEmpty(invoiceLines)) {
			return;
		}
		invoiceLines.sort((o1, o2) -> o1.getDeliveryDate().compareTo(o2.getDeliveryDate()));
		Date validFrom = invoiceLines.get(0).getDeliveryDate();
		Date validTo = invoiceLines.get(invoiceLines.size() - 1).getDeliveryDate();
		outgoingInvoice.setDeliveryDateFrom(validFrom);
		outgoingInvoice.setDeliverydateTo(validTo);
	}

	private String getInvoiceNumberSeries() throws BusinessException {
		try {
			synchronized (this.documentNumberSeriesService) {
				return this.documentNumberSeriesService.getNextDocumentNumber(DocumentNumberSeriesType._CREDIT_MEMO_);
			}
		} catch (DocumentSeriesOutOfBoundException e) {
			LOG.warn("Warning:could not retrieve the next document number, using invoice number from current date!", e);
			return "CM-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		}
	}
}
