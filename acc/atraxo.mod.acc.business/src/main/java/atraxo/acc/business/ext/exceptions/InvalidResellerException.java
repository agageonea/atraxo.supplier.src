package atraxo.acc.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class InvalidResellerException extends BusinessException {

	public InvalidResellerException() {
		super(AccErrorCode.INVALID_RESELLER, AccErrorCode.INVALID_RESELLER.getErrMsg());
	}

}
