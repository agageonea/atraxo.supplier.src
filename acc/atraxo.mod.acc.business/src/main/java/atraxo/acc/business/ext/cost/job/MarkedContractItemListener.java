package atraxo.acc.business.ext.cost.job;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;

import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.JobRunResult;
import seava.j4e.scheduler.batch.listener.DefaultActionListener;

public class MarkedContractItemListener extends DefaultActionListener {

	private static final String NR_CONTRACTS = "nrContracts";
	private static final String SUCCESS = "success";
	private static final String FAILED = "failed";
	private static final String STRUCTURE = "structure";

	@Override
	public void afterJob(JobExecution jobExecution) {
		ExitStatus status = jobExecution.getExitStatus();

		List<StepExecution> stepExecution = (List<StepExecution>) jobExecution.getStepExecutions();

		String msg = this.buildMessage(status, stepExecution);
		if (!status.getExitCode().equals(ExitStatus.FAILED.getExitCode())) {
			status = new ExitStatus(JobRunResult._COMPLETED___SUCCESSFUL_.toString(), msg);
		} else {
			status = new ExitStatus(JobRunResult._COMPLETED___FAILED_.toString(), msg);
		}
		jobExecution.setExitStatus(status);
	}

	private String buildMessage(ExitStatus status, List<StepExecution> stepExecution) {
		StringBuilder sb = new StringBuilder();
		for (StepExecution se : stepExecution) {
			ExecutionContext executionContext = se.getExecutionContext();
			if (executionContext.containsKey(NR_CONTRACTS)) {
				int nrContracts = (Integer) executionContext.get(NR_CONTRACTS);
				sb.append(String.format(AccErrorCode.FUEL_COST_PROCESSED_CONTRACTS.getErrMsg(), nrContracts));
			}
			if (executionContext.containsKey(SUCCESS)) {
				int notfailed = executionContext.getInt(SUCCESS, 0);
				sb.append("\n");
				sb.append(AccErrorCode.FUEL_COST_SUCCESFULL_UPDATED.getErrMsg());
				sb.append(notfailed);
			}
			if (executionContext.containsKey(FAILED)) {
				sb.append("\n");
				String failed = executionContext.getString(FAILED);
				sb.append(AccErrorCode.FUEL_COST_FAILED_UPDATED.getErrMsg());
				sb.append(failed.isEmpty() ? "0" : failed);
				status.and(ExitStatus.FAILED);
			}
			if (executionContext.containsKey(STRUCTURE)) {
				int structure = executionContext.getInt(STRUCTURE, 0);
				sb.append("\n");
				sb.append(String.format(AccErrorCode.FUEL_EVENT_DETAILS_UPDATED.getErrMsg(), structure));
			}
		}
		return sb.toString();
	}
}
