/**
 *
 */
package atraxo.acc.business.ext.bpm.delegate.invoice.notifications;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.domain.impl.acc_type.ExportTypeInvoice;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.business.ext.exceptions.AdErrorCode;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author vhojda
 */
public class GenerateReportsDelegate extends AbstractInvoiceNotificationDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateReportsDelegate.class);

	private static final int NR_INVOICES_PACK = 10;

	@Autowired
	private IExternalReportService externalReportService;

	@Override
	@SuppressWarnings("unchecked")
	public void executeDelegate() throws Exception {

		Object repordIdObj = this.execution.getVariable(WorkflowVariablesConstants.REPORT_ID);
		String reportName = (String) this.execution.getVariable(WorkflowVariablesConstants.REPORT_NAME);

		ExternalReport externalReport = this.getExternalReport(repordIdObj, reportName);
		if (externalReport != null) {

			List<Attachment> reports = this.getWkfReports();
			List<OutgoingInvoice> invoices = this.getWkfInvoices();
			Integer offsetInvoices = this.getWkfInvoiceOffset();

			int startIndex = offsetInvoices;
			int endIndex = offsetInvoices + NR_INVOICES_PACK;
			int maxIndex = invoices.size();

			for (int i = startIndex; i < endIndex; i++) {
				OutgoingInvoice invoice = invoices.get(i);

				LOGGER.info("Process BULK EXPORT WKF for invoice " + invoice.getInvoiceNo() + " with index " + i + "/" + endIndex + "/" + maxIndex);
				try {
					this.processInvoice(invoice, repordIdObj, reports);
				} catch (Exception e) {
					this.execution.setVariable(WorkflowVariablesConstants.SKIP_TO_GENERATE_REPORTS, Boolean.TRUE);
					throw e;
				}

				offsetInvoices++;

				if (offsetInvoices == maxIndex) {
					break;
				}
			}

			// set common variables
			this.execution.setVariable(WorkflowVariablesConstants.MAP_REPORTS, reports);
			this.execution.setVariable(WorkflowVariablesConstants.MAP_NOTIFICATIONS, invoices);
			this.execution.setVariable(WorkflowVariablesConstants.MAP_INVOICES_OFFSET, offsetInvoices);

			// set continue to generate report variable
			this.execution.setVariable(WorkflowVariablesConstants.SKIP_TO_GENERATE_REPORTS, offsetInvoices == maxIndex);

		} else {
			this.execution.setVariable(WorkflowVariablesConstants.SKIP_TO_GENERATE_REPORTS, Boolean.TRUE);
		}

	}

	private Integer getWkfInvoiceOffset() {
		Integer invoicesOffset = (Integer) this.execution.getVariable(WorkflowVariablesConstants.MAP_INVOICES_OFFSET);
		if (invoicesOffset == null) {
			invoicesOffset = Integer.valueOf(0);
		}
		return invoicesOffset;
	}

	private List<OutgoingInvoice> getWkfInvoices() {
		List<OutgoingInvoice> invoices = (List<OutgoingInvoice>) this.execution.getVariable(WorkflowVariablesConstants.MAP_NOTIFICATIONS);
		if (invoices == null) {
			invoices = new ArrayList<>();
		}
		return invoices;
	}

	private List<Attachment> getWkfReports() {
		List<Attachment> reports = (List<Attachment>) this.execution.getVariable(WorkflowVariablesConstants.MAP_REPORTS);
		if (reports == null) {
			reports = new ArrayList<>();
		}
		return reports;
	}

	/**
	 * @param invoice
	 * @param repordIdObj
	 * @param reports
	 * @throws BusinessException
	 */
	private void processInvoice(OutgoingInvoice invoice, Object repordIdObj, List<Attachment> reports) throws BusinessException {
		Attachment attach = this.externalReportService.generateSaleInvoiceReportSync(invoice.getId().toString(), repordIdObj.toString(),
				ExportTypeInvoice._PDF_.getName(), invoice.getEntityAlias(),
				invoice.getReceiver().getCode() + "_" + invoice.getDeliveryLoc().getCode() + "_" + invoice.getInvoiceNo() + ".pdf");
		if (attach != null) {
			reports.add(attach);
		}
	}

	/**
	 * @param repordIdObj
	 * @param reportName
	 * @return
	 */
	private ExternalReport getExternalReport(Object repordIdObj, String reportName) {
		ExternalReport externalReport = null;
		if (repordIdObj != null) {
			externalReport = this.externalReportService.findById(repordIdObj);
			if (externalReport == null) {
				this.execution.setVariable(WorkflowVariablesConstants.SKIP_TO_NOTIFICATIONS, true);
				this.execution.setVariable(WorkflowVariablesConstants.ERROR_NOTIFICATION,
						String.format(AdErrorCode.REPORT_NOT_FOUND.getErrMsg(), reportName));
			}
		}
		return externalReport;
	}

	@Override
	public void afterExecuteDelegate() {
		// do nothing
	}

	@Override
	public String getDelegateErrorCode() {
		return AdErrorCode.GENERAL_REPORT_CENTER_ERROR.getErrMsg();
	}
}
