package atraxo.acc.business.ext.invoices.job.export;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;

import seava.j4e.scheduler.batch.listener.DefaultActionListener;

public class ExportSaleInvoicesListener extends DefaultActionListener {

	@SuppressWarnings("unchecked")
	@Override
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);

		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder();
		List<StepExecution> stepExecution = (List<StepExecution>) jobExecution.getStepExecutions();

		for (StepExecution se : stepExecution) {
			ExecutionContext executionContext = se.getExecutionContext();
			if (executionContext.containsKey(ExportSaleInvoicesTasklet.SUCCESS) || executionContext.containsKey(ExportSaleInvoicesTasklet.FAILED)) {
				List<String> success = (List<String>) executionContext.get(ExportSaleInvoicesTasklet.SUCCESS);
				List<String> failed = (List<String>) executionContext.get(ExportSaleInvoicesTasklet.FAILED);
				sb.append("Number of SUCCESS exported invoices: ").append(success.size()).append("\n");
				for (String fileName : success) {
					sb.append("\t - ").append(fileName).append(";").append("\n");
				}
				sb.append("Number of FAILED invoices: ").append(failed.size()).append("\n");
				for (String invoice : failed) {
					sb.append("\t - ").append(invoice).append(";").append("\n");
				}
			} else {
				sb.append("There are no Sales Invoices or Credit Memos to export.");
			}
		}
		status = new ExitStatus(status.getExitCode(), sb.toString());
		jobExecution.setExitStatus(status);

	}
}
