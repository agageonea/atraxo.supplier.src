/**
 *
 */
package atraxo.acc.business.ext.bpm.delegate.invoice.notifications;

import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ad.domain.impl.ad.Category;
import atraxo.ad.domain.impl.ad.NotificationAction;
import atraxo.ad.domain.impl.ad.Priority;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.fmbas_type.NotificationType;
import atraxo.fmbas.domain.impl.fmbas_type.WorkflowInstanceStatus;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.fmbas.domain.impl.user.UserSupp;

/**
 * @author vhojda
 */
public class SendNotificationsDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendNotificationsDelegate.class);

	@Autowired
	private INotificationService notifySrv;

	@Override
	public void execute() throws Exception {
		String error = (String) this.execution.getVariable(WorkflowVariablesConstants.ERROR_NOTIFICATION);
		boolean skipToNotification = (boolean) this.execution.getVariable(WorkflowVariablesConstants.SKIP_TO_NOTIFICATIONS);

		try {
			Notification notification;
			if (skipToNotification) {
				notification = this.buildNotification(Category._ERROR_, WorkflowInstanceStatus._FAILURE_, "link", error);
			} else {
				notification = this.buildNotificationForDownload();
			}

			UserSupp user = (UserSupp) this.execution.getVariable(WorkflowVariablesConstants.USER);
			this.notifySrv.addUsersToNotification(notification, user, NotificationType._EVENT_, null);
			this.notifySrv.sendAndSave(notification, Collections.emptyList(), Collections.emptyMap());

			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR, "Notifications sent.");
		} catch (Exception e) {
			LOGGER.error("ERROR:could not send notifications !", e);
			this.throwBpmErrorForWorkflow(BusinessErrorCode.WORKFLOW_UNKNOWN_ERROR.getErrMsg(), "Could not send notifications :" + e.getMessage(), e);
		}
	}

	/**
	 * @param category
	 * @param status
	 * @return
	 */
	private Notification buildNotificationForDownload() {
		AttachmentType type = (AttachmentType) this.execution.getVariable(WorkflowVariablesConstants.ATTACHMENT_TYPE);
		String baseUrl = type.getBaseUrl();
		String zipName = (String) this.execution.getVariable(WorkflowVariablesConstants.ZIP_NAME);

		String downloadPath = new StringBuilder(baseUrl).append('/')
				.append(this.execution.getVariable(WorkflowVariablesConstants.ZIP_REFID).toString()).append('/').append(zipName).toString();

		Notification notification = this.buildNotification(Category._ACTIVITY_, WorkflowInstanceStatus._SUCCESS_, downloadPath, zipName);
		notification.setMethodName("downloadZip");
		notification.setMethodParam("'" + downloadPath + "'");

		return notification;
	}

	/**
	 * @param category
	 * @param status
	 * @param description
	 * @return
	 */
	private Notification buildNotification(Category category, WorkflowInstanceStatus status, String link, String description) {
		Notification notification = new Notification();
		notification.setTitle("BULK INVOICE EXPORT");
		notification.setLink(link);
		notification.setAction(NotificationAction._NOACTION_);
		notification.setEventDate(GregorianCalendar.getInstance().getTime());
		notification.setLifeTime(GregorianCalendar.getInstance().getTime());
		notification.setCategory(category);
		notification.setDescripion(description);
		notification.setPriority(Priority._DEFAULT_);
		notification.setStatus(status.getName());
		notification.setUsers(new ArrayList<UserSupp>());
		return notification;
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

}
