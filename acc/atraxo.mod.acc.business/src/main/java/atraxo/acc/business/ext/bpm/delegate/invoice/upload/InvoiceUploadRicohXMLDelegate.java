package atraxo.acc.business.ext.bpm.delegate.invoice.upload;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.domain.impl.acc_type.ControlNo;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;
import atraxo.fmbas.business.ext.ftp.SessionFactoryBuilder;
import atraxo.fmbas.business.ext.ftp.workflow.FtpWriteFiles;

public class InvoiceUploadRicohXMLDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceUploadRicohXMLDelegate.class);

	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;

	@Autowired
	private IChangeHistoryService historyService;

	@Override
	public void execute() throws Exception {
		LOGGER.info("Start upload to ftp");

		// Get params from first step
		String xml = (String) this.execution.getVariable(WorkflowVariablesConstants.INVOICE_RICOH_XML);
		String fileName = (String) this.execution.getVariable(WorkflowVariablesConstants.INVOICE_RICOH_FILE_NAME);

		Integer invoiceId = (Integer) this.execution.getVariable(WorkflowVariablesConstants.INVOICE_ID_VAR);
		OutgoingInvoice invoice = this.outgoingInvoiceService.findByBusiness(invoiceId);

		// Get ftp worflow params
		String ftpServer = (String) this.execution.getVariable(WorkflowVariablesConstants.FTP_SERVER);
		Integer ftpServerPort = (Integer) this.execution.getVariable(WorkflowVariablesConstants.FTP_SERVER_PORT);
		String ftpRemoteFolder = (String) this.execution.getVariable(WorkflowVariablesConstants.FTP_REMOTE_FOLDER);
		String ftpServerUsername = (String) this.execution.getVariable(WorkflowVariablesConstants.FTP_SERVER_USERNAME);
		String ftpServerPassword = (String) this.execution.getVariable(WorkflowVariablesConstants.FTP_SERVER_PASSWORD);

		try {
			// Initiate ftp connection
			FtpWriteFiles ftp = (FtpWriteFiles) this.applicationContext.getBean("ftpWriteFiles", SessionFactoryBuilder.getInstance()
					.buildSessionFactory(ftpServer, ftpServerUsername, ftpServerPassword, ftpServerPort.toString(), Boolean.TRUE, null),
					ftpRemoteFolder, "xml");

			// Build file path for ftp
			String filePath = ftpRemoteFolder.concat("/").concat("Process").concat("/").concat(fileName).concat(".xml");

			Map<String, String> ftpFiles = new HashMap<>();
			ftpFiles.put(filePath, xml);

			// Upload file to ftp
			ftp.execute(ftpFiles);

			// Update Controle Number
			invoice.setControlNo(ControlNo._WAITING_FOR_.getName());
			this.outgoingInvoiceService.update(invoice);

			// Save to history
			String message = "File " + fileName + " successfully uploaded to ricoh printer";
			this.historyService.saveChangeHistory(invoice, "Send to Ricoh printer", message);

			this.execution.setVariable(WorkflowVariablesConstants.TASK_ACCEPTANCE_VAR, true);
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR, message);
		} catch (Exception e) {
			LOGGER.error("Could not upload invoice ricoh xml to ftp", e);
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_RELEASE, e.getMessage(), e);
		}

		LOGGER.info("End upload to ftp");
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

}
