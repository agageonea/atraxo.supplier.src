package atraxo.acc.business.ext.exposure;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.creditLines.ICreditLinesService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.exposure.IExposureService;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.fmbas_type.ExposureLastAction;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class ExposureUpdater_Service extends AbstractBusinessBaseService {
	private static final Logger LOG = LoggerFactory.getLogger(ExposureUpdater_Service.class);

	@Autowired
	private IExposureService expSrv;
	@Autowired
	private IContractService contractSrv;
	@Autowired
	private ICustomerService custSrv;
	@Autowired
	private ICreditLinesService creditLineSrv;

	private static final String PRICE_SOURCE_TYPE = "Contract";
	private static final String CHANGE_REASON = "Update";

	/**
	 * @param ev
	 * @throws BusinessException
	 */
	public void addToUtilization(FuelEvent ev) throws BusinessException {
		this.manageUtilization(ev, ExposureLastAction._DEBIT_);
	}

	/**
	 * @param ev
	 * @throws BusinessException
	 */
	public void updateUtilization(FuelEvent ev) throws BusinessException {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		if (ev.getBillPriceSourceType().equals(ExposureUpdater_Service.PRICE_SOURCE_TYPE)) {
			Contract contract = contractService.findById(ev.getBillPriceSourceId());
			CreditLines agreement = this.custSrv.getValidCreditLine(contract.getRiskHolder(), ev.getFuelingDate());
			CreditTerm type = this.getTypeForExposure(ev);

			if (agreement.getCreditTerm().getName().equals(type.getName())) {
				this.updateAvailability(agreement, this.getConvertedPrice(ev, agreement, ev.getOldBillableCost()), ExposureLastAction._CREDIT_);
				this.updateAvailability(agreement, this.getConvertedPrice(ev, agreement), ExposureLastAction._DEBIT_);
				this.creditLineSrv.updateCreditTriggerExposure(agreement, ExposureLastAction._RECONCILIATION_, CHANGE_REASON);
			}
		}
	}

	/**
	 * @param ev
	 * @throws BusinessException
	 */
	public void removeFromUtilization(FuelEvent ev) throws BusinessException {
		this.manageUtilization(ev, ExposureLastAction._CREDIT_);
	}

	/**
	 * @param inv
	 * @throws BusinessException
	 */
	public void removeFromUtilization(OutgoingInvoice inv) throws BusinessException {
		this.updateUtilization(inv, ExposureLastAction._CREDIT_);
	}

	/**
	 * @param inv
	 * @throws BusinessException
	 */
	public void addToUtilization(OutgoingInvoice inv) throws BusinessException {
		this.updateUtilization(inv, ExposureLastAction._DEBIT_);
	}

	private void updateUtilization(OutgoingInvoice inv, ExposureLastAction lastAction) throws BusinessException {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		Contract contract = contractService.findByCode(inv.getReferenceDocNo());
		CreditLines agreement = null;
		try {
			agreement = this.custSrv.getValidCreditLine(contract.getRiskHolder(), inv.getBaseLineDate());
		} catch (BusinessException e) {
			LOG.info("Exposure not found for Invoice: " + inv.getId(), e);
		}

		if (agreement != null) {
			CreditTerm type = this.getTypeForExposure(inv);
			if (agreement.getCreditTerm().getName().equals(type.getName())) {
				this.updateAvailability(agreement, this.getConvertedPrice(inv, agreement), lastAction);
				this.creditLineSrv.updateCreditTriggerExposure(agreement, ExposureLastAction._CREDIT_, ExposureUpdater_Service.CHANGE_REASON);
			}
		}
	}

	/**
	 * Updates availability depending on case
	 *
	 * @param ev
	 * @param action
	 * @throws BusinessException
	 */
	private void manageUtilization(FuelEvent ev, ExposureLastAction action) throws BusinessException {
		IContractService contractService = (IContractService) this.findEntityService(Contract.class);
		if (ExposureUpdater_Service.PRICE_SOURCE_TYPE.equals(ev.getBillPriceSourceType())) {
			Contract contract = contractService.findById(ev.getBillPriceSourceId());
			CreditLines creditLine = null;
			try {
				creditLine = this.custSrv.getValidCreditLine(contract.getRiskHolder(), ev.getFuelingDate());
			} catch (BusinessException e) {
				LOG.info("Exposure not found for FuelEvent: " + ev.getId(), e);
			}

			if (creditLine != null) {
				CreditTerm type = this.getTypeForExposure(ev);
				if (creditLine.getCreditTerm().getName().equals(type.getName())) {
					this.updateAvailability(creditLine, this.getConvertedPrice(ev, creditLine), action);
					this.creditLineSrv.updateCreditTriggerExposure(creditLine, action, ExposureUpdater_Service.CHANGE_REASON);
				}
			}
		}
	}

	/**
	 * Convert fuel event billable cost to exposure currency. In case that otherCost is set that the first element from otherCost will be converted.
	 * In both cases the source currency is the event billable cost currency.
	 *
	 * @param ev
	 * @param exposure
	 * @param otherCost
	 * @return
	 * @throws BusinessException
	 */
	private BigDecimal getConvertedPrice(FuelEvent ev, CreditLines creditLines, BigDecimal... otherCost) throws BusinessException {
		ExchangeRate_Bd exchangeRateBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		BigDecimal cost = otherCost != null && otherCost.length > 0 ? otherCost[0] : ev.getBillableCost();
		ConversionResult conversionResult = exchangeRateBd.convert(ev.getBillableCostCurrency(), creditLines.getCurrency(), ev.getFuelingDate(), cost,
				false);
		return conversionResult.getValue();
	}

	/**
	 * @param fuelOrderLocation
	 * @param sysCurrency
	 * @param exchangeRate_Bd
	 * @return
	 * @throws BusinessException
	 */
	private BigDecimal getConvertedPrice(OutgoingInvoice inv, CreditLines creditLines) throws BusinessException {
		ExchangeRate_Bd exchangeRateBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		ConversionResult conversionResult = exchangeRateBd.convert(inv.getCurrency(), creditLines.getCurrency(), inv.getInvoiceDate(),
				inv.getTotalAmount(), false);
		return conversionResult.getValue();
	}

	/**
	 * Update exposure.
	 *
	 * @param exposure - {@link Exposure}.
	 * @param price - {@link BigDecimal}.
	 */
	private void updateAvailability(CreditLines creditLines, BigDecimal price, ExposureLastAction lastAction) {
		if (lastAction.equals(ExposureLastAction._DEBIT_)) {
			creditLines.setAvailability(creditLines.getAvailability() + price.negate().setScale(0, RoundingMode.HALF_UP).intValueExact());
		} else if (lastAction.equals(ExposureLastAction._CREDIT_)) {
			creditLines.setAvailability(creditLines.getAvailability() + price.setScale(0, RoundingMode.HALF_UP).intValueExact());
		}
	}

	/**
	 * @param ev
	 * @return
	 * @throws BusinessException
	 */
	private CreditTerm getTypeForExposure(FuelEvent ev) {
		if (ev.getBillPriceSourceType().equalsIgnoreCase(FuelOrderLocation.class.getSimpleName())) {
			try {
				CreditLines agreement = this.custSrv.getValidCreditLine(ev.getCustomer(), ev.getFuelingDate());
				return CreditTerm.getByName(agreement.getCreditTerm().getName());
			} catch (BusinessException e) {
				LOG.warn(e.getLocalizedMessage(), e);
				return null;
			}
		} else {
			Contract contract = this.contractSrv.findById(ev.getBillPriceSourceId());
			return contract.getCreditTerms();
		}
	}

	/**
	 * @param ev
	 * @return
	 * @throws BusinessException
	 */
	private CreditTerm getTypeForExposure(OutgoingInvoice inv) throws BusinessException {
		if (inv.getReferenceDocType().equals(InvoiceReferenceDocType._CONTRACT_)) {
			Contract contract = this.contractSrv.findById(inv.getReferenceDocId());
			return contract.getCreditTerms();
		} else {
			CreditLines cl = this.custSrv.getValidCreditLine(inv.getReceiver(), inv.getInvoiceDate());
			return CreditTerm.getByName(cl.getCreditTerm().getName());
		}
	}

}
