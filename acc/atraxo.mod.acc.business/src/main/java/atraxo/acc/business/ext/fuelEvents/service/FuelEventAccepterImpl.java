package atraxo.acc.business.ext.fuelEvents.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import atraxo.acc.business.api.ext.fuelEvents.IFuelEventAccepter;
import atraxo.acc.business.api.ext.invoices.IAccountInfoAcknoledgmentService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceHistoryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.externalInterfaces.processor.AbstractIncomingMessageProcessor;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ProcessStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TransportStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;

/**
 * @author zspeter
 */
public class FuelEventAccepterImpl extends AbstractIncomingMessageProcessor implements IFuelEventAccepter {

	private static final Logger LOG = LoggerFactory.getLogger(FuelEventAccepterImpl.class);

	@Autowired
	@Qualifier("accountInfoAcknoledgmentService")
	private IAccountInfoAcknoledgmentService srv;
	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageHistoryFacade;
	@Autowired
	private IExternalInterfaceHistoryService historyService;
	@Autowired
	private IExternalInterfaceService externalInterfaceService;

	@Override
	public MSG acknoledge(MSG request) throws BusinessException {
		String correlationID = "";
		boolean success = false;
		try {
			correlationID = request.getHeaderCommon().getCorrelationID();
			String clientIdentifier = GUIDMessageGenerator.extractClientIdentifier(correlationID);
			this.createSessionUser(clientIdentifier);
			synchronized (this.srv) {
				success = this.srv.processFuelEvent(request);
			}
			LOG.info("Fuel Event acknowledgement processed success!");
		} catch (Exception e) {
			LOG.warn("Fuel Event acknowledgement processed failure!", e);
			request.getHeaderCommon().setErrorCode(e.getMessage());
			request.getHeaderCommon().setErrorDescription(e.getMessage());
		}

		request.getHeaderCommon().setTransportStatus(TransportStatus._PROCESSED_.getName());
		request.getHeaderCommon().setProcessStatus(success ? ProcessStatus._SUCCESS_.getName() : ProcessStatus._FAILURE_.getName());

		this.updateMessageHistory(request, success);
		this.historyService.updateHistoryOutboundFromACK(success, request.getHeaderCommon().getCorrelationID(), true);

		return request;
	}

	private void createSessionUser(String clientIdentifier) throws BusinessException {
		try {
			this.userSrv.createSessionUser(clientIdentifier);
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOG.warn("Invalid client in correlation id!", ex);
				throw new BusinessException(AccErrorCode.INVALID_CORRELATION_ID, AccErrorCode.INVALID_CORRELATION_ID.getErrMsg());
			} else {
				throw ex;
			}
		}
	}

	private void updateMessageHistory(MSG message, boolean success) throws BusinessException {
		ExternalInterfaceMessageHistoryStatus status = success ? ExternalInterfaceMessageHistoryStatus._ACKNOWLEDGED_
				: ExternalInterfaceMessageHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_;
		this.messageHistoryFacade.updateMessageHistory(message, status);
	}

	@Override
	protected ExternalInterface getExternalInterface() {
		return this.externalInterfaceService.findByName(ExtInterfaceNames.ACK_FUEL_EVENT_INTERFACE.getValue());
	}
}
