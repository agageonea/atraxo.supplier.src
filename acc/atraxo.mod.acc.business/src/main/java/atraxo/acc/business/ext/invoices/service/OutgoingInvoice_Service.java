/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.ext.invoices.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.joda.time.DateTimeComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoiceLines.IOutgoingInvoiceLineService;
import atraxo.acc.business.api.invoices.IInvoiceService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.api.receivableAccruals.IReceivableAccrualService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.exposure.ExposureUpdater_Service;
import atraxo.acc.business.ext.fuelEvents.delegate.DensityDelegate;
import atraxo.acc.business.ext.invoices.delegate.GeneratorBd;
import atraxo.acc.business.ext.invoices.delegate.IncomingInvoiceGenerator_Bd;
import atraxo.acc.business.ext.receivableAccruals.delegate.ReceivableGenerator_Bd;
import atraxo.acc.business.ws.toNav.WSInvoiceToDTOTransformer;
import atraxo.acc.domain.ext.invoices.GeneratorResponse;
import atraxo.acc.domain.impl.acc_type.ExportTypeInvoice;
import atraxo.acc.domain.impl.acc_type.InvoiceExportStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceProcessStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.acc_type.InvoiceTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.ad.business.api.attachment.IAttachmentService;
import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.cmm.business.api.contracts.IContractInvoiceService;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.YesNo;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.tolerance.IToleranceService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.business.ext.tolerance.ToleranceResult;
import atraxo.fmbas.business.ext.tolerance.ToleranceVerifier;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.business.ext.util.LambdaUtil;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.client.PumaRestClient;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceCostToAccrual;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceType;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.enums.DateFormatAttribute;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link OutgoingInvoice} domain entity.
 *
 * @author abolindu
 * @author vhojda
 */
public class OutgoingInvoice_Service extends atraxo.acc.business.impl.invoices.OutgoingInvoice_Service implements IOutgoingInvoiceService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OutgoingInvoice_Service.class);

	@Autowired
	private ExposureUpdater_Service expSrv;
	@Autowired
	private IFuelEventService fuelEventService;
	@Autowired
	private IFuelTicketService ticketSrv;
	@Autowired
	private IContractService contractService;
	@Autowired
	private IFuelOrderLocationService fuelOrderLocationService;
	@Autowired
	private IContractInvoiceService ciService;
	@Autowired
	private IOutgoingInvoiceLineService invoiceLineService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private IToleranceService toleranceSrv;
	@Autowired
	private ISystemParameterService systemParameterService;
	@Autowired
	private ICurrenciesService curenciesSrv;
	@Autowired
	private IReceivableAccrualService recAccrualServ;
	@Autowired
	private UnitConverterService unitConverterService;
	@Autowired
	private ExternalInterfaceHistoryFacade historyFacade;
	@Autowired
	private IExternalInterfaceService externalInterfaceService;
	@Autowired
	private IExternalReportService externalReportService;
	@Autowired
	private IChangeHistoryService historyService;
	@Autowired
	private IAttachmentService attachmentService;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageFacade;
	@Autowired
	private IInvoiceService incomingInvoiceService;
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Override
	@Transactional
	public void updateTransmissionStatus(OutgoingInvoiceLine oil, InvoiceLineTransmissionStatus status, String transactionID)
			throws BusinessException {
		OutgoingInvoice oi = this.findById(oil.getOutgoingInvoice().getId());
		int failed = 0;
		int transmitted = 0;
		int inProgress = 0;
		for (OutgoingInvoiceLine o : oi.getInvoiceLines()) {
			if (o.getId().equals(oil.getId())) {
				o.setTransmissionStatus(status);
				o.setErpReference(transactionID);
				FuelEvent fe = o.getFuelEvent();
				fe.setErpSaleInvoiceNo(transactionID);
				this.fuelEventService.updateWithoutBusiness(fe);
			}
			switch (o.getTransmissionStatus()) {
			case _FAILED_:
				failed++;
				break;
			case _IN_PROGRESS_:
				inProgress++;
				break;
			case _TRANSMITTED_:
				transmitted++;
				break;
			default:
				break;
			}
		}
		if (failed == oi.getInvoiceLines().size()) {
			oi.setTransmissionStatus(InvoiceTransmissionStatus._FAILED_);
		} else if (transmitted == oi.getInvoiceLines().size()) {
			oi.setTransmissionStatus(InvoiceTransmissionStatus._TRANSMITTED_);
		} else if (transmitted < oi.getInvoiceLines().size()) {
			oi.setTransmissionStatus(InvoiceTransmissionStatus._PARTIALLY_TRANSMITTED_);
		}
		if (inProgress > 0) {
			oi.setTransmissionStatus(InvoiceTransmissionStatus._IN_PROGRESS_);
		}
		this.onUpdate(oi);
	}

	@Override
	@Transactional
	public GeneratorResponse generate(Date invoiceDate, Date dateTo, Date closeDate, TransactionType transactionType, Customer customer,
			boolean separatePerShipTo) throws BusinessException {
		return this.getBusinessDelegate(GeneratorBd.class).generate(invoiceDate, dateTo, closeDate, transactionType, separatePerShipTo, customer);
	}

	@Override
	@Transactional
	public GeneratorResponse generate(Date invoiceDate, Date dateTo, Date closeDate, TransactionType transactionType, Customer customer,
			boolean separatePerShipTo, Locations location) throws BusinessException {
		return this.getBusinessDelegate(GeneratorBd.class).generate(invoiceDate, dateTo, closeDate, transactionType, customer, separatePerShipTo,
				location);
	}

	@Override
	@Transactional
	public GeneratorResponse generate(Date invoiceDate, Date dateTo, Date closeDate, TransactionType transactionType, Customer customer,
			boolean separatePerShipTo, Area area) throws BusinessException {
		return this.getBusinessDelegate(GeneratorBd.class).generate(invoiceDate, dateTo, closeDate, transactionType, customer, separatePerShipTo,
				area);
	}

	@Override
	@Transactional
	public void updatePrice(Integer id, FuelEvent event) throws BusinessException {
		Density density = this.getBusinessDelegate(DensityDelegate.class).getDensity(event);
		@SuppressWarnings("unchecked")
		GeneratorBd<AbstractEntity> bd = this.getBusinessDelegate(GeneratorBd.class);
		OutgoingInvoice e = this.findById(id);
		this.resetInvoiceHeader(e);
		for (OutgoingInvoiceLine invLine : e.getInvoiceLines()) {
			if (invLine.getFuelEvent().equals(event)) {
				ReceivableAccrual accrual = event.getReceivableAccruals().iterator().next();
				invLine.setAmount(accrual.getAccrualAmountSet());
				invLine.setVat(accrual.getAccrualVATSet());
				invLine.setQuantity(this.unitConverterService.convert(event.getUnit(), e.getUnit(), event.getQuantity(), density));
				invLine.setDetails(null);
				bd.generateInvoiceLineDetails(invLine, event);
			}
			bd.adjustInvoiceHeader(invLine);
		}
		this.onUpdate(e);
	}

	private void resetInvoiceHeader(OutgoingInvoice e) {
		e.setItemsNumber(0);
		e.setNetAmount(BigDecimal.ZERO);
		e.setQuantity(BigDecimal.ZERO);
		e.setTotalAmount(BigDecimal.ZERO);
		e.setVatAmount(BigDecimal.ZERO);
	}

	@Override
	protected void preInsert(OutgoingInvoice e) throws BusinessException {
		super.preInsert(e);
		this.calculateDueDay(e);
		this.checkInvoiceDateValidity(e);
	}

	@Override
	protected void preUpdate(OutgoingInvoice e) throws BusinessException {
		super.preUpdate(e);
		this.calculateDueDay(e);
		this.checkInvoiceDateValidity(e);
	}

	/**
	 * @param e
	 * @throws BusinessException
	 */
	private void addChangeHistory(OutgoingInvoice e) throws BusinessException {

		// Add to history if the baseline has been changed
		if (e.getHistoryFlag() != null && e.getHistoryFlag()) {
			SimpleDateFormat sdf = Session.user.get().getSettings().getDateFormat(DateFormatAttribute.JAVA_DATE_FORMAT);
			this.generateSaveToHistory(e.getId(), "Base line date manually changed", "Modified from " + sdf.format(e.getBaseLineDate()));
		}
	}

	private void checkInvoiceDateValidity(OutgoingInvoice e) throws BusinessException {
		if (e.getInvoiceDate() != null && e.getDeliverydateTo() != null
				&& DateTimeComparator.getDateOnlyInstance().compare(e.getInvoiceDate(), e.getDeliverydateTo()) < 0) {
			throw new BusinessException(AccErrorCode.INVOICE_DATE_GRATER_THAN_DATE_TO, AccErrorCode.INVOICE_DATE_GRATER_THAN_DATE_TO.getErrMsg());
		}
		if (InvoiceTypeAcc._CRN_.equals(e.getInvoiceType())) {
			OutgoingInvoice oi = e.getInvoiceReference();
			if (DateTimeComparator.getDateOnlyInstance().compare(oi.getInvoiceDate(), e.getInvoiceDate()) > 0) {
				throw new BusinessException(AccErrorCode.CREDIT_NOTE_DATE_BEFORE_INVOICE, AccErrorCode.CREDIT_NOTE_DATE_BEFORE_INVOICE.getErrMsg());
			}
		} else {
			for (OutgoingInvoiceLine oil : e.getInvoiceLines()) {
				if (DateTimeComparator.getDateOnlyInstance().compare(oil.getFuelEvent().getFuelingDate(), e.getInvoiceDate()) > 0) {
					throw new BusinessException(AccErrorCode.INVOICE_DATE_BEFORE_FUEL_EVENT_FUELING_DATE,
							AccErrorCode.INVOICE_DATE_BEFORE_FUEL_EVENT_FUELING_DATE.getErrMsg());
				}
			}
		}
	}

	private void calculateDueDay(OutgoingInvoice outgoingInvoice) {
		if (outgoingInvoice.getReferenceDocType() != null) {
			// Dan: if the reference document type is a contract
			if (InvoiceReferenceDocType._CONTRACT_.equals(outgoingInvoice.getReferenceDocType())) {
				Contract contract = this.contractService.findById(outgoingInvoice.getReferenceDocId());
				if (contract != null) {
					this.setDueDay(outgoingInvoice, contract.getPaymentRefDay(), contract.getId(), contract.getPaymentTerms());
				}
			}
			// Dan: if the reference document type is a purchase order
			else if (InvoiceReferenceDocType._PURCHASE_ORDER_.equals(outgoingInvoice.getReferenceDocType())) {
				FuelOrderLocation fuelOrderLocation = this.fuelOrderLocationService.findById(outgoingInvoice.getReferenceDocId());
				if (fuelOrderLocation != null) {
					this.setDueDay(outgoingInvoice, fuelOrderLocation.getPaymentRefDay(), fuelOrderLocation.getId(),
							fuelOrderLocation.getPaymentTerms());
				}
			}
		}
	}

	/**
	 * Calculate the Base Line Date based on the payment terms or Closing Date. In case a closing date was specified, the baseline date is calculated
	 * using the closing date and not what was indicated on the contract (will be used one time at invoice generation).While a close date is defined
	 * or the base line date has been changed manually the due date will no longer be modified on update
	 *
	 * @param outgoingInvoice
	 * @param paymentDay
	 * @param refDocId
	 * @param paymentTerms
	 */
	private void setDueDay(OutgoingInvoice outgoingInvoice, PaymentDay paymentDay, Integer refDocId, Integer paymentTerms) {
		boolean useCloseDate = outgoingInvoice.getId() == null && outgoingInvoice.getCloseDate() != null;
		boolean canSetDueDate = outgoingInvoice.getCloseDate() == null || useCloseDate;
		Date deliveryDate = useCloseDate ? outgoingInvoice.getCloseDate() : this.setDeliveryDate(outgoingInvoice, paymentDay);

		if (deliveryDate == null || !canSetDueDate) {
			return;
		}

		Date limit = this.calculateLimit(paymentTerms, deliveryDate);
		OutgoingInvoice oldInvoice = outgoingInvoice.getId() == null ? null : this.findById(outgoingInvoice.getId());

		// Verify if the ref doc has been changed
		boolean refDocChanged = this.isRefDocChanged(refDocId, oldInvoice);

		// Verify if the invoice date has been changed
		boolean invoiceLineChanged = this.isInvoiceDateChanged(outgoingInvoice, oldInvoice);

		// Verify that the base line date has been changed
		boolean baseLineChanged = false;
		if (oldInvoice != null && oldInvoice.getBaseLineDate() != null) {
			Date oldDeliveryDate = useCloseDate ? outgoingInvoice.getCloseDate() : this.setDeliveryDate(oldInvoice, paymentDay);
			baseLineChanged = !this.substractLimit(paymentTerms, outgoingInvoice.getBaseLineDate()).equals(oldDeliveryDate);
		}

		if (outgoingInvoice.getBaseLineDate() == null || refDocChanged || (invoiceLineChanged && !baseLineChanged)) {
			outgoingInvoice.setBaseLineDate(limit);
		}
	}

	private boolean isInvoiceDateChanged(OutgoingInvoice outgoingInvoice, OutgoingInvoice oldInvoice) {
		return oldInvoice == null || (oldInvoice.getInvoiceDate() != null && !oldInvoice.getInvoiceDate().equals(outgoingInvoice.getInvoiceDate()));
	}

	private boolean isRefDocChanged(Integer refDocId, OutgoingInvoice oldInvoice) {
		return oldInvoice == null || oldInvoice.getReferenceDocId() == null || !oldInvoice.getReferenceDocId().equals(refDocId);
	}

	/**
	 * In function of the payment day return the correct value of delivery date.
	 *
	 * @param outgoingInvoice
	 * @param paymentDay
	 * @return
	 */
	private Date setDeliveryDate(OutgoingInvoice outgoingInvoice, PaymentDay paymentDay) {
		Date deliveryDate = null;
		switch (paymentDay) {
		case _INVOICE_DATE_:
			deliveryDate = outgoingInvoice.getInvoiceDate();
			break;
		case _INVOICE_RECEIVING_DATE_:
			deliveryDate = outgoingInvoice.getIssueDate();
			break;
		case _LAST_DELIVERY_DATE_:
			deliveryDate = outgoingInvoice.getDeliverydateTo();
			break;
		case _EMPTY_:
		default:
			break;
		}
		return deliveryDate;
	}

	/**
	 * Add to the input date the number of days from the payment terms.
	 *
	 * @param paymentTerm
	 * @param deliveryDate
	 * @return
	 */
	private Date calculateLimit(Integer paymentTerm, Date deliveryDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(deliveryDate);
		cal.add(Calendar.DAY_OF_YEAR, paymentTerm);
		return cal.getTime();
	}

	/**
	 * Substract from the base line date the number of days from the payment terms.
	 *
	 * @param paymentTerm
	 * @param baseLineDate
	 * @return
	 */
	private Date substractLimit(Integer paymentTerm, Date baseLineDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(baseLineDate);
		cal.add(Calendar.DAY_OF_YEAR, -1 * paymentTerm);
		return cal.getTime();
	}

	@Override
	protected void postInsert(OutgoingInvoice e) throws BusinessException {
		super.postInsert(e);
		if (InvoiceReferenceDocType._CONTRACT_.equals(e.getReferenceDocType())) {
			Contract contract = this.contractService.findById(e.getReferenceDocId());
			this.ciService.insertUpdateLink(contract, e.getId(), e.getInvoiceNo(), e.getStatus().getName(), e.getDeliveryDateFrom(),
					e.getDeliverydateTo(), DealType._SELL_);
		}

		boolean useSaleInvoiceWkf = this.systemParameterService.getSalesInvoiceApprovalWorkflow();
		boolean useCreditMemoWkf = this.systemParameterService.getCreditMemoApprovalWorkflow();
		boolean isCreditMemo = InvoiceTypeAcc._CRN_.equals(e.getInvoiceType());

		this.updateStatus(Arrays.asList(e), this.getBillStatus(isCreditMemo, useSaleInvoiceWkf, useCreditMemoWkf),
				this.getInvoiceStatus(e, isCreditMemo, useSaleInvoiceWkf));

		if (e.getStatus().equals(BillStatus._AWAITING_PAYMENT_)) {
			e.setTransmissionStatus(InvoiceTransmissionStatus._NEW_);
			this.update(e);
			this.exportToNAV(e);
		}

		if (isCreditMemo) {
			for (OutgoingInvoiceLine invLine : e.getInvoiceLines()) {
				FuelEvent event = this.fuelEventService.findById(invLine.getFuelEvent().getId());
				event.setCreditMemoNo(e.getInvoiceNo());
				event.setRegenerateDetails(false);
				this.fuelEventService.update(event);
			}
		}
	}

	/**
	 * @param isCreditMemo
	 * @param useSaleInvoiceWkf
	 * @param useCreditMemoWkf
	 * @return
	 */
	private BillStatus getBillStatus(boolean isCreditMemo, boolean useSaleInvoiceWkf, boolean useCreditMemoWkf) {
		if (isCreditMemo) {
			return useCreditMemoWkf ? BillStatus._AWAITING_APPROVAL_ : BillStatus._AWAITING_PAYMENT_;
		} else {
			return useSaleInvoiceWkf ? BillStatus._AWAITING_APPROVAL_ : BillStatus._AWAITING_PAYMENT_;
		}
	}

	/**
	 * @param outInvoice
	 * @param isCreditMemo
	 * @param useSaleInvoiceWkf
	 * @return
	 */
	private OutgoingInvoiceStatus getInvoiceStatus(OutgoingInvoice outInvoice, boolean isCreditMemo, boolean useSaleInvoiceWkf) {
		if (isCreditMemo) {
			if (BillStatus._AWAITING_PAYMENT_.equals(outInvoice.getInvoiceReference().getStatus())) {
				return OutgoingInvoiceStatus._AWAITING_PAYMENT_;
			} else if (BillStatus._PAID_.equals(outInvoice.getInvoiceReference().getStatus())) {
				return OutgoingInvoiceStatus._PAID_;
			} else {
				return OutgoingInvoiceStatus._CREDITED_;
			}
		} else {
			return useSaleInvoiceWkf ? OutgoingInvoiceStatus._AWAITING_APPROVAL_ : OutgoingInvoiceStatus._AWAITING_PAYMENT_;
		}
	}

	@Override
	protected void postUpdate(OutgoingInvoice e) throws BusinessException {
		super.postUpdate(e);
		if (InvoiceReferenceDocType._CONTRACT_.equals(e.getReferenceDocType())) {
			Contract contract = this.contractService.findById(e.getReferenceDocId());
			this.ciService.insertUpdateLink(contract, e.getId(), e.getInvoiceNo(), e.getStatus().getName(), e.getDeliveryDateFrom(),
					e.getDeliverydateTo(), DealType._SELL_);
		}
		this.addChangeHistory(e);

	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		for (Object id : ids) {
			this.ciService.removeLink((Integer) id, DealType._SELL_);
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		List<Object> invoiceLineIds = new ArrayList<>();
		List<OutgoingInvoice> outgoingInvoices = this.findByIds(ids);
		for (OutgoingInvoice invoice : outgoingInvoices) {
			for (OutgoingInvoiceLine il : invoice.getInvoiceLines()) {
				invoiceLineIds.add(il.getId());
			}
		}

		this.markSources(outgoingInvoices);
		if (!CollectionUtils.isEmpty(invoiceLineIds)) {
			this.invoiceLineService.deleteByIds(invoiceLineIds);
		}
		this.incomingInvoiceService.deleteByOutgoingInvoice(outgoingInvoices);
	}

	private void markSources(List<OutgoingInvoice> outgoingInvoices) throws BusinessException {
		this.updateStatus(outgoingInvoices, BillStatus._NOT_BILLED_, OutgoingInvoiceStatus._NOT_INVOICED_);
		this.update(outgoingInvoices);

	}

	@Override
	@Transactional
	@History(type = OutgoingInvoice.class)
	public void reject(List<OutgoingInvoice> list, @Action String action, @Reason String reason) throws BusinessException {
		for (OutgoingInvoice elem : list) {
			elem.setApprovalStatus(BidApprovalStatus._REJECTED_);
		}
		this.update(list);
	}

	@Override
	@Transactional
	@History(type = OutgoingInvoice.class, value = "Approved")
	public void approve(List<OutgoingInvoice> list, @Reason String reason) throws BusinessException {

		// set approval status (if the case)
		for (OutgoingInvoice inv : list) {
			// set approvalStatus to APPROVED only for CREDIT MEMO, because only credit memos have workflow and need this status changed to approve
			// operation (for invoice the approvalStatus is not important, it should remain unchanged)
			if (inv.getInvoiceType().equals(InvoiceTypeAcc._CRN_)) {
				inv.setApprovalStatus(BidApprovalStatus._APPROVED_);
			}
		}

		// update status and fuel events
		this.updateStatus(list, BillStatus._AWAITING_PAYMENT_, OutgoingInvoiceStatus._AWAITING_PAYMENT_, OutgoingInvoiceStatus._CREDITED_);

		// export to NAV and create reports
		for (OutgoingInvoice elem : list) {
			this.exportToNAV(elem);

			// if CREDIT MEMO and invoice reference is NOT PAID (see #SONE-3832 -comments)
			if (elem.getInvoiceType().equals(InvoiceTypeAcc._CRN_)) {
				OutgoingInvoice referenceInvoice = elem.getInvoiceReference();

				if (referenceInvoice != null && !referenceInvoice.getStatus().equals(BillStatus._PAID_)) {
					this.expSrv.addToUtilization(elem);
				}

				if (BillStatus._AWAITING_PAYMENT_.equals(elem.getStatus()) && this.hasAllLinesCredited(referenceInvoice)) {
					this.markAsCredited(referenceInvoice);
					// update the Incoming Invoice coresponding to credited Outgoing Invoice
					this.incomingInvoiceService.markAsCredited(referenceInvoice);
				}
			}

			// Create invoice report and save it to documents
			this.createInvoiceReport(elem);

			// Approve linked incoming invoice in resale scenario
			if (elem.getIssuer().getIsSubsidiary() && elem.getReceiver().getIsSubsidiary()) {
				this.updateResaleIncomingInvoice(elem);
			}
		}
		this.updateAccruals(list, false);

		// generate Incoming Invoice
		this.getBusinessDelegate(IncomingInvoiceGenerator_Bd.class).generate(list);
	}

	/**
	 * Create a sale invoice pdf report based on the contract invoice template. Upload the report to webdav and insert a attachment with the report
	 * linked to the sale invoice.In case of exceptions the approval process will continue and a history entry will be added with the detailes.
	 *
	 * @param elem
	 */
	@Override
	public void createInvoiceReport(OutgoingInvoice elem) {
		boolean canCreateReport = false;

		try {
			// Verify if the reference contract has the invoice type as PDF and a template
			Contract referenceContract = null;
			if (InvoiceReferenceDocType._CONTRACT_.equals(elem.getReferenceDocType())) {
				referenceContract = this.contractService.findById(elem.getReferenceDocId());
				if (referenceContract != null && InvoiceType._PDF_FORMAT_.equals(referenceContract.getInvoiceType())
						&& referenceContract.getInvoiceTemplate() != null) {
					canCreateReport = true;
				}
			}

			// Exit method if the report cannot be created
			if (!canCreateReport) {
				return;
			}

			// Extract the external invoice report template from the contract
			ExternalReport externalReport = referenceContract.getInvoiceTemplate();

			this.externalReportService.generateSaleInvoiceReport(elem.getId().toString(), externalReport.getId(), ExportTypeInvoice._PDF_.getName(),
					elem.getEntityAlias(), elem.getReceiver().getCode() + "_" + elem.getDeliveryLoc().getCode() + "_" + elem.getInvoiceNo() + ".pdf");

		} catch (Exception e) {
			LOGGER.warn("Failed to generate sale invoice report!", e);
			try {
				this.generateSaveToHistory(elem.getId(), "Failed_to_generate_report", e.getMessage());
			} catch (BusinessException e1) {
				LOGGER.warn("Could not save generate invoice report error to history !", e1);
			}
		}
	}

	private void generateSaveToHistory(Integer id, String objectValue, String remark) throws BusinessException {
		ChangeHistory ch = new ChangeHistory();
		ch.setObjectId(id);
		ch.setObjectType(OutgoingInvoice.class.getSimpleName());
		ch.setObjectValue(objectValue);
		ch.setRemarks(remark);
		this.historyService.insert(ch);
	}

	@Override
	@Transactional
	@History(type = OutgoingInvoice.class, value = "Approved")
	public void approve(List<OutgoingInvoice> list) throws BusinessException {
		this.approve(list, "");
	}

	@Override
	@History(type = OutgoingInvoice.class, value = "Paid")
	@Transactional(rollbackFor = BusinessException.class)
	public void markAsPaid(List<OutgoingInvoice> list) throws BusinessException {
		this.updateStatus(list, BillStatus._PAID_, OutgoingInvoiceStatus._PAID_);
		this.update(list);
		this.updateAccruals(list, false);
	}

	@Override
	@History(type = OutgoingInvoice.class, value = "Paid")
	@Transactional(rollbackFor = BusinessException.class)
	public void markAsPaidCreditMemo(OutgoingInvoice outgoingInvoice) throws BusinessException {
		this.updateStatus(Arrays.asList(outgoingInvoice), BillStatus._PAID_, OutgoingInvoiceStatus._NOT_INVOICED_);
		this.update(outgoingInvoice);
		this.updateAccruals(Arrays.asList(outgoingInvoice), true);
	}

	private void updateAccruals(List<OutgoingInvoice> list, boolean creditMemo) throws BusinessException {
		InvoiceCostToAccrual invoiceCostToAccrual = this.systemParameterService.getInvoiceCostToAccruals();
		Currencies sysCurrency = this.curenciesSrv.findByCode(this.systemParameterService.getSysCurrency());
		ExchangeRate_Bd bd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		List<ReceivableAccrual> accList = new ArrayList<>();
		for (OutgoingInvoice inv : list) {
			if (this.checkStatus(invoiceCostToAccrual, inv)) {
				accList = this.getAccList(sysCurrency, bd, inv);
				// if paid CREDIT MEMO do not update the credit utilization (see #SONE-3832 -comments)
				if (!creditMemo) {
					this.expSrv.removeFromUtilization(inv);
				}
			}
			this.recAccrualServ.update(accList);
		}
	}

	private List<ReceivableAccrual> getAccList(Currencies sysCurrency, ExchangeRate_Bd bd, OutgoingInvoice inv) throws BusinessException {
		List<ReceivableAccrual> accList = new ArrayList<>();
		for (OutgoingInvoiceLine invLine : inv.getInvoiceLines()) {
			for (ReceivableAccrual accrualTemp : invLine.getFuelEvent().getReceivableAccruals()) {
				ReceivableAccrual accrual = this.recAccrualServ.findById(accrualTemp.getId());
				BigDecimal sysValue = bd.convert(invLine.getOutgoingInvoice().getCurrency(), sysCurrency, GregorianCalendar.getInstance().getTime(),
						invLine.getAmount(), false).getValue();
				accrual.setInvoicedAmountSet(accrual.getInvoicedAmountSet().add(invLine.getAmount()));
				accrual.setInvoicedAmountSys(accrual.getInvoicedAmountSys().add(sysValue));
				accrual.setInvoicedQuantity(accrual.getInvoicedQuantity().add(this.getInSystemQuantity(
						invLine.getReferenceInvoiceLine() == null ? invLine.getQuantity() : invLine.getQuantity().negate(), inv.getUnit())));
				sysValue = bd.convert(invLine.getOutgoingInvoice().getCurrency(), sysCurrency, GregorianCalendar.getInstance().getTime(),
						invLine.getVat(), false).getValue();
				accrual.setInvoicedVATSet(accrual.getInvoicedVATSet().add(invLine.getVat()));
				accrual.setInvoicedVATSys(accrual.getInvoicedVATSys().add(sysValue));
				this.getBusinessDelegate(ReceivableGenerator_Bd.class).calculateAccruedCost(accrual);
				this.closeAccrual(accrual);
				accrual.setIsOpen(inv.getInvoiceReference() != null ? true : false);
				accList.add(accrual);
			}
		}
		return accList;
	}

	private boolean checkStatus(InvoiceCostToAccrual invoiceCostToAccrual, OutgoingInvoice inv) {
		return (inv.getStatus().equals(BillStatus._AWAITING_PAYMENT_) && !InvoiceCostToAccrual._ON_PAYMENT_.equals(invoiceCostToAccrual))
				|| (inv.getStatus().equals(BillStatus._PAID_) && InvoiceCostToAccrual._ON_PAYMENT_.equals(invoiceCostToAccrual));
	}

	private BigDecimal getInSystemQuantity(BigDecimal quantity, Unit fromUnit) throws BusinessException {
		String densityStr = this.systemParameterService.getDensity();
		Double density = Double.valueOf(densityStr);
		Unit toUnit = this.getSysVol(this.systemParameterService);
		return this.unitConverterService.convert(fromUnit, toUnit, quantity, density);
	}

	private Unit getSysVol(ISystemParameterService paramSrv) throws BusinessException {
		String sysVolume = paramSrv.getSysVol();
		return this.unitService.findByCode(sysVolume);
	}

	private void closeAccrual(ReceivableAccrual accrual) throws BusinessException {
		if (accrual.getExpectedAmountSys().equals(accrual.getAccrualAmountSys())) {
			accrual.setInvProcessStatus(InvoiceProcessStatus._NOT_INVOICED_);
			return;
		}
		Tolerance tolerance = this.toleranceSrv.getAccrualAutoCloseTolerance();
		ToleranceVerifier verifier = new ToleranceVerifier(this.unitConverterService, this.getBusinessDelegate(ExchangeRate_Bd.class),
				this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity());

		ToleranceResult result = verifier.checkTolerance(BigDecimal.ZERO, accrual.getAccrualAmountSys(), accrual.getAccrualCurrency(),
				accrual.getAccrualCurrency(), tolerance, GregorianCalendar.getInstance().getTime());
		switch (result.getType()) {
		case MARGIN:
		case WITHIN:
			accrual.setIsOpen(false);
			accrual.setInvProcessStatus(InvoiceProcessStatus._COMPLETELY_INVOICED_);
			break;
		case NOT_WITHIN:
			accrual.setInvProcessStatus(InvoiceProcessStatus._PARTIAL_INVOICED_);
			break;
		default:
			break;
		}
	}

	/**
	 * @param list
	 * @param billStatus
	 * @param invStatus
	 * @throws BusinessException
	 */
	private void updateStatus(List<OutgoingInvoice> list, BillStatus billStatus, OutgoingInvoiceStatus invStatus) throws BusinessException {
		this.updateStatus(list, billStatus, invStatus, OutgoingInvoiceStatus._EMPTY_);
	}

	/**
	 * @param list
	 * @param billStatus
	 * @param invoiceStatus
	 * @param creditMemoStatus
	 * @throws BusinessException
	 */
	private void updateStatus(List<OutgoingInvoice> list, BillStatus billStatus, OutgoingInvoiceStatus invoiceStatus,
			OutgoingInvoiceStatus creditMemoStatus) throws BusinessException {
		@SuppressWarnings("unchecked")
		GeneratorBd<AbstractEntity> bd = this.getBusinessDelegate(GeneratorBd.class);
		List<FuelEvent> eventList = new ArrayList<>();
		for (OutgoingInvoice inv : list) {
			inv.setStatus(billStatus);
			inv.setIssueDate(GregorianCalendar.getInstance().getTime());
			OutgoingInvoiceStatus invStatus = (InvoiceTypeAcc._CRN_.equals(inv.getInvoiceType())
					&& !OutgoingInvoiceStatus._EMPTY_.equals(creditMemoStatus)) ? creditMemoStatus : invoiceStatus;
			for (OutgoingInvoiceLine invLine : inv.getInvoiceLines()) {
				FuelEvent event = this.fuelEventService.findById(invLine.getFuelEvent().getId());
				if (OutgoingInvoiceStatus._CREDITED_.equals(event.getInvoiceStatus()) || invLine.getIsCredited()) {
					continue;
				}
				this.updateFuelTicket(invStatus, event);
				switch (inv.getReferenceDocType()) {
				case _PURCHASE_ORDER_:
				case _CONTRACT_:
					bd.adjustFlightEventStatus(event, invStatus);
					break;
				default:
					break;
				}
				event.setInvoiceStatus(invStatus);
				event.setRegenerateDetails(false);
				eventList.add(event);
			}
		}
		this.fuelEventService.update(eventList);
	}

	private void updateFuelTicket(OutgoingInvoiceStatus invStatus, FuelEvent event) throws BusinessException {
		if (QuantitySource._FUEL_TICKET_.equals(event.getQuantitySource())) {
			for (DeliveryNote dn : event.getDeliveryNotes()) {
				if (dn.getUsed()) {
					FuelTicket ticket = this.ticketSrv.findById(dn.getObjectId());
					ticket.setInvoiceStatus(invStatus);
					this.ticketSrv.updateWithoutBL(Arrays.asList(ticket));
				}
			}
		}
	}

	private void updateResaleIncomingInvoice(OutgoingInvoice outgoingInv) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("invoiceNo", outgoingInv.getInvoiceNo());
		List<Invoice> inv = this.incomingInvoiceService.findEntitiesByAttributes(params);
		this.incomingInvoiceService.modifyStatus(inv, "Approved", outgoingInv.getStatus());

	}

	@Override
	@History(type = OutgoingInvoice.class, value = "Credited")
	@Transactional
	public void markAsCredited(OutgoingInvoice outgoingInvoice) throws BusinessException {
		this.updateStatus(Arrays.asList(outgoingInvoice), BillStatus._CREDITED_, OutgoingInvoiceStatus._CREDITED_);
		this.update(outgoingInvoice);
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.api.invoices.IOutgoingInvoiceService#exportToNAV(atraxo.acc.domain.impl.invoices.OutgoingInvoice)
	 */
	@Transactional
	@Override
	public boolean exportToNAV(OutgoingInvoice invoice) throws BusinessException {
		boolean exported = true;

		List<OutgoingInvoiceLine> invoiceLines = (List<OutgoingInvoiceLine>) invoice.getInvoiceLines();
		try {
			if (invoice.getInvoiceType().equals(InvoiceTypeAcc._INV_)) {
				for (OutgoingInvoiceLine invoiceLine : invoiceLines) {
					this.startExternalInterface(invoiceLine, ExtInterfaceNames.EXPORT_INVOICES_TO_NAV.getValue(), invoice.getIssuer(), false, false);
					invoiceLine.setTransmissionStatus(InvoiceLineTransmissionStatus._IN_PROGRESS_);

					if (invoiceLine.getFuelEvent().getIsResale() && invoiceLine.getFuelEvent().getPayableCost() != null) {
						this.startExternalInterface(invoiceLine, ExtInterfaceNames.EXPORT_INVOICES_TO_NAV.getValue(), invoice.getReceiver(), true,
								false);
					}
				}
			} else if (invoice.getInvoiceType().equals(InvoiceTypeAcc._CRN_)) {
				for (OutgoingInvoiceLine invoiceLine : invoiceLines) {
					this.startExternalInterface(invoiceLine, ExtInterfaceNames.EXPORT_CREDIT_MEMO_TO_NAV.getValue(), invoice.getIssuer(), false,
							true);
					invoiceLine.setTransmissionStatus(InvoiceLineTransmissionStatus._IN_PROGRESS_);

					if (invoiceLine.getFuelEvent().getIsResale() && invoiceLine.getFuelEvent().getPayableCost() != null) {
						this.startExternalInterface(invoiceLine, ExtInterfaceNames.EXPORT_CREDIT_MEMO_TO_NAV.getValue(), invoice.getReceiver(), true,
								true);
					}
				}

			}
			invoice.setTransmissionStatus(InvoiceTransmissionStatus._IN_PROGRESS_);
		} catch (BusinessException e) {
			exported = false;
			if (e.getErrorCode().equals(BusinessErrorCode.INTERFACE_DISABLED)) {
				LOGGER.debug(e.getMessage());
			} else {
				throw e;
			}
		}
		this.onUpdate(invoice);

		return exported;
	}

	/**
	 * Starts the external interface
	 *
	 * @param list
	 * @param interfaceName
	 * @param hasPayableCost
	 * @param isCreditMemo
	 * @throws BusinessException
	 */
	private void startExternalInterface(OutgoingInvoiceLine line, String interfaceName, Customer subsidiary, boolean hasPayableCost,
			boolean isCreditMemo) throws BusinessException {
		WSInvoiceToDTOTransformer transformer = (WSInvoiceToDTOTransformer) this.getApplicationContext().getBean("invoiceTransformer");
		ExternalInterface extInterface = this.externalInterfaceService.findByName(interfaceName);
		ExtInterfaceParameters params = new ExtInterfaceParameters(extInterface.getParams());
		params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS, "");
		params.put(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS, "FALSE");
		if (hasPayableCost) {
			params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_PAYABLE, "");
			params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_OBJECT_TYPE, InvoiceLine.class.getSimpleName());
		} else {
			params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_OBJECT_TYPE, OutgoingInvoiceLine.class.getSimpleName());
		}
		if (isCreditMemo) {
			params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_CREDIT_MEMO, "");
		}

		PumaRestClient<OutgoingInvoiceLine> client = new PumaRestClient<>(this.taskExecutor, extInterface, params, Arrays.asList(line),
				this.historyFacade, transformer, this.messageFacade);

		client.setSourceCompany(subsidiary.getCode());

		client.start(1);
	}

	public synchronized boolean checkLines(OutgoingInvoiceLine outgoingInvoiceLine) throws BusinessException {
		OutgoingInvoice outgoingInvoice = outgoingInvoiceLine.getOutgoingInvoice();
		int failedLines = this.invoiceLineService.getFailedInvoiceLines(outgoingInvoice).size();

		if ((failedLines + 1) == outgoingInvoice.getInvoiceLines().size()) {
			outgoingInvoice.setTransmissionStatus(InvoiceTransmissionStatus._FAILED_);
			IOutgoingInvoiceService outgoingInvoiceService = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
			outgoingInvoiceService.update(outgoingInvoice);
			return false;
		}
		return true;
	}

	@Transactional
	@History(type = OutgoingInvoice.class, value = "Exported")
	@Override
	public void markAsExported(OutgoingInvoice outgoingInvoice) throws BusinessException {
		outgoingInvoice.setExportStatus(InvoiceExportStatus._EXPORTED_);
		outgoingInvoice.setExportDate(Calendar.getInstance().getTime());
	}

	@Transactional
	@History(type = OutgoingInvoice.class, value = "Uploaded on FTP")
	@Override
	public void markAsUploaded(OutgoingInvoice outgoingInvoice) throws BusinessException {
		outgoingInvoice.setExportStatus(InvoiceExportStatus._UPLOADED_);
		outgoingInvoice.setExportDate(Calendar.getInstance().getTime());
	}

	@Override
	@Transactional
	@History(type = OutgoingInvoice.class)
	public void submitForApproval(OutgoingInvoice e, BidApprovalStatus status, @Action String action, @Reason String reason)
			throws BusinessException {
		this.checkInvoiceDateValidity(e);
		e.setApprovalStatus(status);
		this.update(e);
	}

	@Override
	@Transactional
	public Attachment createAttachment(OutgoingInvoice invoice, String name, AttachmentType type, String targetAlias, String notes, String extension,
			Boolean active) throws BusinessException {
		Attachment doc = new Attachment();
		doc.setTargetAlias(targetAlias);
		doc.setTargetRefid(invoice.getId().toString());
		doc.setFileName(name);
		doc.setName(name);
		doc.setType(type);
		doc.setActive(true);
		doc.setNotes("Sale invoice report document generated by the system");
		doc.setContentType(extension);
		this.attachmentService.insert(doc);
		return doc;
	}

	@Override
	public List<OutgoingInvoice> exportToMail(List<String> issuers, List<String> receivers, List<Locations> locations, Date offsetDate, String resend)
			throws BusinessException {
		StringBuilder hql = new StringBuilder();
		hql.append("select e from ").append(OutgoingInvoice.class.getSimpleName());
		hql.append(" e where e.clientId = :clientId and e.subsidiaryId in :subsidiaryIds and e.status in :statuses ");

		if (!issuers.isEmpty()) {
			hql.append("and e.issuer in :issuers ");
		}

		if (!receivers.isEmpty()) {
			hql.append("and e.receiver in :receivers ");
		}

		if (!locations.isEmpty()) {
			hql.append("and e.deliveryLoc in :locations ");
		}

		if (offsetDate != null) {
			hql.append("and e.invoiceDate >= :offsetDate ");
		}

		if (resend.equals(YesNo._False_.getName())) {
			hql.append("and e.invoiceSent = :send ");
		}

		TypedQuery<OutgoingInvoice> query = this.getEntityManager().createQuery(hql.toString(), OutgoingInvoice.class);
		query.setParameter("clientId", Session.user.get().getClientId());
		query.setParameter("subsidiaryIds", Session.user.get().getProfile().getOrganizationIds());
		query.setParameter("statuses",
				LambdaUtil.filterEnum(BillStatus.class, c -> c.equals(BillStatus._AWAITING_PAYMENT_) || c.equals(BillStatus._PAID_)));

		if (!issuers.isEmpty()) {
			query.setParameter("issuers", issuers);
		}

		if (!receivers.isEmpty()) {
			query.setParameter("receivers", receivers);
		}

		if (!locations.isEmpty()) {
			query.setParameter("locations", locations);
		}

		if (offsetDate != null) {
			query.setParameter("offsetDate", offsetDate);
		}

		if (resend.equals(YesNo._False_.getName())) {
			query.setParameter("send", false);
		}

		return query.getResultList();
	}

	@Override
	public boolean hasAllLinesCredited(OutgoingInvoice outgoingInvoice) throws BusinessException {
		boolean hasAllLinesCredited = true;
		for (OutgoingInvoiceLine line : outgoingInvoice.getInvoiceLines()) {
			if (!line.getIsCredited()) {
				hasAllLinesCredited = false;
				break;
			}
		}
		return hasAllLinesCredited;
	}
}
