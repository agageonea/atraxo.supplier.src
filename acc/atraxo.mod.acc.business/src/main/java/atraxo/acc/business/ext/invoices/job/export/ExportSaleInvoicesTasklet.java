package atraxo.acc.business.ext.invoices.job.export;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.acc.business.api.ext.invoices.IIataInvoiceService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.invoices.service.OutgoingInvoiceToXMLTransformer;
import atraxo.acc.domain.impl.acc_type.InvoiceExportStatus;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.business.ext.utils.DateUtils;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.business.service.storage.StorageService;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceTransmission;

/**
 * @author abolindu
 */
public class ExportSaleInvoicesTasklet implements Tasklet {

	private static final Logger LOG = LoggerFactory.getLogger(ExportSaleInvoicesTasklet.class);
	private static final String CUSTOMERS = "CUSTOMER(S)";
	private static final String INVOICE_ISSUER_ID = "INVOICEISSUERID";
	private static final String INVOICE_RECEIVER_ID = "INVOICERECEIVERID";
	private static final String RE_EXPORT = "RE-EXPORT";
	private static final String OFFSET = "OFFSET";
	private static final String YES = "Yes";

	protected static final String FAILED = "failed";
	protected static final String SUCCESS = "success";

	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;
	@Autowired
	private IIataInvoiceService iataInvoiceService;
	@Autowired
	private OutgoingInvoiceToXMLTransformer transformer;
	@Autowired
	private StorageService storageService;

	private enum InvoiceCustomerId {
		ACCOUNT_CODE("Account code"), ACCOUNT_NUMBER("Account Number"), IATA_CODE("IATA Code"), ICAO_CODE("ICAO Code");

		private String value;

		private InvoiceCustomerId(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}
	}

	@Override
	@Transactional
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		ExecutionContext context = chunkContext.getStepContext().getStepExecution().getExecutionContext();

		String customers = (String) chunkContext.getStepContext().getJobParameters().get(ExportSaleInvoicesTasklet.CUSTOMERS);
		String invoiceIssuer = (String) chunkContext.getStepContext().getJobParameters().get(ExportSaleInvoicesTasklet.INVOICE_ISSUER_ID);
		String invoiceReceiver = (String) chunkContext.getStepContext().getJobParameters().get(ExportSaleInvoicesTasklet.INVOICE_RECEIVER_ID);
		String reexport = (String) chunkContext.getStepContext().getJobParameters().get(ExportSaleInvoicesTasklet.RE_EXPORT);
		String offset = (String) chunkContext.getStepContext().getJobParameters().get(ExportSaleInvoicesTasklet.OFFSET);

		List<String> outgoingInvoiceSuccessList = new ArrayList<>();
		List<String> outgoingInvoiceFailedList = new ArrayList<>();

		List<OutgoingInvoice> outgoingInvoiceList = this.getOutgoingInvoices(customers, this.getDate(new Integer(offset)),
				reexport.equalsIgnoreCase(ExportSaleInvoicesTasklet.YES) ? Boolean.TRUE : Boolean.FALSE);

		if (!outgoingInvoiceList.isEmpty()) {
			for (OutgoingInvoice outgoingInvoice : outgoingInvoiceList) {
				try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
					String issuerId = this.getCustomerId(outgoingInvoice.getIssuer(), invoiceIssuer);
					String receiverId = this.getCustomerId(outgoingInvoice.getReceiver(), invoiceReceiver);
					String fileName = this.generateFileName(outgoingInvoice, issuerId, receiverId);
					InvoiceTransmission transmission = this.transformer.generateInvoiceTransmission(outgoingInvoice, issuerId, receiverId);
					this.iataInvoiceService.generateIataXml(transmission, out);
					this.storageService.uploadFile(out.toByteArray(), outgoingInvoice.getRefid(), fileName, OutgoingInvoice.class.getSimpleName());
					this.outgoingInvoiceService.markAsExported(outgoingInvoice);
					outgoingInvoiceSuccessList.add(fileName);
				} catch (Exception e) {
					if (LOG.isDebugEnabled()) {
						LOG.debug(e.getMessage(), e);
					}
					outgoingInvoice.setExportStatus(InvoiceExportStatus._EXPORT_FAILED_);
					outgoingInvoiceFailedList.add(new StringBuilder(outgoingInvoice.getInvoiceNo()).append(" - ").append(e.getMessage()).toString());
				} finally {
					context.put(ExportSaleInvoicesTasklet.SUCCESS, outgoingInvoiceSuccessList);
					context.put(ExportSaleInvoicesTasklet.FAILED, outgoingInvoiceFailedList);
				}
			}
			this.outgoingInvoiceService.update(outgoingInvoiceList);
		}
		return null;
	}

	/**
	 * @param customerList
	 * @param invoiceDate
	 * @param reexport
	 * @return
	 */
	private List<OutgoingInvoice> getOutgoingInvoices(String customerList, Date invoiceDate, boolean reexport) {
		Map<String, Object> params = new HashMap<>();
		if (!reexport) {
			params.put("exportStatus", InvoiceExportStatus._NEW_);
		}
		if (!customerList.isEmpty()) {
			params.put("receiver", Arrays.asList(customerList.split(",")));
		}
		List<BillStatus> statusList = new ArrayList<>();
		statusList.add(BillStatus._AWAITING_PAYMENT_);
		statusList.add(BillStatus._PAID_);
		params.put("status", statusList);

		List<OutgoingInvoice> outgoingInvoiceList = this.outgoingInvoiceService.findEntitiesByAttributes(params);

		if (invoiceDate != null) {
			Iterator<OutgoingInvoice> iterator = outgoingInvoiceList.iterator();
			while (iterator.hasNext()) {
				OutgoingInvoice outgoingInvoice = iterator.next();
				if (outgoingInvoice.getInvoiceDate().before(invoiceDate)) {
					iterator.remove();
				}
			}
		}

		return outgoingInvoiceList;
	}

	/**
	 * @param customer
	 * @param paramValue
	 * @return
	 */
	private String getCustomerId(Customer customer, String paramValue) {
		String customerId = null;
		if (paramValue.equalsIgnoreCase(InvoiceCustomerId.ACCOUNT_CODE.getValue())) {
			customerId = customer.getCode();
		} else if (paramValue.equalsIgnoreCase(InvoiceCustomerId.ACCOUNT_NUMBER.getValue())) {
			customerId = customer.getAccountNumber();
		} else if (paramValue.equalsIgnoreCase(InvoiceCustomerId.IATA_CODE.getValue())) {
			customerId = customer.getIataCode();
		} else if (paramValue.equalsIgnoreCase(InvoiceCustomerId.ICAO_CODE.getValue())) {
			customerId = customer.getIcaoCode();
		}
		return customerId;
	}

	/**
	 * @param offset
	 * @return
	 */
	private Date getDate(Integer offset) {
		if (offset != -1) {
			Calendar calendar = Calendar.getInstance();
			return DateUtils.calculateDateWithOffset(calendar.getTime(), offset > 0 ? -offset : offset, Calendar.DAY_OF_YEAR);
		}
		return null;
	}

	/**
	 * @param invoice
	 * @param invoiceIssuer
	 * @param invoiceReceiver
	 * @return
	 */
	private String generateFileName(OutgoingInvoice invoice, String invoiceIssuer, String invoiceReceiver) {
		char separator = '_';
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String date = formatter.format(new Date());
		StringBuilder fileName = new StringBuilder(invoiceIssuer).append(separator).append(invoiceReceiver).append(separator)
				.append(invoice.getInvoiceNo()).append(separator).append(date).append(".xml");
		return fileName.toString();
	}

}
