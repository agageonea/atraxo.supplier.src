/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.invoices;

import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link OutgoingInvoice} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class OutgoingInvoice_Service
		extends
			AbstractEntityService<OutgoingInvoice> {

	/**
	 * Public constructor for OutgoingInvoice_Service
	 */
	public OutgoingInvoice_Service() {
		super();
	}

	/**
	 * Public constructor for OutgoingInvoice_Service
	 * 
	 * @param em
	 */
	public OutgoingInvoice_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<OutgoingInvoice> getEntityClass() {
		return OutgoingInvoice.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoice
	 */
	public OutgoingInvoice findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(OutgoingInvoice.NQ_FIND_BY_BUSINESS,
							OutgoingInvoice.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"OutgoingInvoice", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"OutgoingInvoice", "id"), nure);
		}
	}

	/**
	 * Find by reference: receiver
	 *
	 * @param receiver
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByReceiver(Customer receiver) {
		return this.findByReceiverId(receiver.getId());
	}
	/**
	 * Find by ID of reference: receiver.id
	 *
	 * @param receiverId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByReceiverId(Integer receiverId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoice e where e.clientId = :clientId and e.receiver.id = :receiverId",
						OutgoingInvoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("receiverId", receiverId).getResultList();
	}
	/**
	 * Find by reference: deliveryLoc
	 *
	 * @param deliveryLoc
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByDeliveryLoc(Locations deliveryLoc) {
		return this.findByDeliveryLocId(deliveryLoc.getId());
	}
	/**
	 * Find by ID of reference: deliveryLoc.id
	 *
	 * @param deliveryLocId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByDeliveryLocId(Integer deliveryLocId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoice e where e.clientId = :clientId and e.deliveryLoc.id = :deliveryLocId",
						OutgoingInvoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("deliveryLocId", deliveryLocId).getResultList();
	}
	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByCountry(Country country) {
		return this.findByCountryId(country.getId());
	}
	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByCountryId(Integer countryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoice e where e.clientId = :clientId and e.country.id = :countryId",
						OutgoingInvoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("countryId", countryId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoice e where e.clientId = :clientId and e.unit.id = :unitId",
						OutgoingInvoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoice e where e.clientId = :clientId and e.currency.id = :currencyId",
						OutgoingInvoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: issuer
	 *
	 * @param issuer
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByIssuer(Customer issuer) {
		return this.findByIssuerId(issuer.getId());
	}
	/**
	 * Find by ID of reference: issuer.id
	 *
	 * @param issuerId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByIssuerId(Integer issuerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoice e where e.clientId = :clientId and e.issuer.id = :issuerId",
						OutgoingInvoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("issuerId", issuerId).getResultList();
	}
	/**
	 * Find by reference: invoiceReference
	 *
	 * @param invoiceReference
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByInvoiceReference(
			OutgoingInvoice invoiceReference) {
		return this.findByInvoiceReferenceId(invoiceReference.getId());
	}
	/**
	 * Find by ID of reference: invoiceReference.id
	 *
	 * @param invoiceReferenceId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByInvoiceReferenceId(
			Integer invoiceReferenceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from OutgoingInvoice e where e.clientId = :clientId and e.invoiceReference.id = :invoiceReferenceId",
						OutgoingInvoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("invoiceReferenceId", invoiceReferenceId)
				.getResultList();
	}
	/**
	 * Find by reference: invoiceLines
	 *
	 * @param invoiceLines
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByInvoiceLines(
			OutgoingInvoiceLine invoiceLines) {
		return this.findByInvoiceLinesId(invoiceLines.getId());
	}
	/**
	 * Find by ID of reference: invoiceLines.id
	 *
	 * @param invoiceLinesId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByInvoiceLinesId(Integer invoiceLinesId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from OutgoingInvoice e, IN (e.invoiceLines) c where e.clientId = :clientId and c.id = :invoiceLinesId",
						OutgoingInvoice.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("invoiceLinesId", invoiceLinesId).getResultList();
	}
}
