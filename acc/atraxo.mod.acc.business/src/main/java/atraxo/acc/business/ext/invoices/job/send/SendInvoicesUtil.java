package atraxo.acc.business.ext.invoices.job.send;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.business.ext.utils.DateUtils;
import atraxo.cmm.domain.impl.cmm_type.ExportTypeBid;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.customer.CustomerNotification;

class SendInvoicesUtil {

	private void SendInvoicesUtil() {

	}

	protected static final Map<Customer, List<OutgoingInvoice>> groupInvoicesByReceiver(List<OutgoingInvoice> outgoingInvoiceList) {
		Map<Customer, List<OutgoingInvoice>> receiverInvoices = new HashMap<>();
		for (OutgoingInvoice invoice : outgoingInvoiceList) {
			if (receiverInvoices.containsKey(invoice.getReceiver())) {
				receiverInvoices.get(invoice.getReceiver()).add(invoice);
			} else {
				List<OutgoingInvoice> invoices = new ArrayList<>();
				invoices.add(invoice);
				receiverInvoices.put(invoice.getReceiver(), invoices);
			}
		}
		return receiverInvoices;
	}

	/**
	 * @param fileName
	 * @return
	 */
	protected static final String getInvoiceFormat(String fileName, String invoiceFormatParam) {
		String extension = ExportTypeBid._PDF_.getName();
		int delimiter = fileName.lastIndexOf('.');
		if ("Original".equals(invoiceFormatParam) && delimiter > 0) {
			extension = fileName.substring(delimiter + 1);
		}

		return extension;
	}

	/**
	 * @param outgoingInvoiceList
	 * @return
	 */
	protected static final Map<Customer, List<OutgoingInvoice>> groupInvoicesByIssuer(List<OutgoingInvoice> outgoingInvoiceList) {
		Map<Customer, List<OutgoingInvoice>> issuerInvoices = new HashMap<>();
		for (OutgoingInvoice invoice : outgoingInvoiceList) {
			if (issuerInvoices.get(invoice.getIssuer()) != null) {
				issuerInvoices.get(invoice.getIssuer()).add(invoice);
			} else {
				List<OutgoingInvoice> invoices = new ArrayList<>();
				invoices.add(invoice);

				issuerInvoices.put(invoice.getIssuer(), invoices);
			}
		}
		return issuerInvoices;
	}

	/**
	 * @param offset
	 * @return
	 */
	protected static final Date getDate(Integer offset) {
		if (offset != -1) {
			Calendar calendar = Calendar.getInstance();
			Date offsetDate = DateUtils.calculateDateWithOffset(calendar.getTime(), offset > 0 ? -offset : offset, Calendar.DAY_OF_YEAR);
			return DateUtils.removeTime(offsetDate);
		}
		return null;
	}

	protected static final CustomerNotification getCustomerNotification(List<CustomerNotification> notifications, OutgoingInvoice invoice) {
		// Search the notifications area
		CustomerNotification cn = null;
		for (CustomerNotification n : notifications) {
			if (n.getAssignedArea().getLocations().contains(invoice.getDeliveryLoc())
					&& (cn == null || n.getAssignedArea().getLocations().size() < cn.getAssignedArea().getLocations().size())) {
				cn = n;
			}
		}
		return cn;
	}

}
