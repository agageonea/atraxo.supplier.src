package atraxo.acc.business.ext.exceptions;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.j4e.api.exceptions.IErrorCode;
import seava.j4e.api.session.Session;

public enum AccErrorCode implements IErrorCode {

	VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO(6000, "Valid from can not be greater than valid to!", "Error"),
	INVOICE_LINE_AMOUNT_BALANCE_OUT_OF_TOLERANCE(6001, "The amount exceeds the defined tolerance CHECK_INVOICE_AMOUNT_AUTO_BALANCE", "Error"),
	INVOICE_LINE_QUANTITY_BALANCE_OUT_OF_TOLERANCE(6002, "The quantity exceeds the defined tolerance CHECK_INVOICE_QUANTITY_AUTO_BALANCE", "Error"),
	INVOICE_LINE_BILL_AMOUNT_OUT_OF_TOLERANCE(6003, "The amount exceeds the defined tolerance CHECK_BILL_AMOUT", "Error"),
	INVOICE_LINE_BILL_VAT_OUT_OF_TOLERANCE(6004, "The VAT amount exceeds the defined tolerance CHECK_BILL_VAT", "Error"),
	INVALID_INDEX(6005, "Start index must be lower than end index.", "Error"),
	INVALID_START_INDEX(6006, "Start index cannot be lower than the next invoice number.", "Error"),

	DUPLICATE_ENTITY(6010, "Duplicate entity %s based on %s fields combination.", "Error"),

	INVOICE_PRICE_DIFFERENT(6011, "Invoice price is different from contract price", "Error"),
	INVOICE_PRICE_VAT_DIFFERENT(6012, "Invoice price VAT is different from VAT calculated based on the contract.", "Error"),
	DUPLICATE_INV_LINE_EXISTS(6013, "the following fueling event(s) was previously invoiced:", "Error"),
	DUPLICATE_INV_LINE(6014, "Duplicate invoice line.", "Error"),
	INVOICE_FINISH_INPUT(6015, "Finish input error.", "InvChkResult"),
	INVOICE_BALANCED_VALUES_DIFFERENCE(6016, "Quantity and amount balance are different than 0.", "Error"),
	INVOICE_LINE_DELETE_STATUS_NOT_DRAFT(6017, "Paid/Awaiting payment invoices cannot be deleted!", "Error"),
	INV_LINE_DEL_DELIVERY_NOTE_USED(
			6018,
			"Can not delete invoice line because the generated delivery note is used or the fuel event is invoiced.",
			"Error"),
	CAN_NOT_DELETE_DELIVERY_NOTE(
			6019,
			"Can not delete the delivery note because the fuel event's invoice/bill status is not Not Invoiced/Not billed.",
			"Error"),

	INVALID_CONTRACT(6020, "Delivery period does not fit in contract period.<br/> Please recheck the contract.", "Error"),
	INVOICE_NO_INVOICE_LINE(6021, "Invoice does not contain any invoice lines", "Error"),
	INVOICE_DRAFT_PAID(6022, "Draft/Paid invoice - %s - cannot be reseted!", "Error"),
	INVOICE_FINISH_INPUT_DRAFT(6023, "The invoices status must be draft and the amount and quantity balance must be 0!", "Error"),
	INVOICE_STATUS_CHECK_PASSED(6024, "The invoice - %s - status must be check passed!", "Error"),
	INVOICE_STATUS_AWAITING_APPROVAL(6025, "The invoice - %s - status must be awaiting approval!", "Error"),
	INVOICE_STATUS_AWAITING_FOR_PAYMENT(6026, "The invoice - %s - status must be awaiting payment!", "Error"),
	FUEL_EVENT_RELEASE_STATUS_NOT_ON_HOLD(6027, "Can not delete fuel evens with ticket number %s, the receive status must be ON HOLD!", "Error"),
	INVOICE_DELETE_RPC_ERROR(6028, "Some of the selected invoices have been deleted by somebody else.", "Error"),
	INVOICE_CHECK_RESULT_TITLE(6030, "Invoice check result", ""),
	INVOICE_CHECK_FAILED_SUBTITLE(6031, "The invoice check failed due to:", "Error"),
	INVOICE_CHECK_FOOTER(6032, "Please review the invoice captured data and if necessary the correctness of the contract prices.", "Error"),
	INVOICE_NO_CONTRACT(6033, "No contract, please assign a contract.", "Error"),
	INVALID_FUEL_EVENT(6034, "Can not determine the contract associated to the fuel event. Please recheck the Fuel event.", "Error"),
	FUEL_EVENT_RPC_ACTION_ERROR(
			6035,
			"Action cannot be executed on the following Fuel Events (identified by ticket number) because they are not synchronized with database: %s The records have been reloaded. <br>Please try again.",
			"Validation"),
	FUEL_EVENT_CAN_NOT_DELETED_IS_INVOICED(6036, "The fuel event can not delete because is invoiced or billed!", "Error"),
	FUEL_EVENT_YOU_HAVE_NO_RIGHTS(6037, "You don't have access to another part of resale.", "Error"),
	INVALID_SERIES(6042, "Can not determine the invoice number series. Please verify Invoice Number series.", "Error"),
	INVALID_INV_GEN_PARAM(6043, "Please complete the mandatory fields.", "Error"),
	INVOICE_STATUS_PAY(6044, "The invoice status is awaiting for payment or paid, you can not modify the contract!", "Error"),
	INVOICE_NO_SERIES_OVERDUE(6045, "Invoice Number series overdue. Please verify.", "Error"),

	INVALID_RESELLER(6050, "Rejected due to missing direct indirect commercial transaction.", "Error"),

	INVOICE_STATUS_GENERATE_CREDIT_NOTE(6060, "The invoice status must be awaiting for payment or paid.", "Error"),
	CREDIT_NOTE_STATUS_DELETE(6061, "The credit note status is awaiting for payment or paid, you can not modify or delete!", "Error"),
	CREDIT_NOTE_DATE_BEFORE_INVOICE(6062, "The date of the credit note can not be before the credited invoice date.", "Error"),
	INVOICE_DATE_BEFORE_FUEL_EVENT_FUELING_DATE(
			6063,
			"The fueling date of one or more fuel events of the invoice is after the invoicing date.",
			"Error"),
	INVOICE_DATE_AFTER_TODAY(6064, "The invoicing date cannot be greater than the present date.", "Error"),
	THE_FUEL_EVENT_IS_ALREADY_ADDED_TO_THE_INVOICE(6065, "The fuel event is already added to the invoice.", "Error"),
	NO_SALE_DOCUMENT_ASSIGNED(6070, "The fuel event have not sale document assigned!", "Error"),
	NO_OUTGOING_INVOICE_LINE_FOUND(6080, "Can not find the outgoing invoice line!", "Error"),
	NO_FUEL_EVENT_FOUND(6081, "Can not find Fuel event!", "Error"),
	INVALID_CORRELATION_ID(6090, "Invalid correlation id!", "Error"),
	INVALID_TRANSACTION_OR_CORRELATION_ID(6091, "Invalid transaction or correlation id!", "Error"),
	IS_NOT_THE_LAST_EXPORT(6092, "This response is not for the last sent message!", "Error"),

	FILE_NOT_FOUND(6100, "The file couldn't be found on the webdav server!", "Error"),
	FTP_UPLOAD_ERROR(6101, "Cannot get files from webdav or upload them the ftp server. Please contact system administrator.\n", "Error"),
	INVOICE_DATE_GRATER_THAN_DATE_TO(6110, "The invoice date must be equal or bigger than the delivery up to date!", "Error"),
	IATA_INVOICE_TOO_MANY_TAX_LEVELS(6120, "Invoice contains too many tax levels in respect to IATA standard", "Error"),
	IATA_INVOICE_WRONG_PRODUCT_OR_FEE_CODE(6121, "Iata standard does not support products or fees with code: %s", "Error"),
	LOCATION_DOES_NOT_HAVE_IATA_CODE(
			6122,
			"The location does not have an IATA code specified. Please define it and try exporting the invoice again.",
			"ERROR"),

	ERROR_CREATING_ZIP(6130, "Error creating package with exported invoices. Please contact application administrator.", "Error"),

	FUEL_COST_SUCCESFULL_UPDATED(6300, "Successfully updated fuel event costs: ", "Message"),
	FUEL_COST_FAILED_UPDATED(6301, "Failed updated fuel events costs: ", "Message"),
	FUEL_COST_PROCESSED_CONTRACTS(6302, "Number of processed contracts by cost updater: %s.", "Message"),
	FUEL_EVENT_DETAILS_UPDATED(6303, "Successful updated fuel event details for %s fuel events.", "Message"),

	EXPORT_INVOICE_LIST_EMPTY(6310, "List with selected invoices for bulk export is empty.", "Error"),
	EXPORT_INVOICE_TO_RICOH_FAILED(
			6320,
			"Some invoices could not be exported because they still have invoice lines that had not been sent or confirmed by NAV. %s",
			"Error"),
	EXPORT_INVOICE_TO_RICOH_WRONG_INVOICE_TYPE(
			6321,
			"Some invoices were not exported because the contract specifies a different invoice type. %s",
			"Error"),

	INVOICE_RPC_ACTION_ERROR(
			6400,
			"Action cannot be executed on the following Invoices because they are not synchronized with database: %s The records have been reloaded. <br>Please try again.",
			"Validation"),

	CREDIT_MEMO_APPROVAL_WORKFLOW_RESULT_SUBMIT(6420, "Successfully submitted %s credit memos out of existing %s .", "Error"),
	CREDIT_MEMO_APPROVAL_WORKFLOW_RESULT_CHECK(6421, "Check credit memo history for specific error description.", "Error"),
	CREDIT_MEMO_APPROVAL_WORKFLOW_RESULT_NOT_FOUND(6422, "%s credit memo(s) has/have been changed in the meantime by a user or process.", "Error"),
	CREDIT_MEMO_APPROVAL_WORKFLOW_NO_SYS_PARAM(
			6423,
			"Can not submit for approval, please check the 'Use Credit Memo Approval Workflow' system parameter.",
			"Error"),
	CM_WKF_VALIDATION_ACTION_ERROR(
			6424,
			"Action cannot be executed on the following Credit Memo(s) because of the following reasons: <br/> %s <br/> Review and complete the worklow definition. <br>Please try again afterwards.",
			"Validation"),

	CREDIT_MEMO_WORKFLOW_APPROVED(
			6425,
			"The selected Credit Memo has been changed in the meantime and can no longer be approved/rejected. Please reload the data and try again.",
			"Error"),
	CREDIT_MEMO_WORKFLOW_IDENTIFICATION_ERROR(6426, "Workflow instance identification error. Check Credit Memo history for details.", "Error"),

	CREDIT_MEMO_APPROVAL_WORKFLOW_SUBJECT(6427, "Credit memo approval request - %s / %s", "Error"),
	CREDIT_MEMO_APPROVE_TOTAL_PROCESSING(6428, "%d credit memos approved out of a total of %d.<br/>", "Info"),
	CREDIT_MEMO_REJECT_TOTAL_PROCESSING(6429, "%d credit memos rejected out of a total of %d.<br/>", "Info"),

	CREDIT_MEMO_APPROVE_PARTIAL_PROCESSING(6430, "Reasons for failed approvals:<br/>", "Info"),

	CREDIT_MEMO_REJECT_PARTIAL_PROCESSING(6431, "%Reasons for failed rejections:<br/>", "Info"),
	CREDIT_MEMO_APPROVAL_WORKFLOW_NO_SYS_PARAM_APPROVE_REJECT(
			6432,
			"Can not complete the requested operation, please check the 'Use Credit Memo Approval Workflow' system parameter.",
			"Error"),
	UPLOAD_RICOH_INVOICE_WORKFLOW_NOT_START(6433, "Upload process could not be started for the following invoice. %s", "Error"),

	INVOICE_BULK_EXPORT_ERROR(6440, "Process could not be started. Reason : ", "Error"),
	INVOICE_BULK_EXPORT(6441, "Workflow has started, a notification will be sent when the process will be finished.", "Error"),
	INVOICE_JOB_MAIL_DELIVERY_ERROR(6442, "Could not generate invoices issued by %s for customer %s", "Error"),

	FUEL_EVENT_DUPLICATE_BUSINESS_KEY(
			6500,
			"More than one Fuel Event found with business key: fueling date %s, customer code %s, Departure code %s, aircraft code %s, flight number %s, ticket number %s and sufix %s.",
			"Error"),
	INCOMING_INVOICE_DUPLICATE_BUSINESS_KEY(
			6501,
			"More than one Incoming Invoice found with business key: number %s, issuer code %s and issue date %s.",
			"Error"),
	FUEL_TICKET_DUPLICATE_BUSINESS_KEY(6502, "More than one Fuel Ticket with same business key found.", "Error"),
	PAYABLE_ACCRUALS_DUPLICATE_BUSINESS_KEY(6503, "More than one Accrual with same business key.", "Error"),
	DELIVERY_NOTE_NOT_FOUND(6504, "No Delivery Note found for Purchase Invoice with code %s", "Error"),
	BLANK_ERROR_MESSAGE(6600, "%s", "Error"),
	TAG_NOT_DEFINED_CORRECTLY(7000, "Value of tag %s is not defined correctly!", "Error");

	private final Logger LOG = LoggerFactory.getLogger(AccErrorCode.class);

	private ResourceBundle boundle;

	private final int errNo;
	private final String errMsg;
	private final String errGroup;

	private AccErrorCode(int errNo, String errMsg, String errGroup) {
		this.errNo = errNo;
		this.errMsg = errMsg;
		this.errGroup = errGroup;
	}

	@Override
	public String getErrGroup() {
		return this.errGroup;
	}

	@Override
	public int getErrNo() {
		return this.errNo;
	}

	@Override
	public String getErrMsg() {
		try {
			String language = Session.user.get().getSettings().getLanguage();
			if (language.equals("sys")) {
				this.boundle = ResourceBundle.getBundle("locale.sone.acc.error.messages");
			} else {
				this.boundle = ResourceBundle.getBundle("locale.sone.acc.error.messages", new Locale(language));
			}
		} catch (NullPointerException e) {
			this.LOG.trace("No session while testing", e);
			this.boundle = ResourceBundle.getBundle("locale.sone.acc.error.messages");
		}
		return this.boundle.containsKey(Integer.toString(this.errNo)) ? this.boundle.getString(Integer.toString(this.errNo)) : this.errMsg;
	}
}
