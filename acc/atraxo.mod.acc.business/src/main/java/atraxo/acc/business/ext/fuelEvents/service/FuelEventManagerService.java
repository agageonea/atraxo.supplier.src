package atraxo.acc.business.ext.fuelEvents.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.deliveryNotes.service.DeliveryNoteBuilderService;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.business.api.suppliers.ISuppliersService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.business.ext.exceptions.RevokeFuelOrderException;
import atraxo.ops.business.ext.integration.FuelEventSource;
import atraxo.ops.domain.ext.DeliveryNoteMethod.DeliveryNoteMethod;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FlightEventStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author zspeter
 */
public class FuelEventManagerService extends AbstractBusinessBaseService {

	@Autowired
	private IDeliveryNoteService dnSrv;
	@Autowired
	private IFuelEventService feSrv;
	@Autowired
	private ISuppliersService supplierSrv;
	@Autowired
	private DeliveryNoteBuilderService dnBuilderSrv;
	@Autowired
	private FuelEventModifierService feModifierSrv;
	@Autowired
	private IFuelTicketService ticketService;
	@Autowired
	private IFlightEventService eventService;
	@Autowired
	private IOutgoingInvoiceService outgoingInvoiceService;
	@Autowired
	private IContractService contractService;

	/**
	 * Export fuel events generated form the list of fuel tickets.
	 *
	 * @param object
	 * @throws BusinessException
	 */
	public void export(Object object) throws BusinessException {
		if (object instanceof List) {
			@SuppressWarnings("unchecked")
			List<FuelTicket> list = (List<FuelTicket>) object;
			this.feSrv.export(list);
		}
	}

	/**
	 * Reorders the elements in the list of <code>FuelEvent</code> so that the resale fuel events(payable and receivable prices exist both)are in teh
	 * top of the list (at the beginning so they are processed first)
	 *
	 * @param fuelEventList
	 */
	public void reOrderResaleFuelEvents(List<FuelEvent> fuelEventList) {
		// make sure to put the resale fuel events(payable and receivable prices exist both) first
		List<FuelEvent> fuelEventListPayAndRec = new ArrayList<>();
		List<FuelEvent> fuelEventListRest = new ArrayList<>();
		for (FuelEvent e : fuelEventList) {
			if (e.getPayableCost() != null && e.getBillableCost() != null) {
				fuelEventListPayAndRec.add(e);
			} else {
				fuelEventListRest.add(e);
			}
		}
		fuelEventList.clear();
		fuelEventList.addAll(fuelEventListPayAndRec);
		fuelEventList.addAll(fuelEventListRest);
	}

	/**
	 * @param invoice
	 * @throws BusinessException
	 */
	public void manage(Invoice invoice) throws BusinessException {
		List<FuelEvent> list = new ArrayList<>();
		for (InvoiceLine il : invoice.getInvoiceLines()) {
			List<DeliveryNote> deliveryNotes = this.dnSrv.findByObjectIdAndType(il.getId(), InvoiceLine.class.getSimpleName());
			if (CollectionUtils.isEmpty(deliveryNotes)) {
				list.add(this.feModifierSrv.generate(il));
			}
		}
		this.setSubsidiaryOnDeliveryNotes(list);
		this.persist(list);
		this.setParentBillStatus(list);
	}

	/**
	 * @param object
	 * @throws BusinessException
	 */
	public void manage(Object object) throws BusinessException {
		if (object instanceof FuelEventSource) {
			FuelEventSource feSource = (FuelEventSource) object;
			List<FuelEvent> updList = new ArrayList<>();
			List<FuelEvent> delList = new ArrayList<>();
			if (feSource.getSource() instanceof FuelTicket) {
				this.manageFuelTicket(feSource, updList, delList);
			} else if (feSource.getSource() instanceof FuelOrder) {
				this.manageFuelOrder(feSource, updList, delList);
			} else if (feSource.getSource() instanceof FlightEvent) {
				this.manageFlightEvent(feSource, updList, delList);
			}
			this.feSrv.delete(delList);
			this.setSubsidiaryOnDeliveryNotes(updList);
			this.persist(updList);
			this.setParentBillStatus(updList);
		}
	}

	private void setSubsidiaryOnDeliveryNotes(List<FuelEvent> list) {
		for (FuelEvent fe : list) {
			for (DeliveryNote dn : fe.getDeliveryNotes()) {
				dn.setSubsidiaryId(fe.getSubsidiaryId());
			}
		}
	}

	private void setParentBillStatus(List<FuelEvent> list) throws BusinessException {
		for (FuelEvent fe : list) {
			for (DeliveryNote dn : fe.getDeliveryNotes()) {
				if (QuantitySource._INVOICE_.equals(dn.getSource())) {
					continue;
				}
				if (QuantitySource._FUEL_TICKET_.equals(dn.getSource())) {
					FuelTicket ft = this.ticketService.findById(dn.getObjectId());
					ft.setBillStatus(fe.getBillStatus());
					this.ticketService.updateWithoutBL(Arrays.asList(ft));
				}

				if (QuantitySource._FUEL_ORDER_.equals(dn.getSource())) {
					FlightEvent flightEvent = this.eventService.findById(dn.getObjectId());
					flightEvent.setBillStatus(fe.getBillStatus());
					this.eventService.updateWithoutBusiness(flightEvent);
				}
			}
		}
	}

	private void manageFlightEvent(FuelEventSource feSource, List<FuelEvent> updList, List<FuelEvent> delList) throws BusinessException {
		FlightEvent flightEvent = (FlightEvent) feSource.getSource();
		if (DeliveryNoteMethod.DELETE.equals(feSource.getMethod())) {
			this.feModifierSrv.remove(updList, delList, flightEvent);
		} else {
			updList.addAll(this.manageFlightEvent(flightEvent));
		}
	}

	private void manageFuelOrder(FuelEventSource feSource, List<FuelEvent> updList, List<FuelEvent> delList) throws BusinessException {
		if (DeliveryNoteMethod.DELETE.equals(feSource.getMethod())) {
			FuelOrder fuelOrder = (FuelOrder) feSource.getSource();
			this.checkForOutgoingInvoice(fuelOrder);
			Collection<FuelOrderLocation> fuelOrderLocations = fuelOrder.getFuelOrderLocations();
			for (FuelOrderLocation fol : fuelOrderLocations) {
				Collection<FlightEvent> flightEvents = fol.getFlightEvents();
				for (FlightEvent fe : flightEvents) {
					this.feModifierSrv.remove(updList, delList, fe);
				}
			}
		} else {
			updList.addAll(this.manageFuelOrder(feSource));
		}
	}

	private void checkForOutgoingInvoice(FuelOrder order) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("referenceDocType", InvoiceReferenceDocType._PURCHASE_ORDER_);
		params.put("referenceDocNo", order.getCode());
		List<OutgoingInvoice> list = this.outgoingInvoiceService.findEntitiesByAttributes(params);
		if (!list.isEmpty()) {
			throw new RevokeFuelOrderException(list.iterator().next().getInvoiceNo());
		}
	}

	private List<FuelEvent> manageFuelOrder(FuelEventSource feSource) throws BusinessException {
		List<FuelEvent> list = new ArrayList<>();
		FuelOrder fuelOrder = (FuelOrder) feSource.getSource();
		Collection<FuelOrderLocation> fuelOrderLocations = fuelOrder.getFuelOrderLocations();
		for (FuelOrderLocation fol : fuelOrderLocations) {
			Collection<FlightEvent> flightEvents = fol.getFlightEvents();
			for (FlightEvent fe : flightEvents) {
				list.addAll(this.manageFlightEvent(fe));
			}
		}
		return list;
	}

	private List<FuelEvent> manageFlightEvent(FlightEvent fe) throws BusinessException {
		if (fe.getStatus().equals(FlightEventStatus._SCHEDULED_)) {
			List<DeliveryNote> dnList = this.dnSrv.findByObjectIdAndType(fe.getId(), fe.getClass().getSimpleName());
			if (dnList.isEmpty()) {
				return Arrays.asList(this.feModifierSrv.generate(fe));
			}
		}
		return Collections.emptyList();
	}

	private void manageFuelTicket(FuelEventSource feSource, List<FuelEvent> updList, List<FuelEvent> delList) throws BusinessException {
		if (feSource.getMethod().equals(DeliveryNoteMethod.DELETE)) {
			this.feModifierSrv.remove(updList, delList, (FuelTicket) feSource.getSource());
		} else {
			FuelTicket ft = (FuelTicket) feSource.getSource();
			List<Contract> sellContracts = feSource.getSellContracts();
			FuelOrderLocation fol = feSource.getFol();
			Contract buyContract = feSource.getBuyContract();
			if (fol != null) {
				updList.addAll(this.manage(ft, fol));
			} else if (buyContract != null) {
				updList.addAll(this.manage(ft, sellContracts, buyContract));
			} else {
				Customer ftCustomer = ft.getCustomer();
				Suppliers ftSupplier = ft.getSupplier();
				for (Contract sellContract : sellContracts) {
					Contract tempContract = this.contractService.findById(sellContract.getId());
					ft.setCustomer(tempContract.getCustomer());
					ft.setSupplier(this.supplierSrv.findById(tempContract.getHolder().getId()));
					updList.addAll(this.manage(ft, tempContract, ftCustomer));
				}
				ft.setCustomer(ftCustomer);
				ft.setSupplier(ftSupplier);
			}
		}
	}

	private void persist(List<FuelEvent> list) throws BusinessException {
		for (FuelEvent fe : list) {
			if (fe.getId() == null) {
				this.feSrv.insert(fe);
			} else {
				this.feSrv.update(fe);
			}
		}
	}

	private List<FuelEvent> manage(FuelTicket ft, List<Contract> sellContracts, Contract buyContract) throws BusinessException {
		List<FuelEvent> list = new ArrayList<>();
		Customer ftCustomer = ft.getCustomer();
		Suppliers ftSupplier = ft.getSupplier();
		ft.setSupplier(this.supplierSrv.findById(buyContract.getHolder().getId()));
		Contract resaleContract = this.findResaleContract(sellContracts, ft.getCustomer().getId(), false);
		List<FuelEvent> fuelEvents1 = this.manage(ft, resaleContract, ftCustomer);
		ft.setCustomer(buyContract.getHolder());
		ft.setSupplier(buyContract.getSupplier());
		resaleContract = this.findResaleContract(sellContracts, ft.getCustomer().getId(), true);
		List<FuelEvent> fuelEvents2 = this.manage(ft, resaleContract, ftCustomer);
		ft.setCustomer(ftCustomer);
		ft.setSupplier(ftSupplier);

		list.addAll(fuelEvents2);
		list.addAll(fuelEvents1);

		this.markAsResale(list);
		return list;
	}

	private void markAsResale(List<FuelEvent> list) {
		for (FuelEvent fe : list) {
			fe.setIsResale(true);
		}
	}

	private Contract findResaleContract(List<Contract> list, int customerId, boolean internal) {
		Contract contract = null;
		for (Contract c : list) {
			if (c.getCustomer().getId().equals(customerId) || this.isInShipTo(customerId, c)) {
				if ((internal && c.getCustomer().getIsSubsidiary()) || (!internal && !c.getCustomer().getIsSubsidiary())) {
					contract = c;
					break;
				}
			}
		}
		return contract;
	}

	private boolean isInShipTo(int customerId, Contract saleContract) {
		if (saleContract.getShipTo() != null) {
			for (Iterator<ShipTo> iter = saleContract.getShipTo().iterator(); iter.hasNext();) {
				ShipTo shipTo = iter.next();
				if (shipTo.getCustomer().getId().equals(customerId)) {
					return true;
				}
			}
		}
		return false;
	}

	private List<FuelEvent> manage(FuelTicket ft, FuelOrderLocation fol) throws BusinessException {
		List<FuelEvent> feList = new ArrayList<>();
		List<DeliveryNote> list = this.dnSrv.findByObjectIdAndType(ft.getId(), ft.getClass().getSimpleName());
		if (list.isEmpty()) {
			feList.add(this.feModifierSrv.generate(ft, fol));
		}
		return feList;
	}

	private List<FuelEvent> manage(FuelTicket ft, Contract sellContract, Customer shipTo) throws BusinessException {
		List<FuelEvent> feList = new ArrayList<>();
		List<DeliveryNote> list = this.dnSrv.findByObjectIdAndType(ft.getId(), ft.getClass().getSimpleName());
		if (CollectionUtils.isEmpty(feList)) {
			feList.add(this.feModifierSrv.generate(ft, sellContract, shipTo));
		} else {
			for (DeliveryNote dn : list) {
				dn = this.dnBuilderSrv.build(ft, dn, sellContract.getCustomer(), shipTo);
				FuelEvent fe = dn.getFuelEvent();
				this.feModifierSrv.update(ft, fe, sellContract);
				feList.add(fe);
			}
		}
		return feList;
	}
}
