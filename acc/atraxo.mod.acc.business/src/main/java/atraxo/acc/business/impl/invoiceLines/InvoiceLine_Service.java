/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.impl.invoiceLines;

import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link InvoiceLine} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class InvoiceLine_Service extends AbstractEntityService<InvoiceLine> {

	/**
	 * Public constructor for InvoiceLine_Service
	 */
	public InvoiceLine_Service() {
		super();
	}

	/**
	 * Public constructor for InvoiceLine_Service
	 * 
	 * @param em
	 */
	public InvoiceLine_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<InvoiceLine> getEntityClass() {
		return InvoiceLine.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return InvoiceLine
	 */
	public InvoiceLine findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(InvoiceLine.NQ_FIND_BY_BUSINESS,
							InvoiceLine.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"InvoiceLine", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"InvoiceLine", "id"), nure);
		}
	}

	/**
	 * Find by reference: invoice
	 *
	 * @param invoice
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByInvoice(Invoice invoice) {
		return this.findByInvoiceId(invoice.getId());
	}
	/**
	 * Find by ID of reference: invoice.id
	 *
	 * @param invoiceId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByInvoiceId(Integer invoiceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InvoiceLine e where e.clientId = :clientId and e.invoice.id = :invoiceId",
						InvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("invoiceId", invoiceId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InvoiceLine e where e.clientId = :clientId and e.unit.id = :unitId",
						InvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: netQuantityUnit
	 *
	 * @param netQuantityUnit
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByNetQuantityUnit(Unit netQuantityUnit) {
		return this.findByNetQuantityUnitId(netQuantityUnit.getId());
	}
	/**
	 * Find by ID of reference: netQuantityUnit.id
	 *
	 * @param netQuantityUnitId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByNetQuantityUnitId(Integer netQuantityUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InvoiceLine e where e.clientId = :clientId and e.netQuantityUnit.id = :netQuantityUnitId",
						InvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("netQuantityUnitId", netQuantityUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: grossQuantityUnit
	 *
	 * @param grossQuantityUnit
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByGrossQuantityUnit(Unit grossQuantityUnit) {
		return this.findByGrossQuantityUnitId(grossQuantityUnit.getId());
	}
	/**
	 * Find by ID of reference: grossQuantityUnit.id
	 *
	 * @param grossQuantityUnitId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByGrossQuantityUnitId(
			Integer grossQuantityUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InvoiceLine e where e.clientId = :clientId and e.grossQuantityUnit.id = :grossQuantityUnitId",
						InvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("grossQuantityUnitId", grossQuantityUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByContract(Contract contract) {
		return this.findByContractId(contract.getId());
	}
	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByContractId(Integer contractId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InvoiceLine e where e.clientId = :clientId and e.contract.id = :contractId",
						InvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractId", contractId).getResultList();
	}
	/**
	 * Find by reference: destination
	 *
	 * @param destination
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByDestination(Locations destination) {
		return this.findByDestinationId(destination.getId());
	}
	/**
	 * Find by ID of reference: destination.id
	 *
	 * @param destinationId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByDestinationId(Integer destinationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InvoiceLine e where e.clientId = :clientId and e.destination.id = :destinationId",
						InvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("destinationId", destinationId).getResultList();
	}
	/**
	 * Find by reference: referenceInvoiceLine
	 *
	 * @param referenceInvoiceLine
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByReferenceInvoiceLine(
			InvoiceLine referenceInvoiceLine) {
		return this.findByReferenceInvoiceLineId(referenceInvoiceLine.getId());
	}
	/**
	 * Find by ID of reference: referenceInvoiceLine.id
	 *
	 * @param referenceInvoiceLineId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByReferenceInvoiceLineId(
			Integer referenceInvoiceLineId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InvoiceLine e where e.clientId = :clientId and e.referenceInvoiceLine.id = :referenceInvoiceLineId",
						InvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("referenceInvoiceLineId", referenceInvoiceLineId)
				.getResultList();
	}
	/**
	 * Find by reference: airlineDesignator
	 *
	 * @param airlineDesignator
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByAirlineDesignator(Customer airlineDesignator) {
		return this.findByAirlineDesignatorId(airlineDesignator.getId());
	}
	/**
	 * Find by ID of reference: airlineDesignator.id
	 *
	 * @param airlineDesignatorId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByAirlineDesignatorId(
			Integer airlineDesignatorId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from InvoiceLine e where e.clientId = :clientId and e.airlineDesignator.id = :airlineDesignatorId",
						InvoiceLine.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("airlineDesignatorId", airlineDesignatorId)
				.getResultList();
	}
}
