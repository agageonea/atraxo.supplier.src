package atraxo.acc.business.ext.invoices.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;

import atraxo.acc.business.api.ext.invoices.IAccountInfoAcknoledgmentService;
import atraxo.acc.business.api.ext.invoices.service.IRicohAccepter;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.externalInterfaces.processor.AbstractIncomingMessageProcessor;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;

/**
 * @author abolindu
 */
public class RicohAccepterImpl extends AbstractIncomingMessageProcessor implements IRicohAccepter {

	private static final Logger LOG = LoggerFactory.getLogger(RicohAccepterImpl.class);

	@Autowired
	@Qualifier("accountInfoAcknoledgmentService")
	private IAccountInfoAcknoledgmentService srv;
	@Autowired
	private IExternalInterfaceService externalInterfaceService;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageHistoryFacade;
	@Autowired
	private IUserSuppService userSrv;

	@Override
	public MSG acknowledgeRicohSentFiles(MSG request) throws BusinessException {
		String correlationID = "";
		boolean success = false;
		try {
			correlationID = request.getHeaderCommon().getCorrelationID();
			String clientIdentifier = GUIDMessageGenerator.extractClientIdentifier(correlationID);
			this.createSessionUser(clientIdentifier);
			success = this.srv.processRicohAcknoledgment(request);
		} catch (Exception e) {
			LOG.warn("Invoice acknowledgement processed failure!", e);
			request.getHeaderCommon().setErrorCode(e.getMessage());
			request.getHeaderCommon().setErrorDescription(e.getMessage());
		}
		this.updateMessageHistory(request, success);
		return request;
	}

	@Override
	protected ExternalInterface getExternalInterface() {
		return this.externalInterfaceService.findByName(ExtInterfaceNames.ACK_FROM_RICOH_PRINTER.getValue());
	}

	private void updateMessageHistory(MSG message, boolean success) throws BusinessException {
		ExternalInterfaceMessageHistoryStatus status = success ? ExternalInterfaceMessageHistoryStatus._ACKNOWLEDGED_
				: ExternalInterfaceMessageHistoryStatus._FAILED_NEGATIVE_ACKNOWLEDGEMENT_;
		this.messageHistoryFacade.updateMessageHistory(message, status);
	}

	private void createSessionUser(String clientIdentifier) throws BusinessException {
		try {
			this.userSrv.createSessionUser(clientIdentifier);
			Message<IUser> message = MessageBuilder.withPayload(Session.user.get()).build();
			this.getApplicationContext().getBean("updateJobSessionChannel", MessageChannel.class).send(message);
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOG.warn("Invalid client in correlation id!", ex);
				throw new BusinessException(AccErrorCode.INVALID_CORRELATION_ID, AccErrorCode.INVALID_CORRELATION_ID.getErrMsg());
			} else {
				throw ex;
			}
		}
	}
}
