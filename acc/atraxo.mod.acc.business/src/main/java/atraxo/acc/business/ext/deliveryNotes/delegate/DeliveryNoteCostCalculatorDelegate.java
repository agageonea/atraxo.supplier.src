package atraxo.acc.business.ext.deliveryNotes.delegate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.payableAccruals.IPayableAccrualService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.fuelEvents.delegate.FuelEvent_Bd;
import atraxo.acc.business.ext.fuelEvents.service.CostCalculatorService;
import atraxo.acc.domain.ext.deliveryNotes.AccrualDateParameter;
import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class DeliveryNoteCostCalculatorDelegate extends AbstractBusinessDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(DeliveryNoteCostCalculatorDelegate.class);

	/**
	 * @param dn
	 * @return true in case that payable cost have been changed, false otherwise
	 * @throws BusinessException
	 */
	public boolean calculatePayableCost(DeliveryNote dn) throws BusinessException {
		Collection<DeliveryNoteContract> dnContracts = dn.getDeliveryNoteContracts() != null ? dn.getDeliveryNoteContracts()
				: Collections.emptyList();
		if (dnContracts.isEmpty()) {
			return false;
		}
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		Currencies contractCurrency = this.getContractCurrency(dnContracts);
		String accrualDateParameter = paramSrv.getAccrualEvaluationDate();
		BigDecimal sum = BigDecimal.ZERO;
		Date calcDate = GregorianCalendar.getInstance().getTime();
		ExchangeRate_Bd exchBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		CostCalculatorService calculatorSrv = (CostCalculatorService) this.getApplicationContext().getBean("costCalculator");

		for (DeliveryNoteContract dnc : dnContracts) {
			if (accrualDateParameter.equalsIgnoreCase(AccrualDateParameter.EVENT_DATE.getValue())) {
				calcDate = dnc.getDeliveryNotes().getFuelingDate();
			}
			FuelEvent tmpFe = this.buildTmpFuelEvent(dn);
			BigDecimal contractCost = calculatorSrv.calculate(dnc.getContracts(), tmpFe).getCost();
			contractCost = exchBd.convert(dnc.getContracts().getSettlementCurr(), contractCurrency, calcDate, contractCost, false).getValue();
			sum = sum.add(contractCost);
		}
		if (dn.getPayableCost() == null || !sum.setScale(6, RoundingMode.HALF_UP).equals(dn.getPayableCost().setScale(6, RoundingMode.HALF_UP))) {
			dn.setPayableCost(sum);
			dn.setPayableCostCurrency(contractCurrency);
			return true;
		}
		return false;
	}

	private Currencies getContractCurrency(Collection<DeliveryNoteContract> dnContracts) throws BusinessException {
		if (CollectionUtils.isEmpty(dnContracts)) {
			ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
			String sysCurrencyCode = paramSrv.getSysCurrency();
			ICurrenciesService currSrv = (ICurrenciesService) this.findEntityService(Currencies.class);
			return currSrv.findByCode(sysCurrencyCode);
		}
		return dnContracts.iterator().next().getContracts().getSettlementCurr();
	}

	private FuelEvent buildTmpFuelEvent(DeliveryNote dn) throws BusinessException {
		FuelEvent fe = new FuelEvent();
		fe.setFuelingDate(dn.getFuelingDate());
		fe.setCustomer(dn.getCustomer());
		fe.setDeparture(dn.getDeparture());
		fe.setAircraft(dn.getAircraft());
		fe.setFlightNumber(dn.getFlightNumber());
		fe.setSuffix(dn.getSuffix());
		fe.setTicketNumber(dn.getTicketNumber() != null ? dn.getTicketNumber() : "");
		fe.setFuelSupplier(dn.getFuelSupplier());
		fe.setIplAgent(dn.getIplAgent());
		fe.setQuantity(dn.getQuantity() != null ? dn.getQuantity() : BigDecimal.ZERO);
		fe.setUnit(dn.getUnit());
		fe.setQuantitySource(dn.getSource());
		fe.setInvoiceStatus(OutgoingInvoiceStatus._NOT_INVOICED_);
		fe.setBillStatus(BillStatus._NOT_BILLED_);
		fe.setEventType(dn.getEventType());
		fe.setPayableCost(dn.getPayableCost());
		fe.setPayableCostCurrency(dn.getPayableCostCurrency());
		fe.setNetUpliftQuantity(dn.getNetUpliftQuantity());
		fe.setNetUpliftUnit(dn.getNetUpliftUnit());
		fe.setFuelingOperation(dn.getFuelingOperation());
		fe.setTransport(dn.getTransport());
		fe.setOperationType(dn.getOperationType());
		fe.setStatus(FuelEventStatus._ORIGINAL_);
		fe.setTransmissionStatusSale(FuelEventTransmissionStatus._NEW_);
		fe.setTransmissionStatusPurchase(FuelEventTransmissionStatus._NEW_);
		fe.setRevocationReason(RevocationReason._EMPTY_);
		this.getBusinessDelegate(FuelEvent_Bd.class).setSystemQuantity(fe, dn);
		return fe;
	}

	public void recalculatePayableCost(List<DeliveryNote> dns) throws BusinessException {
		for (DeliveryNote dn : dns) {
			this.recalculatePayableCost(dn);
		}
	}

	public void recalculatePayableCost(DeliveryNote dn) throws BusinessException {
		this.calculatePayableCost(dn);
		if (dn.getUsed()) {
			FuelEvent fe = dn.getFuelEvent();
			fe.setPayableCost(dn.getPayableCost());
			fe.setPayableCostCurrency(dn.getPayableCostCurrency());
			fe.setSysPayableCost(this.getBusinessDelegate(FuelEvent_Bd.class).calculateSysPayableCost(fe));
			IFuelEventService fuelEventService = (IFuelEventService) this.findEntityService(FuelEvent.class);
			fuelEventService.update(fe);
			this.maintainPayableAccrual(dn);
		}
	}

	public void maintainPayableAccrual(DeliveryNote dn) throws BusinessException {
		IPayableAccrualService payableAccrualService = (IPayableAccrualService) this.findEntityService(PayableAccrual.class);
		Collection<DeliveryNoteContract> dnContracts = dn.getDeliveryNoteContracts() == null ? new ArrayList<>() : dn.getDeliveryNoteContracts();
		for (DeliveryNoteContract dnc : dnContracts) {
			try {
				PayableAccrual pa = payableAccrualService.findByFuelEventContract(dnc.getContracts(), dn.getFuelEvent());
				payableAccrualService.updatePayableAccruals(pa);
			} catch (ApplicationException e) {
				if (J4eErrorCode.DB_NO_RESULT.equals(e.getErrorCode())) {
					LOG.warn("Warning:could not update payable accruals, will generate payable accruals !", e);
					payableAccrualService.generatePayableAccruals(dn, dnc.getContracts());
				} else if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
					throw new BusinessException(AccErrorCode.PAYABLE_ACCRUALS_DUPLICATE_BUSINESS_KEY,
							AccErrorCode.PAYABLE_ACCRUALS_DUPLICATE_BUSINESS_KEY.getErrMsg(), e);
				}
			}
		}
	}
}
