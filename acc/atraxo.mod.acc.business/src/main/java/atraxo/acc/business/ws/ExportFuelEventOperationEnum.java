package atraxo.acc.business.ws;

/**
 * @author vhojda
 */
public enum ExportFuelEventOperationEnum {

	NEW_FUEL_EVENT("FuelEvent"), CANCEL_FUEL_EVENT("Reversal"), COST_UPDATE_FUEL_EVENT("FuelEvent");

	private String fuelEventType;

	/**
	 * @param fuelEventType
	 */
	private ExportFuelEventOperationEnum(String fuelEventType) {
		this.fuelEventType = fuelEventType;
	}

	public String getFuelEventType() {
		return this.fuelEventType;
	}

}
