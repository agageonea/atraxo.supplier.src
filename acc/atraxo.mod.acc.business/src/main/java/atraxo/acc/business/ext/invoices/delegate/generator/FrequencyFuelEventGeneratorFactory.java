package atraxo.acc.business.ext.invoices.delegate.generator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;

/**
 * @author vhojda
 */
public class FrequencyFuelEventGeneratorFactory {

	private static final Logger LOG = LoggerFactory.getLogger(FrequencyFuelEventGeneratorFactory.class);

	private static FrequencyFuelEventGeneratorFactory factory;

	/**
	 * Default private constructor
	 */
	private FrequencyFuelEventGeneratorFactory() {

	}

	/**
	 * @return
	 */
	public static synchronized FrequencyFuelEventGeneratorFactory getFactory() {
		if (factory == null) {
			factory = new FrequencyFuelEventGeneratorFactory();
		}
		return factory;
	}

	/**
	 * @param frequency
	 * @return
	 */
	public FrequencyFuelEventGenerator getFrequencyFuelEventGenerator(InvoiceFreq frequency) {
		FrequencyFuelEventGenerator generator = null;

		if (frequency.equals(InvoiceFreq._PER_DELIVERY_)) {
			generator = new PerDeliveryFuelEventGenerator();
		} else {
			generator = new OneInvoiceFuelEventGenerator();
		}

		// switch (frequency) {
		// case _DAILY_:
		// generator = new DailyFuelEventGenerator();
		// break;
		// case _MONTHLY_:
		// generator = new MonthlyFuelEventGenerator();
		// break;
		// case _SEMIMONTHLY_:
		// generator = new SemimonthlyFuelEventGenerator();
		// break;
		// case _WEEKLY_:
		// generator = new WeeklyFuelEventGenerator();
		// break;
		// case _PER_DELIVERY_:
		// generator = new PerDeliveryFuelEventGenerator();
		// break;
		// case _EVERY_:
		// generator = new OneInvoiceFuelEventGenerator();
		// break;
		// default:
		// LOG.error("ERROR:could not locate a Frequency Fuel Event Generator for type " + frequency + "!!!");
		// break;
		// }

		return generator;
	}

}
