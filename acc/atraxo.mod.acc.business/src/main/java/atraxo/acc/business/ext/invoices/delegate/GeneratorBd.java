package atraxo.acc.business.ext.invoices.delegate;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.api.invoices.IOutgoingInvoiceService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.fuelEvents.delegate.DensityDelegate;
import atraxo.acc.business.ext.fuelEvents.service.PriceRestriction_Service;
import atraxo.acc.business.ext.invoices.delegate.generator.FrequencyFuelEventGenerator;
import atraxo.acc.business.ext.invoices.delegate.generator.FrequencyFuelEventGeneratorFactory;
import atraxo.acc.business.ext.invoices.delegate.generator.InvoicingDatePeriod;
import atraxo.acc.domain.ext.invoices.GeneratorResponse;
import atraxo.acc.domain.impl.acc_type.ControlNo;
import atraxo.acc.domain.impl.acc_type.InvoiceCategory;
import atraxo.acc.domain.impl.acc_type.InvoiceExportStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceFormAcc;
import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.acc_type.InvoiceTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.acc_type.ReceivedStatus;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.business.api.documentNumberSeries.IDocumentNumberSeriesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.exceptions.DocumentSeriesOutOfBoundException;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.documentNumberSeries.DocumentNumberSeries;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * @author zspeter
 * @param <T>
 */
public class GeneratorBd<T extends AbstractEntity> extends AbstractBusinessDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(GeneratorBd.class);

	private Map<FuelEvent, T> fuelEventDocMap = new HashMap<>();

	/**
	 * @param fuelEvent
	 * @param priceCategory
	 * @return
	 * @throws BusinessException
	 */
	public boolean isNotUsed(FuelEvent fuelEvent, ContractPriceCategory priceCategory) throws BusinessException {
		PriceRestriction_Service restrictionSrv = this.getApplicationContext().getBean(PriceRestriction_Service.class);
		return priceCategory.getRestriction() && !restrictionSrv.applyRestriction(priceCategory, fuelEvent);
	}

	/**
	 * @param startDate
	 * @param endDate
	 * @param transactionType
	 * @param customer
	 * @return
	 * @throws BusinessException
	 */
	public GeneratorResponse generate(Date startDate, Date endDate, Date closeDate, TransactionType transactionType, boolean separatePerShipTo,
			Customer customer) throws BusinessException {
		return this.generate(startDate, endDate, closeDate, transactionType, customer, separatePerShipTo, (Locations) null);
	}

	/**
	 * @param startDate
	 * @param endDate
	 * @param transactionType
	 * @param customer
	 * @param separatePerShipTo
	 * @param area
	 * @return
	 * @throws BusinessException
	 */
	public GeneratorResponse generate(Date startDate, Date endDate, Date closeDate, TransactionType transactionType, Customer customer,
			boolean separatePerShipTo, Area area) throws BusinessException {
		GeneratorResponse response = new GeneratorResponse(0, null);

		Set<Locations> set = new HashSet<>();

		if (area.getLocations() != null) {
			set.addAll(area.getLocations());
		}
		for (Locations location : set) {
			GeneratorResponse partialResponse = this.generate(startDate, endDate, closeDate, transactionType, customer, separatePerShipTo, location);
			response.setNrInvoicesGenerated(response.getNrInvoicesGenerated() + partialResponse.getNrInvoicesGenerated());
			if (partialResponse.getErrorMsg() != null) {
				response.setErrorMsg(partialResponse.getErrorMsg());
				break;
			}
		}
		return response;
	}

	/**
	 * @param invoiceGenerationDate
	 * @param endDate
	 * @param transactionType
	 * @param customer
	 * @param separatePerShipTo
	 * @param location
	 * @return
	 * @throws BusinessException
	 */
	public GeneratorResponse generate(Date invoiceGenerationDate, Date endDate, Date closeDate, TransactionType transactionType, Customer customer,
			boolean separatePerShipTo, Locations location) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START generate().");
		}
		int generatedInvoiceNumber = 0;

		GeneratorResponse response = new GeneratorResponse(generatedInvoiceNumber, null);

		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		boolean approved = paramSrv.getSalesInvoiceApprovalWorkflow();
		BillStatus status = this.getBillStatus(approved);
		OutgoingInvoiceStatus invStatus = this.getInvoiceStatus(approved);

		IFuelEventService fuelEventService = (IFuelEventService) this.findEntityService(FuelEvent.class);

		List<FuelEvent> allEventsList = fuelEventService.getForOutgoingInvoicesIgnoreStart(endDate, customer, location);
		List<FuelEvent> invoicedEventsList = new ArrayList<>();

		// group the events by Frequency and filter by ReceivedStatus
		EnumMap<InvoiceFreq, List<FuelEvent>> eventsByFrequencyMap = this.groupByFrequency(allEventsList);

		// iterate through the events grouped by frequency and generate invoices
		for (Iterator<InvoiceFreq> it = eventsByFrequencyMap.keySet().iterator(); it.hasNext();) {
			InvoiceFreq keyFreq = it.next();
			List<FuelEvent> events = eventsByFrequencyMap.get(keyFreq);

			if (events != null && !events.isEmpty()) {

				FrequencyFuelEventGenerator freqFuelEventGen = FrequencyFuelEventGeneratorFactory.getFactory()
						.getFrequencyFuelEventGenerator(keyFreq);

				if (freqFuelEventGen != null) {

					if (!events.isEmpty()) {

						// sort the events
						events.sort((o1, o2) -> o1.getFuelingDate().compareTo(o2.getFuelingDate()));

						// split the events by frequency
						Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap = freqFuelEventGen.splitByFrequency(events);
						for (Iterator<InvoicingDatePeriod> itr = eventsMap.keySet().iterator(); itr.hasNext();) {
							InvoicingDatePeriod keyPeriod = itr.next();
							List<FuelEvent> eventsPerPeriod = eventsMap.get(keyPeriod);

							if (!eventsPerPeriod.isEmpty()) {
								// generate invoices for the events and add the number of invoices to the total number of invoices
								GeneratorResponse partialResponse = this.generateInvoicesForEvents(eventsPerPeriod, invStatus, status,
										transactionType, separatePerShipTo, invoiceGenerationDate, closeDate);
								response.setNrInvoicesGenerated(response.getNrInvoicesGenerated() + partialResponse.getNrInvoicesGenerated());
								if (partialResponse.getErrorMsg() != null) {
									response.setErrorMsg(partialResponse.getErrorMsg());
								}

								// mark these events as invoiced and add them to the invoiced fuele events list
								invoicedEventsList.addAll(eventsPerPeriod);
							}
						}
						if (response.getErrorMsg() != null) {
							break;
						}
					}
				} else {
					LOGGER.error("ERROR:could not locate a FrequencyFuelEventGenerator for invoice frequency:" + keyFreq.getName()
							+ " !!!! Skipping !!!!");
				}

			}
		}

		// also update the events that were invoiced
		fuelEventService.update(allEventsList);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END generate(); generatedInvoiceNumber= {}.", generatedInvoiceNumber);
		}
		return response;
	}

	private EnumMap<InvoiceFreq, List<FuelEvent>> groupByFrequency(List<FuelEvent> allEventsList) throws BusinessException {
		EnumMap<InvoiceFreq, List<FuelEvent>> eventsByFrequencyMap = new EnumMap<>(InvoiceFreq.class);
		for (FuelEvent event : allEventsList) {
			// also, make sure to not include the On Hold events
			if (!event.getReceivedStatus().equals(ReceivedStatus._ON_HOLD_)) {
				InvoiceFreq freq = this.getFrequency(event);
				if (freq != null) {
					if (!eventsByFrequencyMap.containsKey(freq)) {
						eventsByFrequencyMap.put(freq, new ArrayList<FuelEvent>());
					}
					eventsByFrequencyMap.get(freq).add(event);
				} else {
					LOGGER.warn("WARNING:could not extract Frequency for event with ID " + event.getId()
							+ "; will not include it in invoice generation !");
				}
			}
		}
		return eventsByFrequencyMap;
	}

	/**
	 * @param eventsPerPeriod
	 * @param invStatus
	 * @param status
	 * @param transactionType
	 * @param separatePerShipTo
	 * @return
	 * @throws BusinessException
	 */
	private GeneratorResponse generateInvoicesForEvents(List<FuelEvent> eventsPerPeriod, OutgoingInvoiceStatus invStatus, BillStatus status,
			TransactionType transactionType, boolean separatePerShipTo, Date invoiceGenerationDate, Date closeDate) throws BusinessException {

		// calculate the last delivery date : it's the last existing fueling date from this group of events

		Date lastDeliveryDate = invoiceGenerationDate == null ? eventsPerPeriod.get(eventsPerPeriod.size() - 1).getFuelingDate()
				: invoiceGenerationDate;

		Map<InvoiceBussinesKey<?>, OutgoingInvoice> invoiceMap = new HashMap<>();
		String error = null;
		UnitConverterService unitConverterService = this.getApplicationContext().getBean(UnitConverterService.class);
		// generate invoices for the filtered groups of events
		for (FuelEvent event : eventsPerPeriod) {
			T saleDocument = this.getSaleDocument(event);
			int paymentTerms = this.getPaymentTerm(saleDocument);
			InvoiceBussinesKey<T> key = new InvoiceBussinesKey<>(event.getCustomer(), event.getDeparture(), saleDocument,
					separatePerShipTo ? event.getShipTo() : null);

			if (!invoiceMap.containsKey(key)) {
				try {
					OutgoingInvoice outInvoice = this.generateInvoiceHeader(event, status, saleDocument, transactionType, paymentTerms,
							lastDeliveryDate, closeDate);
					invoiceMap.put(key, outInvoice);
				} catch (DocumentSeriesOutOfBoundException e) {
					LOGGER.warn("Warning:could not generate invoice header !", e);
					error = e.getMessage();
				}
			}

			OutgoingInvoice invoice = invoiceMap.get(key);
			if (invoice != null) {
				this.addInvoiceLine(invStatus, event, invoice, unitConverterService);
				this.updateFuelEvent(invoice, invStatus, event);
			}
		}

		IOutgoingInvoiceService invSrv = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		invSrv.insert(new ArrayList<>(invoiceMap.values()));

		if (OutgoingInvoiceStatus._AWAITING_PAYMENT_.equals(invStatus)) {
			for (OutgoingInvoice oi : invoiceMap.values()) {
				invSrv.createInvoiceReport(oi);
			}
		}

		if (BillStatus._AWAITING_PAYMENT_.equals(status)) {
			this.getBusinessDelegate(IncomingInvoiceGenerator_Bd.class).generate(new ArrayList<>(invoiceMap.values()));
		}

		int generatedInvoiceNumber = invoiceMap.size();

		return new GeneratorResponse(generatedInvoiceNumber, error);
	}

	/**
	 * @param events
	 * @param invoice
	 * @throws BusinessException
	 */
	public void addInvoiceLines(List<FuelEvent> events, OutgoingInvoice invoice) throws BusinessException {
		OutgoingInvoiceStatus invStatus;
		switch (invoice.getStatus()) {
		case _AWAITING_PAYMENT_:
			invStatus = OutgoingInvoiceStatus._AWAITING_PAYMENT_;
			break;
		case _AWAITING_APPROVAL_:
			invStatus = OutgoingInvoiceStatus._AWAITING_APPROVAL_;
			break;
		case _PAID_:
			invStatus = OutgoingInvoiceStatus._PAID_;
			break;
		default:
			invStatus = OutgoingInvoiceStatus._NOT_INVOICED_;
			break;
		}
		UnitConverterService unitConverterService = this.getApplicationContext().getBean(UnitConverterService.class);
		for (FuelEvent event : events) {
			this.addInvoiceLine(invStatus, event, invoice, unitConverterService);
			this.updateFuelEvent(invoice, invStatus, event);

		}
		IOutgoingInvoiceService invSrv = (IOutgoingInvoiceService) this.findEntityService(OutgoingInvoice.class);
		IFuelEventService srv = (IFuelEventService) this.findEntityService(FuelEvent.class);
		srv.update(events);
		invSrv.update(invoice);
	}

	private void updateFuelEvent(OutgoingInvoice invoice, OutgoingInvoiceStatus invStatus, FuelEvent event) throws BusinessException {
		event.setInvoiceStatus(invStatus);
		event.setSalesInvoiceNo(invoice.getInvoiceNo());
		if (event.getPayableCost() != null) {
			event.setPurchaseInvoiceNo(invoice.getInvoiceNo());
		}
		event.setRegenerateDetails(false);
	}

	private void addInvoiceLine(OutgoingInvoiceStatus invStatus, FuelEvent event, OutgoingInvoice invoice, UnitConverterService unitConverterService)
			throws BusinessException {
		Density density = this.getBusinessDelegate(DensityDelegate.class).getDensity(event);
		OutgoingInvoiceLine invLine = this.generateInvoiceLine(event, invoice.getUnit(), density, unitConverterService);
		invoice.addToInvoiceLines(invLine);
		this.generateInvoiceLineDetails(invLine, event);
		this.adjustInvoiceHeader(invLine);
		this.adjustFlightEventStatus(event, invStatus);
	}

	/**
	 * @param line
	 * @param event
	 * @throws BusinessException
	 */
	public void generateInvoiceLineDetails(OutgoingInvoiceLine line, FuelEvent event) {
		if (!InvoiceReferenceDocType._CONTRACT_.equals(line.getOutgoingInvoice().getReferenceDocType())) {
			return;
		}
		for (FuelEventsDetails feDetails : event.getDetails()) {
			if (DealType._SELL_.equals(feDetails.getTransaction())) {
				OutgoingInvoiceLineDetails invLineDetails = new OutgoingInvoiceLineDetails();
				invLineDetails.setCalculateIndicator(feDetails.getCalculateIndicator());
				invLineDetails.setExchangeRate(feDetails.getExchangeRate());
				invLineDetails.setMainCategory(feDetails.getMainCategory());
				invLineDetails.setOriginalPrice(feDetails.getOriginalPrice());
				invLineDetails.setOriginalPriceCurrency(feDetails.getOriginalPriceCurrency());
				invLineDetails.setOriginalPriceUnit(feDetails.getOriginalPriceUnit());
				invLineDetails.setPriceCategory(feDetails.getPriceCategory());
				invLineDetails.setPriceName(feDetails.getPriceName());
				invLineDetails.setSettlementAmount(feDetails.getSettlementAmount());
				invLineDetails.setSettlementPrice(feDetails.getSettlementPrice());
				invLineDetails.setVatAmount(feDetails.getVatAmount());
				invLineDetails.setOutgoingInvoice(line.getOutgoingInvoice());
				line.addToDetails(invLineDetails);
			}
		}
	}

	public ContractPriceComponent getPriceFromPriceComponents(List<ContractPriceComponentConv> list, ContractPriceCategory cpCategory,
			Date fuelingDate) throws BusinessException {
		BigDecimal price = BigDecimal.ZERO;
		ContractPriceComponent retComp = new ContractPriceComponent();
		IContractPriceCategoryService cpcService = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);

		// SONE-5969 - percent inside composite
		if (PriceInd._COMPOSITE_.equals(cpCategory.getPriceCategory().getPricePer())) {
			ContractPriceComponent compositeComponent = this.getPriceComponentPerDate(fuelingDate, cpCategory);
			for (ContractPriceCategory compositeParent : cpCategory.getParentPriceCategory()) {
				if (PriceInd._PERCENT_.equals(compositeParent.getPriceCategory().getPricePer())) {
					return this.getPercent(cpCategory, fuelingDate, cpcService, compositeComponent, compositeParent);
				}
			}
		}

		if (list.size() == 1) {
			return list.get(0).getContractPriceComponent();
		}
		// we have to deal with composite price category
		for (ContractPriceComponentConv cpc : list) {
			price = price.add(cpc.getEquivalent());
		}
		retComp.setPrice(price);
		retComp.setCurrency(cpCategory.getContract().getSettlementCurr());
		retComp.setUnit(cpCategory.getContract().getSettlementUnit());
		retComp.setContrPriceCtgry(cpCategory);
		return retComp;
	}

	private ContractPriceComponent getPercent(ContractPriceCategory cpCategory, Date fuelingDate, IContractPriceCategoryService cpcService,
			ContractPriceComponent compositeComponent, ContractPriceCategory compositeParent) throws BusinessException {
		for (ContractPriceCategory percentParent : compositeParent.getParentPriceCategory()) {
			for (ContractPriceComponent component : percentParent.getPriceComponents()) {
				if (fuelingDate.compareTo(component.getValidFrom()) >= 0 && fuelingDate.compareTo(component.getValidTo()) <= 0) {
					ContractPriceComponent retComp = new ContractPriceComponent();
					retComp.setCurrency(compositeComponent != null ? compositeComponent.getCurrency() : component.getCurrency());
					retComp.setUnit(compositeComponent != null ? compositeComponent.getUnit() : component.getUnit());
					retComp.setContrPriceCtgry(cpCategory);
					retComp.setPrice(
							cpcService.getPriceInCurrencyUnit(cpCategory, retComp.getUnit().getCode(), retComp.getCurrency().getCode(), fuelingDate));
					return retComp;
				}
			}
		}
		return new ContractPriceComponent();
	}

	private ContractPriceComponent getPriceComponentPerDate(Date fuelingDate, ContractPriceCategory cpCategory) {
		for (ContractPriceComponent component : cpCategory.getPriceComponents()) {
			if (fuelingDate.compareTo(component.getValidFrom()) >= 0 && fuelingDate.compareTo(component.getValidTo()) <= 0) {
				return component;
			}
		}
		return null;
	}

	/**
	 * @param event
	 * @param c
	 * @return
	 * @throws BusinessException
	 */
	public List<ContractPriceComponentConv> getPriceComponentPerCategory(FuelEvent event, ContractPriceCategory c) throws BusinessException {
		List<ContractPriceComponent> componentList = this.buildComponentList(event, c);
		List<ContractPriceComponentConv> retList = new ArrayList<>();
		Date fuelingDate = DateUtils.truncate(event.getFuelingDate(), Calendar.DAY_OF_MONTH);
		for (ContractPriceComponent comp : componentList) {
			if (comp.getContrPriceCtgry().getPricingBases().getValidFrom().compareTo(fuelingDate) <= 0
					&& comp.getContrPriceCtgry().getPricingBases().getValidTo().compareTo(fuelingDate) >= 0) {
				for (ContractPriceComponentConv conv : comp.getConvertedPrices()) {
					if (conv.getValidFrom().compareTo(fuelingDate) <= 0 && conv.getValidTo().compareTo(fuelingDate) >= 0) {
						retList.add(conv);
					}
				}
			}
		}
		return retList;
	}

	private List<ContractPriceComponent> buildComponentList(FuelEvent event, ContractPriceCategory c) throws BusinessException {
		List<ContractPriceComponent> componentList = new ArrayList<>();
		if (PriceInd._COMPOSITE_.equals(c.getPriceCategory().getPricePer())) {
			for (ContractPriceCategory cpc : c.getParentPriceCategory()) {
				if (!this.isNotUsed(event, cpc)) {
					componentList.addAll(cpc.getPriceComponents());
				}
			}
		} else {
			componentList.addAll(c.getPriceComponents());
		}
		return componentList;
	}

	/**
	 * @param event
	 * @param invStatus
	 * @throws BusinessException
	 */
	public void adjustFlightEventStatus(FuelEvent event, OutgoingInvoiceStatus invStatus) throws BusinessException {
		if (FuelOrderLocation.class.getSimpleName().equalsIgnoreCase(event.getBillPriceSourceType())) {
			IFlightEventService srv = (IFlightEventService) this.findEntityService(FlightEvent.class);
			for (DeliveryNote dn : event.getDeliveryNotes()) {
				if (QuantitySource._FUEL_ORDER_.equals(dn.getSource())) {
					int flightEventId = dn.getObjectId();
					FlightEvent flightEvent = srv.findById(flightEventId);
					flightEvent.setInvoiceStatus(invStatus);
					srv.updateWithoutBusiness(flightEvent);
				}
			}
		}
	}

	/**
	 * @param invLine
	 */
	public void adjustInvoiceHeader(OutgoingInvoiceLine invLine) {
		OutgoingInvoice invoice = invLine.getOutgoingInvoice();
		invoice.setItemsNumber(invoice.getItemsNumber() + 1);
		invoice.setNetAmount(invoice.getNetAmount().add(invLine.getAmount(), MathContext.DECIMAL64));
		invoice.setQuantity(invoice.getQuantity().add(invLine.getQuantity(), MathContext.DECIMAL64));
		invoice.setTotalAmount(invoice.getTotalAmount().add(invLine.getAmount(), MathContext.DECIMAL64).add(invLine.getVat(), MathContext.DECIMAL64));
		invoice.setVatAmount(invoice.getVatAmount().add(invLine.getVat(), MathContext.DECIMAL64));
		if (invLine.getDeliveryDate().before(invoice.getDeliveryDateFrom())) {
			invoice.setDeliveryDateFrom(invLine.getDeliveryDate());
		}
		if (invLine.getDeliveryDate().after(invoice.getDeliverydateTo())) {
			invoice.setDeliverydateTo(invLine.getDeliveryDate());
		}
	}

	private BillStatus getBillStatus(boolean approved) {
		return approved ? BillStatus._AWAITING_APPROVAL_ : BillStatus._AWAITING_PAYMENT_;
	}

	private OutgoingInvoiceStatus getInvoiceStatus(boolean approved) {
		return approved ? OutgoingInvoiceStatus._AWAITING_APPROVAL_ : OutgoingInvoiceStatus._AWAITING_PAYMENT_;
	}

	private int getPaymentTerm(T saleDocument) {
		if (saleDocument instanceof Contract) {
			return ((Contract) saleDocument).getPaymentTerms() != null ? ((Contract) saleDocument).getPaymentTerms().intValue() : 0;
		}
		if (saleDocument instanceof FuelOrderLocation) {
			return ((FuelOrderLocation) saleDocument).getPaymentTerms() != null ? ((FuelOrderLocation) saleDocument).getPaymentTerms().intValue() : 0;
		}
		return 0;
	}

	private OutgoingInvoiceLine generateInvoiceLine(FuelEvent event, Unit invoiceUnit, Density density, UnitConverterService unitConverterService)
			throws BusinessException {
		OutgoingInvoiceLine invLine = new OutgoingInvoiceLine();
		ReceivableAccrual accrual = event.getReceivableAccruals().iterator().next();
		invLine.setAircraftRegistrationNumber(event.getAircraft() != null ? event.getAircraft().getRegistration() : null);
		invLine.setAmount(accrual.getAccrualAmountSet());
		invLine.setCustomer(event.getShipTo());
		invLine.setDeliveryDate(event.getFuelingDate());
		invLine.setDestination(event.getDeparture());
		invLine.setFlightNo(event.getFlightNumber());
		invLine.setFlightSuffix(event.getSuffix());
		invLine.setFlightType(event.getEventType());
		invLine.setFuelEvent(event);
		invLine.setIsCredited(false);
		invLine.setQuantity(unitConverterService.convert(event.getUnit(), invoiceUnit, event.getQuantity(), density));
		invLine.setTicketNo(event.getTicketNumber());
		invLine.setVat(accrual.getAccrualVATSet());
		invLine.setTransmissionStatus(InvoiceLineTransmissionStatus._NEW_);
		invLine.setBolNumber(event.getBolNumber());

		return invLine;
	}

	/**
	 * @param event
	 * @param status
	 * @param saleDocument
	 * @param transactionType
	 * @param paymentTerm
	 * @param lastDeliveryDate
	 * @return
	 * @throws BusinessException
	 */
	private OutgoingInvoice generateInvoiceHeader(FuelEvent event, BillStatus status, T saleDocument, TransactionType transactionType,
			int paymentTerm, Date lastDeliveryDate, Date closeDate) throws BusinessException {
		IDocumentNumberSeriesService srv = (IDocumentNumberSeriesService) this.findEntityService(DocumentNumberSeries.class);
		OutgoingInvoice invoice = new OutgoingInvoice();
		invoice.setCategory(InvoiceCategory._PRODUCT_INTO_PLANE_);
		Date today = GregorianCalendar.getInstance().getTime();
		invoice.setBaseLineDate(DateUtils.addDays(today, paymentTerm));
		invoice.setCountry(event.getDeparture().getCountry());
		invoice.setCurrency(event.getBillableCostCurrency());
		invoice.setDeliveryDateFrom(event.getFuelingDate());
		invoice.setDeliverydateTo(event.getFuelingDate());
		invoice.setDeliveryLoc(event.getDeparture());
		invoice.setInvoiceDate(lastDeliveryDate);
		invoice.setInvoiceForm(InvoiceFormAcc._ELECTRONIC_);
		invoice.setInvoiceNo(srv.getNextDocumentNumber(DocumentNumberSeriesType._INVOICE_));
		invoice.setInvoiceType(InvoiceTypeAcc._INV_);
		invoice.setCloseDate(closeDate);
		invoice.setIssueDate(today);
		invoice.setReceiver(event.getCustomer());
		invoice.setProduct(event.getProduct());
		invoice.setTransmissionStatus(InvoiceTransmissionStatus._NEW_);
		invoice.setCreditMemoReason(RevocationReason._EMPTY_);
		invoice.setInvoiceSent(false);
		if (saleDocument instanceof Contract) {
			Contract c = (Contract) saleDocument;
			invoice.setReferenceDocId(c.getId());
			invoice.setReferenceDocNo(c.getCode());
			invoice.setReferenceDocType(InvoiceReferenceDocType._CONTRACT_);
			invoice.setTaxType(c.getTax());
			invoice.setUnit(c.getSettlementUnit());
			invoice.setIssuer(c.getHolder());
			invoice.setSubsidiaryId(c.getSubsidiaryId());
			if (c.getBillTo() != null) {
				invoice.setReceiver(c.getBillTo());
			}
		} else if (saleDocument instanceof FuelOrderLocation) {
			FuelOrderLocation orderLocation = (FuelOrderLocation) saleDocument;
			invoice.setReferenceDocId(orderLocation.getId());
			invoice.setReferenceDocNo(orderLocation.getFuelOrder().getCode());
			invoice.setReferenceDocType(InvoiceReferenceDocType._PURCHASE_ORDER_);
			invoice.setTaxType(orderLocation.getContract().getTax());
			invoice.setUnit(orderLocation.getPaymentUnit());
			invoice.setIssuer(orderLocation.getFuelOrder().getSubsidiary());
		}
		invoice.setStatus(status);
		invoice.setApprovalStatus(BidApprovalStatus._NEW_);
		invoice.setTransactionType(transactionType.equals(TransactionType._EMPTY_) ? TransactionType._ORIGINAL_ : transactionType);
		invoice.setExportStatus(InvoiceExportStatus._NEW_);
		invoice.setItemsNumber(0);
		invoice.setNetAmount(BigDecimal.ZERO);
		invoice.setQuantity(BigDecimal.ZERO);
		invoice.setTotalAmount(BigDecimal.ZERO);
		invoice.setVatAmount(BigDecimal.ZERO);
		invoice.setControlNo(ControlNo._NOT_AVAILABLE_.getName());

		return invoice;
	}

	/**
	 * @param event
	 * @return
	 * @throws BusinessException
	 */
	private InvoiceFreq getFrequency(FuelEvent event) throws BusinessException {
		InvoiceFreq freq = null;
		T saleDocument = this.getSaleDocument(event);
		if (saleDocument instanceof Contract) {
			freq = ((Contract) saleDocument).getInvoiceFreq();
		} else if (saleDocument instanceof FuelOrderLocation) {
			freq = ((FuelOrderLocation) saleDocument).getInvoiceFrequency();
		}
		return freq;
	}

	@SuppressWarnings("unchecked")
	private T getSaleDocument(FuelEvent event) throws BusinessException {

		if (!this.fuelEventDocMap.containsKey(event)) {
			T t;
			if (event.getBillPriceSourceType() == null || event.getBillPriceSourceId() == null) {
				throw new BusinessException(AccErrorCode.NO_SALE_DOCUMENT_ASSIGNED, AccErrorCode.NO_SALE_DOCUMENT_ASSIGNED.getErrMsg());
			}
			switch (event.getBillPriceSourceType()) {
			case "Contract":
				IContractService contractSrv = (IContractService) this.findEntityService(Contract.class);
				t = (T) contractSrv.findById(event.getBillPriceSourceId());
				break;
			case "FuelOrderLocation":
				IFuelOrderLocationService locSrv = (IFuelOrderLocationService) this.findEntityService(FuelOrderLocation.class);
				FuelOrderLocation locOrder = locSrv.findById(event.getBillPriceSourceId());
				t = (T) locOrder;
				break;
			default:
				throw new BusinessException(AccErrorCode.INVALID_FUEL_EVENT, AccErrorCode.INVALID_FUEL_EVENT.getErrMsg());
			}
			this.fuelEventDocMap.put(event, t);
		}
		return this.fuelEventDocMap.get(event);
	}

}

class InvoiceBussinesKey<T extends AbstractEntity> {
	private Customer customer;
	private Locations location;
	private T saleDocument;
	private Customer shipTo;

	public InvoiceBussinesKey(Customer customer, Locations location, T saleDocument, Customer shiptTo) {
		super();
		this.customer = customer;
		this.location = location;
		this.saleDocument = saleDocument;
		this.shipTo = shiptTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.customer == null) ? 0 : this.customer.hashCode());
		result = prime * result + ((this.location == null) ? 0 : this.location.hashCode());
		result = prime * result + ((this.saleDocument == null) ? 0 : this.saleDocument.hashCode());
		result = prime * result + ((this.shipTo == null) ? 0 : this.shipTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		InvoiceBussinesKey<?> other = (InvoiceBussinesKey<?>) obj;
		if (this.customer == null) {
			if (other.customer != null) {
				return false;
			}
		} else if (!this.customer.equals(other.customer)) {
			return false;
		}
		if (this.location == null) {
			if (other.location != null) {
				return false;
			}
		} else if (!this.location.equals(other.location)) {
			return false;
		}
		if (this.saleDocument == null) {
			if (other.saleDocument != null) {
				return false;
			}
		} else if (!this.saleDocument.equals(other.saleDocument)) {
			return false;
		}
		if (this.shipTo == null) {
			if (other.shipTo != null) {
				return false;
			}
		} else if (!this.shipTo.equals(other.shipTo)) {
			return false;
		}
		return true;
	}
}