/**
 *
 */
package atraxo.acc.business.ext.invoices.delegate.generator;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.business.ext.utils.DateUtils;

/**
 * @author vhojda
 */
public class MonthlyFuelEventGenerator extends AbstractFuelEventGenerator {

	/**
	 *
	 */
	public MonthlyFuelEventGenerator() {
		// The explicit default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#filterByFrequency(java.util.Collection)
	 */
	@Override
	public void filterByFrequency(Collection<FuelEvent> events) {
		for (Iterator<FuelEvent> it = events.iterator(); it.hasNext();) {
			FuelEvent event = it.next();
			// if NOT in the past month then remove it
			if (!DateUtils.inPastMonth(event.getFuelingDate())) {
				it.remove();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.acc.business.ext.invoices.delegate.generator. FrequencyFuelEventGenerator#splitByFrequency(java.util.Collection)
	 */
	@Override
	public Map<InvoicingDatePeriod, List<FuelEvent>> splitByFrequency(List<FuelEvent> events) {
		Map<InvoicingDatePeriod, List<FuelEvent>> eventsMap = new HashMap<>();

		if (!events.isEmpty()) {
			FuelEvent firstFuelEvent = events.get(0);
			FuelEvent lastFuelEvent = events.get(events.size() - 1);

			Calendar firstFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(firstFuelEvent.getFuelingDate());
			Calendar lastFuelEventCal = DateUtils.getCalendarFromDateIgnoreHours(lastFuelEvent.getFuelingDate());

			int firstMonth = firstFuelEventCal.get(Calendar.YEAR) * 12 + firstFuelEventCal.get(Calendar.MONTH);
			int lastMonth = lastFuelEventCal.get(Calendar.YEAR) * 12 + lastFuelEventCal.get(Calendar.MONTH);

			// for each month put 1 collection of events
			int diff = lastMonth - firstMonth;
			for (int i = 0; i <= diff; i++) {
				Calendar startDayMonthCal = DateUtils.getCalendarFromCalendarIgnoreHours(firstFuelEventCal);
				startDayMonthCal.add(Calendar.MONTH, i);
				startDayMonthCal.set(Calendar.DAY_OF_MONTH, 1);

				Calendar endDayMonthCal = DateUtils.getCalendarFromCalendarIgnoreHours(startDayMonthCal);
				endDayMonthCal.set(Calendar.DAY_OF_MONTH, startDayMonthCal.getActualMaximum(Calendar.DAY_OF_MONTH));

				eventsMap.put(new InvoicingDatePeriod(startDayMonthCal, endDayMonthCal), null);
			}

			// fill in the events map depending on the period with events
			this.fillFuelEventsPeriodMap(eventsMap, events);

		}

		return eventsMap;
	}

}
