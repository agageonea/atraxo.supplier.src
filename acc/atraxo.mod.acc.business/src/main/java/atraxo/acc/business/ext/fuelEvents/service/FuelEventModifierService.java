package atraxo.acc.business.ext.fuelEvents.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.acc.business.api.deliveryNotes.IDeliveryNoteService;
import atraxo.acc.business.api.fuelEvents.IFuelEventService;
import atraxo.acc.business.ext.deliveryNotes.delegate.DeliveryNoteCostCalculatorDelegate;
import atraxo.acc.business.ext.deliveryNotes.delegate.LinkToPurchaseConctract_Bd;
import atraxo.acc.business.ext.deliveryNotes.service.DeliveryNoteBuilderService;
import atraxo.acc.business.ext.exceptions.AccErrorCode;
import atraxo.acc.business.ext.fuelEvents.delegate.DensityDelegate;
import atraxo.acc.business.ext.fuelEvents.delegate.FuelEventAssignSaleContract_Bd;
import atraxo.acc.business.ext.fuelEvents.delegate.FuelEvent_Bd;
import atraxo.acc.business.ext.invoices.delegate.GeneratorBd;
import atraxo.acc.business.ext.payableAccruals.delegate.PayableAccrualGenerate_Bd;
import atraxo.acc.business.ext.receivableAccruals.delegate.ReceivableGenerator_Bd;
import atraxo.acc.domain.ext.deliveryNotes.AccrualDateParameter;
import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.acc_type.ReceivedStatus;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.utils.DateUtils;
import atraxo.cmm.domain.ext.contracts.CostVat;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.cmm.domain.impl.prices.ContractPriceComponentConv;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import atraxo.ops.domain.impl.ops_type.TemperatureUnit;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author zspeter
 */
public class FuelEventModifierService extends AbstractBusinessBaseService {

	/**
	 * @author zspeter
	 */
	private final class DeliveryNoteComparator implements Comparator<DeliveryNote> {
		@Override
		public int compare(DeliveryNote o1, DeliveryNote o2) {
			if (o1.getSource().equals(QuantitySource._FUEL_TICKET_)) {
				return 1;
			} else if (o2.getSource().equals(QuantitySource._FUEL_TICKET_)) {
				return -1;
			} else if (o1.getSource().equals(QuantitySource._FUEL_ORDER_)) {
				return 1;
			} else if (o2.getSource().equals(QuantitySource._FUEL_ORDER_)) {
				return -1;
			} else {
				return -1;
			}
		}
	}

	private static final Logger LOG = LoggerFactory.getLogger(FuelEventModifierService.class);

	@Autowired
	private ISystemParameterService sysParamSrv;
	@Autowired
	private IDeliveryNoteService dnSrv;
	@Autowired
	private DeliveryNoteBuilderService dnBuilderSrv;
	@Autowired
	private IFuelEventService feSrv;
	@Autowired
	private CostCalculatorService calculatorSrv;
	@Autowired
	private IContractService contractSrv;
	@Autowired
	private IContractPriceCategoryService cpcSrv;
	@Autowired
	private IUnitService unitSrv;
	@Autowired
	private ICurrenciesService currenciesService;
	@Autowired
	private IFuelOrderLocationService orderLocationService;
	@Autowired
	private UnitConverterService unitConverterService;

	/**
	 * Generate fuel event from incoming invoice line;
	 *
	 * @param il
	 * @return
	 * @throws BusinessException
	 */
	public FuelEvent generate(InvoiceLine il) throws BusinessException {
		DeliveryNote dn = this.dnBuilderSrv.build(il);
		FuelEvent fe = this.getFuelEvent(dn, il);
		fe.addToDeliveryNotes(dn);
		this.updateFuelEvent(dn, fe);
		return fe;
	}

	/**
	 * Generate fuel event from fuel ticket and assign to sell contract.
	 *
	 * @param shipTo
	 * @param e
	 * @param sellContract
	 * @param shipTo
	 * @return
	 * @throws BusinessException
	 */
	public FuelEvent generate(FuelTicket e, Contract sellContract, Customer shipTo) throws BusinessException {
		DeliveryNote dn = this.dnBuilderSrv.build(e, null, sellContract != null ? sellContract.getCustomer() : null, shipTo);
		FuelEvent fe = this.getFuelEvent(dn, null);
		fe.addToDeliveryNotes(dn);
		this.addDensityAndTemperature(fe, e);
		this.assignSellContract(fe, sellContract);
		this.updateFuelEvent(dn, fe);
		return fe;
	}

	private void updateFuelEvent(DeliveryNote dn, FuelEvent fe) throws BusinessException {
		this.setUsed(dn);
		this.getBusinessDelegate(LinkToPurchaseConctract_Bd.class).linkToPurchaseContract(dn);
		this.getBusinessDelegate(DeliveryNoteCostCalculatorDelegate.class).calculatePayableCost(dn);
		if (dn.getUsed()) {
			this.updateFuelEvent(dn);
			this.calculateReceivableCost(fe, dn);
			this.generateAccruals(fe);
			this.generateDetails(fe);
		}
	}

	/**
	 * Generate fuel event from fuel ticket.
	 *
	 * @param ft
	 * @param fol
	 * @return
	 * @throws BusinessException
	 */
	public FuelEvent generate(FuelTicket ft, FuelOrderLocation fol) throws BusinessException {
		List<DeliveryNote> list = this.dnSrv.findByObjectIdAndType(fol.getId(), fol.getClass().getSimpleName());
		DeliveryNote dn = this.dnBuilderSrv.build(ft, null, fol.getFuelOrder().getCustomer(), fol.getFuelOrder().getCustomer());
		FuelEvent fe;
		if (!list.isEmpty()) {
			fe = list.get(0).getFuelEvent();
		} else {
			fe = this.getFuelEvent(dn, null);
		}
		fe.addToDeliveryNotes(dn);
		this.addDensityAndTemperature(fe, ft);
		this.updateFuelEvent(dn, fe);
		return fe;
	}

	/**
	 * Generate fuel event from flight event.
	 *
	 * @param flightEvent
	 * @return
	 * @throws BusinessException
	 */
	public FuelEvent generate(FlightEvent flightEvent) throws BusinessException {
		DeliveryNote dn = this.dnBuilderSrv.build(flightEvent);
		FuelEvent fe = this.getFuelEvent(dn, null);
		fe.setBillPriceSourceId(flightEvent.getLocOrder().getId());
		fe.setBillPriceSourceType(flightEvent.getLocOrder().getClass().getSimpleName());
		fe.setContractHolder(flightEvent.getLocOrder().getContract().getHolder());
		fe.setSubsidiaryId(flightEvent.getLocOrder().getContract().getSubsidiaryId());
		fe.addToDeliveryNotes(dn);
		this.updateFuelEvent(dn, fe);
		return fe;
	}

	private FuelEvent build(DeliveryNote dn, InvoiceLine il) throws BusinessException {
		FuelEvent fe = new FuelEvent();
		fe.setFuelingDate(dn.getFuelingDate());
		fe.setCustomer(dn.getCustomer());
		fe.setDeparture(dn.getDeparture());
		fe.setAircraft(dn.getAircraft());
		fe.setFlightNumber(dn.getFlightNumber());
		fe.setSuffix(dn.getSuffix());
		fe.setTicketNumber(dn.getTicketNumber() != null ? dn.getTicketNumber() : "");
		fe.setFuelSupplier(dn.getFuelSupplier());
		fe.setIplAgent(dn.getIplAgent());
		fe.setQuantity(dn.getQuantity() != null ? dn.getQuantity() : BigDecimal.ZERO);
		fe.setUnit(dn.getUnit());
		fe.setSysQuantity(this.getSystemQuantity(dn));
		fe.setQuantitySource(dn.getSource());
		fe.setInvoiceStatus(OutgoingInvoiceStatus._NOT_INVOICED_);
		fe.setBillStatus(il == null ? BillStatus._NOT_BILLED_ : il.getInvoice().getStatus());
		fe.setEventType(dn.getEventType());
		fe.setPayableCost(dn.getPayableCost());
		fe.setPayableCostCurrency(dn.getPayableCostCurrency());
		fe.setIsResale(Boolean.FALSE);
		fe.setNetUpliftQuantity(dn.getNetUpliftQuantity());
		fe.setNetUpliftUnit(dn.getNetUpliftUnit());
		fe.setFuelingOperation(dn.getFuelingOperation());
		fe.setTransport(dn.getTransport());
		fe.setOperationType(dn.getOperationType());
		fe.setProduct(dn.getProduct());
		fe.setShipTo(dn.getShipTo());
		fe.setFlightID(dn.getFlightID());
		fe.setContractHolder(dn.getContractHolder());
		fe.setTransmissionStatusSale(FuelEventTransmissionStatus._NEW_);
		fe.setTransmissionStatusPurchase(FuelEventTransmissionStatus._NEW_);
		fe.setStatus(dn.getTicketStatus() != null ? FuelEventStatus.getByName(dn.getTicketStatus().getName()) : FuelEventStatus._ORIGINAL_);
		fe.setPurchaseInvoiceNo(il != null ? il.getInvoice().getInvoiceNo() : "");
		fe.setTemperatureUnit(TemperatureUnit._EMPTY_);
		fe.setRevocationReason(RevocationReason._EMPTY_);
		String defaultFuelEventReceivedStatus = this.sysParamSrv.getDefaultFuelEventReceivedStatus();
		ReceivedStatus receivedStatus = ReceivedStatus.getByName(defaultFuelEventReceivedStatus);
		if (LOG.isDebugEnabled()) {
			LOG.debug("DEFAULT fuel event received status name:" + defaultFuelEventReceivedStatus);
			LOG.debug("DEFAULT fuel event received status:" + receivedStatus);
		}
		fe.setReceivedStatus(receivedStatus);
		return fe;
	}

	/**
	 * Update and existing fuel event.
	 *
	 * @param ft
	 * @param fe
	 * @param sellContract
	 * @throws BusinessException
	 */
	public void update(FuelTicket ft, FuelEvent fe, Contract sellContract) throws BusinessException {
		this.setUsed(fe);
		this.assignSellContract(fe, sellContract);
		for (DeliveryNote dn : fe.getDeliveryNotes()) {
			if (dn.getObjectType().equalsIgnoreCase(ft.getClass().getSimpleName()) && dn.getObjectId().equals(ft.getId())) {
				this.getBusinessDelegate(LinkToPurchaseConctract_Bd.class).linkToPurchaseContract(dn);
				this.getBusinessDelegate(DeliveryNoteCostCalculatorDelegate.class).calculatePayableCost(dn);
				this.updateFuelEvent(dn);
				this.calculateReceivableCost(fe, dn);
			}
		}
		this.addDensityAndTemperature(fe, ft);
		this.generateAccruals(fe);
		this.generateDetailsFromContract(fe, sellContract);
		this.removeOldDetails(fe);
	}

	/**
	 * Remove a delivery note/fuel event generated from a flight event.
	 *
	 * @param updList
	 * @param delList
	 * @param ft
	 * @throws BusinessException
	 */
	public void remove(List<FuelEvent> updList, List<FuelEvent> delList, FuelTicket ft) throws BusinessException {
		List<DeliveryNote> dnList = this.dnSrv.findByObjectIdAndType(ft.getId(), ft.getClass().getSimpleName());
		this.remove(updList, delList, dnList);
	}

	private void remove(List<FuelEvent> updList, List<FuelEvent> delList, List<DeliveryNote> dnList) throws BusinessException {
		for (DeliveryNote dn : dnList) {
			if (this.canDelete(dn)) {
				FuelEvent fuelEvent = dn.getFuelEvent();
				fuelEvent.getDeliveryNotes().remove(dn);
				if (fuelEvent.getDeliveryNotes().isEmpty()) {
					delList.add(fuelEvent);
				} else {
					this.setUsed(fuelEvent);
					this.removeBillableCost(fuelEvent, dn);
					this.calculateReceivableCost(fuelEvent, null);
					updList.add(fuelEvent);
				}
			}
		}
	}

	/**
	 * Remove a delivery note/fuel event generated from a flight event.
	 *
	 * @param updList
	 * @param delList
	 * @param fe
	 * @throws BusinessException
	 */
	public void remove(List<FuelEvent> updList, List<FuelEvent> delList, FlightEvent fe) throws BusinessException {
		List<DeliveryNote> dnList = this.dnSrv.findByObjectIdAndType(fe.getId(), fe.getClass().getSimpleName());
		this.remove(updList, delList, dnList);
	}

	/**
	 * Generate accruals (receivable and payable for fuel event).
	 *
	 * @param fe
	 * @param dn
	 * @throws BusinessException
	 */
	public void generateAccruals(FuelEvent fe) throws BusinessException {
		if (fe.getBillPriceSourceType() != null) {
			// resale event, no receivable contract
			this.getBusinessDelegate(ReceivableGenerator_Bd.class).generate(fe);
		}
		this.getBusinessDelegate(PayableAccrualGenerate_Bd.class).generate(fe);
	}

	/**
	 * @param dn
	 * @return An existing fuel event associated to delivery note, or generate one.
	 * @throws BusinessException
	 */
	private FuelEvent getFuelEvent(DeliveryNote dn, InvoiceLine il) throws BusinessException {
		try {
			return this.feSrv.findByKey(dn.getFuelingDate(), dn.getCustomer(), dn.getDeparture(), dn.getAircraft(), dn.getFlightNumber(),
					dn.getSuffix(), dn.getTicketNumber());
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_NO_RESULT.equals(ex.getErrorCode())) {
				LOG.warn("Delivery get by business key  " + ex.getMessage(), ex);
				return this.build(dn, il);
			} else if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(ex.getErrorCode())) {
				throw new BusinessException(AccErrorCode.FUEL_EVENT_DUPLICATE_BUSINESS_KEY,
						String.format(AccErrorCode.FUEL_EVENT_DUPLICATE_BUSINESS_KEY.getErrMsg(), dn.getFuelingDate(), dn.getCustomer().getCode(),
								dn.getDeparture().getCode(), dn.getAircraft().getRegistration(), dn.getFlightNumber(), dn.getTicketNumber(),
								dn.getSuffix()),
						ex);
			}
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e.getMessage(), e);
		}
		throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Cannot get Fuel event");
	}

	/**
	 * Get Quantity from delivery note in system volume.
	 *
	 * @param dn
	 * @return
	 * @throws BusinessException
	 */
	private BigDecimal getSystemQuantity(DeliveryNote dn) throws BusinessException {
		Density density = this.getBusinessDelegate(DensityDelegate.class).getDensity(dn.getFuelEvent());
		return this.unitConverterService.convert(dn.getUnit(), this.unitSrv.findByCode(this.sysParamSrv.getSysVol()), dn.getQuantity(), density);
	}

	/**
	 * @param dn
	 * @throws BusinessException
	 */
	private void setUsed(DeliveryNote dn) throws BusinessException {
		FuelEvent fe = dn.getFuelEvent();
		fe = fe.getId() != null ? this.feSrv.findById(fe.getId()) : fe;
		Collection<DeliveryNote> notes = fe.getDeliveryNotes() != null ? fe.getDeliveryNotes() : Collections.emptyList();
		if (CollectionUtils.isEmpty(notes) || QuantitySource._FUEL_TICKET_.equals(dn.getSource())) {
			dn.setUsed(true);
			if (fe.getSubsidiaryId() == null && !CollectionUtils.isEmpty(dn.getDeliveryNoteContracts())) {
				fe.setSubsidiaryId(dn.getDeliveryNoteContracts().iterator().next().getContracts().getSubsidiaryId());
			}
			dn.setFuelEvent(fe);
			for (DeliveryNote note : notes) {
				if (note != null && !note.equals(dn)) {
					note.setUsed(false);
					this.dnSrv.update(note);
				}
			}
		} else if (notes.size() == 1) {
			dn.setUsed(true);
		} else {
			dn.setUsed(false);
		}
	}

	private boolean canDelete(DeliveryNote dn) {
		return !OutgoingInvoiceStatus._PAID_.equals(dn.getFuelEvent().getInvoiceStatus())
				&& !OutgoingInvoiceStatus._AWAITING_PAYMENT_.equals(dn.getFuelEvent().getInvoiceStatus())
				&& !BillStatus._AWAITING_PAYMENT_.equals(dn.getFuelEvent().getBillStatus())
				&& !BillStatus._PAID_.equals(dn.getFuelEvent().getBillStatus());
	}

	private void assignSellContract(FuelEvent fe, Contract sellContract) {
		if (sellContract != null
				&& (fe.getBillPriceSourceType() == null || !fe.getBillPriceSourceType().equalsIgnoreCase(FuelOrderLocation.class.getSimpleName()))) {
			fe.setBillPriceSourceId(sellContract.getId());
			fe.setBillPriceSourceType(sellContract.getClass().getSimpleName());
			fe.setSubsidiaryId(sellContract.getSubsidiaryId());
			fe.setContractHolder(sellContract.getHolder());
		}
	}

	/**
	 * Update fuel event based on delivery note.
	 *
	 * @param dn
	 * @throws BusinessException
	 */
	public void updateFuelEvent(DeliveryNote dn) throws BusinessException {
		if (!dn.getUsed()) {
			return;
		}
		FuelEvent fe = dn.getFuelEvent();
		fe.setQuantity(dn.getQuantity());
		fe.setUnit(dn.getUnit());
		fe.setSysQuantity(this.getSystemQuantity(dn));
		fe.setQuantitySource(dn.getSource());
		if ((fe.getPayableCost() != null)
				&& (!dn.getPayableCost().setScale(6, RoundingMode.HALF_UP).equals(fe.getPayableCost().setScale(6, RoundingMode.HALF_UP)))) {
			fe.setStatus(FuelEventStatus._UPDATED_);
		}
		fe.setPayableCost(dn.getPayableCost());
		fe.setPayableCostCurrency(dn.getPayableCostCurrency());
		fe.setSysPayableCost(this.getBusinessDelegate(FuelEvent_Bd.class).calculateSysPayableCost(fe));
	}

	/**
	 * Calculate receivable cost.
	 *
	 * @param fuelEvent
	 * @param dn
	 * @throws BusinessException
	 */
	public boolean calculateReceivableCost(FuelEvent fuelEvent, DeliveryNote dn) throws BusinessException {
		if (fuelEvent.getBillPriceSourceId() == null || fuelEvent.getBillPriceSourceType() == null) {
			return false;
		}
		BigDecimal sysCost = this.getCostInSysMeasurment(fuelEvent);
		BigDecimal settCost = this.getCostInSettMeasurment(fuelEvent);
		if (fuelEvent.getBillableCost() != null) {
			if (sysCost.setScale(6, RoundingMode.HALF_UP).equals(fuelEvent.getSysBillableCost().setScale(6, RoundingMode.HALF_UP))
					&& settCost.setScale(6, RoundingMode.HALF_UP).equals(fuelEvent.getBillableCost().setScale(6, RoundingMode.HALF_UP))) {
				return false;
			}
			fuelEvent.setOldBillableCost(fuelEvent.getBillableCost());
		}
		fuelEvent.setSysBillableCost(sysCost);
		fuelEvent.setBillableCost(settCost);
		this.setDeliveryNotesBillPrice(fuelEvent, dn);
		return true;
	}

	private void setUsed(FuelEvent e) {
		List<DeliveryNote> list = new ArrayList<>();
		list.addAll(e.getDeliveryNotes());
		list.sort(new DeliveryNoteComparator());
		boolean used = false;
		for (DeliveryNote dn : list) {
			switch (dn.getSource()) {
			case _FUEL_TICKET_:
				used = true;
				dn.setUsed(used);
				if (e.getSubsidiaryId() == null && CollectionUtils.isEmpty(dn.getDeliveryNoteContracts())) {
					e.setSubsidiaryId(dn.getDeliveryNoteContracts().iterator().next().getContracts().getSubsidiaryId());
				}
				break;
			case _FUEL_ORDER_:
			case _INVOICE_:
				if (!used) {
					used = true;
					dn.setUsed(used);
					if (e.getSubsidiaryId() == null && !dn.getDeliveryNoteContracts().isEmpty()) {
						e.setSubsidiaryId(dn.getDeliveryNoteContracts().iterator().next().getContracts().getSubsidiaryId());
					}
				}
				break;
			default:
				dn.setUsed(false);
				break;
			}
		}
	}

	private void removeBillableCost(FuelEvent fe, DeliveryNote dn) throws BusinessException {
		if (QuantitySource._FUEL_ORDER_.equals(dn.getSource())) {
			fe.setBillableCost(null);
			fe.setBillableCostCurrency(null);
			fe.setBillPriceSourceId(null);
			fe.setBillPriceSourceType(null);
			DeliveryNote dnUsed = this.getUsedDeliveryNote(fe);
			FuelEventAssignSaleContract_Bd bd = this.getBusinessDelegate(FuelEventAssignSaleContract_Bd.class);
			bd.assignContract(fe, dnUsed);
		}
	}

	private DeliveryNote getUsedDeliveryNote(FuelEvent fe) {
		for (DeliveryNote dn : fe.getDeliveryNotes()) {
			if (dn.getUsed()) {
				return dn;
			}
		}
		return null;
	}

	private BigDecimal getCostInSysMeasurment(FuelEvent fuelEvent) throws BusinessException {
		String systemCurrencyCode = this.sysParamSrv.getSysCurrency();
		Currencies sysCurrency = this.currenciesService.findByCode(systemCurrencyCode);
		ExchangeRate_Bd exchBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		String accrualDateParameter = this.sysParamSrv.getAccrualEvaluationDate();
		Date date = new Date();
		if (accrualDateParameter.equalsIgnoreCase(AccrualDateParameter.EVENT_DATE.getValue())) {
			date = fuelEvent.getFuelingDate();
		}
		if (fuelEvent.getBillPriceSourceType().equalsIgnoreCase(FuelOrderLocation.class.getSimpleName())) {
			FuelOrderLocation orderLocation = this.orderLocationService.findById(fuelEvent.getBillPriceSourceId());
			BigDecimal cost = this.calculatorSrv.calculate(orderLocation, fuelEvent).getCost();
			return exchBd.convert(orderLocation.getPaymentCurrency(), sysCurrency, date, cost, false).getValue().setScale(6, RoundingMode.HALF_UP);
		} else if (fuelEvent.getBillPriceSourceType().equalsIgnoreCase(Contract.class.getSimpleName())) {
			Contract contract = this.contractSrv.findById(fuelEvent.getBillPriceSourceId());
			BigDecimal cost = this.calculatorSrv.calculate(contract, fuelEvent).getCost();
			return exchBd.convert(contract.getSettlementCurr(), sysCurrency, date, cost, false).getValue().setScale(6, RoundingMode.HALF_UP);
		} else {
			return BigDecimal.ZERO;
		}
	}

	/**
	 * Get cost in settlement currency(orderLocation or contract) from fuel event bill source and set billable cost currency on fuel event.
	 *
	 * @param fuelEvent
	 * @return
	 * @throws BusinessException
	 */
	private BigDecimal getCostInSettMeasurment(FuelEvent fuelEvent) throws BusinessException {
		String accrualDateParameter = this.sysParamSrv.getAccrualEvaluationDate();
		Date date = GregorianCalendar.getInstance().getTime();
		ExchangeRate_Bd exchBd = this.getBusinessDelegate(ExchangeRate_Bd.class);
		if (accrualDateParameter.equalsIgnoreCase(AccrualDateParameter.EVENT_DATE.getValue())) {
			date = fuelEvent.getFuelingDate();
		}
		if (fuelEvent.getBillPriceSourceType().equalsIgnoreCase(FuelOrderLocation.class.getSimpleName())) {
			FuelOrderLocation orderLocation = this.orderLocationService.findById(fuelEvent.getBillPriceSourceId());
			fuelEvent.setBillableCostCurrency(orderLocation.getPaymentCurrency());
			BigDecimal cost = this.calculatorSrv.calculate(orderLocation, fuelEvent).getCost();
			return exchBd.convert(orderLocation.getPaymentCurrency(), fuelEvent.getBillableCostCurrency(), date, cost, false).getValue().setScale(6,
					RoundingMode.HALF_UP);
		} else if (fuelEvent.getBillPriceSourceType().equalsIgnoreCase(Contract.class.getSimpleName())) {
			Contract contract = this.contractSrv.findById(fuelEvent.getBillPriceSourceId());
			fuelEvent.setBillableCostCurrency(contract.getSettlementCurr());
			BigDecimal cost = this.calculatorSrv.calculate(contract, fuelEvent).getCost();
			return exchBd.convert(contract.getSettlementCurr(), fuelEvent.getBillableCostCurrency(), date, cost, false).getValue().setScale(6,
					RoundingMode.HALF_UP);
		} else {
			return BigDecimal.ZERO;
		}
	}

	private void setDeliveryNotesBillPrice(FuelEvent fuelEvent, DeliveryNote deliveryNote) {
		for (DeliveryNote dn : fuelEvent.getDeliveryNotes()) {
			dn.setBillableCost(fuelEvent.getBillableCost());
			dn.setBillableCostCurrency(fuelEvent.getBillableCostCurrency());
		}
		if (deliveryNote != null) {
			deliveryNote.setBillableCost(fuelEvent.getBillableCost());
			deliveryNote.setBillableCostCurrency(fuelEvent.getBillableCostCurrency());
		}
	}

	/**
	 * Generate fuel event details.
	 *
	 * @param fuelEvent
	 * @throws BusinessException
	 */
	public void generateDetails(FuelEvent fuelEvent) throws BusinessException {
		if (!(fuelEvent.getBillPriceSourceType() == null
				|| fuelEvent.getBillPriceSourceType().equalsIgnoreCase(FuelOrderLocation.class.getSimpleName())
				|| fuelEvent.getBillPriceSourceType().equalsIgnoreCase(FuelOrder.class.getSimpleName()))) {
			Contract salesContract = this.contractSrv.findById(fuelEvent.getBillPriceSourceId());
			this.generateDetailsFromContract(fuelEvent, salesContract);
		}
		DeliveryNote deliveryNote = null;
		for (DeliveryNote dn : fuelEvent.getDeliveryNotes()) {
			if (dn.getUsed()) {
				deliveryNote = dn;
				break;
			}
		}
		if (deliveryNote != null && !CollectionUtils.isEmpty(deliveryNote.getDeliveryNoteContracts())) {
			for (DeliveryNoteContract dnContract : deliveryNote.getDeliveryNoteContracts()) {
				Contract contract = dnContract.getContracts();
				this.generateDetailsFromContract(fuelEvent, contract);
			}
		}
		this.removeOldDetails(fuelEvent);

	}

	private void removeOldDetails(FuelEvent fuelEvent) {
		if (fuelEvent.getDetails() != null) {
			Iterator<FuelEventsDetails> iter = fuelEvent.getDetails().iterator();
			while (iter.hasNext()) {
				FuelEventsDetails d = iter.next();
				if (d.getUpdated() == null || !d.getUpdated()) {
					iter.remove();
				}
			}
		}
	}

	private void generateDetailsFromContract(FuelEvent fuelEvent, Contract contract) throws BusinessException {
		GeneratorBd<?> bd = this.getBusinessDelegate(GeneratorBd.class);
		for (ContractPriceCategory cpCategory : contract.getPriceCategories()) {
			Date fuelEventEndDate = DateUtils.removeTime(fuelEvent.getFuelingDate());

			if (!PriceType._PRODUCT_.equals(cpCategory.getPriceCategory().getType())
					&& !this.cpcSrv.hasValidInterval(fuelEvent.getFuelingDate(), cpCategory)) {
				continue;
			}

			if (fuelEvent.getFuelingDate().compareTo(cpCategory.getPricingBases().getValidFrom()) >= 0
					&& fuelEventEndDate.compareTo(cpCategory.getPricingBases().getValidTo()) <= 0) {

				this.buildFuelEventDetails(fuelEvent, contract, bd, cpCategory);
			}
		}
	}

	/**
	 * Create and add details to fuel event.
	 *
	 * @param fuelEvent
	 * @param contract
	 * @param bd
	 * @param cpCategory
	 * @throws BusinessException
	 */
	private void buildFuelEventDetails(FuelEvent fuelEvent, Contract contract, GeneratorBd<?> bd, ContractPriceCategory cpCategory)
			throws BusinessException {
		List<ContractPriceComponentConv> compList = bd.getPriceComponentPerCategory(fuelEvent, cpCategory);
		ContractPriceComponent comp = bd.getPriceFromPriceComponents(compList, cpCategory, fuelEvent.getFuelingDate());
		if (!bd.isNotUsed(fuelEvent, cpCategory) && comp != null) {
			FuelEventsDetails details = new FuelEventsDetails();
			details.setContractCode(contract.getCode());
			details.setPriceCategory(cpCategory.getPriceCategory());
			details.setFuelEvent(fuelEvent);
			details = this.getExistingDetails(fuelEvent, details);
			details.setUpdated(true);
			details.setTransaction(contract.getDealType());
			details.setMainCategory(cpCategory.getPriceCategory().getMainCategory());
			details.setCalculateIndicator(cpCategory.getCalculateIndicator());
			details.setOriginalPrice(comp.getPrice());
			details.setOriginalPriceCurrency(comp.getCurrency());
			details.setOriginalPriceUnit(comp.getUnit());
			details.setPriceName(cpCategory.getName());
			BigDecimal settPrice = this.calculatorSrv.getSettlementPricePerCategory(fuelEvent, cpCategory);
			details.setSettlementPrice(settPrice);
			details.setExchangeRate(this.calculateExchRate(comp, fuelEvent.getFuelingDate()));
			CostVat costVat = this.calculatorSrv.calculate(cpCategory, fuelEvent);
			details.setSettlementAmount(costVat.getCost());
			details.setVatAmount(costVat.getVat());
			if (this.notExists(fuelEvent, details) && details.getSettlementAmount().compareTo(BigDecimal.ZERO) != 0) {
				fuelEvent.addToDetails(details);
			} else if (!this.notExists(fuelEvent, details) && details.getSettlementAmount().compareTo(BigDecimal.ZERO) == 0) {
				Iterator<FuelEventsDetails> iter = fuelEvent.getDetails().iterator();
				while (iter.hasNext()) {
					FuelEventsDetails d = iter.next();
					if (d.equals(details)) {
						iter.remove();
					}
				}
			}
		}
	}

	private boolean notExists(FuelEvent fuelEvent, FuelEventsDetails details) {
		return fuelEvent.getDetails() == null || !fuelEvent.getDetails().contains(details);
	}

	private BigDecimal calculateExchRate(ContractPriceComponent comp, Date date) {
		if (PriceInd._COMPOSITE_.equals(comp.getContrPriceCtgry().getPriceCategory().getPricePer())) {
			return BigDecimal.ONE;
		}
		Date fuelingDate = org.apache.commons.lang.time.DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
		if (comp.getConvertedPrices() != null) {
			for (ContractPriceComponentConv conv : comp.getConvertedPrices()) {
				if (conv.getValidFrom().compareTo(fuelingDate) <= 0 && conv.getValidTo().compareTo(fuelingDate) >= 0) {
					return conv.getExchangeRate();
				}
			}
		}
		LOG.warn("Cannot find a valid exchange rate!");
		return BigDecimal.ONE;
	}

	/**
	 * in case that fuel event has already a detail equal with <code>details</code> return the existing details.
	 *
	 * @param fuelEvent
	 * @param details
	 * @return
	 */
	private FuelEventsDetails getExistingDetails(FuelEvent fuelEvent, FuelEventsDetails details) {
		if (!CollectionUtils.isEmpty(fuelEvent.getDetails())) {
			for (FuelEventsDetails d : fuelEvent.getDetails()) {
				if (d.equals(details)) {
					return d;
				}
			}
		}
		return details;
	}

	private void addDensityAndTemperature(FuelEvent fe, FuelTicket ft) {
		fe.setDensity(ft.getDensity());
		fe.setDensityUnit(ft.getDensityUnit());
		fe.setDensityVolume(ft.getDensityVolume());
		fe.setTemperature(ft.getTemperature());
		fe.setTemperatureUnit(ft.getTemperatureUnit());
	}
}
