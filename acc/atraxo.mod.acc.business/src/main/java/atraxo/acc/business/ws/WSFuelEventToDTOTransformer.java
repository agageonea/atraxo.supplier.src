/**
 *
 */
package atraxo.acc.business.ws;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.profile.IAccountingRulesService;
import atraxo.fmbas.business.api.ws.WSModelToDTOTransformer;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.AccountingRulesType;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.fmbas.domain.ws.dto.WSPayloadDataDTO;
import atraxo.fmbas.domain.ws.dto.WSTransportDataType;
import atraxo.fmbas.domain.ws.fuelEvent.Any;
import atraxo.fmbas.domain.ws.fuelEvent.Any.FuelPlusFuelEvent;
import atraxo.fmbas.domain.ws.fuelEvent.Any.FuelPlusFuelEvent.FuelEvents;
import atraxo.fmbas.domain.ws.fuelEvent.Any.FuelPlusFuelEvent.FuelEvents.FuelEventLines;
import atraxo.fmbas.domain.ws.fuelEvent.Any.FuelPlusFuelEvent.FuelEvents.FuelEventLines.FuelEventLine;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import seava.j4e.business.utils.DateUtils;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author vhojda
 */
public class WSFuelEventToDTOTransformer implements WSModelToDTOTransformer<FuelEvent> {

	private static final Logger LOGGER = LoggerFactory.getLogger(WSFuelEventToDTOTransformer.class);

	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IContractService contractSrv;
	@Autowired
	IAccountingRulesService accountingRulesSrv;

	private ExportFuelEventOperationEnum fuelEventOperationType;

	/**
	 *
	 */
	public WSFuelEventToDTOTransformer() {
		// The explicit default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#transformModelToDTO(java.util.Collection, java.util.Map)
	 */
	@Override
	public WSPayloadDataDTO transformModelToDTO(Collection<FuelEvent> modelEntities, Map<String, String> paramMap) throws JAXBException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START transformModelToDTO()");
		}

		WSPayloadDataDTO returnedDto = new WSPayloadDataDTO();

		Any fuelEventAny = new Any();

		if (modelEntities.size() == 1) {
			FuelEvent event = modelEntities.iterator().next();
			FuelPlusFuelEvent fuelPlusEvent = new FuelPlusFuelEvent();

			boolean splitComposite = this.splitComposite(event, Boolean.valueOf(paramMap.get(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_PAYABLE)));
			FuelEvents fe = this.toFuelEvents(event, splitComposite);

			fuelPlusEvent.setFuelEvents(fe);
			fuelEventAny.setFuelPlusFuelEvent(fuelPlusEvent);

		} else {
			// TODO if the case
			LOGGER.warn("Could not transform multiple fuel events in payload ! Will do nothing !");
		}

		// set the payload
		returnedDto.setPayload(XMLUtils.toXML(fuelEventAny, fuelEventAny.getClass()));

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END transformModelToDTO()");
		}
		return returnedDto;
	}

	/**
	 * @param event
	 * @return
	 */
	private FuelEvents toFuelEvents(FuelEvent ev, boolean splitComposite) {
		FuelEvents fe = new FuelEvents();
		fe.setAircraftRegistrationNo(ev.getAircraft().getRegistration());
		fe.setContractHolder(this.getCustomerInfo(ev.getContractHolder(), ev));
		fe.setCustomerErpNo(ev.getCustomer().getAccountNumber());
		fe.setDeliveryTicketNumber(ev.getTicketNumber());
		fe.setDepartureLocation(this.determineDepartureLocation(ev));
		fe.setFlightID(ev.getFlightID());
		fe.setCustomerAccountNumber(ev.getCustomer().getCode());
		fe.setFuelEventID(new BigInteger(ev.getId() + ""));// TODO see if the schema can chage and send the REF ID and not the actual DB ID
		fe.setFuelEventType(this.fuelEventOperationType != null ? this.fuelEventOperationType.getFuelEventType() : "");
		fe.setFuelingDate(DateUtils.toXMLGregorianCalendar(ev.getFuelingDate()));
		fe.setFuelSupplier(this.getCustomerInfo(this.customerService.findByCode(ev.getFuelSupplier().getCode()), ev));
		fe.setGrossQuantity(ev.getQuantity() != null ? ev.getQuantity() : BigDecimal.ZERO);
		fe.setInvoiceNumber("");
		fe.setIPLAgent(this.customerService.findByCode(ev.getIplAgent().getCode()).getAccountNumber());
		fe.setIsInternationalFlight(ev.getEventType().equals(FlightTypeIndicator._INTERNATIONAL_));
		if (ev.getNetUpliftQuantity() != null && ev.getNetUpliftUnit() != null) {
			fe.setNetQuantity(ev.getNetUpliftQuantity());
			fe.setNetQuantityUOM(ev.getNetUpliftUnit().getCode());
		}
		fe.setPayableCost(ev.getPayableCost() != null ? ev.getPayableCost() : BigDecimal.ZERO);
		fe.setPayableCostCurrency(ev.getPayableCostCurrency() != null ? ev.getPayableCostCurrency().getCode() : "");
		// SONE-3871 set payment terms
		fe.setPaymentTerms(this.getFuelEventPaymentTerms(ev));

		fe.setPostingDate(DateUtils.toXMLGregorianCalendar(null));
		fe.setProduct(ev.getProduct().getIataName());
		fe.setQuantity(ev.getQuantity() != null ? ev.getQuantity() : BigDecimal.ZERO);
		fe.setReasonCode(ev.getRevocationReason() != null ? ev.getRevocationReason().getCode() : RevocationReason._EMPTY_.getCode());
		fe.setReceivableCost(ev.getBillableCost() != null ? ev.getBillableCost() : BigDecimal.ZERO);
		fe.setReceivableCostCurrency(ev.getBillableCostCurrency() != null ? ev.getBillableCostCurrency().getCode() : "");
		fe.setSettlementDate(DateUtils.toXMLGregorianCalendar(null));
		fe.setShipTo(ev.getShipTo().getAccountNumber());
		fe.setUOM(ev.getUnit().getCode());

		fe.setTemperature(BigDecimal.valueOf(ev.getTemperature() != null ? ev.getTemperature() : 15));
		fe.setDensity(ev.getDensity() != null ? ev.getDensity() : BigDecimal.valueOf(0.8));

		FuelEventLines lines = new FuelEventLines();
		BigDecimal vatTotal = BigDecimal.ZERO;
		for (FuelEventsDetails d : ev.getDetails()) {
			// SONE-5996
			if (splitComposite && d.getPriceCategory().getPricePer().equals(PriceInd._COMPOSITE_)) {
				continue;
			}
			if (!splitComposite && d.getCalculateIndicator().equals(CalculateIndicator._CALCULATE_ONLY_)) {
				continue;
			}

			FuelEventLine line = new FuelEventLine();
			line.setExchangeRate(d.getExchangeRate() != null ? d.getExchangeRate() : BigDecimal.ONE);
			line.setMainCategory(d.getMainCategory().getCode());
			line.setOriginalPrice(d.getOriginalPrice() != null ? d.getOriginalPrice() : BigDecimal.ZERO);
			line.setOriginalPriceCurrency(d.getOriginalPriceCurrency().getCode());
			line.setOriginalPriceUOM(d.getOriginalPriceUnit().getCode());
			line.setPriceCategory("Product".equalsIgnoreCase(d.getMainCategory().getCode()) ? d.getFuelEvent().getProduct().getIataName()
					: d.getPriceCategory().getIata().getCode());
			line.setPriceName(d.getPriceName());
			line.setSettlementAmount(d.getSettlementAmount() != null ? d.getSettlementAmount() : BigDecimal.ZERO);
			line.setSettlementPrice(d.getSettlementPrice() != null ? d.getSettlementPrice() : BigDecimal.ZERO);
			line.setSettlementPriceCurrency(d.getFuelEvent().getBillableCostCurrency() != null ? d.getFuelEvent().getBillableCostCurrency().getCode()
					: d.getFuelEvent().getPayableCostCurrency().getCode());
			line.setVatAmount(d.getVatAmount() != null ? d.getVatAmount() : BigDecimal.ZERO);
			lines.getFuelEventLine().add(line);
			vatTotal = vatTotal.add(line.getVatAmount());
		}

		fe.setVATAmountTotal(vatTotal);
		fe.setFuelEventLines(lines);
		return fe;
	}

	private String determineDepartureLocation(FuelEvent ev) {
		if (ev.getDeparture() != null) {
			String iataCode = ev.getDeparture().getIataCode();
			String icaoCode = ev.getDeparture().getIcaoCode();

			if (!StringUtils.isEmpty(iataCode)) {
				return iataCode;
			} else if (!StringUtils.isEmpty(icaoCode)) {
				return icaoCode;
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	/**
	 * @param fuelEvent
	 * @param isPayableCost
	 * @return
	 */
	private boolean splitComposite(FuelEvent fuelEvent, boolean isPayableCost) {
		try {
			Contract contract;
			if (isPayableCost) {
				contract = this.contractSrv.findById(fuelEvent.getBillPriceSourceId()).getResaleRef();
			} else {
				contract = this.contractSrv.findById(fuelEvent.getBillPriceSourceId());
			}

			this.accountingRulesSrv.findByAccountingRules(AccountingRulesType._EXPORT_DETAILED_COMPOSITE_PRICE_, contract.getHolder());

		} catch (Exception e) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(e.getMessage(), e);
			}
			return false;
		}
		return true;
	}

	private String getCustomerInfo(Customer customer, FuelEvent ev) {
		if (customer == null) {
			return "";
		}
		return ev.getIsResale() ? customer.getCode() : customer.getAccountNumber();
	}

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#getTransportDataType()
	 */
	@Override
	public WSTransportDataType getTransportDataType() {
		return WSTransportDataType.FUEL_EVENT;
	}

	public ExportFuelEventOperationEnum getFuelEventOperationType() {
		return this.fuelEventOperationType;
	}

	public void setFuelEventOperationType(ExportFuelEventOperationEnum fuelEventOperationType) {
		this.fuelEventOperationType = fuelEventOperationType;
	}

	private String getFuelEventPaymentTerms(FuelEvent ev) {
		Contract contract = null;
		if (ev.getPayableCost() != null) {
			// exporting Fuel Event for payable cost
			DeliveryNote deliveryNote = null;
			for (DeliveryNote dn : ev.getDeliveryNotes()) {
				if (dn.getUsed()) {
					deliveryNote = dn;
					break;
				}
			}
			if (deliveryNote != null && !CollectionUtils.isEmpty(deliveryNote.getDeliveryNoteContracts())) {
				for (DeliveryNoteContract dnContract : deliveryNote.getDeliveryNoteContracts()) {
					contract = dnContract.getContracts();
				}
			}
		} else {
			// exporting Fuel Event for billable cost

			if (!(ev.getBillPriceSourceType() == null || ev.getBillPriceSourceType().equalsIgnoreCase(FuelOrderLocation.class.getSimpleName())
					|| ev.getBillPriceSourceType().equalsIgnoreCase(FuelOrder.class.getSimpleName()))) {
				contract = this.contractSrv.findById(ev.getBillPriceSourceId());
			}
		}
		return contract != null ? contract.getPaymentTerms().toString() : "";
	}
}
