package atraxo.acc.business.ext.fuelEvents.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceRestriction;
import atraxo.fmbas.business.api.ac.IAcTypesService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.uom.Unit;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class PriceRestriction_Service extends AbstractBusinessBaseService {

	@Autowired
	private UnitConverterService unitConverterService;

	public boolean applyRestriction(ContractPriceCategory c, FuelEvent event) throws BusinessException {
		boolean apply = true;
		for (ContractPriceRestriction r : c.getPriceRestrictions()) {
			switch (r.getRestrictionType()) {
			case _VOLUME_:
				apply = apply && this.applyVolume(event, r);
				break;
			case _AIRCRAFT_TYPE_:
				apply = apply && this.applyAcType(event, r);
				break;
			case _FUELING_TYPE_:
				apply = apply && this.applyFuelingType(event, r);
				break;
			case _TRANSPORT_TYPE_:
				apply = apply && this.applyTransportType(event, r);
				break;
			case _FLIGHT_TYPE_:
				apply = apply && this.applyFlightType(event, r);
				break;
			case _REGISTRATION_NUMBER_:
				apply = apply && this.applyRegistrationNumber(event, r);
				break;
			// case _OPERATION_TYPE_:
			// apply = apply && this.applyAcOperationType(event, r);
			// break;
			case _TIME_OF_OPERATION_:
				apply = apply && this.applyTimeOfOperation(event, r);
				break;
			case _SHIP_TO_:
				apply = apply && this.applyToShipTo(event, r);
				break;
			default:
				// do nothing
				break;
			}
		}
		return apply;
	}

	private boolean applyToShipTo(FuelEvent event, ContractPriceRestriction r) throws BusinessException {
		boolean apply = false;
		ICustomerService srv = (ICustomerService) this.findEntityService(Customer.class);
		Customer customer;
		switch (r.getOperator()) {
		case _EQUAL_:
			customer = srv.findByCode(r.getValue());
			apply = customer.equals(event.getShipTo());
			break;
		case _NOT_EQUAL_:
			customer = srv.findByCode(r.getValue());
			apply = customer.equals(event.getShipTo());
			break;
		case _HIGHER_:
		case _HIGHER_EQ_:
		case _IN_:
		case _LOWER_:
		case _LOWER_EQ_:
		default:
			break;
		}
		return apply;
	}

	private boolean applyTransportType(FuelEvent event, ContractPriceRestriction r) {
		boolean apply = false;
		switch (r.getOperator()) {
		case _EQUAL_:
			apply = event.getTransport().getName().equals(r.getValue());
			break;
		case _NOT_EQUAL_:
			apply = event.getTransport().getName().equals(r.getValue());
			break;
		case _HIGHER_:
		case _HIGHER_EQ_:
		case _IN_:
		case _LOWER_:
		case _LOWER_EQ_:
		default:
			break;
		}
		return apply;
	}

	private boolean applyFuelingType(FuelEvent event, ContractPriceRestriction r) {
		boolean apply = false;
		switch (r.getOperator()) {
		case _EQUAL_:
			apply = event.getFuelingOperation().getName().equals(r.getValue());
			break;
		case _NOT_EQUAL_:
			apply = !event.getFuelingOperation().getName().equals(r.getValue());
			break;
		case _HIGHER_:
		case _HIGHER_EQ_:
		case _IN_:
		case _LOWER_:
		case _LOWER_EQ_:
		default:
			break;
		}
		return apply;
	}

	private boolean applyFlightType(FuelEvent event, ContractPriceRestriction r) {
		boolean apply = false;
		switch (r.getOperator()) {
		case _EQUAL_:
			apply = event.getEventType().getName().equals(r.getValue());
			break;
		case _HIGHER_:
		case _HIGHER_EQ_:
		case _IN_:
		case _LOWER_:
		case _LOWER_EQ_:
		default:
			break;
		}
		return apply;
	}

	private boolean applyAcOperationType(FuelEvent event, ContractPriceRestriction r) {
		boolean apply = false;
		switch (r.getOperator()) {
		case _EQUAL_:
			apply = event.getOperationType().getName().equals(r.getValue());
			break;
		case _HIGHER_:
		case _HIGHER_EQ_:
		case _IN_:
		case _LOWER_:
		case _LOWER_EQ_:
		default:
			break;
		}
		return apply;
	}

	private boolean applyRegistrationNumber(FuelEvent event, ContractPriceRestriction r) {
		boolean apply = false;
		switch (r.getOperator()) {
		case _EQUAL_:
			if (event.getAircraft().getRegistration().equals(r.getValue())) {
				apply = true;
			}
			break;
		case _NOT_EQUAL_:
			if (!event.getAircraft().getRegistration().equals(r.getValue())) {
				apply = true;
			}
			break;
		case _HIGHER_:
		case _HIGHER_EQ_:
		case _IN_:
		case _LOWER_:
		case _LOWER_EQ_:
		default:
			break;
		}
		return apply;
	}

	private boolean applyTimeOfOperation(FuelEvent event, ContractPriceRestriction r) {
		Calendar fuelingCal = GregorianCalendar.getInstance();
		Date fuelingDate = event.getFuelingDate();
		fuelingCal.setTime(fuelingDate);
		Calendar restrictionCal = GregorianCalendar.getInstance();
		restrictionCal.setTime(fuelingDate);

		String[] arr = r.getValue().split(":");
		switch (arr.length) {
		case 2:
			restrictionCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(arr[0]));
			restrictionCal.set(Calendar.MINUTE, Integer.parseInt(arr[1]));
			break;
		case 3:
			String[] arrayTemp = arr[0].split(" ");
			restrictionCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(arrayTemp[arrayTemp.length - 1]));
			restrictionCal.set(Calendar.MINUTE, Integer.parseInt(arr[1]));
			break;
		default:
			return false;
		}

		boolean apply = false;
		switch (r.getOperator()) {
		case _EQUAL_:
			apply = fuelingCal.equals(restrictionCal);
			break;
		case _HIGHER_:
			apply = fuelingCal.after(restrictionCal);
			break;
		case _HIGHER_EQ_:
			apply = fuelingCal.compareTo(restrictionCal) >= 0;
			break;
		case _IN_:
		case _LOWER_:
			apply = fuelingCal.before(restrictionCal);
			break;
		case _LOWER_EQ_:
			apply = fuelingCal.compareTo(restrictionCal) <= 0;
			break;
		case _NOT_EQUAL_:
			apply = !fuelingCal.equals(restrictionCal);
			break;
		default:
			break;
		}
		return apply;
	}

	private boolean applyVolume(FuelEvent event, ContractPriceRestriction r) throws BusinessException {
		// event.getSysQuantity()
		ISystemParameterService srv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		double sysDensity = Double.parseDouble(srv.getDensity());
		String sysUnitCode = srv.getSysVol();
		IUnitService unitSrv = (IUnitService) this.findEntityService(Unit.class);
		Unit sysUnit = unitSrv.findByCode(sysUnitCode);
		BigDecimal restSysQuantity = this.unitConverterService.convert(r.getUnit(), sysUnit, new BigDecimal(r.getValue()), sysDensity);
		boolean apply = false;
		switch (r.getOperator()) {
		case _EQUAL_:
			apply = event.getSysQuantity().compareTo(restSysQuantity) == 0;
			break;
		case _HIGHER_:
			apply = event.getSysQuantity().compareTo(restSysQuantity) > 0;
			break;
		case _HIGHER_EQ_:
			apply = event.getSysQuantity().compareTo(restSysQuantity) >= 0;
			break;
		case _IN_:
			break;
		case _LOWER_:
			apply = event.getSysQuantity().compareTo(restSysQuantity) < 0;
			break;
		case _LOWER_EQ_:
			apply = event.getSysQuantity().compareTo(restSysQuantity) <= 0;
			break;
		case _NOT_EQUAL_:
			apply = event.getSysQuantity().compareTo(restSysQuantity) != 0;
			break;
		default:
			break;
		}
		return apply;
	}

	private boolean applyAcType(FuelEvent event, ContractPriceRestriction r) throws BusinessException {
		boolean apply = false;
		IAcTypesService srv = (IAcTypesService) this.findEntityService(AcTypes.class);
		AcTypes acType;
		switch (r.getOperator()) {
		case _EQUAL_:
			acType = srv.findByCode(r.getValue());
			apply = event.getAircraft().getAcTypes().equals(acType);
			break;
		case _NOT_EQUAL_:
			acType = srv.findByCode(r.getValue());
			apply = !event.getAircraft().getAcTypes().equals(acType);
			break;
		case _HIGHER_:
		case _HIGHER_EQ_:
		case _IN_:
		case _LOWER_:
		case _LOWER_EQ_:
		default:
			break;
		}
		return apply;
	}
}
