package atraxo.mod.acc.business.ext.mailMerge.delegate.data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;

/**
 * Simple POJO class with the role of having data to be tested by the Dto Transformer
 *
 * @author vhojda
 */
public class TestData extends AbstractEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 5156817653908657272L;

	private String aString;

	private Float aFloat;

	private Double aDouble;

	private BigDecimal aBigDecimal;

	private Boolean aBoolean;

	private Long aLong;

	private BigInteger aBigInteger;

	private Integer anInteger;

	private TestChildData childData;

	private Collection<TestChildData> collectionChildData;

	public TestData() {
		this.setCollectionChildData(new ArrayList<>());
	}

	public String getaString() {
		return this.aString;
	}

	public void setaString(String aString) {
		this.aString = aString;
	}

	public Float getaFloat() {
		return this.aFloat;
	}

	public void setaFloat(Float aFloat) {
		this.aFloat = aFloat;
	}

	public Double getaDouble() {
		return this.aDouble;
	}

	public void setaDouble(Double aDouble) {
		this.aDouble = aDouble;
	}

	public BigDecimal getaBigDecimal() {
		return this.aBigDecimal;
	}

	public void setaBigDecimal(BigDecimal aBigDecimal) {
		this.aBigDecimal = aBigDecimal;
	}

	public Boolean getaBoolean() {
		return this.aBoolean;
	}

	public void setaBoolean(Boolean aBoolean) {
		this.aBoolean = aBoolean;
	}

	public Long getaLong() {
		return this.aLong;
	}

	public void setaLong(Long aLong) {
		this.aLong = aLong;
	}

	public BigInteger getaBigInteger() {
		return this.aBigInteger;
	}

	public void setaBigInteger(BigInteger aBigInteger) {
		this.aBigInteger = aBigInteger;
	}

	public Integer getAnInteger() {
		return this.anInteger;
	}

	public void setAnInteger(Integer anInteger) {
		this.anInteger = anInteger;
	}

	public TestChildData getChildData() {
		return this.childData;
	}

	public void setChildData(TestChildData childData) {
		this.childData = childData;
	}

	public Collection<TestChildData> getCollectionChildData() {
		return this.collectionChildData;
	}

	public void setCollectionChildData(Collection<TestChildData> collectionChildData) {
		this.collectionChildData = collectionChildData;
	}

	@Override
	public String toString() {
		return "TestData [aString=" + this.aString + ", aFloat=" + this.aFloat + ", aDouble=" + this.aDouble + ", aBigDecimal=" + this.aBigDecimal
				+ ", aBoolean=" + this.aBoolean + ", aLong=" + this.aLong + ", aBigInteger=" + this.aBigInteger + ", anInteger=" + this.anInteger
				+ ", childData=" + this.childData + ", collectionChildData=" + this.collectionChildData + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.aBigDecimal == null) ? 0 : this.aBigDecimal.hashCode());
		result = prime * result + ((this.aBigInteger == null) ? 0 : this.aBigInteger.hashCode());
		result = prime * result + ((this.aBoolean == null) ? 0 : this.aBoolean.hashCode());
		result = prime * result + ((this.aDouble == null) ? 0 : this.aDouble.hashCode());
		result = prime * result + ((this.aFloat == null) ? 0 : this.aFloat.hashCode());
		result = prime * result + ((this.aLong == null) ? 0 : this.aLong.hashCode());
		result = prime * result + ((this.aString == null) ? 0 : this.aString.hashCode());
		result = prime * result + ((this.anInteger == null) ? 0 : this.anInteger.hashCode());
		result = prime * result + ((this.childData == null) ? 0 : this.childData.hashCode());
		result = prime * result + ((this.collectionChildData == null) ? 0 : this.collectionChildData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		TestData other = (TestData) obj;
		if (this.aBigDecimal == null) {
			if (other.aBigDecimal != null) {
				return false;
			}
		} else if (!this.aBigDecimal.equals(other.aBigDecimal)) {
			return false;
		}
		if (this.aBigInteger == null) {
			if (other.aBigInteger != null) {
				return false;
			}
		} else if (!this.aBigInteger.equals(other.aBigInteger)) {
			return false;
		}
		if (this.aBoolean == null) {
			if (other.aBoolean != null) {
				return false;
			}
		} else if (!this.aBoolean.equals(other.aBoolean)) {
			return false;
		}
		if (this.aDouble == null) {
			if (other.aDouble != null) {
				return false;
			}
		} else if (!this.aDouble.equals(other.aDouble)) {
			return false;
		}
		if (this.aFloat == null) {
			if (other.aFloat != null) {
				return false;
			}
		} else if (!this.aFloat.equals(other.aFloat)) {
			return false;
		}
		if (this.aLong == null) {
			if (other.aLong != null) {
				return false;
			}
		} else if (!this.aLong.equals(other.aLong)) {
			return false;
		}
		if (this.aString == null) {
			if (other.aString != null) {
				return false;
			}
		} else if (!this.aString.equals(other.aString)) {
			return false;
		}
		if (this.anInteger == null) {
			if (other.anInteger != null) {
				return false;
			}
		} else if (!this.anInteger.equals(other.anInteger)) {
			return false;
		}
		if (this.childData == null) {
			if (other.childData != null) {
				return false;
			}
		} else if (!this.childData.equals(other.childData)) {
			return false;
		}
		if (this.collectionChildData == null) {
			if (other.collectionChildData != null) {
				return false;
			}
		} else if (!this.collectionChildData.equals(other.collectionChildData)) {
			return false;
		}
		return true;
	}

}
