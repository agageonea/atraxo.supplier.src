package atraxo.mod.acc.business.ext.mailMerge.delegate.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;
import atraxo.mod.acc.business.ext.mailMerge.delegate.data.TestData;
import seava.j4e.api.annotation.Dto;
import seava.j4e.api.annotation.DtoField;

@Dto(entity = TestData.class)
public class TestDto extends AbstractDataDto {

	/**
	 *
	 */
	private static final long serialVersionUID = 4228545677136493417L;

	@DtoField
	private String aString;

	@DtoField(path = "aString")
	private String anotherString;

	@DtoField
	private Float aFloat;

	@DtoField
	private Double aDouble;

	@DtoField
	private BigDecimal aBigDecimal;

	@DtoField
	private Boolean aBoolean;

	@DtoField
	private Long aLong;

	@DtoField
	private BigInteger aBigInteger;

	@DtoField
	private Integer anInteger;

	@DtoField(rootElement = false, path = "childData")
	private TestChildDto testChildDto;

	@DtoField(rootElement = true)
	private TestChildDto anotherTestChildDto;

	@DtoField(path = "childData.aString")
	private String testChildDataString;

	@DtoField
	private Collection<TestChildDto> collectionChildData;

	public TestDto() {

	}

	public String getaString() {
		return this.aString;
	}

	public void setaString(String aString) {
		this.aString = aString;
	}

	public String getAnotherString() {
		return this.anotherString;
	}

	public void setAnotherString(String anotherString) {
		this.anotherString = anotherString;
	}

	public Float getaFloat() {
		return this.aFloat;
	}

	public void setaFloat(Float aFloat) {
		this.aFloat = aFloat;
	}

	public Double getaDouble() {
		return this.aDouble;
	}

	public void setaDouble(Double aDouble) {
		this.aDouble = aDouble;
	}

	public BigDecimal getaBigDecimal() {
		return this.aBigDecimal;
	}

	public void setaBigDecimal(BigDecimal aBigDecimal) {
		this.aBigDecimal = aBigDecimal;
	}

	public Boolean getaBoolean() {
		return this.aBoolean;
	}

	public void setaBoolean(Boolean aBoolean) {
		this.aBoolean = aBoolean;
	}

	public Long getaLong() {
		return this.aLong;
	}

	public void setaLong(Long aLong) {
		this.aLong = aLong;
	}

	public BigInteger getaBigInteger() {
		return this.aBigInteger;
	}

	public void setaBigInteger(BigInteger aBigInteger) {
		this.aBigInteger = aBigInteger;
	}

	public Integer getAnInteger() {
		return this.anInteger;
	}

	public void setAnInteger(Integer anInteger) {
		this.anInteger = anInteger;
	}

	public TestChildDto getTestChildDto() {
		return this.testChildDto;
	}

	public void setTestChildDto(TestChildDto testChildDto) {
		this.testChildDto = testChildDto;
	}

	public TestChildDto getAnotherTestChildDto() {
		return this.anotherTestChildDto;
	}

	public void setAnotherTestChildDto(TestChildDto anotherTestChildDto) {
		this.anotherTestChildDto = anotherTestChildDto;
	}

	public String getTestChildDataString() {
		return this.testChildDataString;
	}

	public void setTestChildDataString(String testChildDataString) {
		this.testChildDataString = testChildDataString;
	}

	@Override
	public void setDefaultValues() {

	}

	public Collection<TestChildDto> getCollectionChildData() {
		return this.collectionChildData;
	}

	public void setCollectionChildData(Collection<TestChildDto> collectionChildData) {
		this.collectionChildData = collectionChildData;
	}

	@Override
	public String toString() {
		return "TestDto [aString=" + this.aString + ", anotherString=" + this.anotherString + ", aFloat=" + this.aFloat + ", aDouble=" + this.aDouble
				+ ", aBigDecimal=" + this.aBigDecimal + ", aBoolean=" + this.aBoolean + ", aLong=" + this.aLong + ", aBigInteger=" + this.aBigInteger
				+ ", anInteger=" + this.anInteger + ", testChildDto=" + this.testChildDto + ", anotherTestChildDto=" + this.anotherTestChildDto
				+ ", testChildDataString=" + this.testChildDataString + ", collectionChildData=" + this.collectionChildData + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.aBigDecimal == null) ? 0 : this.aBigDecimal.hashCode());
		result = prime * result + ((this.aBigInteger == null) ? 0 : this.aBigInteger.hashCode());
		result = prime * result + ((this.aBoolean == null) ? 0 : this.aBoolean.hashCode());
		result = prime * result + ((this.aDouble == null) ? 0 : this.aDouble.hashCode());
		result = prime * result + ((this.aFloat == null) ? 0 : this.aFloat.hashCode());
		result = prime * result + ((this.aLong == null) ? 0 : this.aLong.hashCode());
		result = prime * result + ((this.aString == null) ? 0 : this.aString.hashCode());
		result = prime * result + ((this.anInteger == null) ? 0 : this.anInteger.hashCode());
		result = prime * result + ((this.anotherString == null) ? 0 : this.anotherString.hashCode());
		result = prime * result + ((this.anotherTestChildDto == null) ? 0 : this.anotherTestChildDto.hashCode());
		result = prime * result + ((this.collectionChildData == null) ? 0 : this.collectionChildData.hashCode());
		result = prime * result + ((this.testChildDataString == null) ? 0 : this.testChildDataString.hashCode());
		result = prime * result + ((this.testChildDto == null) ? 0 : this.testChildDto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		TestDto other = (TestDto) obj;
		if (this.aBigDecimal == null) {
			if (other.aBigDecimal != null) {
				return false;
			}
		} else if (!this.aBigDecimal.equals(other.aBigDecimal)) {
			return false;
		}
		if (this.aBigInteger == null) {
			if (other.aBigInteger != null) {
				return false;
			}
		} else if (!this.aBigInteger.equals(other.aBigInteger)) {
			return false;
		}
		if (this.aBoolean == null) {
			if (other.aBoolean != null) {
				return false;
			}
		} else if (!this.aBoolean.equals(other.aBoolean)) {
			return false;
		}
		if (this.aDouble == null) {
			if (other.aDouble != null) {
				return false;
			}
		} else if (!this.aDouble.equals(other.aDouble)) {
			return false;
		}
		if (this.aFloat == null) {
			if (other.aFloat != null) {
				return false;
			}
		} else if (!this.aFloat.equals(other.aFloat)) {
			return false;
		}
		if (this.aLong == null) {
			if (other.aLong != null) {
				return false;
			}
		} else if (!this.aLong.equals(other.aLong)) {
			return false;
		}
		if (this.aString == null) {
			if (other.aString != null) {
				return false;
			}
		} else if (!this.aString.equals(other.aString)) {
			return false;
		}
		if (this.anInteger == null) {
			if (other.anInteger != null) {
				return false;
			}
		} else if (!this.anInteger.equals(other.anInteger)) {
			return false;
		}
		if (this.anotherString == null) {
			if (other.anotherString != null) {
				return false;
			}
		} else if (!this.anotherString.equals(other.anotherString)) {
			return false;
		}
		if (this.anotherTestChildDto == null) {
			if (other.anotherTestChildDto != null) {
				return false;
			}
		} else if (!this.anotherTestChildDto.equals(other.anotherTestChildDto)) {
			return false;
		}
		if (this.collectionChildData == null) {
			if (other.collectionChildData != null) {
				return false;
			}
		} else if (!this.collectionChildData.equals(other.collectionChildData)) {
			return false;
		}
		if (this.testChildDataString == null) {
			if (other.testChildDataString != null) {
				return false;
			}
		} else if (!this.testChildDataString.equals(other.testChildDataString)) {
			return false;
		}
		if (this.testChildDto == null) {
			if (other.testChildDto != null) {
				return false;
			}
		} else if (!this.testChildDto.equals(other.testChildDto)) {
			return false;
		}
		return true;
	}

}
