package atraxo.mod.acc.business.ext.mailMerge.delegate;

import java.util.Collection;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import atraxo.acc.business.ext.RandomObjectFiller;
import atraxo.fmbas.business.ext.dto.DataTransferObjectManager;
import atraxo.mod.acc.business.ext.mailMerge.delegate.data.TestChildData;
import atraxo.mod.acc.business.ext.mailMerge.delegate.data.TestData;
import atraxo.mod.acc.business.ext.mailMerge.delegate.dto.TestChildDto;
import atraxo.mod.acc.business.ext.mailMerge.delegate.dto.TestDto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/spring/jee/acc-business-test-context.xml" })
public class DataTransferObjectManager_Test extends AbstractTransactionalJUnit4SpringContextTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataTransferObjectManager_Test.class);

	@Autowired
	private RandomObjectFiller randomObjectFiller;

	@Autowired
	private DataTransferObjectManager dataTransferObjectManager;

	private TestData data;

	private TestDto dto;

	@Before
	public void setUp() throws Exception {
		this.data = this.randomObjectFiller.createAndFill(TestData.class);
		for (byte i = 0; i < 5; i++) {
			this.data.getCollectionChildData().add(this.randomObjectFiller.createAndFill(TestChildData.class));
		}

		this.dto = this.dataTransferObjectManager.objectToDto(this.data, TestDto.class);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPath() {
		Assert.assertEquals(this.data.getaString(), this.dto.getAnotherString());
	}

	@Test
	public void testCollection() {
		Assert.assertEquals(this.data.getCollectionChildData().size(), this.dto.getCollectionChildData().size());

		for (TestChildDto childDataDto : this.dto.getCollectionChildData()) {
			TestChildData childData = this.getTestChildDataByAString(childDataDto.getaString(), this.data.getCollectionChildData());
			Assert.assertNotNull(childData);
		}
	}

	@Test
	public void testNestedEntity() {
		Assert.assertEquals(this.data.getChildData().getaString(), this.dto.getTestChildDto().getaString());
	}

	@Test
	public void testNestedProperty() {
		Assert.assertEquals(this.data.getChildData().getaString(), this.dto.getTestChildDataString());
	}

	@Test
	public void testRootPropertyField() {
		Assert.assertEquals(this.data.getaString(), this.dto.getAnotherTestChildDto().getaString());
	}

	@Test
	public void testInteger() {
		Assert.assertEquals(this.data.getAnInteger(), this.dto.getAnInteger());
	}

	@Test
	public void testString() {
		Assert.assertEquals(this.data.getaString(), this.dto.getaString());
	}

	@Test
	public void testBigInteger() {
		Assert.assertEquals(this.data.getaBigInteger(), this.dto.getaBigInteger());
	}

	@Test
	public void testBigDecimal() {
		Assert.assertEquals(this.data.getaBigDecimal(), this.dto.getaBigDecimal());
	}

	@Test
	public void testLong() {
		Assert.assertEquals(this.data.getaLong(), this.dto.getaLong());
	}

	@Test
	public void testFloat() {
		Assert.assertEquals(this.data.getaFloat(), this.dto.getaFloat());
	}

	@Test
	public void testDouble() {
		Assert.assertEquals(this.data.getaDouble(), this.dto.getaDouble());
	}

	@Test
	public void testBoolean() {
		Assert.assertEquals(this.data.getaBoolean(), this.dto.getaBoolean());
	}

	private TestChildData getTestChildDataByAString(String aString, Collection<TestChildData> collection) {
		TestChildData child = null;
		for (TestChildData d : collection) {
			if (d.getaString().equals(aString)) {
				child = d;
				break;
			}
		}
		return child;
	}

}
