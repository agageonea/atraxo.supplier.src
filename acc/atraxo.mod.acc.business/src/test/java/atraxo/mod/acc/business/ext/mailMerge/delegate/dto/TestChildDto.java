package atraxo.mod.acc.business.ext.mailMerge.delegate.dto;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;
import atraxo.mod.acc.business.ext.mailMerge.delegate.data.TestChildData;
import seava.j4e.api.annotation.Dto;
import seava.j4e.api.annotation.DtoField;

@Dto(entity = TestChildData.class)
public class TestChildDto extends AbstractDataDto {

	/**
	 *
	 */
	private static final long serialVersionUID = 1307276629742979337L;

	@DtoField
	private String aString;

	public TestChildDto() {

	}

	public String getaString() {
		return this.aString;
	}

	public void setaString(String aString) {
		this.aString = aString;
	}

	@Override
	public void setDefaultValues() {

	}

	@Override
	public String toString() {
		return "TestChildDto [aString=" + this.aString + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.aString == null) ? 0 : this.aString.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		TestChildDto other = (TestChildDto) obj;
		if (this.aString == null) {
			if (other.aString != null) {
				return false;
			}
		} else if (!this.aString.equals(other.aString)) {
			return false;
		}
		return true;
	}

}
