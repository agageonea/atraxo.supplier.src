package atraxo.mod.acc.business.ext.mailMerge.delegate.data;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;

/**
 * Simple POJO class with the role of having data to be tested by the Dto Transformer
 *
 * @author vhojda
 */
public class TestChildData extends AbstractEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -872868409604214244L;

	private String aString;

	private String aChildString;

	public TestChildData() {

	}

	public String getaChildString() {
		return this.aChildString;
	}

	public void setaChildString(String aChildString) {
		this.aChildString = aChildString;
	}

	public String getaString() {
		return this.aString;
	}

	public void setaString(String aString) {
		this.aString = aString;
	}

	@Override
	public String toString() {
		return "TestChildData [aString=" + this.aString + ", aChildString=" + this.aChildString + "]";
	}

}
