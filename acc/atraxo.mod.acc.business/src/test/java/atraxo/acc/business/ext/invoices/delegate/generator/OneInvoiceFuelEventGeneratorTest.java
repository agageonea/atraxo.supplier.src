package atraxo.acc.business.ext.invoices.delegate.generator;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Test;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;

public class OneInvoiceFuelEventGeneratorTest {

	private final static int NR_EVENTS = 5000;
	List<FuelEvent> fuelEvents;
	OneInvoiceFuelEventGenerator generator;

	@Before
	public void setup() {

		this.generator = (OneInvoiceFuelEventGenerator) FrequencyFuelEventGeneratorFactory.getFactory()
				.getFrequencyFuelEventGenerator(InvoiceFreq._EVERY_);
		this.fuelEvents = createRandomDateFuelEvents();
		
		softFuelEventsByDate();
		
	}

	@Test
	public void allEventsInOneSingleMap() {

		Map<InvoicingDatePeriod, List<FuelEvent>> map = generator.splitByFrequency(fuelEvents);
		assertTrue(!map.isEmpty() && map.size() == 1);

	}
	
	private void softFuelEventsByDate() {
		Collections.sort(this.fuelEvents, new Comparator<FuelEvent>() {
			@Override
			public int compare(FuelEvent o1, FuelEvent o2) {
				return o1.getFuelingDate().compareTo(o2.getFuelingDate());
			}
		});
	}

	private List<FuelEvent> createRandomDateFuelEvents() {
		fuelEvents = new ArrayList<>();

		for (int i = 0; i < NR_EVENTS; i++) {
			FuelEvent ev = new FuelEvent();
			ev.setFuelingDate(new Date(Math.abs(System.currentTimeMillis() - RandomUtils.nextLong())));
			this.fuelEvents.add(ev);
		}

		return fuelEvents;
	}

}
