package atraxo.acc.business.ext;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class RandomObjectFiller {

	private Random random = new Random();

	public <T> T createAndFill(Class<T> clazz) throws Exception {
		T instance = clazz.newInstance();
		for (Field field : clazz.getDeclaredFields()) {
			// avoid infinite loops
			if (field.getType().getCanonicalName().equals(clazz.getCanonicalName())) {
				break;
			}
			if (!java.lang.reflect.Modifier.isStatic(field.getModifiers()) && !java.lang.reflect.Modifier.isProtected(field.getModifiers())) {
				field.setAccessible(true);
				Object value = this.getRandomValueForField(field);
				field.set(instance, value);
			}
		}
		return instance;
	}

	private Object getRandomValueForField(Field field) throws Exception {
		Class<?> type = field.getType();

		if (type.isEnum()) {
			Object[] enumValues = type.getEnumConstants();
			return enumValues[this.random.nextInt(enumValues.length)];
		} else if (type.equals(Integer.TYPE) || type.equals(Integer.class)) {
			return this.random.nextInt();
		} else if (type.equals(Long.TYPE) || type.equals(Long.class)) {
			return this.random.nextLong();
		} else if (type.equals(Double.TYPE) || type.equals(Double.class)) {
			return this.random.nextDouble();
		} else if (type.equals(Float.TYPE) || type.equals(Float.class)) {
			return this.random.nextFloat();
		} else if (type.equals(Date.class)) {
			return new Date();
		} else if (type.equals(String.class)) {
			return UUID.randomUUID().toString();
		} else if (type.equals(BigInteger.class)) {
			return BigInteger.valueOf(this.random.nextInt());
		} else if (type.equals(BigDecimal.class)) {
			return new BigDecimal(this.random.nextInt());
		} else if (type.equals(Boolean.class)) {
			return this.random.nextBoolean();
		} else if (type.equals(Collection.class)) {
			return new ArrayList<>();
		} else {
			Class cs = Class.forName(type.getCanonicalName());
			return this.createAndFill(cs);
		}
	}

}
