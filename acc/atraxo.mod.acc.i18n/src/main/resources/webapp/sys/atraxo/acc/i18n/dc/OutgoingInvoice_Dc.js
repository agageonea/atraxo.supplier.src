
Ext.define("atraxo.acc.i18n.dc.OutgoingInvoice_Dc$ApprovalNote", {
	remarks__lbl: ".Add your note to be sent with the request to approvers"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoice_Dc$Export", {
	exportTypeLabel__lbl: ".Document type",
	reportNameLabel__lbl: ".Template",
	reportName__lbl: ".Report name"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoice_Dc$Filter", {
	customerName__lbl: ".Receiver name",
	customer__lbl: ".Receiver",
	exportDate__lbl: ".Export date",
	exportStatus__lbl: ".Export status",
	issuerCode__lbl: ".Invoice issuer",
	issuerCode__tlp: ".Invoice issuer",
	product__lbl: ".Product Type"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoice_Dc$Generate", {
	closeDate__lbl: ".Closing date",
	deliveryTo__lbl: ".Deliveries up to",
	invoiceGenerationDate__lbl: ".Invoice date",
	separatePerShipTo__lbl: ".Separate invoice per ship-to"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoice_Dc$InvoiceHeader", {
	deliveryDateFrom__lbl: ".Delivery period from",
	deliveryLocCode__lbl: ".For location",
	deliverydateTo__lbl: ".period until",
	dueIn__lbl: ".DUE IN",
	invDate__lbl: ".Invoice date",
	invoiceForm__lbl: ".INVOICE FORM",
	invoiceNo__lbl: ".invoice #",
	invoiceRefNo__lbl: ".Reference invoice",
	issueDate__lbl: ".Issued on",
	issuer__lbl: ".Invoice issuer ",
	product__lbl: ".Product",
	quantity__lbl: ".Total quantity",
	receiver__lbl: ".Invoice receiver",
	refDocNo__lbl: ".Document #",
	refDoc__lbl: ".Reference document",
	status__lbl: ".STATUS",
	totalAmount__lbl: ".Total amount",
	transactionType__lbl: ".Transaction type"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoice_Dc$InvoiceList", {
	amount__lbl: ".Gross Amount",
	approvalStatus__lbl: ".Approval status",
	controlNo__lbl: ".Control Number",
	currency__lbl: ".Currency",
	deliveryFrom__lbl: ".Delivery from",
	deliveryTo__lbl: ".Delivery to",
	exportDate__lbl: ".Export Date",
	exportStatus__lbl: ".Export Status",
	invoiceNo__lbl: ".Invoice #",
	invoiceRefNo__lbl: ".Reference invoice",
	issuerCode__lbl: ".Invoice issuer ",
	items__lbl: ".Items",
	location__lbl: ".Location",
	product__lbl: ".Product",
	quantity__lbl: ".Quantity",
	status__lbl: ".Status",
	type__lbl: ".Type",
	unitCode__lbl: ".Unit",
	vatAmount__lbl: ".Tax"
});
