Ext.define("atraxo.acc.i18n.ds.PayableAccrual_Ds", {
	accrualAmountSet__lbl: ".Acrruals in settlement currency",
	accrualAmountSet__tlp: ".Acrruals in settlement currency",
	accrualAmountSys__lbl: ".Accruals in system currency",
	accrualAmountSys__tlp: ".Accruals in system currency",
	accrualQuantity__lbl: ".Accrued quantity in system volum unit",
	accrualQuantity__tlp: ".Accrued quantity in system volum unit",
	accrualVATSet__lbl: ".Accrual VAT amount in settlement currency",
	accrualVATSet__tlp: ".Accrual VAT amount in settlement currency",
	accrualVATSys__lbl: ".Accrual VAT amount in system currency",
	accrualVATSys__tlp: ".Accrual VAT amount in system currency",
	accruelCurrCode__lbl: ".Accrual currency",
	accruelCurrCode__tlp: ".Alphabetic code",
	accruelCurrId__lbl: ".Accrual currency(ID)",
	accruelCurrId__tlp: ".Currency of accrual",
	aicraftRegistration__lbl: ".Aircraft registration",
	aicraftRegistration__tlp: ".Registration",
	aircraftId__lbl: ".Aircraft registration number(ID)",
	aircraftId__tlp: ".Aircraft registration number",
	closingDate__lbl: ".Closing date",
	closingDate__tlp: ".Closing date of the accrual. In case the accural was automatically close then is the date of the last invoice. If the accural was closed manually then the day of manual closing.",
	contractScope__lbl: ".Contract scope",
	contractScope__tlp: ".The contract scope: into plane or non into plane",
	contractSubType__lbl: ".Contract subtype",
	contractSubType__tlp: ".The contract subtype",
	contractType__lbl: ".Contract type",
	contractType__tlp: ".Type of the contract: Product or Fueling Service",
	custCode__lbl: ".Customer",
	custCode__tlp: ".Account code",
	custId__lbl: ".Customer (ID)",
	custId__tlp: ".The company that benefits for the product or/and service delivered by the supplier",
	events__lbl: ".Events",
	exchangeRate__lbl: ".Exchange rate",
	exchangeRate__tlp: ".Exchange rate between settlement currency and system currency",
	expCurrCode__lbl: ".Expected currency",
	expCurrCode__tlp: ".Alphabetic code",
	expCurrId__lbl: ".Expected currency(ID)",
	expCurrId__tlp: ".Currency of expected cost. If contract exist then the contract setllement currency otherwise the system currency.",
	expectedAmountSet__lbl: ".Expected cost in settlement currency",
	expectedAmountSet__tlp: ".Expect cost calculated using contract settlement currency",
	expectedAmountSys__lbl: ".Expected cost in system currency",
	expectedAmountSys__tlp: ".Expect cost calculated using system currency and standard exchange rate for conversion",
	expectedQuantity__lbl: ".Expected quantity",
	expectedQuantity__tlp: ".Expected quantity in system volum unit",
	expectedVATSet__lbl: ".Expected VAT amount in settlement currency",
	expectedVATSet__tlp: ".Expected VAT amount in settlement currency",
	expectedVATSys__lbl: ".Expected VAT amount in system currency",
	expectedVATSys__tlp: ".Expected VAT amount in system currency",
	feFlightNumber__lbl: ".Flight number",
	feFlightNumber__tlp: ".Flight number",
	feFuelingDate__lbl: ".Fueling date",
	feFuelingDate__tlp: ".Fueling date",
	fuelEventId__lbl: ".Fuel Event identifier(ID)",
	fuelEventId__tlp: ".Fuel event identifier",
	fuelingDate__lbl: ".Fueling date",
	fuelingDate__tlp: ".Date of fueling operation",
	grossAmount__lbl: ".Gross Amount",
	invCurrCode__lbl: ".Invoiced currency",
	invCurrCode__tlp: ".Alphabetic code",
	invCurrId__lbl: ".Invoiced currency(ID)",
	invCurrId__tlp: ".Currency of the invoiced cost. If invoice extist the invoice currency otherwise the system currency.",
	invProcessStatus__lbl: ".Invoice statuc",
	invProcessStatus__tlp: ".This status indicates if any fuel deliveries (fuel uplift quantities) have already been invoiced for this fueling event",
	invoicedAmountSet__lbl: ".Invoiced cost calculated using contract settlement currency",
	invoicedAmountSet__tlp: ".Invoiced cost calculated using contract settlement currency",
	invoicedAmountSys__lbl: ".Invoiced cost calculated using system currency and standard exchange rate for conversion",
	invoicedAmountSys__tlp: ".Invoiced cost calculated using system currency and standard exchange rate for conversion",
	invoicedQuantity__lbl: ".Invoiced quantity in system volum unit",
	invoicedQuantity__tlp: ".Invoiced quantity in system volum unit",
	invoicedVATSet__lbl: ".Invoiced VAT amount in settlement currency",
	invoicedVATSet__tlp: ".Invoiced VAT amount in settlement currency",
	invoicedVATSys__lbl: ".Invoiced VAT amount in system currency",
	invoicedVATSys__tlp: ".Invoiced VAT amount in system currency",
	isOpen__lbl: ".Open",
	isOpen__tlp: ".Indicates if the accrual is stiil open. Used for speed up the search for open accruals.",
	locCode__lbl: ".Location",
	locCode__tlp: ".Code",
	locId__lbl: ".Fueling location(ID)",
	locId__tlp: ".The fueling location (departure location of the flight event)",
	month__lbl: ".Month",
	netAmount__lbl: ".Net Amount",
	suppCode__lbl: ".Supplier",
	suppCode__tlp: ".Code",
	suppId__lbl: ".Supplier(ID)",
	suppId__tlp: ".The company providing the service or product",
	vatAmount__lbl: ".VAT Amount"
});
