Ext.define("atraxo.acc.i18n.ds.OutgoingInvoiceLov_Ds", {
	invoiceId__lbl: ".Id",
	invoiceNo__lbl: ".Invoice number",
	invoiceNo__tlp: ".Invoice number",
	status__lbl: ".Status",
	status__tlp: ".Invoice status indicator"
});
