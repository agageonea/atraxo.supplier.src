
Ext.define("atraxo.acc.i18n.dc.OutgoingInvoiceBulkExportWizardInfiniteList_Dc$bulkInvoiceExportStep2", {
	delLocCode__lbl: ".Location",
	invoiceDate__lbl: ".Invoice Date",
	invoiceNo__lbl: ".Invoice #",
	issuerCode__lbl: ".Invoice Issuer",
	receiverCode__lbl: ".Invoice receiver"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoiceBulkExportWizardInfiniteList_Dc$bulkInvoiceExportStep2TopLabel", {
	step2Description__lbl: ".Select invoices to be included in the export. Invoice list filtered based on the criteria indicated previously."
});
