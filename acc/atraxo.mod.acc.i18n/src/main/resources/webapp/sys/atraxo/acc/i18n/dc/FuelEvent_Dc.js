
Ext.define("atraxo.acc.i18n.dc.FuelEvent_Dc$Filter", {
	accountNumber__lbl: ".Customer account number",
	aircraft__lbl: ".Aircraft registration",
	billStatus__lbl: ".Bill status",
	billableCost__lbl: ".Billable cost",
	billableCurrency__lbl: ".Currency",
	contractHolder__lbl: ".Contract Holder",
	customer__lbl: ".Customer",
	erpSaleInvoiceNo__lbl: ".External sales invoice #",
	flightNumber__lbl: ".Flight number",
	fuelingDate__lbl: ".Fueling date",
	fuelingQuantity__lbl: ".Fueling quantity",
	invoiceStatus__lbl: ".Invoice status",
	iplAgent__lbl: ".IPL agent",
	isResale__lbl: ".Is reasale",
	location__lbl: ".Location",
	payableCost__lbl: ".Payable cost",
	payableCurrency__lbl: ".Currency",
	product__lbl: ".Product Type",
	receivedStatus__lbl: ".Status",
	revocationReason__lbl: ".Revocation reason",
	shipToName__lbl: ".Ship-to name",
	shipTo__lbl: ".Ship To",
	source__lbl: ".Source",
	supplier__lbl: ".Fuel supplier",
	unit__lbl: ".Unit"
});

Ext.define("atraxo.acc.i18n.dc.FuelEvent_Dc$FilterForm", {
	aircraftReg__lbl: ".Aircraft registration #",
	dateEnd__lbl: ".Fueling date end",
	dateStart__lbl: ".Fueling date start",
	flight__lbl: ".Flight #",
	fuelTicket__lbl: ".Fuel ticket #",
	supplier__lbl: ".Supplier"
});

Ext.define("atraxo.acc.i18n.dc.FuelEvent_Dc$Form", {
	aircraft__lbl: ". Aircraft",
	billStatus__lbl: ".Bill status",
	billableCost__lbl: ".Receivable amount",
	fuelingDate__lbl: ".Fueling date",
	invoiceStaus__lbl: ".Invoice status",
	location__lbl: ".Location",
	payableCost__lbl: ".Payable amount"
});

Ext.define("atraxo.acc.i18n.dc.FuelEvent_Dc$List", {
	accountNumber__lbl: ".Customer account number",
	aircraftRegistration__lbl: ".Aircraft registration",
	billStatus__lbl: ".Bill status",
	billableCost__lbl: ".Billable cost",
	billableCurrency__lbl: ".Currency",
	contractHolderCode__lbl: ".Contract Holder",
	creditMemoNo__lbl: ".Credit memo#",
	customer__lbl: ".Customer",
	densityMassUnit__lbl: ".Density Mass Unit",
	densityVolumeUnit__lbl: ".Density Volume Unit",
	density__lbl: ".Density",
	erpSaleInvoiceNo__lbl: ".External sales invoice #",
	eventType__lbl: ".Flight type",
	flightID__lbl: ".Flight ID",
	flightNumber__lbl: ".Flight #",
	fuelingDate__lbl: ".Fueling date",
	fuelingOperation__lbl: ".Fueling type",
	invoiceStatus__lbl: ".Invoice status",
	iplAgent__lbl: ".IPL agent",
	location__lbl: ".Location",
	netUnit__lbl: ".NetUnit",
	netUpliftQuantity__lbl: ".NetQuantity",
	operationType__lbl: ".Operation type",
	payableCost__lbl: ".Payable cost",
	payableCurrency__lbl: ".Currency",
	product__lbl: ".Product",
	purchaseInvoiceNo__lbl: ".Purchase Invoice number",
	quantity__lbl: ".Fueling quantity",
	receivedStatus__lbl: ".Received status",
	salesInvoiceNo__lbl: ".Sales Invoice number",
	shipToName__lbl: ".Ship-to name",
	shipTo__lbl: ".Ship To",
	source__lbl: ".Source",
	status__lbl: ".Fuel Event Status",
	supplier__lbl: ".Fuel supplier",
	temperatureUnit__lbl: ".Temperature Unit",
	temperature__lbl: ".Temperature",
	ticketNumber__lbl: ".Ticket #",
	transport__lbl: ".Transport type",
	unit__lbl: ".Unit"
});

Ext.define("atraxo.acc.i18n.dc.FuelEvent_Dc$ListOutgoingInvoiceLine", {
	aircraftRegistration__lbl: ".Aircraft registration",
	billableCost__lbl: ".Amount",
	billableCurrency__lbl: ".Currency",
	customer__lbl: ".Customer",
	flightNumber__lbl: ".Flight #",
	fuelingDate__lbl: ".Fueling date",
	iplAgent__lbl: ".IPL agent",
	quantity__lbl: ".Quantity",
	source__lbl: ".Source",
	supplier__lbl: ".Fuel supplier",
	ticketNumber__lbl: ".Ticket #",
	unit__lbl: ".Unit"
});
