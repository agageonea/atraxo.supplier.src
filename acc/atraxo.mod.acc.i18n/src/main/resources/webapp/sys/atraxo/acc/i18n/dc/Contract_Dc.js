
Ext.define("atraxo.acc.i18n.dc.Contract_Dc$List", {
	contractCode__lbl: ".Contract #",
	eventType__lbl: ".Event type",
	scope__lbl: ".Scope",
	subtype__lbl: ".Delivery",
	supplier__lbl: ".Supplier",
	type__lbl: ".Type"
});
