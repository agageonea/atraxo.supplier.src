Ext.define("atraxo.acc.i18n.ds.OutgoingInvoice_Ds", {
	approvalNote__lbl: ".Approval Note",
	approvalStatus__lbl: ".Approval status",
	approvalStatus__tlp: ".Workflow approval status",
	approveRejectResult__lbl: ".Approve Reject Result",
	baseLineDate__lbl: ".Base line date",
	baseLineDate__tlp: ".Date for payment deadline calculated by the system based on the contract payment terms. Can be overwritten by the user.",
	canBeCompleted__lbl: "",
	category__lbl: ".Category",
	category__tlp: ".Indicates the invoice category such as Product into plane, Fueling Service, etc.",
	closeDate__lbl: ".Closing date",
	closeDate__tlp: ".Closing date for generated invoices. Due date will be calculated based on this and not by the payment terms reference indicated on the contract",
	closingDate__lbl: ".Closing Date",
	controlNo__lbl: ".Control number",
	controlNo__tlp: ".Control number",
	countryCode__lbl: ".Country",
	countryCode__tlp: ".Code",
	countryId__lbl: ".Country(ID)",
	creditMemoReason__lbl: ".Credit memo reason",
	creditMemoReason__tlp: ".Credit memo reason",
	currCode__lbl: ".Currency",
	currCode__tlp: ".Alphabetic code",
	currId__lbl: ".Currency(ID)",
	dateFrom__lbl: ".Date From",
	dateTo__lbl: ".Date To",
	delLocCode__lbl: ".For location or area",
	delLocCode__tlp: ".Code",
	delLocId__lbl: ".Delivery Loc(ID)",
	deliveryAreaIdParam__lbl: ".Delivery Area Id Param",
	deliveryDateFrom__lbl: ".Delivery date from",
	deliveryDateFrom__tlp: ".First date of the delivery period cover by the invoice",
	deliveryDateTo__lbl: ".Delivery date to",
	deliveryDateTo__tlp: ".Last date of the delivery period cover by the invoice",
	deliveryId__lbl: "",
	deliveryLocIdParam__lbl: ".Delivery Loc Id Param",
	deliveryType__lbl: "",
	dueDays__lbl: ".Due days",
	dummyField__lbl: "",
	exportDate__lbl: ".Export Date",
	exportDate__tlp: ".Date when the Invoice was exported",
	exportInvoiceDescription__lbl: ".Export Invoice Description",
	exportInvoiceOption__lbl: ".Export Invoice Option",
	exportInvoiceResult__lbl: ".Export Invoice Result",
	exportStatus__lbl: ".Export Status",
	exportStatus__tlp: ".Export Status",
	exportType__lbl: ".Export Type",
	filter__lbl: ".Filter",
	iataXmlFileName__lbl: ".Iata Xml File Name",
	invGeneratorErrorMsg__lbl: ".Inv Generator Error Msg",
	invoiceDate__lbl: ".Invoice date",
	invoiceDate__tlp: ".Invoice date",
	invoiceForm__lbl: ".Invoice form",
	invoiceForm__tlp: ".Indicates if the invoice is an electronic invoice or a paper invoice",
	invoiceGenerationDate__lbl: ".Invoice Generation Date",
	invoiceNo__lbl: ".Invoice number",
	invoiceNo__tlp: ".Invoice number",
	invoiceRefId__lbl: ".Invoice Reference(ID)",
	invoiceRefNo__lbl: ".Invoice reference number",
	invoiceRefNo__tlp: ".Invoice number",
	invoiceType__lbl: ".Invoice type",
	invoiceType__tlp: ".Indicates the invoice type: invoice, credit note, etc.",
	issueDate__lbl: ".Issue date",
	issueDate__tlp: ".Invoice issue date",
	issuerCode__lbl: ".Invoice issuer",
	issuerCode__tlp: ".Account code",
	issuerId__lbl: ".Issuer(ID)",
	itemsNumber__lbl: ".Items",
	itemsNumber__tlp: ".Number of invoice lines",
	labelField__lbl: "",
	marked__lbl: ".New Inv",
	nrGeneratedInvoices__lbl: ".Nr Generated Invoices",
	paymentDate__lbl: ".Payment date",
	paymentDate__tlp: ".Date when the invoice was payed",
	procent__lbl: ".%",
	product__lbl: ".Product",
	product__tlp: ".Fuel event product",
	quantity__lbl: ".Quantity",
	quantity__tlp: ".Invoiced quantity in the indicated unit",
	receiverCode__lbl: ".Customer",
	receiverCode__tlp: ".Account code",
	receiverIdParam__lbl: ".Receiver Id Param",
	receiverId__lbl: ".Receiver(ID)",
	receiverName__lbl: ".Receiver name",
	receiverName__tlp: ".Name",
	referenceDocId__lbl: ".Reference value",
	referenceDocId__tlp: ".Reference value for the defined type",
	referenceDocNo__lbl: ".Reference document number",
	referenceDocNo__tlp: ".Reference document number",
	referenceDocType__lbl: ".Reference document",
	referenceDocType__tlp: ".Reference document: sales contract or purchase order",
	reportCode__lbl: ".Report Code",
	reportName__lbl: ".Report Name",
	scope__lbl: "",
	selectedAllInvoices__lbl: ".Selected All Invoices",
	selectedAttachments__lbl: ".Selected Attachments",
	selectedInvoices__lbl: ".Selected Invoices",
	separatePerShipTo__lbl: ".Separate Per Ship To",
	standardFilter__lbl: ".Standard Filter",
	status__lbl: ".Status",
	status__tlp: ".Invoice status indicator",
	submitForApprovalDescription__lbl: ".Submit For Approval Description",
	submitForApprovalResult__lbl: ".Submit For Approval Result",
	taxType__lbl: ".Tax type",
	taxType__tlp: ".Tax type indicator used only for outgoing invoices",
	totalAmount__lbl: ".Total amount",
	totalAmount__tlp: ".Total amount due on the Invoice (including InvoiceTax, fees & InvoiceFreight etc.)",
	transactionTypeParam__lbl: ".Invoice type",
	transactionType__lbl: ".Transaction type",
	transactionType__tlp: ".Invoice transaction type to describe the invoice purpose such as final bill, pre-payment, etc.",
	transmissionStatus__lbl: ".Transmission status ",
	transmissionStatus__tlp: ".Transmission status ",
	type__lbl: "",
	unitCode__lbl: ".Unit",
	unitCode__tlp: ".Code",
	unitId__lbl: ".Unit(ID)",
	unselectedInvoices__lbl: ".Unselected Invoices",
	vatAmount__lbl: ".VAT amount",
	vatAmount__tlp: ".VAT Amount",
	vatRate__lbl: ".Vat rate",
	vatTaxableAmount__lbl: ".Net amount",
	vatTaxableAmount__tlp: ".Invoice amount excluding VAT in the specified currency"
});
