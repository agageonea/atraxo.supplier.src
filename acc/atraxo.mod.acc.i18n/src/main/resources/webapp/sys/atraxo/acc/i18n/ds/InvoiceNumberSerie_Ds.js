Ext.define("atraxo.acc.i18n.ds.InvoiceNumberSerie_Ds", {
	currentIndex__lbl: ".Position",
	currentIndex__tlp: ".Position",
	enabled__lbl: ".Enabled",
	enabled__tlp: ".Enabled",
	endIndex__lbl: ".End index",
	endIndex__tlp: ".End index",
	prefix__lbl: ".Prefix",
	prefix__tlp: ".Prefix",
	restart__lbl: ".Restart",
	restart__tlp: ".Restart",
	startIndex__lbl: ".Start index",
	startIndex__tlp: ".Start index",
	type__lbl: ".Type",
	type__tlp: ".Type"
});
