
Ext.define("atraxo.acc.i18n.dc.Invoice_Dc$Edit", {
	btn__lbl: "....",
	btn__tlp: "....",
	contractCode__lbl: ".Contract #",
	deliveryDateFrom__lbl: ".Delivery period from",
	deliveryLocCode__lbl: ".For location",
	deliverydateTo__lbl: ".period until",
	dueIn__lbl: ".DUE IN",
	invoiceForm__lbl: ".INVOICE FORM",
	invoiceNo__lbl: ".invoice #",
	issueDate__lbl: ".Issued on",
	issuerBaseLine__lbl: ".Issuer baseline",
	issuerCode__lbl: ".Invoice issuer",
	quantity__lbl: ".Total quantity",
	receiverCode__lbl: ".Invoice receiver",
	receivingDate__lbl: ".Received on",
	status__lbl: ".STATUS",
	totalAmount__lbl: ".Total amount",
	transactionType__lbl: ".Transaction type"
});

Ext.define("atraxo.acc.i18n.dc.Invoice_Dc$List", {
	amount__lbl: ".Amount",
	currency__lbl: ".Currency",
	date__lbl: ".Date",
	invoiceNumber__lbl: ".Invoice #",
	issuer__lbl: ".Issuer",
	location__lbl: ".Location",
	quantity__lbl: ".Quantity",
	status__lbl: ".Status",
	type__lbl: ".Type",
	unitCode__lbl: ".Unit"
});

Ext.define("atraxo.acc.i18n.dc.Invoice_Dc$New", {
	deliveryDateFrom__lbl: ".Delivery period from",
	deliveryLocCode__lbl: ".For location",
	deliverydateTo__lbl: ".until",
	invoiceNo__lbl: ".Invoice #",
	issueDate__lbl: ".Issued on",
	issuerCode__lbl: ".Received from",
	quantity__lbl: ".Total quantity",
	totalAmount__lbl: ".Total amount"
});
