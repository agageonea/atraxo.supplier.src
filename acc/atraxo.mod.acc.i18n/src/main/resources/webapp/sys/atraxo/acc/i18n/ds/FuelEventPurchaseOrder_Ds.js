Ext.define("atraxo.acc.i18n.ds.FuelEventPurchaseOrder_Ds", {
	billabelCostCurrency__lbl: ".Currency",
	billabelCostCurrency__tlp: ".Alphabetic code",
	billableCost__lbl: ".Billable cost",
	billableCost__tlp: ".The amount to be payed for the supplier for the delivered fuel",
	convertedAmount__lbl: ".Converted amount",
	customerCode__lbl: ".Customer",
	customerCode__tlp: ".Account code",
	eventType__lbl: ".Event Type",
	eventType__tlp: ".Event type",
	fuelingDate__lbl: ".Fueling date",
	fuelingDate__tlp: ".Fueling date",
	operationalType__lbl: ".Operational type",
	orderCode__lbl: ".Fuel order#",
	orderLocationId__lbl: ".Bill Price Source Id",
	product__lbl: ".Product",
	quantity__lbl: ".Quantity",
	quantity__tlp: ".Uplifted fuel quantity",
	systemCurrency__lbl: ".System currency",
	unitCode__lbl: ".Unit",
	unitCode__tlp: ".Code",
	unitId__lbl: ".Unit(ID)",
	unitId__tlp: ".Unit of measure for the uplifed quantity"
});
