
Ext.define("atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$AddList", {
	aircraftRegistration__lbl: ".Registration",
	customer__lbl: ".Ship-to",
	flightNo__lbl: ".Flight #",
	ticketNo__lbl: ".Ticket #"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$EditList", {
	amount__lbl: ".Amount",
	bolNumber__lbl: ".Bill of lading number",
	erpReference__lbl: ".External sales invoice #",
	flightID__lbl: ".Flight ID",
	flightType__lbl: ".Flight type",
	quantity__lbl: ".Quantity",
	shipToName__lbl: ".Ship to Name",
	shipTo__lbl: ".Ship to",
	suffix__lbl: ".Suffix",
	ticketNo__lbl: ".Ticket #",
	transmissionStatus__lbl: ".Transmission status",
	vatAmount__lbl: ".VAT amount"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$Filter", {
	bolNumber__lbl: ".Bill of lading number"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$MemoList", {
	aircraftRegistration__lbl: ".Registration",
	customer__lbl: ".Ship-to",
	flightNo__lbl: ".Flight #",
	ticketNo__lbl: ".Ticket #"
});

Ext.define("atraxo.acc.i18n.dc.OutgoingInvoiceLine_Dc$creditMemoReason", {
	creditMemoReasonLabel__lbl: ".Reason for creating the credit memo"
});
