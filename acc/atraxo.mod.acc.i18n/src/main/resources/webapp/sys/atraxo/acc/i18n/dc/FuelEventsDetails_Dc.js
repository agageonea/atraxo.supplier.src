
Ext.define("atraxo.acc.i18n.dc.FuelEventsDetails_Dc$List", {
	contractAmount__lbl: ".Contract amount",
	contractCode__lbl: ".Contract code",
	exchangeRate__lbl: ".Exchange rate",
	mainCategoryName__lbl: ".Main category",
	originalPriceCurrencyCode__lbl: ".Currency",
	originalPriceUnitCode__lbl: ".Unit",
	originalPrice__lbl: ".Contract original price",
	priceCategoryName__lbl: ".Price category",
	priceName__lbl: ".Name",
	settlementPrice__lbl: ".Contract price",
	vatAmount__lbl: ".VAT amount"
});
