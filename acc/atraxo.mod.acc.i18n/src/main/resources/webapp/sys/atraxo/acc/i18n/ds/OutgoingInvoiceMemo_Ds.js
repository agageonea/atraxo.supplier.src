Ext.define("atraxo.acc.i18n.ds.OutgoingInvoiceMemo_Ds", {
	baseLineDate__lbl: ".Base line date",
	baseLineDate__tlp: ".Date for payment deadline calculated by the system based on the contract payment terms. Can be overwritten by the user.",
	delLocCode__lbl: ".Delivery Loc",
	delLocCode__tlp: ".Code",
	delLocId__lbl: ".Delivery Loc(ID)",
	invoiceDate__lbl: ".Invoice date",
	invoiceDate__tlp: ".Invoice date",
	invoiceNo__lbl: ".Invoice number",
	invoiceNo__tlp: ".Invoice number",
	receiverCode__lbl: ".Receiver",
	receiverCode__tlp: ".Account code",
	receiverId__lbl: ".Receiver(ID)",
	referenceInvoiceId__lbl: ".Invoice Reference(ID)",
	referenceInvoiceNumber__lbl: ".Invoice number",
	referenceInvoiceNumber__tlp: ".Invoice number",
	status__lbl: ".Status",
	status__tlp: ".Invoice status indicator"
});
