Ext.define("atraxo.acc.i18n.frame.PayableAccruals_Ui", {
	/* view */
	History__ttl: ".History",
	/* menu */
	/* button */
	btnDetails__lbl: ".Details",
	btnDetails__tlp: ".Details",
	closeAccrual__lbl: ".Close",
	closeAccrual__tlp: ".Close",
	fuelEvDetails__lbl: ".Details",
	fuelEvDetails__tlp: ".Details",
	helpWdw__lbl: ".Help",
	helpWdw__tlp: ".Help",
	helpWdwEditor__lbl: ".Help",
	helpWdwEditor__tlp: ".Help",
	noInvoicing__lbl: ".No invoicing",
	noInvoicing__tlp: ".No invoicing",
	reopenAccrual__lbl: ".Reopen",
	reopenAccrual__tlp: ".Reopen",
	
	title: ".Accrued Payables"
});
