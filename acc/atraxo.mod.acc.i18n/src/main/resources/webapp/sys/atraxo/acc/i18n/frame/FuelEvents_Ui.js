Ext.define("atraxo.acc.i18n.frame.FuelEvents_Ui", {
	/* view */
	DeliveryNoteContract__ttl: ".Purchase contracts",
	FuelReleasedHistory__ttl: ".History",
	PurchaseOrder__ttl: ".Purchase order",
	SaleContract__ttl: ".Sale contracts",
	wdwOnHoldReasonList__ttl: ".Remark",
	/* menu */
	/* button */
	btnCloseOnHoldHistoryWdwList__lbl: ".Cancel",
	btnCloseOnHoldHistoryWdwList__tlp: ".Cancel",
	btnDeleteFuelEvent__lbl: ".Delete",
	btnDeleteFuelEvent__tlp: ".Delete",
	btnDetailsDeliveryNote__lbl: ".Details",
	btnDetailsDeliveryNote__tlp: ".Details",
	btnDnDetalis__lbl: ".Details",
	btnDnDetalis__tlp: ".Details",
	btnExportFailed__lbl: ".Failed",
	btnExportFailed__tlp: ".Resend fuel events that have failed to be transmitted to financial system",
	btnExportSelected__lbl: ".Selected",
	btnExportSelected__tlp: ".Resend selected fuel events to financial system",
	btnExportWithMenu__lbl: ".Resend",
	btnExportWithMenu__tlp: ".Resend fuel events to financial system",
	btnPurchaseOrderOpen__lbl: ".Open order",
	btnPurchaseOrderOpen__tlp: ".Open order",
	btnRelease__lbl: ".Release",
	btnRelease__tlp: ".Release",
	btnSaleContractOpen__lbl: ".Open contract",
	btnSaleContractOpen__tlp: ".Open contract",
	btnSaveCloseOnHoldHistoryWdwList__lbl: ".Save",
	btnSaveCloseOnHoldHistoryWdwList__tlp: ".Save",
	helpWdw__lbl: ".Help",
	helpWdw__tlp: ".Help",
	helpWdwEdit__lbl: ".Help",
	helpWdwEdit__tlp: ".Help",
	
	title: ".Fuel Events"
});
