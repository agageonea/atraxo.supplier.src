
Ext.define("atraxo.acc.i18n.dc.OutgoingRefInvoiceLine_Dc$AddList", {
	aircraftRegistration__lbl: ".Registration",
	customer__lbl: ".Ship-to",
	flightNo__lbl: ".Flight #",
	ticketNo__lbl: ".Ticket #"
});
