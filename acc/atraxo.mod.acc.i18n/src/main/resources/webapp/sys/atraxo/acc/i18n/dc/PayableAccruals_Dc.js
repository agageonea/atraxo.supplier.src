
Ext.define("atraxo.acc.i18n.dc.PayableAccruals_Dc$Filter", {
	invoiceStatus__lbl: ".Invoice status"
});

Ext.define("atraxo.acc.i18n.dc.PayableAccruals_Dc$List", {
	accruedCost__lbl: ".Accrued cost",
	expectedAmountSet__lbl: ".Calculated cost",
	invoiceCost__lbl: ".Invoice cost",
	invoiceStatus__lbl: ".Invoicing status"
});

Ext.define("atraxo.acc.i18n.dc.PayableAccruals_Dc$mainList", {
	fuelingDate__lbl: ".Month",
	grossAmount__lbl: ".Gross amount",
	netAmount__lbl: ".Net amount",
	vatAmount__lbl: ".VAT amount"
});
