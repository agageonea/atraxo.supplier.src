
Ext.define("atraxo.acc.i18n.dc.DeliveryNoteContract_Dc$pcList", {
	amount__lbl: ".Amount",
	contracType__lbl: ".Type",
	contractCode__lbl: ".Contract#",
	contractSubType__lbl: ".Delivery to",
	convAmount__lbl: ".Converted amount",
	convCurr__lbl: ".System currency",
	currency__lbl: ".Currency",
	isSpot__lbl: ".Term/Spot",
	scope__lbl: ".Scope",
	supplier__lbl: ".Supplier"
});
