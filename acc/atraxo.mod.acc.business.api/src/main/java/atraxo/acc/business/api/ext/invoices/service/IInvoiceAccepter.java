package atraxo.acc.business.api.ext.invoices.service;

import atraxo.fmbas.business.api.ext.ws.IIncomingMessageProcessor;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Service interface with asynchronous methods for invoice/credit memo accepter.
 *
 * @author zspeter
 */
public interface IInvoiceAccepter extends IIncomingMessageProcessor {

	/**
	 * Asynchronous method for invoice acknowledgment.
	 *
	 * @param request
	 * @return
	 * @throws BusinessException
	 */
	MSG acknoledgeInvoice(MSG request) throws BusinessException;

	/**
	 * Asynchronous method for credit memo acknowledgment.
	 *
	 * @param request
	 * @return
	 * @throws BusinessException
	 */
	MSG acknoledgeCreditMemo(MSG request) throws BusinessException;
}
