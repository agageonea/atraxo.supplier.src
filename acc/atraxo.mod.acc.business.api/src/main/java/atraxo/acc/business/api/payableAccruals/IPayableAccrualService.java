/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.payableAccruals;

import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import java.text.SimpleDateFormat;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link PayableAccrual} domain entity.
 */
public interface IPayableAccrualService extends IEntityService<PayableAccrual> {

	/**
	 * Custom service generatePayableAccruals
	 *
	 * @return void
	 */
	public void generatePayableAccruals(DeliveryNote deliveryNote,
			Contract contract) throws BusinessException;

	/**
	 * Custom service updatePayableAccruals
	 *
	 * @return void
	 */
	public void updatePayableAccruals(PayableAccrual payableAccrual)
			throws BusinessException;

	/**
	 * Custom service findAll
	 *
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findAll() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return PayableAccrual
	 */
	public PayableAccrual findByFuelEventContract(Contract contract,
			FuelEvent fuelEvent);

	/**
	 * Find by unique key
	 *
	 * @return PayableAccrual
	 */
	public PayableAccrual findByFuelEventContract(Long contractId,
			Long fuelEventId);

	/**
	 * Find by unique key
	 *
	 * @return PayableAccrual
	 */
	public PayableAccrual findByBusiness(Integer id);

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByFuelEvent(FuelEvent fuelEvent);

	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByFuelEventId(Integer fuelEventId);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByLocationId(Integer locationId);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findBySupplier(Suppliers supplier);

	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findBySupplierId(Integer supplierId);

	/**
	 * Find by reference: expectedCurrency
	 *
	 * @param expectedCurrency
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByExpectedCurrency(
			Currencies expectedCurrency);

	/**
	 * Find by ID of reference: expectedCurrency.id
	 *
	 * @param expectedCurrencyId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByExpectedCurrencyId(
			Integer expectedCurrencyId);

	/**
	 * Find by reference: invoicedCurrency
	 *
	 * @param invoicedCurrency
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByInvoicedCurrency(
			Currencies invoicedCurrency);

	/**
	 * Find by ID of reference: invoicedCurrency.id
	 *
	 * @param invoicedCurrencyId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByInvoicedCurrencyId(
			Integer invoicedCurrencyId);

	/**
	 * Find by reference: accrualCurrency
	 *
	 * @param accrualCurrency
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByAccrualCurrency(Currencies accrualCurrency);

	/**
	 * Find by ID of reference: accrualCurrency.id
	 *
	 * @param accrualCurrencyId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByAccrualCurrencyId(
			Integer accrualCurrencyId);

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByContract(Contract contract);

	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<PayableAccrual>
	 */
	public List<PayableAccrual> findByContractId(Integer contractId);
}
