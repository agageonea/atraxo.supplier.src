package atraxo.acc.business.api.ext.invoices.service;

import atraxo.fmbas.business.api.ext.ws.IIncomingMessageProcessor;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author abolindu
 */
public interface IRicohAccepter extends IIncomingMessageProcessor {

	/**
	 * Asynchronous method for invoice sent to Ricoh acknowledgement
	 *
	 * @param request
	 * @return
	 * @throws BusinessException
	 */
	MSG acknowledgeRicohSentFiles(MSG request) throws BusinessException;
}
