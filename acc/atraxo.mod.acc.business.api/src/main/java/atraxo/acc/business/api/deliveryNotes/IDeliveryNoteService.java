/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.deliveryNotes;

import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DeliveryNote} domain entity.
 */
public interface IDeliveryNoteService extends IEntityService<DeliveryNote> {

	/**
	 * Custom service reallocate
	 *
	 * @return void
	 */
	public void reallocate(Object object) throws BusinessException;

	/**
	 * Custom service propagateIncInvoiceStatus
	 *
	 * @return void
	 */
	public void propagateIncInvoiceStatus(InvoiceLine invoiceLine)
			throws BusinessException;

	/**
	 * Custom service findByObjectIdAndType
	 *
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByObjectIdAndType(Integer objectId,
			String objectType) throws BusinessException;

	/**
	 * Custom service deleteDeliveryNote
	 *
	 * @return void
	 */
	public void deleteDeliveryNote(Integer objectId, String objectType)
			throws BusinessException;

	/**
	 * Custom service deleteDeliveryNote
	 *
	 * @return void
	 */
	public void deleteDeliveryNote(DeliveryNote deliveryNote)
			throws BusinessException;

	/**
	 * Custom service resetSupplierContract
	 *
	 * @return void
	 */
	public void resetSupplierContract(Contract contract)
			throws BusinessException;

	/**
	 * Custom service addRemoveDeliveryNoteContract
	 *
	 * @return void
	 */
	public void addRemoveDeliveryNoteContract(DeliveryNote deliveryNote,
			Contract contract, boolean addRemove) throws BusinessException;

	/**
	 * Custom service maintainDeliveryNotesContracts
	 *
	 * @return void
	 */
	public void maintainDeliveryNotesContracts(Contract contract)
			throws BusinessException;

	/**
	 * Custom service removeDeliveryNoteContract
	 *
	 * @return void
	 */
	public void removeDeliveryNoteContract(Contract contract)
			throws BusinessException;

	/**
	 * Custom service getDensity
	 *
	 * @return double
	 */
	public double getDensity(DeliveryNote dn) throws BusinessException;

	/**
	 * Custom service checkOutRangedFuelEvents
	 *
	 * @return void
	 */
	public void checkOutRangedFuelEvents(Contract contract)
			throws BusinessException;

	/**
	 * Custom service removeByOutgoingInvoiceLines
	 *
	 * @return void
	 */
	public void removeByOutgoingInvoiceLines(
			List<OutgoingInvoiceLine> outgoingInvoiceLines)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return DeliveryNote
	 */
	public DeliveryNote findByKey(QuantitySource source, Date fuelingDate,
			Locations departure, Customer customer, Aircraft aircraft,
			String flightNumber, Suffix suffix, String ticketNumber);

	/**
	 * Find by unique key
	 *
	 * @return DeliveryNote
	 */
	public DeliveryNote findByKey(QuantitySource source, Date fuelingDate,
			Long departureId, Long customerId, Long aircraftId,
			String flightNumber, Suffix suffix, String ticketNumber);

	/**
	 * Find by unique key
	 *
	 * @return DeliveryNote
	 */
	public DeliveryNote findByBusiness(Integer id);

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByFuelEvent(FuelEvent fuelEvent);

	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByFuelEventId(Integer fuelEventId);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: departure
	 *
	 * @param departure
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByDeparture(Locations departure);

	/**
	 * Find by ID of reference: departure.id
	 *
	 * @param departureId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByDepartureId(Integer departureId);

	/**
	 * Find by reference: aircraft
	 *
	 * @param aircraft
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByAircraft(Aircraft aircraft);

	/**
	 * Find by ID of reference: aircraft.id
	 *
	 * @param aircraftId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByAircraftId(Integer aircraftId);

	/**
	 * Find by reference: fuelSupplier
	 *
	 * @param fuelSupplier
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByFuelSupplier(Suppliers fuelSupplier);

	/**
	 * Find by ID of reference: fuelSupplier.id
	 *
	 * @param fuelSupplierId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByFuelSupplierId(Integer fuelSupplierId);

	/**
	 * Find by reference: iplAgent
	 *
	 * @param iplAgent
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByIplAgent(Suppliers iplAgent);

	/**
	 * Find by ID of reference: iplAgent.id
	 *
	 * @param iplAgentId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByIplAgentId(Integer iplAgentId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByUnitId(Integer unitId);

	/**
	 * Find by reference: payableCostCurrency
	 *
	 * @param payableCostCurrency
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByPayableCostCurrency(
			Currencies payableCostCurrency);

	/**
	 * Find by ID of reference: payableCostCurrency.id
	 *
	 * @param payableCostCurrencyId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByPayableCostCurrencyId(
			Integer payableCostCurrencyId);

	/**
	 * Find by reference: billableCostCurrency
	 *
	 * @param billableCostCurrency
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByBillableCostCurrency(
			Currencies billableCostCurrency);

	/**
	 * Find by ID of reference: billableCostCurrency.id
	 *
	 * @param billableCostCurrencyId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByBillableCostCurrencyId(
			Integer billableCostCurrencyId);

	/**
	 * Find by reference: netUpliftUnit
	 *
	 * @param netUpliftUnit
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByNetUpliftUnit(Unit netUpliftUnit);

	/**
	 * Find by ID of reference: netUpliftUnit.id
	 *
	 * @param netUpliftUnitId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByNetUpliftUnitId(Integer netUpliftUnitId);

	/**
	 * Find by reference: contractHolder
	 *
	 * @param contractHolder
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByContractHolder(Customer contractHolder);

	/**
	 * Find by ID of reference: contractHolder.id
	 *
	 * @param contractHolderId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByContractHolderId(Integer contractHolderId);

	/**
	 * Find by reference: shipTo
	 *
	 * @param shipTo
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByShipTo(Customer shipTo);

	/**
	 * Find by ID of reference: shipTo.id
	 *
	 * @param shipToId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByShipToId(Integer shipToId);

	/**
	 * Find by reference: deliveryNoteContracts
	 *
	 * @param deliveryNoteContracts
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByDeliveryNoteContracts(
			DeliveryNoteContract deliveryNoteContracts);

	/**
	 * Find by ID of reference: deliveryNoteContracts.id
	 *
	 * @param deliveryNoteContractsId
	 * @return List<DeliveryNote>
	 */
	public List<DeliveryNote> findByDeliveryNoteContractsId(
			Integer deliveryNoteContractsId);
}
