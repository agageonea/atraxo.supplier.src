/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.deliveryNotes;

import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.cmm.domain.impl.contracts.Contract;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link DeliveryNoteContract} domain entity.
 */
public interface IDeliveryNoteContractService
		extends
			IEntityService<DeliveryNoteContract> {

	/**
	 * Custom service findByContract
	 *
	 * @return List<DeliveryNoteContract>
	 */
	public List<DeliveryNoteContract> findByContract(Contract c)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return DeliveryNoteContract
	 */
	public DeliveryNoteContract findByDeliveryNotesContracts(
			Contract contracts, DeliveryNote deliveryNotes);

	/**
	 * Find by unique key
	 *
	 * @return DeliveryNoteContract
	 */
	public DeliveryNoteContract findByDeliveryNotesContracts(Long contractsId,
			Long deliveryNotesId);

	/**
	 * Find by unique key
	 *
	 * @return DeliveryNoteContract
	 */
	public DeliveryNoteContract findByBusiness(Integer id);

	/**
	 * Find by reference: contracts
	 *
	 * @param contracts
	 * @return List<DeliveryNoteContract>
	 */
	public List<DeliveryNoteContract> findByContracts(Contract contracts);

	/**
	 * Find by ID of reference: contracts.id
	 *
	 * @param contractsId
	 * @return List<DeliveryNoteContract>
	 */
	public List<DeliveryNoteContract> findByContractsId(Integer contractsId);

	/**
	 * Find by reference: deliveryNotes
	 *
	 * @param deliveryNotes
	 * @return List<DeliveryNoteContract>
	 */
	public List<DeliveryNoteContract> findByDeliveryNotes(
			DeliveryNote deliveryNotes);

	/**
	 * Find by ID of reference: deliveryNotes.id
	 *
	 * @param deliveryNotesId
	 * @return List<DeliveryNoteContract>
	 */
	public List<DeliveryNoteContract> findByDeliveryNotesId(
			Integer deliveryNotesId);
}
