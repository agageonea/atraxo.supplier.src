/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.invoices;

import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link Invoice} domain entity.
 */
public interface IInvoiceService extends IEntityService<Invoice> {

	/**
	 * Custom service resetInvoice
	 *
	 * @return void
	 */
	public void resetInvoice(List<Invoice> invoices) throws BusinessException;

	/**
	 * Custom service finishInput
	 *
	 * @return void
	 */
	public void finishInput(Invoice invoice, String value)
			throws BusinessException;

	/**
	 * Custom service generateFuelEvent
	 *
	 * @return void
	 */
	public void generateFuelEvent(Invoice invoice) throws BusinessException;

	/**
	 * Custom service modifyStatus
	 *
	 * @return void
	 */
	public void modifyStatus(List<Invoice> invoices, String action,
			BillStatus status) throws BusinessException;

	/**
	 * Custom service updateWithoutBusiness
	 *
	 * @return void
	 */
	public void updateWithoutBusiness(Invoice invice) throws BusinessException;

	/**
	 * Custom service modifyStatus
	 *
	 * @return void
	 */
	public void modifyStatus(Object object) throws BusinessException;

	/**
	 * Custom service findByInvoiceNumbers
	 *
	 * @return List<Invoice>
	 */
	public List<Invoice> findByInvoiceNumbers(List<String> list)
			throws BusinessException;

	/**
	 * Custom service deleteByOutgoingInvoice
	 *
	 * @return void
	 */
	public void deleteByOutgoingInvoice(List<OutgoingInvoice> list)
			throws BusinessException;

	/**
	 * Custom service markAsCredited
	 *
	 * @return void
	 */
	public void markAsCredited(OutgoingInvoice outgoingInvoice)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return Invoice
	 */
	public Invoice findByKey(Suppliers issuer, Date issueDate, String invoiceNo);

	/**
	 * Find by unique key
	 *
	 * @return Invoice
	 */
	public Invoice findByKey(Long issuerId, Date issueDate, String invoiceNo);

	/**
	 * Find by unique key
	 *
	 * @return Invoice
	 */
	public Invoice findByBusiness(Integer id);

	/**
	 * Find by reference: receiver
	 *
	 * @param receiver
	 * @return List<Invoice>
	 */
	public List<Invoice> findByReceiver(Customer receiver);

	/**
	 * Find by ID of reference: receiver.id
	 *
	 * @param receiverId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByReceiverId(Integer receiverId);

	/**
	 * Find by reference: deliveryLoc
	 *
	 * @param deliveryLoc
	 * @return List<Invoice>
	 */
	public List<Invoice> findByDeliveryLoc(Locations deliveryLoc);

	/**
	 * Find by ID of reference: deliveryLoc.id
	 *
	 * @param deliveryLocId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByDeliveryLocId(Integer deliveryLocId);

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<Invoice>
	 */
	public List<Invoice> findByCountry(Country country);

	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByCountryId(Integer countryId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<Invoice>
	 */
	public List<Invoice> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByUnitId(Integer unitId);

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<Invoice>
	 */
	public List<Invoice> findByContract(Contract contract);

	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByContractId(Integer contractId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<Invoice>
	 */
	public List<Invoice> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: issuer
	 *
	 * @param issuer
	 * @return List<Invoice>
	 */
	public List<Invoice> findByIssuer(Suppliers issuer);

	/**
	 * Find by ID of reference: issuer.id
	 *
	 * @param issuerId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByIssuerId(Integer issuerId);

	/**
	 * Find by reference: invoiceLines
	 *
	 * @param invoiceLines
	 * @return List<Invoice>
	 */
	public List<Invoice> findByInvoiceLines(InvoiceLine invoiceLines);

	/**
	 * Find by ID of reference: invoiceLines.id
	 *
	 * @param invoiceLinesId
	 * @return List<Invoice>
	 */
	public List<Invoice> findByInvoiceLinesId(Integer invoiceLinesId);
}
