/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.receivableAccruals;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import java.text.SimpleDateFormat;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ReceivableAccrual} domain entity.
 */
public interface IReceivableAccrualService
		extends
			IEntityService<ReceivableAccrual> {

	/**
	 * Custom service findAll
	 *
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findAll() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return ReceivableAccrual
	 */
	public ReceivableAccrual findByBusiness(Integer id);

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByFuelEvent(FuelEvent fuelEvent);

	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByFuelEventId(Integer fuelEventId);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByLocationId(Integer locationId);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findBySupplier(Suppliers supplier);

	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findBySupplierId(Integer supplierId);

	/**
	 * Find by reference: expectedCurrency
	 *
	 * @param expectedCurrency
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByExpectedCurrency(
			Currencies expectedCurrency);

	/**
	 * Find by ID of reference: expectedCurrency.id
	 *
	 * @param expectedCurrencyId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByExpectedCurrencyId(
			Integer expectedCurrencyId);

	/**
	 * Find by reference: invoicedCurrency
	 *
	 * @param invoicedCurrency
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByInvoicedCurrency(
			Currencies invoicedCurrency);

	/**
	 * Find by ID of reference: invoicedCurrency.id
	 *
	 * @param invoicedCurrencyId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByInvoicedCurrencyId(
			Integer invoicedCurrencyId);

	/**
	 * Find by reference: accrualCurrency
	 *
	 * @param accrualCurrency
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByAccrualCurrency(
			Currencies accrualCurrency);

	/**
	 * Find by ID of reference: accrualCurrency.id
	 *
	 * @param accrualCurrencyId
	 * @return List<ReceivableAccrual>
	 */
	public List<ReceivableAccrual> findByAccrualCurrencyId(
			Integer accrualCurrencyId);
}
