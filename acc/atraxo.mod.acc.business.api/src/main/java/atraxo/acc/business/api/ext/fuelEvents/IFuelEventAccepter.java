package atraxo.acc.business.api.ext.fuelEvents;

import atraxo.fmbas.business.api.ext.ws.IIncomingMessageProcessor;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Service with asynchrony methods for fuel event acknowledgment.
 *
 * @author zspeter
 */
public interface IFuelEventAccepter extends IIncomingMessageProcessor {

	/**
	 * @param request
	 * @return
	 * @throws BusinessException
	 */
	MSG acknoledge(MSG request) throws BusinessException;
}
