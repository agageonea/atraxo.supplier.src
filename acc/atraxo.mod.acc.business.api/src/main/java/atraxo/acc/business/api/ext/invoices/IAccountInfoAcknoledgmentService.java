package atraxo.acc.business.api.ext.invoices;

import javax.xml.bind.JAXBException;

import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author apetho
 */
public interface IAccountInfoAcknoledgmentService {

	/**
	 * @param msg
	 * @return succesfull flag
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	boolean processInvoice(MSG msg) throws BusinessException, JAXBException;

	/**
	 * Process fuel event acknowledgment.
	 *
	 * @param msg
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	boolean processFuelEvent(MSG msg) throws BusinessException, JAXBException;

	/**
	 * @param msg
	 * @return succesfull flag
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	public boolean processCreditMemo(MSG msg) throws BusinessException, JAXBException;

	/**
	 * @param msg
	 * @return
	 * @throws BusinessException
	 * @throws JAXBException
	 */
	boolean processRicohAcknoledgment(MSG msg) throws BusinessException, JAXBException;
}
