/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.invoiceLines;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link OutgoingInvoiceLine} domain entity.
 */
public interface IOutgoingInvoiceLineService
		extends
			IEntityService<OutgoingInvoiceLine> {

	/**
	 * Custom service getFailedInvoiceLines
	 *
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> getFailedInvoiceLines(
			OutgoingInvoice outgoingInvoice) throws BusinessException;

	/**
	 * Custom service updateWithoutBusiness
	 *
	 * @return void
	 */
	public void updateWithoutBusiness(List<OutgoingInvoiceLine> invoiceLines)
			throws BusinessException;

	/**
	 * Custom service insertNewInvoiceLines
	 *
	 * @return void
	 */
	public void insertNewInvoiceLines(List<FuelEvent> fuelEvents,
			OutgoingInvoice outgoingInvoice) throws BusinessException;

	/**
	 * Custom service updateHeaderOnDeleteInvoiceLine
	 *
	 * @return void
	 */
	public void updateHeaderOnDeleteInvoiceLine(List<OutgoingInvoiceLine> oils)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoiceLine
	 */
	public OutgoingInvoiceLine findByKey(OutgoingInvoice outgoingInvoice,
			FuelEvent fuelEvent);

	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoiceLine
	 */
	public OutgoingInvoiceLine findByKey(Long outgoingInvoiceId,
			Long fuelEventId);

	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoiceLine
	 */
	public OutgoingInvoiceLine findByBusiness(Integer id);

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByFuelEvent(FuelEvent fuelEvent);

	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByFuelEventId(Integer fuelEventId);

	/**
	 * Find by reference: outgoingInvoice
	 *
	 * @param outgoingInvoice
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByOutgoingInvoice(
			OutgoingInvoice outgoingInvoice);

	/**
	 * Find by ID of reference: outgoingInvoice.id
	 *
	 * @param outgoingInvoiceId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByOutgoingInvoiceId(
			Integer outgoingInvoiceId);

	/**
	 * Find by reference: referenceInvItem
	 *
	 * @param referenceInvItem
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByReferenceInvItem(
			OutgoingInvoice referenceInvItem);

	/**
	 * Find by ID of reference: referenceInvItem.id
	 *
	 * @param referenceInvItemId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByReferenceInvItemId(
			Integer referenceInvItemId);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: destination
	 *
	 * @param destination
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByDestination(Locations destination);

	/**
	 * Find by ID of reference: destination.id
	 *
	 * @param destinationId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByDestinationId(Integer destinationId);

	/**
	 * Find by reference: referenceInvoiceLine
	 *
	 * @param referenceInvoiceLine
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByReferenceInvoiceLine(
			OutgoingInvoiceLine referenceInvoiceLine);

	/**
	 * Find by ID of reference: referenceInvoiceLine.id
	 *
	 * @param referenceInvoiceLineId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByReferenceInvoiceLineId(
			Integer referenceInvoiceLineId);

	/**
	 * Find by reference: details
	 *
	 * @param details
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByDetails(
			OutgoingInvoiceLineDetails details);

	/**
	 * Find by ID of reference: details.id
	 *
	 * @param detailsId
	 * @return List<OutgoingInvoiceLine>
	 */
	public List<OutgoingInvoiceLine> findByDetailsId(Integer detailsId);
}
