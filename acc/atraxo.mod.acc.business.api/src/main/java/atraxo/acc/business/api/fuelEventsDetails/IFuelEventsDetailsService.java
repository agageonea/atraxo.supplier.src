/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.fuelEventsDetails;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelEventsDetails} domain entity.
 */
public interface IFuelEventsDetailsService
		extends
			IEntityService<FuelEventsDetails> {

	/**
	 * Find by unique key
	 *
	 * @return FuelEventsDetails
	 */
	public FuelEventsDetails findByKey(FuelEvent fuelEvent,
			String contractCode, PriceCategory priceCategory);

	/**
	 * Find by unique key
	 *
	 * @return FuelEventsDetails
	 */
	public FuelEventsDetails findByKey(Long fuelEventId, String contractCode,
			Long priceCategoryId);

	/**
	 * Find by unique key
	 *
	 * @return FuelEventsDetails
	 */
	public FuelEventsDetails findByBusiness(Integer id);

	/**
	 * Find by reference: fuelEvent
	 *
	 * @param fuelEvent
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByFuelEvent(FuelEvent fuelEvent);

	/**
	 * Find by ID of reference: fuelEvent.id
	 *
	 * @param fuelEventId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByFuelEventId(Integer fuelEventId);

	/**
	 * Find by reference: priceCategory
	 *
	 * @param priceCategory
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByPriceCategory(
			PriceCategory priceCategory);

	/**
	 * Find by ID of reference: priceCategory.id
	 *
	 * @param priceCategoryId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByPriceCategoryId(Integer priceCategoryId);

	/**
	 * Find by reference: mainCategory
	 *
	 * @param mainCategory
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByMainCategory(MainCategory mainCategory);

	/**
	 * Find by ID of reference: mainCategory.id
	 *
	 * @param mainCategoryId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByMainCategoryId(Integer mainCategoryId);

	/**
	 * Find by reference: originalPriceCurrency
	 *
	 * @param originalPriceCurrency
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByOriginalPriceCurrency(
			Currencies originalPriceCurrency);

	/**
	 * Find by ID of reference: originalPriceCurrency.id
	 *
	 * @param originalPriceCurrencyId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByOriginalPriceCurrencyId(
			Integer originalPriceCurrencyId);

	/**
	 * Find by reference: originalPriceUnit
	 *
	 * @param originalPriceUnit
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByOriginalPriceUnit(
			Unit originalPriceUnit);

	/**
	 * Find by ID of reference: originalPriceUnit.id
	 *
	 * @param originalPriceUnitId
	 * @return List<FuelEventsDetails>
	 */
	public List<FuelEventsDetails> findByOriginalPriceUnitId(
			Integer originalPriceUnitId);
}
