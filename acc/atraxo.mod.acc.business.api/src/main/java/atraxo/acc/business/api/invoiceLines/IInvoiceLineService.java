/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.invoiceLines;

import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link InvoiceLine} domain entity.
 */
public interface IInvoiceLineService extends IEntityService<InvoiceLine> {

	/**
	 * Custom service balanceInvoice
	 *
	 * @return void
	 */
	public void balanceInvoice(InvoiceLine invoiceLine)
			throws BusinessException;

	/**
	 * Custom service updateWithoutBusiness
	 *
	 * @return void
	 */
	public void updateWithoutBusiness(InvoiceLine invoiceLine)
			throws BusinessException;

	/**
	 * Custom service removeInvoiceLines
	 *
	 * @return void
	 */
	public void removeInvoiceLines(
			List<OutgoingInvoiceLine> outgoingInvoiceLines)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return InvoiceLine
	 */
	public InvoiceLine findByBusiness(Integer id);

	/**
	 * Find by reference: invoice
	 *
	 * @param invoice
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByInvoice(Invoice invoice);

	/**
	 * Find by ID of reference: invoice.id
	 *
	 * @param invoiceId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByInvoiceId(Integer invoiceId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByUnitId(Integer unitId);

	/**
	 * Find by reference: netQuantityUnit
	 *
	 * @param netQuantityUnit
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByNetQuantityUnit(Unit netQuantityUnit);

	/**
	 * Find by ID of reference: netQuantityUnit.id
	 *
	 * @param netQuantityUnitId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByNetQuantityUnitId(Integer netQuantityUnitId);

	/**
	 * Find by reference: grossQuantityUnit
	 *
	 * @param grossQuantityUnit
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByGrossQuantityUnit(Unit grossQuantityUnit);

	/**
	 * Find by ID of reference: grossQuantityUnit.id
	 *
	 * @param grossQuantityUnitId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByGrossQuantityUnitId(
			Integer grossQuantityUnitId);

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByContract(Contract contract);

	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByContractId(Integer contractId);

	/**
	 * Find by reference: destination
	 *
	 * @param destination
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByDestination(Locations destination);

	/**
	 * Find by ID of reference: destination.id
	 *
	 * @param destinationId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByDestinationId(Integer destinationId);

	/**
	 * Find by reference: referenceInvoiceLine
	 *
	 * @param referenceInvoiceLine
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByReferenceInvoiceLine(
			InvoiceLine referenceInvoiceLine);

	/**
	 * Find by ID of reference: referenceInvoiceLine.id
	 *
	 * @param referenceInvoiceLineId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByReferenceInvoiceLineId(
			Integer referenceInvoiceLineId);

	/**
	 * Find by reference: airlineDesignator
	 *
	 * @param airlineDesignator
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByAirlineDesignator(Customer airlineDesignator);

	/**
	 * Find by ID of reference: airlineDesignator.id
	 *
	 * @param airlineDesignatorId
	 * @return List<InvoiceLine>
	 */
	public List<InvoiceLine> findByAirlineDesignatorId(
			Integer airlineDesignatorId);
}
