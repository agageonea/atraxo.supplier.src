/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.invoices;

import atraxo.acc.domain.ext.invoices.GeneratorResponse;
import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.ad.domain.impl.attachment.Attachment;
import atraxo.ad.domain.impl.attachment.AttachmentType;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Area;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link OutgoingInvoice} domain entity.
 */
public interface IOutgoingInvoiceService
		extends
			IEntityService<OutgoingInvoice> {

	/**
	 * Custom service generate
	 *
	 * @return GeneratorResponse
	 */
	public GeneratorResponse generate(Date dateFrom, Date dateTo,
			Date closeDate, TransactionType cltransactionType,
			Customer customer, boolean separatePerShipTo, Locations location)
			throws BusinessException;

	/**
	 * Custom service generate
	 *
	 * @return GeneratorResponse
	 */
	public GeneratorResponse generate(Date dateFrom, Date dateTo,
			Date closeDate, TransactionType transactionType, Customer customer,
			boolean separatePerShipTo, Area area) throws BusinessException;

	/**
	 * Custom service generate
	 *
	 * @return GeneratorResponse
	 */
	public GeneratorResponse generate(Date dateFrom, Date dateTo,
			Date closeDate, TransactionType transactionType, Customer customer,
			boolean separatePerShipTo) throws BusinessException;

	/**
	 * Custom service approve
	 *
	 * @return void
	 */
	public void approve(List<OutgoingInvoice> list) throws BusinessException;

	/**
	 * Custom service approve
	 *
	 * @return void
	 */
	public void approve(List<OutgoingInvoice> list, String reason)
			throws BusinessException;

	/**
	 * Custom service reject
	 *
	 * @return void
	 */
	public void reject(List<OutgoingInvoice> list, String action, String reason)
			throws BusinessException;

	/**
	 * Custom service markAsPaid
	 *
	 * @return void
	 */
	public void markAsPaid(List<OutgoingInvoice> list) throws BusinessException;

	/**
	 * Custom service markAsPaidCreditMemo
	 *
	 * @return void
	 */
	public void markAsPaidCreditMemo(OutgoingInvoice outgoingInvoice)
			throws BusinessException;

	/**
	 * Custom service exportToNAV
	 *
	 * @return boolean
	 */
	public boolean exportToNAV(OutgoingInvoice outgoingInvoice)
			throws BusinessException;

	/**
	 * Custom service updatePrice
	 *
	 * @return void
	 */
	public void updatePrice(Integer id, FuelEvent event)
			throws BusinessException;

	/**
	 * Custom service markAsCredited
	 *
	 * @return void
	 */
	public void markAsCredited(OutgoingInvoice outgoingInvoice)
			throws BusinessException;

	/**
	 * Custom service markAsExported
	 *
	 * @return void
	 */
	public void markAsExported(OutgoingInvoice outgoingInvoice)
			throws BusinessException;

	/**
	 * Custom service markAsUploaded
	 *
	 * @return void
	 */
	public void markAsUploaded(OutgoingInvoice outgoingInvoice)
			throws BusinessException;

	/**
	 * Custom service updateTransmissionStatus
	 *
	 * @return void
	 */
	public void updateTransmissionStatus(OutgoingInvoiceLine oil,
			InvoiceLineTransmissionStatus status, String transactionID)
			throws BusinessException;

	/**
	 * Custom service submitForApproval
	 *
	 * @return void
	 */
	public void submitForApproval(OutgoingInvoice e, BidApprovalStatus status,
			String action, String reason) throws BusinessException;

	/**
	 * Custom service createAttachment
	 *
	 * @return Attachment
	 */
	public Attachment createAttachment(OutgoingInvoice invoice, String name,
			AttachmentType type, String targetAlias, String notes,
			String extension, Boolean active) throws BusinessException;

	/**
	 * Custom service exportToMail
	 *
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> exportToMail(List<String> issuers,
			List<String> receivers, List<Locations> locations, Date offsetDate,
			String resend) throws BusinessException;

	/**
	 * Custom service hasAllLinesCredited
	 *
	 * @return boolean
	 */
	public boolean hasAllLinesCredited(OutgoingInvoice outgoingInvoice)
			throws BusinessException;

	/**
	 * Custom service createInvoiceReport
	 *
	 * @return void
	 */
	public void createInvoiceReport(OutgoingInvoice outgoingInvoice)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoice
	 */
	public OutgoingInvoice findByBusiness(Integer id);

	/**
	 * Find by reference: receiver
	 *
	 * @param receiver
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByReceiver(Customer receiver);

	/**
	 * Find by ID of reference: receiver.id
	 *
	 * @param receiverId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByReceiverId(Integer receiverId);

	/**
	 * Find by reference: deliveryLoc
	 *
	 * @param deliveryLoc
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByDeliveryLoc(Locations deliveryLoc);

	/**
	 * Find by ID of reference: deliveryLoc.id
	 *
	 * @param deliveryLocId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByDeliveryLocId(Integer deliveryLocId);

	/**
	 * Find by reference: country
	 *
	 * @param country
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByCountry(Country country);

	/**
	 * Find by ID of reference: country.id
	 *
	 * @param countryId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByCountryId(Integer countryId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByUnitId(Integer unitId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: issuer
	 *
	 * @param issuer
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByIssuer(Customer issuer);

	/**
	 * Find by ID of reference: issuer.id
	 *
	 * @param issuerId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByIssuerId(Integer issuerId);

	/**
	 * Find by reference: invoiceReference
	 *
	 * @param invoiceReference
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByInvoiceReference(
			OutgoingInvoice invoiceReference);

	/**
	 * Find by ID of reference: invoiceReference.id
	 *
	 * @param invoiceReferenceId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByInvoiceReferenceId(
			Integer invoiceReferenceId);

	/**
	 * Find by reference: invoiceLines
	 *
	 * @param invoiceLines
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByInvoiceLines(
			OutgoingInvoiceLine invoiceLines);

	/**
	 * Find by ID of reference: invoiceLines.id
	 *
	 * @param invoiceLinesId
	 * @return List<OutgoingInvoice>
	 */
	public List<OutgoingInvoice> findByInvoiceLinesId(Integer invoiceLinesId);
}
