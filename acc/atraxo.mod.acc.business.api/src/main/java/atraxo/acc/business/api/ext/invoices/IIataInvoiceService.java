package atraxo.acc.business.api.ext.invoices;

import java.io.OutputStream;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.iata.fuelplus.iata.invoice.InvoiceTransmission;

/**
 * @author zspeter
 */
public interface IIataInvoiceService {

	/**
	 * @param invoiceTransmission
	 * @param out
	 */
	void generateIataXml(InvoiceTransmission invoiceTransmission, OutputStream out) throws BusinessException;

}
