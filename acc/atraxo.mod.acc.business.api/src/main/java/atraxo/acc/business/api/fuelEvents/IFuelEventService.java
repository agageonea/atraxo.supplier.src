/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.fuelEvents;

import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelEvent} domain entity.
 */
public interface IFuelEventService extends IEntityService<FuelEvent> {

	/**
	 * Custom service calculateReceivableCost
	 *
	 * @return void
	 */
	public void calculateReceivableCost(FuelEvent fuelEvent)
			throws BusinessException;

	/**
	 * Custom service getForOutgoingInvoices
	 *
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> getForOutgoingInvoices(Date startDate, Date endDate,
			Customer customer, Locations location) throws BusinessException;

	/**
	 * Custom service getForOutgoingInvoicesIgnoreStart
	 *
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> getForOutgoingInvoicesIgnoreStart(Date endDate,
			Customer customer, Locations location) throws BusinessException;

	/**
	 * Custom service resetSaleContract
	 *
	 * @return void
	 */
	public void resetSaleContract(Contract contract) throws BusinessException;

	/**
	 * Custom service putOnHoldFuelEvents
	 *
	 * @return void
	 */
	public void putOnHoldFuelEvents(List<FuelEvent> fuelEvents, String reason)
			throws BusinessException;

	/**
	 * Custom service releaseFuelEvents
	 *
	 * @return void
	 */
	public void releaseFuelEvents(List<FuelEvent> fuelEvents)
			throws BusinessException;

	/**
	 * Custom service export
	 *
	 * @return void
	 */
	public void export(FuelEvent e) throws BusinessException;

	/**
	 * Custom service export
	 *
	 * @return void
	 */
	public void export(List<FuelTicket> fuelTicketList)
			throws BusinessException;

	/**
	 * Custom service findByFuelTicket
	 *
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByFuelTicket(FuelTicket ticket)
			throws BusinessException;

	/**
	 * Custom service calculateVolume
	 *
	 * @return BigDecimal
	 */
	public BigDecimal calculateVolume(Date from, Date to, Unit unit)
			throws BusinessException;

	/**
	 * Custom service updateWithoutBusiness
	 *
	 * @return void
	 */
	public void updateWithoutBusiness(FuelEvent e) throws BusinessException;

	/**
	 * Custom service setBillOfLading
	 *
	 * @return void
	 */
	public void setBillOfLading(String bolNumber, FuelEvent fuelEvent)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FuelEvent
	 */
	public FuelEvent findByKey(Date fuelingDate, Customer customer,
			Locations departure, Aircraft aircraft, String flightNumber,
			Suffix suffix, String ticketNumber);

	/**
	 * Find by unique key
	 *
	 * @return FuelEvent
	 */
	public FuelEvent findByKey(Date fuelingDate, Long customerId,
			Long departureId, Long aircraftId, String flightNumber,
			Suffix suffix, String ticketNumber);

	/**
	 * Find by unique key
	 *
	 * @return FuelEvent
	 */
	public FuelEvent findByBusiness(Integer id);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: departure
	 *
	 * @param departure
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDeparture(Locations departure);

	/**
	 * Find by ID of reference: departure.id
	 *
	 * @param departureId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDepartureId(Integer departureId);

	/**
	 * Find by reference: aircraft
	 *
	 * @param aircraft
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByAircraft(Aircraft aircraft);

	/**
	 * Find by ID of reference: aircraft.id
	 *
	 * @param aircraftId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByAircraftId(Integer aircraftId);

	/**
	 * Find by reference: fuelSupplier
	 *
	 * @param fuelSupplier
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByFuelSupplier(Suppliers fuelSupplier);

	/**
	 * Find by ID of reference: fuelSupplier.id
	 *
	 * @param fuelSupplierId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByFuelSupplierId(Integer fuelSupplierId);

	/**
	 * Find by reference: iplAgent
	 *
	 * @param iplAgent
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByIplAgent(Suppliers iplAgent);

	/**
	 * Find by ID of reference: iplAgent.id
	 *
	 * @param iplAgentId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByIplAgentId(Integer iplAgentId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByUnitId(Integer unitId);

	/**
	 * Find by reference: payableCostCurrency
	 *
	 * @param payableCostCurrency
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByPayableCostCurrency(
			Currencies payableCostCurrency);

	/**
	 * Find by ID of reference: payableCostCurrency.id
	 *
	 * @param payableCostCurrencyId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByPayableCostCurrencyId(
			Integer payableCostCurrencyId);

	/**
	 * Find by reference: billableCostCurrency
	 *
	 * @param billableCostCurrency
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByBillableCostCurrency(
			Currencies billableCostCurrency);

	/**
	 * Find by ID of reference: billableCostCurrency.id
	 *
	 * @param billableCostCurrencyId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByBillableCostCurrencyId(
			Integer billableCostCurrencyId);

	/**
	 * Find by reference: netUpliftUnit
	 *
	 * @param netUpliftUnit
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByNetUpliftUnit(Unit netUpliftUnit);

	/**
	 * Find by ID of reference: netUpliftUnit.id
	 *
	 * @param netUpliftUnitId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByNetUpliftUnitId(Integer netUpliftUnitId);

	/**
	 * Find by reference: contractHolder
	 *
	 * @param contractHolder
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByContractHolder(Customer contractHolder);

	/**
	 * Find by ID of reference: contractHolder.id
	 *
	 * @param contractHolderId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByContractHolderId(Integer contractHolderId);

	/**
	 * Find by reference: shipTo
	 *
	 * @param shipTo
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByShipTo(Customer shipTo);

	/**
	 * Find by ID of reference: shipTo.id
	 *
	 * @param shipToId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByShipToId(Integer shipToId);

	/**
	 * Find by reference: densityUnit
	 *
	 * @param densityUnit
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDensityUnit(Unit densityUnit);

	/**
	 * Find by ID of reference: densityUnit.id
	 *
	 * @param densityUnitId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDensityUnitId(Integer densityUnitId);

	/**
	 * Find by reference: densityVolume
	 *
	 * @param densityVolume
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDensityVolume(Unit densityVolume);

	/**
	 * Find by ID of reference: densityVolume.id
	 *
	 * @param densityVolumeId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDensityVolumeId(Integer densityVolumeId);

	/**
	 * Find by reference: deliveryNotes
	 *
	 * @param deliveryNotes
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDeliveryNotes(DeliveryNote deliveryNotes);

	/**
	 * Find by ID of reference: deliveryNotes.id
	 *
	 * @param deliveryNotesId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDeliveryNotesId(Integer deliveryNotesId);

	/**
	 * Find by reference: payableAccruals
	 *
	 * @param payableAccruals
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByPayableAccruals(PayableAccrual payableAccruals);

	/**
	 * Find by ID of reference: payableAccruals.id
	 *
	 * @param payableAccrualsId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByPayableAccrualsId(Integer payableAccrualsId);

	/**
	 * Find by reference: receivableAccruals
	 *
	 * @param receivableAccruals
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByReceivableAccruals(
			ReceivableAccrual receivableAccruals);

	/**
	 * Find by ID of reference: receivableAccruals.id
	 *
	 * @param receivableAccrualsId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByReceivableAccrualsId(
			Integer receivableAccrualsId);

	/**
	 * Find by reference: details
	 *
	 * @param details
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDetails(FuelEventsDetails details);

	/**
	 * Find by ID of reference: details.id
	 *
	 * @param detailsId
	 * @return List<FuelEvent>
	 */
	public List<FuelEvent> findByDetailsId(Integer detailsId);
}
