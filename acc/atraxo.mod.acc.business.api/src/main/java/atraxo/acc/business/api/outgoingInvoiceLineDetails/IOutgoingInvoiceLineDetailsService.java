/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.business.api.outgoingInvoiceLineDetails;

import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link OutgoingInvoiceLineDetails} domain entity.
 */
public interface IOutgoingInvoiceLineDetailsService
		extends
			IEntityService<OutgoingInvoiceLineDetails> {

	/**
	 * Find by unique key
	 *
	 * @return OutgoingInvoiceLineDetails
	 */
	public OutgoingInvoiceLineDetails findByBusiness(Integer id);

	/**
	 * Find by reference: outgoingInvoiceLine
	 *
	 * @param outgoingInvoiceLine
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOutgoingInvoiceLine(
			OutgoingInvoiceLine outgoingInvoiceLine);

	/**
	 * Find by ID of reference: outgoingInvoiceLine.id
	 *
	 * @param outgoingInvoiceLineId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOutgoingInvoiceLineId(
			Integer outgoingInvoiceLineId);

	/**
	 * Find by reference: outgoingInvoice
	 *
	 * @param outgoingInvoice
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOutgoingInvoice(
			OutgoingInvoice outgoingInvoice);

	/**
	 * Find by ID of reference: outgoingInvoice.id
	 *
	 * @param outgoingInvoiceId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOutgoingInvoiceId(
			Integer outgoingInvoiceId);

	/**
	 * Find by reference: priceCategory
	 *
	 * @param priceCategory
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByPriceCategory(
			PriceCategory priceCategory);

	/**
	 * Find by ID of reference: priceCategory.id
	 *
	 * @param priceCategoryId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByPriceCategoryId(
			Integer priceCategoryId);

	/**
	 * Find by reference: mainCategory
	 *
	 * @param mainCategory
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByMainCategory(
			MainCategory mainCategory);

	/**
	 * Find by ID of reference: mainCategory.id
	 *
	 * @param mainCategoryId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByMainCategoryId(
			Integer mainCategoryId);

	/**
	 * Find by reference: originalPriceCurrency
	 *
	 * @param originalPriceCurrency
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOriginalPriceCurrency(
			Currencies originalPriceCurrency);

	/**
	 * Find by ID of reference: originalPriceCurrency.id
	 *
	 * @param originalPriceCurrencyId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOriginalPriceCurrencyId(
			Integer originalPriceCurrencyId);

	/**
	 * Find by reference: originalPriceUnit
	 *
	 * @param originalPriceUnit
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOriginalPriceUnit(
			Unit originalPriceUnit);

	/**
	 * Find by ID of reference: originalPriceUnit.id
	 *
	 * @param originalPriceUnitId
	 * @return List<OutgoingInvoiceLineDetails>
	 */
	public List<OutgoingInvoiceLineDetails> findByOriginalPriceUnitId(
			Integer originalPriceUnitId);
}
