/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelEventTransmissionStatus {

	_EMPTY_("", ""), _NEW_("New", "New"), _IN_PROGRESS_("In progress",
			"In progress"), _TRANSMITTED_("Transmitted", "Transmitted"), _FAILED_(
			"Failed", "Failed");

	private String name;
	private String interfaceName;

	private FuelEventTransmissionStatus(String name, String interfaceName) {
		this.name = name;
		this.interfaceName = interfaceName;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelEventTransmissionStatus getByName(String name) {
		for (FuelEventTransmissionStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelEventTransmissionStatus with name: " + name);
	}

	public String getInterfaceName() {
		return this.interfaceName;
	}

	public static FuelEventTransmissionStatus getByInterfaceName(String code) {
		for (FuelEventTransmissionStatus status : values()) {
			if (status.getInterfaceName().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelEventTransmissionStatus with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
