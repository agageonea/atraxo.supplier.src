/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum CheckResult {

	_EMPTY_(""), _NO_DISCREPANCIES_("No discrepancies"), _IN_TOLERANCE_(
			"In tolerance"), _OUT_TOLERANCE_("Out tolerance");

	private String name;

	private CheckResult(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static CheckResult getByName(String name) {
		for (CheckResult status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent CheckResult with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
