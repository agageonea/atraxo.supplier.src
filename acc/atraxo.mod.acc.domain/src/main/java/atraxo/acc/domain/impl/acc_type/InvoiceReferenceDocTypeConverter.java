/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceReferenceDocType} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceReferenceDocTypeConverter
		implements
			AttributeConverter<InvoiceReferenceDocType, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceReferenceDocType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null InvoiceReferenceDocType.");
		}
		return value.getName();
	}

	@Override
	public InvoiceReferenceDocType convertToEntityAttribute(String value) {
		return InvoiceReferenceDocType.getByName(value);
	}

}
