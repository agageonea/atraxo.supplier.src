/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceTransmissionStatus {

	_EMPTY_(""), _NEW_("New"), _IN_PROGRESS_("In progress"), _FAILED_("Failed"), _PARTIALLY_TRANSMITTED_(
			"Partially transmitted"), _TRANSMITTED_("Transmitted");

	private String name;

	private InvoiceTransmissionStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceTransmissionStatus getByName(String name) {
		for (InvoiceTransmissionStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InvoiceTransmissionStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
