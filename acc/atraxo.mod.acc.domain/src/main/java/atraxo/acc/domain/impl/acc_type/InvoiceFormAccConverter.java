/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceFormAcc} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceFormAccConverter
		implements
			AttributeConverter<InvoiceFormAcc, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceFormAcc value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null InvoiceFormAcc.");
		}
		return value.getName();
	}

	@Override
	public InvoiceFormAcc convertToEntityAttribute(String value) {
		return InvoiceFormAcc.getByName(value);
	}

}
