/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum CheckStatus {

	_EMPTY_(""), _OK_("OK"), _FAILED_("Failed"), _NOT_CHECKED_("Not checked"), _NEEDS_RECHECK_(
			"Needs recheck");

	private String name;

	private CheckStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static CheckStatus getByName(String name) {
		for (CheckStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent CheckStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
