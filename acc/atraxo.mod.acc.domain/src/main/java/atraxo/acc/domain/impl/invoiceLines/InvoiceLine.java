/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.invoiceLines;

import atraxo.acc.domain.impl.acc_type.CheckResult;
import atraxo.acc.domain.impl.acc_type.CheckResultConverter;
import atraxo.acc.domain.impl.acc_type.CheckStatus;
import atraxo.acc.domain.impl.acc_type.CheckStatusConverter;
import atraxo.acc.domain.impl.acc_type.InvoiceLinesTaxType;
import atraxo.acc.domain.impl.acc_type.InvoiceLinesTaxTypeConverter;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.acc.domain.impl.invoices.Invoice;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicatorConverter;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.ops_type.Suffix;
import atraxo.ops.domain.impl.ops_type.SuffixConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link InvoiceLine} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = InvoiceLine.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM InvoiceLine e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = InvoiceLine.TABLE_NAME)
public class InvoiceLine extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "acc_INVOICE_LINES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "InvoiceLine.findByBusiness";

	@Column(name = "invoice_line", precision = 11)
	private Integer invoiceLine;

	@Temporal(TemporalType.DATE)
	@Column(name = "event_date")
	private Date eventDate;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "delivery_date", nullable = false)
	private Date deliveryDate;

	@Column(name = "ticket_no", length = 32)
	private String ticketNo;

	@NotNull
	@Column(name = "quantity", nullable = false, precision = 19, scale = 6)
	private BigDecimal quantity;

	@Column(name = "amount", precision = 19, scale = 6)
	private BigDecimal amount;

	@Column(name = "vat", precision = 19, scale = 6)
	private BigDecimal vat;

	@Column(name = "netquantity", precision = 19, scale = 6)
	private BigDecimal netQuantity;

	@Column(name = "grossquantity", precision = 19, scale = 6)
	private BigDecimal grossQuantity;

	@Column(name = "calculated_vat", precision = 19, scale = 6)
	private BigDecimal calculatedVat;

	@Column(name = "invoiced_vat", precision = 19, scale = 6)
	private BigDecimal invoicedVat;

	@Column(name = "tax_type", length = 32)
	@Convert(converter = InvoiceLinesTaxTypeConverter.class)
	private InvoiceLinesTaxType taxType;

	@Column(name = "event_check_status", length = 32)
	@Convert(converter = CheckStatusConverter.class)
	private CheckStatus eventCheckStatus;

	@Column(name = "quantity_check_result", length = 32)
	@Convert(converter = CheckResultConverter.class)
	private CheckResult quantityCheckResult;

	@Column(name = "quantity_check_status", length = 32)
	@Convert(converter = CheckStatusConverter.class)
	private CheckStatus quantityCheckStatus;

	@Column(name = "original_quantity_check_result", length = 32)
	@Convert(converter = CheckStatusConverter.class)
	private CheckStatus originalQuantityCheckStatus;

	@Column(name = "vat_check_status", length = 32)
	@Convert(converter = CheckStatusConverter.class)
	private CheckStatus vatCheckStatus;

	@Column(name = "vat_check_result", length = 32)
	@Convert(converter = CheckResultConverter.class)
	private CheckResult vatCheckResult;

	@Column(name = "original_vatcheck_status", length = 32)
	@Convert(converter = CheckStatusConverter.class)
	private CheckStatus originalVatCheckStatus;

	@Column(name = "overall_check_status", length = 32)
	@Convert(converter = CheckStatusConverter.class)
	private CheckStatus overAllCheckStatus;

	@Column(name = "flight_string", length = 32)
	private String flightString;

	@Column(name = "flight_no", length = 4)
	private String flightNo;

	@Column(name = "suffix", length = 16)
	@Convert(converter = SuffixConverter.class)
	private Suffix suffix;

	@Column(name = "aircraft_registration_no", length = 10)
	private String aircraftRegistrationNumber;

	@Column(name = "flight_type", length = 16)
	@Convert(converter = FlightTypeIndicatorConverter.class)
	private FlightTypeIndicator flightType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Invoice.class)
	@JoinColumn(name = "invoice_id", referencedColumnName = "id")
	private Invoice invoice;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "netquantity_unit_id", referencedColumnName = "id")
	private Unit netQuantityUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "grossquantity_unit_id", referencedColumnName = "id")
	private Unit grossQuantityUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contract;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "destination_id", referencedColumnName = "id")
	private Locations destination;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = InvoiceLine.class)
	@JoinColumn(name = "reference_invoice_item_id", referencedColumnName = "id")
	private InvoiceLine referenceInvoiceLine;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "airline_designator_id", referencedColumnName = "id")
	private Customer airlineDesignator;

	public Integer getInvoiceLine() {
		return this.invoiceLine;
	}

	public void setInvoiceLine(Integer invoiceLine) {
		this.invoiceLine = invoiceLine;
	}

	public Date getEventDate() {
		return this.eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getTicketNo() {
		return this.ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getVat() {
		return this.vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public BigDecimal getNetQuantity() {
		return this.netQuantity;
	}

	public void setNetQuantity(BigDecimal netQuantity) {
		this.netQuantity = netQuantity;
	}

	public BigDecimal getGrossQuantity() {
		return this.grossQuantity;
	}

	public void setGrossQuantity(BigDecimal grossQuantity) {
		this.grossQuantity = grossQuantity;
	}

	public BigDecimal getCalculatedVat() {
		return this.calculatedVat;
	}

	public void setCalculatedVat(BigDecimal calculatedVat) {
		this.calculatedVat = calculatedVat;
	}

	public BigDecimal getInvoicedVat() {
		return this.invoicedVat;
	}

	public void setInvoicedVat(BigDecimal invoicedVat) {
		this.invoicedVat = invoicedVat;
	}

	public InvoiceLinesTaxType getTaxType() {
		return this.taxType;
	}

	public void setTaxType(InvoiceLinesTaxType taxType) {
		this.taxType = taxType;
	}

	public CheckStatus getEventCheckStatus() {
		return this.eventCheckStatus;
	}

	public void setEventCheckStatus(CheckStatus eventCheckStatus) {
		this.eventCheckStatus = eventCheckStatus;
	}

	public CheckResult getQuantityCheckResult() {
		return this.quantityCheckResult;
	}

	public void setQuantityCheckResult(CheckResult quantityCheckResult) {
		this.quantityCheckResult = quantityCheckResult;
	}

	public CheckStatus getQuantityCheckStatus() {
		return this.quantityCheckStatus;
	}

	public void setQuantityCheckStatus(CheckStatus quantityCheckStatus) {
		this.quantityCheckStatus = quantityCheckStatus;
	}

	public CheckStatus getOriginalQuantityCheckStatus() {
		return this.originalQuantityCheckStatus;
	}

	public void setOriginalQuantityCheckStatus(
			CheckStatus originalQuantityCheckStatus) {
		this.originalQuantityCheckStatus = originalQuantityCheckStatus;
	}

	public CheckStatus getVatCheckStatus() {
		return this.vatCheckStatus;
	}

	public void setVatCheckStatus(CheckStatus vatCheckStatus) {
		this.vatCheckStatus = vatCheckStatus;
	}

	public CheckResult getVatCheckResult() {
		return this.vatCheckResult;
	}

	public void setVatCheckResult(CheckResult vatCheckResult) {
		this.vatCheckResult = vatCheckResult;
	}

	public CheckStatus getOriginalVatCheckStatus() {
		return this.originalVatCheckStatus;
	}

	public void setOriginalVatCheckStatus(CheckStatus originalVatCheckStatus) {
		this.originalVatCheckStatus = originalVatCheckStatus;
	}

	public CheckStatus getOverAllCheckStatus() {
		return this.overAllCheckStatus;
	}

	public void setOverAllCheckStatus(CheckStatus overAllCheckStatus) {
		this.overAllCheckStatus = overAllCheckStatus;
	}

	public String getFlightString() {
		return this.flightString;
	}

	public void setFlightString(String flightString) {
		this.flightString = flightString;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public String getAircraftRegistrationNumber() {
		return this.aircraftRegistrationNumber;
	}

	public void setAircraftRegistrationNumber(String aircraftRegistrationNumber) {
		this.aircraftRegistrationNumber = aircraftRegistrationNumber;
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}

	public Invoice getInvoice() {
		return this.invoice;
	}

	public void setInvoice(Invoice invoice) {
		if (invoice != null) {
			this.__validate_client_context__(invoice.getClientId());
		}
		this.invoice = invoice;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public Unit getNetQuantityUnit() {
		return this.netQuantityUnit;
	}

	public void setNetQuantityUnit(Unit netQuantityUnit) {
		if (netQuantityUnit != null) {
			this.__validate_client_context__(netQuantityUnit.getClientId());
		}
		this.netQuantityUnit = netQuantityUnit;
	}
	public Unit getGrossQuantityUnit() {
		return this.grossQuantityUnit;
	}

	public void setGrossQuantityUnit(Unit grossQuantityUnit) {
		if (grossQuantityUnit != null) {
			this.__validate_client_context__(grossQuantityUnit.getClientId());
		}
		this.grossQuantityUnit = grossQuantityUnit;
	}
	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		if (contract != null) {
			this.__validate_client_context__(contract.getClientId());
		}
		this.contract = contract;
	}
	public Locations getDestination() {
		return this.destination;
	}

	public void setDestination(Locations destination) {
		if (destination != null) {
			this.__validate_client_context__(destination.getClientId());
		}
		this.destination = destination;
	}
	public InvoiceLine getReferenceInvoiceLine() {
		return this.referenceInvoiceLine;
	}

	public void setReferenceInvoiceLine(InvoiceLine referenceInvoiceLine) {
		if (referenceInvoiceLine != null) {
			this.__validate_client_context__(referenceInvoiceLine.getClientId());
		}
		this.referenceInvoiceLine = referenceInvoiceLine;
	}
	public Customer getAirlineDesignator() {
		return this.airlineDesignator;
	}

	public void setAirlineDesignator(Customer airlineDesignator) {
		if (airlineDesignator != null) {
			this.__validate_client_context__(airlineDesignator.getClientId());
		}
		this.airlineDesignator = airlineDesignator;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
