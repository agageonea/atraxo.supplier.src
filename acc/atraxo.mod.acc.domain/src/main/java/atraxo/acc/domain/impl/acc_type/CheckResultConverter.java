/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link CheckResult} enum.
 * Generated code. Do not modify in this file.
 */
public class CheckResultConverter
		implements
			AttributeConverter<CheckResult, String> {

	@Override
	public String convertToDatabaseColumn(CheckResult value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null CheckResult.");
		}
		return value.getName();
	}

	@Override
	public CheckResult convertToEntityAttribute(String value) {
		return CheckResult.getByName(value);
	}

}
