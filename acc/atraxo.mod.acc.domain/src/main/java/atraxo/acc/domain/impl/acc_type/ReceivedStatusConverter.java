/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ReceivedStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class ReceivedStatusConverter
		implements
			AttributeConverter<ReceivedStatus, String> {

	@Override
	public String convertToDatabaseColumn(ReceivedStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ReceivedStatus.");
		}
		return value.getName();
	}

	@Override
	public ReceivedStatus convertToEntityAttribute(String value) {
		return ReceivedStatus.getByName(value);
	}

}
