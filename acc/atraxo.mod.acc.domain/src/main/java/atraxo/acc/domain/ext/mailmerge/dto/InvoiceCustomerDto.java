package atraxo.acc.domain.ext.mailmerge.dto;

import org.codehaus.jackson.annotate.JsonProperty;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;
import atraxo.fmbas.domain.impl.customer.Customer;
import seava.j4e.api.annotation.Dto;
import seava.j4e.api.annotation.DtoField;

@Dto(entity = Customer.class)
public class InvoiceCustomerDto extends AbstractDataDto {

	private static final long serialVersionUID = 5711509837550741912L;

	@DtoField
	@JsonProperty(value = "Code")
	private String code;

	@DtoField
	@JsonProperty(value = "Name")
	private String name;

	@DtoField
	@JsonProperty(value = "ERP_Code")
	private String accountNumber;

	@DtoField
	@JsonProperty(value = "Office_Street")
	private String officeStreet;

	@DtoField
	@JsonProperty(value = "Office_City")
	private String officeCity;

	@DtoField
	@JsonProperty(value = "Office_Postal_Code")
	private String officePostalCode;

	@JsonProperty(value = "Office_Country")
	@DtoField(path = "officeCountry.name")
	private String officeCountryName;

	@DtoField
	@JsonProperty(value = "Phone_Number")
	private String phoneNo;

	@DtoField
	@JsonProperty(value = "Email")
	private String email;

	@DtoField
	@JsonProperty(value = "Tax_ID_Number")
	private String taxIdNumber;

	@DtoField
	@JsonProperty(value = "Vat_Reg_Number")
	private String vatRegNumber;

	@DtoField
	@JsonProperty(value = "Income_Tax_Number")
	private String incomeTaxNumber;

	@JsonProperty(value = "Bank_Name")
	private String bankName;

	@JsonProperty(value = "Bank_Address")
	private String bankAddress;

	@JsonProperty(value = "Bank_Account_Holder")
	private String bankAccountHolder;

	@JsonProperty(value = "Bank_Account_No")
	private String bankAccountNr;

	@JsonProperty(value = "Bank_Iban_No")
	private String bankIbanNr;

	@JsonProperty(value = "Bank_Swift_No")
	private String bankSwiftNr;

	@JsonProperty(value = "Bank_Account_Currency")
	private String bankAccountCurrency;

	public InvoiceCustomerDto() {
		super();
	}

	@Override
	public void setDefaultValues() {
		if (this.taxIdNumber == null || this.taxIdNumber.isEmpty()) {
			this.taxIdNumber = "-";
		}

		if (this.vatRegNumber == null || this.vatRegNumber.isEmpty()) {
			this.vatRegNumber = "-";
		}

		if (this.incomeTaxNumber == null || this.incomeTaxNumber.isEmpty()) {
			this.incomeTaxNumber = "-";
		}
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getOfficeStreet() {
		return this.officeStreet;
	}

	public void setOfficeStreet(String officeStreet) {
		this.officeStreet = officeStreet;
	}

	public String getOfficeCity() {
		return this.officeCity;
	}

	public void setOfficeCity(String officeCity) {
		this.officeCity = officeCity;
	}

	public String getOfficePostalCode() {
		return this.officePostalCode;
	}

	public void setOfficePostalCode(String officePostalCode) {
		this.officePostalCode = officePostalCode;
	}

	public String getOfficeCountryName() {
		return this.officeCountryName;
	}

	public void setOfficeCountryName(String officeCountryName) {
		this.officeCountryName = officeCountryName;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTaxIdNumber() {
		return this.taxIdNumber;
	}

	public void setTaxIdNumber(String taxIdNumber) {
		this.taxIdNumber = taxIdNumber;
	}

	public String getVatRegNumber() {
		return this.vatRegNumber;
	}

	public void setVatRegNumber(String vatRegNumber) {
		this.vatRegNumber = vatRegNumber;
	}

	public String getIncomeTaxNumber() {
		return this.incomeTaxNumber;
	}

	public void setIncomeTaxNumber(String incomeTaxNumber) {
		this.incomeTaxNumber = incomeTaxNumber;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAddress() {
		return this.bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public String getBankAccountHolder() {
		return this.bankAccountHolder;
	}

	public void setBankAccountHolder(String bankAccountHolder) {
		this.bankAccountHolder = bankAccountHolder;
	}

	public String getBankAccountNr() {
		return this.bankAccountNr;
	}

	public void setBankAccountNr(String bankAccountNr) {
		this.bankAccountNr = bankAccountNr;
	}

	public String getBankIbanNr() {
		return this.bankIbanNr;
	}

	public void setBankIbanNr(String bankIbanNr) {
		this.bankIbanNr = bankIbanNr;
	}

	public String getBankSwiftNr() {
		return this.bankSwiftNr;
	}

	public void setBankSwiftNr(String bankSwiftNr) {
		this.bankSwiftNr = bankSwiftNr;
	}

	public String getBankAccountCurrency() {
		return this.bankAccountCurrency;
	}

	public void setBankAccountCurrency(String bankAccountCurrency) {
		this.bankAccountCurrency = bankAccountCurrency;
	}

}
