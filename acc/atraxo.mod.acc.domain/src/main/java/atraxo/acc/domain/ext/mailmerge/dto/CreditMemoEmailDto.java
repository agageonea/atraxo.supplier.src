/**
 *
 */
package atraxo.acc.domain.ext.mailmerge.dto;

import java.math.BigDecimal;
import java.util.Date;

import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;
import seava.j4e.api.annotation.Dto;
import seava.j4e.api.annotation.DtoField;

/**
 * @author vhojda
 */
@Dto(entity = OutgoingInvoice.class)
public class CreditMemoEmailDto extends AbstractDataDto {

	private static final long serialVersionUID = 6653995846152766102L;

	@DtoField(path = "user.title.name")
	private String title;

	private String name;

	@DtoField(path = "invoiceNo")
	private String invoiceNumber;

	@DtoField(path = "invoiceDate")
	private Date invoiceDate;

	@DtoField(path = "deliveryLoc.name")
	private String invoiceLocation;

	@DtoField(path = "issuer.name")
	private String invoiceIssuer;

	@DtoField(path = "receiver.name")
	private String invoiceReceiver;

	@DtoField(path = "unit.code")
	private String invoiceUnit;

	@DtoField(path = "quantity")
	private BigDecimal invoiceQuantity;

	@DtoField(path = "netAmount")
	private BigDecimal invoiceNetAmount;

	@DtoField(path = "creditMemoReason.name")
	private String invoiceCreditMemoReason;

	private String fullnameOfRequester;

	private String approveNote;

	public CreditMemoEmailDto() {
		// default constructor
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceLocation() {
		return this.invoiceLocation;
	}

	public void setInvoiceLocation(String invoiceLocation) {
		this.invoiceLocation = invoiceLocation;
	}

	public String getInvoiceIssuer() {
		return this.invoiceIssuer;
	}

	public void setInvoiceIssuer(String invoiceIssuer) {
		this.invoiceIssuer = invoiceIssuer;
	}

	public String getInvoiceReceiver() {
		return this.invoiceReceiver;
	}

	public void setInvoiceReceiver(String invoiceReceiver) {
		this.invoiceReceiver = invoiceReceiver;
	}

	public String getApproveNote() {
		return this.approveNote;
	}

	public void setApproveNote(String approveNote) {
		this.approveNote = approveNote;
	}

	public String getFullnameOfRequester() {
		return this.fullnameOfRequester;
	}

	public void setFullnameOfRequester(String fullnameOfRequester) {
		this.fullnameOfRequester = fullnameOfRequester;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInvoiceUnit() {
		return this.invoiceUnit;
	}

	public void setInvoiceUnit(String invoiceUnit) {
		this.invoiceUnit = invoiceUnit;
	}

	public BigDecimal getInvoiceQuantity() {
		return this.invoiceQuantity;
	}

	public void setInvoiceQuantity(BigDecimal invoiceQuantity) {
		this.invoiceQuantity = invoiceQuantity;
	}

	public String getInvoiceCreditMemoReason() {
		return this.invoiceCreditMemoReason;
	}

	public void setInvoiceCreditMemoReason(String invoiceCreditMemoReason) {
		this.invoiceCreditMemoReason = invoiceCreditMemoReason;
	}

	@Override
	public void setDefaultValues() {

	}

	public BigDecimal getInvoiceNetAmount() {
		return this.invoiceNetAmount;
	}

	public void setInvoiceNetAmount(BigDecimal invoiceNetAmount) {
		this.invoiceNetAmount = invoiceNetAmount;
	}

}
