package atraxo.acc.domain.ext.invoices;

public class GeneratorResponse {

	private int nrInvoicesGenerated;
	private String errorMsg;

	public GeneratorResponse(int nrInvoicesGenerated, String errorMsg) {
		this.nrInvoicesGenerated = nrInvoicesGenerated;
		this.errorMsg = errorMsg;
	}

	public int getNrInvoicesGenerated() {
		return this.nrInvoicesGenerated;
	}

	public void setNrInvoicesGenerated(int nrInvoicesGenerated) {
		this.nrInvoicesGenerated = nrInvoicesGenerated;
	}

	public String getErrorMsg() {
		return this.errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}
