/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum QuantitySource {

	_EMPTY_(""), _FUEL_ORDER_("Fuel order"), _FUEL_TICKET_("Fuel ticket"), _INVOICE_(
			"Invoice");

	private String name;

	private QuantitySource(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static QuantitySource getByName(String name) {
		for (QuantitySource status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent QuantitySource with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
