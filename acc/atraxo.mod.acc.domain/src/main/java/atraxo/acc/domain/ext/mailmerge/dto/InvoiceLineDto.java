package atraxo.acc.domain.ext.mailmerge.dto;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;
import atraxo.ops.domain.impl.ops_type.Suffix;
import seava.j4e.api.annotation.Dto;
import seava.j4e.api.annotation.DtoField;

@Dto(entity = OutgoingInvoiceLine.class)
@JsonRootName(value = "InvoiceLine")
public class InvoiceLineDto extends AbstractDataDto {

	private static final long serialVersionUID = 7161837333269972440L;

	@DtoField
	@JsonProperty(value = "Delivery_Date")
	private Date deliveryDate;

	@DtoField(path = "customer.iataCode")
	@JsonProperty(value = "ShipTo_IATA_Code")
	private String shipToIataCode;

	@DtoField(path = "customer.name")
	@JsonProperty(value = "ShipTo_Name")
	private String shipToName;

	@DtoField
	@JsonProperty(value = "Aircraft")
	private String aircraftRegistrationNumber;

	@DtoField
	@JsonProperty(value = "Flight_Number")
	private String flightNo;

	@DtoField
	@JsonProperty(value = "Flight_Sufix")
	private Suffix flightSuffix;

	@DtoField
	@JsonProperty(value = "Ticket_Number")
	private String ticketNo;

	@DtoField
	@JsonProperty(value = "Event_Type")
	private FlightTypeIndicator flightType;

	@DtoField
	@JsonProperty(value = "Quatity")
	private BigDecimal quantity;

	@DtoField(path = "outgoingInvoice.unit.code")
	@JsonProperty(value = "UoM")
	private String unitCode;

	@DtoField
	@JsonProperty(value = "Amount")
	private BigDecimal amount;

	@DtoField(path = "outgoingInvoice.currency.code")
	@JsonProperty(value = "Currency")
	private String currencyCode;

	@DtoField
	@JsonProperty(value = "Bill_of_Lading")
	private String bolNumber;

	@DtoField(path = "customer")
	@JsonProperty(value = "ShipTo")
	private InvoiceCustomerDto customer;

	@DtoField
	@JsonProperty(value = "InvoiceLineDetail")
	private Collection<InvoiceLineDetailDto> details;

	@JsonProperty(value = "Invoice_Line_Details_Count")
	private BigDecimal detailsLineCount;

	public InvoiceLineDto() {
		super();
	}

	@Override
	public void setDefaultValues() {
		this.detailsLineCount = this.details != null && !this.details.isEmpty() ? new BigDecimal(this.details.size()) : BigDecimal.ZERO;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getShipToIataCode() {
		return this.shipToIataCode;
	}

	public void setShipToIataCode(String shipToIataCode) {
		this.shipToIataCode = shipToIataCode;
	}

	public String getShipToName() {
		return this.shipToName;
	}

	public void setShipToName(String shipToName) {
		this.shipToName = shipToName;
	}

	public String getAircraftRegistrationNumber() {
		return this.aircraftRegistrationNumber;
	}

	public void setAircraftRegistrationNumber(String aircraftRegistrationNumber) {
		this.aircraftRegistrationNumber = aircraftRegistrationNumber;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Suffix getFlightSuffix() {
		return this.flightSuffix;
	}

	public void setFlightSuffix(Suffix flightSuffix) {
		this.flightSuffix = flightSuffix;
	}

	public String getTicketNo() {
		return this.ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getBolNumber() {
		return this.bolNumber;
	}

	public void setBolNumber(String bolNumber) {
		this.bolNumber = bolNumber;
	}

	public BigDecimal getDetailsLineCount() {
		return this.detailsLineCount;
	}

	public void setDetailsLineCount(BigDecimal detailsLineCount) {
		this.detailsLineCount = detailsLineCount;
	}

	public InvoiceCustomerDto getCustomer() {
		return this.customer;
	}

	public void setCustomer(InvoiceCustomerDto customer) {
		this.customer = customer;
	}

	public Collection<InvoiceLineDetailDto> getDetails() {
		return this.details;
	}

	public void setDetails(Collection<InvoiceLineDetailDto> details) {
		this.details = details;
	}
}
