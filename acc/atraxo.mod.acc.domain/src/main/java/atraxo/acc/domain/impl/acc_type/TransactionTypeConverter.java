/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TransactionType} enum.
 * Generated code. Do not modify in this file.
 */
public class TransactionTypeConverter
		implements
			AttributeConverter<TransactionType, String> {

	@Override
	public String convertToDatabaseColumn(TransactionType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TransactionType.");
		}
		return value.getName();
	}

	@Override
	public TransactionType convertToEntityAttribute(String value) {
		return TransactionType.getByName(value);
	}

}
