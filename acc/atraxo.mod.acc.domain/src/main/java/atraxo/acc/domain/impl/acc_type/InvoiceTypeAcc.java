/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceTypeAcc {

	_EMPTY_(""), _INV_("Invoice"), _CRN_("Credit memo");

	private String name;

	private InvoiceTypeAcc(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceTypeAcc getByName(String name) {
		for (InvoiceTypeAcc status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent InvoiceTypeAcc with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
