/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelEventTransmissionStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelEventTransmissionStatusConverter
		implements
			AttributeConverter<FuelEventTransmissionStatus, String> {

	@Override
	public String convertToDatabaseColumn(FuelEventTransmissionStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null FuelEventTransmissionStatus.");
		}
		return value.getName();
	}

	@Override
	public FuelEventTransmissionStatus convertToEntityAttribute(String value) {
		return FuelEventTransmissionStatus.getByName(value);
	}

}
