/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ControlNo {

	_EMPTY_(""), _NOT_AVAILABLE_("Not available"), _WAITING_FOR_("Waiting for");

	private String name;

	private ControlNo(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ControlNo getByName(String name) {
		for (ControlNo status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ControlNo with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
