/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.invoices;

import atraxo.acc.domain.impl.acc_type.InvoiceCategory;
import atraxo.acc.domain.impl.acc_type.InvoiceCategoryConverter;
import atraxo.acc.domain.impl.acc_type.InvoiceFormAcc;
import atraxo.acc.domain.impl.acc_type.InvoiceFormAccConverter;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceType;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceTypeConverter;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAccConverter;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import atraxo.acc.domain.impl.acc_type.TransactionTypeConverter;
import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.DealTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TaxTypeConverter;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link Invoice} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = Invoice.NQ_FIND_BY_KEY, query = "SELECT e FROM Invoice e WHERE e.clientId = :clientId and e.issuer = :issuer and e.issueDate = :issueDate and e.invoiceNo = :invoiceNo and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Invoice.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM Invoice e WHERE e.clientId = :clientId and e.issuer.id = :issuerId and e.issueDate = :issueDate and e.invoiceNo = :invoiceNo and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = Invoice.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM Invoice e WHERE e.clientId = :clientId and e.id = :id and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = Invoice.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = Invoice.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "issuer_id", "issue_date",
		"invoice_no"})})
public class Invoice extends AbstractSubsidiary implements Serializable {

	public static final String TABLE_NAME = "acc_INVOICES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "Invoice.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "Invoice.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "Invoice.findByBusiness";

	@Column(name = "deal_type", length = 32)
	@Convert(converter = DealTypeConverter.class)
	private DealType dealType;

	@Column(name = "invoice_type", length = 32)
	@Convert(converter = InvoiceTypeAccConverter.class)
	private InvoiceTypeAcc invoiceType;

	@NotBlank
	@Column(name = "invoice_no", nullable = false, length = 32)
	private String invoiceNo;

	@Temporal(TemporalType.DATE)
	@Column(name = "issue_date")
	private Date issueDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "receiving_date")
	private Date receivingDate;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_date_from", nullable = false)
	private Date deliveryDateFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_date_to")
	private Date deliverydateTo;

	@NotBlank
	@Column(name = "transaction_type", nullable = false, length = 32)
	@Convert(converter = TransactionTypeConverter.class)
	private TransactionType transactionType;

	@NotBlank
	@Column(name = "invoice_form", nullable = false, length = 32)
	@Convert(converter = InvoiceFormAccConverter.class)
	private InvoiceFormAcc invoiceForm;

	@NotBlank
	@Column(name = "category", nullable = false, length = 32)
	@Convert(converter = InvoiceCategoryConverter.class)
	private InvoiceCategory category;

	@Column(name = "reference_type", length = 32)
	@Convert(converter = InvoiceReferenceTypeConverter.class)
	private InvoiceReferenceType referenceType;

	@Column(name = "reference_value", length = 32)
	private String referenceValue;

	@NotNull
	@Column(name = "total_amount", nullable = false, precision = 19, scale = 6)
	private BigDecimal totalAmount;

	@Column(name = "vat_amount", precision = 19, scale = 6)
	private BigDecimal vatAmount;

	@Column(name = "net_amount", precision = 19, scale = 6)
	private BigDecimal netAmount;

	@NotNull
	@Column(name = "quantity", nullable = false, precision = 19, scale = 6)
	private BigDecimal quantity;

	@Temporal(TemporalType.DATE)
	@Column(name = "issuerbase_line_date")
	private Date issuerBaselinedate;

	@Temporal(TemporalType.DATE)
	@Column(name = "base_line_date")
	private Date baseLineDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "calculated_base_line_date")
	private Date calculatedBaseLineDate;

	@Column(name = "items_no", precision = 11)
	private Integer itemsNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "closing_date")
	private Date closingDate;

	@Column(name = "tax_type", length = 32)
	@Convert(converter = TaxTypeConverter.class)
	private TaxType taxType;

	@NotBlank
	@Column(name = "status", nullable = false, length = 32)
	@Convert(converter = BillStatusConverter.class)
	private BillStatus status;

	@Column(name = "erp_reference", length = 32)
	private String erpReference;

	@Column(name = "check_result")
	private Boolean checkResult;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "receiver_id", referencedColumnName = "id")
	private Customer receiver;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "delivery_location_id", referencedColumnName = "id")
	private Locations deliveryLoc;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Country.class)
	@JoinColumn(name = "country_id", referencedColumnName = "id")
	private Country country;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contract;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currencies currency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "issuer_id", referencedColumnName = "id")
	private Suppliers issuer;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = InvoiceLine.class, mappedBy = "invoice", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<InvoiceLine> invoiceLines;

	public DealType getDealType() {
		return this.dealType;
	}

	public void setDealType(DealType dealType) {
		this.dealType = dealType;
	}

	public InvoiceTypeAcc getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceTypeAcc invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getIssueDate() {
		return this.issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getReceivingDate() {
		return this.receivingDate;
	}

	public void setReceivingDate(Date receivingDate) {
		this.receivingDate = receivingDate;
	}

	public Date getDeliveryDateFrom() {
		return this.deliveryDateFrom;
	}

	public void setDeliveryDateFrom(Date deliveryDateFrom) {
		this.deliveryDateFrom = deliveryDateFrom;
	}

	public Date getDeliverydateTo() {
		return this.deliverydateTo;
	}

	public void setDeliverydateTo(Date deliverydateTo) {
		this.deliverydateTo = deliverydateTo;
	}

	public TransactionType getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public InvoiceFormAcc getInvoiceForm() {
		return this.invoiceForm;
	}

	public void setInvoiceForm(InvoiceFormAcc invoiceForm) {
		this.invoiceForm = invoiceForm;
	}

	public InvoiceCategory getCategory() {
		return this.category;
	}

	public void setCategory(InvoiceCategory category) {
		this.category = category;
	}

	public InvoiceReferenceType getReferenceType() {
		return this.referenceType;
	}

	public void setReferenceType(InvoiceReferenceType referenceType) {
		this.referenceType = referenceType;
	}

	public String getReferenceValue() {
		return this.referenceValue;
	}

	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}

	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Date getIssuerBaselinedate() {
		return this.issuerBaselinedate;
	}

	public void setIssuerBaselinedate(Date issuerBaselinedate) {
		this.issuerBaselinedate = issuerBaselinedate;
	}

	public Date getBaseLineDate() {
		return this.baseLineDate;
	}

	public void setBaseLineDate(Date baseLineDate) {
		this.baseLineDate = baseLineDate;
	}

	public Date getCalculatedBaseLineDate() {
		return this.calculatedBaseLineDate;
	}

	public void setCalculatedBaseLineDate(Date calculatedBaseLineDate) {
		this.calculatedBaseLineDate = calculatedBaseLineDate;
	}

	public Integer getItemsNumber() {
		return this.itemsNumber;
	}

	public void setItemsNumber(Integer itemsNumber) {
		this.itemsNumber = itemsNumber;
	}

	public Date getClosingDate() {
		return this.closingDate;
	}

	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	public TaxType getTaxType() {
		return this.taxType;
	}

	public void setTaxType(TaxType taxType) {
		this.taxType = taxType;
	}

	public BillStatus getStatus() {
		return this.status;
	}

	public void setStatus(BillStatus status) {
		this.status = status;
	}

	public String getErpReference() {
		return this.erpReference;
	}

	public void setErpReference(String erpReference) {
		this.erpReference = erpReference;
	}

	public Boolean getCheckResult() {
		return this.checkResult;
	}

	public void setCheckResult(Boolean checkResult) {
		this.checkResult = checkResult;
	}

	public Customer getReceiver() {
		return this.receiver;
	}

	public void setReceiver(Customer receiver) {
		if (receiver != null) {
			this.__validate_client_context__(receiver.getClientId());
		}
		this.receiver = receiver;
	}
	public Locations getDeliveryLoc() {
		return this.deliveryLoc;
	}

	public void setDeliveryLoc(Locations deliveryLoc) {
		if (deliveryLoc != null) {
			this.__validate_client_context__(deliveryLoc.getClientId());
		}
		this.deliveryLoc = deliveryLoc;
	}
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		if (country != null) {
			this.__validate_client_context__(country.getClientId());
		}
		this.country = country;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		if (contract != null) {
			this.__validate_client_context__(contract.getClientId());
		}
		this.contract = contract;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}
	public Suppliers getIssuer() {
		return this.issuer;
	}

	public void setIssuer(Suppliers issuer) {
		if (issuer != null) {
			this.__validate_client_context__(issuer.getClientId());
		}
		this.issuer = issuer;
	}

	public Collection<InvoiceLine> getInvoiceLines() {
		return this.invoiceLines;
	}

	public void setInvoiceLines(Collection<InvoiceLine> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

	/**
	 * @param e
	 */
	public void addToInvoiceLines(InvoiceLine e) {
		if (this.invoiceLines == null) {
			this.invoiceLines = new ArrayList<>();
		}
		e.setInvoice(this);
		this.invoiceLines.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
