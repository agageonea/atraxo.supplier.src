/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.fuelEvents;

import atraxo.acc.domain.impl.acc_type.FuelEventStatus;
import atraxo.acc.domain.impl.acc_type.FuelEventStatusConverter;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.FuelEventTransmissionStatusConverter;
import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.acc_type.QuantitySourceConverter;
import atraxo.acc.domain.impl.acc_type.ReceivedStatus;
import atraxo.acc.domain.impl.acc_type.ReceivedStatusConverter;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.acc.domain.impl.fuelEventsDetails.FuelEventsDetails;
import atraxo.acc.domain.impl.payableAccruals.PayableAccrual;
import atraxo.acc.domain.impl.receivableAccruals.ReceivableAccrual;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicatorConverter;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperation;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperationConverter;
import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.FuelingTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.ProductConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.OperationTypeConverter;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatusConverter;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import atraxo.ops.domain.impl.ops_type.RevocationReasonConverter;
import atraxo.ops.domain.impl.ops_type.Suffix;
import atraxo.ops.domain.impl.ops_type.SuffixConverter;
import atraxo.ops.domain.impl.ops_type.TemperatureUnit;
import atraxo.ops.domain.impl.ops_type.TemperatureUnitConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link FuelEvent} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelEvent.NQ_FIND_BY_KEY, query = "SELECT e FROM FuelEvent e WHERE e.clientId = :clientId and e.fuelingDate = :fuelingDate and e.customer = :customer and e.departure = :departure and e.aircraft = :aircraft and e.flightNumber = :flightNumber and e.suffix = :suffix and e.ticketNumber = :ticketNumber and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelEvent.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM FuelEvent e WHERE e.clientId = :clientId and e.fuelingDate = :fuelingDate and e.customer.id = :customerId and e.departure.id = :departureId and e.aircraft.id = :aircraftId and e.flightNumber = :flightNumber and e.suffix = :suffix and e.ticketNumber = :ticketNumber and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelEvent.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelEvent e WHERE e.clientId = :clientId and e.id = :id and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelEvent.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelEvent.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "fueling_date", "customer_id",
		"departure_id", "aircraft_id", "flight_number", "suffix",
		"ticket_number"})})
public class FuelEvent extends AbstractSubsidiary implements Serializable {

	public static final String TABLE_NAME = "acc_FUEL_EVENTS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "FuelEvent.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "FuelEvent.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelEvent.findByBusiness";

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fueling_date", nullable = false)
	private Date fuelingDate;

	@NotBlank
	@Column(name = "flight_number", nullable = false, length = 32)
	private String flightNumber;

	@Column(name = "suffix", length = 16)
	@Convert(converter = SuffixConverter.class)
	private Suffix suffix;

	@Column(name = "ticket_number", length = 32)
	private String ticketNumber;

	@NotBlank
	@Column(name = "event_type", nullable = false, length = 16)
	@Convert(converter = FlightTypeIndicatorConverter.class)
	private FlightTypeIndicator eventType;

	@NotNull
	@Column(name = "quantity", nullable = false, precision = 19, scale = 6)
	private BigDecimal quantity;

	@NotBlank
	@Column(name = "quantitysource", nullable = false, length = 32)
	@Convert(converter = QuantitySourceConverter.class)
	private QuantitySource quantitySource;

	@Column(name = "payable_cost", precision = 19, scale = 6)
	private BigDecimal payableCost;

	@Column(name = "billable_cost", precision = 19, scale = 6)
	private BigDecimal billableCost;

	@NotNull
	@Column(name = "sys_quantity", nullable = false, precision = 19, scale = 6)
	private BigDecimal sysQuantity;

	@Column(name = "sys_payable_cost", precision = 19, scale = 6)
	private BigDecimal sysPayableCost;

	@Column(name = "sys_billable_cost", precision = 19, scale = 6)
	private BigDecimal sysBillableCost;

	@Column(name = "invoice_status", length = 32)
	@Convert(converter = OutgoingInvoiceStatusConverter.class)
	private OutgoingInvoiceStatus invoiceStatus;

	@Column(name = "bill_status", length = 32)
	@Convert(converter = BillStatusConverter.class)
	private BillStatus billStatus;

	@Column(name = "received_status", length = 32)
	@Convert(converter = ReceivedStatusConverter.class)
	private ReceivedStatus receivedStatus;

	@Column(name = "bill_price_source_id", precision = 11)
	private Integer billPriceSourceId;

	@Column(name = "bill_price_source_type", length = 45)
	private String billPriceSourceType;

	@Column(name = "is_resale")
	private Boolean isResale;

	@Transient
	private BigDecimal oldBillableCost;

	@Column(name = "net_quantity", precision = 19, scale = 6)
	private BigDecimal netUpliftQuantity;

	@Column(name = "fueling_operation", length = 16)
	@Convert(converter = FuelTicketFuelingOperationConverter.class)
	private FuelTicketFuelingOperation fuelingOperation;

	@Column(name = "transport", length = 16)
	@Convert(converter = FuelingTypeConverter.class)
	private FuelingType transport;

	@Column(name = "operation_type", length = 32)
	@Convert(converter = OperationTypeConverter.class)
	private OperationType operationType;

	@Column(name = "erp_sales_shipment", length = 32)
	private String erpSalesShipment;

	@Column(name = "sales_invoice_no", length = 32)
	private String salesInvoiceNo;

	@Column(name = "purchase_invoice_no", length = 32)
	private String purchaseInvoiceNo;

	@Column(name = "credit_memo_no", length = 32)
	private String creditMemoNo;

	@Column(name = "product", length = 32)
	@Convert(converter = ProductConverter.class)
	private Product product;

	@Column(name = "flight_id", length = 32)
	private String flightID;

	@NotBlank
	@Column(name = "status", nullable = false, length = 64)
	@Convert(converter = FuelEventStatusConverter.class)
	private FuelEventStatus status;

	@Column(name = "bol_number", length = 25)
	private String bolNumber;

	@NotBlank
	@Column(name = "transmission_status_sale", nullable = false, length = 32)
	@Convert(converter = FuelEventTransmissionStatusConverter.class)
	private FuelEventTransmissionStatus transmissionStatusSale;

	@NotBlank
	@Column(name = "transmission_status_purchase", nullable = false, length = 32)
	@Convert(converter = FuelEventTransmissionStatusConverter.class)
	private FuelEventTransmissionStatus transmissionStatusPurchase;

	@Column(name = "erp_purchase_receipt", length = 32)
	private String erpPurchaseReceipt;

	@Column(name = "erp_sale_invoice_no", length = 32)
	private String erpSaleInvoiceNo;

	@Column(name = "density", precision = 19, scale = 6)
	private BigDecimal density;

	@Column(name = "temperature", precision = 11)
	private Integer temperature;

	@Column(name = "temperature_unit", length = 32)
	@Convert(converter = TemperatureUnitConverter.class)
	private TemperatureUnit temperatureUnit;

	@Transient
	private Boolean regenerateDetails;

	@Column(name = "revocation_reason", length = 64)
	@Convert(converter = RevocationReasonConverter.class)
	private RevocationReason revocationReason;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "departure_id", referencedColumnName = "id")
	private Locations departure;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Aircraft.class)
	@JoinColumn(name = "aircraft_id", referencedColumnName = "id")
	private Aircraft aircraft;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "fuel_supplier_id", referencedColumnName = "id")
	private Suppliers fuelSupplier;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "iplagent_id", referencedColumnName = "id")
	private Suppliers iplAgent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "payable_cost_currency_id", referencedColumnName = "id")
	private Currencies payableCostCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "billable_cost_currency_id", referencedColumnName = "id")
	private Currencies billableCostCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "net_unit_id", referencedColumnName = "id")
	private Unit netUpliftUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "contract_holder_id", referencedColumnName = "id")
	private Customer contractHolder;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "ship_to_id", referencedColumnName = "id")
	private Customer shipTo;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "density_mass_unit_id", referencedColumnName = "id")
	private Unit densityUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "density_volume_unit_id", referencedColumnName = "id")
	private Unit densityVolume;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = DeliveryNote.class, mappedBy = "fuelEvent", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<DeliveryNote> deliveryNotes;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = PayableAccrual.class, mappedBy = "fuelEvent", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<PayableAccrual> payableAccruals;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = ReceivableAccrual.class, mappedBy = "fuelEvent", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<ReceivableAccrual> receivableAccruals;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FuelEventsDetails.class, mappedBy = "fuelEvent", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<FuelEventsDetails> details;

	public Date getFuelingDate() {
		return this.fuelingDate;
	}

	public void setFuelingDate(Date fuelingDate) {
		this.fuelingDate = fuelingDate;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public String getTicketNumber() {
		return this.ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public FlightTypeIndicator getEventType() {
		return this.eventType;
	}

	public void setEventType(FlightTypeIndicator eventType) {
		this.eventType = eventType;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public QuantitySource getQuantitySource() {
		return this.quantitySource;
	}

	public void setQuantitySource(QuantitySource quantitySource) {
		this.quantitySource = quantitySource;
	}

	public BigDecimal getPayableCost() {
		return this.payableCost;
	}

	public void setPayableCost(BigDecimal payableCost) {
		this.payableCost = payableCost;
	}

	public BigDecimal getBillableCost() {
		return this.billableCost;
	}

	public void setBillableCost(BigDecimal billableCost) {
		this.billableCost = billableCost;
	}

	public BigDecimal getSysQuantity() {
		return this.sysQuantity;
	}

	public void setSysQuantity(BigDecimal sysQuantity) {
		this.sysQuantity = sysQuantity;
	}

	public BigDecimal getSysPayableCost() {
		return this.sysPayableCost;
	}

	public void setSysPayableCost(BigDecimal sysPayableCost) {
		this.sysPayableCost = sysPayableCost;
	}

	public BigDecimal getSysBillableCost() {
		return this.sysBillableCost;
	}

	public void setSysBillableCost(BigDecimal sysBillableCost) {
		this.sysBillableCost = sysBillableCost;
	}

	public OutgoingInvoiceStatus getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(OutgoingInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public BillStatus getBillStatus() {
		return this.billStatus;
	}

	public void setBillStatus(BillStatus billStatus) {
		this.billStatus = billStatus;
	}

	public ReceivedStatus getReceivedStatus() {
		return this.receivedStatus;
	}

	public void setReceivedStatus(ReceivedStatus receivedStatus) {
		this.receivedStatus = receivedStatus;
	}

	public Integer getBillPriceSourceId() {
		return this.billPriceSourceId;
	}

	public void setBillPriceSourceId(Integer billPriceSourceId) {
		this.billPriceSourceId = billPriceSourceId;
	}

	public String getBillPriceSourceType() {
		return this.billPriceSourceType;
	}

	public void setBillPriceSourceType(String billPriceSourceType) {
		this.billPriceSourceType = billPriceSourceType;
	}

	public Boolean getIsResale() {
		return this.isResale;
	}

	public void setIsResale(Boolean isResale) {
		this.isResale = isResale;
	}

	public BigDecimal getOldBillableCost() {
		return this.oldBillableCost;
	}

	public void setOldBillableCost(BigDecimal oldBillableCost) {
		this.oldBillableCost = oldBillableCost;
	}

	public BigDecimal getNetUpliftQuantity() {
		return this.netUpliftQuantity;
	}

	public void setNetUpliftQuantity(BigDecimal netUpliftQuantity) {
		this.netUpliftQuantity = netUpliftQuantity;
	}

	public FuelTicketFuelingOperation getFuelingOperation() {
		return this.fuelingOperation;
	}

	public void setFuelingOperation(FuelTicketFuelingOperation fuelingOperation) {
		this.fuelingOperation = fuelingOperation;
	}

	public FuelingType getTransport() {
		return this.transport;
	}

	public void setTransport(FuelingType transport) {
		this.transport = transport;
	}

	public OperationType getOperationType() {
		return this.operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public String getErpSalesShipment() {
		return this.erpSalesShipment;
	}

	public void setErpSalesShipment(String erpSalesShipment) {
		this.erpSalesShipment = erpSalesShipment;
	}

	public String getSalesInvoiceNo() {
		return this.salesInvoiceNo;
	}

	public void setSalesInvoiceNo(String salesInvoiceNo) {
		this.salesInvoiceNo = salesInvoiceNo;
	}

	public String getPurchaseInvoiceNo() {
		return this.purchaseInvoiceNo;
	}

	public void setPurchaseInvoiceNo(String purchaseInvoiceNo) {
		this.purchaseInvoiceNo = purchaseInvoiceNo;
	}

	public String getCreditMemoNo() {
		return this.creditMemoNo;
	}

	public void setCreditMemoNo(String creditMemoNo) {
		this.creditMemoNo = creditMemoNo;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getFlightID() {
		return this.flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}

	public FuelEventStatus getStatus() {
		return this.status;
	}

	public void setStatus(FuelEventStatus status) {
		this.status = status;
	}

	public String getBolNumber() {
		return this.bolNumber;
	}

	public void setBolNumber(String bolNumber) {
		this.bolNumber = bolNumber;
	}

	public FuelEventTransmissionStatus getTransmissionStatusSale() {
		return this.transmissionStatusSale;
	}

	public void setTransmissionStatusSale(
			FuelEventTransmissionStatus transmissionStatusSale) {
		this.transmissionStatusSale = transmissionStatusSale;
	}

	public FuelEventTransmissionStatus getTransmissionStatusPurchase() {
		return this.transmissionStatusPurchase;
	}

	public void setTransmissionStatusPurchase(
			FuelEventTransmissionStatus transmissionStatusPurchase) {
		this.transmissionStatusPurchase = transmissionStatusPurchase;
	}

	public String getErpPurchaseReceipt() {
		return this.erpPurchaseReceipt;
	}

	public void setErpPurchaseReceipt(String erpPurchaseReceipt) {
		this.erpPurchaseReceipt = erpPurchaseReceipt;
	}

	public String getErpSaleInvoiceNo() {
		return this.erpSaleInvoiceNo;
	}

	public void setErpSaleInvoiceNo(String erpSaleInvoiceNo) {
		this.erpSaleInvoiceNo = erpSaleInvoiceNo;
	}

	public BigDecimal getDensity() {
		return this.density;
	}

	public void setDensity(BigDecimal density) {
		this.density = density;
	}

	public Integer getTemperature() {
		return this.temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public TemperatureUnit getTemperatureUnit() {
		return this.temperatureUnit;
	}

	public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
		this.temperatureUnit = temperatureUnit;
	}

	public Boolean getRegenerateDetails() {
		return this.regenerateDetails == null ? true : this.regenerateDetails;
	}

	public void setRegenerateDetails(Boolean regenerateDetails) {
		this.regenerateDetails = regenerateDetails;
	}

	public RevocationReason getRevocationReason() {
		return this.revocationReason;
	}

	public void setRevocationReason(RevocationReason revocationReason) {
		this.revocationReason = revocationReason;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Locations getDeparture() {
		return this.departure;
	}

	public void setDeparture(Locations departure) {
		if (departure != null) {
			this.__validate_client_context__(departure.getClientId());
		}
		this.departure = departure;
	}
	public Aircraft getAircraft() {
		return this.aircraft;
	}

	public void setAircraft(Aircraft aircraft) {
		if (aircraft != null) {
			this.__validate_client_context__(aircraft.getClientId());
		}
		this.aircraft = aircraft;
	}
	public Suppliers getFuelSupplier() {
		return this.fuelSupplier;
	}

	public void setFuelSupplier(Suppliers fuelSupplier) {
		if (fuelSupplier != null) {
			this.__validate_client_context__(fuelSupplier.getClientId());
		}
		this.fuelSupplier = fuelSupplier;
	}
	public Suppliers getIplAgent() {
		return this.iplAgent;
	}

	public void setIplAgent(Suppliers iplAgent) {
		if (iplAgent != null) {
			this.__validate_client_context__(iplAgent.getClientId());
		}
		this.iplAgent = iplAgent;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public Currencies getPayableCostCurrency() {
		return this.payableCostCurrency;
	}

	public void setPayableCostCurrency(Currencies payableCostCurrency) {
		if (payableCostCurrency != null) {
			this.__validate_client_context__(payableCostCurrency.getClientId());
		}
		this.payableCostCurrency = payableCostCurrency;
	}
	public Currencies getBillableCostCurrency() {
		return this.billableCostCurrency;
	}

	public void setBillableCostCurrency(Currencies billableCostCurrency) {
		if (billableCostCurrency != null) {
			this.__validate_client_context__(billableCostCurrency.getClientId());
		}
		this.billableCostCurrency = billableCostCurrency;
	}
	public Unit getNetUpliftUnit() {
		return this.netUpliftUnit;
	}

	public void setNetUpliftUnit(Unit netUpliftUnit) {
		if (netUpliftUnit != null) {
			this.__validate_client_context__(netUpliftUnit.getClientId());
		}
		this.netUpliftUnit = netUpliftUnit;
	}
	public Customer getContractHolder() {
		return this.contractHolder;
	}

	public void setContractHolder(Customer contractHolder) {
		if (contractHolder != null) {
			this.__validate_client_context__(contractHolder.getClientId());
		}
		this.contractHolder = contractHolder;
	}
	public Customer getShipTo() {
		return this.shipTo;
	}

	public void setShipTo(Customer shipTo) {
		if (shipTo != null) {
			this.__validate_client_context__(shipTo.getClientId());
		}
		this.shipTo = shipTo;
	}
	public Unit getDensityUnit() {
		return this.densityUnit;
	}

	public void setDensityUnit(Unit densityUnit) {
		if (densityUnit != null) {
			this.__validate_client_context__(densityUnit.getClientId());
		}
		this.densityUnit = densityUnit;
	}
	public Unit getDensityVolume() {
		return this.densityVolume;
	}

	public void setDensityVolume(Unit densityVolume) {
		if (densityVolume != null) {
			this.__validate_client_context__(densityVolume.getClientId());
		}
		this.densityVolume = densityVolume;
	}

	public Collection<DeliveryNote> getDeliveryNotes() {
		return this.deliveryNotes;
	}

	public void setDeliveryNotes(Collection<DeliveryNote> deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	/**
	 * @param e
	 */
	public void addToDeliveryNotes(DeliveryNote e) {
		if (this.deliveryNotes == null) {
			this.deliveryNotes = new ArrayList<>();
		}
		e.setFuelEvent(this);
		this.deliveryNotes.add(e);
	}
	public Collection<PayableAccrual> getPayableAccruals() {
		return this.payableAccruals;
	}

	public void setPayableAccruals(Collection<PayableAccrual> payableAccruals) {
		this.payableAccruals = payableAccruals;
	}

	/**
	 * @param e
	 */
	public void addToPayableAccruals(PayableAccrual e) {
		if (this.payableAccruals == null) {
			this.payableAccruals = new ArrayList<>();
		}
		e.setFuelEvent(this);
		this.payableAccruals.add(e);
	}
	public Collection<ReceivableAccrual> getReceivableAccruals() {
		return this.receivableAccruals;
	}

	public void setReceivableAccruals(
			Collection<ReceivableAccrual> receivableAccruals) {
		this.receivableAccruals = receivableAccruals;
	}

	/**
	 * @param e
	 */
	public void addToReceivableAccruals(ReceivableAccrual e) {
		if (this.receivableAccruals == null) {
			this.receivableAccruals = new ArrayList<>();
		}
		e.setFuelEvent(this);
		this.receivableAccruals.add(e);
	}
	public Collection<FuelEventsDetails> getDetails() {
		return this.details;
	}

	public void setDetails(Collection<FuelEventsDetails> details) {
		this.details = details;
	}

	/**
	 * @param e
	 */
	public void addToDetails(FuelEventsDetails e) {
		if (this.details == null) {
			this.details = new ArrayList<>();
		}
		e.setFuelEvent(this);
		this.details.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
