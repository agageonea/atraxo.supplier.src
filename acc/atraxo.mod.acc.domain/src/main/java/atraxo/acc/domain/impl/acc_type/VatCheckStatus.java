/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum VatCheckStatus {

	_EMPTY_(""), _NOT_CHECKED_("Not checked"), _OK_("OK"), _FAILED_("Failed");

	private String name;

	private VatCheckStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static VatCheckStatus getByName(String name) {
		for (VatCheckStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent VatCheckStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
