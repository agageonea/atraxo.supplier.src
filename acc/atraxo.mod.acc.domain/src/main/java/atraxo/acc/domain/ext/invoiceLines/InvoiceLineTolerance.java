package atraxo.acc.domain.ext.invoiceLines;

public enum InvoiceLineTolerance {
	
	CHECK_INVOICE_AMOUNT_AUTO_BALANCE,
	CHECK_INVOICE_QUANTITY_AUTO_BALANCE,
	
	CHECK_BILL_VAT,
	CHECK_BILL_AMOUNT;

}
