/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceCategory} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceCategoryConverter
		implements
			AttributeConverter<InvoiceCategory, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceCategory value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null InvoiceCategory.");
		}
		return value.getName();
	}

	@Override
	public InvoiceCategory convertToEntityAttribute(String value) {
		return InvoiceCategory.getByName(value);
	}

}
