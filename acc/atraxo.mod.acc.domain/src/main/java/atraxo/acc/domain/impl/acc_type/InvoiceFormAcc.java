/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceFormAcc {

	_EMPTY_(""), _PAPER_("Paper"), _ELECTRONIC_("Electronic");

	private String name;

	private InvoiceFormAcc(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceFormAcc getByName(String name) {
		for (InvoiceFormAcc status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent InvoiceFormAcc with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
