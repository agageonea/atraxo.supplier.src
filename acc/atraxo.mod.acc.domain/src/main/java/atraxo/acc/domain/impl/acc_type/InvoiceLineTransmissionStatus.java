/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceLineTransmissionStatus {

	_EMPTY_(""), _NEW_("New"), _IN_PROGRESS_("In progress"), _FAILED_("Failed"), _TRANSMITTED_(
			"Transmitted");

	private String name;

	private InvoiceLineTransmissionStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceLineTransmissionStatus getByName(String name) {
		for (InvoiceLineTransmissionStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InvoiceLineTransmissionStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
