/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceReferenceType {

	_EMPTY_(""), _BOL_("BOL"), _CTN_("CTN"), _PIN_("PIN"), _PO_("PO"), _SO_(
			"SO"), _WO_("WO"), _EXT_("EXT");

	private String name;

	private InvoiceReferenceType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceReferenceType getByName(String name) {
		for (InvoiceReferenceType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InvoiceReferenceType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
