/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelEventStatus {

	_EMPTY_(""), _ORIGINAL_("Original"), _CANCELED_("Canceled"), _REISSUE_(
			"Reissue"), _UPDATED_("Updated");

	private String name;

	private FuelEventStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelEventStatus getByName(String name) {
		for (FuelEventStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FuelEventStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
