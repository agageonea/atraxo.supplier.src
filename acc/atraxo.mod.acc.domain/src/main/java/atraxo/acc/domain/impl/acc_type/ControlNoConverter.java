/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ControlNo} enum.
 * Generated code. Do not modify in this file.
 */
public class ControlNoConverter
		implements
			AttributeConverter<ControlNo, String> {

	@Override
	public String convertToDatabaseColumn(ControlNo value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ControlNo.");
		}
		return value.getName();
	}

	@Override
	public ControlNo convertToEntityAttribute(String value) {
		return ControlNo.getByName(value);
	}

}
