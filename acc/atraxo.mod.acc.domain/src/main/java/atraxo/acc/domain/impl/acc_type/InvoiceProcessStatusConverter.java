/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceProcessStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceProcessStatusConverter
		implements
			AttributeConverter<InvoiceProcessStatus, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceProcessStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null InvoiceProcessStatus.");
		}
		return value.getName();
	}

	@Override
	public InvoiceProcessStatus convertToEntityAttribute(String value) {
		return InvoiceProcessStatus.getByName(value);
	}

}
