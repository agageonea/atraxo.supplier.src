/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.invoices;

import atraxo.acc.domain.impl.acc_type.InvoiceCategory;
import atraxo.acc.domain.impl.acc_type.InvoiceCategoryConverter;
import atraxo.acc.domain.impl.acc_type.InvoiceExportStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceExportStatusConverter;
import atraxo.acc.domain.impl.acc_type.InvoiceFormAcc;
import atraxo.acc.domain.impl.acc_type.InvoiceFormAccConverter;
import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocTypeConverter;
import atraxo.acc.domain.impl.acc_type.InvoiceTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceTransmissionStatusConverter;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAccConverter;
import atraxo.acc.domain.impl.acc_type.TransactionType;
import atraxo.acc.domain.impl.acc_type.TransactionTypeConverter;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatus;
import atraxo.cmm.domain.impl.cmm_type.BidApprovalStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.ProductConverter;
import atraxo.cmm.domain.impl.cmm_type.TaxType;
import atraxo.cmm.domain.impl.cmm_type.TaxTypeConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Country;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import atraxo.ops.domain.impl.ops_type.RevocationReasonConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link OutgoingInvoice} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = OutgoingInvoice.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM OutgoingInvoice e WHERE e.clientId = :clientId and e.id = :id and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = OutgoingInvoice.TABLE_NAME)
public class OutgoingInvoice extends AbstractSubsidiary implements Serializable {

	public static final String TABLE_NAME = "acc_OUTGOING_INVOICE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "OutgoingInvoice.findByBusiness";

	@Column(name = "invoice_type", length = 32)
	@Convert(converter = InvoiceTypeAccConverter.class)
	private InvoiceTypeAcc invoiceType;

	@NotBlank
	@Column(name = "invoice_no", nullable = false, length = 32)
	private String invoiceNo;

	@Temporal(TemporalType.DATE)
	@Column(name = "invoice_date")
	private Date invoiceDate;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "issue_date", nullable = false)
	private Date issueDate;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_date_from", nullable = false)
	private Date deliveryDateFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_date_to")
	private Date deliverydateTo;

	@NotBlank
	@Column(name = "transaction_type", nullable = false, length = 32)
	@Convert(converter = TransactionTypeConverter.class)
	private TransactionType transactionType;

	@NotBlank
	@Column(name = "invoice_form", nullable = false, length = 32)
	@Convert(converter = InvoiceFormAccConverter.class)
	private InvoiceFormAcc invoiceForm;

	@NotBlank
	@Column(name = "category", nullable = false, length = 32)
	@Convert(converter = InvoiceCategoryConverter.class)
	private InvoiceCategory category;

	@Column(name = "reference_doc_type", length = 32)
	@Convert(converter = InvoiceReferenceDocTypeConverter.class)
	private InvoiceReferenceDocType referenceDocType;

	@Column(name = "reference_doc_no", length = 32)
	private String referenceDocNo;

	@Column(name = "reference_value", precision = 11)
	private Integer referenceDocId;

	@NotNull
	@Column(name = "total_amount", nullable = false, precision = 19, scale = 6)
	private BigDecimal totalAmount;

	@Column(name = "vat_amount", precision = 19, scale = 6)
	private BigDecimal vatAmount;

	@Column(name = "net_amount", precision = 19, scale = 6)
	private BigDecimal netAmount;

	@NotNull
	@Column(name = "quantity", nullable = false, precision = 19, scale = 6)
	private BigDecimal quantity;

	@Temporal(TemporalType.DATE)
	@Column(name = "base_line_date")
	private Date baseLineDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "close_date")
	private Date closeDate;

	@Column(name = "items_no", precision = 11)
	private Integer itemsNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;

	@Column(name = "tax_type", length = 32)
	@Convert(converter = TaxTypeConverter.class)
	private TaxType taxType;

	@NotBlank
	@Column(name = "status", nullable = false, length = 32)
	@Convert(converter = BillStatusConverter.class)
	private BillStatus status;

	@Column(name = "approval_status", length = 32)
	@Convert(converter = BidApprovalStatusConverter.class)
	private BidApprovalStatus approvalStatus;

	@NotBlank
	@Column(name = "product", nullable = false, length = 32)
	@Convert(converter = ProductConverter.class)
	private Product product;

	@Column(name = "credit_memo_reason", length = 64)
	@Convert(converter = RevocationReasonConverter.class)
	private RevocationReason creditMemoReason;

	@NotBlank
	@Column(name = "transmission_status", nullable = false, length = 64)
	@Convert(converter = InvoiceTransmissionStatusConverter.class)
	private InvoiceTransmissionStatus transmissionStatus;

	@NotBlank
	@Column(name = "export_status", nullable = false, length = 64)
	@Convert(converter = InvoiceExportStatusConverter.class)
	private InvoiceExportStatus exportStatus;

	@Temporal(TemporalType.DATE)
	@Column(name = "export_date")
	private Date exportDate;

	@Column(name = "invoice_sent")
	private Boolean invoiceSent;

	@NotBlank
	@Column(name = "control_no", nullable = false, length = 32)
	private String controlNo;

	@Transient
	private Boolean historyFlag;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer receiver;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "delivery_location_id", referencedColumnName = "id")
	private Locations deliveryLoc;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Country.class)
	@JoinColumn(name = "country_id", referencedColumnName = "id")
	private Country country;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currencies currency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "issuer_id", referencedColumnName = "id")
	private Customer issuer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = OutgoingInvoice.class)
	@JoinColumn(name = "invoice_reference_id", referencedColumnName = "id")
	private OutgoingInvoice invoiceReference;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = OutgoingInvoiceLine.class, mappedBy = "outgoingInvoice", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<OutgoingInvoiceLine> invoiceLines;

	public InvoiceTypeAcc getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceTypeAcc invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Date getIssueDate() {
		return this.issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getDeliveryDateFrom() {
		return this.deliveryDateFrom;
	}

	public void setDeliveryDateFrom(Date deliveryDateFrom) {
		this.deliveryDateFrom = deliveryDateFrom;
	}

	public Date getDeliverydateTo() {
		return this.deliverydateTo;
	}

	public void setDeliverydateTo(Date deliverydateTo) {
		this.deliverydateTo = deliverydateTo;
	}

	public TransactionType getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public InvoiceFormAcc getInvoiceForm() {
		return this.invoiceForm;
	}

	public void setInvoiceForm(InvoiceFormAcc invoiceForm) {
		this.invoiceForm = invoiceForm;
	}

	public InvoiceCategory getCategory() {
		return this.category;
	}

	public void setCategory(InvoiceCategory category) {
		this.category = category;
	}

	public InvoiceReferenceDocType getReferenceDocType() {
		return this.referenceDocType;
	}

	public void setReferenceDocType(InvoiceReferenceDocType referenceDocType) {
		this.referenceDocType = referenceDocType;
	}

	public String getReferenceDocNo() {
		return this.referenceDocNo;
	}

	public void setReferenceDocNo(String referenceDocNo) {
		this.referenceDocNo = referenceDocNo;
	}

	public Integer getReferenceDocId() {
		return this.referenceDocId;
	}

	public void setReferenceDocId(Integer referenceDocId) {
		this.referenceDocId = referenceDocId;
	}

	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Date getBaseLineDate() {
		return this.baseLineDate;
	}

	public void setBaseLineDate(Date baseLineDate) {
		this.baseLineDate = baseLineDate;
	}

	public Date getCloseDate() {
		return this.closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public Integer getItemsNumber() {
		return this.itemsNumber;
	}

	public void setItemsNumber(Integer itemsNumber) {
		this.itemsNumber = itemsNumber;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public TaxType getTaxType() {
		return this.taxType;
	}

	public void setTaxType(TaxType taxType) {
		this.taxType = taxType;
	}

	public BillStatus getStatus() {
		return this.status;
	}

	public void setStatus(BillStatus status) {
		this.status = status;
	}

	public BidApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(BidApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public RevocationReason getCreditMemoReason() {
		return this.creditMemoReason;
	}

	public void setCreditMemoReason(RevocationReason creditMemoReason) {
		this.creditMemoReason = creditMemoReason;
	}

	public InvoiceTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			InvoiceTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public InvoiceExportStatus getExportStatus() {
		return this.exportStatus;
	}

	public void setExportStatus(InvoiceExportStatus exportStatus) {
		this.exportStatus = exportStatus;
	}

	public Date getExportDate() {
		return this.exportDate;
	}

	public void setExportDate(Date exportDate) {
		this.exportDate = exportDate;
	}

	public Boolean getInvoiceSent() {
		return this.invoiceSent;
	}

	public void setInvoiceSent(Boolean invoiceSent) {
		this.invoiceSent = invoiceSent;
	}

	public String getControlNo() {
		return this.controlNo;
	}

	public void setControlNo(String controlNo) {
		this.controlNo = controlNo;
	}

	public Boolean getHistoryFlag() {
		return this.historyFlag;
	}

	public void setHistoryFlag(Boolean historyFlag) {
		this.historyFlag = historyFlag;
	}

	public Customer getReceiver() {
		return this.receiver;
	}

	public void setReceiver(Customer receiver) {
		if (receiver != null) {
			this.__validate_client_context__(receiver.getClientId());
		}
		this.receiver = receiver;
	}
	public Locations getDeliveryLoc() {
		return this.deliveryLoc;
	}

	public void setDeliveryLoc(Locations deliveryLoc) {
		if (deliveryLoc != null) {
			this.__validate_client_context__(deliveryLoc.getClientId());
		}
		this.deliveryLoc = deliveryLoc;
	}
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		if (country != null) {
			this.__validate_client_context__(country.getClientId());
		}
		this.country = country;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}
	public Customer getIssuer() {
		return this.issuer;
	}

	public void setIssuer(Customer issuer) {
		if (issuer != null) {
			this.__validate_client_context__(issuer.getClientId());
		}
		this.issuer = issuer;
	}
	public OutgoingInvoice getInvoiceReference() {
		return this.invoiceReference;
	}

	public void setInvoiceReference(OutgoingInvoice invoiceReference) {
		if (invoiceReference != null) {
			this.__validate_client_context__(invoiceReference.getClientId());
		}
		this.invoiceReference = invoiceReference;
	}

	public Collection<OutgoingInvoiceLine> getInvoiceLines() {
		return this.invoiceLines;
	}

	public void setInvoiceLines(Collection<OutgoingInvoiceLine> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

	/**
	 * @param e
	 */
	public void addToInvoiceLines(OutgoingInvoiceLine e) {
		if (this.invoiceLines == null) {
			this.invoiceLines = new ArrayList<>();
		}
		e.setOutgoingInvoice(this);
		this.invoiceLines.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
