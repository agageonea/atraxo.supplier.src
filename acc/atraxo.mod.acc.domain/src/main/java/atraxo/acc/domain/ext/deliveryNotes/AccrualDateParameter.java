package atraxo.acc.domain.ext.deliveryNotes;

public enum AccrualDateParameter {

	CURRENT("Current"), EVENT_DATE("Event date");

	private String value;

	private AccrualDateParameter(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

}
