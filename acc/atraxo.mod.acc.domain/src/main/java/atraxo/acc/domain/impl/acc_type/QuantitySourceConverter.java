/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link QuantitySource} enum.
 * Generated code. Do not modify in this file.
 */
public class QuantitySourceConverter
		implements
			AttributeConverter<QuantitySource, String> {

	@Override
	public String convertToDatabaseColumn(QuantitySource value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null QuantitySource.");
		}
		return value.getName();
	}

	@Override
	public QuantitySource convertToEntityAttribute(String value) {
		return QuantitySource.getByName(value);
	}

}
