/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceLineTransmissionStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceLineTransmissionStatusConverter
		implements
			AttributeConverter<InvoiceLineTransmissionStatus, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceLineTransmissionStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null InvoiceLineTransmissionStatus.");
		}
		return value.getName();
	}

	@Override
	public InvoiceLineTransmissionStatus convertToEntityAttribute(String value) {
		return InvoiceLineTransmissionStatus.getByName(value);
	}

}
