/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceTypeAcc} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceTypeAccConverter
		implements
			AttributeConverter<InvoiceTypeAcc, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceTypeAcc value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null InvoiceTypeAcc.");
		}
		return value.getName();
	}

	@Override
	public InvoiceTypeAcc convertToEntityAttribute(String value) {
		return InvoiceTypeAcc.getByName(value);
	}

}
