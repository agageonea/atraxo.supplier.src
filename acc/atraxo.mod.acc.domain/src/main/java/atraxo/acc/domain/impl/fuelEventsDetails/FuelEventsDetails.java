/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.fuelEventsDetails;

import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicatorConverter;
import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.cmm.domain.impl.cmm_type.DealTypeConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link FuelEventsDetails} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelEventsDetails.NQ_FIND_BY_KEY, query = "SELECT e FROM FuelEventsDetails e WHERE e.clientId = :clientId and e.fuelEvent = :fuelEvent and e.contractCode = :contractCode and e.priceCategory = :priceCategory", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelEventsDetails.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM FuelEventsDetails e WHERE e.clientId = :clientId and e.fuelEvent.id = :fuelEventId and e.contractCode = :contractCode and e.priceCategory.id = :priceCategoryId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelEventsDetails.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelEventsDetails e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelEventsDetails.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelEventsDetails.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "fuel_event_id", "contract_code",
		"price_category_id"})})
public class FuelEventsDetails extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "acc_FUEL_EVENTS_DETAILS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "FuelEventsDetails.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "FuelEventsDetails.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelEventsDetails.findByBusiness";

	@Column(name = "contract_code", length = 32)
	private String contractCode;

	@Column(name = "transaction", length = 32)
	@Convert(converter = DealTypeConverter.class)
	private DealType transaction;

	@Column(name = "price_name", length = 64)
	private String priceName;

	@Column(name = "original_price", precision = 19, scale = 6)
	private BigDecimal originalPrice;

	@Column(name = "settlement_price", precision = 19, scale = 6)
	private BigDecimal settlementPrice;

	@Column(name = "settlement_amount", precision = 21, scale = 6)
	private BigDecimal settlementAmount;

	@Column(name = "exchange_rate", precision = 21, scale = 6)
	private BigDecimal exchangeRate;

	@Column(name = "vat_amount", precision = 19, scale = 6)
	private BigDecimal vatAmount;

	@Transient
	private Boolean updated;

	@NotBlank
	@Column(name = "calculate_indicator", nullable = false, length = 32)
	@Convert(converter = CalculateIndicatorConverter.class)
	private CalculateIndicator calculateIndicator;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelEvent.class)
	@JoinColumn(name = "fuel_event_id", referencedColumnName = "id")
	private FuelEvent fuelEvent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PriceCategory.class)
	@JoinColumn(name = "price_category_id", referencedColumnName = "id")
	private PriceCategory priceCategory;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = MainCategory.class)
	@JoinColumn(name = "main_cateogry_id", referencedColumnName = "id")
	private MainCategory mainCategory;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "original_price_crncy_id", referencedColumnName = "id")
	private Currencies originalPriceCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "original_price_unit_id", referencedColumnName = "id")
	private Unit originalPriceUnit;

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public DealType getTransaction() {
		return this.transaction;
	}

	public void setTransaction(DealType transaction) {
		this.transaction = transaction;
	}

	public String getPriceName() {
		return this.priceName;
	}

	public void setPriceName(String priceName) {
		this.priceName = priceName;
	}

	public BigDecimal getOriginalPrice() {
		return this.originalPrice;
	}

	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}

	public BigDecimal getSettlementPrice() {
		return this.settlementPrice;
	}

	public void setSettlementPrice(BigDecimal settlementPrice) {
		this.settlementPrice = settlementPrice;
	}

	public BigDecimal getSettlementAmount() {
		return this.settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public Boolean getUpdated() {
		return this.updated;
	}

	public void setUpdated(Boolean updated) {
		this.updated = updated;
	}

	public CalculateIndicator getCalculateIndicator() {
		return this.calculateIndicator;
	}

	public void setCalculateIndicator(CalculateIndicator calculateIndicator) {
		this.calculateIndicator = calculateIndicator;
	}

	public FuelEvent getFuelEvent() {
		return this.fuelEvent;
	}

	public void setFuelEvent(FuelEvent fuelEvent) {
		if (fuelEvent != null) {
			this.__validate_client_context__(fuelEvent.getClientId());
		}
		this.fuelEvent = fuelEvent;
	}
	public PriceCategory getPriceCategory() {
		return this.priceCategory;
	}

	public void setPriceCategory(PriceCategory priceCategory) {
		if (priceCategory != null) {
			this.__validate_client_context__(priceCategory.getClientId());
		}
		this.priceCategory = priceCategory;
	}
	public MainCategory getMainCategory() {
		return this.mainCategory;
	}

	public void setMainCategory(MainCategory mainCategory) {
		if (mainCategory != null) {
			this.__validate_client_context__(mainCategory.getClientId());
		}
		this.mainCategory = mainCategory;
	}
	public Currencies getOriginalPriceCurrency() {
		return this.originalPriceCurrency;
	}

	public void setOriginalPriceCurrency(Currencies originalPriceCurrency) {
		if (originalPriceCurrency != null) {
			this.__validate_client_context__(originalPriceCurrency
					.getClientId());
		}
		this.originalPriceCurrency = originalPriceCurrency;
	}
	public Unit getOriginalPriceUnit() {
		return this.originalPriceUnit;
	}

	public void setOriginalPriceUnit(Unit originalPriceUnit) {
		if (originalPriceUnit != null) {
			this.__validate_client_context__(originalPriceUnit.getClientId());
		}
		this.originalPriceUnit = originalPriceUnit;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.fuelEvent == null) ? 0 : this.fuelEvent.hashCode());
		result = prime
				* result
				+ ((this.contractCode == null) ? 0 : this.contractCode
						.hashCode());
		result = prime
				* result
				+ ((this.priceCategory == null) ? 0 : this.priceCategory
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		FuelEventsDetails other = (FuelEventsDetails) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.fuelEvent == null) {
			if (other.fuelEvent != null) {
				return false;
			}
		} else if (!this.fuelEvent.equals(other.fuelEvent)) {
			return false;
		}
		if (this.contractCode == null) {
			if (other.contractCode != null) {
				return false;
			}
		} else if (!this.contractCode.equals(other.contractCode)) {
			return false;
		}
		if (this.priceCategory == null) {
			if (other.priceCategory != null) {
				return false;
			}
		} else if (!this.priceCategory.equals(other.priceCategory)) {
			return false;
		}
		return true;
	}
}
