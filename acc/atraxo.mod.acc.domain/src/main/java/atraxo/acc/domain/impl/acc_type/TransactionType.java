/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TransactionType {

	_EMPTY_(""), _CASH_("Cash"), _CORRECTED_("Corrected"), _FINAL_("Final"), _ORIGINAL_(
			"Original"), _PREPAID_INVOICE_CORRECTION_(
			"Prepaid invoice correction"), _PREPAID_INVOICE_("Prepaid invoice"), _PROVISIONAL_(
			"Provisional");

	private String name;

	private TransactionType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TransactionType getByName(String name) {
		for (TransactionType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TransactionType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
