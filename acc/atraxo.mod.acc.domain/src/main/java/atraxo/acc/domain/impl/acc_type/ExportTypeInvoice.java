/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ExportTypeInvoice {

	_EMPTY_(""), _PDF_("PDF"), _XML_("XML");

	private String name;

	private ExportTypeInvoice(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ExportTypeInvoice getByName(String name) {
		for (ExportTypeInvoice status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent ExportTypeInvoice with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
