/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceReferenceType} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceReferenceTypeConverter
		implements
			AttributeConverter<InvoiceReferenceType, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceReferenceType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null InvoiceReferenceType.");
		}
		return value.getName();
	}

	@Override
	public InvoiceReferenceType convertToEntityAttribute(String value) {
		return InvoiceReferenceType.getByName(value);
	}

}
