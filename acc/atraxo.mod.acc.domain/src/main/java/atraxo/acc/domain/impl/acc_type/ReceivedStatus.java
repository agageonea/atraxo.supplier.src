/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ReceivedStatus {

	_EMPTY_(""), _ON_HOLD_("On hold"), _RELEASED_("Released");

	private String name;

	private ReceivedStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ReceivedStatus getByName(String name) {
		for (ReceivedStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ReceivedStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
