package atraxo.acc.domain.ext.mailmerge.dto;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;
import seava.j4e.api.annotation.Dto;
import seava.j4e.api.annotation.DtoField;

@Dto(entity = OutgoingInvoiceLineDetails.class)
@JsonRootName(value = "InvoiceLineDetail")
public class InvoiceLineDetailDto extends AbstractDataDto {

	private static final long serialVersionUID = 5711509837550741912L;

	@DtoField
	@JsonProperty(value = "Price_Name")
	private String priceName;

	@DtoField
	@JsonProperty(value = "Settlement_Price")
	private BigDecimal settlementPrice;

	@DtoField
	@JsonProperty(value = "Settlement_Amount")
	private BigDecimal settlementAmount;

	@DtoField
	@JsonProperty(value = "VAT_Amount")
	private BigDecimal vatAmount;

	@JsonProperty(value = "Currency")
	@DtoField(path = "originalPriceCurrency.code")
	private String currencyCode;

	@DtoField(path = "outgoingInvoice.quantity")
	@JsonProperty(value = "Quantity")
	private BigDecimal quantity;

	@JsonProperty(value = "UoM")
	@DtoField(path = "originalPriceUnit.code")
	private String unitCode;

	public InvoiceLineDetailDto() {
		super();
	}

	@Override
	public void setDefaultValues() {
		// add custom default values
	}

	public String getPriceName() {
		return this.priceName;
	}

	public void setPriceName(String priceName) {
		this.priceName = priceName;
	}

	public BigDecimal getSettlementPrice() {
		return this.settlementPrice;
	}

	public void setSettlementPrice(BigDecimal settlementPrice) {
		this.settlementPrice = settlementPrice;
	}

	public BigDecimal getSettlementAmount() {
		return this.settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

}
