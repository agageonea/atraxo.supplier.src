/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelEventStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelEventStatusConverter
		implements
			AttributeConverter<FuelEventStatus, String> {

	@Override
	public String convertToDatabaseColumn(FuelEventStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelEventStatus.");
		}
		return value.getName();
	}

	@Override
	public FuelEventStatus convertToEntityAttribute(String value) {
		return FuelEventStatus.getByName(value);
	}

}
