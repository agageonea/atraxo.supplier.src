/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.outgoingInvoiceLineDetails;

import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicator;
import atraxo.cmm.domain.impl.cmm_type.CalculateIndicatorConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.categories.MainCategory;
import atraxo.fmbas.domain.impl.categories.PriceCategory;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link OutgoingInvoiceLineDetails} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = OutgoingInvoiceLineDetails.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM OutgoingInvoiceLineDetails e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = OutgoingInvoiceLineDetails.TABLE_NAME)
public class OutgoingInvoiceLineDetails extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "acc_OUTGOING_INVOICE_LINE_DETAILS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "OutgoingInvoiceLineDetails.findByBusiness";

	@Column(name = "price_name", length = 64)
	private String priceName;

	@Column(name = "original_price", precision = 19, scale = 6)
	private BigDecimal originalPrice;

	@Column(name = "settlement_price", precision = 19, scale = 6)
	private BigDecimal settlementPrice;

	@Column(name = "settlement_amount", precision = 21, scale = 6)
	private BigDecimal settlementAmount;

	@Column(name = "exchange_rate", precision = 21, scale = 6)
	private BigDecimal exchangeRate;

	@Column(name = "vat_amount", precision = 19, scale = 6)
	private BigDecimal vatAmount;

	@NotBlank
	@Column(name = "calculate_indicator", nullable = false, length = 32)
	@Convert(converter = CalculateIndicatorConverter.class)
	private CalculateIndicator calculateIndicator;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = OutgoingInvoiceLine.class)
	@JoinColumn(name = "invoice_line_id", referencedColumnName = "id")
	private OutgoingInvoiceLine outgoingInvoiceLine;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = OutgoingInvoice.class)
	@JoinColumn(name = "sales_invoice_id", referencedColumnName = "id")
	private OutgoingInvoice outgoingInvoice;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PriceCategory.class)
	@JoinColumn(name = "price_category_id", referencedColumnName = "id")
	private PriceCategory priceCategory;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = MainCategory.class)
	@JoinColumn(name = "main_category_id", referencedColumnName = "id")
	private MainCategory mainCategory;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "original_price_crncy_id", referencedColumnName = "id")
	private Currencies originalPriceCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "original_price_unit_id", referencedColumnName = "id")
	private Unit originalPriceUnit;

	public String getPriceName() {
		return this.priceName;
	}

	public void setPriceName(String priceName) {
		this.priceName = priceName;
	}

	public BigDecimal getOriginalPrice() {
		return this.originalPrice;
	}

	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}

	public BigDecimal getSettlementPrice() {
		return this.settlementPrice;
	}

	public void setSettlementPrice(BigDecimal settlementPrice) {
		this.settlementPrice = settlementPrice;
	}

	public BigDecimal getSettlementAmount() {
		return this.settlementAmount;
	}

	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public CalculateIndicator getCalculateIndicator() {
		return this.calculateIndicator;
	}

	public void setCalculateIndicator(CalculateIndicator calculateIndicator) {
		this.calculateIndicator = calculateIndicator;
	}

	public OutgoingInvoiceLine getOutgoingInvoiceLine() {
		return this.outgoingInvoiceLine;
	}

	public void setOutgoingInvoiceLine(OutgoingInvoiceLine outgoingInvoiceLine) {
		if (outgoingInvoiceLine != null) {
			this.__validate_client_context__(outgoingInvoiceLine.getClientId());
		}
		this.outgoingInvoiceLine = outgoingInvoiceLine;
	}
	public OutgoingInvoice getOutgoingInvoice() {
		return this.outgoingInvoice;
	}

	public void setOutgoingInvoice(OutgoingInvoice outgoingInvoice) {
		if (outgoingInvoice != null) {
			this.__validate_client_context__(outgoingInvoice.getClientId());
		}
		this.outgoingInvoice = outgoingInvoice;
	}
	public PriceCategory getPriceCategory() {
		return this.priceCategory;
	}

	public void setPriceCategory(PriceCategory priceCategory) {
		if (priceCategory != null) {
			this.__validate_client_context__(priceCategory.getClientId());
		}
		this.priceCategory = priceCategory;
	}
	public MainCategory getMainCategory() {
		return this.mainCategory;
	}

	public void setMainCategory(MainCategory mainCategory) {
		if (mainCategory != null) {
			this.__validate_client_context__(mainCategory.getClientId());
		}
		this.mainCategory = mainCategory;
	}
	public Currencies getOriginalPriceCurrency() {
		return this.originalPriceCurrency;
	}

	public void setOriginalPriceCurrency(Currencies originalPriceCurrency) {
		if (originalPriceCurrency != null) {
			this.__validate_client_context__(originalPriceCurrency
					.getClientId());
		}
		this.originalPriceCurrency = originalPriceCurrency;
	}
	public Unit getOriginalPriceUnit() {
		return this.originalPriceUnit;
	}

	public void setOriginalPriceUnit(Unit originalPriceUnit) {
		if (originalPriceUnit != null) {
			this.__validate_client_context__(originalPriceUnit.getClientId());
		}
		this.originalPriceUnit = originalPriceUnit;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
