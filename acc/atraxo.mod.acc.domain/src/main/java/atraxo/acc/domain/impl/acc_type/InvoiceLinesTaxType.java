/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceLinesTaxType {

	_EMPTY_(""), _BONDED_("Bonded"), _DOMESTIC_("Domestic"), _FOREIGN_TRADE_ZONE_(
			"Foreign trade zone"), _EU_QUALIFIED_PRODUCT_(
			"EU qualified product"), _NON_EU_QUALIFIED_PRODUCT_(
			"Non EU qualified product"), _UNSPECIFIED_("Unspecified");

	private String name;

	private InvoiceLinesTaxType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceLinesTaxType getByName(String name) {
		for (InvoiceLinesTaxType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InvoiceLinesTaxType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
