/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.deliveryNotes;

import atraxo.acc.domain.impl.acc_type.QuantitySource;
import atraxo.acc.domain.impl.acc_type.QuantitySourceConverter;
import atraxo.acc.domain.impl.deliveryNotes.DeliveryNoteContract;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicatorConverter;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperation;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperationConverter;
import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.FuelingTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.ProductConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatus;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.OperationTypeConverter;
import atraxo.ops.domain.impl.ops_type.Suffix;
import atraxo.ops.domain.impl.ops_type.SuffixConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link DeliveryNote} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = DeliveryNote.NQ_FIND_BY_KEY, query = "SELECT e FROM DeliveryNote e WHERE e.clientId = :clientId and e.source = :source and e.fuelingDate = :fuelingDate and e.departure = :departure and e.customer = :customer and e.aircraft = :aircraft and e.flightNumber = :flightNumber and e.suffix = :suffix and e.ticketNumber = :ticketNumber and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = DeliveryNote.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM DeliveryNote e WHERE e.clientId = :clientId and e.source = :source and e.fuelingDate = :fuelingDate and e.departure.id = :departureId and e.customer.id = :customerId and e.aircraft.id = :aircraftId and e.flightNumber = :flightNumber and e.suffix = :suffix and e.ticketNumber = :ticketNumber and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = DeliveryNote.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM DeliveryNote e WHERE e.clientId = :clientId and e.id = :id and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = DeliveryNote.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = DeliveryNote.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "source", "fueling_date",
		"departure_id", "customer_id", "aircraft_id", "flight_number",
		"suffix", "ticket_number"})})
public class DeliveryNote extends AbstractSubsidiary implements Serializable {

	public static final String TABLE_NAME = "acc_DELIVERY_NOTES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "DeliveryNote.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "DeliveryNote.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "DeliveryNote.findByBusiness";

	@NotBlank
	@Column(name = "source", nullable = false, length = 32)
	@Convert(converter = QuantitySourceConverter.class)
	private QuantitySource source;

	@NotNull
	@Column(name = "used", nullable = false)
	private Boolean used;

	@NotNull
	@Column(name = "object_id", nullable = false, precision = 11)
	private Integer objectId;

	@NotBlank
	@Column(name = "object_type", nullable = false, length = 32)
	private String objectType;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fueling_date", nullable = false)
	private Date fuelingDate;

	@NotBlank
	@Column(name = "flight_number", nullable = false, length = 32)
	private String flightNumber;

	@Column(name = "suffix", length = 16)
	@Convert(converter = SuffixConverter.class)
	private Suffix suffix;

	@Column(name = "ticket_number", length = 32)
	private String ticketNumber;

	@NotNull
	@Column(name = "quantity", nullable = false, precision = 19, scale = 6)
	private BigDecimal quantity;

	@NotBlank
	@Column(name = "event_type", nullable = false, length = 16)
	@Convert(converter = FlightTypeIndicatorConverter.class)
	private FlightTypeIndicator eventType;

	@Column(name = "payable_cost", precision = 19, scale = 6)
	private BigDecimal payableCost;

	@Column(name = "billable_cost", precision = 19, scale = 6)
	private BigDecimal billableCost;

	@Column(name = "reallocate")
	private Boolean reallocate;

	@Column(name = "net_quantity", precision = 19, scale = 6)
	private BigDecimal netUpliftQuantity;

	@Column(name = "fueling_operation", length = 16)
	@Convert(converter = FuelTicketFuelingOperationConverter.class)
	private FuelTicketFuelingOperation fuelingOperation;

	@Column(name = "transport", length = 16)
	@Convert(converter = FuelingTypeConverter.class)
	private FuelingType transport;

	@Column(name = "operation_type", length = 32)
	@Convert(converter = OperationTypeConverter.class)
	private OperationType operationType;

	@Column(name = "erp_no", length = 32)
	private String erpNo;

	@Column(name = "product", length = 32)
	@Convert(converter = ProductConverter.class)
	private Product product;

	@Column(name = "flight_id", length = 32)
	private String flightID;

	@Transient
	private FuelTicketStatus ticketStatus;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelEvent.class)
	@JoinColumn(name = "fuel_event_id", referencedColumnName = "id")
	private FuelEvent fuelEvent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "departure_id", referencedColumnName = "id")
	private Locations departure;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Aircraft.class)
	@JoinColumn(name = "aircraft_id", referencedColumnName = "id")
	private Aircraft aircraft;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "fuel_supplier_id", referencedColumnName = "id")
	private Suppliers fuelSupplier;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "iplagent_id", referencedColumnName = "id")
	private Suppliers iplAgent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "payable_cost_currency_id", referencedColumnName = "id")
	private Currencies payableCostCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "billable_cost_currency_id", referencedColumnName = "id")
	private Currencies billableCostCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "net_unit_id", referencedColumnName = "id")
	private Unit netUpliftUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "contract_holder_id", referencedColumnName = "id")
	private Customer contractHolder;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "ship_to_id", referencedColumnName = "id")
	private Customer shipTo;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = DeliveryNoteContract.class, mappedBy = "deliveryNotes", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<DeliveryNoteContract> deliveryNoteContracts;

	public QuantitySource getSource() {
		return this.source;
	}

	public void setSource(QuantitySource source) {
		this.source = source;
	}

	public Boolean getUsed() {
		return this.used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public Date getFuelingDate() {
		return this.fuelingDate;
	}

	public void setFuelingDate(Date fuelingDate) {
		this.fuelingDate = fuelingDate;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public String getTicketNumber() {
		return this.ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public FlightTypeIndicator getEventType() {
		return this.eventType;
	}

	public void setEventType(FlightTypeIndicator eventType) {
		this.eventType = eventType;
	}

	public BigDecimal getPayableCost() {
		return this.payableCost;
	}

	public void setPayableCost(BigDecimal payableCost) {
		this.payableCost = payableCost;
	}

	public BigDecimal getBillableCost() {
		return this.billableCost;
	}

	public void setBillableCost(BigDecimal billableCost) {
		this.billableCost = billableCost;
	}

	public Boolean getReallocate() {
		return this.reallocate;
	}

	public void setReallocate(Boolean reallocate) {
		this.reallocate = reallocate;
	}

	public BigDecimal getNetUpliftQuantity() {
		return this.netUpliftQuantity;
	}

	public void setNetUpliftQuantity(BigDecimal netUpliftQuantity) {
		this.netUpliftQuantity = netUpliftQuantity;
	}

	public FuelTicketFuelingOperation getFuelingOperation() {
		return this.fuelingOperation;
	}

	public void setFuelingOperation(FuelTicketFuelingOperation fuelingOperation) {
		this.fuelingOperation = fuelingOperation;
	}

	public FuelingType getTransport() {
		return this.transport;
	}

	public void setTransport(FuelingType transport) {
		this.transport = transport;
	}

	public OperationType getOperationType() {
		return this.operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public String getErpNo() {
		return this.erpNo;
	}

	public void setErpNo(String erpNo) {
		this.erpNo = erpNo;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getFlightID() {
		return this.flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}

	public FuelTicketStatus getTicketStatus() {
		return this.ticketStatus;
	}

	public void setTicketStatus(FuelTicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public FuelEvent getFuelEvent() {
		return this.fuelEvent;
	}

	public void setFuelEvent(FuelEvent fuelEvent) {
		if (fuelEvent != null) {
			this.__validate_client_context__(fuelEvent.getClientId());
		}
		this.fuelEvent = fuelEvent;
	}
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Locations getDeparture() {
		return this.departure;
	}

	public void setDeparture(Locations departure) {
		if (departure != null) {
			this.__validate_client_context__(departure.getClientId());
		}
		this.departure = departure;
	}
	public Aircraft getAircraft() {
		return this.aircraft;
	}

	public void setAircraft(Aircraft aircraft) {
		if (aircraft != null) {
			this.__validate_client_context__(aircraft.getClientId());
		}
		this.aircraft = aircraft;
	}
	public Suppliers getFuelSupplier() {
		return this.fuelSupplier;
	}

	public void setFuelSupplier(Suppliers fuelSupplier) {
		if (fuelSupplier != null) {
			this.__validate_client_context__(fuelSupplier.getClientId());
		}
		this.fuelSupplier = fuelSupplier;
	}
	public Suppliers getIplAgent() {
		return this.iplAgent;
	}

	public void setIplAgent(Suppliers iplAgent) {
		if (iplAgent != null) {
			this.__validate_client_context__(iplAgent.getClientId());
		}
		this.iplAgent = iplAgent;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public Currencies getPayableCostCurrency() {
		return this.payableCostCurrency;
	}

	public void setPayableCostCurrency(Currencies payableCostCurrency) {
		if (payableCostCurrency != null) {
			this.__validate_client_context__(payableCostCurrency.getClientId());
		}
		this.payableCostCurrency = payableCostCurrency;
	}
	public Currencies getBillableCostCurrency() {
		return this.billableCostCurrency;
	}

	public void setBillableCostCurrency(Currencies billableCostCurrency) {
		if (billableCostCurrency != null) {
			this.__validate_client_context__(billableCostCurrency.getClientId());
		}
		this.billableCostCurrency = billableCostCurrency;
	}
	public Unit getNetUpliftUnit() {
		return this.netUpliftUnit;
	}

	public void setNetUpliftUnit(Unit netUpliftUnit) {
		if (netUpliftUnit != null) {
			this.__validate_client_context__(netUpliftUnit.getClientId());
		}
		this.netUpliftUnit = netUpliftUnit;
	}
	public Customer getContractHolder() {
		return this.contractHolder;
	}

	public void setContractHolder(Customer contractHolder) {
		if (contractHolder != null) {
			this.__validate_client_context__(contractHolder.getClientId());
		}
		this.contractHolder = contractHolder;
	}
	public Customer getShipTo() {
		return this.shipTo;
	}

	public void setShipTo(Customer shipTo) {
		if (shipTo != null) {
			this.__validate_client_context__(shipTo.getClientId());
		}
		this.shipTo = shipTo;
	}

	public Collection<DeliveryNoteContract> getDeliveryNoteContracts() {
		return this.deliveryNoteContracts;
	}

	public void setDeliveryNoteContracts(
			Collection<DeliveryNoteContract> deliveryNoteContracts) {
		this.deliveryNoteContracts = deliveryNoteContracts;
	}

	/**
	 * @param e
	 */
	public void addToDeliveryNoteContracts(DeliveryNoteContract e) {
		if (this.deliveryNoteContracts == null) {
			this.deliveryNoteContracts = new ArrayList<>();
		}
		e.setDeliveryNotes(this);
		this.deliveryNoteContracts.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.source == null) ? 0 : this.source.hashCode());
		result = prime
				* result
				+ ((this.fuelingDate == null) ? 0 : this.fuelingDate.hashCode());
		result = prime * result
				+ ((this.departure == null) ? 0 : this.departure.hashCode());
		result = prime * result
				+ ((this.customer == null) ? 0 : this.customer.hashCode());
		result = prime * result
				+ ((this.aircraft == null) ? 0 : this.aircraft.hashCode());
		result = prime
				* result
				+ ((this.flightNumber == null) ? 0 : this.flightNumber
						.hashCode());
		result = prime * result
				+ ((this.suffix == null) ? 0 : this.suffix.hashCode());
		result = prime
				* result
				+ ((this.ticketNumber == null) ? 0 : this.ticketNumber
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		DeliveryNote other = (DeliveryNote) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.source == null) {
			if (other.source != null) {
				return false;
			}
		} else if (!this.source.equals(other.source)) {
			return false;
		}
		if (this.fuelingDate == null) {
			if (other.fuelingDate != null) {
				return false;
			}
		} else if (!this.fuelingDate.equals(other.fuelingDate)) {
			return false;
		}
		if (this.departure == null) {
			if (other.departure != null) {
				return false;
			}
		} else if (!this.departure.equals(other.departure)) {
			return false;
		}
		if (this.customer == null) {
			if (other.customer != null) {
				return false;
			}
		} else if (!this.customer.equals(other.customer)) {
			return false;
		}
		if (this.aircraft == null) {
			if (other.aircraft != null) {
				return false;
			}
		} else if (!this.aircraft.equals(other.aircraft)) {
			return false;
		}
		if (this.flightNumber == null) {
			if (other.flightNumber != null) {
				return false;
			}
		} else if (!this.flightNumber.equals(other.flightNumber)) {
			return false;
		}
		if (this.suffix == null) {
			if (other.suffix != null) {
				return false;
			}
		} else if (!this.suffix.equals(other.suffix)) {
			return false;
		}
		if (this.ticketNumber == null) {
			if (other.ticketNumber != null) {
				return false;
			}
		} else if (!this.ticketNumber.equals(other.ticketNumber)) {
			return false;
		}
		return true;
	}
}
