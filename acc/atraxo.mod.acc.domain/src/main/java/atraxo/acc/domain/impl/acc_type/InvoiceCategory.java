/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceCategory {

	_EMPTY_(""), _PRODUCT_INTO_PLANE_("Product into plane"), _FUELING_SERVICE_INTO_PLANE_(
			"Fueling service into plane"), _PRODUCT_INTO_STORAGE_(
			"Product into storage");

	private String name;

	private InvoiceCategory(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceCategory getByName(String name) {
		for (InvoiceCategory status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent InvoiceCategory with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
