/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link CheckStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class CheckStatusConverter
		implements
			AttributeConverter<CheckStatus, String> {

	@Override
	public String convertToDatabaseColumn(CheckStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null CheckStatus.");
		}
		return value.getName();
	}

	@Override
	public CheckStatus convertToEntityAttribute(String value) {
		return CheckStatus.getByName(value);
	}

}
