package atraxo.acc.domain.ext.invoices;

public enum InvoiceCategory {

	PRODUCT_INTO_PLANE("Product into plane", "Product", "Into plane"),
	FUELING_SERVICE_INTO_PLANE("Fueling service into plane", "Fueling service", "Into plane"),
	PRODUCT_INTO_STORAGE("Product into storage", "Product", "Into storage");

	private String origName;
	private String scope;
	private String type;

	private InvoiceCategory(String name, String type, String scope) {
		this.origName = name;
		this.type = type;
		this.scope = scope;
	}

	public String getScope() {
		return this.scope;
	}

	public String getType() {
		return this.type;
	}

	public static InvoiceCategory getByName(String name) {
		for (InvoiceCategory invoicceCategory : values()) {
			if (invoicceCategory.origName.equalsIgnoreCase(name)) {
				return invoicceCategory;
			}
		}
		throw new RuntimeException("Invalid invoice category : " + name);
	}

	public String getOrigName() {
		return this.origName;
	}
}
