package atraxo.acc.domain.ext.invoices;

public enum SourceForTheTemplate {

	REPORT_CENTER("Report center"), EXCEL("EXCEL");

	private String name;

	private SourceForTheTemplate(String name) {
		this.name = name;
	}

	public static SourceForTheTemplate getByName(String name) {
		for (SourceForTheTemplate sourceForTheTemplate : values()) {
			if (sourceForTheTemplate.name.equalsIgnoreCase(name)) {
				return sourceForTheTemplate;
			}
		}
		throw new RuntimeException("Invalid name : " + name);
	}

	public String getName() {
		return this.name;
	}
}
