package atraxo.acc.domain.ext.ricoh;

public enum EntityTypeName {
	FCF, CCF, FEX, NDC, NDR;
}
