/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.invoiceLines;

import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceLineTransmissionStatusConverter;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.acc.domain.impl.invoiceLines.OutgoingInvoiceLine;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.acc.domain.impl.outgoingInvoiceLineDetails.OutgoingInvoiceLineDetails;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicatorConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.ops.domain.impl.ops_type.Suffix;
import atraxo.ops.domain.impl.ops_type.SuffixConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link OutgoingInvoiceLine} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = OutgoingInvoiceLine.NQ_FIND_BY_KEY, query = "SELECT e FROM OutgoingInvoiceLine e WHERE e.clientId = :clientId and e.outgoingInvoice = :outgoingInvoice and e.fuelEvent = :fuelEvent", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = OutgoingInvoiceLine.NQ_FIND_BY_KEY_PRIMITIVE, query = "SELECT e FROM OutgoingInvoiceLine e WHERE e.clientId = :clientId and e.outgoingInvoice.id = :outgoingInvoiceId and e.fuelEvent.id = :fuelEventId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = OutgoingInvoiceLine.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM OutgoingInvoiceLine e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = OutgoingInvoiceLine.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = OutgoingInvoiceLine.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "outgoing_invoice_id",
		"fuel_event_id"})})
public class OutgoingInvoiceLine extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "acc_OUTGOING_INVOICE_LINE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Key.
	 */
	public static final String NQ_FIND_BY_KEY = "OutgoingInvoiceLine.findByKey";
	/**
	 * Named query find by unique key: Key using the ID field for references.
	 */
	public static final String NQ_FIND_BY_KEY_PRIMITIVE = "OutgoingInvoiceLine.findByKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "OutgoingInvoiceLine.findByBusiness";

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "delivery_date", nullable = false)
	private Date deliveryDate;

	@Column(name = "aircraft_registration_no", length = 10)
	private String aircraftRegistrationNumber;

	@Column(name = "flight_no", length = 4)
	private String flightNo;

	@Column(name = "flight_suffix", length = 16)
	@Convert(converter = SuffixConverter.class)
	private Suffix flightSuffix;

	@Column(name = "event_type", length = 16)
	@Convert(converter = FlightTypeIndicatorConverter.class)
	private FlightTypeIndicator flightType;

	@Column(name = "ticket_no", length = 32)
	private String ticketNo;

	@NotNull
	@Column(name = "quantity", nullable = false, precision = 19, scale = 6)
	private BigDecimal quantity;

	@Column(name = "amount", precision = 19, scale = 6)
	private BigDecimal amount;

	@Column(name = "vat", precision = 19, scale = 6)
	private BigDecimal vat;

	@Column(name = "is_credited")
	private Boolean isCredited;

	@NotBlank
	@Column(name = "transmission_status", nullable = false, length = 64)
	@Convert(converter = InvoiceLineTransmissionStatusConverter.class)
	private InvoiceLineTransmissionStatus transmissionStatus;

	@Column(name = "erp_reference", length = 32)
	private String erpReference;

	@Column(name = "bol_number", length = 25)
	private String bolNumber;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelEvent.class)
	@JoinColumn(name = "fuel_event_id", referencedColumnName = "id")
	private FuelEvent fuelEvent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = OutgoingInvoice.class)
	@JoinColumn(name = "outgoing_invoice_id", referencedColumnName = "id")
	private OutgoingInvoice outgoingInvoice;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = OutgoingInvoice.class)
	@JoinColumn(name = "reference_invoice_item_id", referencedColumnName = "id")
	private OutgoingInvoice referenceInvItem;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "destination_id", referencedColumnName = "id")
	private Locations destination;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = OutgoingInvoiceLine.class)
	@JoinColumn(name = "invoice_line_reference_id", referencedColumnName = "id")
	private OutgoingInvoiceLine referenceInvoiceLine;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = OutgoingInvoiceLineDetails.class, mappedBy = "outgoingInvoiceLine", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<OutgoingInvoiceLineDetails> details;

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getAircraftRegistrationNumber() {
		return this.aircraftRegistrationNumber;
	}

	public void setAircraftRegistrationNumber(String aircraftRegistrationNumber) {
		this.aircraftRegistrationNumber = aircraftRegistrationNumber;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Suffix getFlightSuffix() {
		return this.flightSuffix;
	}

	public void setFlightSuffix(Suffix flightSuffix) {
		this.flightSuffix = flightSuffix;
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}

	public String getTicketNo() {
		return this.ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getVat() {
		return this.vat;
	}

	public void setVat(BigDecimal vat) {
		this.vat = vat;
	}

	public Boolean getIsCredited() {
		return this.isCredited;
	}

	public void setIsCredited(Boolean isCredited) {
		this.isCredited = isCredited;
	}

	public InvoiceLineTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			InvoiceLineTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public String getErpReference() {
		return this.erpReference;
	}

	public void setErpReference(String erpReference) {
		this.erpReference = erpReference;
	}

	public String getBolNumber() {
		return this.bolNumber;
	}

	public void setBolNumber(String bolNumber) {
		this.bolNumber = bolNumber;
	}

	public FuelEvent getFuelEvent() {
		return this.fuelEvent;
	}

	public void setFuelEvent(FuelEvent fuelEvent) {
		if (fuelEvent != null) {
			this.__validate_client_context__(fuelEvent.getClientId());
		}
		this.fuelEvent = fuelEvent;
	}
	public OutgoingInvoice getOutgoingInvoice() {
		return this.outgoingInvoice;
	}

	public void setOutgoingInvoice(OutgoingInvoice outgoingInvoice) {
		if (outgoingInvoice != null) {
			this.__validate_client_context__(outgoingInvoice.getClientId());
		}
		this.outgoingInvoice = outgoingInvoice;
	}
	public OutgoingInvoice getReferenceInvItem() {
		return this.referenceInvItem;
	}

	public void setReferenceInvItem(OutgoingInvoice referenceInvItem) {
		if (referenceInvItem != null) {
			this.__validate_client_context__(referenceInvItem.getClientId());
		}
		this.referenceInvItem = referenceInvItem;
	}
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Locations getDestination() {
		return this.destination;
	}

	public void setDestination(Locations destination) {
		if (destination != null) {
			this.__validate_client_context__(destination.getClientId());
		}
		this.destination = destination;
	}
	public OutgoingInvoiceLine getReferenceInvoiceLine() {
		return this.referenceInvoiceLine;
	}

	public void setReferenceInvoiceLine(OutgoingInvoiceLine referenceInvoiceLine) {
		if (referenceInvoiceLine != null) {
			this.__validate_client_context__(referenceInvoiceLine.getClientId());
		}
		this.referenceInvoiceLine = referenceInvoiceLine;
	}

	public Collection<OutgoingInvoiceLineDetails> getDetails() {
		return this.details;
	}

	public void setDetails(Collection<OutgoingInvoiceLineDetails> details) {
		this.details = details;
	}

	/**
	 * @param e
	 */
	public void addToDetails(OutgoingInvoiceLineDetails e) {
		if (this.details == null) {
			this.details = new ArrayList<>();
		}
		e.setOutgoingInvoiceLine(this);
		this.details.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.outgoingInvoice == null) ? 0 : this.outgoingInvoice
						.hashCode());
		result = prime * result
				+ ((this.fuelEvent == null) ? 0 : this.fuelEvent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		OutgoingInvoiceLine other = (OutgoingInvoiceLine) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.outgoingInvoice == null) {
			if (other.outgoingInvoice != null) {
				return false;
			}
		} else if (!this.outgoingInvoice.equals(other.outgoingInvoice)) {
			return false;
		}
		if (this.fuelEvent == null) {
			if (other.fuelEvent != null) {
				return false;
			}
		} else if (!this.fuelEvent.equals(other.fuelEvent)) {
			return false;
		}
		return true;
	}
}
