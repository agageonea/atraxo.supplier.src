package atraxo.acc.domain.ext.mailmerge.dto;

import java.util.Collection;

import org.codehaus.jackson.annotate.JsonProperty;

import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;
import seava.j4e.api.annotation.Dto;
import seava.j4e.api.annotation.DtoField;

@Dto(entity = OutgoingInvoice.class)
public class OutgoingInvoiceDto extends AbstractDataDto {

	private static final long serialVersionUID = -8868382378336413752L;

	@DtoField(rootElement = true)
	@JsonProperty(value = "InvoiceHeader")
	private InvoiceHeaderDto header;

	@DtoField(path = "issuer")
	@JsonProperty(value = "InvoiceIssuer")
	private InvoiceCustomerDto issuer;

	@DtoField(path = "receiver")
	@JsonProperty(value = "InvoiceReceiver")
	private InvoiceCustomerDto receiver;

	@DtoField
	@JsonProperty(value = "InvoiceLine")
	private Collection<InvoiceLineDto> invoiceLines;

	public OutgoingInvoiceDto() {
		super();
	}

	@Override
	public void setDefaultValues() {
		if (this.header != null) {
			this.header.setTitle("Invoice");

			// based on type and company
			switch (this.header.getInvoiceType()) {
			case _CRN_:
				this.header.setTitle("Credit Note");
				break;
			case _INV_:
				if (this.issuer != null && "TAN".equals(this.issuer.getCode())) {
					this.header.setTitle("Tax Invoice");
				}
				break;
			default:
				break;

			}
		}
	}

	public InvoiceHeaderDto getHeader() {
		return this.header;
	}

	public void setHeader(InvoiceHeaderDto header) {
		this.header = header;
	}

	public InvoiceCustomerDto getIssuer() {
		return this.issuer;
	}

	public void setIssuer(InvoiceCustomerDto issuer) {
		this.issuer = issuer;
	}

	public InvoiceCustomerDto getReceiver() {
		return this.receiver;
	}

	public void setReceiver(InvoiceCustomerDto receiver) {
		this.receiver = receiver;
	}

	public Collection<InvoiceLineDto> getInvoiceLines() {
		return this.invoiceLines;
	}

	public void setInvoiceLines(Collection<InvoiceLineDto> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

}
