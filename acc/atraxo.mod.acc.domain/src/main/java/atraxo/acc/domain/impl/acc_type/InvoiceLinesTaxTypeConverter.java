/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceLinesTaxType} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceLinesTaxTypeConverter
		implements
			AttributeConverter<InvoiceLinesTaxType, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceLinesTaxType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null InvoiceLinesTaxType.");
		}
		return value.getName();
	}

	@Override
	public InvoiceLinesTaxType convertToEntityAttribute(String value) {
		return InvoiceLinesTaxType.getByName(value);
	}

}
