/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceProcessStatus {

	_EMPTY_(""), _NOT_INVOICED_("Not invoiced"), _PARTIAL_INVOICED_(
			"Partial invoiced"), _COMPLETELY_INVOICED_("Completely invoiced"), _CLOSED_(
			"Closed"), _NO_INVOICING_("No invoicing");

	private String name;

	private InvoiceProcessStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceProcessStatus getByName(String name) {
		for (InvoiceProcessStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InvoiceProcessStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
