/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.deliveryNotes;

import atraxo.acc.domain.impl.deliveryNotes.DeliveryNote;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link DeliveryNoteContract} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = DeliveryNoteContract.NQ_FIND_BY_DELIVERYNOTESCONTRACTS, query = "SELECT e FROM DeliveryNoteContract e WHERE e.clientId = :clientId and e.contracts = :contracts and e.deliveryNotes = :deliveryNotes", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = DeliveryNoteContract.NQ_FIND_BY_DELIVERYNOTESCONTRACTS_PRIMITIVE, query = "SELECT e FROM DeliveryNoteContract e WHERE e.clientId = :clientId and e.contracts.id = :contractsId and e.deliveryNotes.id = :deliveryNotesId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = DeliveryNoteContract.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM DeliveryNoteContract e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = DeliveryNoteContract.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = DeliveryNoteContract.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "contract_id", "delivery_note_id"})})
public class DeliveryNoteContract extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "acc_DELIVERY_NOTES_CONTRACTS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: DeliveryNotesContracts.
	 */
	public static final String NQ_FIND_BY_DELIVERYNOTESCONTRACTS = "DeliveryNoteContract.findByDeliveryNotesContracts";
	/**
	 * Named query find by unique key: DeliveryNotesContracts using the ID field for references.
	 */
	public static final String NQ_FIND_BY_DELIVERYNOTESCONTRACTS_PRIMITIVE = "DeliveryNoteContract.findByDeliveryNotesContracts_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "DeliveryNoteContract.findByBusiness";

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contracts;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = DeliveryNote.class)
	@JoinColumn(name = "delivery_note_id", referencedColumnName = "id")
	private DeliveryNote deliveryNotes;
	public Contract getContracts() {
		return this.contracts;
	}

	public void setContracts(Contract contracts) {
		if (contracts != null) {
			this.__validate_client_context__(contracts.getClientId());
		}
		this.contracts = contracts;
	}
	public DeliveryNote getDeliveryNotes() {
		return this.deliveryNotes;
	}

	public void setDeliveryNotes(DeliveryNote deliveryNotes) {
		if (deliveryNotes != null) {
			this.__validate_client_context__(deliveryNotes.getClientId());
		}
		this.deliveryNotes = deliveryNotes;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.contracts == null) ? 0 : this.contracts.hashCode());
		result = prime
				* result
				+ ((this.deliveryNotes == null) ? 0 : this.deliveryNotes
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		DeliveryNoteContract other = (DeliveryNoteContract) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.contracts == null) {
			if (other.contracts != null) {
				return false;
			}
		} else if (!this.contracts.equals(other.contracts)) {
			return false;
		}
		if (this.deliveryNotes == null) {
			if (other.deliveryNotes != null) {
				return false;
			}
		} else if (!this.deliveryNotes.equals(other.deliveryNotes)) {
			return false;
		}
		return true;
	}
}
