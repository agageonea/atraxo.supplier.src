/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceTransmissionStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceTransmissionStatusConverter
		implements
			AttributeConverter<InvoiceTransmissionStatus, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceTransmissionStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null InvoiceTransmissionStatus.");
		}
		return value.getName();
	}

	@Override
	public InvoiceTransmissionStatus convertToEntityAttribute(String value) {
		return InvoiceTransmissionStatus.getByName(value);
	}

}
