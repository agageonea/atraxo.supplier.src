/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.acc_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceReferenceDocType {

	_EMPTY_(""), _CONTRACT_("Contract"), _PURCHASE_ORDER_("Purchase order");

	private String name;

	private InvoiceReferenceDocType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceReferenceDocType getByName(String name) {
		for (InvoiceReferenceDocType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InvoiceReferenceDocType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
