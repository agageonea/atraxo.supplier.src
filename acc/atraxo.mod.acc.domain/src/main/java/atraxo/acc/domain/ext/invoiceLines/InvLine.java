package atraxo.acc.domain.ext.invoiceLines;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.ops.domain.impl.ops_type.Suffix;

/**
 * Invoice Line with business key: fueling date, registration number, flight number and suffix.
 *
 * @author zspeter
 */
public class InvLine {

	private static final String SEPARATOR = " / ";

	private InvoiceLine invoiceLine;

	private Date fuelingDate;
	private String regNumber;
	private String flightNumber;
	private Suffix suffix;
	private String ticketNumber;

	public InvLine(InvoiceLine invLine) {
		this.invoiceLine = invLine;
		this.fuelingDate = invLine.getDeliveryDate();
		this.regNumber = invLine.getAircraftRegistrationNumber();
		this.flightNumber = invLine.getFlightNo();
		this.suffix = invLine.getSuffix();
		this.ticketNumber = invLine.getTicketNo();
	}

	public InvoiceLine getInvLine() {
		return this.invoiceLine;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.flightNumber == null) ? 0 : this.flightNumber.hashCode());
		result = prime * result + ((this.fuelingDate == null) ? 0 : this.fuelingDate.hashCode());
		result = prime * result + ((this.regNumber == null) ? 0 : this.regNumber.hashCode());
		result = prime * result + ((this.suffix == null) ? 0 : this.suffix.hashCode());
		result = prime * result + ((this.ticketNumber == null) ? 0 : this.ticketNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof InvLine)) {
			return false;
		}
		InvLine other = (InvLine) obj;
		if (this.flightNumber == null) {
			if (other.flightNumber != null) {
				return false;
			}
		} else if (!this.flightNumber.equals(other.flightNumber)) {
			return false;
		}
		if (this.fuelingDate == null) {
			if (other.fuelingDate != null) {
				return false;
			}
		} else if (!this.fuelingDate.equals(other.fuelingDate)) {
			return false;
		}
		if (this.regNumber == null) {
			if (other.regNumber != null) {
				return false;
			}
		} else if (!this.regNumber.equals(other.regNumber)) {
			return false;
		}
		if (this.suffix == null) {
			if (other.suffix != null) {
				return false;
			}
		} else if (!this.suffix.equals(other.suffix)) {
			return false;
		}
		if (this.ticketNumber == null) {
			if (other.ticketNumber != null) {
				return false;
			}
		} else if (!this.ticketNumber.equals(other.ticketNumber)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		DateFormat df = new SimpleDateFormat();
		sb.append(df.format(this.fuelingDate)).append(SEPARATOR);
		sb.append(this.regNumber).append(SEPARATOR);
		sb.append(this.flightNumber).append(SEPARATOR);
		sb.append(this.suffix).append(SEPARATOR);
		sb.append(this.ticketNumber);
		return sb.toString();
	}

}
