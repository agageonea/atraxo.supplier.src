package atraxo.acc.domain.ext.deliveryNotes;

import atraxo.acc.domain.impl.invoiceLines.InvoiceLine;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

/**
 * find an use the generated one
 */

public enum DeliveryNoteSource {

	FUEL_TICKET("Fuel ticket", "FuelTicket", FuelTicket.class),
	FUEL_ORDER("Fuel order", "FlightEvent", FlightEvent.class),
	INVOICE("Invoice", "InvoiceLine", InvoiceLine.class);

	private String name;
	private String objectType;
	private Class<? extends AbstractEntity> clazz;

	private DeliveryNoteSource(String name, String objectType, Class<? extends AbstractEntity> clazz) {
		this.name = name;
		this.objectType = objectType;
		this.clazz = clazz;
	}

	public String getName() {
		return this.name;
	}

	public Class<? extends AbstractEntity> getClazz() {
		return this.clazz;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public static DeliveryNoteSource getByName(String name) {
		for (DeliveryNoteSource status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new RuntimeException("Inexistent delivery note status with name: " + name);
	}

	public static DeliveryNoteSource getByObjectType(String objectType) {
		for (DeliveryNoteSource status : values()) {
			if (status.getObjectType().equalsIgnoreCase(objectType)) {
				return status;
			}
		}
		throw new RuntimeException("Inexistent delivery note status with object type: " + objectType);
	}

}
