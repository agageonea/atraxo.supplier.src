/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.acc.domain.impl.payableAccruals;

import atraxo.acc.domain.impl.acc_type.InvoiceProcessStatus;
import atraxo.acc.domain.impl.acc_type.InvoiceProcessStatusConverter;
import atraxo.acc.domain.impl.fuelEvents.FuelEvent;
import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractScopeConverter;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractSubTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.ContractTypeConverter;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link PayableAccrual} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = PayableAccrual.NQ_FIND_BY_FUELEVENTCONTRACT, query = "SELECT e FROM PayableAccrual e WHERE e.clientId = :clientId and e.contract = :contract and e.fuelEvent = :fuelEvent", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = PayableAccrual.NQ_FIND_BY_FUELEVENTCONTRACT_PRIMITIVE, query = "SELECT e FROM PayableAccrual e WHERE e.clientId = :clientId and e.contract.id = :contractId and e.fuelEvent.id = :fuelEventId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = PayableAccrual.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM PayableAccrual e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = PayableAccrual.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = PayableAccrual.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "contract_id", "fuel_event_id"})})
public class PayableAccrual extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "acc_PAYABLE_ACCRUALS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: FuelEventContract.
	 */
	public static final String NQ_FIND_BY_FUELEVENTCONTRACT = "PayableAccrual.findByFuelEventContract";
	/**
	 * Named query find by unique key: FuelEventContract using the ID field for references.
	 */
	public static final String NQ_FIND_BY_FUELEVENTCONTRACT_PRIMITIVE = "PayableAccrual.findByFuelEventContract_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "PayableAccrual.findByBusiness";

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "fueling_date", nullable = false)
	private Date fuelingDate;

	@NotBlank
	@Column(name = "contract_type", nullable = false, length = 32)
	@Convert(converter = ContractTypeConverter.class)
	private ContractType contractType;

	@NotBlank
	@Column(name = "contract_sub_type", nullable = false, length = 32)
	@Convert(converter = ContractSubTypeConverter.class)
	private ContractSubType contractSubType;

	@NotBlank
	@Column(name = "contract_scopde", nullable = false, length = 32)
	@Convert(converter = ContractScopeConverter.class)
	private ContractScope contractScope;

	@NotBlank
	@Column(name = "inv_process_status", nullable = false, length = 32)
	@Convert(converter = InvoiceProcessStatusConverter.class)
	private InvoiceProcessStatus invProcessStatus;

	@Column(name = "exp_quantity", precision = 21, scale = 6)
	private BigDecimal expectedQuantity;

	@NotNull
	@Column(name = "exp_amount_settlement", nullable = false, precision = 21, scale = 6)
	private BigDecimal expectedAmountSet;

	@NotNull
	@Column(name = "exp_amount_system", nullable = false, precision = 21, scale = 6)
	private BigDecimal expectedAmountSys;

	@NotNull
	@Column(name = "exp_vat_settlement", nullable = false, precision = 21, scale = 6)
	private BigDecimal expectedVATSet;

	@NotNull
	@Column(name = "exp_vat_system", nullable = false, precision = 21, scale = 6)
	private BigDecimal expectedVATSys;

	@Column(name = "inv_quantity", precision = 21, scale = 6)
	private BigDecimal invoicedQuantity;

	@NotNull
	@Column(name = "inv_amount_settlement", nullable = false, precision = 21, scale = 6)
	private BigDecimal invoicedAmountSet;

	@NotNull
	@Column(name = "inv_amount_system", nullable = false, precision = 21, scale = 6)
	private BigDecimal invoicedAmountSys;

	@NotNull
	@Column(name = "inv_vat_settlement", nullable = false, precision = 21, scale = 6)
	private BigDecimal invoicedVATSet;

	@NotNull
	@Column(name = "inv_vat_system", nullable = false, precision = 21, scale = 6)
	private BigDecimal invoicedVATSys;

	@Column(name = "accrual_quantity", precision = 21, scale = 6)
	private BigDecimal accrualQuantity;

	@NotNull
	@Column(name = "accrual_amount_settlement", nullable = false, precision = 21, scale = 6)
	private BigDecimal accrualAmountSet;

	@NotNull
	@Column(name = "accrual_amount_system", nullable = false, precision = 21, scale = 6)
	private BigDecimal accrualAmountSys;

	@NotNull
	@Column(name = "accrual_vat_settlement", nullable = false, precision = 21, scale = 6)
	private BigDecimal accrualVATSet;

	@NotNull
	@Column(name = "accrual_vat_system", nullable = false, precision = 21, scale = 6)
	private BigDecimal accrualVATSys;

	@NotNull
	@Column(name = "exchange_rate", nullable = false, precision = 21, scale = 6)
	private BigDecimal exchangeRate;

	@Temporal(TemporalType.DATE)
	@Column(name = "closing_date")
	private Date closingDate;

	@NotNull
	@Column(name = "is_open", nullable = false)
	private Boolean isOpen;

	@Transient
	private String month;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelEvent.class)
	@JoinColumn(name = "fuel_event_id", referencedColumnName = "id")
	private FuelEvent fuelEvent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Locations location;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "supplier_id", referencedColumnName = "id")
	private Suppliers supplier;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "exp_currency_id", referencedColumnName = "id")
	private Currencies expectedCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "inv_currency_id", referencedColumnName = "id")
	private Currencies invoicedCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "accrual_currency_id", referencedColumnName = "id")
	private Currencies accrualCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contract;

	public Date getFuelingDate() {
		return this.fuelingDate;
	}

	public void setFuelingDate(Date fuelingDate) {
		this.fuelingDate = fuelingDate;
	}

	public ContractType getContractType() {
		return this.contractType;
	}

	public void setContractType(ContractType contractType) {
		this.contractType = contractType;
	}

	public ContractSubType getContractSubType() {
		return this.contractSubType;
	}

	public void setContractSubType(ContractSubType contractSubType) {
		this.contractSubType = contractSubType;
	}

	public ContractScope getContractScope() {
		return this.contractScope;
	}

	public void setContractScope(ContractScope contractScope) {
		this.contractScope = contractScope;
	}

	public InvoiceProcessStatus getInvProcessStatus() {
		return this.invProcessStatus;
	}

	public void setInvProcessStatus(InvoiceProcessStatus invProcessStatus) {
		this.invProcessStatus = invProcessStatus;
	}

	public BigDecimal getExpectedQuantity() {
		return this.expectedQuantity;
	}

	public void setExpectedQuantity(BigDecimal expectedQuantity) {
		this.expectedQuantity = expectedQuantity;
	}

	public BigDecimal getExpectedAmountSet() {
		return this.expectedAmountSet;
	}

	public void setExpectedAmountSet(BigDecimal expectedAmountSet) {
		this.expectedAmountSet = expectedAmountSet;
	}

	public BigDecimal getExpectedAmountSys() {
		return this.expectedAmountSys;
	}

	public void setExpectedAmountSys(BigDecimal expectedAmountSys) {
		this.expectedAmountSys = expectedAmountSys;
	}

	public BigDecimal getExpectedVATSet() {
		return this.expectedVATSet;
	}

	public void setExpectedVATSet(BigDecimal expectedVATSet) {
		this.expectedVATSet = expectedVATSet;
	}

	public BigDecimal getExpectedVATSys() {
		return this.expectedVATSys;
	}

	public void setExpectedVATSys(BigDecimal expectedVATSys) {
		this.expectedVATSys = expectedVATSys;
	}

	public BigDecimal getInvoicedQuantity() {
		return this.invoicedQuantity;
	}

	public void setInvoicedQuantity(BigDecimal invoicedQuantity) {
		this.invoicedQuantity = invoicedQuantity;
	}

	public BigDecimal getInvoicedAmountSet() {
		return this.invoicedAmountSet;
	}

	public void setInvoicedAmountSet(BigDecimal invoicedAmountSet) {
		this.invoicedAmountSet = invoicedAmountSet;
	}

	public BigDecimal getInvoicedAmountSys() {
		return this.invoicedAmountSys;
	}

	public void setInvoicedAmountSys(BigDecimal invoicedAmountSys) {
		this.invoicedAmountSys = invoicedAmountSys;
	}

	public BigDecimal getInvoicedVATSet() {
		return this.invoicedVATSet;
	}

	public void setInvoicedVATSet(BigDecimal invoicedVATSet) {
		this.invoicedVATSet = invoicedVATSet;
	}

	public BigDecimal getInvoicedVATSys() {
		return this.invoicedVATSys;
	}

	public void setInvoicedVATSys(BigDecimal invoicedVATSys) {
		this.invoicedVATSys = invoicedVATSys;
	}

	public BigDecimal getAccrualQuantity() {
		return this.accrualQuantity;
	}

	public void setAccrualQuantity(BigDecimal accrualQuantity) {
		this.accrualQuantity = accrualQuantity;
	}

	public BigDecimal getAccrualAmountSet() {
		return this.accrualAmountSet;
	}

	public void setAccrualAmountSet(BigDecimal accrualAmountSet) {
		this.accrualAmountSet = accrualAmountSet;
	}

	public BigDecimal getAccrualAmountSys() {
		return this.accrualAmountSys;
	}

	public void setAccrualAmountSys(BigDecimal accrualAmountSys) {
		this.accrualAmountSys = accrualAmountSys;
	}

	public BigDecimal getAccrualVATSet() {
		return this.accrualVATSet;
	}

	public void setAccrualVATSet(BigDecimal accrualVATSet) {
		this.accrualVATSet = accrualVATSet;
	}

	public BigDecimal getAccrualVATSys() {
		return this.accrualVATSys;
	}

	public void setAccrualVATSys(BigDecimal accrualVATSys) {
		this.accrualVATSys = accrualVATSys;
	}

	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Date getClosingDate() {
		return this.closingDate;
	}

	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	public Boolean getIsOpen() {
		return this.isOpen;
	}

	public void setIsOpen(Boolean isOpen) {
		this.isOpen = isOpen;
	}

	public String getMonth() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
		return sdf.format(this.fuelingDate);
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public FuelEvent getFuelEvent() {
		return this.fuelEvent;
	}

	public void setFuelEvent(FuelEvent fuelEvent) {
		if (fuelEvent != null) {
			this.__validate_client_context__(fuelEvent.getClientId());
		}
		this.fuelEvent = fuelEvent;
	}
	public Locations getLocation() {
		return this.location;
	}

	public void setLocation(Locations location) {
		if (location != null) {
			this.__validate_client_context__(location.getClientId());
		}
		this.location = location;
	}
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Suppliers getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Suppliers supplier) {
		if (supplier != null) {
			this.__validate_client_context__(supplier.getClientId());
		}
		this.supplier = supplier;
	}
	public Currencies getExpectedCurrency() {
		return this.expectedCurrency;
	}

	public void setExpectedCurrency(Currencies expectedCurrency) {
		if (expectedCurrency != null) {
			this.__validate_client_context__(expectedCurrency.getClientId());
		}
		this.expectedCurrency = expectedCurrency;
	}
	public Currencies getInvoicedCurrency() {
		return this.invoicedCurrency;
	}

	public void setInvoicedCurrency(Currencies invoicedCurrency) {
		if (invoicedCurrency != null) {
			this.__validate_client_context__(invoicedCurrency.getClientId());
		}
		this.invoicedCurrency = invoicedCurrency;
	}
	public Currencies getAccrualCurrency() {
		return this.accrualCurrency;
	}

	public void setAccrualCurrency(Currencies accrualCurrency) {
		if (accrualCurrency != null) {
			this.__validate_client_context__(accrualCurrency.getClientId());
		}
		this.accrualCurrency = accrualCurrency;
	}
	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		if (contract != null) {
			this.__validate_client_context__(contract.getClientId());
		}
		this.contract = contract;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.expectedAmountSet == null) {
			this.expectedAmountSet = new BigDecimal("0");
		}
		if (this.expectedAmountSys == null) {
			this.expectedAmountSys = new BigDecimal("0");
		}
		if (this.expectedVATSet == null) {
			this.expectedVATSet = new BigDecimal("0");
		}
		if (this.expectedVATSys == null) {
			this.expectedVATSys = new BigDecimal("0");
		}
		if (this.invoicedAmountSet == null) {
			this.invoicedAmountSet = new BigDecimal("0");
		}
		if (this.invoicedAmountSys == null) {
			this.invoicedAmountSys = new BigDecimal("0");
		}
		if (this.invoicedVATSet == null) {
			this.invoicedVATSet = new BigDecimal("0");
		}
		if (this.invoicedVATSys == null) {
			this.invoicedVATSys = new BigDecimal("0");
		}
		if (this.accrualAmountSet == null) {
			this.accrualAmountSet = new BigDecimal("0");
		}
		if (this.accrualAmountSys == null) {
			this.accrualAmountSys = new BigDecimal("0");
		}
		if (this.accrualVATSet == null) {
			this.accrualVATSet = new BigDecimal("0");
		}
		if (this.accrualVATSys == null) {
			this.accrualVATSys = new BigDecimal("0");
		}
		if (this.exchangeRate == null) {
			this.exchangeRate = new BigDecimal("0");
		}
		if (this.isOpen == null) {
			this.isOpen = new Boolean(true);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
