/**
 *
 */
package atraxo.acc.domain.ext.mailmerge.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

import atraxo.acc.domain.impl.acc_type.InvoiceReferenceDocType;
import atraxo.acc.domain.impl.acc_type.InvoiceTypeAcc;
import atraxo.acc.domain.impl.invoices.OutgoingInvoice;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDataDto;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import seava.j4e.api.annotation.Dto;
import seava.j4e.api.annotation.DtoField;

/**
 * @author mbotorogea
 */
@Dto(entity = OutgoingInvoice.class)
@JsonRootName(value = "InvoiceHeader")
public class InvoiceHeaderDto extends AbstractDataDto {

	private static final long serialVersionUID = 6653995846152766102L;

	@DtoField
	@JsonProperty(value = "Invoice_Number")
	private String invoiceNo;

	@DtoField
	@JsonProperty(value = "Invoice_Date")
	private Date invoiceDate;

	@DtoField
	@JsonProperty(value = "Total_Amount")
	private BigDecimal totalAmount;

	@DtoField
	@JsonProperty(value = "Baseline_Date")
	private Date baseLineDate;

	@DtoField
	@JsonProperty(value = "VAT_Amount")
	private BigDecimal vatAmount;

	@DtoField
	@JsonProperty(value = "NET_Amount")
	private BigDecimal netAmount;

	@DtoField
	@JsonProperty(value = "Quantity")
	private BigDecimal quantity;

	@DtoField
	@JsonProperty(value = "Agreement_Number")
	private String referenceDocNo;

	@DtoField
	@JsonProperty(value = "Type")
	private InvoiceTypeAcc invoiceType;

	@DtoField
	@JsonProperty(value = "Product")
	private Product product;

	@DtoField
	@JsonProperty(value = "Agreement_Type")
	private InvoiceReferenceDocType referenceDocType;

	@DtoField(path = "unit.code")
	@JsonProperty(value = "UoM")
	private String unitCode;

	@DtoField(path = "currency.code")
	@JsonProperty(value = "Currency")
	private String currencyCode;

	@DtoField(path = "deliveryLoc.code")
	@JsonProperty(value = "Location")
	private String delLocCode;

	@JsonProperty(value = "VAT_Rate")
	private BigDecimal vatRate;

	@JsonProperty(value = "Payment_Terms")
	private Integer paymentTerms;

	@JsonProperty(value = "Payment_Terms_Reference")
	private PaymentDay paymentTermsReference;

	@JsonProperty(value = "TZS_USD_Rate")
	private BigDecimal tzsUsdRate;

	@JsonProperty(value = "Title")
	private String title;

	@JsonProperty(value = "Total_Amount_in_Spanish_Words")
	private String totalAmountInWords;

	public InvoiceHeaderDto() {
		super();
	}

	@Override
	public void setDefaultValues() {
		// set default values
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getBaseLineDate() {
		return this.baseLineDate;
	}

	public void setBaseLineDate(Date baseLineDate) {
		this.baseLineDate = baseLineDate;
	}

	public BigDecimal getVatAmount() {
		return this.vatAmount;
	}

	public void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = vatAmount;
	}

	public BigDecimal getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = netAmount;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getReferenceDocNo() {
		return this.referenceDocNo;
	}

	public void setReferenceDocNo(String referenceDocNo) {
		this.referenceDocNo = referenceDocNo;
	}

	public InvoiceTypeAcc getInvoiceType() {
		return this.invoiceType;
	}

	public void setInvoiceType(InvoiceTypeAcc invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public InvoiceReferenceDocType getReferenceDocType() {
		return this.referenceDocType;
	}

	public void setReferenceDocType(InvoiceReferenceDocType referenceDocType) {
		this.referenceDocType = referenceDocType;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDelLocCode() {
		return this.delLocCode;
	}

	public void setDelLocCode(String delLocCode) {
		this.delLocCode = delLocCode;
	}

	public BigDecimal getVatRate() {
		return this.vatRate;
	}

	public void setVatRate(BigDecimal vatRate) {
		this.vatRate = vatRate;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentTermsReference() {
		return this.paymentTermsReference;
	}

	public void setPaymentTermsReference(PaymentDay paymentTermsReference) {
		this.paymentTermsReference = paymentTermsReference;
	}

	public BigDecimal getTzsUsdRate() {
		return this.tzsUsdRate;
	}

	public void setTzsUsdRate(BigDecimal tzsUsdRate) {
		this.tzsUsdRate = tzsUsdRate;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTotalAmountInWords() {
		return this.totalAmountInWords;
	}

	public void setTotalAmountInWords(String totalAmountInWords) {
		this.totalAmountInWords = totalAmountInWords;
	}

}
