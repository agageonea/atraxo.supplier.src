/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.frame.PayableAccruals_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.PayableAccruals_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("accruedPayGroupping", Ext.create(atraxo.acc.ui.extjs.dc.PayableAccrualsGroupping_Dc,{}))
		.addDc("accruedPay", Ext.create(atraxo.acc.ui.extjs.dc.PayableAccruals_Dc,{}))
		.addDc("accruedPayLocGroupping", Ext.create(atraxo.acc.ui.extjs.dc.PayableAccrualsLocGroupping_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.linkDc("accruedPay", "accruedPayGroupping",{fields:[
					{childField:"month", parentField:"month"}]})
				.linkDc("history", "accruedPay",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"fuelEvDetails",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onFuelEvDetails, scope:this})
		.addButton({name:"btnDetails",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnDetails,stateManager:[{ name:"selected_one_clean", dc:"accruedPayGroupping"}], scope:this})
		.addButton({name:"closeAccrual",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onCloseAccrual, scope:this})
		.addButton({name:"noInvoicing",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onNoInvoicing, scope:this})
		.addButton({name:"reopenAccrual",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false, handler: this.onReopenAccrual, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdwEditor",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwEditor, scope:this})
		.addDcFilterFormView("accruedPay", {name:"Filter", xtype:"acc_PayableAccruals_Dc$Filter"})
		.addDcGridView("accruedPay", {name:"Grid", xtype:"acc_PayableAccruals_Dc$mainList",  listeners:{afterrender: {scope: this, fn: function() {this._getDc_('accruedPay').doQuery()}}}})
		.addDcGridView("accruedPayGroupping", {name:"GridGroupping", xtype:"acc_PayableAccrualsGroupping_Dc$mainListGroupping",  listeners:{afterrender: {scope: this, fn: function() {this._getDc_('accruedPayGroupping').doQuery()}}}})
		.addDcGridView("accruedPay", {name:"EditList", xtype:"acc_PayableAccruals_Dc$List"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("accruedPayGroupping", {name:"Chart", height:260, xtype:"acc_PayableAccrualsGroupping_Dc$Chart",  collapsible:false, split:false})
		.addDcGridView("accruedPayLocGroupping", {name:"GridLocGroupping", xtype:"acc_PayableAccrualsLocGroupping_Dc$List"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"historyTab", height:250, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addTabPanelForUserFields({tabPanelName:"historyTab", containerPanelName:"canvas2", dcName:"history"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["GridGroupping", "Chart"], ["center", "north"])
		.addChildrenTo("canvas2", ["EditList", "historyTab"], ["center", "south"])
		.addChildrenTo("canvas3", ["Grid"], ["center"])
		.addChildrenTo("historyTab", ["History"])
		.addToolbarTo("GridGroupping", "tlbGridGroupping")
		.addToolbarTo("EditList", "tlbEditor");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbGridGroupping", {dc: "accruedPayGroupping"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnDetails"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbEditor", {dc: "accruedPay"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("fuelEvDetails"),this._elems_.get("closeAccrual"),this._elems_.get("noInvoicing"),this._elems_.get("reopenAccrual"),this._elems_.get("helpWdwEditor")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button fuelEvDetails
	 */
	,onFuelEvDetails: function() {
		this.showFuelEvent();
	}
	
	/**
	 * On-Click handler for button btnDetails
	 */
	,onBtnDetails: function() {
		this._showStackedViewElement_('main', 'canvas2', this._getDc_('accruedPay').doQuery()); 
	}
	
	/**
	 * On-Click handler for button closeAccrual
	 */
	,onCloseAccrual: function() {
	}
	
	/**
	 * On-Click handler for button noInvoicing
	 */
	,onNoInvoicing: function() {
	}
	
	/**
	 * On-Click handler for button reopenAccrual
	 */
	,onReopenAccrual: function() {
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwEditor
	 */
	,onHelpWdwEditor: function() {
		this.openHelp();
	}
	
	,showFuelEvent: function() {	
		var bundle = "atraxo.mod.acc";
		var frame = "atraxo.acc.ui.extjs.frame.FuelEvents_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				fuelEventId: this._getDc_("accruedPay").getRecord().get("fuelEventId")
			},
			callback: function (params) {
				this._when_called_from_payable_accruals_(params);
			}
		});
	}
	
	,_afterOnReady_: function() {
		
						var accruedPayGroupping = this._getDc_("accruedPayGroupping");
						var accruedPay = this._getDc_("accruedPay");
						var chartPanel = this._get_("Chart");
						var gridGroupping = this._get_("GridGroupping");
						var accruedPayLocGroupping = this._getDc_("accruedPayLocGroupping");
		
						accruedPayGroupping.on("afterDoQuerySuccess", function(dc) {
							this._addChartComponent_();
							this._injectResetButton_();
						}, this);
		
						gridGroupping.on("filterchange", function() {
							var liveFilterResetBtn = Ext.getCmp(gridGroupping._liveFilterResetBtnId_);
							if (gridGroupping.store.isFiltered()) {
								liveFilterResetBtn.setDisabled(false);
								liveFilterResetBtn.setIconCls("glyph-red");
							}
							else {
								liveFilterResetBtn.setDisabled(true);
								liveFilterResetBtn.setIconCls("glyph-normal");
							}
							this._resetChart_();
							this._collapseAll_(gridGroupping);
		
						}, this);
		
						accruedPayLocGroupping.on("afterDoQuerySuccess", function(dc, ajaxResult) {
							if (ajaxResult.options.buildSecondChart == true) {
								this.unmask();
								chartPanel.removeAll();
								chartPanel.add(this._buildSecondChart_());
							}
							
						}, this);
		
						accruedPayLocGroupping.on("rowcollpase", function(dc) {
							chartPanel.removeAll();
							chartPanel.add(this._buildMainChart_());
						}, this);
		
	}
	
	,_injectResetButton_: function() {
		
		
						var gridGroupping = this._get_("GridGroupping");
						var items = gridGroupping.dockedItems.items;
						var toolbar = "";
						gridGroupping._liveFilterResetBtnId_ = Ext.id();
		
						var resetButton = ["->",{
							xtype: "button",
							text: Main.translate("viewAndFilters", "resetFilter__lbl"),
		            		glyph: "xf0b0@FontAwesome",
							id: gridGroupping._liveFilterResetBtnId_,
							disabled: true,
				            handler: function() {
				                gridGroupping.store.clearFilter();
				            }
						}];
						
						if (gridGroupping.__dcViewType__ == "grid") {
							for (var i = 0; i < items.length; i++) {
								var dockedItem = items[i];
								if (items[i].dock == "top" && items[i].xtype == "toolbar") {
									toolbar = items[i];
									break;
								}
							}
						}
						
						if (!Ext.isEmpty(toolbar)) {
							toolbarItems = toolbar.items.items;
							var l = toolbarItems.length;
							toolbar.insert(l,resetButton);
						}
	}
	
	,_buildMainChart_: function() {
		
						var chartCfg = {
							series : {
								xFields : "month", // Required: the field(s) to be displayed on the x axis
								yFields : ["netAmount", "vatAmount", "grossAmount"], // Required: the field(s) to be displayed on the y axis
								yTitles : ["Net amount","VAT amount","Gross amount"], // Required: the y axis titles displayed in the legend
								barColors: ["#A9AFBC","#4FC0E8", "#ffce55"], // Optional: the column colors, defaults are: ["#A9AFBC","#4FC0E8", "#ffce55"]
								strokeColors : ["#A9AFBC","#89defc", "#ffce55"], // Optional: the column border colors
								labelsRenderer : function(tip, record, item) { // Required: the text to be displayed when we hover over the columns
									var fieldName = item.field;
									var regEx = fieldName.replace( /([A-Z])/g, " $1" );
									var itemName = regEx.charAt(0).toUpperCase() + regEx.slice(1);	
			                        tip.setHtml(itemName+" at "+record.get("month") + ": " + record.get(item.field));
			                    }
							}
						}
						var chartStore = this._getDc_("accruedPayGroupping").store;
						var chart = this.buildColumnChart(chartStore, chartCfg);
						return chart;
	}
	
	,_buildSecondChart_: function() {
		
						var chartCfg = {
							series : {
								xFields : "location", // Required: the field(s) to be displayed on the x axis
								yFields : ["netAmount", "vatAmount", "grossAmount"], // Required: the field(s) to be displayed on the y axis
								yTitles : ["Net amount","VAT amount","Gross amount"], // Required: the y axis titles displayed in the legend
								barColors: ["#A9AFBC","#4FC0E8", "#ffce55"], // Optional: the column colors, defaults are: ["#A9AFBC","#4FC0E8", "#ffce55"]
								strokeColors : ["#A9AFBC","#89defc", "#ffce55"], // Optional: the column border colors
								labelsRenderer : function(tip, record, item) {
									var fieldName = item.field;
									var regEx = fieldName.replace( /([A-Z])/g, " $1" );
									var itemName = regEx.charAt(0).toUpperCase() + regEx.slice(1);
			                        tip.setHtml(itemName+" for "+record.get("location")+" at "+record.get("month") + ": " + record.get(item.field));
			                    }
							}
						}
						var chartStore = this._getDc_("accruedPayLocGroupping").store;
						var chart = this.buildColumnChart(chartStore, chartCfg);
						return chart;
	}
	
	,_addChartComponent_: function() {
		
						var chartPanel = this._get_("Chart");
						chartPanel._chartTitleId_ = Ext.id();
						chartPanel.addDocked({
					        xtype: "toolbar",
							cls: "sone-chart-title-tlb",
					        dock: "top",
					        items: [{
								xtype: "tbtext", 
								id : chartPanel._chartTitleId_,
								text: "Accrued payables overview",
								style: "font-family: 'Open Sans',sans-serif; font-size: 18px; font-weight: 600"
					        }]
					    });
						chartPanel._get_("p1").add(this._buildMainChart_());
		
	}
	
	,_resetChart_: function() {
		
						var chartPanel = this._get_("Chart");
						chartPanel.removeAll();
						chartPanel.add(this._buildMainChart_());
	}
	
	,openHelp: function() {
		
						var url = Main.urlHelp+"/PayableAccruals.html";
						window.open( url, "SONE_Help");
	}
});
