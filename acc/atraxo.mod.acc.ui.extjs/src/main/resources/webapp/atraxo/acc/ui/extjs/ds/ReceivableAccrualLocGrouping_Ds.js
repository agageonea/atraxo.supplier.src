/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.ReceivableAccrualLocGrouping_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_ReceivableAccrualLocGrouping_Ds"
	},
	
	
	fields: [
		{name:"fuelingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"expCurrId", type:"int", allowNull:true},
		{name:"expCurrCode", type:"string"},
		{name:"invCurrId", type:"int", allowNull:true},
		{name:"invCurrCode", type:"string"},
		{name:"accruelCurrId", type:"int", allowNull:true},
		{name:"accruelCurrCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"month", type:"string", noFilter:true, noSort:true},
		{name:"location", type:"string", noFilter:true, noSort:true},
		{name:"events", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"netAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"vatAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"grossAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.ReceivableAccrualLocGrouping_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"fuelingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"expCurrId", type:"int", allowNull:true},
		{name:"expCurrCode", type:"string"},
		{name:"invCurrId", type:"int", allowNull:true},
		{name:"invCurrCode", type:"string"},
		{name:"accruelCurrId", type:"int", allowNull:true},
		{name:"accruelCurrCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"month", type:"string", noFilter:true, noSort:true},
		{name:"location", type:"string", noFilter:true, noSort:true},
		{name:"events", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"netAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"vatAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"grossAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
