/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.InvoiceNumberSerie_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.InvoiceNumberSerie_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.acc.ui.extjs.dc.InvoiceNumberSerie_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.acc_InvoiceNumberSerie_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"prefix", dataIndex:"prefix", maxLength:64})
			.addNumberField({name:"startIndex", dataIndex:"startIndex", maxLength:11})
			.addNumberField({name:"endIndex", dataIndex:"endIndex", maxLength:11})
			.addNumberField({name:"currentIndex", dataIndex:"currentIndex", maxLength:11})
			.addBooleanField({ name:"enabled", dataIndex:"enabled"})
			.addBooleanField({ name:"restart", dataIndex:"restart"})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __FMBAS_TYPE__.DocumentNumberSeriesType._INVOICE_, __FMBAS_TYPE__.DocumentNumberSeriesType._CUSTOMER_, __FMBAS_TYPE__.DocumentNumberSeriesType._CREDIT_MEMO_, __FMBAS_TYPE__.DocumentNumberSeriesType._CONSOLIDATED_STATEMENT_],listeners:{
				expand:{scope:this, fn:this._filterByNotCustomer_}
			}})
		;
	},
	/* ==================== Business functions ==================== */
	
	_filterByNotCustomer_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value != "Customer" ) {
								return value;
							}
						});
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.acc.ui.extjs.dc.InvoiceNumberSerie_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.acc_InvoiceNumberSerie_Dc$List",
	_noPaginator_: true,

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addTextColumn({name:"prefix", dataIndex:"prefix", width:120, maxLength:64, 
			editor: { xtype:"textfield", maxLength:64}})
		.addNumberColumn({name:"startIndex", dataIndex:"startIndex", maxLength:11, align:"right" })
		.addNumberColumn({name:"endIndex", dataIndex:"endIndex", maxLength:11, align:"right" })
		.addNumberColumn({name:"currentIndex", dataIndex:"currentIndex", width:150, noEdit: true, maxLength:11, align:"right" })
		.addBooleanColumn({name:"enabled", dataIndex:"enabled", width:105, noEdit: true})
		.addBooleanColumn({name:"restart", dataIndex:"restart", width:105})
		.addComboColumn({name:"type", dataIndex:"type", width:100, 
			editor:{xtype:"combo", mode: 'local',listeners:{
				expand:{scope:this, fn:this._filterByNotCustomer_}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
			}, triggerAction:'all', forceSelection:true, store:[ __FMBAS_TYPE__.DocumentNumberSeriesType._INVOICE_, __FMBAS_TYPE__.DocumentNumberSeriesType._CUSTOMER_, __FMBAS_TYPE__.DocumentNumberSeriesType._CREDIT_MEMO_, __FMBAS_TYPE__.DocumentNumberSeriesType._CONSOLIDATED_STATEMENT_]}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_filterByNotCustomer_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value != "Customer" ) {
								return value;
							}
						});
	}
});
