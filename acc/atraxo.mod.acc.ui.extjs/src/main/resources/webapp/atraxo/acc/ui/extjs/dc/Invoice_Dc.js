/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.Invoice_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.Invoice_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.acc.ui.extjs.dc.Invoice_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.acc_Invoice_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"invoiceNo", dataIndex:"invoiceNo", maxLength:32})
			.addCombo({ xtype:"combo", name:"invoiceTye", dataIndex:"invoiceType", store:[ __ACC_TYPE__.InvoiceTypeAcc._INV_, __ACC_TYPE__.InvoiceTypeAcc._CRN_]})
			.addLov({name:"deliveryLocCode", dataIndex:"delLocCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "delLocId"} ]}})
			.addLov({name:"issuerCode", dataIndex:"issuerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "issuerId"} ]}})
			.addDateField({name:"issueDate", dataIndex:"issueDate"})
			.addNumberField({name:"totalAmount", dataIndex:"totalAmount", sysDec:"dec_crncy", maxLength:19})
			.addLov({name:"currencyCode", dataIndex:"currCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"id", dsField: "currId"} ]}})
			.addNumberField({name:"quantity", dataIndex:"quantity", sysDec:"dec_unit", maxLength:19})
			.addLov({name:"unitCode", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2}})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __CMM_TYPE__.BillStatus._NOT_BILLED_, __CMM_TYPE__.BillStatus._DRAFT_, __CMM_TYPE__.BillStatus._CHECK_FAILED_, __CMM_TYPE__.BillStatus._CHECK_PASSED_, __CMM_TYPE__.BillStatus._NEEDS_RECHECK_, __CMM_TYPE__.BillStatus._AWAITING_APPROVAL_, __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_, __CMM_TYPE__.BillStatus._PAID_, __CMM_TYPE__.BillStatus._NO_CONTRACT_, __CMM_TYPE__.BillStatus._CREDITED_]})
			.addTextField({ name:"erpReference", dataIndex:"erpReference", maxLength:32})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.acc.ui.extjs.dc.Invoice_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_Invoice_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addNumberColumn({ name:"dueDays", dataIndex:"dueDays", width:100,  renderer:function(val, meta, record, rowIndex) { return this._setDueDaysColor_(val, meta, record, rowIndex); }})
		.addTextColumn({ name:"invoiceNumber", dataIndex:"invoiceNo", width:100})
		.addTextColumn({ name:"type", dataIndex:"invoiceType", width:100})
		.addTextColumn({ name:"location", dataIndex:"delLocCode", width:100})
		.addTextColumn({ name:"issuer", dataIndex:"issuerCode", width:100})
		.addTextColumn({ name:"customer", dataIndex:"receiverCode", hidden:true, width:100})
		.addDateColumn({ name:"date", dataIndex:"issueDate", width:100, _mask_: Masks.DATE})
		.addNumberColumn({ name:"amount", dataIndex:"totalAmount", width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"currency", dataIndex:"currCode", width:100})
		.addNumberColumn({ name:"quantity", dataIndex:"quantity", width:100, sysDec:"dec_unit"})
		.addTextColumn({ name:"unitCode", dataIndex:"unitCode", width:80})
		.addTextColumn({ name:"status", dataIndex:"status", width:100})
		.addTextColumn({ name:"country", dataIndex:"countryCode", hidden:true, width:100})
		.addTextColumn({ name:"contract", dataIndex:"contractCode", hidden:true, width:100})
		.addTextColumn({ name:"dealType", dataIndex:"dealtype", hidden:true, width:100})
		.addDateColumn({ name:"receivingDate", dataIndex:"receivingDate", hidden:true, width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"deliveryDateFr", dataIndex:"deliveryDateFrom", hidden:true, width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"deliverydateTo", dataIndex:"deliverydateTo", hidden:true, width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"transactionType", dataIndex:"transactionType", hidden:true, width:100})
		.addTextColumn({ name:"invoiceForm", dataIndex:"invoiceForm", hidden:true, width:100})
		.addTextColumn({ name:"category", dataIndex:"category", hidden:true, width:100})
		.addTextColumn({ name:"referenceType", dataIndex:"referenceType", hidden:true, width:100})
		.addNumberColumn({ name:"vatAmount", dataIndex:"vatAmount", hidden:true, width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"netAmount", dataIndex:"netAmount", hidden:true, width:100, sysDec:"dec_crncy"})
		.addDateColumn({ name:"issuerBLDate", dataIndex:"issuerBaselinedate", hidden:true, width:120, _mask_: Masks.DATE})
		.addDateColumn({ name:"baseLineDate", dataIndex:"baseLineDate", hidden:true, width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"calcBLDate", dataIndex:"calculatedBaseLineDate", hidden:true, width:100, _mask_: Masks.DATE})
		.addNumberColumn({ name:"itemsNumber", dataIndex:"itemsNumber", hidden:true, width:100})
		.addDateColumn({ name:"closingDate", dataIndex:"closingDate", hidden:true, width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"taxType", dataIndex:"taxType", hidden:true, width:100})
		.addBooleanColumn({ name:"checkResult", dataIndex:"checkResult", hidden:true, width:100})
		.addNumberColumn({ name:"vatRate", dataIndex:"vatRate", hidden:true, width:100, sysDec:"dec_prc"})
		.addNumberColumn({ name:"vatTaxableAmount", dataIndex:"vatTaxableAmount", hidden:true, width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"erpReference", dataIndex:"erpReference", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_setDueDaysColor_: function(val,meta,record,rowIndex) {
		
						if(val == null) return;
						var color = "";
						if (val >= 0) {
							color = "green";
						}
						else {
							color = "#F94F52";
						}
						return "<span style='color: "+color+"'>"+val+"</span>";
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.acc.ui.extjs.dc.Invoice_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_Invoice_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"category", bind:"{d.category}", dataIndex:"category", allowBlank:false, width:350, store:[ __ACC_TYPE__.InvoiceCategory._PRODUCT_INTO_PLANE_, __ACC_TYPE__.InvoiceCategory._FUELING_SERVICE_INTO_PLANE_, __ACC_TYPE__.InvoiceCategory._PRODUCT_INTO_STORAGE_], labelWidth:150})
		.addLov({name:"issuerCode", bind:"{d.issuerCode}", dataIndex:"issuerCode", allowBlank:false, width:350, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32, labelWidth:150,
			retFieldMapping: [{lovField:"id", dsField: "issuerId"} ]})
		.addLov({name:"deliveryLocCode", bind:"{d.delLocCode}", dataIndex:"delLocCode", allowBlank:false, width:350, xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:150,
			retFieldMapping: [{lovField:"id", dsField: "delLocId"} ]})
		.addDateField({name:"issueDate", bind:"{d.issueDate}", dataIndex:"issueDate", allowBlank:false, width:350, labelWidth:150})
		.addTextField({ name:"invoiceNo", bind:"{d.invoiceNo}", dataIndex:"invoiceNo", allowBlank:false, width:350, maxLength:32, labelWidth:150})
		.addDateField({name:"deliveryDateFrom", bind:"{d.deliveryDateFrom}", dataIndex:"deliveryDateFrom", allowBlank:false, width:300, labelWidth:150})
		.addDateField({name:"deliverydateTo", bind:"{d.deliverydateTo}", dataIndex:"deliverydateTo", allowBlank:false, width:200, labelWidth:50})
		.addNumberField({name:"totalAmount", bind:"{d.totalAmount}", dataIndex:"totalAmount", allowBlank:false, width:250, sysDec:"dec_crncy", maxLength:19, labelWidth:150})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", allowBlank:false, width:250, sysDec:"dec_unit", maxLength:19, labelWidth:150})
		.addLov({name:"unitCode", bind:"{d.unitCode}", dataIndex:"unitCode", allowBlank:false, width:100, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addLov({name:"currencyCode", bind:"{d.currCode}", dataIndex:"currCode", allowBlank:false, width:100, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ]})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("deliveryDateFrom"),this._getConfig_("deliverydateTo")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("totalAmount"),this._getConfig_("currencyCode")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantity"),this._getConfig_("unitCode")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["category", "issuerCode", "deliveryLocCode", "issueDate", "invoiceNo", "row1", "row2", "row3"]);
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.acc.ui.extjs.dc.Invoice_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_Invoice_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btn", scope: this, handler: this._wdwShow, text: "...", noInsert:true, _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , width: 40})
		.addTextField({ name:"invoiceNo", bind:"{d.invoiceNo}", dataIndex:"invoiceNo", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, maxLength:32, cls:"sone-flat-field"})
		.addTextField({ name:"dummyField", bind:"{d.dummyField}", dataIndex:"dummyField", maxLength:1, style:"display:none"})
		.addDisplayFieldText({ name:"category", bind:"{d.category}", dataIndex:"category", noEdit:true , noLabel: true, maxLength:32})
		.addDisplayFieldText({ name:"receiverCode", bind:"{d.receiverCode}", dataIndex:"receiverCode", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"issuerCode", bind:"{d.issuerCode}", dataIndex:"issuerCode", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"deliveryLocCode", bind:"{d.delLocCode}", dataIndex:"delLocCode", noEdit:true , maxLength:25})
		.addDisplayFieldText({ name:"invoiceForm", bind:"{d.invoiceForm}", dataIndex:"invoiceForm", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"status", bind:"{d.status}", dataIndex:"status", noEdit:true , maxLength:32})
		.addDisplayFieldNumber({ name:"dueIn", bind:"{d.dueDays}", dataIndex:"dueDays", noEdit:true , maxLength:11 })
		.addDateField({name:"receivingDate", bind:"{d.receivingDate}", dataIndex:"receivingDate", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , width:260, labelWidth:150})
		.addDateField({name:"issueDate", bind:"{d.issueDate}", dataIndex:"issueDate", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:260, labelWidth:150})
		.addTextField({ name:"contractCode", bind:"{d.contractCode}", dataIndex:"contractCode", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , width:210, maxLength:32, labelWidth:125})
		.addCombo({ xtype:"combo", name:"transactionType", bind:"{d.transactionType}", dataIndex:"transactionType", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:250, store:[ __ACC_TYPE__.TransactionType._CASH_, __ACC_TYPE__.TransactionType._CORRECTED_, __ACC_TYPE__.TransactionType._FINAL_, __ACC_TYPE__.TransactionType._ORIGINAL_, __ACC_TYPE__.TransactionType._PREPAID_INVOICE_CORRECTION_, __ACC_TYPE__.TransactionType._PREPAID_INVOICE_, __ACC_TYPE__.TransactionType._PROVISIONAL_], labelWidth:125})
		.addDateField({name:"deliveryDateFrom", bind:"{d.deliveryDateFrom}", dataIndex:"deliveryDateFrom", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:260, labelWidth:150})
		.addDateField({name:"deliverydateTo", bind:"{d.deliverydateTo}", dataIndex:"deliverydateTo", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:250, labelWidth:125})
		.addNumberField({name:"totalAmount", bind:"{d.totalAmount}", dataIndex:"totalAmount", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:220, sysDec:"dec_crncy", maxLength:19, labelWidth:120})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:220, sysDec:"dec_unit", maxLength:19, labelWidth:120})
		.addLov({name:"unitCode", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:70, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addLov({name:"currencyCode", bind:"{d.currCode}", dataIndex:"currCode", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:80, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ]})
		.addNumberField({name:"vatTaxableAmount", bind:"{d.vatTaxableAmount}", dataIndex:"vatTaxableAmount", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , width:230, sysDec:"dec_crncy", maxLength:19, labelWidth:120})
		.addNumberField({name:"vatAmount", bind:"{d.vatAmount}", dataIndex:"vatAmount", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , width:190, sysDec:"dec_crncy", maxLength:19, labelWidth:120})
		.addNumberField({name:"vatRate", bind:"{d.vatRate}", dataIndex:"vatRate", noEdit:true , width:40, noLabel: true, sysDec:"dec_prc", maxLength:19, decimals:0})
		.addDisplayFieldText({ name:"procent", bind:"{d.procent}", dataIndex:"procent", noEdit:true , width:10, maxLength:10, labelAlign:"left"})
		.addDateField({name:"baseLine", bind:"{d.baseLineDate}", dataIndex:"baseLineDate", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , width:210})
		.addDateField({name:"issuerBaseLine", bind:"{d.issuerBaselinedate}", dataIndex:"issuerBaselinedate", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , width:210})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("totalAmount"),this._getConfig_("currencyCode")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantity"),this._getConfig_("unitCode")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("vatAmount"),this._getConfig_("vatRate"),this._getConfig_("procent")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("dummyField"),this._getConfig_("invoiceNo")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("contractCode"),this._getConfig_("btn")]})
		.add({name:"cf", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("deliveryDateFrom"),this._getConfig_("deliverydateTo")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"p2",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"h1", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"c0", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c5", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"})
		.addPanel({ name:"v1", width:260, layout:"anchor"})
		.addPanel({ name:"v2", width:250, layout:"anchor"})
		.addPanel({ name:"v3", width:300, layout:"anchor"})
		.addPanel({ name:"v4", width:260, layout:"anchor"})
		.addPanel({ name:"v5", width:300, layout:"anchor"})
		.addPanel({ name:"r1", width:510, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "h1", "r1"])
		.addChildrenTo("p2", ["c0", "c1", "c2", "c3", "c4", "c5"])
		.addChildrenTo("titleAndKpi", ["title", "p2"])
		.addChildrenTo("h1", ["v1", "v2", "v3", "v4", "v5"])
		.addChildrenTo("c0", ["issuerCode"])
		.addChildrenTo("c1", ["receiverCode"])
		.addChildrenTo("c2", ["deliveryLocCode"])
		.addChildrenTo("c3", ["invoiceForm"])
		.addChildrenTo("c4", ["status"])
		.addChildrenTo("c5", ["dueIn"])
		.addChildrenTo("title", ["row5"])
		.addChildrenTo("v1", ["receivingDate", "issueDate"])
		.addChildrenTo("v2", ["row6", "transactionType"])
		.addChildrenTo("v3", ["row2", "row3"])
		.addChildrenTo("v4", ["vatTaxableAmount", "row4"])
		.addChildrenTo("v5", ["baseLine", "issuerBaseLine"])
		.addChildrenTo("r1", ["cf"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
						
	},
	
	_wdwShow: function() {
		
						this._controller_.fireEvent("showWindowContract", this);
	},
	
	_isEnabled: function(rec) {
		
						var res = false;
						if( rec ){
							res = (rec.data.status === __TYPES__.invoices.status.draft);
						}
						return res;
	},
	
	_afterApplyStates_: function() {
		
						var category;
						var r = this._controller_.getRecord();
						if (r) {
							category = r.get("category");
							var formTitle = this._get_("invoiceNo");
							var originalTitleLabel = formTitle.fieldLabel;
							if(formTitle.labelEl){
								formTitle.labelEl.update(category+" "+originalTitleLabel);
							}
						}
						
		
						
	}
});
