/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.FuelEventsDetails_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.FuelEventsDetails_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.acc.ui.extjs.dc.FuelEventsDetails_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_FuelEventsDetails_Dc$List",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"contractCode", dataIndex:"contractCode", width:160,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addTextColumn({ name:"priceName", dataIndex:"priceName", width:160,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addTextColumn({ name:"priceCategoryName", dataIndex:"priceCategoryName", width:160,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addTextColumn({ name:"mainCategoryName", dataIndex:"mainCategoryName", width:160,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addNumberColumn({ name:"originalPrice", dataIndex:"originalPrice", width:160, decimals:6,  renderer:function(val, meta, record) { return this._markUtilization_(val, meta, record); }, flex:1})
		.addTextColumn({ name:"originalPriceCurrencyCode", dataIndex:"originalPriceCurrencyCode", width:80,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addTextColumn({ name:"originalPriceUnitCode", dataIndex:"originalPriceUnitCode", width:60,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addNumberColumn({ name:"settlementPrice", dataIndex:"settlementPrice", width:120, decimals:6,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addNumberColumn({ name:"contractAmount", dataIndex:"contractAmount", width:120, decimals:6,  flex:1, renderer:function(val, meta, record) { return this._markUtilization_(val, meta, record); }})
		.addNumberColumn({ name:"vatAmount", dataIndex:"vatAmount", width:120, decimals:6,  renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addNumberColumn({ name:"exchangeRate", dataIndex:"exchangeRate", hidden:true, width:120, decimals:6,  renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_changeToSelectable_: function(value,meta) {
		
						meta.innerCls = "x-selectable";
						return value;
	},
	
	_markUtilization_: function(value,meta,record) {
		
						var used = record.get("used");
						meta.innerCls = "x-selectable";
						if (!Ext.isEmpty(value)) {
							if(used === true) {
					        	meta.style = "font-weight:bold !important";
						    } 
							return Ext.util.Format.number(value,Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_crncy));
						}
	},
	
	_afterInitComponent_: function() {
		
		
						var dc = this._controller_;
						var headerCt = this.getView().getHeaderCt();
						var convertedPriceColumn = headerCt.down("[dataIndex=settlementPrice]");
						var contractAmountColumn = headerCt.down("[dataIndex=contractAmount]");
		
						dc.on("afterDoQuerySuccess", function() {
		
							var store = dc.store;
							var contractRows = [];
		
							store.each(function(record, index){
								var row = this.getView().getRow(index);
								contractRows.push({
									code: record.get("contractCode"),
									row: row
								});
							}, this);
		
							for (var i=0, len = contractRows.length; i<len; i++) {
								if (i+1 < len && contractRows[i].code !== contractRows[i+1].code) {
									contractRows[i].row.className += " sone-row-delimiter";
								}
							}
		
							var frame = dc.getFrame();
							var invoice = frame._getDc_("fuelEvent");
							var r = invoice.getRecord();
							var settCrncy;
							var settUnit;
							if (r) {
								settCrncy = r.get("billCurrencyCode");
								settUnit  = r.get("settlementUnitCode");
								if (!Ext.isEmpty(settCrncy) && !Ext.isEmpty(settUnit)) {
									convertedPriceColumn.setText(convertedPriceColumn.initialConfig.header+" ("+settCrncy+"/"+settUnit+")");
									contractAmountColumn.setText(contractAmountColumn.initialConfig.header+" ("+settCrncy+")");
								}
								else {
									convertedPriceColumn.setText(convertedPriceColumn.initialConfig.header);
									contractAmountColumn.setText(contractAmountColumn.initialConfig.header);
								}
							}
						}, this);
	}
});
