/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.frame.FuelEvents_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.FuelEvents_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("fuelEvent", Ext.create(atraxo.acc.ui.extjs.dc.FuelEvent_Dc,{trackEditMode: true}))
		.addDc("deliveryNote", Ext.create(atraxo.acc.ui.extjs.dc.DeliveryNote_Dc,{}))
		.addDc("deliveryNoteContract", Ext.create(atraxo.acc.ui.extjs.dc.DeliveryNoteContract_Dc,{}))
		.addDc("purchaseOrder", Ext.create(atraxo.acc.ui.extjs.dc.FuelEventPurchaseOrder_Dc,{}))
		.addDc("saleContract", Ext.create(atraxo.acc.ui.extjs.dc.FuelEventSaleContract_Dc,{}))
		.addDc("fuelEventHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("fuelEventsDetails", Ext.create(atraxo.acc.ui.extjs.dc.FuelEventsDetails_Dc,{}))
		.linkDc("deliveryNote", "fuelEvent",{fetchMode:"auto",fields:[
					{childField:"fuelEventId", parentField:"id"}]})
				.linkDc("deliveryNoteContract", "deliveryNote",{fetchMode:"auto",fields:[
					{childField:"deliveryNoteId", parentField:"id"}]})
				.linkDc("purchaseOrder", "fuelEvent",{fetchMode:"auto",fields:[
					{childField:"id", parentField:"id"}]})
				.linkDc("saleContract", "fuelEvent",{fetchMode:"auto",fields:[
					{childField:"id", parentField:"id"}]})
				.linkDc("fuelEventHistory", "fuelEvent",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("fuelEventsDetails", "fuelEvent",{fields:[
					{childField:"fuelEventId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnDetailsDeliveryNote",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnDetailsDeliveryNote,stateManager:[{ name:"selected_not_zero", dc:"deliveryNote"}], scope:this})
		.addButton({name:"btnDnDetalis",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnDnDetalis,stateManager:[{ name:"selected_not_zero", dc:"deliveryNoteContract"}], scope:this})
		.addButton({name:"btnPurchaseOrderOpen",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnPurchaseOrderOpen,stateManager:[{ name:"selected_not_zero", dc:"purchaseOrder"}], scope:this})
		.addButton({name:"btnSaleContractOpen",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnSaleContractOpen,stateManager:[{ name:"selected_not_zero", dc:"saleContract"}], scope:this})
		.addButton({name:"btnRelease",glyph:fp_asc.play_glyph.glyph,iconCls: fp_asc.play_glyph.css, disabled:true, handler: this.onBtnRelease,stateManager:[{ name:"selected_not_zero", dc:"fuelEvent", and: function(dc) {return (this.canRelease(dc));} }], scope:this})
		.addButton({name:"btnExportWithMenu",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"fuelEvent"}], scope:this})
		.addButton({name:"btnExportSelected",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:true,  _btnContainerName_:"btnExportWithMenu", _isDefaultHandler_:true, handler: this.onBtnExportSelected,stateManager:[{ name:"selected_not_zero", dc:"fuelEvent", and: function(dc) {return (this.canResend(dc));} }], scope:this})
		.addButton({name:"btnExportFailed",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:false,  _btnContainerName_:"btnExportWithMenu", handler: this.onBtnExportFailed, scope:this})
		.addButton({name:"btnSaveCloseOnHoldHistoryWdwList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseOnHoldHistoryWdwList, scope:this})
		.addButton({name:"btnCloseOnHoldHistoryWdwList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseOnHoldHistoryWdwList, scope:this})
		.addButton({name:"btnDeleteFuelEvent",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteFuelEvent,stateManager:[{ name:"selected_one_clean", dc:"fuelEvent", and: function(dc) {return (this.canDeleteFuelEvent(dc));} }], scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdwEdit",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwEdit, scope:this})
		.addDcGridView("fuelEvent", {name:"FuelEventsList", xtype:"acc_FuelEvent_Dc$List"})
		.addDcFilterFormView("fuelEvent", {name:"FueleventsFilter", xtype:"acc_FuelEvent_Dc$Filter"})
		.addDcFormView("fuelEvent", {name:"FuelEventForm", xtype:"acc_FuelEvent_Dc$Form"})
		.addDcGridView("deliveryNote", {name:"DeliveyNoteList", height:"50%", xtype:"acc_DeliveryNote_Dc$List"})
		.addDcGridView("deliveryNoteContract", {name:"DeliveryNoteContract", _hasTitle_:true, xtype:"acc_DeliveryNoteContract_Dc$pcList"})
		.addDcGridView("purchaseOrder", {name:"PurchaseOrder", _hasTitle_:true, xtype:"acc_FuelEventPurchaseOrder_Dc$Orders"})
		.addDcGridView("saleContract", {name:"SaleContract", _hasTitle_:true, xtype:"acc_FuelEventSaleContract_Dc$Contracts"})
		.addDcGridView("fuelEventHistory", {name:"FuelReleasedHistory", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("fuelEvent", {name:"fuelTicketRemarkEditList", xtype:"acc_FuelEvent_Dc$EditRemark", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcGridView("fuelEventsDetails", {name:"fuelEventDetailsList", xtype:"acc_FuelEventsDetails_Dc$List"})
		.addPanel({name:"detailsTab", height:"50%", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"deliveryNoteGrids", layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwOnHoldReasonList", _hasTitle_:true, width:455, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("fuelTicketRemarkEditList")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseOnHoldHistoryWdwList"), this._elems_.get("btnCloseOnHoldHistoryWdwList")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"deliveryNoteGrids", dcName:"deliveryNoteContract"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("detailsTab", ["DeliveryNoteContract"])
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["FuelEventsList"], ["center"])
		.addChildrenTo("canvas2", ["FuelEventForm", "deliveryNoteGrids"], ["north", "center"])
		.addChildrenTo("deliveryNoteGrids", ["DeliveyNoteList", "detailsTab"], ["center", "south"])
		.addToolbarTo("FuelEventsList", "tlbFuelEventsList")
		.addToolbarTo("FuelEventForm", "tlbFuelEventForm")
		.addToolbarTo("DeliveyNoteList", "tlbDeliveryNoteList")
		.addToolbarTo("DeliveryNoteContract", "tlbDeliveryNoteContract")
		.addToolbarTo("PurchaseOrder", "tlbPurchaseOrder")
		.addToolbarTo("SaleContract", "tlbSaleContract");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["SaleContract", "PurchaseOrder", "FuelReleasedHistory"])
		.addChildrenTo2("main", ["canvas2"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbFuelEventsList", {dc: "fuelEvent"})
			.addButtons([])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnDeleteFuelEvent"),this._elems_.get("helpWdw"),this._elems_.get("btnRelease"),this._elems_.get("btnExportWithMenu"),this._elems_.get("btnExportSelected"),this._elems_.get("btnExportFailed")])
			.addReports()
		.end()
		.beginToolbar("tlbFuelEventForm", {dc: "fuelEvent"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdwEdit")])
			.addReports()
		.end()
		.beginToolbar("tlbDeliveryNoteList", {dc: "deliveryNote"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnDetailsDeliveryNote")])
			.addReports()
		.end()
		.beginToolbar("tlbDeliveryNoteContract", {dc: "deliveryNoteContract"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnDnDetalis")])
			.addReports()
		.end()
		.beginToolbar("tlbPurchaseOrder", {dc: "purchaseOrder"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnPurchaseOrderOpen")])
			.addReports()
		.end()
		.beginToolbar("tlbSaleContract", {dc: "saleContract"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnSaleContractOpen")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnDetailsDeliveryNote
	 */
	,onBtnDetailsDeliveryNote: function() {
		this._showDetails_();
	}
	
	/**
	 * On-Click handler for button btnDnDetalis
	 */
	,onBtnDnDetalis: function() {
		this.doDetails();
	}
	
	/**
	 * On-Click handler for button btnPurchaseOrderOpen
	 */
	,onBtnPurchaseOrderOpen: function() {
		this.openPurchaseOrder();
	}
	
	/**
	 * On-Click handler for button btnSaleContractOpen
	 */
	,onBtnSaleContractOpen: function() {
		this.openSaleContract();
	}
	
	/**
	 * On-Click handler for button btnRelease
	 */
	,onBtnRelease: function() {
		var successFn = function() {
			this._getDc_("fuelEventHistory").doReloadPage();
			this._applyStateAllButtons_();
			this._getDc_("fuelEvent").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"releaseList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("fuelEvent").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnExportSelected
	 */
	,onBtnExportSelected: function() {
		this._showConfirmExportFuelEventsToNAV('selected')
	}
	
	/**
	 * On-Click handler for button btnExportFailed
	 */
	,onBtnExportFailed: function() {
		this._showConfirmExportFuelEventsToNAV('failed')
	}
	
	/**
	 * On-Click handler for button btnSaveCloseOnHoldHistoryWdwList
	 */
	,onBtnSaveCloseOnHoldHistoryWdwList: function() {
		var successFn = function(dc) {
			this._getWindow_("wdwOnHoldReasonList").close();
			dc.setParamValue("remark","")
			this._getDc_("fuelEventHistory").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			this._getWindow_("wdwOnHoldReasonList").close();
			Main.rpcFailure(response);
		};
		var o={
			name:"putOnHoldList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("fuelEvent").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCloseOnHoldHistoryWdwList
	 */
	,onBtnCloseOnHoldHistoryWdwList: function() {
		this._getWindow_("wdwOnHoldReasonList").close();
	}
	
	/**
	 * On-Click handler for button btnDeleteFuelEvent
	 */
	,onBtnDeleteFuelEvent: function() {
		this.deleteFuelEvent();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwEdit
	 */
	,onHelpWdwEdit: function() {
		this.openHelp();
	}
	
	,doDetails: function() {	
		var bundle = "atraxo.mod.cmm";
		var frame = "atraxo.cmm.ui.extjs.frame.Contract_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				contractCode: this._getDc_("deliveryNoteContract").getRecord().get("contractCode")
			},
			callback: function (params) {
				this._when_called_for_details_(params);
			}
		});
	}
	
	,openPurchaseOrder: function() {	
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelOrderLocation_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("purchaseOrder").getRecord().get("orderLocationId")
			},
			callback: function (params) {
				this._when_called_from_fuel_event(params);
			}
		});
	}
	
	,openSaleContract: function() {	
		var bundle = "atraxo.mod.cmm";
		var frame = "atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				contractCode: this._getDc_("saleContract").getRecord().get("contractCode")
			},
			callback: function (params) {
				this._when_called_for_details_edit(params);
			}
		});
	}
	
	,_onShow_from_externalInterfaceMessageHistory_: function(params) {
			
						var dc = this._getDc_("fuelEvent");
						dc.setFilterValue("id", params.objectId);
						dc.doQuery({queryFromExternalInterface: true});
	}
	
	,canDeleteFuelEvent: function(dc) {
		
						if(dc.record.data.receivedStatus !== __ACC_TYPE__.ReceivedStatus._ON_HOLD_ || (!Ext.isEmpty(dc.record.data.resaleFuelEventInvoiceStatus) && dc.record.data.resaleFuelEventResaleStatus !== __ACC_TYPE__.ReceivedStatus._ON_HOLD_)){
							return false;
						} 
						return true;
	}
	
	,deleteFuelEvent: function() {
		
						var dc = this._getDc_("fuelEvent");
						var selectedRecord = dc.selectedRecords[0];	
						if(selectedRecord.get("isResale")){
							var anotherStatus = selectedRecord.get("resaleFuelEventResaleStatus");
							if(!Ext.isEmpty(anotherStatus) && anotherStatus === __ACC_TYPE__.ReceivedStatus._ON_HOLD_){						
								Ext.Msg.show({
						       		title : "Delete resale fuel event",
						       		msg : "The other resale fuel event will be, also, deleted!",
						       		icon : Ext.MessageBox.WARNING,
						       		buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
						       		fn : function(btnId) {
						            	if( btnId == "yes" ){
						                     dc.doDelete();
				              			}
				       				},
				       				scope : this
								});
							}
						}else{
							dc.doDelete();
						}
	}
	
	,_showDetails_: function() {
		
					var dc = this._getDc_("deliveryNote");
					var rec = dc.getRecord();
					var objectType = rec.get("objectType"); 
					var objectId = rec.get("objectId");
					var bundle = "";
					var frame = "";
		
					switch(objectType) {
					    case "FuelTicket":
					        bundle = "atraxo.mod.ops";
							frame = "atraxo.ops.ui.extjs.frame.FuelTicket_Ui";
					        break;
					    case "InvoiceLine":
					        bundle = "atraxo.mod.acc";
							frame = "atraxo.acc.ui.extjs.frame.Invoices_Ui";
					        break;
						case "FlightEvent":
					        bundle = "atraxo.mod.ops";
							frame = "atraxo.ops.ui.extjs.frame.FuelOrderLocation_Ui";
					        break;
					}
		
					getApplication().showFrame(frame,{
						url:Main.buildUiPath(bundle, frame, false),
						params: {
							id: objectId
						},
						callback: function (params) {
							this._when_called_from_delivery_notes_(params);
						}
					});
	}
	
	,_afterDefineElements_: function() {
		
		
						this._getBuilder_().change("FuelEventsList",{
							listeners: {
								render: {
									scope: this,
									fn: function(view) {
										var dockedItems = view.dockedItems.items;
										Ext.each(dockedItems, function(i) {
											if (i.xtype == "pagingtoolbar") {//										
												i.on("boxready", function(tlb) {
													tlb.down("numberfield").width = 50;
												}, this);
											}
										});
									}
								}
							}
						});
						
	}
	
	,_afterViewAndFilters_: function() {
		
					var fuelEvent = this._getDc_("fuelEvent");
					fuelEvent.on("afterDoQuerySuccess", function(dc, ajaxResult) {
						if (ajaxResult.options.showFilteredEdit === true) {
							this._showStackedViewElement_("main", "canvas2");
						}
						if(ajaxResult.options.queryFromExternalInterface === true) {
							if (ajaxResult.records.length != 0) {
								this._showStackedViewElement_("main", "canvas2");
							} else {
								Main.info(Main.translate("applicationMsg", "externalInterfaceViewNoObj__lbl"));
								return;
							}
						}
					}, this);
		
					fuelEvent.on("recordChange", function() {
						var fuelEventsList = this._get_("FuelEventsList");
						var expander = fuelEventsList.getPlugin("rowexpanderplus");
						expander.collapseLastRow();
					}, this);
	}
	
	,_when_called_from_receivable_accruals_: function(params) {
		    
		                var dc = this._getDc_("fuelEvent");
						dc.doClearAllFilters();
						dc.setFilterValue("id", params.fuelEventId);				
						dc.doQuery({showFilteredEdit: true});
	}
	
	,_when_called_from_payable_accruals_: function(params) {
		    
		                var dc = this._getDc_("fuelEvent");
						dc.doClearAllFilters();
						dc.setFilterValue("id", params.fuelEventId);				
						dc.doQuery({showFilteredEdit: true});
	}
	
	,openHelp: function() {
		
						var url = Main.urlHelp+"/FuelEvents.html";
						window.open( url, "SONE_Help");
	}
	
	,setupHistoryWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(false);
						remarksField.setReadOnly(false);
						
						remarksField.fieldLabel = label;
						window.title = title;
						
						window.show();
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
	}
	
	,_showConfirmExportFuelEventsToNAV: function(param) {
		
						Ext.Msg.show({
						       title : "Resend Fuel Events",
						       msg : "You are about to resend fuel events to the ERP system. Are you sure?",
						       icon : Ext.MessageBox.WARNING,
						       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
						       fn : function(btnId) {
						              if( btnId == "yes" ){
						                     this._exportFuelEventsToNAV(param);
				              }
				       },
				       scope : this
						});
	}
	
	,_exportFuelEventsToNAV: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.exportFuelEventDescription;							
								var responseResult = responseData.params.exportFuelEventResult;
								if (responseResult == true) {
									Main.info(responseDescription); 
								}
								else {
									Main.error(responseDescription);
								}
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("fuelEvent");
						dc.setParamValue("exportFuelEventOption", param);
		                var t = {
					        name: "exportFuelPlusToNAVFuelEvent",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcDataList(t);                
	}
	
	,canRelease: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var receivedStatus =  selectedRecords[i].get("receivedStatus");
							var status =  selectedRecords[i].get("status");
							if (( receivedStatus===__ACC_TYPE__.ReceivedStatus._ON_HOLD_ && status===__ACC_TYPE__.FuelEventStatus._CANCELED_ ) || receivedStatus===__ACC_TYPE__.ReceivedStatus._RELEASED_) {
								return false;			
							}
						}
						return true;
	}
	
	,canResend: function(dc) {
		
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var receivedStatus =  selectedRecords[i].get("receivedStatus");
							var status =  selectedRecords[i].get("status");
							if ( receivedStatus===__ACC_TYPE__.ReceivedStatus._ON_HOLD_ && status===__ACC_TYPE__.FuelEventStatus._CANCELED_ ) {
								return false;			
							}
						}
						return true;
	}
});
