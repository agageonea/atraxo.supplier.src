/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.Contract_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_Contract_Ds"
	},
	
	
	fields: [
		{name:"supplierId", type:"int", allowNull:true},
		{name:"supplierCode", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"status", type:"string"},
		{name:"scope", type:"string"},
		{name:"eventType", type:"string"},
		{name:"subType", type:"string"},
		{name:"type", type:"string"},
		{name:"code", type:"string"},
		{name:"id", type:"int", allowNull:true},
		{name:"flightType", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.Contract_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"supplierId", type:"int", allowNull:true},
		{name:"supplierCode", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"status", type:"string"},
		{name:"scope", type:"string"},
		{name:"eventType", type:"string"},
		{name:"subType", type:"string"},
		{name:"type", type:"string"},
		{name:"code", type:"string"},
		{name:"id", type:"int", allowNull:true},
		{name:"flightType", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
