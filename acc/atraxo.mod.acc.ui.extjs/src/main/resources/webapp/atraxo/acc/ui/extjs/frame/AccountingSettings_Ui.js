/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.frame.AccountingSettings_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.AccountingSettings_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("invoiceNumberSerie", Ext.create(atraxo.acc.ui.extjs.dc.InvoiceNumberSerie_Dc,{multiEdit: true}))
		.addDc("vat", Ext.create(atraxo.fmbas.ui.extjs.dc.Vat_Dc,{multiEdit: true}))
		.addDc("vatRate", Ext.create(atraxo.fmbas.ui.extjs.dc.VatRate_Dc,{multiEdit: true}))
		.linkDc("vatRate", "vat",{fetchMode:"auto",fields:[
					{childField:"vatId", parentField:"id"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpInvWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpInvWdw, scope:this})
		.addButton({name:"enableInvNumSerie",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onEnableInvNumSerie,stateManager:[{ name:"selected_one_clean", dc:"invoiceNumberSerie", and: function() {return (this._enableIfDisabled_());} }], scope:this})
		.addButton({name:"disableInvNumSerie",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onDisableInvNumSerie,stateManager:[{ name:"selected_one_clean", dc:"invoiceNumberSerie", and: function() {return (this._enableIfEnabled_());} }], scope:this})
		.addDcFilterFormView("invoiceNumberSerie", {name:"invNumSerieFilter", xtype:"acc_InvoiceNumberSerie_Dc$Filter"})
		.addDcEditGridView("invoiceNumberSerie", {name:"invNumSerieList", _hasTitle_:true, xtype:"acc_InvoiceNumberSerie_Dc$List", frame:true})
		.addDcEditGridView("vat", {name:"VatList", height:300, xtype:"fmbas_Vat_Dc$List", frame:true})
		.addDcFilterFormView("vat", {name:"VatFilter", xtype:"fmbas_Vat_Dc$Filter"})
		.addDcEditGridView("vatRate", {name:"VatRateList", xtype:"fmbas_VatRate_Dc$List", frame:true})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false,  cls:"sone-tab-two-levels"})
		.addPanel({name:"vatPanel", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["vatPanel"])
		.addChildrenTo("vatPanel", ["VatList", "VatRateList"], ["north", "center"])
		.addToolbarTo("VatList", "tlbVatList")
		.addToolbarTo("VatRateList", "tlbVatRateList")
		.addToolbarTo("invNumSerieList", "tlbInvNumSerieList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("canvas1", ["invNumSerieList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbVatList", {dc: "vat"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbVatRateList", {dc: "vatRate"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbInvNumSerieList", {dc: "invoiceNumberSerie"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("enableInvNumSerie"),this._elems_.get("disableInvNumSerie"),this._elems_.get("helpInvWdw")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpInvWdw
	 */
	,onHelpInvWdw: function() {
		this.openInvHelp();
	}
	
	/**
	 * On-Click handler for button enableInvNumSerie
	 */
	,onEnableInvNumSerie: function() {
		this._showWarningOnEnable_();
	}
	
	/**
	 * On-Click handler for button disableInvNumSerie
	 */
	,onDisableInvNumSerie: function() {
		var o={
			name:"disable",
			modal:true
		};
		this._getDc_("invoiceNumberSerie").doRpcData(o);
	}
	
	,_afterDefineDcs_: function() {
		
						var invNS = this._getDc_("invoiceNumberSerie");
		
						invNS.on("afterDoSaveSuccess", function(dc) {				
							this._applyStateAllButtons_();
							dc.doReloadPage();
						} , this );
	}
	
	,_enableIfDisabled_: function() {
		
						var dc = this._getDc_("invoiceNumberSerie");
						var r = dc.getRecord();
						var result = true;
						if (r) {
							var enabled = r.get("enabled");
							if (enabled === true) {
								result = false;
							}
						}
						return result;
	}
	
	,_enableIfEnabled_: function() {
		
						var dc = this._getDc_("invoiceNumberSerie");
						var r = dc.getRecord();
						var result = true;
						if (r) {
							var enabled = r.get("enabled");
							if (enabled === false) {
								result = false;
							}
						}
						return result;
	}
	
	,_enableRpc_: function(dc) {
		
						var successFn = function(dc) {
							dc.doReloadPage();
						}
						var o={
							name:"enable",
							modal:true,
							callbacks:{
								successFn: successFn,
								successScope: this
							}
						};
						dc.doRpcData(o);
	}
	
	,_showWarningOnEnable_: function() {
		
		
						var dc = this._getDc_("invoiceNumberSerie");
						var r = dc.getRecord();
						var type;
						if (r) {
							type = r.get("type");
						}	
						var ds = "acc_InvoiceNumberSerie_Ds";
						var alreadyEnabledCounter = 0;
						Ext.Ajax.request({
			            	url: Main.dsAPI(ds, "json").read,
			            	method: "POST",
			            	scope: this,
			            	success: function(response) {
								var data = Ext.getResponseDataInJSON(response).data;
								var dc = this._getDc_("invoiceNumberSerie");
								Ext.each(data, function(item) {
									if (item.enabled === true && item.type === type ) {
										alreadyEnabledCounter ++;
									}
								}, this);
								if (alreadyEnabledCounter > 0) {
									var context = this;
									var execFn = function() {
										context._enableRpc_(dc);
									}
									Main.warning(Main.translate("applicationMsg","documentNumberSeriesEnableWarning1__lbl") + type + Main.translate("applicationMsg","documentNumberSeriesEnableWarning2__lbl"), null, execFn); 
								}
								else {
									this._enableRpc_(dc);
								}
							}
						});				
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/Vat.html";
					window.open( url, "SONE_Help");
	}
	
	,openInvHelp: function() {
		
					var url = Main.urlHelp+"/InvoiceNumberSeries.html";
					window.open( url, "SONE_Help");
	}
});
