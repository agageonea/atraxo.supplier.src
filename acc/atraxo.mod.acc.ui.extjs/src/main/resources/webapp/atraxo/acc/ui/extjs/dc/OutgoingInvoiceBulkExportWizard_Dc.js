/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportWizard_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.acc.ui.extjs.ds.OutgoingInvoice_DsParam,
	recordModel: atraxo.acc.ui.extjs.ds.OutgoingInvoice_Ds
});

/* ================= EDIT FORM: wizardHeader ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportWizard_Dc$wizardHeader", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_OutgoingInvoiceBulkExportWizard_Dc$wizardHeader",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLabel({ name:"phase1"})
		.addLabel({ name:"phase2"})
		/* =========== containers =========== */
		.addPanel({name:"main"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addWizardLabels("main", ["phase1", "phase2"]);
	}
});

/* ================= EDIT FORM: bulkInvoiceExportStep1 ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportWizard_Dc$bulkInvoiceExportStep1", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_OutgoingInvoiceBulkExportWizard_Dc$bulkInvoiceExportStep1",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"step1DescriptionLabel", bind:"{d.labelField}", dataIndex:"labelField", width:500, maxLength:32})
		.addLov({name:"receiverCode", bind:"{d.receiverCode}", dataIndex:"receiverCode", width:150, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsParam: "receiverIdParam"} ,{lovField:"name", dsField: "receiverName"} ]})
		.addLov({name:"delLocCode", bind:"{d.delLocCode}", dataIndex:"delLocCode", width:150, xtype:"fmbas_LocationsAreasLov_Lov", maxLength:25, labelAlign:"top",
			retFieldMapping: [{lovField:"type", dsField: "deliveryType"} ,{lovField:"id", dsField: "deliveryId"} ]})
		.addDisplayFieldText({ name:"periodLabel", bind:"{d.labelField}", dataIndex:"labelField", width:300, maxLength:32, labelAlign:"left", style:"float: left"})
		.addDisplayFieldText({ name:"periodSeparator", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"padding: 0px 0px 0px 5px"})
		.addDateField({name:"dateFrom", bind:"{p.dateFrom}", paramIndex:"dateFrom", allowBlank:false, width:100, noLabel: true,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addDateField({name:"dateTo", bind:"{p.dateTo}", paramIndex:"dateTo", allowBlank:false, width:100, noLabel: true,listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		.addLov({name:"reportName", bind:"{p.reportName}", paramIndex:"reportName", allowBlank:false, width:150, xtype:"ad_ExternalReportLov_Lov", maxLength:100, labelAlign:"top",
			retFieldMapping: [{lovField:"id", dsParam: "reportCode"} ],
			filterFieldMapping: [{lovField:"businessArea", value: "Sales invoices"} ],listeners:{
			change:{scope:this, fn:this._enableContinue_}
		}})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:500})
		.addPanel({ name:"row0", width:500, layout: {type:"table"}})
		.addPanel({ name:"row1", width:500, layout:"anchor"})
		.addPanel({ name:"row2", width:500, layout:"anchor"})
		.addPanel({ name:"row3label", width:500, layout: {type:"table"}})
		.addPanel({ name:"row3", width:500, layout: {type:"table"}})
		.addPanel({ name:"row4", width:500, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["row0", "row1", "row2", "row3label", "row3", "row4"])
		.addChildrenTo("row0", ["step1DescriptionLabel"])
		.addChildrenTo("row1", ["receiverCode"])
		.addChildrenTo("row2", ["delLocCode"])
		.addChildrenTo("row3label", ["periodLabel"])
		.addChildrenTo("row3", ["dateFrom", "periodSeparator", "dateTo"])
		.addChildrenTo("row4", ["reportName"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableContinue_: function() {
		
		
						var dc = this._controller_;
						var frame= dc.getFrame();
						var btn = frame._get_("btnContinueStep1");
						var ctx = this;
						
						var isTimePeriodCorrectlySet = ctx._get_("dateFrom").getValue() <= ctx._get_("dateTo").getValue();
						var originalLabel = Main.translate("invoiceBulkExportSelectBtns", "invoiceDateBetween__lbl");
						var warningLabel = "<span style='color:red'>" + Main.translate("invoiceBulkExportSelectBtns", "invalidDateLbl__lbl") + "</span>";
		
						if (!Ext.isEmpty(ctx._get_("dateFrom").getValue()) && !Ext.isEmpty(ctx._get_("dateTo").getValue())) {
							
							var periodLabel = this._get_("periodLabel");
							if (!isTimePeriodCorrectlySet) {
								periodLabel.labelEl.update(originalLabel + warningLabel);
							} else {
								periodLabel.labelEl.update(originalLabel);
							}
						}
						
						var field = ["dateFrom","dateTo","reportName"];
						var dirtyFields = [];
		
						for (var i = 0; i < field.length; i++) {
							if (Ext.isEmpty(ctx._get_(field[i]).getValue())) {							
								dirtyFields.push(field[i]);
							}
							else {
								var index = dirtyFields.indexOf(field[i]);
								if (index > -1) {
									dirtyFields.splice(index, 1);
								}
							}					
						}
						if (dirtyFields.length > 0 || !isTimePeriodCorrectlySet) {
							btn._disable_();
						}
						else {
							btn._enable_();
						}
	}
});
