/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.PayableAccrualsLocGroupping_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.PayableAccrualLocGrouping_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.acc.ui.extjs.dc.PayableAccrualsLocGroupping_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_PayableAccrualsLocGroupping_Dc$List",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"month", dataIndex:"month", width:100,  filter:true})
		.addTextColumn({ name:"location", dataIndex:"location", width:100,  filter:"list"})
		.addNumberColumn({ name:"events", dataIndex:"events", width:80})
		.addNumberColumn({ name:"netAmount", dataIndex:"netAmount", width:130, decimals:6,  flex:1, renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"vatAmount", dataIndex:"vatAmount", width:130, decimals:6,  flex:1, renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"grossAmount", dataIndex:"grossAmount", width:130, decimals:6,  flex:1, renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_endDefine_: function() {
		
						this.plugins = [].concat(this.plugins?this.plugins:[],[
							{
								ptype: "gridfilters"
							}
						]);
	},
	
	_injectSysCurrency_: function() {
		
		
						var sysCurrency = _SYSTEMPARAMETERS_.syscrncy;
						var view = this.getView();
						var headerCt = view.getHeaderCt();
		
						var netAmount = headerCt.down("[dataIndex=netAmount]");
						var vatAmount = headerCt.down("[dataIndex=vatAmount]");
						var grossAmount = headerCt.down("[dataIndex=grossAmount]");
		
						netAmount.setText(netAmount.initialConfig.header+" ("+sysCurrency+")");
						vatAmount.setText(vatAmount.initialConfig.header+" ("+sysCurrency+")");
						grossAmount.setText(grossAmount.initialConfig.header+" ("+sysCurrency+")");
	},
	
	_afterInitComponent_: function() {
		
						this._injectSysCurrency_();
	}
});
