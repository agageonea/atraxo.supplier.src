/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingRefInvoiceLine_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_OutgoingRefInvoiceLine_Ds"
	},
	
	
	fields: [
		{name:"fuelEvId", type:"int", allowNull:true},
		{name:"outInvId", type:"int", allowNull:true},
		{name:"refInvId", type:"int", allowNull:true},
		{name:"custId", type:"int", allowNull:true},
		{name:"custCode", type:"string"},
		{name:"destId", type:"int", allowNull:true},
		{name:"destCode", type:"string"},
		{name:"invLineId", type:"int", allowNull:true},
		{name:"deliveryDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"aircraftRegistrationNumber", type:"string"},
		{name:"flightSuffix", type:"string"},
		{name:"flightType", type:"string"},
		{name:"flightNo", type:"string"},
		{name:"ticketNo", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"amount", type:"float", allowNull:true},
		{name:"vat", type:"float", allowNull:true},
		{name:"isCredited", type:"boolean"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingRefInvoiceLine_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"fuelEvId", type:"int", allowNull:true},
		{name:"outInvId", type:"int", allowNull:true},
		{name:"refInvId", type:"int", allowNull:true},
		{name:"custId", type:"int", allowNull:true},
		{name:"custCode", type:"string"},
		{name:"destId", type:"int", allowNull:true},
		{name:"destCode", type:"string"},
		{name:"invLineId", type:"int", allowNull:true},
		{name:"deliveryDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"aircraftRegistrationNumber", type:"string"},
		{name:"flightSuffix", type:"string"},
		{name:"flightType", type:"string"},
		{name:"flightNo", type:"string"},
		{name:"ticketNo", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"amount", type:"float", allowNull:true},
		{name:"vat", type:"float", allowNull:true},
		{name:"isCredited", type:"boolean", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.acc.ui.extjs.ds.OutgoingRefInvoiceLine_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"invoiceId", type:"int", allowNull:true}
	]
});
