/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.FuelEventPurchaseOrder_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_FuelEventPurchaseOrder_Ds"
	},
	
	
	fields: [
		{name:"eventType", type:"string"},
		{name:"customerCode", type:"string"},
		{name:"billableCost", type:"float", allowNull:true},
		{name:"billabelCostCurrency", type:"string"},
		{name:"orderCode", type:"string", noFilter:true, noSort:true},
		{name:"operationalType", type:"string", noFilter:true, noSort:true},
		{name:"product", type:"string", noFilter:true, noSort:true},
		{name:"convertedAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"systemCurrency", type:"string", noFilter:true, noSort:true},
		{name:"orderLocationId", type:"int", allowNull:true},
		{name:"fuelingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.FuelEventPurchaseOrder_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"eventType", type:"string"},
		{name:"customerCode", type:"string"},
		{name:"billableCost", type:"float", allowNull:true},
		{name:"billabelCostCurrency", type:"string"},
		{name:"orderCode", type:"string", noFilter:true, noSort:true},
		{name:"operationalType", type:"string", noFilter:true, noSort:true},
		{name:"product", type:"string", noFilter:true, noSort:true},
		{name:"convertedAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"systemCurrency", type:"string", noFilter:true, noSort:true},
		{name:"orderLocationId", type:"int", allowNull:true},
		{name:"fuelingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
