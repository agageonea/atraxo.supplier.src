/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.PayableAccrualsGroupping_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.PayableAccrualGrouping_Ds
});

/* ================= GRID: mainListGroupping ================= */

Ext.define("atraxo.acc.ui.extjs.dc.PayableAccrualsGroupping_Dc$mainListGroupping", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_PayableAccrualsGroupping_Dc$mainListGroupping",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"month", dataIndex:"month", width:100,  filter:true})
		.addNumberColumn({ name:"events", dataIndex:"events", width:80})
		.addNumberColumn({ name:"netAmount", dataIndex:"netAmount", width:130, decimals:6,  flex:1, renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"vatAmount", dataIndex:"vatAmount", width:130, decimals:6,  flex:1, renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"grossAmount", dataIndex:"grossAmount", width:130, decimals:6,  flex:1, renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_endDefine_: function() {
		
						this.plugins = [].concat(this.plugins?this.plugins:[],[
							{
								ptype: "gridfilters"
							},{
								ptype: "rowexpanderplus",
								rowBodyTpl : new Ext.XTemplate(
									"<div id='rec-{month}'></div>"
								)
							}
						]);
						this.selType = "rowmodel"
	},
	
	_injectSysCurrency_: function() {
		
		
						var sysCurrency = _SYSTEMPARAMETERS_.syscrncy;
						var view = this.getView();
						var headerCt = view.getHeaderCt();
		
						var netAmount = headerCt.down("[dataIndex=netAmount]");
						var vatAmount = headerCt.down("[dataIndex=vatAmount]");
						var grossAmount = headerCt.down("[dataIndex=grossAmount]");
		
						netAmount.setText(netAmount.initialConfig.header+" ("+sysCurrency+")");
						vatAmount.setText(vatAmount.initialConfig.header+" ("+sysCurrency+")");
						grossAmount.setText(grossAmount.initialConfig.header+" ("+sysCurrency+")");
	},
	
	_afterInitComponent_: function() {
		
		
						this._buildChildGrid_({
							childDc: "accruedPayLocGroupping",
							gridCls: atraxo.acc.ui.extjs.dc.PayableAccrualsLocGroupping_Dc$List,
							filterField: "month",
							callbackParams: {
								buildSecondChart: true
							}
						});	
						this._injectSysCurrency_();
		
	}
});

/* ================= EDIT FORM: Chart ================= */

Ext.define("atraxo.acc.ui.extjs.dc.PayableAccrualsGroupping_Dc$Chart", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_PayableAccrualsGroupping_Dc$Chart",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"month", bind:"{d.month}", dataIndex:"month", maxLength:32, style:"display:none"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"p1",  width:"100%", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["p1"])
		.addChildrenTo("p1", ["month"]);
	}
});
