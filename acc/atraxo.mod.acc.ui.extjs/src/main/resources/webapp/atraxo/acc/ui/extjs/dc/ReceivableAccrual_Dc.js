/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.ReceivableAccrual_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.ReceivableAccrual_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.acc.ui.extjs.dc.ReceivableAccrual_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.acc_ReceivableAccrual_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"fuelingDate", dataIndex:"feFuelingDate"})
			.addLov({name:"location", dataIndex:"locCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "locId"} ]}})
			.addLov({name:"customer", dataIndex:"custCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "custId"} ]}})
			.addLov({name:"customerName", dataIndex:"custName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "custId"} ]}})
			.addLov({name:"aircraft", dataIndex:"aicraftRegistration", maxLength:10,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AircraftLov_Lov", selectOnFocus:true, maxLength:10,
					retFieldMapping: [{lovField:"id", dsField: "aircraftId"} ]}})
			.addTextField({ name:"flightNo", dataIndex:"feFlightNumber", maxLength:32})
			.addLov({name:"supplier", dataIndex:"suppCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "suppId"} ]}})
			.addCombo({ xtype:"combo", name:"invoiceStatus", dataIndex:"invProcessStatus", store:[ __ACC_TYPE__.InvoiceProcessStatus._NOT_INVOICED_, __ACC_TYPE__.InvoiceProcessStatus._PARTIAL_INVOICED_, __ACC_TYPE__.InvoiceProcessStatus._COMPLETELY_INVOICED_, __ACC_TYPE__.InvoiceProcessStatus._CLOSED_, __ACC_TYPE__.InvoiceProcessStatus._NO_INVOICING_]})
			.addNumberField({name:"accrualQuantity", dataIndex:"accrualQuantity", maxLength:21, decimals:6})
			.addNumberField({name:"accrualAmountSys", dataIndex:"accrualAmountSys", maxLength:21, decimals:6})
			.addNumberField({name:"accrualAmountSet", dataIndex:"accrualAmountSet", sysDec:"dec_crncy", maxLength:21})
			.addNumberField({name:"accrualVATSet", dataIndex:"accrualVATSet", maxLength:21, decimals:6})
			.addNumberField({name:"accrualVATSys", dataIndex:"accrualVATSys", maxLength:21, decimals:6})
		;
	}

});

/* ================= GRID: mainList ================= */

Ext.define("atraxo.acc.ui.extjs.dc.ReceivableAccrual_Dc$mainList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_ReceivableAccrual_Dc$mainList",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"fuelingDate", dataIndex:"fuelingDate", width:100, _mask_: Masks.DATE,  renderer:Ext.util.Format.dateRenderer('m-Y'), flex:1})
		.addTextColumn({ name:"location", dataIndex:"locCode", width:80,  filter:"list", flex:1})
		.addTextColumn({ name:"customer", dataIndex:"custCode", width:80,  filter:"list", flex:1})
		.addNumberColumn({ name:"events", dataIndex:"events", width:80,  flex:1})
		.addNumberColumn({ name:"netAmount", dataIndex:"accrualAmountSys", width:100, decimals:6,  flex:1, renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"vatAmount", dataIndex:"accrualVATSys", width:100, decimals:6,  flex:1, renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"grossAmount", dataIndex:"grossAmount", width:100, decimals:6,  flex:1, renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: List ================= */

Ext.define("atraxo.acc.ui.extjs.dc.ReceivableAccrual_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_ReceivableAccrual_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"fuelingDate", dataIndex:"fuelingDate", width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"location", dataIndex:"locCode", width:80})
		.addTextColumn({ name:"customer", dataIndex:"custCode", width:80})
		.addTextColumn({ name:"customerName", dataIndex:"custName", width:150})
		.addTextColumn({ name:"registration", dataIndex:"aicraftRegistration", width:100})
		.addTextColumn({ name:"flightNo", dataIndex:"feFlightNumber", width:80})
		.addTextColumn({ name:"supplier", dataIndex:"suppCode", width:80})
		.addTextColumn({ name:"businessType", dataIndex:"businessType", width:100})
		.addNumberColumn({ name:"expectedAmountSys", dataIndex:"expectedAmountSys", width:130, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"expectedVATSys", dataIndex:"expectedVATSys", hidden:true, width:130, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"invoicedAmountSys", dataIndex:"invoicedAmountSys", width:130, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"invoicedVATSys", dataIndex:"invoicedVATSys", hidden:true, width:130, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"accrualAmountSys", dataIndex:"accrualAmountSys", width:130, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"accrualVATSys", dataIndex:"accrualVATSys", hidden:true, width:130, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addNumberColumn({ name:"expectedQuantity", dataIndex:"expectedQuantity", width:155, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_unit));}})
		.addNumberColumn({ name:"invoicedQuantity", dataIndex:"invoicedQuantity", width:155, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_unit));}})
		.addNumberColumn({ name:"accrualQuantity", dataIndex:"accrualQuantity", width:155, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_unit));}})
		.addNumberColumn({ name:"calculatedCost", dataIndex:"expectedAmountSet", hidden:true, width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"expectedVATSet", dataIndex:"expectedVATSet", hidden:true, width:130, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addTextColumn({ name:"expCurrCode", dataIndex:"expCurrCode", hidden:true, width:50})
		.addNumberColumn({ name:"invoiceCost", dataIndex:"invoicedAmountSet", hidden:true, width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"invoicedVATSet", dataIndex:"invoicedVATSet", hidden:true, width:130, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addTextColumn({ name:"invCurrCode", dataIndex:"invCurrCode", hidden:true, width:50})
		.addNumberColumn({ name:"accruedCost", dataIndex:"accrualAmountSet", hidden:true, width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"accrualVATSet", dataIndex:"accrualVATSet", hidden:true, width:130, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_amount));}})
		.addTextColumn({ name:"accruelCurrCode", dataIndex:"accruelCurrCode", hidden:true, width:50})
		.addNumberColumn({ name:"exchangeRate", dataIndex:"exchangeRate", hidden:true, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_xr));}})
		.addNumberColumn({ name:"density", dataIndex:"density", hidden:true, decimals:6,  renderer:function(value) {return Ext.util.Format.number(value, Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_unit));}})
		.addTextColumn({ name:"invoiceStatus", dataIndex:"invProcessStatus", width:140})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_injectSysCurrency_: function() {
		
		
						var sysCurrency = _SYSTEMPARAMETERS_.syscrncy;
						var view = this.getView();
						var headerCt = view.getHeaderCt();
		
						var expectedAmountSys = headerCt.down("[dataIndex=expectedAmountSys]");
						var invoicedAmountSys = headerCt.down("[dataIndex=invoicedAmountSys]");
						var accrualAmountSys = headerCt.down("[dataIndex=accrualAmountSys]");
						var expectedVATSys = headerCt.down("[dataIndex=expectedVATSys]");
						var invoicedVATSys = headerCt.down("[dataIndex=invoicedVATSys]");
						var accrualVATSys = headerCt.down("[dataIndex=accrualVATSys]");
		
						expectedAmountSys.setText(expectedAmountSys.initialConfig.header+" ("+sysCurrency+")");
						invoicedAmountSys.setText(invoicedAmountSys.initialConfig.header+" ("+sysCurrency+")");
						accrualAmountSys.setText(accrualAmountSys.initialConfig.header+" ("+sysCurrency+")");
						expectedVATSys.setText(expectedVATSys.initialConfig.header+" ("+sysCurrency+")");
						invoicedVATSys.setText(invoicedVATSys.initialConfig.header+" ("+sysCurrency+")");
						accrualVATSys.setText(accrualVATSys.initialConfig.header+" ("+sysCurrency+")");
		
	},
	
	_injectSysVolume_: function() {
		
		
						var sysVol = _SYSTEMPARAMETERS_.sysvol;
						var sysWeight = _SYSTEMPARAMETERS_.sysweight;
						var view = this.getView();
						var headerCt = view.getHeaderCt();
		
						var invoicedQuantity = headerCt.down("[dataIndex=invoicedQuantity]");
						var accrualQuantity = headerCt.down("[dataIndex=accrualQuantity]");
		                var expectedQuantity = headerCt.down("[dataIndex=expectedQuantity]");
						var density = headerCt.down("[dataIndex=density]");
		
						invoicedQuantity.setText(invoicedQuantity.initialConfig.header+" ("+sysVol+")");
						accrualQuantity.setText(accrualQuantity.initialConfig.header+" ("+sysVol+")");
		                expectedQuantity.setText(expectedQuantity.initialConfig.header+" ("+sysVol+")");
						density.setText(density.initialConfig.header+" ("+sysWeight+"/"+sysVol+")" );
		
	},
	
	_afterInitComponent_: function() {
		
						this._injectSysCurrency_();
						this._injectSysVolume_();
	}
});
