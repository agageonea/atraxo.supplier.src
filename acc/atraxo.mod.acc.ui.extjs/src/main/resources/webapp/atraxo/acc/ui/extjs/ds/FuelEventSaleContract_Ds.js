/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.FuelEventSaleContract_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_FuelEventSaleContract_Ds"
	},
	
	
	fields: [
		{name:"contractCode", type:"string", noFilter:true, noSort:true},
		{name:"type", type:"string", noFilter:true, noSort:true},
		{name:"delivery", type:"string", noFilter:true, noSort:true},
		{name:"scope", type:"string", noFilter:true, noSort:true},
		{name:"termSpot", type:"string", noFilter:true, noSort:true},
		{name:"customerCode", type:"string"},
		{name:"billableCost", type:"float", allowNull:true},
		{name:"billabelCostCurrency", type:"string"},
		{name:"convertedAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"systemCurrency", type:"string", noFilter:true, noSort:true},
		{name:"contractId", type:"int", allowNull:true},
		{name:"sourceType", type:"string"},
		{name:"fuelingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.FuelEventSaleContract_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"contractCode", type:"string", noFilter:true, noSort:true},
		{name:"type", type:"string", noFilter:true, noSort:true},
		{name:"delivery", type:"string", noFilter:true, noSort:true},
		{name:"scope", type:"string", noFilter:true, noSort:true},
		{name:"termSpot", type:"string", noFilter:true, noSort:true},
		{name:"customerCode", type:"string"},
		{name:"billableCost", type:"float", allowNull:true},
		{name:"billabelCostCurrency", type:"string"},
		{name:"convertedAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"systemCurrency", type:"string", noFilter:true, noSort:true},
		{name:"contractId", type:"int", allowNull:true},
		{name:"sourceType", type:"string"},
		{name:"fuelingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
