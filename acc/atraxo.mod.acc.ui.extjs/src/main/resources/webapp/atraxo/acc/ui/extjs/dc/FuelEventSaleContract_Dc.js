/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.FuelEventSaleContract_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.FuelEventSaleContract_Ds
});

/* ================= GRID: Contracts ================= */

Ext.define("atraxo.acc.ui.extjs.dc.FuelEventSaleContract_Dc$Contracts", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_FuelEventSaleContract_Dc$Contracts",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"contractCode", dataIndex:"contractCode", width:100})
		.addTextColumn({ name:"type", dataIndex:"type", width:100})
		.addTextColumn({ name:"deliveryTo", dataIndex:"delivery", width:100})
		.addTextColumn({ name:"scope", dataIndex:"scope", width:100})
		.addTextColumn({ name:"termSpot", dataIndex:"termSpot", width:100})
		.addTextColumn({ name:"customerCode", dataIndex:"customerCode", width:100})
		.addNumberColumn({ name:"origAmount", dataIndex:"billableCost", width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"settlementCurrency", dataIndex:"billabelCostCurrency", width:100})
		.addNumberColumn({ name:"convertedAmount", dataIndex:"convertedAmount", width:120, sysDec:"dec_crncy"})
		.addTextColumn({ name:"sysCurrency", dataIndex:"systemCurrency", width:140})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
