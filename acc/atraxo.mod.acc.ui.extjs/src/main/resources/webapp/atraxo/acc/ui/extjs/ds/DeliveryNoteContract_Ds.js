/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.DeliveryNoteContract_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_DeliveryNoteContract_Ds"
	},
	
	
	fields: [
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"contratType", type:"string"},
		{name:"contractSubType", type:"string"},
		{name:"contractScope", type:"string"},
		{name:"contractIsSpot", type:"string"},
		{name:"settCurrCode", type:"string"},
		{name:"settUnitCode", type:"string"},
		{name:"contractSuppId", type:"int", allowNull:true},
		{name:"contractSupplier", type:"string"},
		{name:"deliveryNoteId", type:"int", allowNull:true},
		{name:"payableCost", type:"float", allowNull:true},
		{name:"dnFuelingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"dnBillableCost", type:"float", allowNull:true},
		{name:"dnQuantity", type:"float", allowNull:true},
		{name:"dnUnitId", type:"int", allowNull:true},
		{name:"dnUnitCode", type:"string"},
		{name:"dnCurrId", type:"int", allowNull:true},
		{name:"dnCurr", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.DeliveryNoteContract_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"contratType", type:"string"},
		{name:"contractSubType", type:"string"},
		{name:"contractScope", type:"string"},
		{name:"contractIsSpot", type:"string"},
		{name:"settCurrCode", type:"string"},
		{name:"settUnitCode", type:"string"},
		{name:"contractSuppId", type:"int", allowNull:true},
		{name:"contractSupplier", type:"string"},
		{name:"deliveryNoteId", type:"int", allowNull:true},
		{name:"payableCost", type:"float", allowNull:true},
		{name:"dnFuelingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"dnBillableCost", type:"float", allowNull:true},
		{name:"dnQuantity", type:"float", allowNull:true},
		{name:"dnUnitId", type:"int", allowNull:true},
		{name:"dnUnitCode", type:"string"},
		{name:"dnCurrId", type:"int", allowNull:true},
		{name:"dnCurr", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
