/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.DeliveryNoteContract_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.DeliveryNoteContract_Ds
});

/* ================= GRID: pcList ================= */

Ext.define("atraxo.acc.ui.extjs.dc.DeliveryNoteContract_Dc$pcList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_DeliveryNoteContract_Dc$pcList",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"contractCode", dataIndex:"contractCode", width:100})
		.addTextColumn({ name:"contracType", dataIndex:"contratType", width:100})
		.addTextColumn({ name:"contractSubType", dataIndex:"contractSubType", width:100})
		.addTextColumn({ name:"scope", dataIndex:"contractScope", width:100})
		.addTextColumn({ name:"isSpot", dataIndex:"contractIsSpot", width:100})
		.addTextColumn({ name:"supplier", dataIndex:"contractSupplier", width:100})
		.addNumberColumn({ name:"amount", dataIndex:"dnBillableCost", width:100, decimals:6})
		.addTextColumn({ name:"currency", dataIndex:"settCurrCode", width:100})
		.addNumberColumn({ name:"convAmount", dataIndex:"payableCost", width:120, sysDec:"dec_crncy"})
		.addTextColumn({ name:"convCurr", dataIndex:"dnCurr", width:120})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
