/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.InvoiceLine_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_InvoiceLine_Ds"
	},
	
	
	validators: {
		deliveryDate: [{type: 'presence'}],
		eventDate: [{type: 'presence'}],
		quantity: [{type: 'presence'}],
		aircraftRegistrationNo: [{type: 'presence'}],
		flightNo: [{type: 'presence'}],
		flightType: [{type: 'presence'}],
		ticketNo: [{type: 'presence'}],
		amount: [{type: 'presence'}],
		airlineDesignatorId: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("taxType", "Unspecified");
		this.set("overAllCheckStatus", "Not checked");
		this.set("eventCheckStatus", "Not checked");
		this.set("quantityCheckStatus", "Not checked");
		this.set("originalVatCheckStatus", "Not checked");
		this.set("originalQuantityCheckStatus", "Not checked");
		this.set("vatCheckStatus", "Not checked");
		this.set("quantityCheckResult", "");
		this.set("vatCheckResult", "");
	},
	
	fields: [
		{name:"invoiceId", type:"int", allowNull:true},
		{name:"invoiceNo", type:"string"},
		{name:"invoiceTotalAmount", type:"float", allowNull:true},
		{name:"invoiceTotalQuantity", type:"float", allowNull:true},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"grossQtyUnitId", type:"int", allowNull:true},
		{name:"grossQtyUnitCode", type:"string"},
		{name:"netQtyUnitId", type:"int", allowNull:true},
		{name:"netQtyUnitCode", type:"string"},
		{name:"referenceInvLineId", type:"int", allowNull:true},
		{name:"airlineDesignatorId", type:"int", allowNull:true},
		{name:"airlineDesignatorCode", type:"string"},
		{name:"aircraftRegistrationNo", type:"string"},
		{name:"amount", type:"float", allowNull:true},
		{name:"calculatedVat", type:"float", allowNull:true},
		{name:"deliveryDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"eventCheckStatus", type:"string"},
		{name:"eventDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"flightNo", type:"string"},
		{name:"flightString", type:"string"},
		{name:"flightType", type:"string"},
		{name:"grossQuantity", type:"float", allowNull:true},
		{name:"id", type:"int", allowNull:true},
		{name:"invoicedVat", type:"float", allowNull:true},
		{name:"invoiceLine", type:"int", allowNull:true},
		{name:"netQuantity", type:"float", allowNull:true},
		{name:"originalVatCheckStatus", type:"string"},
		{name:"overAllCheckStatus", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"quantityCheckStatus", type:"string"},
		{name:"suffix", type:"string"},
		{name:"taxType", type:"string"},
		{name:"ticketNo", type:"string"},
		{name:"vat", type:"float", allowNull:true},
		{name:"vatCheckStatus", type:"string"},
		{name:"originalQuantityCheckStatus", type:"string"},
		{name:"quantityCheckResult", type:"string"},
		{name:"vatCheckResult", type:"string"},
		{name:"contractPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"vatRate", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.InvoiceLine_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"invoiceId", type:"int", allowNull:true},
		{name:"invoiceNo", type:"string"},
		{name:"invoiceTotalAmount", type:"float", allowNull:true},
		{name:"invoiceTotalQuantity", type:"float", allowNull:true},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"grossQtyUnitId", type:"int", allowNull:true},
		{name:"grossQtyUnitCode", type:"string"},
		{name:"netQtyUnitId", type:"int", allowNull:true},
		{name:"netQtyUnitCode", type:"string"},
		{name:"referenceInvLineId", type:"int", allowNull:true},
		{name:"airlineDesignatorId", type:"int", allowNull:true},
		{name:"airlineDesignatorCode", type:"string"},
		{name:"aircraftRegistrationNo", type:"string"},
		{name:"amount", type:"float", allowNull:true},
		{name:"calculatedVat", type:"float", allowNull:true},
		{name:"deliveryDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"eventCheckStatus", type:"string"},
		{name:"eventDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"flightNo", type:"string"},
		{name:"flightString", type:"string"},
		{name:"flightType", type:"string"},
		{name:"grossQuantity", type:"float", allowNull:true},
		{name:"id", type:"int", allowNull:true},
		{name:"invoicedVat", type:"float", allowNull:true},
		{name:"invoiceLine", type:"int", allowNull:true},
		{name:"netQuantity", type:"float", allowNull:true},
		{name:"originalVatCheckStatus", type:"string"},
		{name:"overAllCheckStatus", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"quantityCheckStatus", type:"string"},
		{name:"suffix", type:"string"},
		{name:"taxType", type:"string"},
		{name:"ticketNo", type:"string"},
		{name:"vat", type:"float", allowNull:true},
		{name:"vatCheckStatus", type:"string"},
		{name:"originalQuantityCheckStatus", type:"string"},
		{name:"quantityCheckResult", type:"string"},
		{name:"vatCheckResult", type:"string"},
		{name:"contractPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"vatRate", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.acc.ui.extjs.ds.InvoiceLine_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"amountBalance", type:"float", allowNull:true},
		{name:"origAmountBalance", type:"float", allowNull:true},
		{name:"origQuantityBalance", type:"float", allowNull:true},
		{name:"quantityBalance", type:"float", allowNull:true}
	]
});
