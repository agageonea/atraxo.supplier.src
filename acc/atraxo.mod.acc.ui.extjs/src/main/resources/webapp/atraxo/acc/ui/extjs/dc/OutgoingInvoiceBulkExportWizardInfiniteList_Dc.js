/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportWizardInfiniteList_Dc", {
	extend: "e4e.dc.AbstractDc",
	_infiniteScroll_: true,
	paramModel: atraxo.acc.ui.extjs.ds.OutgoingInvoice_DsParam,
	recordModel: atraxo.acc.ui.extjs.ds.OutgoingInvoice_Ds
});

/* ================= EDIT FORM: bulkInvoiceExportStep2TopLabel ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportWizardInfiniteList_Dc$bulkInvoiceExportStep2TopLabel", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_OutgoingInvoiceBulkExportWizardInfiniteList_Dc$bulkInvoiceExportStep2TopLabel",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"step2Description", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, cls:"sone-flat-label-field"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:500})
		.addPanel({ name:"col1", width:500, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["step2Description"]);
	}
});

/* ================= GRID: bulkInvoiceExportStep2 ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportWizardInfiniteList_Dc$bulkInvoiceExportStep2", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_OutgoingInvoiceBulkExportWizardInfiniteList_Dc$bulkInvoiceExportStep2",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"delLocCode", dataIndex:"delLocCode", width:50,  flex:1})
		.addDateColumn({ name:"invoiceDate", dataIndex:"invoiceDate", _mask_: Masks.DATE,  flex:1})
		.addTextColumn({ name:"invoiceNo", dataIndex:"invoiceNo", width:50,  flex:1})
		.addTextColumn({ name:"issuerCode", dataIndex:"issuerCode", width:50,  flex:1})
		.addTextColumn({ name:"receiverCode", dataIndex:"receiverCode", width:50,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
