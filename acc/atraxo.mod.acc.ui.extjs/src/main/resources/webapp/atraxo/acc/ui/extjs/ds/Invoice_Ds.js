/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.Invoice_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_Invoice_Ds"
	},
	
	
	validators: {
		dealtype: [{type: 'presence'}],
		invoiceType: [{type: 'presence'}],
		invoiceNo: [{type: 'presence'}],
		deliveryDateFrom: [{type: 'presence'}],
		deliverydateTo: [{type: 'presence'}],
		transactionType: [{type: 'presence'}],
		delLocCode: [{type: 'presence'}],
		invoiceForm: [{type: 'presence'}],
		category: [{type: 'presence'}],
		currCode: [{type: 'presence'}],
		totalAmount: [{type: 'presence'}],
		quantity: [{type: 'presence'}],
		unitCode: [{type: 'presence'}],
		status: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("invoiceType", "Invoice");
		this.set("transactionType", "Original");
		this.set("invoiceForm", "Paper");
		this.set("taxType", "Unspecified");
		this.set("status", "Draft");
		this.set("dealtype", "Buy");
		this.set("category", "Product into plane");
	},
	
	fields: [
		{name:"receiverId", type:"int", allowNull:true},
		{name:"receiverCode", type:"string"},
		{name:"delLocId", type:"int", allowNull:true},
		{name:"delLocCode", type:"string"},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"issuerId", type:"int", allowNull:true},
		{name:"issuerCode", type:"string"},
		{name:"dealtype", type:"string"},
		{name:"invoiceType", type:"string"},
		{name:"invoiceNo", type:"string"},
		{name:"issueDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"receivingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliveryDateFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliverydateTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"transactionType", type:"string"},
		{name:"invoiceForm", type:"string"},
		{name:"category", type:"string"},
		{name:"referenceType", type:"string"},
		{name:"referenceValue", type:"string"},
		{name:"totalAmount", type:"float", allowNull:true},
		{name:"vatAmount", type:"float", allowNull:true},
		{name:"netAmount", type:"float", allowNull:true},
		{name:"quantity", type:"float", allowNull:true},
		{name:"issuerBaselinedate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"baseLineDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"calculatedBaseLineDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"itemsNumber", type:"int", allowNull:true},
		{name:"closingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"taxType", type:"string"},
		{name:"status", type:"string"},
		{name:"erpReference", type:"string"},
		{name:"checkResult", type:"boolean"},
		{name:"dueDays", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"vatTaxableAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"vatRate", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"procent", type:"string", noFilter:true, noSort:true},
		{name:"type", type:"string", noFilter:true, noSort:true},
		{name:"scope", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.Invoice_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"receiverId", type:"int", allowNull:true},
		{name:"receiverCode", type:"string"},
		{name:"delLocId", type:"int", allowNull:true},
		{name:"delLocCode", type:"string"},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"issuerId", type:"int", allowNull:true},
		{name:"issuerCode", type:"string"},
		{name:"dealtype", type:"string"},
		{name:"invoiceType", type:"string"},
		{name:"invoiceNo", type:"string"},
		{name:"issueDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"receivingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliveryDateFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliverydateTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"transactionType", type:"string"},
		{name:"invoiceForm", type:"string"},
		{name:"category", type:"string"},
		{name:"referenceType", type:"string"},
		{name:"referenceValue", type:"string"},
		{name:"totalAmount", type:"float", allowNull:true},
		{name:"vatAmount", type:"float", allowNull:true},
		{name:"netAmount", type:"float", allowNull:true},
		{name:"quantity", type:"float", allowNull:true},
		{name:"issuerBaselinedate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"baseLineDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"calculatedBaseLineDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"itemsNumber", type:"int", allowNull:true},
		{name:"closingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"taxType", type:"string"},
		{name:"status", type:"string"},
		{name:"erpReference", type:"string"},
		{name:"checkResult", type:"boolean", allowNull:true},
		{name:"dueDays", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"vatTaxableAmount", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"vatRate", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"procent", type:"string", noFilter:true, noSort:true},
		{name:"type", type:"string", noFilter:true, noSort:true},
		{name:"scope", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
