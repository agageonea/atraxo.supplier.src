/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
__ACC_TYPE__ = {
	InvoiceProcessStatus : {
		_NOT_INVOICED_ : "Not invoiced" , 
		_PARTIAL_INVOICED_ : "Partial invoiced" , 
		_COMPLETELY_INVOICED_ : "Completely invoiced" , 
		_CLOSED_ : "Closed" , 
		_NO_INVOICING_ : "No invoicing" 
	},
	InvoiceTypeAcc : {
		_INV_ : "Invoice" , 
		_CRN_ : "Credit memo" 
	},
	TransactionType : {
		_CASH_ : "Cash" , 
		_CORRECTED_ : "Corrected" , 
		_FINAL_ : "Final" , 
		_ORIGINAL_ : "Original" , 
		_PREPAID_INVOICE_CORRECTION_ : "Prepaid invoice correction" , 
		_PREPAID_INVOICE_ : "Prepaid invoice" , 
		_PROVISIONAL_ : "Provisional" 
	},
	InvoiceFormAcc : {
		_PAPER_ : "Paper" , 
		_ELECTRONIC_ : "Electronic" 
	},
	InvoiceCategory : {
		_PRODUCT_INTO_PLANE_ : "Product into plane" , 
		_FUELING_SERVICE_INTO_PLANE_ : "Fueling service into plane" , 
		_PRODUCT_INTO_STORAGE_ : "Product into storage" 
	},
	InvoiceReferenceDocType : {
		_CONTRACT_ : "Contract" , 
		_PURCHASE_ORDER_ : "Purchase order" 
	},
	InvoiceReferenceType : {
		_BOL_ : "BOL" , 
		_CTN_ : "CTN" , 
		_PIN_ : "PIN" , 
		_PO_ : "PO" , 
		_SO_ : "SO" , 
		_WO_ : "WO" , 
		_EXT_ : "EXT" 
	},
	InvoiceLinesTaxType : {
		_BONDED_ : "Bonded" , 
		_DOMESTIC_ : "Domestic" , 
		_FOREIGN_TRADE_ZONE_ : "Foreign trade zone" , 
		_EU_QUALIFIED_PRODUCT_ : "EU qualified product" , 
		_NON_EU_QUALIFIED_PRODUCT_ : "Non EU qualified product" , 
		_UNSPECIFIED_ : "Unspecified" 
	},
	CheckStatus : {
		_OK_ : "OK" , 
		_FAILED_ : "Failed" , 
		_NOT_CHECKED_ : "Not checked" , 
		_NEEDS_RECHECK_ : "Needs recheck" 
	},
	CheckResult : {
		_NO_DISCREPANCIES_ : "No discrepancies" , 
		_IN_TOLERANCE_ : "In tolerance" , 
		_OUT_TOLERANCE_ : "Out tolerance" 
	},
	VatCheckStatus : {
		_NOT_CHECKED_ : "Not checked" , 
		_OK_ : "OK" , 
		_FAILED_ : "Failed" 
	},
	QuantitySource : {
		_FUEL_ORDER_ : "Fuel order" , 
		_FUEL_TICKET_ : "Fuel ticket" , 
		_INVOICE_ : "Invoice" 
	},
	ReceivedStatus : {
		_ON_HOLD_ : "On hold" , 
		_RELEASED_ : "Released" 
	},
	FuelEventStatus : {
		_ORIGINAL_ : "Original" , 
		_CANCELED_ : "Canceled" , 
		_REISSUE_ : "Reissue" , 
		_UPDATED_ : "Updated" 
	},
	FuelEventTransmissionStatus : {
		_NEW_ : "New" , 
		_IN_PROGRESS_ : "In progress" , 
		_TRANSMITTED_ : "Transmitted" , 
		_FAILED_ : "Failed" 
	},
	InvoiceTransmissionStatus : {
		_NEW_ : "New" , 
		_IN_PROGRESS_ : "In progress" , 
		_FAILED_ : "Failed" , 
		_PARTIALLY_TRANSMITTED_ : "Partially transmitted" , 
		_TRANSMITTED_ : "Transmitted" 
	},
	InvoiceLineTransmissionStatus : {
		_NEW_ : "New" , 
		_IN_PROGRESS_ : "In progress" , 
		_FAILED_ : "Failed" , 
		_TRANSMITTED_ : "Transmitted" 
	},
	InvoiceExportStatus : {
		_NEW_ : "New" , 
		_EXPORTED_ : "Exported" , 
		_UPLOADED_ : "Uploaded" , 
		_EXPORT_FAILED_ : "Export Failed" , 
		_UPLOAD_FAILED_ : "Upload Failed" 
	},
	ExportTypeInvoice : {
		_PDF_ : "PDF" , 
		_XML_ : "XML" 
	},
	ControlNo : {
		_NOT_AVAILABLE_ : "Not available" , 
		_WAITING_FOR_ : "Waiting for" 
	}
}
