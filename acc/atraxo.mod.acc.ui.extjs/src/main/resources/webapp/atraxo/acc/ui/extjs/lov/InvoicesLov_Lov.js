/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.lov.InvoicesLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.acc_InvoicesLov_Lov",
	displayField: "invoiceNo", 
	_columns_: ["invoiceNo"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{invoiceNo}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.acc.ui.extjs.ds.OutgoingInvoiceLov_Ds
});
