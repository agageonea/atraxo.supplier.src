/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceMemo_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.OutgoingInvoiceMemo_Ds
});

/* ================= GRID: CreditMemoList ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceMemo_Dc$CreditMemoList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_OutgoingInvoiceMemo_Dc$CreditMemoList",
	_noPaginator_: true,
	selType: null,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"customer", dataIndex:"receiverCode", width:50,  flex:1})
		.addTextColumn({ name:"invoiceNumber", dataIndex:"invoiceNo", width:50,  flex:1})
		.addDateColumn({ name:"invoiceDate", dataIndex:"invoiceDate", _mask_: Masks.DATE,  flex:1})
		.addTextColumn({ name:"location", dataIndex:"delLocCode", width:50,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
