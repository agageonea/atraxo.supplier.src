/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.Contract_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.Contract_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.acc.ui.extjs.dc.Contract_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_Contract_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"supplier", dataIndex:"supplierCode", width:100})
		.addTextColumn({ name:"contractCode", dataIndex:"code", width:100})
		.addTextColumn({ name:"eventType", dataIndex:"flightType", width:100})
		.addTextColumn({ name:"type", dataIndex:"type", width:100})
		.addTextColumn({ name:"subtype", dataIndex:"subType", width:100})
		.addTextColumn({ name:"scope", dataIndex:"scope", width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
