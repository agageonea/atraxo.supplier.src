/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.frame.Invoices_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.Invoices_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("invoice", Ext.create(atraxo.acc.ui.extjs.dc.Invoice_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("invoiceLine", Ext.create(atraxo.acc.ui.extjs.dc.InvoiceLine_Dc,{multiEdit: true}))
		.addDc("contract", Ext.create(atraxo.acc.ui.extjs.dc.Contract_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.linkDc("attachment", "invoice",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("invoiceLine", "invoice",{fetchMode:"auto",fields:[
					{childField:"invoiceId", parentField:"id"}]})
				.linkDc("contract", "invoice",{fields:[
					{childField:"locationId", parentField:"delLocId"}, {childField:"supplierId", parentField:"issuerId"}, {childField:"scope", parentField:"scope"}, {childField:"type", parentField:"type"}]})
				.linkDc("history", "invoice",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "invoice",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdwEdit",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwEdit, scope:this})
		.addButton({name:"btnNewWithMenu",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, isBtnContainer:true, scope:this})
		.addButton({name:"btnNewCreditNote",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewCreditNote, scope:this})
		.addButton({name:"btnNewInvoice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", _isDefaultHandler_:true, handler: this.onBtnNewInvoice, scope:this})
		.addButton({name:"btnSaveClose",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveClose, scope:this})
		.addButton({name:"btnSaveEdit",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEdit, scope:this})
		.addButton({name:"btnCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancel, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnSelect",glyph:fp_asc.commit_glyph.glyph,iconCls: fp_asc.commit_glyph.css, disabled:false, handler: this.onBtnSelect, scope:this})
		.addButton({name:"btnSuppCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnSuppCancel, scope:this})
		.addButton({name:"btnReset",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnReset,stateManager:[{ name:"selected_one", dc:"invoice", and: function(dc) {return (dc.record.data.status!=__CMM_TYPE__.BillStatus._DRAFT_ && dc.record.data.status!=__CMM_TYPE__.BillStatus._AWAITING_PAYMENT_ && dc.record.data.status!=__CMM_TYPE__.BillStatus._PAID_);} }], scope:this})
		.addButton({name:"btnResetList",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnResetList,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canReset(dc));} }], scope:this})
		.addButton({name:"btnAutoBalance",glyph:fp_asc.negociate_glyph.glyph, disabled:true, handler: this.onBtnAutoBalance,stateManager:[{ name:"selected_not_zero", dc:"invoiceLine", and: function() {return (this._getDc_("invoice").record.data.status == __TYPES__.invoices.status.draft);} }], scope:this})
		.addButton({name:"btnFinishInput",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnFinishInput,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (this.canFinish(dc));} }], scope:this})
		.addButton({name:"btnRecheckList",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnRecheckList,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (dc.record.data.status!=__CMM_TYPE__.BillStatus._DRAFT_ && dc.record.data.status!=__CMM_TYPE__.BillStatus._AWAITING_PAYMENT_ && dc.record.data.status!=__CMM_TYPE__.BillStatus._PAID_);} }], scope:this})
		.addButton({name:"btnRecheckEdit",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnRecheckEdit,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (dc.record.data.status!=__CMM_TYPE__.BillStatus._DRAFT_ && dc.record.data.status!=__CMM_TYPE__.BillStatus._AWAITING_PAYMENT_ && dc.record.data.status!=__CMM_TYPE__.BillStatus._PAID_);} }], scope:this})
		.addButton({name:"btnSubmit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSubmit,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (dc.record.data.status ==  __TYPES__.invoices.status.checkPassed);} }], scope:this})
		.addButton({name:"btnSubmitList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSubmitList,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canSubmit(dc));} }], scope:this})
		.addButton({name:"btnApprove",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApprove,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (dc.record.data.status ==  __TYPES__.invoices.status.awaitingApproval);} }], scope:this})
		.addButton({name:"btnApproveList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveList,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canApprove(dc));} }], scope:this})
		.addButton({name:"btnPaid",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPaid,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (dc.record.data.status ==  __TYPES__.invoices.status.awaitingPayment);} }], scope:this})
		.addButton({name:"btnPaidList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPaidList,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canPaid(dc));} }], scope:this})
		.addButton({name:"btnInvoiceLineDetails",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnInvoiceLineDetails,stateManager:[{ name:"selected_one_clean", dc:"invoiceLine"}], scope:this})
		.addButton({name:"backToCanvas2",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false, handler: this.onBackToCanvas2, scope:this})
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"invoiceLine", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addDcGridView("invoice", {name:"InvoiceList", xtype:"acc_Invoice_Dc$List"})
		.addDcFilterFormView("invoice", {name:"InvoiceFilter", xtype:"acc_Invoice_Dc$Filter"})
		.addDcFormView("invoice", {name:"NewInvoicePopup", xtype:"acc_Invoice_Dc$New"})
		.addDcFormView("invoice", {name:"InvoiceEdit", xtype:"acc_Invoice_Dc$Edit"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"NotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("contract", {name:"ContractList", xtype:"acc_Contract_Dc$List"})
		.addDcEditGridView("invoiceLine", {name:"InvoiceLineList", _hasTitle_:true, xtype:"acc_InvoiceLine_Dc$EditList", frame:true, _summaryDefined_:true})
		.addDcFilterFormView("invoiceLine", {name:"invoiceLineFilter", xtype:"acc_InvoiceLine_Dc$Filter"})
		.addDcFormView("invoiceLine", {name:"InvoiceLineDetails", xtype:"acc_InvoiceLine_Dc$Details"})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addWindow({name:"wdwNewInvoicePopUp", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NewInvoicePopup")],  listeners:{ close:{fn:this.onWdwInvoiceClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveClose"), this._elems_.get("btnSaveEdit"), this._elems_.get("btnCancel")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NotesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"wdwSupplier", _hasTitle_:true, width:1170, height:500, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("ContractList")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSelect"), this._elems_.get("btnSuppCancel")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"invoice"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("detailsTab", ["InvoiceLineList"])
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["InvoiceList"], ["center"])
		.addChildrenTo("canvas2", ["InvoiceEdit", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["InvoiceLineDetails"], ["center"])
		.addToolbarTo("InvoiceList", "tlbInvoicetList")
		.addToolbarTo("InvoiceEdit", "tlbInvoiceEditt")
		.addToolbarTo("NotesList", "tlbNoteList")
		.addToolbarTo("InvoiceLineList", "tlbInvoiceLineList")
		.addToolbarTo("InvoiceLineDetails", "tlbInvoiceLineDetails")
		.addToolbarTo("attachmentList", "tlbAttachments");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["NotesList", "attachmentList", "History"])
		.addChildrenTo2("main", ["canvas2", "canvas3"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbInvoicetList", {dc: "invoice"})
			.addButtons([])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnResetList"),this._elems_.get("btnRecheckList"),this._elems_.get("btnSubmitList"),this._elems_.get("btnApproveList"),this._elems_.get("btnPaidList"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbInvoiceEditt", {dc: "invoice"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnReset"),this._elems_.get("btnRecheckEdit"),this._elems_.get("btnFinishInput"),this._elems_.get("btnSubmit"),this._elems_.get("btnApprove"),this._elems_.get("btnPaid"),this._elems_.get("helpWdwEdit")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbInvoiceLineList", {dc: "invoiceLine"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"true",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll"),this._elems_.get("btnInvoiceLineDetails"),this._elems_.get("btnAutoBalance")])
			.addReports()
		.end()
		.beginToolbar("tlbInvoiceLineDetails", {dc: "invoiceLine"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("backToCanvas2")])
			.addPrevRec({glyph:fp_asc.prev_glyph.glyph})
			.addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	
	,_applyTabLoadRestriction_: function(){
		this._getBuilder_()
		.addTabLoadRestriction("invoiceLine","detailsTab",["InvoiceLineList"])
		.addTabLoadRestriction("note","detailsTab",["NotesList"])
		.addTabLoadRestriction("history","detailsTab",["History"])
		.addTabLoadRestriction("attachment","detailsTab",["attachmentList"])
		;
	}

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwEdit
	 */
	,onHelpWdwEdit: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnNewCreditNote
	 */
	,onBtnNewCreditNote: function() {
	}
	
	/**
	 * On-Click handler for button btnNewInvoice
	 */
	,onBtnNewInvoice: function() {
		this._getDc_("invoice").doNew();
	}
	
	/**
	 * On-Click handler for button btnSaveClose
	 */
	,onBtnSaveClose: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveEdit
	 */
	,onBtnSaveEdit: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button btnCancel
	 */
	,onBtnCancel: function() {
		this.onWdwInvoiceClose();
		this._getWindow_("wdwNewInvoicePopUp").close();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this._getDc_("note").doSave();
		this._getWindow_("noteWdw").close();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSelect
	 */
	,onBtnSelect: function() {
		this.selectSupplier();
		this._getWindow_("wdwSupplier").close();
	}
	
	/**
	 * On-Click handler for button btnSuppCancel
	 */
	,onBtnSuppCancel: function() {
		this._getWindow_("wdwSupplier").close();
	}
	
	/**
	 * On-Click handler for button btnReset
	 */
	,onBtnReset: function() {
		var successFn = function(dc) {
			this._getDc_("history").doReloadPage();
			this._get_("InvoiceEdit")._applyStates_(dc.record);
								 var invoiceLIne = this._getDc_("invoiceLine");
								 this.setInvoiceLineNewDeleteButtonAvailability(invoiceLIne);
								 this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"reset",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnResetList
	 */
	,onBtnResetList: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"resetList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnAutoBalance
	 */
	,onBtnAutoBalance: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._getDc_("invoiceLine").doReloadPage();
			this._getDc_("invoice").doReloadRecord();
			this._get_("InvoiceEdit")._applyStates_(this._getDc_("invoice").record);
		};
		var o={
			name:"autoBalance",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("invoiceLine").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnFinishInput
	 */
	,onBtnFinishInput: function() {
		var successFn = function(dc) {
			this._getDc_("history").doReloadPage();
			this._getDc_("invoiceLine").doReloadPage();
			this.setInvoiceLineNewDeleteButtonAvailability(this._getDc_("invoiceLine"));//invoice line enable function.
									 this._applyStateAllButtons_();//buttons enable rule
									this._get_("InvoiceEdit")._applyStates_(dc.record);
		};
		var failureFn = function() {
			this._setReadOnly_();
			this._applyStateAllButtons_();
		};
		var o={
			name:"finishInput",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnRecheckList
	 */
	,onBtnRecheckList: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._getDc_("invoiceLine").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			this._setReadOnly_();
			this._applyStateAllButtons_();Main.rpcFailure(response);
		};
		var o={
			name:"recheck",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnRecheckEdit
	 */
	,onBtnRecheckEdit: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._getDc_("invoiceLine").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			this._setReadOnly_();
			this._applyStateAllButtons_();Main.rpcFailure(response);
		};
		var o={
			name:"recheck",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnSubmit
	 */
	,onBtnSubmit: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"submit",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnSubmitList
	 */
	,onBtnSubmitList: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"submitList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnApprove
	 */
	,onBtnApprove: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"release",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnApproveList
	 */
	,onBtnApproveList: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"releaseList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnPaid
	 */
	,onBtnPaid: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"pay",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnPaidList
	 */
	,onBtnPaidList: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"payList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnInvoiceLineDetails
	 */
	,onBtnInvoiceLineDetails: function() {
		this._showStackedViewElement_('main', 'canvas3')
	}
	
	/**
	 * On-Click handler for button backToCanvas2
	 */
	,onBackToCanvas2: function() {
		this._backToCanvas2_();
	}
	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	,calculateAmount: function() {	
		var successFn = function() {
			this._updateSummary_();
		};
		var o={
			name:"calculateAmount",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("invoiceLine").doRpcData(o);
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,_setReadOnly_: function() {
		
						var dc = this._getDc_("invoice");
						dc.doReloadPage();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/Invoices.html";
					window.open( url, "SONE_Help");
	}
	
	,_backToCanvas2_: function() {
		
		
						var invoiceLineDc = this._getDc_("invoiceLine");
						var invoiceDc = this._getDc_("invoice");
						var rec = invoiceLineDc.getRecord();
						var invoiceId = rec.get("invoiceId");
						
						invoiceLineDc.doRestoreCtxFilter();
		
						if (invoiceLineDc._calledFromDeliveryNotes_ === true) {
							invoiceDc.setFilterValue("id", invoiceId);
						}
		
						this._showStackedViewElement_("main", "canvas2");
						invoiceDc.doQuery();
		
	}
	
	,_when_called_from_delivery_notes_: function(params) {
		
		
						var dc = this._getDc_("invoiceLine");
						this._showStackedViewElement_("main", "canvas3");
		
						dc._calledFromDeliveryNotes_ = true;
		
						// Dan: Because the dc is a child of another dc, before setting new filter values we must clear the 
						// context filter values (is this case: the "invoiceId" previously set at "link-to invoice ( invoiceId : id )")
		
						dc.doClearAllFilters();
		
						dc.setFilterValue("invoiceId", null);
						dc.setFilterValue("id", params.id);
		
						dc.doQuery();
		
	}
	
	,_when_called_for_details_: function() {
		
						var dc = this._getDc_("invoice");
						dc.doEditOut({doNewAfterEditOut:true});
	}
	
	,_when_called_from_appMenu_: function() {
		
						var dc = this._getDc_("invoice");
						dc.doEditOut({doNewAfterEditOut:true});
	}
	
	,_enableDisableFinishInput_: function() {
		
						var invoiceLine = this._getDc_("invoiceLine");
						var invoice = this._getDc_("invoice");
		
						var r = invoice.getRecord();
						var invLineRec = invoiceLine.getRecord();
		
						if (r && invLineRec) {
		
							var status = r.get("status");
							var contractCode = r.get("contractCode");
							var amountBalance = invoiceLine.getParamValue("amountBalance");
							var quantityBalance = invoiceLine.getParamValue("quantityBalance");
							var btnFinishInput = this._get_("btnFinishInput");
		
							if (status === __TYPES__.invoices.status.draft && contractCode != "" && amountBalance === 0 && quantityBalance === 0) {
								btnFinishInput.enable();
							}
							else {
								btnFinishInput.disable();
							}
						}
	}
	
	,_afterDefineDcs_: function() {
		
						var invoice = this._getDc_("invoice");
						var history = this._getDc_("history");
						var invoiceLine = this._getDc_("invoiceLine");
		
						invoice.on("afterDoNew", function() {				
							this._getWindow_("wdwNewInvoicePopUp").show();				
						} , this );
		
						invoice.on("afterDoSaveSuccess", function (dc, ajaxResult) {
							if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
								this._getWindow_("wdwNewInvoicePopUp").close();
			
							} else if (ajaxResult.options.options.doNewAfterSave === true) {
								invoice.doNew();
							}else if (ajaxResult.options.options.doCloseEditAfterSave === true) {
								this._getWindow_("wdwNewInvoicePopUp").close();	
								dc.doEditIn();
							}
							dc.doReloadRecord();
							invoiceLine.doReloadPage();
							history.doReloadPage();
							this.updateInvoiceLineHeader();
						} , this );
		
		
						invoice.on("showWindowContract", function() {
							this._getDc_("contract").doQuery();
							this._getWindow_("wdwSupplier").show(undefined, function(){
							 	this._onShowSupplierWindow_();
							},this);	
						} , this );
		
						invoice.on("onEditOut", function(dc, ajaxResult) {
							if (ajaxResult.doNewAfterEditOut === true) {
						        // Dan: wait until the store is loaded and after call the doNew() method
								dc.store.on("load", function() {
									dc.doNew();
								}, this);
						    }
						}, this);
		
						invoice.on("onEditIn", function () {	
							this.updateInvoiceLineHeader();
						}, this);
		
						var note = this._getDc_("note");
						note.on("afterDoNew", function () {
						    this.showNoteWdw();
						}, this);
						note.on("OnEditIn", function() {
						   this._getWindow_("noteWdw").show();
						}, this);
		
						invoiceLine.on("OnEditIn", function(dc) {	
							var invLineRec = dc.getRecord();
							var invRec = invoice.getRecord();
							invLineRec.set("contractPrice", invRec.get("contractPrice"));
						}, this);
		
						invoiceLine.on("afterDoCancel", function() {	
							this._updateSummary_();
						}, this);
		
						invoiceLine.on("afterDoNew", function(dc) {				
							var invRec = invoice.getRecord();
							var invLineRec = dc.getRecord();
							var unitId = invRec.get("unitId");
							var unitCode = invRec.get("unitCode");
							invLineRec.set("unitId", unitId);	
							invLineRec.set("unitCode", unitCode);		
						}, this);	
		
						invoiceLine.on("calculateAmount", function(dc,val) {				
							invoiceLine.getRecord().set("quantity",val);
							this.calculateAmount();
						}, this);
		
						invoiceLine.on("updateSummary", function(list,field) {				
							this._updateSummary_(list,field,true);
						}, this);
		
						invoiceLine.on("afterDoSaveSuccess", function (dc) {
							invoice.doReloadRecord();
							dc.doReloadPage();					
							this._applyStateAllButtons_();
							this._get_("InvoiceEdit")._applyStates_(this._getDc_("invoice").record);
						}, this);
		
						invoiceLine.on("afterDoQuerySuccess", function(dc){
							this.setInvoiceLineNewDeleteButtonAvailability(dc);
						},this);
		
						invoiceLine.on("afterDoDeleteSuccess", function (dc){
							if(dc.store.data.length === 0){
								this.setamountAndQuantityValues();
							}
						},this);
	}
	
	,_updateVatSummary_: function(list,field,blur,vat) {
		
		
						if (blur === true) {
							field.blur();
						}
		
						var store = this._getDc_("invoiceLine").store;
						var invoiceLineList = this._get_("InvoiceLineList");
		
						var vatSum = store.sum("vat");
		
						if (Ext.isNumber(vat)) {
							vatSum = vat + vatSum;
						}
		
						invoiceLineList.setSummaryVal("vat", vatSum);
		
	}
	
	,_updateAmountSummary_: function(list,field,blur,amount) {
		
		
						if (blur === true) {
							field.blur();
						}
		
						var store = this._getDc_("invoiceLine").store;
						var invoiceLineList = this._get_("InvoiceLineList");
		
						var amountSum = store.sum("amount");
		
						if (Ext.isNumber(amount)) {
							amountSum = amount + amountSum;
						}
		
						invoiceLineList.setSummaryVal("amount", amountSum);
		
	}
	
	,_updateSummary_: function(list,field,blur,qty) {
		
		
						if (blur === true) {
							field.blur();
						}
		
						var store = this._getDc_("invoiceLine").store;
						var invoiceLineList = this._get_("InvoiceLineList");
		
						var quantitySum = store.sum("quantity");
						var vatSum = store.sum("vat");
						var amountSum = store.sum("amount");
		
						if (Ext.isNumber(qty)) {
							quantitySum = qty + quantitySum;
						}
		
						invoiceLineList.setSummaryVal("quantity", quantitySum);
						invoiceLineList.setSummaryVal("vat", vatSum);
						invoiceLineList.setSummaryVal("amount", amountSum);
		
	}
	
	,_onShowSupplierWindow_: function() {
		
						var btnSelect = Ext.ComponentQuery.query("[name=btnSelect]")[0];
						var supplierGrid = this._get_("ContractList").getView();
						var store = this._getDc_("contract").store;
		
						store.on("load", function() {
							if (store.getCount() === 0) {
								btnSelect.setDisabled(true);
							}
							else {
								btnSelect.setDisabled(false);
							}
						});
		
						supplierGrid.getSelectionModel().on("selectionchange", function(sm, selectedRows) {
						    if (selectedRows.length === 1) {
								btnSelect.setDisabled(false);
							}
							else {
								btnSelect.setDisabled(true);
							}
						}, this);
		
	}
	
	,saveClose: function() {
		
						var invoice = this._getDc_("invoice");
						invoice.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveEdit: function() {
		
						var invoice = this._getDc_("invoice");
						invoice.doSave({doCloseEditAfterSave: true });
	}
	
	,onWdwInvoiceClose: function() {
		
						var dc = this._getDc_("invoice");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,updateInvoiceLineHeader: function() {
		
						var invoice = this._getDc_("invoice");
						this.setInvoiceLineNewDeleteButtonAvailability(this._getDc_("invoiceLine"));
						this._applyStateAllButtons_();
						var invRec = invoice.getRecord();
						var unitCode = invRec.get("unitCode");
						var currencyCode = invRec.get("currCode");
						var grid = this._get_("InvoiceLineList");
						var view = grid.getView();
						var headerCt = view.getHeaderCt();
		
						var quantityColumn = headerCt.down("[dataIndex=quantity]");
						var amountColumn = headerCt.down("[dataIndex=amount]");
						var vatAmountColumn = headerCt.down("[dataIndex=vat]");
		
						quantityColumn.setText(quantityColumn.initialConfig.header+" ("+unitCode+")");
						amountColumn.setText(amountColumn.initialConfig.header+" ("+currencyCode+")");
						vatAmountColumn.setText(vatAmountColumn.initialConfig.header+" ("+currencyCode+")");
						this.setamountAndQuantityValues();
	}
	
	,setamountAndQuantityValues: function() {
		
						var invoice = this._getDc_("invoice");
						var invoiceData = invoice.record.data;
						var invoiceLine = this._getDc_("invoiceLine");
						invoiceLine.dcContext._updateCtxData_();
						if(invoiceLine.store.data.length === 0){
							var quantity = invoiceData.quantity;
							var amount = invoiceData.totalAmount;
							invoiceLine.setParamValue("quantityBalance",quantity*(-1));
							invoiceLine.setParamValue("origQuantityBalance",quantity*(-1));
							invoiceLine.setParamValue("amountBalance",amount*(-1));
							invoiceLine.setParamValue("origAmountBalance",amount*(-1));		
						}
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,selectSupplier: function() {
		 
						var invoice = this._getDc_("invoice").getRecord();
						var contract = this._getDc_("contract").getRecord();
		
						invoice.set("contractId", contract.get("id"));
						invoice.set("contractCode", contract.get("code"));
		
						this._getDc_("invoice").doSave();
						this._getDc_("invoice").doReloadRecord();
		
	}
	
	,setInvoiceLineNewDeleteButtonAvailability: function(dc) {
						
						var invoiceData = this._getDc_("invoice").getRecord().data;				
						var status = invoiceData.status;
						if (status !== __TYPES__.invoices.status.draft) {
							dc.canDoDelete = false;						
							dc.setReadOnly(true);
							dc.updateDcState();						
						}
						else {
							dc.canDoDelete = true;						
							dc.setReadOnly(false);
							dc.updateDcState();						
						}
	}
	
	,canReset: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.status === __CMM_TYPE__.BillStatus._DRAFT_ || record.data.status === __CMM_TYPE__.BillStatus._PAID_ || record.data.status === __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_){
								return false;			
							}
						}
						return true;
	}
	
	,canFinish: function(dc) {
		
						var invoiceLineDc = this._getDc_("invoiceLine");
						return dc.record.data.status === __TYPES__.invoices.status.draft && dc.record.data.contractCode !== "" && invoiceLineDc.params.get("amountBalance") === 0 && invoiceLineDc.params.get("quantityBalance") === 0; 
	}
	
	,canSubmit: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.status !== __TYPES__.invoices.status.checkPassed){
								return false;			
							}
						}
						return true;
	}
	
	,canApprove: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.status !== __TYPES__.invoices.status.awaitingApproval){
								return false;			
							}
						}
						return true;
	}
	
	,canPaid: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.status !== __TYPES__.invoices.status.awaitingPayment){
								return false;			
							}
						}
						return true;
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("invoiceLine");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("invoiceLine");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
});
