/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoiceMemo_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_OutgoingInvoiceMemo_Ds"
	},
	
	
	fields: [
		{name:"receiverId", type:"int", allowNull:true},
		{name:"receiverCode", type:"string"},
		{name:"delLocId", type:"int", allowNull:true},
		{name:"delLocCode", type:"string"},
		{name:"invoiceNo", type:"string"},
		{name:"invoiceDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"baseLineDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"referenceInvoiceId", type:"int", allowNull:true},
		{name:"referenceInvoiceNumber", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoiceMemo_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"receiverId", type:"int", allowNull:true},
		{name:"receiverCode", type:"string"},
		{name:"delLocId", type:"int", allowNull:true},
		{name:"delLocCode", type:"string"},
		{name:"invoiceNo", type:"string"},
		{name:"invoiceDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"baseLineDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"status", type:"string"},
		{name:"referenceInvoiceId", type:"int", allowNull:true},
		{name:"referenceInvoiceNumber", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
