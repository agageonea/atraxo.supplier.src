/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.DeliveryNote_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.DeliveryNote_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.acc.ui.extjs.dc.DeliveryNote_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.acc_DeliveryNote_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addCombo({ xtype:"combo", name:"source", dataIndex:"source", store:[ __ACC_TYPE__.QuantitySource._FUEL_ORDER_, __ACC_TYPE__.QuantitySource._FUEL_TICKET_, __ACC_TYPE__.QuantitySource._INVOICE_]})
			.addTextField({ name:"ticketNumber", dataIndex:"ticketNumber", maxLength:32})
			.addBooleanField({ name:"used", dataIndex:"used"})
			.addNumberField({name:"fuelingQuantity", dataIndex:"quantity", sysDec:"dec_unit", maxLength:19})
			.addLov({name:"unit", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"id", dsField: "unitId"} ]}})
			.addNumberField({name:"payableCost", dataIndex:"payableCost", sysDec:"dec_crncy", maxLength:19})
			.addLov({name:"payableCurrency", dataIndex:"payCurrencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"id", dsField: "payCurrencyId"} ]}})
			.addNumberField({name:"billableCost", dataIndex:"billabeCost", sysDec:"dec_crncy", maxLength:19})
			.addLov({name:"billableCostCurrency", dataIndex:"billCurrencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"id", dsField: "billCurrencyId"} ]}})
			.addLov({name:"supplier", dataIndex:"supplierCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "supplierId"} ]}})
			.addLov({name:"iplAgent", dataIndex:"iplAgentCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "iplAgentCode"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.acc.ui.extjs.dc.DeliveryNote_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_DeliveryNote_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"source", dataIndex:"source", width:100})
		.addTextColumn({ name:"ticketNumber", dataIndex:"ticketNumber", width:100})
		.addBooleanColumn({ name:"used", dataIndex:"used", width:70})
		.addNumberColumn({ name:"quantity", dataIndex:"quantity", width:100, sysDec:"dec_unit"})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:100})
		.addTextColumn({ name:"supplier", dataIndex:"supplierCode", width:100})
		.addTextColumn({ name:"iplAgent", dataIndex:"iplAgentCode", width:100})
		.addDateColumn({ name:"fuelingDate", dataIndex:"fuelingDate", hidden:true, width:120, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"location", dataIndex:"departureCode", hidden:true, width:100})
		.addTextColumn({ name:"customer", dataIndex:"customerCode", hidden:true, width:100})
		.addTextColumn({ name:"aircraftRegistration", dataIndex:"aircraftRegistration", hidden:true, width:50})
		.addTextColumn({ name:"flightNumber", dataIndex:"flightNumber", hidden:true, width:50})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
