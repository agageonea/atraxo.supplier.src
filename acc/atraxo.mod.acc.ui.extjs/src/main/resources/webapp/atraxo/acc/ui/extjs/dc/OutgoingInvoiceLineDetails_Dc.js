/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceLineDetails_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.OutgoingInvoiceLineDetails_Ds
});

/* ================= GRID: List ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceLineDetails_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_OutgoingInvoiceLineDetails_Dc$List",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"priceName", dataIndex:"priceName", width:160,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addTextColumn({ name:"priceCategoryName", dataIndex:"priceCategoryName", width:160,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addTextColumn({ name:"mainCategoryName", dataIndex:"mainCategoryName", width:160,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addNumberColumn({ name:"originalPrice", dataIndex:"originalPrice", width:160, decimals:6,  renderer:function(val, meta, record) { return this._markUtilization_(val, meta, record); }, flex:1})
		.addTextColumn({ name:"originalPriceCurrencyCode", dataIndex:"originalPriceCurrencyCode", width:80,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addTextColumn({ name:"originalPriceUnitCode", dataIndex:"originalPriceUnitCode", width:60,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addNumberColumn({ name:"settlementPrice", dataIndex:"settlementPrice", width:120, decimals:6,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addNumberColumn({ name:"contractAmount", dataIndex:"contractAmount", width:120, decimals:6,  flex:1, renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addNumberColumn({ name:"exchangeRate", dataIndex:"exchangeRate", hidden:true, width:120, decimals:6,  renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addNumberColumn({ name:"vatAmount", dataIndex:"vatAmount", hidden:true, width:120, decimals:6,  renderer:function(val, meta) { return this._changeToSelectable_(val, meta); }})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_changeToSelectable_: function(value,meta) {
		
						meta.innerCls = "x-selectable";
						return value;
	},
	
	_markUtilization_: function(value,meta,record) {
		
						var used = record.get("used");
						meta.innerCls = "x-selectable";
						if (!Ext.isEmpty(value)) {
							if(used === true) {
					        	meta.style = "font-weight:bold !important";
						    } 
							return Ext.util.Format.number(value,Main.getNumberFormat(_SYSTEMPARAMETERS_.dec_crncy));
						}				
	},
	
	_afterInitComponent_: function() {
		
		
						var dc = this._controller_;
						var headerCt = this.getView().getHeaderCt();
						var convertedPriceColumn = headerCt.down("[dataIndex=settlementPrice]");
						var contractAmountColumn = headerCt.down("[dataIndex=contractAmount]");
		
						dc.on("afterDoQuerySuccess", function() {
							var frame = dc.getFrame();
							var invoice = frame._getDc_("invoice");
							var r = invoice.getRecord();
							var settCrncy;
							if (r) {
								settCrncy = r.get("currCode");
								if (!Ext.isEmpty(settCrncy)) {
									convertedPriceColumn.setText(convertedPriceColumn.initialConfig.header+" ("+settCrncy+")");
									contractAmountColumn.setText(contractAmountColumn.initialConfig.header+" ("+settCrncy+")");
								}
								else {
									convertedPriceColumn.setText(convertedPriceColumn.initialConfig.header);
									contractAmountColumn.setText(contractAmountColumn.initialConfig.header);
								}
							}
						}, this);
	}
});
