/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.FuelEventsDetails_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_FuelEventsDetails_Ds"
	},
	
	
	fields: [
		{name:"fuelEventId", type:"int", allowNull:true},
		{name:"mainCategoryId", type:"int", allowNull:true},
		{name:"mainCategoryCode", type:"string"},
		{name:"mainCategoryName", type:"string"},
		{name:"priceCategoryId", type:"int", allowNull:true},
		{name:"priceCategoryName", type:"string"},
		{name:"originalPriceCurrencyId", type:"int", allowNull:true},
		{name:"originalPriceCurrencyCode", type:"string"},
		{name:"originalPriceCurrencyName", type:"string"},
		{name:"originalPriceUnitId", type:"int", allowNull:true},
		{name:"originalPriceUnitCode", type:"string"},
		{name:"originalPriceUnitName", type:"string"},
		{name:"contractCode", type:"string"},
		{name:"priceName", type:"string"},
		{name:"originalPrice", type:"float", allowNull:true},
		{name:"settlementPrice", type:"float", allowNull:true},
		{name:"exchangeRate", type:"float", allowNull:true},
		{name:"vatAmount", type:"float", allowNull:true},
		{name:"contractAmount", type:"float", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.FuelEventsDetails_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"fuelEventId", type:"int", allowNull:true},
		{name:"mainCategoryId", type:"int", allowNull:true},
		{name:"mainCategoryCode", type:"string"},
		{name:"mainCategoryName", type:"string"},
		{name:"priceCategoryId", type:"int", allowNull:true},
		{name:"priceCategoryName", type:"string"},
		{name:"originalPriceCurrencyId", type:"int", allowNull:true},
		{name:"originalPriceCurrencyCode", type:"string"},
		{name:"originalPriceCurrencyName", type:"string"},
		{name:"originalPriceUnitId", type:"int", allowNull:true},
		{name:"originalPriceUnitCode", type:"string"},
		{name:"originalPriceUnitName", type:"string"},
		{name:"contractCode", type:"string"},
		{name:"priceName", type:"string"},
		{name:"originalPrice", type:"float", allowNull:true},
		{name:"settlementPrice", type:"float", allowNull:true},
		{name:"exchangeRate", type:"float", allowNull:true},
		{name:"vatAmount", type:"float", allowNull:true},
		{name:"contractAmount", type:"float", allowNull:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
