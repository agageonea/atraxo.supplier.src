/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.FuelEventPurchaseOrder_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.acc.ui.extjs.ds.FuelEventPurchaseOrder_Ds
});

/* ================= GRID: Orders ================= */

Ext.define("atraxo.acc.ui.extjs.dc.FuelEventPurchaseOrder_Dc$Orders", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_FuelEventPurchaseOrder_Dc$Orders",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"orderCode", dataIndex:"orderCode", width:100})
		.addTextColumn({ name:"eventType", dataIndex:"eventType", width:100})
		.addTextColumn({ name:"operationalType", dataIndex:"operationalType", width:120})
		.addTextColumn({ name:"product", dataIndex:"product", width:100})
		.addTextColumn({ name:"customerCode", dataIndex:"customerCode", width:100})
		.addNumberColumn({ name:"origAmount", dataIndex:"billableCost", width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"settlementCurrency", dataIndex:"billabelCostCurrency", width:100})
		.addNumberColumn({ name:"convertedAmount", dataIndex:"convertedAmount", width:120, sysDec:"dec_crncy"})
		.addTextColumn({ name:"sysCurrency", dataIndex:"systemCurrency", width:120})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
