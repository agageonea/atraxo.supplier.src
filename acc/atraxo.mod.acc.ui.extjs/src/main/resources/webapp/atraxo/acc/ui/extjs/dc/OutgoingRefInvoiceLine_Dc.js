/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.OutgoingRefInvoiceLine_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.acc.ui.extjs.ds.OutgoingRefInvoiceLine_DsParam,
	recordModel: atraxo.acc.ui.extjs.ds.OutgoingRefInvoiceLine_Ds
});

/* ================= GRID: AddList ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingRefInvoiceLine_Dc$AddList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_OutgoingRefInvoiceLine_Dc$AddList",
	_noPaginator_: true,

	/**
	 * 
	 */
	initComponent : function(config) {
		Ext.apply(this, {
			features: [{
				ftype: 'summary',
				remoteRoot: 'summaries',
				dock: 'bottom'
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"deliveryDate", dataIndex:"deliveryDate", _mask_: Masks.DATETIME,  flex:1})
		.addTextColumn({ name:"customer", dataIndex:"custCode", width:50,  flex:1})
		.addTextColumn({ name:"ticketNo", dataIndex:"ticketNo", width:50,  flex:1})
		.addTextColumn({ name:"aircraftRegistration", dataIndex:"aircraftRegistrationNumber", width:50,  flex:1})
		.addTextColumn({ name:"flightNo", dataIndex:"flightNo", width:50,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
