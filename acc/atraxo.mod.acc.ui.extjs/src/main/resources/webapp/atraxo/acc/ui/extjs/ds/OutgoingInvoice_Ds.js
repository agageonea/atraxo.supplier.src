/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoice_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_OutgoingInvoice_Ds"
	},
	
	
	validators: {
		invoiceType: [{type: 'presence'}],
		invoiceNo: [{type: 'presence'}],
		issueDate: [{type: 'presence'}],
		deliveryDateFrom: [{type: 'presence'}],
		deliveryDateTo: [{type: 'presence'}],
		transactionType: [{type: 'presence'}],
		delLocCode: [{type: 'presence'}],
		receiverCode: [{type: 'presence'}],
		countryCode: [{type: 'presence'}],
		invoiceForm: [{type: 'presence'}],
		category: [{type: 'presence'}],
		currCode: [{type: 'presence'}],
		totalAmount: [{type: 'presence'}],
		quantity: [{type: 'presence'}],
		unitCode: [{type: 'presence'}],
		status: [{type: 'presence'}],
		controlNo: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("invoiceType", "Invoice");
		this.set("transactionType", "Original");
		this.set("invoiceForm", "Electronic");
		this.set("taxType", "Unspecified");
		this.set("status", "Draft");
		this.set("category", "Product into plane");
		this.set("transmissionStatus", "new");
		this.set("controlNo", "Not available");
	},
	
	fields: [
		{name:"receiverId", type:"int", allowNull:true},
		{name:"receiverCode", type:"string"},
		{name:"receiverName", type:"string"},
		{name:"delLocId", type:"int", allowNull:true},
		{name:"delLocCode", type:"string"},
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"issuerId", type:"int", allowNull:true},
		{name:"issuerCode", type:"string"},
		{name:"invoiceType", type:"string"},
		{name:"invoiceNo", type:"string"},
		{name:"invoiceDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"issueDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliveryDateFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliveryDateTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"transactionType", type:"string"},
		{name:"invoiceForm", type:"string"},
		{name:"category", type:"string"},
		{name:"referenceDocType", type:"string"},
		{name:"referenceDocNo", type:"string"},
		{name:"referenceDocId", type:"int", allowNull:true},
		{name:"totalAmount", type:"float", allowNull:true},
		{name:"vatAmount", type:"float", allowNull:true},
		{name:"vatTaxableAmount", type:"float", allowNull:true},
		{name:"quantity", type:"float", allowNull:true},
		{name:"baseLineDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"closeDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"itemsNumber", type:"int", allowNull:true},
		{name:"paymentDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"taxType", type:"string"},
		{name:"status", type:"string"},
		{name:"approvalStatus", type:"string"},
		{name:"product", type:"string"},
		{name:"creditMemoReason", type:"string"},
		{name:"transmissionStatus", type:"string"},
		{name:"exportStatus", type:"string"},
		{name:"exportDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"controlNo", type:"string"},
		{name:"invoiceRefId", type:"int", allowNull:true},
		{name:"invoiceRefNo", type:"string"},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"dueDays", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"vatRate", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"procent", type:"string", noFilter:true, noSort:true},
		{name:"type", type:"string", noFilter:true, noSort:true},
		{name:"scope", type:"string", noFilter:true, noSort:true},
		{name:"marked", type:"boolean", noFilter:true, noSort:true},
		{name:"deliveryType", type:"string", noFilter:true, noSort:true},
		{name:"deliveryId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"canBeCompleted", type:"boolean", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoice_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"receiverId", type:"int", allowNull:true},
		{name:"receiverCode", type:"string"},
		{name:"receiverName", type:"string"},
		{name:"delLocId", type:"int", allowNull:true},
		{name:"delLocCode", type:"string"},
		{name:"countryId", type:"int", allowNull:true},
		{name:"countryCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"issuerId", type:"int", allowNull:true},
		{name:"issuerCode", type:"string"},
		{name:"invoiceType", type:"string"},
		{name:"invoiceNo", type:"string"},
		{name:"invoiceDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"issueDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliveryDateFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliveryDateTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"transactionType", type:"string"},
		{name:"invoiceForm", type:"string"},
		{name:"category", type:"string"},
		{name:"referenceDocType", type:"string"},
		{name:"referenceDocNo", type:"string"},
		{name:"referenceDocId", type:"int", allowNull:true},
		{name:"totalAmount", type:"float", allowNull:true},
		{name:"vatAmount", type:"float", allowNull:true},
		{name:"vatTaxableAmount", type:"float", allowNull:true},
		{name:"quantity", type:"float", allowNull:true},
		{name:"baseLineDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"closeDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"itemsNumber", type:"int", allowNull:true},
		{name:"paymentDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"taxType", type:"string"},
		{name:"status", type:"string"},
		{name:"approvalStatus", type:"string"},
		{name:"product", type:"string"},
		{name:"creditMemoReason", type:"string"},
		{name:"transmissionStatus", type:"string"},
		{name:"exportStatus", type:"string"},
		{name:"exportDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"controlNo", type:"string"},
		{name:"invoiceRefId", type:"int", allowNull:true},
		{name:"invoiceRefNo", type:"string"},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"dueDays", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"vatRate", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"procent", type:"string", noFilter:true, noSort:true},
		{name:"type", type:"string", noFilter:true, noSort:true},
		{name:"scope", type:"string", noFilter:true, noSort:true},
		{name:"marked", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"deliveryType", type:"string", noFilter:true, noSort:true},
		{name:"deliveryId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"canBeCompleted", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoice_DsParam", {
	extend: 'Ext.data.Model',



	initParam: function() {
		this.set("transactionTypeParam", "Original");
		this.set("exportType", "PDF");
	},

	fields: [
		{name:"approvalNote", type:"string"},
		{name:"approveRejectResult", type:"string"},
		{name:"closingDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"dateFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"dateTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliveryAreaIdParam", type:"int", allowNull:true},
		{name:"deliveryLocIdParam", type:"int", allowNull:true},
		{name:"exportInvoiceDescription", type:"string"},
		{name:"exportInvoiceOption", type:"string"},
		{name:"exportInvoiceResult", type:"boolean"},
		{name:"exportType", type:"string"},
		{name:"filter", type:"string"},
		{name:"iataXmlFileName", type:"string"},
		{name:"invGeneratorErrorMsg", type:"string"},
		{name:"invoiceGenerationDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"nrGeneratedInvoices", type:"int", allowNull:true},
		{name:"receiverIdParam", type:"int", allowNull:true},
		{name:"reportCode", type:"string"},
		{name:"reportName", type:"string"},
		{name:"selectedAllInvoices", type:"boolean"},
		{name:"selectedAttachments", type:"string"},
		{name:"selectedInvoices", type:"string"},
		{name:"separatePerShipTo", type:"boolean"},
		{name:"standardFilter", type:"string"},
		{name:"submitForApprovalDescription", type:"string"},
		{name:"submitForApprovalResult", type:"boolean"},
		{name:"transactionTypeParam", type:"string"},
		{name:"unselectedInvoices", type:"string"}
	]
});
