/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoice_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.acc.ui.extjs.ds.OutgoingInvoice_DsParam,
	recordModel: atraxo.acc.ui.extjs.ds.OutgoingInvoice_Ds
});

/* ================= EDIT FORM: Export ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoice_Dc$Export", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_OutgoingInvoice_Dc$Export",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"exportType", bind:"{p.exportType}", paramIndex:"exportType", noLabel: true, store:[ __ACC_TYPE__.ExportTypeInvoice._PDF_, __ACC_TYPE__.ExportTypeInvoice._XML_],listeners:{
			change:{scope:this, fn:this._enableOkBtn_}
		}})
		.addDisplayFieldText({ name:"exportTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addLov({name:"reportName", bind:"{p.reportName}", paramIndex:"reportName", _visibleFn_: function(dc, rec) { return dc.params.get('exportType') == 'PDF'; } , noLabel: true, xtype:"ad_ExternalReportLov_Lov", maxLength:100,
			retFieldMapping: [{lovField:"designName", dsParam: "reportCode"} ],
			filterFieldMapping: [{lovField:"businessArea", value: "Sales invoices"} ],listeners:{
			change:{scope:this, fn:this._enableOkBtn_}
		}})
		.addDisplayFieldText({ name:"reportNameLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"row", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["row"])
		.addChildrenTo("row", ["exportTypeLabel", "exportType", "reportNameLabel", "reportName"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableOkBtn_: function() {
		
					var ctrl = this._controller_;
					var frame = ctrl.getFrame();
					var btn = frame._get_("btnOkExport");
					var exportType = this._get_("exportType").getValue();
					var reportName = this._get_("reportName");
					var reportNameLabel = this._get_("reportNameLabel");
					if(Ext.isEmpty(exportType)){
						btn.disable();
						reportName.setVisible(false);
						reportNameLabel.setVisible(false);
					} else {
						if(exportType !== "PDF" || !Ext.isEmpty(this._get_("reportName").getValue())){
							btn.enable();
						} else{
							btn.disable();
						}				
						reportName.setVisible(exportType === "PDF");
						reportNameLabel.setVisible(exportType === "PDF");						
					}
	}
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoice_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.acc_OutgoingInvoice_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"invoiceNo", dataIndex:"invoiceNo", maxLength:32})
			.addCombo({ xtype:"combo", name:"invoiceTye", dataIndex:"invoiceType", store:[ __ACC_TYPE__.InvoiceTypeAcc._INV_, __ACC_TYPE__.InvoiceTypeAcc._CRN_]})
			.addLov({name:"deliveryLocCode", dataIndex:"delLocCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "delLocId"} ]}})
			.addDateField({name:"issueDate", dataIndex:"issueDate"})
			.addNumberField({name:"totalAmount", dataIndex:"totalAmount", sysDec:"dec_amount", maxLength:19})
			.addLov({name:"currencyCode", dataIndex:"currCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"id", dsField: "currId"} ]}})
			.addNumberField({name:"quantity", dataIndex:"quantity", sysDec:"dec_unit", maxLength:19})
			.addLov({name:"unitCode", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2}})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __CMM_TYPE__.BillStatus._NOT_BILLED_, __CMM_TYPE__.BillStatus._DRAFT_, __CMM_TYPE__.BillStatus._CHECK_FAILED_, __CMM_TYPE__.BillStatus._CHECK_PASSED_, __CMM_TYPE__.BillStatus._NEEDS_RECHECK_, __CMM_TYPE__.BillStatus._AWAITING_APPROVAL_, __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_, __CMM_TYPE__.BillStatus._PAID_, __CMM_TYPE__.BillStatus._NO_CONTRACT_, __CMM_TYPE__.BillStatus._CREDITED_]})
			.addCombo({ xtype:"combo", name:"approvalStatus", dataIndex:"approvalStatus", store:[ __CMM_TYPE__.BidApprovalStatus._NEW_, __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_, __CMM_TYPE__.BidApprovalStatus._APPROVED_, __CMM_TYPE__.BidApprovalStatus._REJECTED_]})
			.addLov({name:"issuerCode", dataIndex:"issuerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserSubsidiaryLov_Lov", selectOnFocus:true, maxLength:32}})
			.addCombo({ xtype:"combo", name:"product", dataIndex:"product", store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_]})
			.addLov({name:"customer", dataIndex:"receiverCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "receiverId"} ]}})
			.addLov({name:"customerName", dataIndex:"receiverName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "receiverId"} ]}})
			.addCombo({ xtype:"combo", name:"creditMemoReason", dataIndex:"creditMemoReason", store:[ __OPS_TYPE__.RevocationReason._INCORRECT_CUSTOMER_INFORMATION_, __OPS_TYPE__.RevocationReason._INCORRECT_FLIGHT_AIRCRAFT_INFORMATION_, __OPS_TYPE__.RevocationReason._INCORRECT_VOLUME_, __OPS_TYPE__.RevocationReason._INCORRECT_PRICE___DISCOUNT___TAX___FREIGHT_, __OPS_TYPE__.RevocationReason._COMMERCIAL_REASON_]})
			.addCombo({ xtype:"combo", name:"transmissionStatus", dataIndex:"transmissionStatus", store:[ __ACC_TYPE__.InvoiceTransmissionStatus._NEW_, __ACC_TYPE__.InvoiceTransmissionStatus._IN_PROGRESS_, __ACC_TYPE__.InvoiceTransmissionStatus._FAILED_, __ACC_TYPE__.InvoiceTransmissionStatus._PARTIALLY_TRANSMITTED_, __ACC_TYPE__.InvoiceTransmissionStatus._TRANSMITTED_]})
			.addCombo({ xtype:"combo", name:"exportStatus", dataIndex:"exportStatus", store:[ __ACC_TYPE__.InvoiceExportStatus._NEW_, __ACC_TYPE__.InvoiceExportStatus._EXPORTED_, __ACC_TYPE__.InvoiceExportStatus._UPLOADED_, __ACC_TYPE__.InvoiceExportStatus._EXPORT_FAILED_, __ACC_TYPE__.InvoiceExportStatus._UPLOAD_FAILED_]})
			.addDateField({name:"exportDate", dataIndex:"exportDate"})
		;
	}

});

/* ================= GRID: InvoiceList ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoice_Dc$InvoiceList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_OutgoingInvoice_Dc$InvoiceList",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"invoiceNo", dataIndex:"invoiceNo", width:80})
		.addBooleanColumn({ name:"marked", dataIndex:"marked", width:50,  renderer:function(val, meta, record, rowIndex) { return this._setCheckStatusColor_(val, meta, record, rowIndex); }})
		.addNumberColumn({ name:"dueDays", dataIndex:"dueDays", width:80,  renderer:function(val, meta, record, rowIndex) { return this._setDueDaysColor_(val, meta, record, rowIndex); }})
		.addDateColumn({ name:"dueDate", dataIndex:"baseLineDate", width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"closeDate", dataIndex:"closeDate", hidden:true, width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"customer", dataIndex:"receiverCode", width:80})
		.addTextColumn({ name:"customerName", dataIndex:"receiverName", width:150})
		.addTextColumn({ name:"type", dataIndex:"invoiceType", width:80})
		.addTextColumn({ name:"location", dataIndex:"delLocCode", width:80})
		.addDateColumn({ name:"deliveryFrom", dataIndex:"deliveryDateFrom", width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"deliveryTo", dataIndex:"deliveryDateTo", width:100, _mask_: Masks.DATE})
		.addNumberColumn({ name:"items", dataIndex:"itemsNumber", width:60})
		.addTextColumn({ name:"taxType", dataIndex:"taxType", width:100})
		.addNumberColumn({ name:"netAmount", dataIndex:"vatTaxableAmount", width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"vatAmount", dataIndex:"vatAmount", width:80, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"amount", dataIndex:"totalAmount", width:100, sysDec:"dec_amount"})
		.addTextColumn({ name:"currency", dataIndex:"currCode", width:80})
		.addNumberColumn({ name:"quantity", dataIndex:"quantity", width:100, sysDec:"dec_unit"})
		.addTextColumn({ name:"unitCode", dataIndex:"unitCode", width:60})
		.addTextColumn({ name:"status", dataIndex:"status", width:130})
		.addTextColumn({ name:"approvalStatus", dataIndex:"approvalStatus", width:130})
		.addDateColumn({ name:"invDate", dataIndex:"invoiceDate", hidden:true, width:100, _mask_: Masks.DATE})
		.addDateColumn({ name:"issueDate", dataIndex:"issueDate", hidden:true, width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"country", dataIndex:"countryCode", hidden:true, width:100})
		.addTextColumn({ name:"transactionType", dataIndex:"transactionType", hidden:true, width:100})
		.addTextColumn({ name:"invoiceForm", dataIndex:"invoiceForm", hidden:true, width:100})
		.addTextColumn({ name:"category", dataIndex:"category", hidden:true, width:100})
		.addTextColumn({ name:"referenceDocType", dataIndex:"referenceDocType", hidden:true, width:100})
		.addTextColumn({ name:"referenceDocNo", dataIndex:"referenceDocNo", hidden:true, width:100})
		.addNumberColumn({ name:"referenceDocId", dataIndex:"referenceDocId", hidden:true, width:100})
		.addDateColumn({ name:"paymentDate", dataIndex:"paymentDate", hidden:true, width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"issuerCode", dataIndex:"issuerCode", hidden:true, width:100})
		.addTextColumn({ name:"invoiceRefNo", dataIndex:"invoiceRefNo", hidden:true, width:140})
		.addTextColumn({ name:"product", dataIndex:"product", hidden:true, width:140})
		.addTextColumn({ name:"creditMemoReason", dataIndex:"creditMemoReason", hidden:true, width:100})
		.addTextColumn({ name:"transmissionStatus", dataIndex:"transmissionStatus", hidden:true, width:100})
		.addTextColumn({ name:"exportStatus", dataIndex:"exportStatus", hidden:true, width:100})
		.addDateColumn({ name:"exportDate", dataIndex:"exportDate", hidden:true, width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"controlNo", dataIndex:"controlNo", hidden:true, width:120})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_setCheckStatusColor_: function(val,meta,record,rowIndex) {
		
						var icon = "";
						if (val) {
							icon = "<div style='width:100%; text-align:center'><i class='fa fa-circle' style='color:#f65a84'></i></div>";
						}
						else {
							icon = "<div style='width:100%; text-align:center'><i class='fa fa-circle' style='color:#FFFFFF'></i></div>";
						}
						return icon;
	},
	
	_setDueDaysColor_: function(val,meta,record,rowIndex) {
		
						if(val == null) return;
						var color = "";
						if (val >= 0) {
							color = "green";
						}
						else {
							color = "#F94F52";
						}
						return "<span style='color: "+color+"'>"+val+"</span>";
	}
});

/* ================= EDIT FORM: ApprovalNote ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoice_Dc$ApprovalNote", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_OutgoingInvoice_Dc$ApprovalNote",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.approvalNote}", paramIndex:"approvalNote", width:580, labelAlign:"top", labelStyle:"font-weight:bold", fieldStyle:"margin-top: 5px;"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:580})
		.addPanel({ name:"col1", width:580, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
	}
});

/* ================= EDIT FORM: Generate ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoice_Dc$Generate", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_OutgoingInvoice_Dc$Generate",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"deliveryTo", bind:"{p.dateTo}", paramIndex:"dateTo", allowBlank:false, width:350, labelWidth:175})
		.addCombo({ xtype:"combo", name:"transactionType", bind:"{p.transactionTypeParam}", paramIndex:"transactionTypeParam", allowBlank:false, width:350, store:[ __ACC_TYPE__.TransactionType._CASH_, __ACC_TYPE__.TransactionType._CORRECTED_, __ACC_TYPE__.TransactionType._FINAL_, __ACC_TYPE__.TransactionType._ORIGINAL_, __ACC_TYPE__.TransactionType._PREPAID_INVOICE_CORRECTION_, __ACC_TYPE__.TransactionType._PREPAID_INVOICE_, __ACC_TYPE__.TransactionType._PROVISIONAL_], labelWidth:175})
		.addDateField({name:"invoiceGenerationDate", bind:"{p.invoiceGenerationDate}", paramIndex:"invoiceGenerationDate", width:350, labelWidth:175})
		.addDateField({name:"closeDate", bind:"{p.closingDate}", paramIndex:"closingDate", width:350, labelWidth:175})
		.addLov({name:"customer", bind:"{d.receiverCode}", dataIndex:"receiverCode", width:350, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32, labelWidth:175,
			retFieldMapping: [{lovField:"id", dsParam: "receiverIdParam"} ,{lovField:"name", dsField: "receiverName"} ]})
		.addBooleanField({ name:"separatePerShipTo", bind:"{p.separatePerShipTo}", paramIndex:"separatePerShipTo", width:350, labelWidth:175})
		.addLov({name:"deliveryLoc", bind:"{d.delLocCode}", dataIndex:"delLocCode", width:350, xtype:"fmbas_LocationsAreasLov_Lov", maxLength:25, labelWidth:175,
			retFieldMapping: [{lovField:"type", dsField: "deliveryType"} ,{lovField:"id", dsField: "deliveryId"} ]})
		.addHiddenField({ name:"deliveryId", bind:"{d.deliveryId}", dataIndex:"deliveryId"})
		.addHiddenField({ name:"deliveryType", bind:"{d.deliveryType}", dataIndex:"deliveryType"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"row", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["row"])
		.addChildrenTo("row", ["deliveryTo", "transactionType", "invoiceGenerationDate", "closeDate", "customer", "separatePerShipTo", "deliveryLoc", "deliveryType", "deliveryId"]);
	}
});

/* ================= EDIT FORM: InvoiceHeader ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoice_Dc$InvoiceHeader", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_OutgoingInvoice_Dc$InvoiceHeader",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"invoiceNo", bind:"{d.invoiceNo}", dataIndex:"invoiceNo", noEdit:true , allowBlank:false, maxLength:32, cls:"sone-flat-field"})
		.addTextField({ name:"dummyField", bind:"{d.dummyField}", dataIndex:"dummyField", maxLength:1, style:"display:none"})
		.addDisplayFieldText({ name:"category", bind:"{d.category}", dataIndex:"category", noEdit:true , noLabel: true, maxLength:32})
		.addDisplayFieldText({ name:"receiver", bind:"{d.receiverCode}", dataIndex:"receiverCode", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"issuer", bind:"{d.issuerCode}", dataIndex:"issuerCode", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"deliveryLocCode", bind:"{d.delLocCode}", dataIndex:"delLocCode", noEdit:true , maxLength:25})
		.addDisplayFieldText({ name:"invoiceForm", bind:"{d.invoiceForm}", dataIndex:"invoiceForm", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"status", bind:"{d.status}", dataIndex:"status", noEdit:true , maxLength:32})
		.addDisplayFieldNumber({ name:"dueIn", bind:"{d.dueDays}", dataIndex:"dueDays", noEdit:true , maxLength:11 })
		.addDisplayFieldText({ name:"invoiceRefNo", bind:"{d.invoiceRefNo}", dataIndex:"invoiceRefNo", _enableFn_: function(dc, rec) { return dc.getRecord().get('status') != __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_ && dc.getRecord().get('status') != __CMM_TYPE__.BillStatus._PAID_; } , maxLength:32, style:"cursor: pointer", cls:"sone-trigger-kpi", listeners:{afterrender: {scope: this, fn: function(el) {this._changeReferenceInvoice_(el)}}}})
		.addDateField({name:"invDate", bind:"{d.invoiceDate}", dataIndex:"invoiceDate", _enableFn_: function(dc, rec) { return dc.getRecord().get('status') == __CMM_TYPE__.BillStatus._AWAITING_APPROVAL_ && dc.getRecord().get('approvalStatus')!= __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_; } , width:260, labelWidth:150})
		.addDateField({name:"issueDate", bind:"{d.issueDate}", dataIndex:"issueDate", _enableFn_: function(dc, rec) { return dc.getRecord().get('status') == __CMM_TYPE__.BillStatus._AWAITING_APPROVAL_ && dc.getRecord().get('approvalStatus')!= __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_; } , allowBlank:false, width:260, labelWidth:150})
		.addCombo({ xtype:"combo", name:"refDoc", bind:"{d.referenceDocType}", dataIndex:"referenceDocType", noEdit:true , width:270, store:[ __ACC_TYPE__.InvoiceReferenceDocType._CONTRACT_, __ACC_TYPE__.InvoiceReferenceDocType._PURCHASE_ORDER_], labelWidth:150})
		.addTextField({ name:"refDocNo", bind:"{d.referenceDocNo}", dataIndex:"referenceDocNo", noEdit:true , width:270, maxLength:32, labelWidth:150})
		.addCombo({ xtype:"combo", name:"transactionType", bind:"{d.transactionType}", dataIndex:"transactionType", _enableFn_: function(dc, rec) { return dc.getRecord().get('status') == __CMM_TYPE__.BillStatus._AWAITING_APPROVAL_ && dc.getRecord().get('transactionType') != 'Corrected'; } , allowBlank:false, width:240, store:[ __ACC_TYPE__.TransactionType._CASH_, __ACC_TYPE__.TransactionType._CORRECTED_, __ACC_TYPE__.TransactionType._FINAL_, __ACC_TYPE__.TransactionType._ORIGINAL_, __ACC_TYPE__.TransactionType._PREPAID_INVOICE_CORRECTION_, __ACC_TYPE__.TransactionType._PREPAID_INVOICE_, __ACC_TYPE__.TransactionType._PROVISIONAL_], labelWidth:125})
		.addDateField({name:"deliveryDateFrom", bind:"{d.deliveryDateFrom}", dataIndex:"deliveryDateFrom", noEdit:true , allowBlank:false, width:260, labelWidth:150})
		.addDateField({name:"deliverydateTo", bind:"{d.deliveryDateTo}", dataIndex:"deliveryDateTo", noEdit:true , allowBlank:false, width:270, labelWidth:150})
		.addNumberField({name:"totalAmount", bind:"{d.totalAmount}", dataIndex:"totalAmount", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:220, sysDec:"dec_amount", maxLength:19, labelWidth:120})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:220, sysDec:"dec_unit", maxLength:19, labelWidth:120})
		.addTextField({ name:"product", bind:"{d.product}", dataIndex:"product", noEdit:true , width:290, maxLength:32, labelWidth:120})
		.addLov({name:"unitCode", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:70, noLabel: true, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addLov({name:"currencyCode", bind:"{d.currCode}", dataIndex:"currCode", _enableFn_: function(dc, rec) { return this._isEnabled(rec); } , allowBlank:false, width:70, noLabel: true, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ]})
		.addNumberField({name:"vatTaxableAmount", bind:"{d.vatTaxableAmount}", dataIndex:"vatTaxableAmount", noEdit:true , width:230, sysDec:"dec_crncy", maxLength:19, labelWidth:120})
		.addNumberField({name:"vatAmount", bind:"{d.vatAmount}", dataIndex:"vatAmount", noEdit:true , width:190, sysDec:"dec_crncy", maxLength:19, labelWidth:120})
		.addTextField({ name:"controlNo", bind:"{d.controlNo}", dataIndex:"controlNo", allowBlank:false, width:240, maxLength:32, labelWidth:130})
		.addNumberField({name:"vatRate", bind:"{d.vatRate}", dataIndex:"vatRate", noEdit:true , width:40, noLabel: true, sysDec:"dec_prc", maxLength:19})
		.addDisplayFieldText({ name:"procent", bind:"{d.procent}", dataIndex:"procent", noEdit:true , width:10, maxLength:10, labelAlign:"left"})
		.addDateField({name:"baseLine", bind:"{d.baseLineDate}", dataIndex:"baseLineDate", _enableFn_: function(dc, rec) { return dc.getRecord().get('status') == __CMM_TYPE__.BillStatus._AWAITING_APPROVAL_ && dc.getRecord().get('approvalStatus')!= __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_; } , width:240, labelWidth:125})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("totalAmount"),this._getConfig_("currencyCode")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantity"),this._getConfig_("unitCode")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("vatAmount"),this._getConfig_("vatRate"),this._getConfig_("procent")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("dummyField"),this._getConfig_("invoiceNo")]})
		.add({name:"cf", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("deliveryDateFrom"),this._getConfig_("deliverydateTo"),this._getConfig_("product"),this._getConfig_("controlNo")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"p2",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"h1", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"c00", width:150, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c0", width:150, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c1", width:130, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:130, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:185, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:80, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c5", width:80, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"})
		.addPanel({ name:"v1", width:260, layout:"anchor"})
		.addPanel({ name:"v2", width:270, layout:"anchor"})
		.addPanel({ name:"v3", width:300, layout:"anchor"})
		.addPanel({ name:"v4", width:260, layout:"anchor"})
		.addPanel({ name:"v5", width:300, layout:"anchor"})
		.addPanel({ name:"r1", width:1500, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "h1", "r1"])
		.addChildrenTo("p2", ["c00", "c0", "c1", "c2", "c3", "c4", "c5"])
		.addChildrenTo("titleAndKpi", ["title", "p2"])
		.addChildrenTo("h1", ["v1", "v2", "v3", "v4", "v5"])
		.addChildrenTo("c00", ["issuer"])
		.addChildrenTo("c0", ["receiver"])
		.addChildrenTo("c1", ["deliveryLocCode"])
		.addChildrenTo("c2", ["invoiceForm"])
		.addChildrenTo("c3", ["status"])
		.addChildrenTo("c4", ["dueIn"])
		.addChildrenTo("c5", ["invoiceRefNo"])
		.addChildrenTo("title", ["row5"])
		.addChildrenTo("v1", ["invDate", "issueDate"])
		.addChildrenTo("v2", ["refDoc", "refDocNo"])
		.addChildrenTo("v3", ["row2", "row3"])
		.addChildrenTo("v4", ["vatTaxableAmount", "row4"])
		.addChildrenTo("v5", ["baseLine", "transactionType"])
		.addChildrenTo("r1", ["cf"]);
	},
	/* ==================== Business functions ==================== */
	
	_changeReferenceInvoice_: function(el) {
		
		
						el.getEl().on("click", function() {
		
							var dc = this._controller_;
							var r = dc.getRecord();
		 
							if (r) {
								var status = r.get("status");
		
								if (status !== __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_ && status !== __CMM_TYPE__.BillStatus._PAID_) {
									dc.setParamValue("displayCustomDelMsg", true);
									dc.doDelete({showWizard: true});
								}
							}
					    }, this); 
	},
	
	_filterInvoices_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							var filters = [
								new Ext.util.Filter({
							      filterFn: function(item){
							        return item.get("status") === __TYPES__.fuelTicket.invoiceStatus.awaitingPayment || item.get("status") === __TYPES__.fuelTicket.invoiceStatus.paid && item.get("invoiceRefId") == "";
							      }
								})
							];
							store.filter(filters);
						}, this);
	},
	
	_afterDefineElements_: function() {
		
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});				
	},
	
	_isEnabled: function(rec) {
		
						var res = false;
						if( rec ){
							res = (rec.data.status === __TYPES__.invoices.status.draft);
						}
						return res;
	},
	
	_afterApplyStates_: function() {
		
						var r = this._controller_.getRecord();
						if (r) {
							var formTitle = this._get_("invoiceNo");
							if( formTitle.labelEl ){
								var category = r.get("category");
								var originalTitleLabel = formTitle.fieldLabel;
								formTitle.labelEl.update(category+" "+originalTitleLabel);
							}
						}
	}
});
