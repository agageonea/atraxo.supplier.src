/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoiceLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_OutgoingInvoiceLov_Ds"
	},
	
	
	fields: [
		{name:"invoiceId", type:"int", allowNull:true},
		{name:"invoiceNo", type:"string"},
		{name:"status", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoiceLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"invoiceId", type:"int", allowNull:true},
		{name:"invoiceNo", type:"string"},
		{name:"status", type:"string"},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
