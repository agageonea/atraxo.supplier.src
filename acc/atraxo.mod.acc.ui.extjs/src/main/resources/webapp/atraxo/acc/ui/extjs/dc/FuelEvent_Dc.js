/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.FuelEvent_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.acc.ui.extjs.ds.FuelEvents_DsParam,
	recordModel: atraxo.acc.ui.extjs.ds.FuelEvents_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.acc.ui.extjs.dc.FuelEvent_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.acc_FuelEvent_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"fuelingDate", dataIndex:"fuelingDate"})
			.addLov({name:"location", dataIndex:"departureCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "departureId"} ]}})
			.addLov({name:"customer", dataIndex:"customerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addLov({name:"aircraft", dataIndex:"aircraftRegistration", maxLength:10,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AircraftLov_Lov", selectOnFocus:true, maxLength:10,
					retFieldMapping: [{lovField:"id", dsField: "aircraftId"} ]}})
			.addTextField({ name:"flightNumber", dataIndex:"flightNumber", maxLength:32})
			.addTextField({ name:"ticketNumber", dataIndex:"ticketNumber", maxLength:32})
			.addLov({name:"supplier", dataIndex:"supplierCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "supplierId"} ]}})
			.addLov({name:"iplAgent", dataIndex:"iplAgentCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "iplAgentCode"} ]}})
			.addNumberField({name:"fuelingQuantity", dataIndex:"quantity", sysDec:"dec_unit", maxLength:19})
			.addLov({name:"unit", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"id", dsField: "unitId"} ]}})
			.addNumberField({name:"payableCost", dataIndex:"payableCost", sysDec:"dec_crncy", maxLength:19})
			.addLov({name:"payableCurrency", dataIndex:"payCurrencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"id", dsField: "payCurrencyId"} ]}})
			.addNumberField({name:"billableCost", dataIndex:"billabeCost", sysDec:"dec_crncy", maxLength:19})
			.addLov({name:"billableCurrency", dataIndex:"billCurrencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"id", dsField: "billCurrencyId"} ]}})
			.addCombo({ xtype:"combo", name:"source", dataIndex:"quantitySource", store:[ __ACC_TYPE__.QuantitySource._FUEL_ORDER_, __ACC_TYPE__.QuantitySource._FUEL_TICKET_, __ACC_TYPE__.QuantitySource._INVOICE_]})
			.addCombo({ xtype:"combo", name:"invoiceStatus", dataIndex:"invoiceStatus", store:[ __OPS_TYPE__.OutgoingInvoiceStatus._NOT_INVOICED_, __OPS_TYPE__.OutgoingInvoiceStatus._AWAITING_APPROVAL_, __OPS_TYPE__.OutgoingInvoiceStatus._AWAITING_PAYMENT_, __OPS_TYPE__.OutgoingInvoiceStatus._PAID_, __OPS_TYPE__.OutgoingInvoiceStatus._CREDITED_]})
			.addCombo({ xtype:"combo", name:"billStatus", dataIndex:"billStatus", store:[ __CMM_TYPE__.BillStatus._NOT_BILLED_, __CMM_TYPE__.BillStatus._DRAFT_, __CMM_TYPE__.BillStatus._CHECK_FAILED_, __CMM_TYPE__.BillStatus._CHECK_PASSED_, __CMM_TYPE__.BillStatus._NEEDS_RECHECK_, __CMM_TYPE__.BillStatus._AWAITING_APPROVAL_, __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_, __CMM_TYPE__.BillStatus._PAID_, __CMM_TYPE__.BillStatus._NO_CONTRACT_, __CMM_TYPE__.BillStatus._CREDITED_]})
			.addCombo({ xtype:"combo", name:"receivedStatus", dataIndex:"receivedStatus", store:[ __ACC_TYPE__.ReceivedStatus._ON_HOLD_, __ACC_TYPE__.ReceivedStatus._RELEASED_]})
			.addBooleanField({ name:"isResale", dataIndex:"isResale"})
			.addLov({name:"contractHolder", dataIndex:"contractHolderCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "contractHolderID"} ]}})
			.addLov({name:"shipTo", dataIndex:"shipToCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "shipToID"} ]}})
			.addLov({name:"shipToName", dataIndex:"shipToName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "shipToID"} ]}})
			.addTextField({ name:"accountNumber", dataIndex:"accountNumber", maxLength:32})
			.addCombo({ xtype:"combo", name:"product", dataIndex:"product", store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_]})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __ACC_TYPE__.FuelEventStatus._ORIGINAL_, __ACC_TYPE__.FuelEventStatus._CANCELED_, __ACC_TYPE__.FuelEventStatus._REISSUE_, __ACC_TYPE__.FuelEventStatus._UPDATED_]})
			.addCombo({ xtype:"combo", name:"transmissionStatusSale", dataIndex:"transmissionStatusSale", store:[ __ACC_TYPE__.FuelEventTransmissionStatus._NEW_, __ACC_TYPE__.FuelEventTransmissionStatus._IN_PROGRESS_, __ACC_TYPE__.FuelEventTransmissionStatus._TRANSMITTED_, __ACC_TYPE__.FuelEventTransmissionStatus._FAILED_]})
			.addCombo({ xtype:"combo", name:"transmissionStatusPurchase", dataIndex:"transmissionStatusPurchase", store:[ __ACC_TYPE__.FuelEventTransmissionStatus._NEW_, __ACC_TYPE__.FuelEventTransmissionStatus._IN_PROGRESS_, __ACC_TYPE__.FuelEventTransmissionStatus._TRANSMITTED_, __ACC_TYPE__.FuelEventTransmissionStatus._FAILED_]})
			.addTextField({ name:"erpSalesShipmentNo", dataIndex:"erpSalesShipmentNo", maxLength:32})
			.addTextField({ name:"erpPurchaseReceiptNo", dataIndex:"erpPurchaseReceiptNo", maxLength:32})
			.addTextField({ name:"erpSaleInvoiceNo", dataIndex:"erpSaleInvoiceNo", maxLength:32})
			.addTextField({ name:"creditMemoNo", dataIndex:"creditMemoNo", maxLength:32})
			.addCombo({ xtype:"combo", name:"revocationReason", dataIndex:"revocationReason", store:[ __OPS_TYPE__.RevocationReason._INCORRECT_CUSTOMER_INFORMATION_, __OPS_TYPE__.RevocationReason._INCORRECT_FLIGHT_AIRCRAFT_INFORMATION_, __OPS_TYPE__.RevocationReason._INCORRECT_VOLUME_, __OPS_TYPE__.RevocationReason._INCORRECT_PRICE___DISCOUNT___TAX___FREIGHT_, __OPS_TYPE__.RevocationReason._COMMERCIAL_REASON_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.acc.ui.extjs.dc.FuelEvent_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_FuelEvent_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addBooleanColumn({ name:"isResale", dataIndex:"isResale", width:50,  draggable:false, align:"center", renderer:function(val, meta, record) {return this._markResale_(val, meta, record);}})
		.addTextColumn({ name:"status", dataIndex:"status", width:145,  draggable:false, hideable:false, renderer:function(value, metaData) {return this._badgifyColumn_(value); }})
		.addDateColumn({ name:"fuelingDate", dataIndex:"fuelingDate", width:120, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"location", dataIndex:"departureCode", width:100})
		.addTextColumn({ name:"customer", dataIndex:"customerCode", width:100})
		.addTextColumn({ name:"aircraftRegistration", dataIndex:"aircraftRegistration", width:130})
		.addTextColumn({ name:"flightNumber", dataIndex:"flightNumber", width:80})
		.addTextColumn({ name:"ticketNumber", dataIndex:"ticketNumber", width:80})
		.addTextColumn({ name:"supplier", dataIndex:"supplierCode", width:100})
		.addTextColumn({ name:"iplAgent", dataIndex:"iplAgentCode", width:100})
		.addNumberColumn({ name:"quantity", dataIndex:"quantity", width:120, sysDec:"dec_unit"})
		.addNumberColumn({ name:"sysQuantity", dataIndex:"sysQuantity", hidden:true, width:120, sysDec:"dec_unit"})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:70})
		.addNumberColumn({ name:"payableCost", dataIndex:"payableCost", width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"payableCurrency", dataIndex:"payCurrencyCode", width:100})
		.addNumberColumn({ name:"sysPayableCost", dataIndex:"sysPayableCost", hidden:true, width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"billableCost", dataIndex:"billabeCost", width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"billableCurrency", dataIndex:"billCurrencyCode", width:100})
		.addNumberColumn({ name:"sysBillableCost", dataIndex:"sysBillableCost", hidden:true, width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"source", dataIndex:"quantitySource", width:100})
		.addTextColumn({ name:"invoiceStatus", dataIndex:"invoiceStatus", width:100})
		.addTextColumn({ name:"billStatus", dataIndex:"billStatus", width:100})
		.addTextColumn({ name:"receivedStatus", dataIndex:"receivedStatus", hidden:true, width:100})
		.addTextColumn({ name:"eventType", dataIndex:"eventType", hidden:true, width:100})
		.addNumberColumn({ name:"netUpliftQuantity", dataIndex:"netUpliftQuantity", hidden:true, width:100, sysDec:"dec_unit"})
		.addTextColumn({ name:"netUnit", dataIndex:"netUnitCode", hidden:true, width:70})
		.addTextColumn({ name:"fuelingOperation", dataIndex:"fuelingOperation", hidden:true, width:100})
		.addTextColumn({ name:"transport", dataIndex:"transport", hidden:true, width:100})
		.addTextColumn({ name:"operationType", dataIndex:"operationType", hidden:true, width:100})
		.addTextColumn({ name:"erpSalesShipmentNo", dataIndex:"erpSalesShipmentNo", hidden:true, width:100})
		.addTextColumn({ name:"salesInvoiceNo", dataIndex:"salesInvoiceNo", hidden:true, width:100})
		.addTextColumn({ name:"purchaseInvoiceNo", dataIndex:"purchaseInvoiceNo", hidden:true, width:100})
		.addTextColumn({ name:"creditMemoNo", dataIndex:"creditMemoNo", hidden:true, width:120})
		.addTextColumn({ name:"contractHolderCode", dataIndex:"contractHolderCode", width:120})
		.addTextColumn({ name:"shipTo", dataIndex:"shipToCode", hidden:true, width:100})
		.addTextColumn({ name:"shipToName", dataIndex:"shipToName", hidden:true, width:150})
		.addTextColumn({ name:"accountNumber", dataIndex:"accountNumber", hidden:true, width:100})
		.addTextColumn({ name:"product", dataIndex:"product", hidden:true, width:100})
		.addTextColumn({ name:"flightID", dataIndex:"flightID", hidden:true, width:100})
		.addTextColumn({ name:"transmissionStatusSale", dataIndex:"transmissionStatusSale", hidden:true, width:100})
		.addTextColumn({ name:"transmissionStatusPurchase", dataIndex:"transmissionStatusPurchase", hidden:true, width:100})
		.addTextColumn({ name:"erpPurchaseReceiptNo", dataIndex:"erpPurchaseReceiptNo", hidden:true, width:100})
		.addTextColumn({ name:"erpSaleInvoiceNo", dataIndex:"erpSaleInvoiceNo", hidden:true, width:100})
		.addNumberColumn({ name:"density", dataIndex:"density", hidden:true, width:70, sysDec:"dec_unit"})
		.addTextColumn({ name:"densityMassUnit", dataIndex:"densityUnitCode", hidden:true, width:120})
		.addTextColumn({ name:"densityVolumeUnit", dataIndex:"densityVolumeCode", hidden:true, width:130})
		.addNumberColumn({ name:"temperature", dataIndex:"temperature", hidden:true, width:90})
		.addTextColumn({ name:"temperatureUnit", dataIndex:"temperatureUnit", hidden:true, width:120})
		.addTextColumn({ name:"revocationReason", dataIndex:"revocationReason", hidden:true, width:120})
		.addTextColumn({ name:"bolNumber", dataIndex:"bolNumber", hidden:true, width:140})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_markResale_: function(val,meta,record) {
		
						var marker = "";
						if (val === true) {
							marker = "<i class='fa fa-1x fa-registered sone-resale-marker'></i>";
						}
		
						meta.tdAttr= "data-qtip='" + record.getData().resaleDetail + "'";
						return marker;
	},
	
	_badgifyColumn_: function(value) {
		
						var badgeCls = "sone-badge-default";
						if (value === __ACC_TYPE__.FuelEventStatus._ORIGINAL_) {
							badgeCls = "sone-badge-default";
						}
						else if (value === __ACC_TYPE__.FuelEventStatus._CANCELED_) {
							badgeCls = "sone-badge-red";
						}
						else if (value === __ACC_TYPE__.FuelEventStatus._REISSUE_) {
							badgeCls = "sone-badge-yellow";
						}				
						else if (value === __ACC_TYPE__.FuelEventStatus._UPDATED_) {
							badgeCls = "sone-badge-blue";
						}
						var badge = "<div class='sone-badge "+badgeCls+"'>"+value+"</div>";
						return badge;
	},
	
	_endDefine_: function() {
		
						this.plugins = [].concat(this.plugins?this.plugins:[],[
							{
								ptype: "gridfilters"
							},{
								ptype: "rowexpanderplus",
								pluginId : "rowexpanderplus",
								rowBodyTpl : new Ext.XTemplate(
									"<div id='rec-{id}'></div>"
								)
							}
						]);
						this.selType = "rowmodel"
	},
	
	_afterInitComponent_: function() {
		
						var headerCt = this.getView().getHeaderCt();
						var isResale = headerCt.down("[dataIndex=isResale]");
						isResale.text = "";
						this._buildChildGrid_({
							childDc: "fuelEventsDetails",
							gridCls: atraxo.acc.ui.extjs.dc.FuelEventsDetails_Dc$List,
							filterField: "id",
							applyFilter : false
						});	
		
	}
});

/* ================= GRID: ListOutgoingInvoiceLine ================= */

Ext.define("atraxo.acc.ui.extjs.dc.FuelEvent_Dc$ListOutgoingInvoiceLine", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_FuelEvent_Dc$ListOutgoingInvoiceLine",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"fuelingDate", dataIndex:"fuelingDate", width:120, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"customer", dataIndex:"customerCode", width:50,  flex:1})
		.addTextColumn({ name:"aircraftRegistration", dataIndex:"aircraftRegistration", width:140})
		.addTextColumn({ name:"flightNumber", dataIndex:"flightNumber", width:50,  flex:1})
		.addTextColumn({ name:"ticketNumber", dataIndex:"ticketNumber", width:50,  flex:1})
		.addTextColumn({ name:"supplier", dataIndex:"supplierCode", width:100})
		.addTextColumn({ name:"iplAgent", dataIndex:"iplAgentCode", width:50,  flex:1})
		.addNumberColumn({ name:"quantity", dataIndex:"quantity", sysDec:"dec_unit",  flex:1})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:50,  flex:1})
		.addNumberColumn({ name:"billableCost", dataIndex:"billabeCost", sysDec:"dec_crncy",  flex:1})
		.addTextColumn({ name:"billableCurrency", dataIndex:"billCurrencyCode", width:50,  flex:1})
		.addTextColumn({ name:"source", dataIndex:"quantitySource", width:70,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: FilterForm ================= */

Ext.define("atraxo.acc.ui.extjs.dc.FuelEvent_Dc$FilterForm", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_FuelEvent_Dc$FilterForm",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"dateStart", bind:"{p.dateStart}", paramIndex:"dateStart", width:165})
		.addDateField({name:"dateEnd", bind:"{p.dateEnd}", paramIndex:"dateEnd", width:165})
		.addTextField({ name:"aircraftReg", bind:"{p.aircraftReg}", paramIndex:"aircraftReg", width:165, maxLength:64})
		.addTextField({ name:"flight", bind:"{p.flight}", paramIndex:"flight", width:165, maxLength:64})
		.addTextField({ name:"fuelTicket", bind:"{p.fuelTicket}", paramIndex:"fuelTicket", width:165, maxLength:64})
		.addLov({name:"supplier", bind:"{p.supplier}", paramIndex:"supplier", width:160, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:64,
			retFieldMapping: [{lovField:"code", dsParam: "supplier"} ]})
		/* =========== containers =========== */
		.addPanel({ name:"c0", width:180, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c1", width:180, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:180, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:180, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:180, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c5", width:180, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"table", layout: {type:"table"}})
		.addPanel({ name:"main", autoScroll:true,  cls:"sone-no-overflow"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("c0", ["dateStart"])
		.addChildrenTo("c1", ["dateEnd"])
		.addChildrenTo("c2", ["aircraftReg"])
		.addChildrenTo("c3", ["flight"])
		.addChildrenTo("c4", ["fuelTicket"])
		.addChildrenTo("c5", ["supplier"])
		.addChildrenTo("table", ["c0", "c1", "c2", "c3", "c4", "c5"])
		.addChildrenTo("main", ["table"]);
	}
});

/* ================= EDIT FORM: Form ================= */

Ext.define("atraxo.acc.ui.extjs.dc.FuelEvent_Dc$Form", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_FuelEvent_Dc$Form",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldText({ name:"formTitle", bind:"{d.formTitle}", dataIndex:"formTitle", maxLength:100})
		.addHiddenField({ name:"flightNumber", bind:"{d.flightNumber}", dataIndex:"flightNumber"})
		.addDisplayFieldDate({ name:"fuelingDate", bind:"{d.fuelingDate}", dataIndex:"fuelingDate", noEdit:true  })
		.addDisplayFieldText({ name:"location", bind:"{d.departureCode}", dataIndex:"departureCode", noEdit:true , maxLength:25})
		.addDisplayFieldText({ name:"aircraft", bind:"{d.aircraftRegistration}", dataIndex:"aircraftRegistration", noEdit:true , maxLength:10})
		.addDisplayFieldText({ name:"billStatus", bind:"{d.billStatus}", dataIndex:"billStatus", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"invoiceStaus", bind:"{d.invoiceStatus}", dataIndex:"invoiceStatus", noEdit:true , width:130, maxLength:32, labelWidth:130})
		.addDisplayFieldText({ name:"customerCode", bind:"{d.customerCode}", dataIndex:"customerCode", maxLength:32})
		.addDisplayFieldText({ name:"payableCost", bind:"{d.payableCostAndCurr}", dataIndex:"payableCostAndCurr", noEdit:true , maxLength:64})
		.addDisplayFieldText({ name:"billableCost", bind:"{d.billableCostAndCurr}", dataIndex:"billableCostAndCurr", noEdit:true , maxLength:64})
		.add({name:"row7", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("formTitle")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"p2",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"c0", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c5", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c6", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c7", width:300,  cls:"sone-title-panel", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi"])
		.addChildrenTo("p2", ["c0", "c1", "c2", "c3", "c4", "c5", "c6"])
		.addChildrenTo("titleAndKpi", ["c7", "p2"])
		.addChildrenTo("c0", ["fuelingDate"])
		.addChildrenTo("c1", ["location"])
		.addChildrenTo("c2", ["aircraft"])
		.addChildrenTo("c3", ["billStatus"])
		.addChildrenTo("c4", ["invoiceStaus"])
		.addChildrenTo("c5", ["payableCost"])
		.addChildrenTo("c6", ["billableCost"])
		.addChildrenTo("c7", ["row7"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
						
	},
	
	_afterApplyStates_: function() {
		
		
						var flightNumber = this._controller_.getRecord().get("flightNumber");
						var customerCode = this._controller_.getRecord().get("customerCode");
						var formTitle = this._get_("formTitle");
						var originalTitleLabel = formTitle.fieldLabel;
						formTitle.labelEl.update(originalTitleLabel+" "+customerCode+" "+flightNumber);				
	}
});

/* ================= EDIT FORM: EditRemark ================= */

Ext.define("atraxo.acc.ui.extjs.dc.FuelEvent_Dc$EditRemark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_FuelEvent_Dc$EditRemark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remark}", paramIndex:"remark", width:400, labelAlign:"top", fieldStyle:"margin-top: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:440})
		.addPanel({ name:"col1", width:435, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
	}
});
