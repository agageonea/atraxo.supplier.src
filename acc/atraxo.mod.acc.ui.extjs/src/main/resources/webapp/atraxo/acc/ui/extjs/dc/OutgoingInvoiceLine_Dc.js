/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceLine_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.acc.ui.extjs.ds.OutgoingInvoiceLine_DsParam,
	recordModel: atraxo.acc.ui.extjs.ds.OutgoingInvoiceLine_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceLine_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.acc_OutgoingInvoiceLine_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"deliveryDate", dataIndex:"deliveryDate"})
			.addLov({name:"shipTo", dataIndex:"shipToCode", width:120, maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "custId"} ]}})
			.addTextField({ name:"flightID", dataIndex:"flightID", maxLength:64})
			.addCombo({ xtype:"combo", name:"flightType", dataIndex:"flightType", store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_]})
			.addNumberField({name:"quantity", dataIndex:"quantity", sysDec:"dec_unit", maxLength:19})
			.addNumberField({name:"amount", dataIndex:"amount", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"vat", dataIndex:"vat", sysDec:"dec_crncy", maxLength:19})
			.addCombo({ xtype:"combo", name:"transmissionStatus", dataIndex:"transmissionStatus", store:[ __ACC_TYPE__.InvoiceLineTransmissionStatus._NEW_, __ACC_TYPE__.InvoiceLineTransmissionStatus._IN_PROGRESS_, __ACC_TYPE__.InvoiceLineTransmissionStatus._FAILED_, __ACC_TYPE__.InvoiceLineTransmissionStatus._TRANSMITTED_]})
			.addTextField({ name:"erpReference", dataIndex:"erpReference", maxLength:32})
			.addTextField({ name:"bolNumber", dataIndex:"bolNumber", maxLength:25})
		;
	}

});

/* ================= GRID: MemoList ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceLine_Dc$MemoList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_OutgoingInvoiceLine_Dc$MemoList",
	_noExport_: true,
	_noPaginator_: true,

	/**
	 * 
	 */
	initComponent : function(config) {
		Ext.apply(this, {
			features: [{
				ftype: 'summary',
				remoteRoot: 'summaries',
				dock: 'bottom'
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"deliveryDate", dataIndex:"deliveryDate", _mask_: Masks.DATETIME,  flex:1})
		.addTextColumn({ name:"customer", dataIndex:"custCode", width:50,  flex:1})
		.addTextColumn({ name:"ticketNo", dataIndex:"ticketNo", width:50,  flex:1})
		.addTextColumn({ name:"aircraftRegistration", dataIndex:"aircraftRegistrationNumber", width:50,  flex:1})
		.addTextColumn({ name:"flightNo", dataIndex:"flightNo", width:50,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: AddList ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceLine_Dc$AddList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.acc_OutgoingInvoiceLine_Dc$AddList",
	_noPaginator_: true,

	/**
	 * 
	 */
	initComponent : function(config) {
		Ext.apply(this, {
			features: [{
				ftype: 'summary',
				remoteRoot: 'summaries',
				dock: 'bottom'
			}]
		});
		this.callParent(arguments);
	},

	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"deliveryDate", dataIndex:"deliveryDate", _mask_: Masks.DATETIME,  flex:1})
		.addTextColumn({ name:"customer", dataIndex:"custCode", width:50,  flex:1})
		.addTextColumn({ name:"ticketNo", dataIndex:"ticketNo", width:50,  flex:1})
		.addTextColumn({ name:"aircraftRegistration", dataIndex:"aircraftRegistrationNumber", width:50,  flex:1})
		.addTextColumn({ name:"flightNo", dataIndex:"flightNo", width:50,  flex:1})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceLine_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.acc_OutgoingInvoiceLine_Dc$EditList",

	/**
	 * 
	 */
	initComponent : function(config) {
		Ext.apply(this, {
			features: [{
				ftype: 'summary',
				remoteRoot: 'summaries',
				dock: 'bottom'
			}]
		});
		this.callParent(arguments);
	},
	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addDateColumn({name:"deliveryDate", dataIndex:"deliveryDate", noEdit: true, allowBlank: false, _mask_: Masks.DATETIME,  flex:1 })
		.addTextColumn({name:"shipTo", dataIndex:"shipToCode", width:50, noEdit: true, allowBlank: false, maxLength:32,  flex:1})
		.addTextColumn({name:"shipToName", dataIndex:"shipToName", width:120, noEdit: true, allowBlank: false, maxLength:100,  flex:1})
		.addTextColumn({name:"aircraftRegistration", dataIndex:"aircraftRegistrationNumber", width:50, noEdit: true, allowBlank: false, maxLength:10,  flex:1})
		.addTextColumn({name:"flightID", dataIndex:"flightID", width:120, noEdit: true, allowBlank: false, maxLength:64,  flex:1})
		.addComboColumn({name:"suffix", dataIndex:"flightSuffix", width:70, noEdit: true,  flex:1})
		.addComboColumn({name:"flightType", dataIndex:"flightType", width:70, noEdit: true, allowBlank: false,  flex:1})
		.addTextColumn({name:"ticketNo", dataIndex:"ticketNo", width:50, noEdit: true, allowBlank: false, maxLength:32,  flex:1})
		.addNumberColumn({name:"quantity", dataIndex:"quantity", noEdit: true, allowBlank: false, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:19, align:"right",  flex:1 })
		.addNumberColumn({name:"amount", dataIndex:"amount", noEdit: true, allowBlank: false, sysDec:"dec_crncy",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_crncy']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:19, align:"right",  flex:1 })
		.addNumberColumn({name:"vatAmount", dataIndex:"vat", noEdit: true, sysDec:"dec_crncy",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_crncy']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:19, align:"right",  flex:1 })
		.addComboColumn({name:"transmissionStatus", dataIndex:"transmissionStatus", hidden:true, width:70, noEdit: true,  flex:1})
		.addTextColumn({name:"erpReference", dataIndex:"erpReference", hidden:true, width:50, noEdit: true, maxLength:32})
		.addTextColumn({name:"bolNumber", dataIndex:"bolNumber", width:50, _enableFn_: function(dc, rec, col, fld) { return dc.record.data.invoicetype === __ACC_TYPE__.InvoiceTypeAcc._INV_ && dc.record.data.outInvStatus === __OPS_TYPE__.OutgoingInvoiceStatus._AWAITING_APPROVAL_; } , maxLength:25,  flex:1, 
			editor: { xtype:"textfield", maxLength:25}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_endDefine_: function() {
		
						this.plugins = [].concat(this.plugins?this.plugins:[],[
							{
								ptype: "gridfilters"
							},{
								ptype: "rowexpanderplus",
								rowBodyTpl : new Ext.XTemplate(
									"<div id='rec-{id}'></div>"
								)
							}
						]);
						this.selType = "rowmodel"
	},
	
	_afterInitComponent_: function() {
		
		
						this._buildChildGrid_({
							childDc: "outgoingInvoiceLineDetails",
							gridCls: atraxo.acc.ui.extjs.dc.OutgoingInvoiceLineDetails_Dc$List,
							filterField: "id",
							applyFilter : false
						});	
		
	}
});

/* ================= EDIT FORM: creditMemoReason ================= */

Ext.define("atraxo.acc.ui.extjs.dc.OutgoingInvoiceLine_Dc$creditMemoReason", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_OutgoingInvoiceLine_Dc$creditMemoReason",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"creditMemoReason", bind:"{p.creditMemoReason}", paramIndex:"creditMemoReason", allowBlank:false, width:280, noLabel: true, store:[],listeners:{
			change:{scope:this, fn:this._enableSearch_}
		}})
		.addDisplayFieldText({ name:"creditMemoReasonLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"table", layout: {type:"table"}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table"])
		.addChildrenTo("table", ["creditMemoReasonLabel", "creditMemoReason"]);
	},
	/* ==================== Business functions ==================== */
	
	_enableSearch_: function(el,newVal) {
		
		
						var dc  = this._controller_;
						var frame = dc.getFrame();
						var searchField = Ext.getCmp(frame._creditMemoSearchFieldId_);
						
						if (!Ext.isEmpty(newVal)) {					
							searchField.setReadOnly(false);
						} 
						else {
							searchField.setReadOnly(true);
							searchField.setValue("");
						}
	},
	
	_afterDefineElements_: function() {
		
						var storeObject = __OPS_TYPE__.RevocationReason;
						var storeArray = [];
		
						for(var key in storeObject) {
						    storeArray.push(storeObject[key]);
						}
		
						this._getBuilder_().change("creditMemoReason",{
							store: storeArray,
							editable: false
						});
						
	}
});
