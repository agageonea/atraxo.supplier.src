/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.dc.InvoiceLine_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.acc.ui.extjs.ds.InvoiceLine_DsParam,
	recordModel: atraxo.acc.ui.extjs.ds.InvoiceLine_Ds,
		
			/* ================ Business functions ================ */
	
	afterDoCancel: function() {
		
						var origQuantityBalance = this.params.get("origQuantityBalance");
						var origAmountBalance = this.params.get("origAmountBalance");
						this.params.set("quantityBalance", origQuantityBalance);
						this.params.set("amountBalance", origAmountBalance);
	}

});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.acc.ui.extjs.dc.InvoiceLine_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.acc_InvoiceLine_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"deliveryDate", dataIndex:"deliveryDate"})
			.addDateField({name:"eventDate", dataIndex:"eventDate"})
			.addLov({name:"airlineDesignator", dataIndex:"airlineDesignatorCode", width:120, maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "airlineDesignatorId"} ]}})
			.addTextField({ name:"aircraftRegistrationNo", dataIndex:"aircraftRegistrationNo", maxLength:10})
			.addTextField({ name:"flightNo", dataIndex:"flightNo", maxLength:4})
			.addCombo({ xtype:"combo", name:"suffix", dataIndex:"suffix", store:[ __OPS_TYPE__.Suffix._D_DIVERTED_, __OPS_TYPE__.Suffix._F_FERRY_, __OPS_TYPE__.Suffix._G_GROUND_RETURN_]})
			.addCombo({ xtype:"combo", name:"flightType", dataIndex:"flightType", store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_]})
			.addTextField({ name:"ticketNo", dataIndex:"ticketNo", maxLength:32})
			.addNumberField({name:"quantity", dataIndex:"quantity", sysDec:"dec_unit", maxLength:19})
			.addNumberField({name:"amount", dataIndex:"amount", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"vat", dataIndex:"vat", sysDec:"dec_crncy", maxLength:19})
			.addCombo({ xtype:"combo", name:"overAllCheckStatus", dataIndex:"overAllCheckStatus", store:[ __ACC_TYPE__.CheckStatus._OK_, __ACC_TYPE__.CheckStatus._FAILED_, __ACC_TYPE__.CheckStatus._NOT_CHECKED_, __ACC_TYPE__.CheckStatus._NEEDS_RECHECK_]})
		;
	}

});

/* ================= EDIT FORM: Details ================= */

Ext.define("atraxo.acc.ui.extjs.dc.InvoiceLine_Dc$Details", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.acc_InvoiceLine_Dc$Details",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"deliveryDate", bind:"{d.deliveryDate}", dataIndex:"deliveryDate", labelWidth:180})
		.addDateField({name:"eventDate", bind:"{d.eventDate}", dataIndex:"eventDate", labelWidth:180})
		.addLov({name:"airlineDesignator", bind:"{d.airlineDesignatorCode}", dataIndex:"airlineDesignatorCode", xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32, labelWidth:180,
			retFieldMapping: [{lovField:"id", dsField: "airlineDesignatorId"} ]})
		.addTextField({ name:"aircraftRegistrationNo", bind:"{d.aircraftRegistrationNo}", dataIndex:"aircraftRegistrationNo", maxLength:10, labelWidth:180})
		.addTextField({ name:"flightNo", bind:"{d.flightNo}", dataIndex:"flightNo", maxLength:4, labelWidth:180, minLength:0, listeners:{keydown: { scope: this, fn: function(field, e) { this._restrictNumeric_(field, e) }}}, enableKeyEvents:true})
		.addCombo({ xtype:"combo", name:"suffix", bind:"{d.suffix}", dataIndex:"suffix", store:[ __OPS_TYPE__.Suffix._D_DIVERTED_, __OPS_TYPE__.Suffix._F_FERRY_, __OPS_TYPE__.Suffix._G_GROUND_RETURN_], labelWidth:180})
		.addCombo({ xtype:"combo", name:"flightType", bind:"{d.flightType}", dataIndex:"flightType", store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:180})
		.addTextField({ name:"ticketNo", bind:"{d.ticketNo}", dataIndex:"ticketNo", maxLength:32, labelWidth:180})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", sysDec:"dec_unit", maxLength:19, labelWidth:180})
		.addNumberField({name:"amount", bind:"{d.amount}", dataIndex:"amount", sysDec:"dec_crncy", maxLength:19, labelWidth:180})
		.addNumberField({name:"vat", bind:"{d.vat}", dataIndex:"vat", sysDec:"dec_crncy", maxLength:19, labelWidth:180})
		.addCombo({ xtype:"combo", name:"overAllCheckStatus", bind:"{d.overAllCheckStatus}", dataIndex:"overAllCheckStatus", store:[ __ACC_TYPE__.CheckStatus._OK_, __ACC_TYPE__.CheckStatus._FAILED_, __ACC_TYPE__.CheckStatus._NOT_CHECKED_, __ACC_TYPE__.CheckStatus._NEEDS_RECHECK_], labelWidth:180})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["deliveryDate", "eventDate", "airlineDesignator", "aircraftRegistrationNo", "flightNo", "suffix", "flightType", "ticketNo", "quantity", "amount", "vat", "overAllCheckStatus"]);
	},
	/* ==================== Business functions ==================== */
	
	_restrictNumeric_: function(field,e) {
		
						var code = e.browserEvent.keyCode;
						var keyCodes = [8,9,39,37,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105];
				        if (keyCodes.indexOf(code) < 0) {
				            e.stopEvent();
				        }
	}
});

/* ================= EDIT-GRID: EditList ================= */

Ext.define("atraxo.acc.ui.extjs.dc.InvoiceLine_Dc$EditList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.acc_InvoiceLine_Dc$EditList",

	/**
	 * 
	 */
	initComponent : function(config) {
		Ext.apply(this, {
			features: [{
				ftype: 'summary',
				remoteRoot: 'summaries',
				dock: 'bottom'
			}]
		});
		this.callParent(arguments);
	},
	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addDateColumn({name:"deliveryDate", dataIndex:"deliveryDate", width:100, allowBlank: false, _mask_: Masks.DATETIME,listeners:{
			blur:{scope:this, fn:this._setContractPriceAndVat_}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
		} })
		.addDateColumn({name:"eventDate", dataIndex:"eventDate", width:100, allowBlank: false, _mask_: Masks.DATE })
		.addLov({name:"airlineDesignator", dataIndex:"airlineDesignatorCode", width:120, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CustomerCodeLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "airlineDesignatorId"} ]}})
		.addTextColumn({name:"aircraftRegistration", dataIndex:"aircraftRegistrationNo", width:120, allowBlank: false, maxLength:10, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:10}})
		.addTextColumn({name:"flightNo", dataIndex:"flightNo", width:100, allowBlank: false, maxLength:4, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:4}})
		.addComboColumn({name:"suffix", dataIndex:"suffix", width:100, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __OPS_TYPE__.Suffix._D_DIVERTED_, __OPS_TYPE__.Suffix._F_FERRY_, __OPS_TYPE__.Suffix._G_GROUND_RETURN_]}})
		.addComboColumn({name:"flightType", dataIndex:"flightType", width:100, allowBlank: false, 
			editor:{xtype:"combo", mode: 'local', allowBlank:false,listeners:{
				fpchange:{scope:this, fn:this._setContractPriceAndVat_}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
			}, triggerAction:'all', forceSelection:true, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_]}})
		.addTextColumn({name:"ticketNo", dataIndex:"ticketNo", width:100, allowBlank: false, maxLength:32, 
			editor: { xtype:"textfield", allowBlank:false, maxLength:32}})
		.addNumberColumn({name:"quantity", dataIndex:"quantity", width:100, allowBlank: false, sysDec:"dec_unit",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_unit']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:19, align:"right",listeners:{
			change:{scope:this, fn:this.onChangeQuantity}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
		} })
		.addNumberColumn({name:"amount", dataIndex:"amount", width:100, allowBlank: false, sysDec:"dec_crncy",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_crncy']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:19, align:"right",listeners:{
			change:{scope:this, fn:this.onChangeAmount}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
		} })
		.addNumberColumn({name:"vatAmount", dataIndex:"vat", width:120, sysDec:"dec_crncy",summaryRenderer: 
					function(value, summaryData, dataIndex) {var fmt=function(){ var _fmtMsk_ = '0.000000'; var d=_SYSTEMPARAMETERS_['dec_crncy']; if(Ext.isNumeric(d)){_fmtMsk_=Main.getNumberFormat(1*d);} return Ext.util.Format.number(value,_fmtMsk_); }; return "<b>"+" "+fmt()+"</b>"; }, maxLength:19, align:"right",listeners:{
			change:{scope:this, fn:this.onChangeVat}, buffer: Main.viewConfig.GRID_EDITOR_CHANGE_EVT_BUFFER
		} })
		.addTextColumn({name:"overAllCheckStatus", dataIndex:"overAllCheckStatus", width:100, noEdit: true, maxLength:32,  renderer:function(val) { return this._setCheckStatusColor_(val); }})
		.addNumberColumn({name:"invoiceLine", dataIndex:"invoiceLine", hidden:true, maxLength:11, align:"right" })
		.addTextColumn({name:"unit", dataIndex:"unitCode", hidden:true, width:50, noEdit: true, maxLength:2})
		.addNumberColumn({name:"netQuantity", dataIndex:"netQuantity", hidden:true, noEdit: true, sysDec:"dec_unit", maxLength:19, align:"right" })
		.addTextColumn({name:"netQtyUnitCode", dataIndex:"netQtyUnitCode", hidden:true, width:50, noEdit: true, maxLength:2})
		.addNumberColumn({name:"grossQuantity", dataIndex:"grossQuantity", hidden:true, noEdit: true, maxLength:19, decimals:6, align:"right" })
		.addTextColumn({name:"grossQtyUnitCode", dataIndex:"grossQtyUnitCode", hidden:true, width:50, noEdit: true, maxLength:2})
		.addNumberColumn({name:"calculatedVat", dataIndex:"calculatedVat", hidden:true, noEdit: true, sysDec:"dec_unit", maxLength:19, align:"right" })
		.addNumberColumn({name:"invoicedVat", dataIndex:"invoicedVat", hidden:true, noEdit: true, sysDec:"dec_crncy", maxLength:19, align:"right" })
		.addTextColumn({name:"contractCode", dataIndex:"contractCode", hidden:true, width:50, noEdit: true, maxLength:32})
		.addTextColumn({name:"taxType", dataIndex:"taxType", hidden:true, width:70, noEdit: true, maxLength:32})
		.addTextColumn({name:"qtyCheckStatus", dataIndex:"quantityCheckStatus", hidden:true, width:70, noEdit: true, maxLength:32})
		.addTextColumn({name:"eventCheckStatus", dataIndex:"eventCheckStatus", hidden:true, width:70, noEdit: true, maxLength:32})
		.addTextColumn({name:"vatCheckStatus", dataIndex:"vatCheckStatus", hidden:true, width:70, noEdit: true, maxLength:32})
		.addTextColumn({name:"origVatCheckStatus", dataIndex:"originalVatCheckStatus", hidden:true, width:70, noEdit: true, maxLength:32})
		.addTextColumn({name:"flighString", dataIndex:"flightString", hidden:true, width:50, noEdit: true, maxLength:32})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		.addDisplayFieldNumber({ name:"quantityBalance", bind:"{p.quantityBalance}", paramIndex:"quantityBalance", maxLength:19, listeners:{change: {scope: this, fn:function(el) {this._setBackgroundColor_(el); this._controller_.getFrame()._applyStateAllButtons_(); this._controller_.getFrame()._enableDisableFinishInput_();}}}, decimals:2 })
		.addDisplayFieldNumber({ name:"amountBalance", bind:"{p.amountBalance}", paramIndex:"amountBalance", maxLength:19, listeners:{change: {scope: this, fn:function(el) {this._setBackgroundColor_(el); this._controller_.getFrame()._applyStateAllButtons_(); this._controller_.getFrame()._enableDisableFinishInput_();}}}, decimals:2 })
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_setContractPriceAndVat_: function() {
		
		
						var f = function(){
							var dc = this._controller_;
							var r = dc.getRecord();
							if (r) {
								var o= {
									name: "setContractPriceAndVat",
									modal:false
								};
				
								dc.doRpcData(o);
							}
						};
						Ext.defer(f, 100, this);
	},
	
	_setCheckStatusColor_: function(val) {
		
		//				if(!val){
		//					return;
		//				}
						var icon = "";
						if (val === "OK") {
							icon = "<div style='width:100%; text-align:center'><i class='fa fa-circle' style='color:#52AA39'></i></div>";
						}else if(val === "Not checked"){
							icon = "<div style='width:100%; text-align:center'><i class='fa fa-circle' style='color:#FFFFFF'></i></div>";
						}else if(val === "Failed"){
							icon = "<div style='width:100%; text-align:center'><i class='fa fa-circle' style='color:#F94F52'></i></div>";
						}else if(val === "Needs recheck"){
							icon = "<div style='width:100%; text-align:center'><i class='fa fa-circle' style='color:#FFFF00'></i></div>";
						}
						else {
							icon = val;
						}
						return icon;
	},
	
	_setBackgroundColor_: function(el) {
		
		
						var value = el.getValue();
						var redStyles = {background: "#F94F52", color: "#FFFFFF", opacity: 1, border: "1px solid #F94F52"};
						var greenStyles = {background: "#52AA39", color: "#FFFFFF", opacity: 1, border: "1px solid #52AA39" };
		
						if (!Ext.isEmpty(value)) {
							if (value === 0) {
								el.inputEl.setStyle(greenStyles);
							}
							else {
								el.inputEl.setStyle(redStyles);
							}
						}				
	},
	
	_endDefine_: function() {
		
						this._delayedTask_ = new Ext.util.DelayedTask();
	},
	
	_setAmountAndSummary_: function(field,value) {
			
						this._controller_.fireEvent("calculateAmount", this, value);
	},
	
	_setSummary_: function(field) {
			
						this._controller_.fireEvent("updateSummary", this, field);
	},
	
	onChangeAmount: function(field,newValue,oldValue) {
		
		
						var dc = this._controller_;
						var r = dc.getRecord();
						var frame = dc.getFrame();
						var newAmountVal = newValue;
		
						if (r) {
		
							var amount = r.get("amount");
							if (Ext.isNumber(amount)) {
								newAmountVal = newAmountVal - amount;
							}
		
						}
		
			    	    if(!Ext.isNumeric(oldValue)) {
							oldValue = 0;
						}
			    	    if(!Ext.isNumeric(newValue)) {
							newValue = 0;
						}
						var amountBalance = this._controller_.params.get("amountBalance");
						amountBalance = amountBalance - parseFloat(oldValue.toFixed(2)) + parseFloat(newValue.toFixed(2));
						this._controller_.params.set("amountBalance", amountBalance);
		
						frame._updateAmountSummary_(this, field, false, newAmountVal);
	},
	
	onChangeVat: function(field,newValue) {
		
		
						var dc = this._controller_;
						var r = dc.getRecord();
						var frame = dc.getFrame();
						var newVatVal = newValue;
		
						if (r) {
		
							var vat = r.get("vat");
							if (Ext.isNumber(vat)) {
								newVatVal = newVatVal - vat;
							}
		
						}
		
						frame._updateVatSummary_(this, field, false, newVatVal);
	},
	
	onChangeQuantity: function(field,newValue,oldValue) {
		
			    	    if(!Ext.isNumeric(oldValue)) {
							oldValue = 0;
						}
			    	    if(!Ext.isNumeric(newValue)) {
							newValue = 0;
						}
						var quantityBalance = this._controller_.params.get("quantityBalance");
						quantityBalance = quantityBalance - parseFloat(oldValue.toFixed(2)) + parseFloat(newValue.toFixed(2));
						this._controller_.params.set("quantityBalance", quantityBalance);
		
						// ###################################################################################
		
						var dc = this._controller_;
						var r = dc.getRecord();
						var frame = dc.getFrame();
						var qty = newValue;
		
						if (r) {
		
							var quantity = r.get("quantity");
							if (Ext.isNumber(quantity)) {
								qty = qty - quantity;
							}
							
							var contractPrice = r.get("contractPrice");
							var vatRate = r.get("vatRate");
		
							var currentAmount = r.get("amount");
							var amountBalance = dc.getParamValue("amountBalance");
							quantity = newValue;
							var newAmount = quantity*contractPrice;
		
							r.beginEdit();
							r.set("amount", newAmount);
							r.set("vat", quantity*vatRate);
							r.set("calculatedVat", quantity*vatRate);
		
							if (!Ext.isEmpty(currentAmount)) {
								amountBalance = amountBalance - currentAmount;
							}
		
							dc.setParamValue("amountBalance",amountBalance+newAmount)
		
							r.endEdit();
						}
		
						frame._updateSummary_(this, field, false, qty);
		
						// ###################################################################################
	}
});
