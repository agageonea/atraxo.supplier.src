/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.InvoiceNumberSerie_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_InvoiceNumberSerie_Ds"
	},
	
	
	validators: {
		startIndex: [{type: 'presence'}],
		endIndex: [{type: 'presence'}],
		enabled: [{type: 'presence'}],
		restart: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("type", "Invoice");
	},
	
	fields: [
		{name:"prefix", type:"string"},
		{name:"startIndex", type:"int", allowNull:true},
		{name:"endIndex", type:"int", allowNull:true},
		{name:"currentIndex", type:"int", allowNull:true},
		{name:"enabled", type:"boolean"},
		{name:"restart", type:"boolean"},
		{name:"type", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.InvoiceNumberSerie_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"prefix", type:"string"},
		{name:"startIndex", type:"int", allowNull:true},
		{name:"endIndex", type:"int", allowNull:true},
		{name:"currentIndex", type:"int", allowNull:true},
		{name:"enabled", type:"boolean", allowNull:true},
		{name:"restart", type:"boolean", allowNull:true},
		{name:"type", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
