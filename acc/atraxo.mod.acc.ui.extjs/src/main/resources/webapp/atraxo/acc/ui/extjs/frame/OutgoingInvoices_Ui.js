/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.acc.ui.extjs.frame.OutgoingInvoices_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.OutgoingInvoices_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("invoice", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoice_Dc,{}))
		.addDc("creditMemo", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceMemo_Dc,{}))
		.addDc("fuelEvent", Ext.create(atraxo.acc.ui.extjs.dc.FuelEvent_Dc,{}))
		.addDc("fuelEventFilter", Ext.create(atraxo.acc.ui.extjs.dc.FuelEvent_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("outgoingInvoiceLineDetails", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceLineDetails_Dc,{}))
		.addDc("invoiceLine", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceLine_Dc,{multiEdit: true}))
		.addDc("invoiceLineMemo", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceLine_Dc,{multiEdit: true}))
		.addDc("refInvoiceLine", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingRefInvoiceLine_Dc,{multiEdit: true}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("attachmentPop", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("outgoingInvoiceBulkExportWizard", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportWizard_Dc,{}))
		.addDc("outgoingInvoiceBulkExportRicohWizard", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportRicohWizard_Dc,{}))
		.addDc("outgoingInvoiceBulkExportWizardInfiniteListLabelTop", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportWizardInfiniteList_Dc,{}))
		.addDc("outgoingInvoiceBulkExportRicohWizardInfiniteListLabelTop", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportRicohWizardInfiniteList_Dc,{}))
		.addDc("outgoingInvoiceBulkExportWizardInfiniteList", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportWizardInfiniteList_Dc,{ _disableAutoSelection_:true}))
		.addDc("outgoingInvoiceBulkExportRicohWizardInfiniteList", Ext.create(atraxo.acc.ui.extjs.dc.OutgoingInvoiceBulkExportRicohWizardInfiniteList_Dc,{ _disableAutoSelection_:true}))
		.linkDc("attachment", "invoice",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("outgoingInvoiceLineDetails", "invoiceLine",{fields:[
					{childField:"outgoingInvoiceLineId", parentField:"invLineId"}]})
				.linkDc("invoiceLine", "invoice",{fetchMode:"auto",fields:[
					{childField:"outInvId", parentField:"id"}]})
				.linkDc("invoiceLineMemo", "creditMemo",{fetchMode:"auto",fields:[
					{childField:"outInvId", parentField:"id"}]})
				.linkDc("refInvoiceLine", "invoice",{fetchMode:"auto",fields:[
					{childField:"outInvId", parentField:"invoiceRefId"}, {childField:"isCredited", value:"false"}]})
				.linkDc("history", "invoice",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "invoice",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("attachmentPop", "invoice",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"categType", value:"upload"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdwEdit",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwEdit, scope:this})
		.addButton({name:"btnNewWithMenu",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, isBtnContainer:true, scope:this})
		.addButton({name:"btnNewCreditNote",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", handler: this.onBtnNewCreditNote, scope:this})
		.addButton({name:"btnNewInvoice",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", _isDefaultHandler_:true, handler: this.onBtnNewInvoice, scope:this})
		.addButton({name:"btnGenerate",glyph:fp_asc.commit_glyph.glyph,iconCls: fp_asc.commit_glyph.css, disabled:false,  _btnContainerName_:"btnNewWithMenu", _isDefaultHandler_:true, handler: this.onBtnGenerate, scope:this})
		.addButton({name:"btnDeleteSalesInvoice",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteSalesInvoice,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (dc.record.data.status != __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_ && dc.record.data.status != __CMM_TYPE__.BillStatus._PAID_);} }], scope:this})
		.addButton({name:"btnCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancel, scope:this})
		.addButton({name:"btnRemove",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRemove,stateManager:[{ name:"selected_not_zero", dc:"invoiceLine", and: function() {return (this._canClick_());} }], scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnApproveSingleRegular",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveSingleRegular,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (this.canApproveSingle(dc));} }], scope:this})
		.addButton({name:"btnApproveListRegular",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveListRegular,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canApproveList(dc));} }], scope:this})
		.addButton({name:"btnPaid",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPaid,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (dc.record.data.status ==  __TYPES__.invoices.status.awaitingPayment);} }], scope:this})
		.addButton({name:"btnPaidList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnPaidList,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canPaid(dc));} }], scope:this})
		.addButton({name:"btnAddFeulEv",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true,  hidden:true, handler: this.onBtnAddFeulEv,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function() {return (this._canClick_());} }], scope:this})
		.addButton({name:"btnInvAddFeulEv",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true,  hidden:true, handler: this.onBtnInvAddFeulEv,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function() {return (this._canClick_());} }], scope:this})
		.addButton({name:"btnExportWithMenu",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canResend(dc));} }], scope:this})
		.addButton({name:"btnExportSelected",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:true,  _btnContainerName_:"btnExportWithMenu", _isDefaultHandler_:true, handler: this.onBtnExportSelected,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canResend(dc));} }], scope:this})
		.addButton({name:"btnExportFailed",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css, disabled:false,  _btnContainerName_:"btnExportWithMenu", handler: this.onBtnExportFailed, scope:this})
		.addButton({name:"btnSubmitForApprovalList",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalList,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflow() })
		.addButton({name:"btnSubmitForApproval",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApproval,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflow() })
		.addButton({name:"btnSaveCloseApprovalWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApprovalWdw, scope:this})
		.addButton({name:"btnCancelApprovalWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApprovalWdw, scope:this})
		.addButton({name:"btnApproveList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveList,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: this.useWorkflow() })
		.addButton({name:"btnApproveEdit",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApproveEdit,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: this.useWorkflow() })
		.addButton({name:"btnRejectList",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRejectList,stateManager:[{ name:"selected_not_zero", dc:"invoice", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: this.useWorkflow() })
		.addButton({name:"btnRejectEdit",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnRejectEdit,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: this.useWorkflow() })
		.addButton({name:"btnSaveCloseApproveWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApproveWdw, scope:this})
		.addButton({name:"btnSaveCloseRejectWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectWdw, scope:this})
		.addButton({name:"btnCancelApproveWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApproveWdw, scope:this})
		.addButton({name:"btnCancelRejectWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRejectWdw, scope:this})
		.addButton({name:"btnExportInvoice",glyph:fp_asc.export_glyph.glyph, disabled:true, handler: this.onBtnExportInvoice,stateManager:[{ name:"selected_one", dc:"invoice", and: function(dc) {return (dc.record.data.status == __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_ || dc.record.data.status == __CMM_TYPE__.BillStatus._PAID_);} }], scope:this})
		.addButton({name:"btnExportInvoicesMenu",glyph:fp_asc.export_glyph.glyph, disabled:false, isBtnContainer:true, scope:this})
		.addButton({name:"btnExportInvoiceList",glyph:fp_asc.export_glyph.glyph, disabled:true,  _btnContainerName_:"btnExportInvoicesMenu", _isDefaultHandler_:true, handler: this.onBtnExportInvoiceList,stateManager:[{ name:"selected_one", dc:"invoice", and: function(dc) {return (dc.record.data.status == __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_ || dc.record.data.status == __CMM_TYPE__.BillStatus._PAID_);} }], scope:this})
		.addButton({name:"btnExportInvoicesBulk",glyph:fp_asc.export_glyph.glyph, disabled:false,  _btnContainerName_:"btnExportInvoicesMenu", handler: this.onBtnExportInvoicesBulk, scope:this})
		.addButton({name:"btnOkExport",glyph:fp_asc.send_glyph.glyph,iconCls: fp_asc.send_glyph.css,  disabled:true, handler: this.onBtnOkExport, scope:this})
		.addButton({name:"btnCancelExport",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelExport, scope:this})
		.addButton({name:"btnContinueMemo",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnContinueMemo,stateManager:[{ name:"selected_one_clean", dc:"creditMemo", and: function() {return (this._hasReason_());} }], scope:this})
		.addButton({name:"btAddInvoiceLine",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true, handler: this.onBtAddInvoiceLine,stateManager:[{ name:"selected_not_zero", dc:"refInvoiceLine"}], scope:this})
		.addButton({name:"btnCancelInvoiceLine",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelInvoiceLine, scope:this})
		.addButton({name:"btnContinueMemo2",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css,  hidden:true, disabled:true, handler: this.onBtnContinueMemo2,stateManager:[{ name:"selected_not_zero", dc:"invoiceLineMemo"}], scope:this})
		.addButton({name:"btnBackMemo",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false,  hidden:true, handler: this.onBtnBackMemo, scope:this})
		.addButton({name:"btnCancelMemo",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelMemo, scope:this})
		.addButton({name:"btnSelectFuelEvent",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSelectFuelEvent,stateManager:[{ name:"selected_not_zero", dc:"fuelEvent"}], scope:this})
		.addButton({name:"btnCancelFuelEvent",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelFuelEvent, scope:this})
		.addButton({name:"btnGenerateFunction",glyph:fp_asc.next_glyph.glyph, disabled:false, handler: this.onBtnGenerateFunction, scope:this})
		.addButton({name:"btnApplyFuelEventFilter", disabled:false, handler: this.onBtnApplyFuelEventFilter, scope:this})
		.addButton({name:"btnResetFuelEventFilter", disabled:false,  glyph:"xf021@FontAwesome", listeners:{afterrender: {scope: this, fn: function(el) {this._hideText_(el)}}}, handler: this.onBtnResetFuelEventFilter, scope:this})
		.addButton({name:"btnContinueStep1",glyph:fp_asc.next_glyph.glyph, disabled:false, handler: this.onBtnContinueStep1,stateManager:[{ name:"dc_in_any_state", dc:"invoice"}], scope:this})
		.addButton({name:"btnContinueRicohStep1",glyph:fp_asc.next_glyph.glyph,  disabled:true, handler: this.onBtnContinueRicohStep1, scope:this})
		.addButton({name:"btnDiscardStep1",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnDiscardStep1, scope:this})
		.addButton({name:"btnDiscardRicohStep1",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnDiscardRicohStep1, scope:this})
		.addButton({name:"btnBackToConditions",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false, handler: this.onBtnBackToConditions, scope:this})
		.addButton({name:"btnBackToRicohConditions",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false, handler: this.onBtnBackToRicohConditions, scope:this})
		.addButton({name:"btnExportBulkRicohInvoices",glyph:fp_asc.export_glyph.glyph, disabled:true, handler: this.onBtnExportBulkRicohInvoices,stateManager:[{ name:"selected_not_zero", dc:"outgoingInvoiceBulkExportRicohWizardInfiniteList"}], scope:this})
		.addButton({name:"btnExportBulkInvoices",glyph:fp_asc.export_glyph.glyph, disabled:true, handler: this.onBtnExportBulkInvoices,stateManager:[{ name:"selected_not_zero", dc:"outgoingInvoiceBulkExportWizardInfiniteList"}], scope:this})
		.addButton({name:"btnNewAttach",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:true, handler: this._addNewAttachment_,stateManager:[{ name:"selected_one", dc:"invoice", and: function() {return (!this._inApprovalProcess_());} }], scope:this})
		.addButton({name:"btnDeleteAttach",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this._deleteAttachment_,stateManager:[{ name:"selected_not_zero", dc:"attachment", and: function() {return (!this._inApprovalProcess_());} }], scope:this})
		.addButton({name:"btnExportToRicoh",glyph:fp_asc.export_glyph.glyph, disabled:true, handler: this.onBtnExportToRicoh,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (this.canSendToRicoh(dc));} }], scope:this})
		.addButton({name:"btnExportToRicohMenu",glyph:fp_asc.export_glyph.glyph, disabled:false, isBtnContainer:true, scope:this})
		.addButton({name:"btnExportToRicohSelected",glyph:fp_asc.export_glyph.glyph, disabled:true,  _btnContainerName_:"btnExportToRicohMenu", _isDefaultHandler_:true, handler: this.onBtnExportToRicohSelected,stateManager:[{ name:"selected_one_clean", dc:"invoice", and: function(dc) {return (this.canSendToRicoh(dc));} }], scope:this})
		.addButton({name:"btnExportToRicohBulk",glyph:fp_asc.export_glyph.glyph, disabled:false,  _btnContainerName_:"btnExportToRicohMenu", handler: this.onBtnExportToRicohBulk, scope:this})
		.addDcFormView("invoice", {name:"InvoiceGenerate", xtype:"acc_OutgoingInvoice_Dc$Generate"})
		.addDcGridView("invoice", {name:"InvoiceList", xtype:"acc_OutgoingInvoice_Dc$InvoiceList"})
		.addDcFilterFormView("invoice", {name:"InvoiceFilter", xtype:"acc_OutgoingInvoice_Dc$Filter"})
		.addDcFormView("invoice", {name:"InvoiceHeader", xtype:"acc_OutgoingInvoice_Dc$InvoiceHeader"})
		.addDcFormView("invoice", {name:"InvoiceExportForm", xtype:"acc_OutgoingInvoice_Dc$Export"})
		.addDcGridView("creditMemo", {name:"creditMemoList", xtype:"acc_OutgoingInvoiceMemo_Dc$CreditMemoList",  split:false})
		.addDcGridView("creditMemo", {name:"creditMemoListWdw", xtype:"acc_OutgoingInvoiceMemo_Dc$CreditMemoList",  split:false})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"NotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcEditGridView("invoiceLine", {name:"InvoiceLineList", _hasTitle_:true, xtype:"acc_OutgoingInvoiceLine_Dc$EditList", frame:true, _summaryDefined_:true,  listeners:{afterrender: {scope: this, fn: function(el) {this._adjustSummary_(el)}}}})
		.addDcFilterFormView("invoiceLine", {name:"invoiceLineFilter", xtype:"acc_OutgoingInvoiceLine_Dc$Filter"})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addDcGridView("outgoingInvoiceLineDetails", {name:"outgoingInvoiceLineDetailsList", xtype:"acc_OutgoingInvoiceLineDetails_Dc$List"})
		.addDcGridView("invoiceLineMemo", {name:"outgoingInvoiceLineMemoList", xtype:"acc_OutgoingInvoiceLine_Dc$MemoList", _summaryDefined_:true,  _showViewFilterToolbar_:false, hidden:true, cls:"sone-grid-no-summary"})
		.addDcGridView("refInvoiceLine", {name:"outgoingInvoiceLineAddList", xtype:"acc_OutgoingRefInvoiceLine_Dc$AddList", _summaryDefined_:true,  _showViewFilterToolbar_:false})
		.addDcGridView("fuelEvent", {name:"fuelEventList", xtype:"acc_FuelEvent_Dc$ListOutgoingInvoiceLine",  _showViewFilterToolbar_:false})
		.addDcFormView("fuelEventFilter", {name:"fuelEventFilter", height:80, xtype:"acc_FuelEvent_Dc$FilterForm",  split:false})
		.addDcFormView("invoiceLine", {name:"creditMemoReason", xtype:"acc_OutgoingInvoiceLine_Dc$creditMemoReason",  _skipMaskWhenNoRecordInDc_:true})
		.addDcFormView("invoice", {name:"approvalNote", xtype:"acc_OutgoingInvoice_Dc$ApprovalNote",  split:false})
		.addDcGridView("attachmentPop", {name:"documentsAssignList", _hasTitle_:true, height:300, xtype:"ad_Attachment_Dc$attachmentAssignList",  cls:"sone-custom-panel-title"})
		.addDcFormView("invoice", {name:"approveNote", xtype:"acc_OutgoingInvoice_Dc$ApprovalNote"})
		.addDcFormView("invoice", {name:"rejectNote", xtype:"acc_OutgoingInvoice_Dc$ApprovalNote"})
		.addDcFormView("outgoingInvoiceBulkExportWizard", {name:"bulkExportHeader", height:110, xtype:"acc_OutgoingInvoiceBulkExportWizard_Dc$wizardHeader"})
		.addDcFormView("outgoingInvoiceBulkExportRicohWizard", {name:"bulkExportRicohHeader", height:110, xtype:"acc_OutgoingInvoiceBulkExportRicohWizard_Dc$wizardHeader"})
		.addDcFormView("outgoingInvoiceBulkExportWizard", {name:"bulkExportStep1", height:500, xtype:"acc_OutgoingInvoiceBulkExportWizard_Dc$bulkInvoiceExportStep1"})
		.addDcFormView("outgoingInvoiceBulkExportRicohWizard", {name:"bulkExportRicohStep1", height:500, xtype:"acc_OutgoingInvoiceBulkExportRicohWizard_Dc$bulkInvoiceExportStep1"})
		.addDcFormView("outgoingInvoiceBulkExportWizardInfiniteListLabelTop", {name:"bulkExportStep2Label", width:500, xtype:"acc_OutgoingInvoiceBulkExportWizardInfiniteList_Dc$bulkInvoiceExportStep2TopLabel",  split:false})
		.addDcFormView("outgoingInvoiceBulkExportRicohWizardInfiniteListLabelTop", {name:"bulkExportRicohStep2Label", width:500, xtype:"acc_OutgoingInvoiceBulkExportRicohWizardInfiniteList_Dc$bulkInvoiceExportRicohStep2TopLabel",  split:false})
		.addDcGridView("outgoingInvoiceBulkExportWizardInfiniteList", {name:"bulkExportStep2List", width:500, xtype:"acc_OutgoingInvoiceBulkExportWizardInfiniteList_Dc$bulkInvoiceExportStep2",  listeners:{afterrender: {scope: this, fn: function(grid) {this._handleSelect_(grid)}}}, selModel:"{mode: 'SINGLE'}"})
		.addDcGridView("outgoingInvoiceBulkExportRicohWizardInfiniteList", {name:"bulkExportRicohStep2List", width:500, xtype:"acc_OutgoingInvoiceBulkExportRicohWizardInfiniteList_Dc$bulkInvoiceExportRicohStep2",  listeners:{afterrender: {scope: this, fn: function(grid) {this._handleRicohSelect_(grid)}}}, selModel:"{mode: 'SINGLE'}"})
		.addPanel({name:"bulkExportStep2", width:500, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"bulkExportRicohStep2", width:500, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"selectFuelEventPanel", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"newCreditMemo", layout:"fit"})
		.addPanel({name:"panelAssignAttachments", layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwApprovalNote", _hasTitle_:true, width:600, height:530, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("panelAssignAttachments")],  closable:true, listeners:{ show:{fn:this.onApprovalWdwShow, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApprovalWdw"), this._elems_.get("btnCancelApprovalWdw")]}]})
		.addWindow({name:"wdwGenerate", _hasTitle_:true, width:410, height:325, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("InvoiceGenerate")],  listeners:{ close:{fn:this.onWdwInvoiceClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnGenerateFunction"), this._elems_.get("btnCancel")]}]})
		.addWindow({name:"wdwApproveNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approveNote")],  closable:true, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApproveWdw"), this._elems_.get("btnCancelApproveWdw")]}]})
		.addWindow({name:"wdwRejectNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("rejectNote")],  closable:true, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectWdw"), this._elems_.get("btnCancelRejectWdw")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NotesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"newCreditMemoWdw", _hasTitle_:true, width:620, height:450, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("creditMemoListWdw")],  listeners:{ close:{fn:this.onNewCreditMemoWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnBackMemo"), this._elems_.get("btnContinueMemo"), this._elems_.get("btnContinueMemo2"), this._elems_.get("btnCancelMemo")]}]})
		.addWindow({name:"addInvoiceLineWdw", _hasTitle_:true, width:620, height:350, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("outgoingInvoiceLineAddList")],  listeners:{ close:{fn:this.onExportWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btAddInvoiceLine"), this._elems_.get("btnCancelInvoiceLine")]}]})
		.addWindow({name:"addFuelEventWdw", title:"", width:1080, height:350, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("selectFuelEventPanel")],  listeners:{ close:{fn: function() {this._onCloseAddFuelEventWdw_()}, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSelectFuelEvent"), this._elems_.get("btnCancelFuelEvent")]}]})
		.addWindow({name:"exportInvoiceWdw", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("InvoiceExportForm")],  listeners:{ close:{fn:this.onAddInvoiceLineWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnOkExport"), this._elems_.get("btnCancelExport")]}]})
		.addWizardWindow({name:"wdwBulkExportWizard", _hasTitle_:true, width:600, height:500,_labelPanelName_:"bulkExportHeader"
			,_stepPanels_:["bulkExportStep1","bulkExportStep2"]
			,  listeners:{ show:{fn:this._resetExportWdw_, scope:this} , close:{fn:this._resetExportWdw_, scope:this}}, cls:"sone-bulk-export-window", 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnBackToConditions"), this._elems_.get("btnDiscardStep1"), this._elems_.get("btnContinueStep1"), this._elems_.get("btnExportBulkInvoices")]}]})
		.addWizardWindow({name:"wdwBulkExportRicohWizard", _hasTitle_:true, width:600, height:420,_labelPanelName_:"bulkExportRicohHeader"
			,_stepPanels_:["bulkExportRicohStep1","bulkExportRicohStep2"]
			,  listeners:{ show:{fn:this._resetExportRicohWdw_, scope:this} , close:{fn:this._resetExportRicohWdw_, scope:this}}, cls:"sone-bulk-export-window", 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnBackToRicohConditions"), this._elems_.get("btnDiscardRicohStep1"), this._elems_.get("btnContinueRicohStep1"), this._elems_.get("btnExportBulkRicohInvoices")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"invoice"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("bulkExportStep2", ["bulkExportStep2Label", "bulkExportStep2List"], ["north", "center"])
		.addChildrenTo("bulkExportRicohStep2", ["bulkExportRicohStep2Label", "bulkExportRicohStep2List"], ["north", "center"])
		.addChildrenTo("selectFuelEventPanel", ["fuelEventFilter", "fuelEventList"], ["north", "center"])
		.addChildrenTo("detailsTab", ["InvoiceLineList"])
		.addChildrenTo("newCreditMemo", ["creditMemoList"])
		.addChildrenTo("panelAssignAttachments", ["approvalNote", "documentsAssignList"], ["north", "center"])
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["InvoiceList"], ["center"])
		.addChildrenTo("canvas2", ["InvoiceHeader", "detailsTab"], ["north", "center"])
		.addToolbarTo("InvoiceList", "tlbInvoicetList")
		.addToolbarTo("InvoiceHeader", "tlbInvoiceEditt")
		.addToolbarTo("NotesList", "tlbNoteList")
		.addToolbarTo("InvoiceLineList", "tlbInvoiceLineList")
		.addToolbarTo("attachmentList", "tlbAttachments")
		.addToolbarTo("fuelEventList", "tlbFuelEventList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("detailsTab", ["NotesList", "attachmentList", "History"])
		.addChildrenTo2("main", ["canvas2"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbInvoicetList", {dc: "invoice"})
			.addButtons([this._elems_.get("btnNewWithMenu") ])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnDeleteSalesInvoice"),this._elems_.get("btnGenerate"),this._elems_.get("btnApproveListRegular"),this._elems_.get("btnPaidList"),this._elems_.get("btnExportWithMenu"),this._elems_.get("btnExportSelected"),this._elems_.get("btnExportFailed"),this._elems_.get("btnSubmitForApprovalList"),this._elems_.get("btnApproveList"),this._elems_.get("btnRejectList"),this._elems_.get("btnExportInvoicesMenu"),this._elems_.get("btnExportInvoiceList"),this._elems_.get("btnExportInvoicesBulk"),this._elems_.get("btnExportToRicohMenu"),this._elems_.get("btnExportToRicohSelected"),this._elems_.get("btnExportToRicohBulk"),this._elems_.get("btnNewCreditNote"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbInvoiceEditt", {dc: "invoice"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSaveInChild({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnApproveSingleRegular"),this._elems_.get("btnPaid"),this._elems_.get("btnSubmitForApproval"),this._elems_.get("btnApproveEdit"),this._elems_.get("btnRejectEdit"),this._elems_.get("btnExportInvoice"),this._elems_.get("btnExportToRicoh"),this._elems_.get("helpWdwEdit")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbInvoiceLineList", {dc: "invoiceLine"})
			.addButtons([])
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnInvAddFeulEv"),this._elems_.get("btnAddFeulEv"),this._elems_.get("btnRemove")])
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnNewAttach"),this._elems_.get("btnDeleteAttach")])
			.addReports()
		.end()
		.beginToolbar("tlbFuelEventList", {dc: "fuelEvent"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnApplyFuelEventFilter"),this._elems_.get("btnResetFuelEventFilter")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwEdit
	 */
	,onHelpWdwEdit: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnNewCreditNote
	 */
	,onBtnNewCreditNote: function() {
		this._newCreditNote_();
	}
	
	/**
	 * On-Click handler for button btnNewInvoice
	 */
	,onBtnNewInvoice: function() {
	}
	
	/**
	 * On-Click handler for button btnGenerate
	 */
	,onBtnGenerate: function() {
		this._getDc_("invoice").doNew();
		this.showGenerateWdw();
	}
	
	/**
	 * On-Click handler for button btnDeleteSalesInvoice
	 */
	,onBtnDeleteSalesInvoice: function() {
		this._deleteInvoice_();
	}
	
	/**
	 * On-Click handler for button btnCancel
	 */
	,onBtnCancel: function() {
		this.onWdwInvoiceClose();
		this._getWindow_("wdwGenerate").close();
	}
	
	/**
	 * On-Click handler for button btnRemove
	 */
	,onBtnRemove: function() {
		this._getDc_("invoiceLine").doDelete();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this._getDc_("note").doSave();
		this._getWindow_("noteWdw").close();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnApproveSingleRegular
	 */
	,onBtnApproveSingleRegular: function() {
		var successFn = function() {
			this._getDc_("invoice").doReloadRecord();
			this._getDc_("invoiceLine").doReloadPage();
			this._getDc_("note").doReloadPage();
			this._getDc_("history").doReloadPage();
			this._getDc_("attachment").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"approveRegular",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnApproveListRegular
	 */
	,onBtnApproveListRegular: function() {
		var successFn = function() {
			this._getDc_("invoice").doReloadRecord();
			this._getDc_("invoiceLine").doReloadPage();
			this._getDc_("note").doReloadPage();
			this._getDc_("history").doReloadPage();
			this._getDc_("attachment").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"approveRegularList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnPaid
	 */
	,onBtnPaid: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._getDc_("invoiceLine").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"pay",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnPaidList
	 */
	,onBtnPaidList: function() {
		var successFn = function() {
			this._getDc_("history").doReloadPage();
			this._getDc_("invoiceLine").doReloadPage();
			this._applyStateAllButtons_();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"payList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnAddFeulEv
	 */
	,onBtnAddFeulEv: function() {
		this._addInvoiceLine_()
	}
	
	/**
	 * On-Click handler for button btnInvAddFeulEv
	 */
	,onBtnInvAddFeulEv: function() {
		this._addFuelEvent_()
	}
	
	/**
	 * On-Click handler for button btnExportSelected
	 */
	,onBtnExportSelected: function() {
		this._showConfirmExportInvoicesToNAV('selected')
	}
	
	/**
	 * On-Click handler for button btnExportFailed
	 */
	,onBtnExportFailed: function() {
		this._showConfirmExportInvoicesToNAV('failed')
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalList
	 */
	,onBtnSubmitForApprovalList: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApproval
	 */
	,onBtnSubmitForApproval: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApprovalWdw
	 */
	,onBtnSaveCloseApprovalWdw: function() {
		this.submitForApprovalRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApprovalWdw
	 */
	,onBtnCancelApprovalWdw: function() {
		this._getWindow_("wdwApprovalNote").close();
	}
	
	/**
	 * On-Click handler for button btnApproveList
	 */
	,onBtnApproveList: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnApproveEdit
	 */
	,onBtnApproveEdit: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectList
	 */
	,onBtnRejectList: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectEdit
	 */
	,onBtnRejectEdit: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApproveWdw
	 */
	,onBtnSaveCloseApproveWdw: function() {
		var successFn = function(dc,response) {
			this._getWindow_("wdwApproveNote").close();
			this.initFeedback(response)
			this._getDc_("invoice").doReloadPage();
		};
		var o={
			name:"approveList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectWdw
	 */
	,onBtnSaveCloseRejectWdw: function() {
		var successFn = function(dc,response) {
			this._getWindow_("wdwRejectNote").close();
			this.initFeedback(response)
			this._getDc_("invoice").doReloadPage();
		};
		var o={
			name:"rejectList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("invoice").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnCancelApproveWdw
	 */
	,onBtnCancelApproveWdw: function() {
		this._getWindow_("wdwApproveNote").close();
		this._getDc_("invoice").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelRejectWdw
	 */
	,onBtnCancelRejectWdw: function() {
		this._getWindow_("wdwRejectNote").close();
		this._getDc_("invoice").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnExportInvoice
	 */
	,onBtnExportInvoice: function() {
		this._getWindow_("exportInvoiceWdw").show();
	}
	
	/**
	 * On-Click handler for button btnExportInvoiceList
	 */
	,onBtnExportInvoiceList: function() {
		this._getWindow_("exportInvoiceWdw").show();
	}
	
	/**
	 * On-Click handler for button btnExportInvoicesBulk
	 */
	,onBtnExportInvoicesBulk: function() {
		this._getWindow_("wdwBulkExportWizard").show();
		this._buttonsController_( 1 )
	}
	
	/**
	 * On-Click handler for button btnOkExport
	 */
	,onBtnOkExport: function() {
		this._exportSalesInvoice_();
		this._getWindow_("exportInvoiceWdw").close();
	}
	
	/**
	 * On-Click handler for button btnCancelExport
	 */
	,onBtnCancelExport: function() {
		this._getWindow_("exportInvoiceWdw").close();
		this._getDc_("invoice").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnContinueMemo
	 */
	,onBtnContinueMemo: function() {
		this._jumpToSecondStep_();
	}
	
	/**
	 * On-Click handler for button btAddInvoiceLine
	 */
	,onBtAddInvoiceLine: function() {
		this._saveInvoiceLine_()
	}
	
	/**
	 * On-Click handler for button btnCancelInvoiceLine
	 */
	,onBtnCancelInvoiceLine: function() {
		this._cancelAddInvoiceLine_(true)
	}
	
	/**
	 * On-Click handler for button btnContinueMemo2
	 */
	,onBtnContinueMemo2: function() {
		this._jumpToThirdStep_();
	}
	
	/**
	 * On-Click handler for button btnBackMemo
	 */
	,onBtnBackMemo: function() {
		this._cancelCreditMemo_()
	}
	
	/**
	 * On-Click handler for button btnCancelMemo
	 */
	,onBtnCancelMemo: function() {
		this._cancelCreditMemo_(true)
	}
	
	/**
	 * On-Click handler for button btnSelectFuelEvent
	 */
	,onBtnSelectFuelEvent: function() {
		this._addNewOutgoingInvoiceLine_();
	}
	
	/**
	 * On-Click handler for button btnCancelFuelEvent
	 */
	,onBtnCancelFuelEvent: function() {
		this._closeAddFuelEventWdw_();
	}
	
	/**
	 * On-Click handler for button btnGenerateFunction
	 */
	,onBtnGenerateFunction: function() {
		this._generateInvoice_();
	}
	
	/**
	 * On-Click handler for button btnApplyFuelEventFilter
	 */
	,onBtnApplyFuelEventFilter: function() {
		this._applyFuelEventFilter_();
	}
	
	/**
	 * On-Click handler for button btnResetFuelEventFilter
	 */
	,onBtnResetFuelEventFilter: function() {
		this._resetFuelEventFilter_();
	}
	
	/**
	 * On-Click handler for button btnContinueStep1
	 */
	,onBtnContinueStep1: function() {
		this._goToStep2_();
	}
	
	/**
	 * On-Click handler for button btnContinueRicohStep1
	 */
	,onBtnContinueRicohStep1: function() {
		this._goToRicohStep2_();
	}
	
	/**
	 * On-Click handler for button btnDiscardStep1
	 */
	,onBtnDiscardStep1: function() {
		this._getWindow_("wdwBulkExportWizard").close();
	}
	
	/**
	 * On-Click handler for button btnDiscardRicohStep1
	 */
	,onBtnDiscardRicohStep1: function() {
		this._getWindow_("wdwBulkExportRicohWizard").close();
	}
	
	/**
	 * On-Click handler for button btnBackToConditions
	 */
	,onBtnBackToConditions: function() {
		this._backToStep1_()
	}
	
	/**
	 * On-Click handler for button btnBackToRicohConditions
	 */
	,onBtnBackToRicohConditions: function() {
		this._backToRicohStep1_()
	}
	
	/**
	 * On-Click handler for button btnExportBulkRicohInvoices
	 */
	,onBtnExportBulkRicohInvoices: function() {
		this._standardFilterRicohResetHack_();
		var successFn = function(dc,response) {
			this.showResult(response);
			this._getWindow_("wdwBulkExportRicohWizard").close();
			this._getDc_("invoice").doReloadPage();
		};
		var failureFn = function(dc,response) {
			this._getWindow_("wdwBulkExportRicohWizard").close();
			Main.rpcFailure(response,true);
		};
		var o={
			name:"exportBulkToRicoh",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("outgoingInvoiceBulkExportRicohWizardInfiniteList").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnExportBulkInvoices
	 */
	,onBtnExportBulkInvoices: function() {
		this._standardFilterResetHack_();
		var successFn = function(dc,response) {
			this.showResult(response);
			this._getWindow_("wdwBulkExportWizard").close();
			this._getDc_("invoice").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response,true);
		};
		var o={
			name:"bulkExportInvoices",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("outgoingInvoiceBulkExportWizardInfiniteList").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnExportToRicoh
	 */
	,onBtnExportToRicoh: function() {
		this._exportSelectedToRicoh_();
	}
	
	/**
	 * On-Click handler for button btnExportToRicohSelected
	 */
	,onBtnExportToRicohSelected: function() {
		this._exportSelectedToRicoh_();
	}
	
	/**
	 * On-Click handler for button btnExportToRicohBulk
	 */
	,onBtnExportToRicohBulk: function() {
		this._getWindow_("wdwBulkExportRicohWizard").show();
		this._buttonsControllerRicoh_( 1 )
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,showGenerateWdw: function() {	
		this._getWindow_("wdwGenerate").show();
	}
	
	,generateReport: function() {	
		var o={
			name:"generateReport",
			modal:true
		};
		this._getDc_("invoice").doRpcData(o);
	}
	
	,_show_removed_: function(params) {
		
						
						var dc = this._getDc_("invoice");
						var af = [];
						Ext.each(params.invoices, function(param) {
							af.push({
								"id":null,
								"fieldName": "invoiceNo",
								"operation":"=",
								"value1": param.invoiceNumber,
								"groupOp":"OR1"
							});
						}, this);
						Ext.each(params.creditMemos, function(param) {
							af.push({
								"id":null,
								"fieldName": "invoiceNo",
								"operation":"=",
								"value1": param.invoiceNumber,
								"groupOp":"OR1"
							});
						}, this);
						dc.doCancel();
						dc.doClearAllFilters();
						dc.advancedFilter = null;
						dc.advancedFilter = af;
						dc.doQuery();
	}
	
	,_onCloseAddFuelEventWdw_: function() {
		
						var view = this._get_("fuelEventFilter");
						var dc = this._getDc_("fuelEvent");
		
						view.getForm().reset();
						dc.advancedFilter = null;
						
	}
	
	,_when_called_from_dshboard_: function(id) {
		
						var dc = this._getDc_("invoice");
						dc.doCancel();
						dc.doClearAllFilters();
						dc.setFilterValue("id", id);				
						dc.doQuery({showEdit: true}); 
	}
	
	,_canClick_: function() {
		
						var dc = this._getDc_("invoice");
						var r = dc.getRecord();
						var state = false;
						if (r) {
							var s = r.get("status");
							var approvalStatus = r.get("approvalStatus");
							var t = r.get("invoiceType");
							if (s != __TYPES__.invoices.status.awaitingPayment && s !=  __TYPES__.invoices.status.paid && approvalStatus != __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_) {
								state = true;
							}
						}
						return state;
	}
	
	,openSubmitForApprovalWindow: function() {
		
						var label = "Please provide a note for approval.";
						var title = "Submit for approval";
						this.setupSubmitForApprovalWindow("wdwApprovalNote","approvalNote","btnSaveCloseApprovalWdw","btnCancelApprovalWdw" , label , title);				
	}
	
	,setupSubmitForApprovalWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(true);
						
						remarksField.fieldLabel = label;
						remarksField.setValue("");
						window.title = title;
						
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
								cancelButton.setDisabled(true);
						    }
						});
		
						//reload documents
						var view = this._get_("documentsAssignList");
						view.store.load();
		
						window.show();
	}
	
	,_exportSalesInvoice_: function() {
		
						var view = this._get_("InvoiceExportForm");
						var field = view._get_("exportType");
						var fieldValue = field.getValue();
		
						if(fieldValue) {
							if(fieldValue=="PDF") {
								this.generateReport();
							} else if (fieldValue=="XML") {
								var dc = this._getDc_("invoice");
								var successFn = function() {
									this.showIataXml();
								}
								var o={
									name:"exportAsIataXML",
									callbacks:{
										successFn: successFn,
										successScope: this
									},
									modal:true
								};
								dc.doRpcData(o);
							}
							field.resetOriginalValue();
						}
	}
	
	,_exportSelectedToRicoh_: function() {
		
						var dc = this._getDc_("invoice");
						var r = dc.getRecord();
						if(r){
							var o = {
								name:"exportToRicoh",
								modal:true,
								callbacks:{
									failureFn: function() {
										dc.doReloadPage();
									}
								}
							}
							var message = Main.translate("applicationMsg", "exportToRicohSentControlNoMsg_lbl");
							if(r.get("controlNo") === __ACC_TYPE__.ControlNo._WAITING_FOR_){
								Ext.Msg.show({
						       		title : "Resend invoice to Ricoh?",
		                            msg : message,
		                            icon : Ext.MessageBox.WARNING,
						       		buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
						       		fn : function(btnId) {
						            	if( btnId == "yes" ){				                    
											dc.doRpcData(o);
				              			}
				       				},
				       				scope : this
								});
							}
							else {
								dc.doRpcData(o);
							}
						}
	}
	
	,_hasReason_: function() {
		
		
						var view = this._get_("creditMemoReason");
						var reason = view._get_("creditMemoReason");
						var result = false;
		
						if (!Ext.isEmpty(reason.getValue())) {
							result = true;
						}
						return result;
	}
	
	,_adjustSummary_: function(view) {
		
						var customCls = "sone-custom-summary";
						var summary = view._getSummary_();
						summary.addCls(customCls);
	}
	
	,_addNewOutgoingInvoiceLine_: function() {
		
		
						var invoiceLineDc = this._getDc_("invoiceLine");
						var outInvId = invoiceLineDc.getFilterValue("outInvId");
						var invoice = this._getDc_("invoice");
						var dc = this._getDc_("fuelEvent");
		
						dc.setParamValue("outgoingInvoiceId", outInvId);
		
						var successFn = function(dc,response,serviceName,specs) {
							invoiceLineDc.doReloadPage();
							this._closeAddFuelEventWdw_();
							invoice.doReloadRecord();
						};
		
						var o={
							name:"addNewOutgoingInvoiceLine",
							callbacks:{
								successFn: successFn,
								successScope: this
							},
							modal:true
						};
		
						dc.doRpcDataList(o);
	}
	
	,_closeAddFuelEventWdw_: function() {
		
						var w = this._getWindow_("addFuelEventWdw");
						var view = this._get_("fuelEventFilter");
						view.getForm().reset();
						w.close();
	}
	
	,_resetFuelEventFilter_: function() {
		
						var dc = this._getDc_("fuelEvent");
						dc.advancedFilter = null;
						dc.doQuery();
	}
	
	,_applyFuelEventFilter_: function() {
		
						var form = this._get_("fuelEventFilter");
						var dc = this._getDc_("fuelEvent");
		
						var map = {
							aircraftReg : "aircraftRegistration",
							flight: "flightNumber",
							fuelTicket: "ticketNumber",
							supplier: "supplierCode",
							dateStart: "fuelingDate",
							dateEnd: "fuelingDate"
						}
		
						var fields = form.getForm().getFields().items, l = fields.length, i = 0;
						var af = [];
						
		
						for (i; i<l; i++) {
							if (!Ext.isEmpty(fields[i].getValue())) {
		
								var operation = "";
								var value1 = "";
		
								if (map[fields[i].name] != "fuelingDate") {
									operation ="like";
									value1 = "%"+fields[i].getValue()+"%";
								}
								else {
		
									if (fields[i].name == "dateStart") {
										operation = ">=";
										value1 = fields[i].getValue();
									}
		
									if (fields[i].name == "dateEnd") {
										operation = "<=";
										value1 = fields[i].getValue();
									}
								}
		
								var o = {
									"id":null,
									"fieldName":map[fields[i].name],
									"operation":operation,
									"value1": value1
								};
								af.push(o);
							}
							
						}
						dc.advancedFilter = af;
						dc.doQuery();
	}
	
	,_hideText_: function(el) {
		
						return el.setText("");
	}
	
	,_addFuelEvent_: function() {
		
		
						var w = this._getWindow_("addFuelEventWdw");
						var titlePartOne = Main.translate("applicationMsg","selectFuelEventFor__lbl");
						var titlePartTwo = Main.translate("applicationMsg","selectFuelEventCustomer__lbl");
						var invoice = this._getDc_("invoice");
						var fuelEvent = this._getDc_("fuelEvent");
						var r = invoice.getRecord();
						var location = "";
						var receiver = "";
						var objectId = "";
						var objectType = "";
		
						if (r) {
							location = r.get("delLocCode");
							objectType = r.get("referenceDocType");
							if(objectType == __ACC_TYPE__.InvoiceReferenceDocType._PURCHASE_ORDER_){
								objectType = "FuelOrderLocation";
							}
							objectId = r.get("referenceDocId");
						}
		
						w.setTitle(titlePartOne+" "+location+" "+titlePartTwo+" "+receiver);
						w.show(undefined, function() {
							fuelEvent.setFilterValue("customerCode",receiver);
							fuelEvent.setFilterValue("departureCode",location);
							fuelEvent.setFilterValue("invoiceStatus", __OPS_TYPE__.OutgoingInvoiceStatus._NOT_INVOICED_);
							fuelEvent.setFilterValue("billPriceSourceId",objectId);
							fuelEvent.setFilterValue("billPriceSourceType",objectType);
		
							// Dan: SONE-3662
		
							var filters = [
								new Ext.util.Filter({
							      filterFn: function(item){
							         return item.get("status") != __ACC_TYPE__.FuelEventStatus._CANCELED_;
							      }
								})
							];
							this._get_("fuelEventList").store.filter(filters);
							fuelEvent.doQuery();
						}, this);				
	}
	
	,_saveInvoiceLine_: function() {
		
		
						var dc = this._getDc_("refInvoiceLine");
						var invoiceLineDc = this._getDc_("invoiceLine");
						var invoice = this._getDc_("invoice");
						var outInvId = invoiceLineDc.getFilterValue("outInvId");
						dc.setParamValue("invoiceId", outInvId);
		
						var successFn = function(dc,response,serviceName,specs) {
		
							invoiceLineDc.doReloadPage();
							this._cancelAddInvoiceLine_(true);
							invoice.doReloadRecord();
						};
		
						var o={
							name:"addNewCreditMemoLine",
							callbacks:{
								successFn: successFn,
								successScope: this
							},
							modal:true
						};
						dc.doRpcDataList(o);
	}
	
	,_addInvoiceLine_: function() {
		
						var w = this._getWindow_("addInvoiceLineWdw");
						w.show();
	}
	
	,_newCreditNote_: function() {
		
						var w = this._getWindow_("newCreditMemoWdw");
						w.show();
		
	}
	
	,_deleteInvoice_: function() {
		
						var dc = this._getDc_("invoice");
						dc.doDelete();
	}
	
	,_generateInvoice_: function() {
		
						var f = this._get_("InvoiceGenerate");
						if( f._checkIfValid_() ){
							this._setLocationOrAreaParams_();
							this._refreshInvNumSerie_();
						}
	}
	
	,_setLocationOrAreaParams_: function() {
		
		
						var dc = this._getDc_("invoice");
						var r = dc.getRecord();
						var ctx = this;
		
						var resetParams = function() {
							dc.setParamValue("deliveryLocIdParam", null);
							dc.setParamValue("deliveryAreaIdParam", null);
						}
		
						var doRpcCall = function() {
							var successFn = function(dc,response,serviceName,specs) {
								ctx.messageWdw(response);
								dc.doReloadPage();
							};
							var o={
								name:"generate",
								callbacks:{
									successFn: successFn,
									successScope: this
								},
								modal:true
							};
							dc.doRpcFilter(o);
						}
						
						if (r) {
							var deliveryId = r.get("deliveryId");
							var deliveryType = r.get("deliveryType");
		
							if (deliveryType == __FMBAS_TYPE__.GeoType._LOCATION_) {
								resetParams();
								dc.setParamValue("deliveryLocIdParam", deliveryId);
								doRpcCall();
							
							}
							else if (deliveryType == __FMBAS_TYPE__.GeoType._AREA_) {
								resetParams();
								dc.setParamValue("deliveryAreaIdParam", deliveryId);
								doRpcCall();
							}
							else {
								doRpcCall();
							}
						}
	}
	
	,_cancelAddInvoiceLine_: function(close) {
		
		
						var w = this._getWindow_("addInvoiceLineWdw");
		
						var searchFieldId = Ext.getCmp(this._InvoiceLineSearchFieldId_);
						var grid = this._get_("outgoingInvoiceLineAddList");
						var dc = grid._controller_;
		
						searchFieldId.setValue("");
						dc.advancedFilter = null;
						dc.doQuery();
		
						if (close) {
							w.close();
						}
	}
	
	,_showConfirmExportInvoicesToNAV: function(param) {
		
						Ext.Msg.show({
						       title : "Resend Invoices",
						       msg : "You are about to resend invoices/credit memos to the ERP system. Are you sure?",
						       icon : Ext.MessageBox.WARNING,
						       buttons : Ext.MessageBox.YES + Ext.MessageBox.NO,
						       fn : function(btnId) {
						              if( btnId == "yes" ){
						                     this._exportInvoicesToNAV(param);
				              }
				       },
				       scope : this
						});
	}
	
	,submitForApprovalRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.submitForApprovalDescription;							
								var responseResult = responseData.params.submitForApprovalResult;
								
								//close window
								this._getWindow_("wdwApprovalNote").close();
								
								//display results
								if (responseResult == true) {
									Main.info(responseDescription); 
								}
								else {
									Main.error(responseDescription);
								}
								//reload page(s)
								dcInvoiceLine.doReloadPage();
								dc.doReloadPage();
							}
						};
						this._sendAttachments_();
						var dcInvoiceLine = this._getDc_("invoice");
		                var dc = this._getDc_("invoice");
		                var t = {
					        name: "submitForApproval",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcDataList(t);
	}
	
	,showResult: function(response) {
		
						var responseData = Ext.getResponseDataInJSON(response);
						if (responseData && responseData.params) {
							var responseDescription = responseData.params.submitForApprovalDescription;							
							var responseResult = responseData.params.submitForApprovalResult;
							
							//display results
							if (responseResult == true) {
								Main.info(responseDescription); 
							}
						}
	}
	
	,_exportInvoicesToNAV: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.exportInvoiceDescription;							
								var responseResult = responseData.params.exportInvoiceResult;
								if (responseResult == true) {
									Main.info(responseDescription); 
								}
								else {
									Main.error(responseDescription);
								}
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("invoice");
						dc.setParamValue("exportInvoiceOption", param);
		                var t = {
					        name: "exportFuelPlusToNAVInvoice",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcDataList(t);
	}
	
	,_cancelCreditMemo_: function(close) {
		
		
						var w = this._getWindow_("newCreditMemoWdw");
						var centerPanel = Ext.getCmp(this._wdwMemoCenterPanelId_);
						var northPanel = Ext.getCmp(this._wdwMemoNorthPanelId_);
						var parent = Ext.get(this._wdwMemoNorthPanelId_);
						var wizardSteps = parent.select(".sone-wizard-step").elements;
						var wizardLine = parent.select(".sone-wizard-line").elements[0];
						var creditMemoList = this._elems_.get("creditMemoList");
						var btnContinueMemo2 = this._get_("btnContinueMemo2");
						var btnContinueMemo = this._get_("btnContinueMemo");
						var btnBackMemo = this._get_("btnBackMemo");
						var creditMemoDc = this._getDc_("creditMemo");
						
						Ext.each(wizardSteps, function(e) {
		
							var glyphContainer = e.getElementsByClassName("sone-step-indicator")[0];
		
							if (e.className.indexOf("step-done") > -1) {
								e.classList.remove("step-done");
								e.classList.add("step-pending");
								
								glyphContainer.innerHTML = "<i class='fa fa-2x fa-times' style='color: #BBBBBB'></i>";
							}
							if (e.className.indexOf("step-1") > -1) {
								e.classList.add("step-done");
								glyphContainer.innerHTML = "<i class='fa fa-2x fa-check' style='color: #FFFFFF'></i>";
							}
						}, this);
		
						// Dan: SONE-4061
		
						var secondSearchFieldId = Ext.getCmp(this._outgoingInvoiceLineSearchFieldId_);
						var grid = this._get_("outgoingInvoiceLineMemoList");
						var dc = grid._controller_;
						secondSearchFieldId.setValue("");
						dc.advancedFilter = null;
						dc.doQuery();
		
						wizardLine.style.background = "#e5e5e5";
						
						this._get_("outgoingInvoiceLineMemoList").hide();
						this._get_("creditMemoList").show();
		
						var searchFieldId = Ext.getCmp(this._creditMemoSearchFieldId_);
						var grid = this._get_("creditMemoList");
						searchFieldId.setValue("");
						grid.store.loadData([],false);
		
						btnContinueMemo2.hide();
						btnContinueMemo.show();
						btnContinueMemo.disable();
						btnBackMemo.hide();
		
						if (close) {
							w.close();
						}
		
	}
	
	,_jumpToThirdStep_: function() {
		
		
						var dc = this._getDc_("invoiceLineMemo");
						var outInvId = dc.getFilterValue("outInvId");
						dc.setParamValue("invoiceId", outInvId);
						dc.setParamValue("creditMemoReason", this._getDc_("invoiceLine").getParamValue("creditMemoReason"));
		
						var successFn = function(dc,response,serviceName,specs) {
		
							var params = Ext.getResponseDataInJSON(response).params;
							var generatedCreditMemoId = params.generatedCreditMemoId;
							var incoiceDc = this._getDc_("invoice");
		
							incoiceDc.doClearAllFilters();
							incoiceDc.setFilterValue("id", generatedCreditMemoId);			
							incoiceDc.doQuery({showCreditMemoDetails: true});   
		
							this._cancelCreditMemo_(true);
		
						if (!Ext.isEmpty(params.numberSeriesGeneratorMessage)) {
							Main.info(params.numberSeriesGeneratorMessage);
						}
						};
						var o={
							name:"newCreditMemo",
							callbacks:{
								successFn: successFn,
								successScope: this
							},
							modal:true
						};
						dc.doRpcDataList(o);
	}
	
	,_jumpToSecondStep_: function() {
		
						
						var centerPanel = Ext.getCmp(this._wdwMemoCenterPanelId_);
						var northPanel = Ext.getCmp(this._wdwMemoNorthPanelId_);
						var northPanelHtml = northPanel.body.dom.innerHTML;
						var outgoingInvoiceLineMemoList = this._elems_.get("outgoingInvoiceLineMemoList");
						var creditMemoList = this._elems_.get("creditMemoList");
		
						// Change the panel from the center region
		
						this._get_("outgoingInvoiceLineMemoList").show();
						this._get_("creditMemoList").hide();
		
						// Mark the second step as active
		
						var parent = Ext.get(this._wdwMemoNorthPanelId_);
						var wizardSteps = parent.select(".sone-wizard-step").elements;
						var wizardLine = parent.select(".sone-wizard-line").elements[0];
						var invoiceLineDc = this._getDc_("invoiceLine");
						var creditMemoDc = this._getDc_("creditMemo");
						var creditMemoRec = creditMemoDc.getRecord();
						var btnContinueMemo2 = this._get_("btnContinueMemo2");
						var btnContinueMemo = this._get_("btnContinueMemo");
						var btnBackMemo = this._get_("btnBackMemo");
						var creditMemoInvoiceId;
		
						if (creditMemoRec) {
		
							creditMemoInvoiceId = creditMemoRec.get("id");
							invoiceLineDc.setFilterValue("outInvId", creditMemoInvoiceId, true);
		
							// Set filter for isCredited
		
							var filters = [
								new Ext.util.Filter({
							      filterFn: function(item){
							         return item.get("isCredited") == false;
							      }
								})
							];
		
							this._get_("outgoingInvoiceLineMemoList").store.filter(filters);
							invoiceLineDc.doQuery();
							
							Ext.each(wizardSteps, function(e) {
								if (e.className.indexOf("step-2") > -1) {
									e.classList.add("step-done");
									var glyphContainer = e.getElementsByClassName("sone-step-indicator")[0];
									glyphContainer.innerHTML = "<i class='fa fa-2x fa-check' style='color: #FFFFFF'></i>";
									
								}
							}, this);
						}
		
						wizardLine.style.background = "linear-gradient(90deg, #a0d468 50%, #e5e5e5 50%)";
		
						btnContinueMemo2.show();
						btnContinueMemo.hide();
						btnBackMemo.show();
						outgoingInvoiceLineMemoList._controller_.doReloadPage();
	}
	
	,_afterDefineElements_: function() {
		
		
						var builder = this._getBuilder_();
						this._creditMemoSearchFieldId_ = Ext.id();
						this._InvoiceLineSearchFieldId_ = Ext.id();
						this._wdwMemoCenterPanelId_ = Ext.id();
						this._wdwMemoNorthPanelId_ = Ext.id();	
						this._wdwMemoReasonPanelId_ = Ext.id();	
						this._outgoingInvoiceLineSearchFieldId_ = Ext.id();	
		
						builder.change("outgoingInvoiceLineMemoList",{
							tbar: [{ 
								xtype: "textfield",
								id: this._outgoingInvoiceLineSearchFieldId_,
							    width: 200,
							    emptyText: "Enter criteria for full text search",
							    listeners: {
							        change: {
							            scope: this,
										buffer: 1000,
							            fn: function(field, newVal) {
							                var grid = this._get_("outgoingInvoiceLineMemoList");
							                
							                if (newVal && newVal != "*") {
												var af = [];
												var dateFilter = [];
												var dc = grid._controller_;
		
												for(i=0;i<grid.columns.length;i++){
													var operation ="like";
													var value1 = "%"+newVal+"%";
		
													if (grid.columns[i].dataIndex == "deliveryDate") {
		
														// If the value is a valid date in the following format: dd.mm.yyyy
		
														var splitValue = newVal.split(".");
														var formatedValue;
														if (splitValue.length == 3) {
															formatedValue = new Date(splitValue[2], splitValue[1] - 1, splitValue[0]);		
														}
														else {
															formatedValue = null;
														}
														operation = "=";
														value1 = formatedValue;
													}
		
													var o = {
														"id":null,
														"fieldName":grid.columns[i].dataIndex,
														"operation":operation,
														"value1": value1,
														"groupOp":"OR1"
													};
													if (grid.columns[i].hidden == false) {
														af.push(o);
													}
													
												}
												dc.advancedFilter = af;
												dc.doQuery();
							                }
							            }
							        }
							    }
							}, {
								glyph: "xf021@FontAwesome",
		                        xtype: "button",
								listeners: {
									click: {
										scope: this,
										fn: function() {
											var searchFieldId = Ext.getCmp(this._outgoingInvoiceLineSearchFieldId_);
											var grid = this._get_("outgoingInvoiceLineMemoList");
											var dc = grid._controller_;
											searchFieldId.setValue("");
		
											dc.advancedFilter = null;
											dc.doQuery();
										}
									}
								}
							}]
						});
		
						builder.change("outgoingInvoiceLineAddList",{
							tbar: [{ 
								xtype: "textfield",
								id: this._InvoiceLineSearchFieldId_,
							    width: 200,
							    emptyText: "Enter criteria for full text search",
							    listeners: {
							        change: {
							            scope: this,
										buffer: 1000,
							            fn: function(field, newVal) {
							                var grid = this._get_("outgoingInvoiceLineAddList");
							                
							                if (newVal && newVal != "*") {
												var af = [];
												var dateFilter = [];
												var dc = grid._controller_;
		
												for(i=0;i<grid.columns.length;i++){
													var operation ="like";
													var value1 = "%"+newVal+"%";
		
													if (grid.columns[i].dataIndex == "deliveryDate") {
		
														// If the value is a valid date in the following format: dd.mm.yyyy
		
														var splitValue = newVal.split(".");
														var formatedValue;
														if (splitValue.length == 3) {
															formatedValue = new Date(splitValue[2], splitValue[1] - 1, splitValue[0]);		
														}
														else {
															formatedValue = null;
														}
														operation = "=";
														value1 = formatedValue;
													}
		
													var o = {
														"id":null,
														"fieldName":grid.columns[i].dataIndex,
														"operation":operation,
														"value1": value1,
														"groupOp":"OR1"
													};
													if (grid.columns[i].hidden == false) {
														af.push(o);
													}
													
												}
												dc.advancedFilter = af;
												dc.doQuery();
							                }
							            }
							        }
							    }
							}, {
								glyph: "xf021@FontAwesome",
		                        xtype: "button",
								listeners: {
									click: {
										scope: this,
										fn: function() {
											var searchFieldId = Ext.getCmp(this._InvoiceLineSearchFieldId_);
											var grid = this._get_("outgoingInvoiceLineAddList");
											var dc = grid._controller_;
											searchFieldId.setValue("");
		
											dc.advancedFilter = null;
											dc.doQuery();
										}
									}
								}
							}]
						});
		
						builder.change("creditMemoList",{
							tbar: [{ 
								xtype: "textfield",
								readOnly: true,
								id: this._creditMemoSearchFieldId_,
							    width: 200,
							    emptyText: "Enter criteria for full text search",
							    listeners: {
							        change: {
							            scope: this,
										buffer: 1000,
							            fn: function(field, newVal) {
							                var grid = this._get_("creditMemoList");
							                
							                if (newVal && newVal != "*") {
												var af = [];
												var dateFilter = [];
												var dc = grid._controller_;
												for(i=0;i<grid.columns.length;i++){
													var operation ="like";
													var value1 = "%"+newVal+"%";
		
													if (grid.columns[i].dataIndex == "invoiceDate") {
														// BEGIN SONE-3059
														// when the date-to-string conversion function will be enabled in database
														// de-comment the following two lines and delete the rest of the code, until the END comment
														//operation = "OP_DATE";
														//value1 = value1.replaceAll("-",".").replaceAll("/",".");
		
														// If the value is a valid date in the following format: dd.mm.yyyy
		
														var splitValue = newVal.split(".");
														var formatedValue;
														if (splitValue.length == 3) {
															formatedValue = new Date(splitValue[2], splitValue[1] - 1, splitValue[0]);		
														}
														else {
															formatedValue = null;
														}
														operation = "=";
														value1 = formatedValue;
														// END SONE-3059
													}
		
													var o = {
														"id":null,
														"fieldName":grid.columns[i].dataIndex,
														"operation":operation,
														"value1": value1,
														"groupOp":"OR1"
													};
													if (grid.columns[i].hidden == false) {
														af.push(o);
													}											
												}
												dc.advancedFilter = af;
												
												// Set filters on status
		
												var filters = [
													new Ext.util.Filter({
												      filterFn: function(item){
														if (item) {
															return item.get("status") == __CMM_TYPE__.BillStatus._AWAITING_PAYMENT_ || item.get("status") == __CMM_TYPE__.BillStatus._PAID_;
														}
												      }
													}),
													new Ext.util.Filter({
														property: "referenceInvoiceId", value: null
													})
												];
		
												grid.store.filter(filters);
		
												dc.doQuery();
							                }
							            }
							        }
							    }
							}, {
								glyph: "xf021@FontAwesome",
		                        xtype: "button",
								listeners: {
									click: {
										scope: this,
										fn: function() {
											var searchFieldId = Ext.getCmp(this._creditMemoSearchFieldId_);
											var grid = this._get_("creditMemoList");
											var continueBtn = this._get_("btnContinueMemo");
											searchFieldId.setValue("");
											grid.store.removeAll();
											continueBtn.disable();
										}
									}
								}
							}]
						});
		
						builder.addPanel({
							name:"creditMemoWizard",
							height: 120
						});
		
						var createWizard = function() {
							return [
							  "<div class='sone-wizard-line'>",
						      "</div>",
						      "<div class='sone-wizard-wrapper'>",
						      "<div class='sone-wizard-step step-1 step-done'>",
						         "<div class='sone-step-body'>",
						           " <div class='sone-step-box'>",
						               "<div class='sone-step-indicator'>",
									   "<i class='fa fa-2x fa-check'></i>",
						               "</div>",
						               "<div class='sone-step-label'>",
						                 Main.translate("applicationMsg", "memoWizardStep1__lbl"),
						              " </div>",
						           " </div>",
						         "</div>",
						      "</div>",
						      "<div class='sone-wizard-step step-2 step-pending'>",
						         "<div class='sone-step-body'>",
						            "<div class='sone-step-box'>",
						               "<div class='sone-step-indicator'>",
									   "<i class='fa fa-2x fa-times'></i>",
						               "</div>",
						               "<div class='sone-step-label'>",
						                  Main.translate("applicationMsg", "memoWizardStep2__lbl"),
						               "</div>",
						            "</div>",
						         "</div>",
						      "</div>",
						      "<div class='sone-wizard-step step-3 step-pending'>",
						         "<div class='sone-step-body'>",
						            "<div class='sone-step-box'>",
						               "<div class='sone-step-indicator'>",
									   "<i class='fa fa-2x fa-times'></i>",
						               "</div>",
						               "<div class='sone-step-label'>",
						                 Main.translate("applicationMsg", "memoWizardStep3__lbl"),
						               "</div>",
						            "</div>",
						         "</div>",
						      "</div>",
						   "</div>"];
						}
		
						builder.change("newCreditMemoWdw",{
							layout: "border",
							items: [{
								region: "north",
								height: 110,
								id : this._wdwMemoNorthPanelId_,
								html: createWizard()
							}, {
								region: "center",
								id: this._wdwMemoReasonPanelId_,
								items: [
									this._elems_.get("creditMemoReason")
								]
								
							}, {
								region: "south",
								layout: "fit",
								height: 210,
								id : this._wdwMemoCenterPanelId_,
								items: [
									this._elems_.get("creditMemoList"),
									this._elems_.get("outgoingInvoiceLineMemoList")
								]						
							}]
						});				
	}
	
	,_refreshInvNumSerie_: function() {
		
						var frameName = "atraxo.acc.ui.extjs.frame.AccountingSettings_Ui";
						var frameToGet = getApplication().getFrameInstance(frameName);
						if (frameToGet) {
							var dcToGet = frameToGet._getDc_("invoiceNumberSerie");
							dcToGet.doReloadPage();
						}				
	}
	
	,_afterDefineDcs_: function() {
		
						var invoice = this._getDc_("invoice");
						var attachment = this._getDc_("attachment");
						var note = this._getDc_("note");
						var outgoingInvoiceLineDetails = this._getDc_("outgoingInvoiceLineDetails");
						var history = this._getDc_("history");
						var invoiceLine = this._getDc_("invoiceLine");
						var invoiceLineMemo = this._getDc_("invoiceLineMemo");
						var creditMemoDc = this._getDc_("creditMemo");
						var outgoingInvoiceBulkExportWizard = this._getDc_("outgoingInvoiceBulkExportWizard");
						var userFields = this._getDc_("userFields");
						
						invoiceLineMemo.store.pageSize = 2000;
						creditMemoDc.store.pageSize = 2000;
		
						invoiceLine.on("afterDoDeleteSuccess", function(dc) {
							invoice.doReloadRecord();
						}, this);
		
						attachment.on("afterDoNew", function (dc) {	
							dc.doReloadPage();				
						}, this);
		
						invoice.on("onEditIn", function (dc) {	
							this.updateInvoiceLineHeader();
						}, this);
		
						invoice.on("onEditIn", function (dc) {	
							this.updateInvoiceLineHeader();
						}, this);
		
						invoice.on("beforeDoDelete", function () {	
							var customDeleteMsg = invoice.getParamValue("displayCustomDelMsg");
							if (!Ext.isEmpty(customDeleteMsg)) {
								invoice.commands.doDelete.confirmMessageBody =  Main.translate("applicationMsg","changeMemoReferenceInvoice__lbl");
							}
						}, this);
		
						invoice.on("afterDoDeleteSuccess", function (dc, ajaxResult) {	
							if (ajaxResult.options.options.showWizard == true) {
		
								invoice.setParamValue("displayCustomDelMsg", null);
								invoice.commands.doDelete.confirmMessageBody =  Main.translate("msg", "dc_confirm_delete_selection");
								var view = this._get_("InvoiceList");
								var resetButton = Ext.getCmp(view._resetButtonId_);
		
								// Pass some custom options to the afterViewFiltersReset event
		
								this._showStackedViewElement_("main", "canvas1", resetButton._resetGrid_({showNewCreditMemoWdw: true}));
		
							}
						}, this);
		
						invoice.on("afterViewFiltersReset", function (dc, options) {
							if (options.showNewCreditMemoWdw == true) {
								this._newCreditNote_();
							}
						}, this);
		
						note.on("afterDoNew", function (dc) {
						    this.showNoteWdw();
						}, this);
		
						outgoingInvoiceLineDetails.on("afterDoQuerySuccess", function(dc, ajaxResult) {
							this.unmask();
						}, this);
		
						invoice.on("afterDoSaveSuccess", function (dc) {	
							dc.doReloadRecord();
							invoiceLine.doReloadPage();
							history.doReloadPage();
						}, this);
		
						invoiceLine.on("afterDoSaveSuccess", function (dc) {	
							this._getDc_("invoice").doReloadRecord();					
						}, this);
		
						invoice.on("afterDoQuerySuccess", function (dc, ajaxResult) {	
							if (ajaxResult.options.showCreditMemoDetails == true) {
								this._showStackedViewElement_("main", "canvas2");
							}
							if (ajaxResult.options.showEdit == true) {
								this._showStackedViewElement_("main", "canvas2");
							}
						}, this);
		
						invoice.on("recordChange", function (dc, ajaxResult) {	
							this._doRecordChange_();
						}, this);
		
						outgoingInvoiceBulkExportWizard.on("afterDoNew", function (dc, ajaxRequest) {	
		                    var window = this._getWindow_("wdwBulkExportWizard");
							var openBulkExportWindow = ajaxRequest.openBulkExportWindow;
							if(openBulkExportWindow) {
								window.show();
								window.setStep(1);						
							}
		                 }, this);
	}
	
	,_doRecordChange_: function() {
		
						var invoice = this._getDc_("invoice");
						var r = invoice.getRecord();
						if (this._get_("InvoiceHeader")) {
							var view = this._get_("InvoiceHeader");
							var formPanel = view._get_("c5");
							var btnAddFeulEv = this._get_("btnAddFeulEv");
							var btnInvAddFeulEv = this._get_("btnInvAddFeulEv");	
							if (r) {
								var invoiceRefId = r.get("invoiceRefId");
								if (!Ext.isEmpty(invoiceRefId)) {
									formPanel.setVisible(true);
									btnAddFeulEv.setVisible(true);
									btnInvAddFeulEv.setVisible(false);
								}
								else {
									formPanel.setVisible(false);
									btnAddFeulEv.setVisible(false);
									btnInvAddFeulEv.setVisible(true);
								}
							}
						}				
	}
	
	,_afterLinkElementsPhase2_: function() {
		
						this._doRecordChange_();
	}
	
	,updateInvoiceLineHeader: function() {
		
						var invoice = this._getDc_("invoice");
						var invRec = invoice.getRecord();
						var unitCode = invRec.get("unitCode");
						var currencyCode = invRec.get("currCode");
						var grid = this._get_("InvoiceLineList");
						var view = grid.getView();
						var headerCt = view.getHeaderCt();
		
						var quantityColumn = headerCt.down("[dataIndex=quantity]");
						var amountColumn = headerCt.down("[dataIndex=amount]");
						var vatAmountColumn = headerCt.down("[dataIndex=vat]");
		
						quantityColumn.setText(quantityColumn.initialConfig.header+" ("+unitCode+")");
						amountColumn.setText(amountColumn.initialConfig.header+" ("+currencyCode+")");
						vatAmountColumn.setText(vatAmountColumn.initialConfig.header+" ("+currencyCode+")");
	}
	
	,_setReadOnly_: function() {
		
						var dc = this._getDc_("invoice");
						dc.doReloadPage();
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/OutgoingInvoices.html";
					window.open( url, "SONE_Help");
	}
	
	,onWdwInvoiceClose: function() {
		
						var dc = this._getDc_("invoice");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onNewCreditMemoWdwClose: function() {
		
						var dc = this._getDc_("invoice");
						this._get_("outgoingInvoiceLineMemoList").store.clearFilter();
						dc.doCancel();
						this._cancelCreditMemo_();
	}
	
	,onExportWdwClose: function() {
		
						var dc = this._getDc_("invoice");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onAddInvoiceLineWdwClose: function() {
		
						this._cancelAddInvoiceLine_();
	}
	
	,messageWdw: function(response) {
		
		
					var params = Ext.getResponseDataInJSON(response).params;
					var numar = params.nrGeneratedInvoices;
					var msg = "Thank you for waiting. We have generated "+numar+" sales invoices for you.";
					var errorMsg = params.invGeneratorErrorMsg;
					if (errorMsg) {
						msg = msg + "<br/>" + errorMsg;
					}
					this._getWindow_("wdwGenerate").close();
					Ext.Msg.show({
				       title : "Note",
				       msg : msg,
				       buttons : Ext.MessageBox.OK,
				       scope : this
						});
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,canReset: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.status == __TYPES__.invoices.status.draft || record.data.status == __TYPES__.invoices.status.paid){
								return false;			
							}
						}
						return true;
	}
	
	,canSubmit: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.status != __TYPES__.invoices.status.checkPassed){
								return false;			
							}
						}
						return true;
	}
	
	,canApproveList: function(dc) {
		
						var useWorkflowSysParam = (_SYSTEMPARAMETERS_.syscreditmemoapproval === "true");
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if ( useWorkflowSysParam ){
								if(record.data.invoiceType === __ACC_TYPE__.InvoiceTypeAcc._CRN_ || record.data.status != __TYPES__.invoices.status.awaitingApproval){
									return false;			
								}
							}
							else{
								if(record.data.status != __TYPES__.invoices.status.awaitingApproval){
									return false;			
								}
							}
						}
						return true;
	}
	
	,canApproveSingle: function(dc) {
		
						var useWorkflowSysParam = (_SYSTEMPARAMETERS_.syscreditmemoapproval === "true");
						if ( useWorkflowSysParam ){
							return dc.record.data.status ==  __TYPES__.invoices.status.awaitingApproval && dc.record.data.invoiceType != __ACC_TYPE__.InvoiceTypeAcc._CRN_;
						}
						else{
							return dc.record.data.status ==  __TYPES__.invoices.status.awaitingApproval;
						}
	}
	
	,canSubmitForApproval: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.invoiceType == __ACC_TYPE__.InvoiceTypeAcc._CRN_ && record.data.status == __CMM_TYPE__.BillStatus._AWAITING_APPROVAL_ 
								&& (record.data.approvalStatus == __CMM_TYPE__.BidApprovalStatus._NEW_ || record.data.approvalStatus == __CMM_TYPE__.BidApprovalStatus._REJECTED_)){
								return true;			
							}
						}
						return false;
	}
	
	,canResend: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.status != __OPS_TYPE__.OutgoingInvoiceStatus._AWAITING_PAYMENT_ && record.data.status != __OPS_TYPE__.OutgoingInvoiceStatus._PAID_ && record.data.status != __OPS_TYPE__.OutgoingInvoiceStatus._CREDITED_){
								return false;			
							}
						}
						return true;
	}
	
	,canPaid: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.status != __TYPES__.invoices.status.awaitingPayment){
								return false;			
							}
						}
						return true;
	}
	
	,generateInvoiceReport: function() {
		
						var dc = this._getDc_("invoice");
						var customExpCols = ["id"]; 
						var gridName = __TYPES__.invoices.reports.gridName;
						var format = __TYPES__.invoices.reports.format;
						var reportCode = dc.params.get("reportCode").split(".")[0];
						var record = dc.record;
		
						this.doCustomReport(gridName, format, reportCode, customExpCols, record);
	}
	
	,showIataXml: function() {
		
		                var dc = this._getDc_("invoice");
						dc.doReloadPage();
		                var fn = dc.getParamValue("iataXmlFileName");
		                var r = window.open(Main.urlDownload + "/" + fn,"Preview");
		                r.focus();  
						
	}
	
	,_resetExportWdw_: function() {
		
						//reset the fields from the first step
						var view = this._get_("bulkExportStep1");
						var fields = ["receiverCode", "delLocCode", "dateFrom", "dateTo", "reportName"];
						for (var i=0; i<fields.length; i++) {
							view._get_(fields[i]).setValue(null);
						}
		
						//reset the label in case the date was incorrectly set
						view._get_("periodLabel").labelEl.update(Main.translate("invoiceBulkExportSelectBtns", "invoiceDateBetween__lbl"));
		
						//resets the params
						var firstStepDc = this._getDc_("outgoingInvoiceBulkExportWizard");
						var secondStepDc = this._getDc_("outgoingInvoiceBulkExportWizardInfiniteList");
						var params = ["filter", "standardFilter", "reportName", "reportCode"];
						for (var i=0; i<params.length; i++) {
							firstStepDc.setParamValue(params[i],"");
							secondStepDc.setParamValue(params[i],"");
						}
		
						this._uncheckAllInvoices_();
	}
	
	,canSendToRicoh: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.controlNo.toUpperCase() !== __ACC_TYPE__.ControlNo._NOT_AVAILABLE_.toUpperCase() 
								&& record.data.controlNo.toUpperCase() !== __ACC_TYPE__.ControlNo._WAITING_FOR_.toUpperCase()){
								return false;
							}
						}
						return true;
	}
	
	,_resetExportRicohWdw_: function() {
		
						//reset the fields from the first step
						var view = this._get_("bulkExportRicohStep1");
						var fields = ["receiverCode", "delLocCode", "dateFrom", "dateTo"];
						for (var i=0; i<fields.length; i++) {
							view._get_(fields[i]).setValue(null);
						}
		
						//reset the label in case the date was incorrectly set
						view._get_("periodLabel").labelEl.update(Main.translate("invoiceBulkExportSelectBtns", "invoiceDateBetween__lbl"));
		
						//resets the params
						var firstStepDc = this._getDc_("outgoingInvoiceBulkExportRicohWizard");
						var secondStepDc = this._getDc_("outgoingInvoiceBulkExportRicohWizardInfiniteList");
						var params = ["filter", "standardFilter"];
						for (var i=0; i<params.length; i++) {
							firstStepDc.setParamValue(params[i],"");
							secondStepDc.setParamValue(params[i],"");
						}
		
						this._uncheckAllRicohInvoices_();
	}
	
	,_uncheckAllInvoices_: function() {
		
						var grid = this._get_("bulkExportStep2List");   
		                var ctrl = grid._controller_;
		                var s = ctrl.store; 
		                var bulkExportWizardDc = this._getDc_("outgoingInvoiceBulkExportWizardInfiniteList"); 
		                var viewSize = s.viewSize;
		                if (viewSize > 0) {
		                    grid.getSelectionModel().deselectAll();
		                    grid._selectedAllInvoices_ = false;
		                    bulkExportWizardDc.setParamValue("selectedAllInvoices", false);
		                    bulkExportWizardDc.setParamValue("unselectedInvoices","");
							bulkExportWizardDc.setParamValue("selectedInvoices","");
		                    grid._deselectedIds_ = [];
		                    grid._deselectedIndexes_ = [];
							grid._selectedIds_ = [];
		                } 
	}
	
	,_uncheckAllRicohInvoices_: function() {
		
						var grid = this._get_("bulkExportRicohStep2List");   
		                var ctrl = grid._controller_;
		                var s = ctrl.store; 
		                var bulkExportWizardDc = this._getDc_("outgoingInvoiceBulkExportRicohWizardInfiniteList"); 
		                var viewSize = s.viewSize;
		                if (viewSize > 0) {
		                    grid.getSelectionModel().deselectAll();
		                    grid._selectedAllInvoices_ = false;
		                    bulkExportWizardDc.setParamValue("selectedAllInvoices", false);
		                    bulkExportWizardDc.setParamValue("unselectedInvoices","");
							bulkExportWizardDc.setParamValue("selectedInvoices","");
		                    grid._deselectedIds_ = [];
		                    grid._deselectedIndexes_ = [];
							grid._selectedIds_ = [];
		                } 
	}
	
	,_goToRicohStep2_: function() {
		
						var w = this._getWindow_("wdwBulkExportRicohWizard");
						var dc = this._getDc_("outgoingInvoiceBulkExportRicohWizardInfiniteList");
						var view = this._get_("bulkExportRicohStep1");
			
						// transferam parametrul de report catre al doilea dc
						var firstStepDc = this._getDc_("outgoingInvoiceBulkExportRicohWizard");
		
						// facem filtrare
		
						var fields = ["receiverCode", "delLocCode", "dateFrom", "dateTo"];
						var af = [];
						var tmp = [];
						var o, controlNumber;
						for (var i=0; i<fields.length; i++) {
							var value = view._get_(fields[i]).getValue();
							var name = view._get_(fields[i]).name;
							if (!Ext.isEmpty(value)) {
		
									if (name == "receiverCode" || name == "delLocCode") {
										operation = "=";
										value1 = value;
									}
		
									if (view._get_(fields[i]).name == "dateFrom") {
										operation = ">=";
										value1 = value;
									}
		
									if (view._get_(fields[i]).name == "dateTo") {
										operation = "<=";
										value1 = value;
									}
		
									if( name == "receiverCode" || name == "delLocCode" ) {
										o = {
											"id":null,
											"fieldName":view._get_(fields[i]).name,
											"operation":operation,
											"value1": value1
										};
									}
		
									if( name == "dateTo" || name == "dateFrom" ) {
										if( Ext.isDate(value1)) {
											value1 = new Date(value1.setHours(value1.getHours() - value1.getTimezoneOffset() / 60));
										}
										o = {
											"id":null,
											"fieldName":"invoiceDate",
											"operation":operation,
											"value1": value1
										};
									}
								af.push(o);
								tmp.push(o);
		
							}
						}
						
						//add advanced filter condition to not show awaiting approval invoices
						o = {
							"id":null,
							"fieldName":"status",
							"operation":"<>",
							"value1": "Awaiting approval"
						};
						controlNumber = {
							"id":null,
							"fieldName":"controlNo",
							"operation":"=",
							"value1": "Not available"
						};
		
						af.push(o);
						tmp.push(o);
						af.push(controlNumber);
						tmp.push(controlNumber);
		
						//send the filter and the standard filter to the second step dc
						dc.advancedFilter = af;
						dc.setParamValue("filter",JSON.stringify(dc.advancedFilter));
						//sets the tmpFilter as when we use quick search and we reset to reset to the original filter
						dc._tmpfilter_ = tmp;
						dc.doQuery();
		
						//afisare/ascundere butoane
						this._buttonsControllerRicoh_ ( 2 );
	}
	
	,_goToStep2_: function() {
		
						var w = this._getWindow_("wdwBulkExportWizard");
						var dc = this._getDc_("outgoingInvoiceBulkExportWizardInfiniteList");
						var view = this._get_("bulkExportStep1");
			
						// transferam parametrul de report catre al doilea dc
						var firstStepDc = this._getDc_("outgoingInvoiceBulkExportWizard");
						dc.setParamValue("reportName",firstStepDc.getParamValue("reportName"));
						dc.setParamValue("reportCode",firstStepDc.getParamValue("reportCode"));
		
						// facem filtrare
		
						var fields = ["receiverCode", "delLocCode", "dateFrom", "dateTo"];
						var af = [];
						var tmp = [];
						var o;
						for (var i=0; i<fields.length; i++) {
							var value = view._get_(fields[i]).getValue();
							var name = view._get_(fields[i]).name;
							if (!Ext.isEmpty(value)) {
		
									if (name == "receiverCode" || name == "delLocCode") {
										operation = "=";
										value1 = value;
									}
		
									if (view._get_(fields[i]).name == "dateFrom") {
										operation = ">=";
										value1 = value;
									}
		
									if (view._get_(fields[i]).name == "dateTo") {
										operation = "<=";
										value1 = value;
									}
		
									if( name == "receiverCode" || name == "delLocCode" ) {
										o = {
											"id":null,
											"fieldName":view._get_(fields[i]).name,
											"operation":operation,
											"value1": value1
										};
									}
		
									if( name == "dateTo" || name == "dateFrom" ) {
										if( Ext.isDate(value1)) {
											value1 = new Date(value1.setHours(value1.getHours() - value1.getTimezoneOffset() / 60));
										}
										o = {
											"id":null,
											"fieldName":"invoiceDate",
											"operation":operation,
											"value1": value1
										};
									}
								af.push(o);
								tmp.push(o);
		
							}
						}
						
						//add advanced filter condition to not show awaiting approval invoices
						o = {
							"id":null,
							"fieldName":"status",
							"operation":"<>",
							"value1": "Awaiting approval"
						};
						af.push(o);
						tmp.push(o);
		
						//add advanced filter condition to not show awaiting approval invoices
						o = {
							"id":null,
							"fieldName":"status",
							"operation":"<>",
							"value1": "Credited"
						};
						af.push(o);
						tmp.push(o);
		
						//send the filter and the standard filter to the second step dc
						dc.advancedFilter = af;
						dc.setParamValue("filter",JSON.stringify(dc.advancedFilter));
						//sets the tmpFilter as when we use quick search and we reset to reset to the original filter
						dc._tmpfilter_ = tmp;
						dc.doQuery();
		
						//afisare/ascundere butoane
						this._buttonsController_ ( 2 );
	}
	
	,_buttonsController_: function(stepNumber) {
		
		
						var window = this._getWindow_("wdwBulkExportWizard");
						var ctx = this;
						var hideElems, showElems;
						if ( stepNumber === 1 ) {
							hideElems = ["btnBackToConditions", "btnExportBulkInvoices"];
							showElems = ["btnContinueStep1", "btnDiscardStep1"];
						} else {
							hideElems = ["btnContinueStep1"];
							showElems = ["btnBackToConditions", "btnExportBulkInvoices"];
						}
		
						Ext.each(hideElems, function(e) {
							ctx._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							ctx._get_(e).show();
						}, this);				
		
						if ( stepNumber === 1 ) {
							window.setStep(1);
						} else {
							window.nextStep();
						}
	}
	
	,_buttonsControllerRicoh_: function(stepNumber) {
		
		
						var window = this._getWindow_("wdwBulkExportRicohWizard");
						var ctx = this;
						var hideElems, showElems;
						if ( stepNumber === 1 ) {
							hideElems = ["btnBackToRicohConditions", "btnExportBulkRicohInvoices"];
							showElems = ["btnContinueRicohStep1", "btnDiscardRicohStep1"];
						} else {
							hideElems = ["btnContinueRicohStep1"];
							showElems = ["btnBackToRicohConditions", "btnExportBulkRicohInvoices"];
						}
		
						Ext.each(hideElems, function(e) {
							ctx._get_(e).hide();
						}, this);
		
						Ext.each(showElems, function(e) {
							ctx._get_(e).show();
						}, this);				
		
						if ( stepNumber === 1 ) {
							window.setStep(1);
						} else {
							window.nextStep();
						}
	}
	
	,_backToStep1_: function() {
		
		
						var window = this._getWindow_("wdwBulkExportWizard");
						this._buttonsController_( 1 );
						this._uncheckAllInvoices_();
						window.setStep(1);
	}
	
	,_backToRicohStep1_: function() {
		
		
						var window = this._getWindow_("wdwBulkExportRicohWizard");
						this._buttonsControllerRicoh_( 1 );
						this._uncheckAllRicohInvoices_();
						window.setStep(1);
	}
	
	,_endDefine_: function() {
		
						
						var builder = this._getBuilder_();
						this._InvoiceSearchFieldId_ = Ext.id();
						this._ricohSearchFieldId_ = Ext.id();
		
						this._getBuilder_().change("bulkExportStep2List", { 
		                    _deselectedIndexes_ : [],
		                    _deselectedIds_ : [],
							_selectedIndexes_ : [],
							_selectedIds_ : []
		                });
		
						this._getBuilder_().change("bulkExportRicohStep2List", { 
		                    _deselectedIndexes_ : [],
		                    _deselectedIds_ : [],
							_selectedIndexes_ : [],
							_selectedIds_ : []
		                });
		
						var selectDeselectAll = [{
		                    glyph: "xf00c@FontAwesome",
		                    text: Main.translate("invoiceBulkExportSelectBtns", "checkAllBtn__lbl"),
		                    xtype: "button",
		                    listeners: {
		                        click: {
		                            scope: this,
									fn: function(b) {
		                                var grid = this._get_("bulkExportStep2List");   
		                                var bulkExportWizardDc = this._getDc_("outgoingInvoiceBulkExportWizardInfiniteList"); 
		                                var ctrl = grid._controller_;
		                                var s = ctrl.store; 
		                                var viewSize = s.viewSize;  
		                                var lastRequestEnd = s.lastRequestEnd;								
		        
		                                if (viewSize > 0) {									
											grid._deselectedIds_ = [];
		                                    grid._deselectedIndexes_ = [];
											grid._selectedIds_ = [];
		                                    grid._selectedIndexes_ = [];
		                                    grid.getSelectionModel().selectRange(0, lastRequestEnd);
		                                    bulkExportWizardDc.setParamValue("selectedAllInvoices", true);
											bulkExportWizardDc.setParamValue("unselectedInvoices","");
											bulkExportWizardDc.setParamValue("selectedInvoices","");
		                                    grid._selectedAllInvoices_ = true;
		                                    bulkExportWizardDc.setParamValue("filter",JSON.stringify(ctrl.advancedFilter));
		                                    bulkExportWizardDc.setParamValue("standardFilter",JSON.stringify(ctrl.getFilter().data));
		                                }
		                            }
		                        }
		                    }
		                },{
		                    glyph: "xf00d@FontAwesome",
		                    text: Main.translate("invoiceBulkExportSelectBtns", "unCheckAllBtn__lbl"),
		                    xtype: "button",
		                    listeners: {
		                        click: {
		                            scope: this,
		                            fn: function() {
										this._uncheckAllInvoices_();
		                            }
		                        }
		                    }
		                }];
		
						var selectDeselectAllRicoh = [{
		                    glyph: "xf00c@FontAwesome",
		                    text: Main.translate("invoiceBulkExportSelectBtns", "checkAllBtn__lbl"),
		                    xtype: "button",
		                    listeners: {
		                        click: {
		                            scope: this,
									fn: function(b) {
		                                var grid = this._get_("bulkExportRicohStep2List");   
		                                var bulkExportWizardDc = this._getDc_("outgoingInvoiceBulkExportRicohWizardInfiniteList"); 
		                                var ctrl = grid._controller_;
		                                var s = ctrl.store; 
		                                var viewSize = s.viewSize;  
		                                var lastRequestEnd = s.lastRequestEnd;								
		        
		                                if (viewSize > 0) {									
											grid._deselectedIds_ = [];
		                                    grid._deselectedIndexes_ = [];
											grid._selectedIds_ = [];
		                                    grid._selectedIndexes_ = [];
		                                    grid.getSelectionModel().selectRange(0, lastRequestEnd);
		                                    bulkExportWizardDc.setParamValue("selectedAllInvoices", true);
											bulkExportWizardDc.setParamValue("unselectedInvoices","");
											bulkExportWizardDc.setParamValue("selectedInvoices","");
		                                    grid._selectedAllInvoices_ = true;
		                                    bulkExportWizardDc.setParamValue("filter",JSON.stringify(ctrl.advancedFilter));
		                                    bulkExportWizardDc.setParamValue("standardFilter",JSON.stringify(ctrl.getFilter().data));
		                                }
		                            }
		                        }
		                    }
		                },{
		                    glyph: "xf00d@FontAwesome",
		                    text: Main.translate("invoiceBulkExportSelectBtns", "unCheckAllBtn__lbl"),
		                    xtype: "button",
		                    listeners: {
		                        click: {
		                            scope: this,
		                            fn: function() {
										this._uncheckAllRicohInvoices_();
		                            }
		                        }
		                    }
		                }];
		
						builder.change("bulkExportStep2List",
							this._injectSearchField_(this._InvoiceSearchFieldId_,"bulkExportStep2List","Search for invoices", 200, true, true, selectDeselectAll)
						);
		
						builder.change("bulkExportRicohStep2List",
							this._injectSearchField_(this._ricohSearchFieldId_,"bulkExportRicohStep2List","Search for invoices", 200, true, true, selectDeselectAllRicoh, true)
						);
		
	}
	
	,_standardFilterResetHack_: function() {
		
		
		                var grid = this._get_("bulkExportStep2List");   
		                var bulkExportWizardDc = this._getDc_("outgoingInvoiceBulkExportWizardInfiniteList"); 
		                var ctrl = grid._controller_;
						bulkExportWizardDc.setParamValue("standardFilter",JSON.stringify(ctrl.getFilter().data));
	}
	
	,_standardFilterRicohResetHack_: function() {
		
		
		                var grid = this._get_("bulkExportRicohStep2List");   
		                var bulkExportWizardDc = this._getDc_("outgoingInvoiceBulkExportRicohWizardInfiniteList"); 
		                var ctrl = grid._controller_;
						bulkExportWizardDc.setParamValue("standardFilter",JSON.stringify(ctrl.getFilter().data));
	}
	
	,_injectSearchField_: function(id,gridName,emptyLabelText,fieldWidth,showResetBtn,buttonInsideField,extraElems,isRicoh) {
		
		
						var defaultMargin = "0px 0px 0px 0px";
						var marginInside = "0px 0px 0px -42px";
						var buttonInsideCls = "sone-search-btn-inside";
						var defaultCls = "sone-search-btn-outside";
						var btnId = id+"-btn";				
		
						var toolbarItems = [{ 
							xtype: "textfield",
							id: id,
						    width: fieldWidth ? fieldWidth : 200,
						    emptyText: emptyLabelText,
						    listeners: {
						        change: {
						            scope: this,
									buffer: 1000,
						            fn: function(field, newVal) {
		
						                var grid = this._get_(gridName);
										var resetBtn = Ext.getCmp(btnId);							
						                
						                if (newVal && newVal != "*") {
		
											var dc = grid._controller_;
											var af = dc.advancedFilter;
											var dateFilter = [];
											
											var s = dc.store;
		
											// Check if the records have been changed and if yes discard the chages
		
											if (s.getNewRecords().length > 0 || s.getUpdatedRecords().length > 0 || s.getRemovedRecords().length > 0) {
												s.rejectChanges();
											}
		
											for(i=0;i<grid.columns.length;i++){
												var operation ="like";
												var value1 = "%"+newVal+"%";
		
												var o = {
													"id":null,
													"fieldName":grid.columns[i].dataIndex,
													"operation":operation,
													"value1": value1,
													"groupOp":"OR1"
												};
												if (grid.columns[i].hidden == false && grid.columns[i].dataIndex !== "invoiceDate") {
													af.push(o);
												}
												
											}
		
											resetBtn.show();									
											dc.advancedFilter = af;
											dc.doQuery();
											//resets the params for check,uncheck
											if (!isRicoh) {
												this._uncheckAllInvoices_();
											}
											else {
												this._uncheckAllRicohInvoices_();
											}
											dc.setParamValue("filter",JSON.stringify(dc.advancedFilter));
						                }
										else {
											resetBtn.hide();
										}
						            }
						        }
						    }
						},{
							glyph: "xf021@FontAwesome",
		                    xtype: "button",
							id : btnId,
							hidden: true,
							margin: buttonInsideField === true ? marginInside : defaultMargin,
							cls: buttonInsideField === true ? buttonInsideCls : defaultCls,
							listeners: {
								click: {
									scope: this,
									fn: function(b) {
										var searchFieldId = Ext.getCmp(id);
										var grid = this._get_(gridName);
										var dc = grid._controller_;
										searchFieldId.setValue("");
										b.hide();
										dc.advancedFilter = dc._tmpfilter_;	
										if (!isRicoh) {
											this._uncheckAllInvoices_();
										}
										else {
											this._uncheckAllRicohInvoices_();
										}
										dc.setParamValue("filter",JSON.stringify(dc.advancedFilter));					
										dc.doQuery();
									}
								}
							}
						}];
		
						if (extraElems) {
							var c = toolbarItems.concat(extraElems);
							toolbarItems = c;
						}
						return { tbar: toolbarItems}
	}
	
	,_handleRicohSelect_: function(grid) {
		
						var store = grid.getStore();
						var dc = this._getDc_("outgoingInvoiceBulkExportRicohWizardInfiniteList");
						var ctx = this;
		
						// Disable select / deselect on cell click if the clicked cell is not the one containing the checkbox
						grid.view.on("beforecellclick", function(view, cell, cellIndex, r, td, rowIndex, e) {
							if (cellIndex > 0) {
								return false;
							}
							if (e.shiftKey === true) {
								return false;
							}	
						});
						
						// if the selected all is false then deselect all the records
						store.on("load", function() {
							if (grid._selectedAllInvoices_ === false) {
								grid.getSelectionModel().deselectAll();
							}
						}, this);
		
						// if one of the column names is pressed, the list is reordered and everything is unchecked
						grid.on("headerclick", function() {
							ctx._uncheckAllInvoices_();
						});
		
						grid.view.getEl().on("scroll", function() {
							if (grid._selectedAllInvoices_ === true) {
		
								var lastRequestStart = store.lastRequestStart; //returneaza primul index selectat
								var lastRequestEnd = store.lastRequestEnd; //returneaza al doilea index selected
								grid.getSelectionModel().selectRange(lastRequestStart, lastRequestEnd, false);
		
								// Do not select the rows already being in the grid._deselectedIndexes_ array
								var l = grid._deselectedIndexes_.length, i = 0;
								if (l > 0) {
									for (i;i<l;i++) {								
										grid.getSelectionModel().deselect(grid._deselectedIndexes_[i]);
									}
								}			
		
							}
						});
		
						grid.on("itemclick", function(view, record, item, index, e) {
		
							var x,id,y;					
		
							if (grid._selectedAllInvoices_ === true) {					
		
								// If the index is already in the _deselectedIndexes_ array then remove it, else add it to the array
		
								x = grid._deselectedIndexes_.indexOf(index);
								id = record.get("id");
								y = grid._deselectedIds_.indexOf(id);
		
								if (x > -1) {							
									grid._deselectedIndexes_.splice(x, 1);
								}
								else {
									grid._deselectedIndexes_.push(index);
								}
		
								if (y > -1) {							
									grid._deselectedIds_.splice(y, 1);
								}
								else {
									grid._deselectedIds_.push(id);
								}	
		
								dc.setParamValue("unselectedInvoices",grid._deselectedIds_.toString());
								
							} 
		
							else {	
						
								x = grid._selectedIndexes_.indexOf(index);
								id = record.get("id");
								y = grid._selectedIds_.indexOf(id);
		
								if (x > -1) {							
									grid._selectedIndexes_.splice(x, 1);
								}
								else {
									grid._selectedIndexes_.push(index);
								}
		
								if (y > -1) {							
									grid._selectedIds_.splice(y, 1);
								}
								else {
									grid._selectedIds_.push(id);
								}	
		
								dc.setParamValue("selectedInvoices",grid._selectedIds_.toString());
								
							}
		
						});
		
	}
	
	,_handleSelect_: function(grid) {
		
						var store = grid.getStore();
						var dc = this._getDc_("outgoingInvoiceBulkExportWizardInfiniteList");
						var ctx = this;
		
						// Disable select / deselect on cell click if the clicked cell is not the one containing the checkbox
						grid.view.on("beforecellclick", function(view, cell, cellIndex, r, td, rowIndex, e) {
							if (cellIndex > 0) {
								return false;
							}
							if (e.shiftKey === true) {
								return false;
							}	
						});
						
						// if the selected all is false then deselect all the records
						store.on("load", function() {
							if (grid._selectedAllInvoices_ === false) {
								grid.getSelectionModel().deselectAll();
							}
						}, this);
		
						// if one of the column names is pressed, the list is reordered and everything is unchecked
						grid.on("headerclick", function() {
							ctx._uncheckAllInvoices_();
						});
		
						grid.view.getEl().on("scroll", function() {
							if (grid._selectedAllInvoices_ === true) {
		
								var lastRequestStart = store.lastRequestStart; //returneaza primul index selectat
								var lastRequestEnd = store.lastRequestEnd; //returneaza al doilea index selected
								grid.getSelectionModel().selectRange(lastRequestStart, lastRequestEnd, false);
		
								// Do not select the rows already being in the grid._deselectedIndexes_ array
								var l = grid._deselectedIndexes_.length, i = 0;
								if (l > 0) {
									for (i;i<l;i++) {								
										grid.getSelectionModel().deselect(grid._deselectedIndexes_[i]);
									}
								}			
		
							}
						});
		
						grid.on("itemclick", function(view, record, item, index, e) {
		
							var x,id,y;					
		
							if (grid._selectedAllInvoices_ === true) {					
		
								// If the index is already in the _deselectedIndexes_ array then remove it, else add it to the array
		
								x = grid._deselectedIndexes_.indexOf(index);
								id = record.get("id");
								y = grid._deselectedIds_.indexOf(id);
		
								if (x > -1) {							
									grid._deselectedIndexes_.splice(x, 1);
								}
								else {
									grid._deselectedIndexes_.push(index);
								}
		
								if (y > -1) {							
									grid._deselectedIds_.splice(y, 1);
								}
								else {
									grid._deselectedIds_.push(id);
								}	
		
								dc.setParamValue("unselectedInvoices",grid._deselectedIds_.toString());
								
							} 
		
							else {	
						
								x = grid._selectedIndexes_.indexOf(index);
								id = record.get("id");
								y = grid._selectedIds_.indexOf(id);
		
								if (x > -1) {							
									grid._selectedIndexes_.splice(x, 1);
								}
								else {
									grid._selectedIndexes_.push(index);
								}
		
								if (y > -1) {							
									grid._selectedIds_.splice(y, 1);
								}
								else {
									grid._selectedIds_.push(id);
								}	
		
								dc.setParamValue("selectedInvoices",grid._selectedIds_.toString());
								
							}
		
						});
		
	}
	
	,openApproveWindow: function() {
		
						var label = "Please provide an approval reason.";
						var title = "Approve";
						this.setupPopUpWindow("wdwApproveNote","approveNote","btnSaveCloseApproveWdw","btnCancelApproveWdw" , label , title);				
	}
	
	,openRejectWindow: function() {
		
						var label = "Please provide a reason for rejection.";
						var title = "Reject";
						this.setupPopUpWindow("wdwRejectNote","rejectNote","btnSaveCloseRejectWdw","btnCancelRejectWdw" , label , title);				
	}
	
	,setupPopUpWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						
						remarksField.fieldLabel = label;
						remarksField.setValue("");
						window.title = title;
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
		
						
						window.show();
	}
	
	,useWorkflow: function() {
		
						return _SYSTEMPARAMETERS_.syscreditmemoapproval === "true";
	}
	
	,canApproveReject: function(dc) {
		
						var selectedRecords  = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; i++){
							var r = selectedRecords[i];
							if (r.data.approvalStatus != __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ || r.data.canBeCompleted == false) {
								return false;
							}
						}
						return true;
	}
	
	,approveRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwApproveNote").close();
		
								Main.info(responseData.params.approveRejectResult);
		
								//reload page
								dc.doReloadPage(); 
							}
						};
		
		                var dc = this._getDc_("invoice");
		                var t = {
					        name: "approveList",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcDataList(t);
	}
	
	,rejectRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwRejectNote").close();
								
								Main.info(responseData.params.approveRejectResult);
		
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("invoice");
		                var t = {
					        name: "rejectList",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcDataList(t);
	}
	
	,_sendAttachments_: function() {
		
						var list = this._get_("documentsAssignList");
						var view = list.getView();
						var selectedAttachments = [];
						var dc = this._getDc_("invoice");
						if (view.getSelectionModel().hasSelection()) {
						   var rows = view.getSelectionModel().getSelection();
						   Ext.each(rows, function(row) {
								selectedAttachments.push(row.data.id);
						   }, this);
						}
						dc.setParamValue("selectedAttachments",JSON.stringify(selectedAttachments));
	}
	
	,onApprovalWdwShow: function() {
		
						var dc = this._getDc_("attachment");
						dc.doQuery();
	}
	
	,_addNewAttachment_: function() {
		
						var dc = this._getDc_("attachment");
						dc.doNew({afterOpt:true});
	}
	
	,_deleteAttachment_: function() {
		
						var dc = this._getDc_("attachment");
						dc.doDelete();
	}
	
	,_inApprovalProcess_: function() {
		
						var inApproval = false;
						if (_SYSTEMPARAMETERS_.syscreditmemoapproval === "true"){
							var rec = this._getDc_("invoice").getRecord();
							if (rec){
								var approvalStatus = rec.get("approvalStatus");		
								var invoiceType = 	rec.get("invoiceType");
								if ( invoiceType === __ACC_TYPE__.InvoiceTypeAcc._CRN_ && approvalStatus == __CMM_TYPE__.BidApprovalStatus._AWAITING_APPROVAL_ ){
									inApproval = true;
								}
							}
						}
						return inApproval;
	}
	
	,initFeedback: function(response) {
		
						var responseData = Ext.getResponseDataInJSON(response);
						Main.info(responseData.params.approveRejectResult);
	}
});
