/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoiceLine_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "acc_OutgoingInvoiceLine_Ds"
	},
	
	
	fields: [
		{name:"fuelEvId", type:"int", allowNull:true},
		{name:"outInvId", type:"int", allowNull:true},
		{name:"outInvStatus", type:"string"},
		{name:"invoicetype", type:"string"},
		{name:"refInvId", type:"int", allowNull:true},
		{name:"custId", type:"int", allowNull:true},
		{name:"custCode", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"destId", type:"int", allowNull:true},
		{name:"destCode", type:"string"},
		{name:"shipToCode", type:"string"},
		{name:"shipToName", type:"string"},
		{name:"invLineId", type:"int", allowNull:true},
		{name:"deliveryDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"aircraftRegistrationNumber", type:"string"},
		{name:"flightSuffix", type:"string"},
		{name:"flightType", type:"string"},
		{name:"flightNo", type:"string"},
		{name:"ticketNo", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"amount", type:"float", allowNull:true},
		{name:"vat", type:"float", allowNull:true},
		{name:"isCredited", type:"boolean"},
		{name:"transmissionStatus", type:"string"},
		{name:"erpReference", type:"string"},
		{name:"bolNumber", type:"string"},
		{name:"flightID", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoiceLine_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"fuelEvId", type:"int", allowNull:true},
		{name:"outInvId", type:"int", allowNull:true},
		{name:"outInvStatus", type:"string"},
		{name:"invoicetype", type:"string"},
		{name:"refInvId", type:"int", allowNull:true},
		{name:"custId", type:"int", allowNull:true},
		{name:"custCode", type:"string"},
		{name:"iataCode", type:"string"},
		{name:"destId", type:"int", allowNull:true},
		{name:"destCode", type:"string"},
		{name:"shipToCode", type:"string"},
		{name:"shipToName", type:"string"},
		{name:"invLineId", type:"int", allowNull:true},
		{name:"deliveryDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"aircraftRegistrationNumber", type:"string"},
		{name:"flightSuffix", type:"string"},
		{name:"flightType", type:"string"},
		{name:"flightNo", type:"string"},
		{name:"ticketNo", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"amount", type:"float", allowNull:true},
		{name:"vat", type:"float", allowNull:true},
		{name:"isCredited", type:"boolean", allowNull:true},
		{name:"transmissionStatus", type:"string"},
		{name:"erpReference", type:"string"},
		{name:"bolNumber", type:"string"},
		{name:"flightID", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.acc.ui.extjs.ds.OutgoingInvoiceLine_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"creditMemoReason", type:"string"},
		{name:"generatedCreditMemoId", type:"int", allowNull:true},
		{name:"invoiceId", type:"int", allowNull:true},
		{name:"numberSeriesGeneratorMessage", type:"string"}
	]
});
