@ECHO OFF

:start

set /p version="Enter the build number:"

IF [%version%]==[] echo The version can not be empty
IF [%version%]==[] goto :start 

:soneversion
echo Please select sone verion:
echo 1 - SONE 1
echo 2 - SONE 2

set sone_version=1
set /p sone_version=Sone version (default - %sone_version%)? : 

IF "%sone_version%"=="1" GOTO :setsone1
GOTO :setsone2

set master_branch=master
set prod_branch=prod-v1

:setsone1

set master_branch=master
set prod_branch=prod-v1

GOTO :variablesaresets

:setsone2

set master_branch=multitenant-spring
set prod_branch=prod-v2

:variablesaresets

echo Source branch: %master_branch%
echo Production branch: %prod_branch%

call git checkout %master_branch%

call git pull origin %master_branch%

if %ERRORLEVEL% == 0 goto :checkout
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:checkout
call git checkout -b %prod_branch% origin/%prod_branch%

if %ERRORLEVEL% == 0 goto :pull
:checkoutv1

:checkoutv1

call git checkout -B %prod_branch%

if %ERRORLEVEL% == 0 goto :merge
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:pull
call git pull -s recursive -X theirs

if %ERRORLEVEL% == 0 goto :merge
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:merge
call git merge -X theirs %master_branch%

if %ERRORLEVEL% == 0 goto :masterpom
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:masterpom
call mvn versions:set -DnewVersion=%version% -N -DgenerateBackupPoms=false
if %ERRORLEVEL% == 0 goto :dbchangelog
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:dbchangelog

call java -jar ChangeProperty.jar %version% custom-sone/atraxo.custom.sone.dbm/src/main/resources/changelog/db-changelog.xml 

if %ERRORLEVEL% == 0 goto :customschangedbmmarketplace
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:customschangedbmmarketplace
call java -jar ChangeProperty.jar %version% custom-marketplace/atraxo.custom.marketplace.dbm/src/main/resources/changelog/db-changelog.xml 

if %ERRORLEVEL% == 0 goto :cleaninstall
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscrpt

:cleaninstall
call mvn clean install

if %ERRORLEVEL% == 0 goto :add
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscrpt

:add
call git add .

if %ERRORLEVEL% == 0 goto :commit
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:commit
call git commit -m "Release %version%"

if %ERRORLEVEL% == 0 goto :tag
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:tag
call git tag Release_%version%

if %ERRORLEVEL% == 0 goto :push
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:push
call git push -f origin %prod_branch%:%prod_branch%

if %ERRORLEVEL% == 0 goto :pushtag
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:pushtag
call git push origin Release_%version%

if %ERRORLEVEL% == 0 goto :endofscript
echo "Errors encountered during execution.  Exited with status: %errorlevel%"
goto :endofscript

:endofscript

@set /p pressedKey=Press any key to exit...