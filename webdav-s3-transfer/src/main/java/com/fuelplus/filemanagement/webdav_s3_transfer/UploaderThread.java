package com.fuelplus.filemanagement.webdav_s3_transfer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.ObjectTagging;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.Tag;
import com.github.sardine.DavResource;
import com.github.sardine.Sardine;

public class UploaderThread implements Runnable {

	private DavResource davFile;
	private AmazonS3 s3Client;
	private String bucketName;
	private Sardine webDavSardine;
	private String webDavUrl;
	private String environmentType;
	private String filePrefix;

	public UploaderThread(DavResource davFile, AmazonS3 s3Service, String bucketName, Sardine webDavSardine, String webDavUrl, String environmentType,
			String filePrefix) {
		this.davFile = davFile;
		this.s3Client = s3Service;
		this.bucketName = bucketName;
		this.webDavSardine = webDavSardine;
		this.webDavUrl = webDavUrl;
		this.environmentType = environmentType;
		this.filePrefix = filePrefix;
	}

	@Override
	public void run() {
		String[] davFilePathSplit = this.davFile.getPath().split("/");
		String dataType = davFilePathSplit[5]; // position in the path for the data type folder nomenclature
		List<Tag> tags = new ArrayList<>();
		String s3FileKey = "";
		switch (dataType) {
		case "Attachment":
			s3FileKey = this.getKeyForAttachmentFile(davFilePathSplit, tags);
			break;
		case "ExternalInterfaces":
			s3FileKey = this.getKeyForExternalInterfaceFile(davFilePathSplit, tags);
			break;
		case "OutgoingInvoice":
			s3FileKey = this.getKeyForOutgoingInvoiceFile(davFilePathSplit, tags);
			break;
		case "SSHStore":
			s3FileKey = this.getKeyForSSHStoreFile(davFilePathSplit, tags);
			break;
		}
		try {
			this.uploadToS3(this.filePrefix + s3FileKey, this.davFile, tags);
		} catch (InterruptedException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void uploadToS3(String s3FileKey, DavResource davFile, List<Tag> tags) throws InterruptedException, IOException {
		InputStream davFileContent = this.downloadFromWebDav(davFile);
		ObjectMetadata metadata = new ObjectMetadata();
		PutObjectRequest req = new PutObjectRequest(this.bucketName, s3FileKey, davFileContent, metadata);
		req.setTagging(new ObjectTagging(tags));
		try {
			this.s3Client.putObject(req);
		} catch (SdkClientException e) {
			System.out.println("Got error while uploading file " + s3FileKey);
			Thread.sleep(TimeUnit.SECONDS.toMillis(30));
			davFileContent = this.downloadFromWebDav(davFile);
			req = new PutObjectRequest(this.bucketName, s3FileKey, davFileContent, metadata);
			this.s3Client.putObject(req);
		}
	}

	private InputStream downloadFromWebDav(DavResource davFile) throws IOException {
		String requestPath = this.webDavUrl + davFile.getPath();
		return this.webDavSardine.get(requestPath);
	}

	private String getKeyForSSHStoreFile(String[] davFilePathSplit, List<Tag> tags) {
		tags.add(new Tag("dataType", davFilePathSplit[5]));
		tags.add(new Tag("fileName", davFilePathSplit[7]));
		tags.add(new Tag("environmentType", this.environmentType));
		return davFilePathSplit[6];
	}

	private String getKeyForOutgoingInvoiceFile(String[] davFilePathSplit, List<Tag> tags) {
		tags.add(new Tag("dataType", davFilePathSplit[5]));
		tags.add(new Tag("fileName", davFilePathSplit[7]));
		tags.add(new Tag("environmentType", this.environmentType));
		return davFilePathSplit[6];
	}

	private String getKeyForExternalInterfaceFile(String[] davFilePathSplit, List<Tag> tags) {
		Tag tag = new Tag("dataType", davFilePathSplit[5] + davFilePathSplit[7]);
		tags.add(new Tag("environmentType", this.environmentType));
		tags.add(tag);
		return davFilePathSplit[8];
	}

	private String getKeyForAttachmentFile(String[] davFilePathSplit, List<Tag> tags) {
		tags.add(new Tag("dataType", davFilePathSplit[5]));
		tags.add(new Tag("fileName", davFilePathSplit[7]));
		tags.add(new Tag("environmentType", this.environmentType));
		return davFilePathSplit[6];
	}

}