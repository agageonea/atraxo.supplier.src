package com.fuelplus.filemanagement.webdav_s3_transfer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.github.sardine.DavResource;
import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;

/**
 * Hello world!
 */
public class App {

	static Map<String, String> params = new HashMap<>();

	static AmazonS3 s3Client;

	static Sardine webDavSardine;
	static String webDavUrl;

	static String environmentType;
	static String clientId;

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException, InterruptedException {
		System.out.println("Starting Transfer");
		if (args.length < 2) {
			System.out.println("2 parameters representing the client code and environment type are requiered");
			return;
		}

		readParams();

		environmentType = args[1];
		String clientCode = args[0].toLowerCase();
		clientId = getClientId(clientCode).toLowerCase();
		if (clientId == null) {
			System.out.println("Unable to identify client with code:" + clientCode);
			return;
		}

		initiateS3Client();
		initiateWebDavClient();
		transferFiles();
		System.out.println("Transfer Ended");
	}

	private static void transferFiles() throws IOException, InterruptedException {
		List<String> paths = Arrays.asList(params.get("webdav.fileslocation").split(","));
		for (String path : paths) {
			String archivePrefix = "";
			if (path.contains("archive")) {
				archivePrefix = "archive/";
			}
			List<DavResource> webDavFileList = getWebDavFiles(webDavUrl + path + clientId.toUpperCase() + "/");
			Collections.sort(webDavFileList, (o1, o2) -> o2.getCreation().compareTo(o1.getCreation()));
			ExecutorService executor = Executors.newFixedThreadPool(8);

			for (DavResource davFile : webDavFileList) {
				Runnable uploader = new UploaderThread(davFile, s3Client, clientId, webDavSardine, webDavUrl, environmentType, archivePrefix);
				executor.execute(uploader);
			}
			executor.shutdown();
			while (!executor.isShutdown()) {
			}

		}
	}

	private static List<DavResource> getWebDavFiles(String string) throws IOException {
		System.out.println("Getting files from folder " + string);
		List<DavResource> fileList = webDavSardine.list(string, 1);
		List<DavResource> returnedList = new ArrayList<>();
		fileList.remove(0);

		for (DavResource resource : fileList) {
			if (resource.isDirectory()) {
				returnedList.addAll(getWebDavFiles(params.get("webdav.url") + resource.getPath()));
			} else {
				if (fileList.size() > 1 && resource.getPath().split("/")[5].equals("OutgoingInvoice")) {
					Collections.sort(fileList, (o1, o2) -> o2.getCreation().compareTo(o1.getCreation()));
					returnedList.add(fileList.get(0));
					break;
				}
				returnedList.add(resource);
			}
		}
		return returnedList;

	}

	private static void initiateWebDavClient() {
		webDavUrl = params.get("webdav.url");
		webDavSardine = SardineFactory.begin(params.get("webdav.user"), params.get("webdav.password"));
	}

	private static void initiateS3Client() {
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(params.get("aws.s3.access_key_id"), params.get("aws.s3.access_key_secret"));
		s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.fromName(params.get("aws.s3.region")))
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
		if (!s3Client.doesBucketExistV2(clientId)) {
			s3Client.createBucket(clientId);
		}
	}

	private static String getClientId(String clientCode) throws ClassNotFoundException, SQLException {
		Class.forName(params.get("jdbc.driverClassName"));
		Connection conn = DriverManager.getConnection(params.get("jdbc.url"), params.get("jdbc.username"), params.get("jdbc.password"));
		Statement stmt = conn.createStatement();
		String query = "select id from sys_client where `code` = '" + clientCode.toUpperCase() + "'";
		ResultSet rs = stmt.executeQuery(query);
		String clientId = null;
		if (rs.getFetchSize() != 1) {
			rs.next();
			clientId = rs.getString(1);
		}
		conn.close();
		return clientId;
	}

	private static void readParams() throws IOException {
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader("src/resources/webdavToS3.prop"));
		} catch (FileNotFoundException e) {
			System.out.println("Properties file \"webdavToS3.prop\" not found");
			throw new RuntimeException(e);
		}
		String line;
		while (in.ready()) {
			line = in.readLine();
			String[] splitLine = line.split("=");
			params.put(splitLine[0], splitLine[1]);
		}
		in.close();
	}
}
