/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.etender.business.impl.message;

import atraxo.etender.business.api.message.IETenderMessageService;
import atraxo.etender.domain.impl.message.ETenderMessage;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link ETenderMessage} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class ETenderMessage_Service
		extends
			AbstractEntityService<ETenderMessage>
		implements
			IETenderMessageService {

	/**
	 * Public constructor for ETenderMessage_Service
	 */
	public ETenderMessage_Service() {
		super();
	}

	/**
	 * Public constructor for ETenderMessage_Service
	 * 
	 * @param em
	 */
	public ETenderMessage_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<ETenderMessage> getEntityClass() {
		return ETenderMessage.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return ETenderMessage
	 */
	public ETenderMessage findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(ETenderMessage.NQ_FIND_BY_BUSINESS,
							ETenderMessage.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"ETenderMessage", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"ETenderMessage", "id"), nure);
		}
	}

}
