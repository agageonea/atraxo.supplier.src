/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.etender.business.api.message;

import atraxo.etender.domain.impl.message.ETenderMessage;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link ETenderMessage} domain entity.
 */
public interface IETenderMessageService extends IEntityService<ETenderMessage> {

	/**
	 * Find by unique key
	 *
	 * @return ETenderMessage
	 */
	public ETenderMessage findByBusiness(Integer id);
}
