/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.etender.domain.impl.etender_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ETenderMessageType} enum.
 * Generated code. Do not modify in this file.
 */
public class ETenderMessageTypeConverter
		implements
			AttributeConverter<ETenderMessageType, String> {

	@Override
	public String convertToDatabaseColumn(ETenderMessageType value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null ETenderMessageType.");
		}
		return value.getName();
	}

	@Override
	public ETenderMessageType convertToEntityAttribute(String value) {
		return ETenderMessageType.getByName(value);
	}

}
