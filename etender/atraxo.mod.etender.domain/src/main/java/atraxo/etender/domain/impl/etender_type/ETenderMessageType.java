/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.etender.domain.impl.etender_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ETenderMessageType {

	_EMPTY_(""), _TENDER_INVITATION_("Tender Invitation"), _BID_("Bid"), _NOBID_(
			"NoBid"), _AWARD_("Award"), _ACCEPTAWARD_("AcceptAward");

	private String name;

	private ETenderMessageType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ETenderMessageType getByName(String name) {
		for (ETenderMessageType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent ETenderMessageType with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
