/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.etender.domain.impl.message;

import atraxo.etender.domain.impl.etender_type.ETenderMessageType;
import atraxo.etender.domain.impl.etender_type.ETenderMessageTypeConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link ETenderMessage} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = ETenderMessage.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM ETenderMessage e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = ETenderMessage.TABLE_NAME)
public class ETenderMessage extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "et_MESSAGE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "ETenderMessage.findByBusiness";

	@Column(name = "message_type", length = 32)
	@Convert(converter = ETenderMessageTypeConverter.class)
	private ETenderMessageType messageType;

	public ETenderMessageType getMessageType() {
		return this.messageType;
	}

	public void setMessageType(ETenderMessageType messageType) {
		this.messageType = messageType;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
