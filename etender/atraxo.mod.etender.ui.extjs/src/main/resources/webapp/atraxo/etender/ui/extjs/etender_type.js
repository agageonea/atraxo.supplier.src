/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
__ETENDER_TYPE__ = {
	ETenderMessageType : {
		_TENDER_INVITATION_ : "Tender Invitation" , 
		_BID_ : "Bid" , 
		_NOBID_ : "NoBid" , 
		_AWARD_ : "Award" , 
		_ACCEPTAWARD_ : "AcceptAward" 
	}
}
