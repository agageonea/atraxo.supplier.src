package atraxo.ops.presenter.ext.fuelTicket.delegate;

import java.util.ArrayList;
import java.util.List;

import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatus;
import atraxo.ops.presenter.impl.fuelTicket.model.ArchivedFuelTicket_Ds;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class FuelTicketArchived_Pd extends AbstractPresenterDelegate {

	public void undo(List<ArchivedFuelTicket_Ds> fuelTicketDsList) throws Exception {

		List<FuelTicket> fuelTicketsList = new ArrayList<>();
		IFuelTicketService service = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		for (ArchivedFuelTicket_Ds ds : fuelTicketDsList) {
			fuelTicketsList.add(service.findById(ds.getId()));
		}
		for (FuelTicket ticket : fuelTicketsList) {
			if (ticket.getTicketStatus().equals(FuelTicketStatus._REISSUE_)) {
				throw new BusinessException(OpsErrorCode.FUEL_TICKET_CAN_NOT_BE_UNDONE, OpsErrorCode.FUEL_TICKET_CAN_NOT_BE_UNDONE.getErrMsg());
			}
		}
		service.setFlagDelete(fuelTicketsList, false);

	}
}
