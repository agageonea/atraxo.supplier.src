/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelOrder.model;

import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelOrder.class, sort = {@SortField(field = FuelOrderLov_Ds.F_CODE)})
public class FuelOrderLov_Ds extends AbstractDs_Ds<FuelOrder> {

	public static final String ALIAS = "ops_FuelOrderLov_Ds";

	public static final String F_CODE = "code";

	@DsField
	private String code;

	/**
	 * Default constructor
	 */
	public FuelOrderLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelOrderLov_Ds(FuelOrder e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
