package atraxo.ops.presenter.ext.fuelOrder.delegate;

import java.util.List;

import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.ops.business.api.fuelOrder.IFuelOrderService;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.ops_type.FuelOrderStatus;
import atraxo.ops.presenter.impl.fuelOrder.model.FuelOrder_Ds;
import atraxo.ops.presenter.impl.fuelOrder.model.FuelOrder_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class UpdateStatus_Pd extends AbstractPresenterDelegate {

	public void updateStatus(List<FuelOrder_Ds> list, FuelOrder_DsParam param) throws Exception {
		for (FuelOrder_Ds ds : list) {
			this.updateStatus(ds, param);
		}
	}

	public void updateStatus(FuelOrder_Ds ds, FuelOrder_DsParam param) throws Exception {
		IFuelOrderService service = (IFuelOrderService) this.findEntityService(FuelOrder.class);
		FuelOrder fuelOrder = service.findById(ds.getId());
		if (param.getAction().equalsIgnoreCase(FuelOrderStatus._CANCELED_.getName())) {
			service.cancel(fuelOrder, param.getRemarks());
		} else {
			service.updateStatus(fuelOrder, param.getAction(), param.getRemarks(), param.getParamStatus());
		}
	}

	public void generateReport(FuelOrder_Ds ds, FuelOrder_DsParam param) throws Exception {
		IExternalReportService reportService = (IExternalReportService) this.findEntityService(ExternalReport.class);
		reportService.runReport(ds, "Fuel Order Release Report", "PDF", ds.getCode());
	}
}
