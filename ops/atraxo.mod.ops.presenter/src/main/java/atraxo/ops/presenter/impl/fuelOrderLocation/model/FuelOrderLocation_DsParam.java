/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelOrderLocation.model;

/**
 * Generated code. Do not modify in this file.
 */
public class FuelOrderLocation_DsParam {

	public static final String f_fuelOrderLocationId = "fuelOrderLocationId";
	public static final String f_remarks = "remarks";
	public static final String f_newContractId = "newContractId";
	public static final String f_fuelOrderStatus = "fuelOrderStatus";

	private Integer fuelOrderLocationId;

	private String remarks;

	private Integer newContractId;

	private String fuelOrderStatus;

	public Integer getFuelOrderLocationId() {
		return this.fuelOrderLocationId;
	}

	public void setFuelOrderLocationId(Integer fuelOrderLocationId) {
		this.fuelOrderLocationId = fuelOrderLocationId;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getNewContractId() {
		return this.newContractId;
	}

	public void setNewContractId(Integer newContractId) {
		this.newContractId = newContractId;
	}

	public String getFuelOrderStatus() {
		return this.fuelOrderStatus;
	}

	public void setFuelOrderStatus(String fuelOrderStatus) {
		this.fuelOrderStatus = fuelOrderStatus;
	}
}
