/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuotation.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FuelQuoteCycle;
import atraxo.ops.domain.impl.ops_type.FuelQuoteScope;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatus;
import atraxo.ops.domain.impl.ops_type.FuelQuoteType;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelQuotation.class, sort = {@SortField(field = FuelQuotation_Ds.F_POSTEDON, desc = true)})
@RefLookups({
		@RefLookup(refId = FuelQuotation_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuotation_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = FuelQuotation_Ds.F_FUELORDERID, namedQuery = FuelOrder.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuotation_Ds.F_FUELORDERCODE)}),
		@RefLookup(refId = FuelQuotation_Ds.F_FUELREQUESTID, namedQuery = FuelRequest.NQ_FIND_BY_CODE, params = {@Param(name = "requestCode", field = FuelQuotation_Ds.F_FUELREQUESTCODE)}),
		@RefLookup(refId = FuelQuotation_Ds.F_SUBSIDIARYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuotation_Ds.F_SUBSIDIARYCODE)})})
public class FuelQuotation_Ds extends AbstractDs_Ds<FuelQuotation> {

	public static final String ALIAS = "ops_FuelQuotation_Ds";

	public static final String F_SEPARATOR = "separator";
	public static final String F_CREDITTERMS = "creditTerms";
	public static final String F_CREDITLIMIT = "creditLimit";
	public static final String F_CREDITCURRENCY = "creditCurrency";
	public static final String F_CREDITCARD = "creditCard";
	public static final String F_EXPOSURE = "exposure";
	public static final String F_PERCENT = "percent";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CUSTOMERENTITYALIAS = "customerEntityAlias";
	public static final String F_CUSTOMERREFID = "customerRefId";
	public static final String F_FUELREQUESTID = "fuelRequestId";
	public static final String F_FUELREQUESTCODE = "fuelRequestCode";
	public static final String F_CODE = "code";
	public static final String F_TYPE = "type";
	public static final String F_SCOPE = "scope";
	public static final String F_STATUS = "status";
	public static final String F_POSTEDON = "postedOn";
	public static final String F_VALIDUNTIL = "validUntil";
	public static final String F_PRICINGCYCLE = "pricingCycle";
	public static final String F_OPENRELEASE = "openRelease";
	public static final String F_OWNER = "owner";
	public static final String F_DISCLAIMER = "disclaimer";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_AVGMTHDCODE = "avgMthdCode";
	public static final String F_FUELORDERID = "fuelOrderId";
	public static final String F_FUELORDERCODE = "fuelOrderCode";
	public static final String F_FINSRCID = "finSrcId";
	public static final String F_FINSRCCODE = "finSrcCode";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";
	public static final String F_SUBSIDIARYNAME = "subsidiaryName";
	public static final String F_PAYMENTTERM = "paymentTerm";
	public static final String F_REFERENCETO = "referenceTo";
	public static final String F_INVFREQ = "invFreq";
	public static final String F_PAYCURR = "payCurr";
	public static final String F_PAYCURRID = "payCurrId";
	public static final String F_PERIOD = "period";
	public static final String F_PRIMARYCLIENTID = "primaryClientId";
	public static final String F_PRIMARYCLIENTNAME = "primaryClientName";
	public static final String F_ALERTLIMIT = "alertLimit";

	@DsField(fetch = false)
	private String separator;

	@DsField(fetch = false)
	private String creditTerms;

	@DsField(fetch = false)
	private BigDecimal creditLimit;

	@DsField(fetch = false)
	private String creditCurrency;

	@DsField(fetch = false)
	private Boolean creditCard;

	@DsField(fetch = false)
	private BigDecimal exposure;

	@DsField(fetch = false)
	private String percent;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", fetch = false, path = "customer.entityAlias")
	private String customerEntityAlias;

	@DsField(join = "left", path = "customer.refid")
	private String customerRefId;

	@DsField(join = "left", path = "fuelRequest.id")
	private Integer fuelRequestId;

	@DsField(join = "left", path = "fuelRequest.requestCode")
	private String fuelRequestCode;

	@DsField
	private String code;

	@DsField
	private FuelQuoteType type;

	@DsField
	private FuelQuoteScope scope;

	@DsField
	private FuelQuoteStatus status;

	@DsField
	private Date postedOn;

	@DsField
	private Date validUntil;

	@DsField
	private FuelQuoteCycle pricingCycle;

	@DsField
	private Boolean openRelease;

	@DsField
	private String owner;

	@DsField
	private String disclaimer;

	@DsField(join = "left", path = "customer.masterAgreement.averageMethod.id")
	private Integer avgMthdId;

	@DsField(join = "left", path = "customer.masterAgreement.averageMethod.code")
	private String avgMthdCode;

	@DsField(join = "left", path = "fuelOrder.id")
	private Integer fuelOrderId;

	@DsField(join = "left", path = "fuelOrder.code")
	private String fuelOrderCode;

	@DsField(join = "left", path = "customer.masterAgreement.financialsource.id")
	private Integer finSrcId;

	@DsField(join = "left", path = "customer.masterAgreement.financialsource.code")
	private String finSrcCode;

	@DsField(join = "left", path = "holder.id")
	private Integer subsidiaryId;

	@DsField(join = "left", path = "holder.code")
	private String subsidiaryCode;

	@DsField(join = "left", path = "holder.name")
	private String subsidiaryName;

	@DsField(fetch = false)
	private Integer paymentTerm;

	@DsField(fetch = false)
	private String referenceTo;

	@DsField(fetch = false)
	private String invFreq;

	@DsField(fetch = false)
	private String payCurr;

	@DsField(fetch = false)
	private Integer payCurrId;

	@DsField(fetch = false)
	private String period;

	@DsField(fetch = false)
	private Integer primaryClientId;

	@DsField(fetch = false)
	private String primaryClientName;

	@DsField(fetch = false)
	private Integer alertLimit;

	/**
	 * Default constructor
	 */
	public FuelQuotation_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelQuotation_Ds(FuelQuotation e) {
		super(e);
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public BigDecimal getCreditLimit() {
		return this.creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCreditCurrency() {
		return this.creditCurrency;
	}

	public void setCreditCurrency(String creditCurrency) {
		this.creditCurrency = creditCurrency;
	}

	public Boolean getCreditCard() {
		return this.creditCard;
	}

	public void setCreditCard(Boolean creditCard) {
		this.creditCard = creditCard;
	}

	public BigDecimal getExposure() {
		return this.exposure;
	}

	public void setExposure(BigDecimal exposure) {
		this.exposure = exposure;
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerEntityAlias() {
		return this.customerEntityAlias;
	}

	public void setCustomerEntityAlias(String customerEntityAlias) {
		this.customerEntityAlias = customerEntityAlias;
	}

	public String getCustomerRefId() {
		return this.customerRefId;
	}

	public void setCustomerRefId(String customerRefId) {
		this.customerRefId = customerRefId;
	}

	public Integer getFuelRequestId() {
		return this.fuelRequestId;
	}

	public void setFuelRequestId(Integer fuelRequestId) {
		this.fuelRequestId = fuelRequestId;
	}

	public String getFuelRequestCode() {
		return this.fuelRequestCode;
	}

	public void setFuelRequestCode(String fuelRequestCode) {
		this.fuelRequestCode = fuelRequestCode;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public FuelQuoteType getType() {
		return this.type;
	}

	public void setType(FuelQuoteType type) {
		this.type = type;
	}

	public FuelQuoteScope getScope() {
		return this.scope;
	}

	public void setScope(FuelQuoteScope scope) {
		this.scope = scope;
	}

	public FuelQuoteStatus getStatus() {
		return this.status;
	}

	public void setStatus(FuelQuoteStatus status) {
		this.status = status;
	}

	public Date getPostedOn() {
		return this.postedOn;
	}

	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	public Date getValidUntil() {
		return this.validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public FuelQuoteCycle getPricingCycle() {
		return this.pricingCycle;
	}

	public void setPricingCycle(FuelQuoteCycle pricingCycle) {
		this.pricingCycle = pricingCycle;
	}

	public Boolean getOpenRelease() {
		return this.openRelease;
	}

	public void setOpenRelease(Boolean openRelease) {
		this.openRelease = openRelease;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getDisclaimer() {
		return this.disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public String getAvgMthdCode() {
		return this.avgMthdCode;
	}

	public void setAvgMthdCode(String avgMthdCode) {
		this.avgMthdCode = avgMthdCode;
	}

	public Integer getFuelOrderId() {
		return this.fuelOrderId;
	}

	public void setFuelOrderId(Integer fuelOrderId) {
		this.fuelOrderId = fuelOrderId;
	}

	public String getFuelOrderCode() {
		return this.fuelOrderCode;
	}

	public void setFuelOrderCode(String fuelOrderCode) {
		this.fuelOrderCode = fuelOrderCode;
	}

	public Integer getFinSrcId() {
		return this.finSrcId;
	}

	public void setFinSrcId(Integer finSrcId) {
		this.finSrcId = finSrcId;
	}

	public String getFinSrcCode() {
		return this.finSrcCode;
	}

	public void setFinSrcCode(String finSrcCode) {
		this.finSrcCode = finSrcCode;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public String getSubsidiaryName() {
		return this.subsidiaryName;
	}

	public void setSubsidiaryName(String subsidiaryName) {
		this.subsidiaryName = subsidiaryName;
	}

	public Integer getPaymentTerm() {
		return this.paymentTerm;
	}

	public void setPaymentTerm(Integer paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getReferenceTo() {
		return this.referenceTo;
	}

	public void setReferenceTo(String referenceTo) {
		this.referenceTo = referenceTo;
	}

	public String getInvFreq() {
		return this.invFreq;
	}

	public void setInvFreq(String invFreq) {
		this.invFreq = invFreq;
	}

	public String getPayCurr() {
		return this.payCurr;
	}

	public void setPayCurr(String payCurr) {
		this.payCurr = payCurr;
	}

	public Integer getPayCurrId() {
		return this.payCurrId;
	}

	public void setPayCurrId(Integer payCurrId) {
		this.payCurrId = payCurrId;
	}

	public String getPeriod() {
		return this.period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Integer getPrimaryClientId() {
		return this.primaryClientId;
	}

	public void setPrimaryClientId(Integer primaryClientId) {
		this.primaryClientId = primaryClientId;
	}

	public String getPrimaryClientName() {
		return this.primaryClientName;
	}

	public void setPrimaryClientName(String primaryClientName) {
		this.primaryClientName = primaryClientName;
	}

	public Integer getAlertLimit() {
		return this.alertLimit;
	}

	public void setAlertLimit(Integer alertLimit) {
		this.alertLimit = alertLimit;
	}
}
