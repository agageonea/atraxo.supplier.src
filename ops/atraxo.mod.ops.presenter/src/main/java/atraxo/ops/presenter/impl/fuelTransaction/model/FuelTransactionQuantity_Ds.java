/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelTransaction.model;

import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import atraxo.ops.domain.impl.ops_type.FuelQuantityType;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelTransactionQuantity.class)
@RefLookups({@RefLookup(refId = FuelTransactionQuantity_Ds.F_FUELTRANSID)})
public class FuelTransactionQuantity_Ds
		extends
			AbstractDs_Ds<FuelTransactionQuantity> {

	public static final String ALIAS = "ops_FuelTransactionQuantity_Ds";

	public static final String F_FUELTRANSID = "fuelTransId";
	public static final String F_FUELQUANTITY = "fuelQuantity";
	public static final String F_QUANTITYUOM = "quantityUOM";
	public static final String F_QUANTITYTYPE = "quantityType";

	@DsField(join = "left", path = "fuelTransaction.id")
	private Integer fuelTransId;

	@DsField
	private BigDecimal fuelQuantity;

	@DsField
	private String quantityUOM;

	@DsField
	private FuelQuantityType quantityType;

	/**
	 * Default constructor
	 */
	public FuelTransactionQuantity_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelTransactionQuantity_Ds(FuelTransactionQuantity e) {
		super(e);
	}

	public Integer getFuelTransId() {
		return this.fuelTransId;
	}

	public void setFuelTransId(Integer fuelTransId) {
		this.fuelTransId = fuelTransId;
	}

	public BigDecimal getFuelQuantity() {
		return this.fuelQuantity;
	}

	public void setFuelQuantity(BigDecimal fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}

	public String getQuantityUOM() {
		return this.quantityUOM;
	}

	public void setQuantityUOM(String quantityUOM) {
		this.quantityUOM = quantityUOM;
	}

	public FuelQuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(FuelQuantityType quantityType) {
		this.quantityType = quantityType;
	}
}
