/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelTransaction.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import atraxo.ops.domain.impl.ops_type.FuelQuantityType;
import atraxo.ops.presenter.impl.fuelTransaction.model.FuelTransactionQuantity_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

/**
 * Custom Service FuelTransactionQuantity_DsService
 */
public class FuelTransactionQuantity_DsService
		extends AbstractEntityDsService<FuelTransactionQuantity_Ds, FuelTransactionQuantity_Ds, Object, FuelTransactionQuantity>
		implements IDsService<FuelTransactionQuantity_Ds, FuelTransactionQuantity_Ds, Object> {

	private final static Logger LOG = LoggerFactory.getLogger(FuelTransactionQuantity_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<FuelTransactionQuantity_Ds, FuelTransactionQuantity_Ds, Object> builder,
			List<FuelTransactionQuantity_Ds> result) throws Exception {
		super.postFind(builder, result);
		Collections.sort(result, new FuelTransactionQuantityComparator((IUnitService) this.findEntityService(Unit.class)));
	}

	private class FuelTransactionQuantityComparator implements Comparator<FuelTransactionQuantity_Ds> {

		private IUnitService unitService;

		/**
		 * @param unitService
		 */
		public FuelTransactionQuantityComparator(IUnitService unitService) {
			this.unitService = unitService;
		}

		@Override
		public int compare(FuelTransactionQuantity_Ds o1, FuelTransactionQuantity_Ds o2) {
			if (FuelQuantityType._GROSS_.equals(o1.getQuantityType()) && FuelQuantityType._NET_.equals(o2.getQuantityType())) {
				return -1;
			} else if (FuelQuantityType._NET_.equals(o1.getQuantityType()) && FuelQuantityType._GROSS_.equals(o2.getQuantityType())) {
				return 1;
			} else {
				try {
					Unit unit1 = this.unitService.findByCode(o1.getQuantityUOM());
					Unit unit2 = this.unitService.findByCode(o2.getQuantityUOM());
					if (UnitType._VOLUME_.equals(unit1.getUnittypeInd()) && UnitType._MASS_.equals(unit2.getUnittypeInd())) {
						return -1;
					} else if (UnitType._MASS_.equals(unit1.getUnittypeInd()) && UnitType._MASS_.equals(unit2.getUnittypeInd())) {
						return 1;
					}
				} catch (ApplicationException nre) {
					if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
						throw nre;
					}
					LOG.warn("Invalid fuel tranzaction. Quantity UOM is not set.", nre);
					// no unit of measure is set
					return 0;
				}
			}
			return 0;
		}

	}
}
