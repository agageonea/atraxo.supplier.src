/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuoteLocation.model;

import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = PricePolicy.class, sort = {@SortField(field = PricePolicies_Ds.F_POLICYNAME)})
@RefLookups({@RefLookup(refId = PricePolicies_Ds.F_LOCATIONID),
		@RefLookup(refId = PricePolicies_Ds.F_CUSTOMERID),
		@RefLookup(refId = PricePolicies_Ds.F_AIRCRAFTTYPEID)})
public class PricePolicies_Ds extends AbstractDs_Ds<PricePolicy> {

	public static final String ALIAS = "ops_PricePolicies_Ds";

	public static final String F_UNIID = "uniId";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_POLICYNAME = "policyName";
	public static final String F_ACTIVE = "active";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_DEFAULTMARGIN = "defaultMargin";
	public static final String F_DEFAULTMARKUP = "defaultMarkup";
	public static final String F_LOCATIONID = "locationId";
	public static final String F_LOCATIONCODE = "locationCode";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_AIRCRAFTTYPEID = "aircraftTypeId";
	public static final String F_AIRCRAFTTYPE = "aircraftType";
	public static final String F_POLICYTYPE = "policyType";
	public static final String F_VALUE = "value";
	public static final String F_POLICYUNIT = "policyUnit";
	public static final String F_MARKUP = "markup";
	public static final String F_SELLINGFUELPRICE = "sellingFuelPrice";
	public static final String F_DIFFERENTIAL = "differential";
	public static final String F_TOTALSELLINGPRICE = "totalSellingPrice";
	public static final String F_GRIDPRODUCTPRICE = "gridProductPrice";
	public static final String F_GRIDDFT = "gridDft";
	public static final String F_SELLINGUNIT = "sellingUnit";
	public static final String F_PRICEPOLICYID = "pricePolicyId";

	@DsField(join = "left", path = "unit.id")
	private Integer uniId;

	@DsField(join = "left", path = "currency.id")
	private Integer currencyId;

	@DsField(path = "name")
	private String policyName;

	@DsField
	private Boolean active;

	@DsField
	private Date validFrom;

	@DsField
	private Date validTo;

	@DsField
	private BigDecimal defaultMargin;

	@DsField
	private BigDecimal defaultMarkup;

	@DsField(join = "left", path = "location.id")
	private Integer locationId;

	@DsField(join = "left", path = "location.code")
	private String locationCode;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "aircraft.id")
	private Integer aircraftTypeId;

	@DsField(join = "left", path = "aircraft.code")
	private String aircraftType;

	@DsField(fetch = false)
	private String policyType;

	@DsField(fetch = false)
	private BigDecimal value;

	@DsField(fetch = false)
	private String policyUnit;

	@DsField(fetch = false)
	private BigDecimal markup;

	@DsField(fetch = false)
	private BigDecimal sellingFuelPrice;

	@DsField(fetch = false)
	private BigDecimal differential;

	@DsField(fetch = false)
	private BigDecimal totalSellingPrice;

	@DsField(fetch = false)
	private BigDecimal gridProductPrice;

	@DsField(fetch = false)
	private BigDecimal gridDft;

	@DsField(fetch = false)
	private String sellingUnit;

	@DsField(fetch = false)
	private Integer pricePolicyId;

	/**
	 * Default constructor
	 */
	public PricePolicies_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PricePolicies_Ds(PricePolicy e) {
		super(e);
	}

	public Integer getUniId() {
		return this.uniId;
	}

	public void setUniId(Integer uniId) {
		this.uniId = uniId;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getPolicyName() {
		return this.policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getDefaultMargin() {
		return this.defaultMargin;
	}

	public void setDefaultMargin(BigDecimal defaultMargin) {
		this.defaultMargin = defaultMargin;
	}

	public BigDecimal getDefaultMarkup() {
		return this.defaultMarkup;
	}

	public void setDefaultMarkup(BigDecimal defaultMarkup) {
		this.defaultMarkup = defaultMarkup;
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getAircraftTypeId() {
		return this.aircraftTypeId;
	}

	public void setAircraftTypeId(Integer aircraftTypeId) {
		this.aircraftTypeId = aircraftTypeId;
	}

	public String getAircraftType() {
		return this.aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getPolicyType() {
		return this.policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getPolicyUnit() {
		return this.policyUnit;
	}

	public void setPolicyUnit(String policyUnit) {
		this.policyUnit = policyUnit;
	}

	public BigDecimal getMarkup() {
		return this.markup;
	}

	public void setMarkup(BigDecimal markup) {
		this.markup = markup;
	}

	public BigDecimal getSellingFuelPrice() {
		return this.sellingFuelPrice;
	}

	public void setSellingFuelPrice(BigDecimal sellingFuelPrice) {
		this.sellingFuelPrice = sellingFuelPrice;
	}

	public BigDecimal getDifferential() {
		return this.differential;
	}

	public void setDifferential(BigDecimal differential) {
		this.differential = differential;
	}

	public BigDecimal getTotalSellingPrice() {
		return this.totalSellingPrice;
	}

	public void setTotalSellingPrice(BigDecimal totalSellingPrice) {
		this.totalSellingPrice = totalSellingPrice;
	}

	public BigDecimal getGridProductPrice() {
		return this.gridProductPrice;
	}

	public void setGridProductPrice(BigDecimal gridProductPrice) {
		this.gridProductPrice = gridProductPrice;
	}

	public BigDecimal getGridDft() {
		return this.gridDft;
	}

	public void setGridDft(BigDecimal gridDft) {
		this.gridDft = gridDft;
	}

	public String getSellingUnit() {
		return this.sellingUnit;
	}

	public void setSellingUnit(String sellingUnit) {
		this.sellingUnit = sellingUnit;
	}

	public Integer getPricePolicyId() {
		return this.pricePolicyId;
	}

	public void setPricePolicyId(Integer pricePolicyId) {
		this.pricePolicyId = pricePolicyId;
	}
}
