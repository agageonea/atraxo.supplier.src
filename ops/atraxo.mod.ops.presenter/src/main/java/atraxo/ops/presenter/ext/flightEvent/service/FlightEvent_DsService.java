/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.flightEvent.service;

import java.util.List;

import atraxo.ops.domain.ext.flightEvent.ProvidedQuantityInforamtionResult;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.presenter.impl.flightEvent.model.FlightEvent_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FlightEvent_DsService extends AbstractEntityDsService<FlightEvent_Ds, FlightEvent_Ds, Object, FlightEvent> implements
		IDsService<FlightEvent_Ds, FlightEvent_Ds, Object> {

	@Override
	protected void postInsert(List<FlightEvent_Ds> list, Object params) throws Exception {
		super.postInsert(list, params);
		this.collectToleranceResult(list);
	}

	@Override
	protected void postUpdate(List<FlightEvent_Ds> list, Object params) throws Exception {
		super.postUpdate(list, params);
		this.collectToleranceResult(list);
	}

	private void collectToleranceResult(List<FlightEvent_Ds> list) {
		for (FlightEvent_Ds ds : list) {
			if (ProvidedQuantityInforamtionResult.resultMap.get() != null) {
				if (ProvidedQuantityInforamtionResult.resultMap.get().containsKey(0)) {
					ds.setToleranceMessage(ProvidedQuantityInforamtionResult.resultMap.get().remove(0));
				} else {
					ds.setToleranceMessage(ProvidedQuantityInforamtionResult.resultMap.get().remove(ds.getId()));
				}
			}
		}
	}
}
