/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelTicket.model;

import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperation;
import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketCustomsStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketSource;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketTransmissionStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelTicket.class)
@RefLookups({
		@RefLookup(refId = FuelTicketMobile_Ds.F_DEPID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_DEPARTURE)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_CUSTID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_CUSTOMER)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_UPLIFTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_UPLIFTUNIT)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_SUPPID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_SUPPLIER)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_FUELLERID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_FUELLER)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_DESTID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_DESTINATION)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_FINALDESTID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_FINALDESTINATION)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_NETUPLIFTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_NETUPLIFTUNIT)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_BEFOREFUELINGUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_BEFOREFUELINGUNIT)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_AFTERFUELINGUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_AFTERFUELINGUNIT)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_DENSITYUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_DENSITYMASSUNIT)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_DENSITYVOLUMEID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_DENSITYVOLUMEUNIT)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_ACTYPEID, namedQuery = AcTypes.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicketMobile_Ds.F_AIRCRAFTTYPE)}),
		@RefLookup(refId = FuelTicketMobile_Ds.F_AIRCRAFTID, namedQuery = Aircraft.NQ_FIND_BY_REG_PRIMITIVE, params = {@Param(name = "customerId", field = FuelTicketMobile_Ds.F_REGISTRATION)})})
public class FuelTicketMobile_Ds extends AbstractSubsidiaryDs_Ds<FuelTicket> {

	public static final String ALIAS = "ops_FuelTicketMobile_Ds";

	public static final String F_DEPID = "depId";
	public static final String F_DEPARTURE = "departure";
	public static final String F_CUSTID = "custId";
	public static final String F_CUSTOMER = "customer";
	public static final String F_UPLIFTUNITID = "upliftUnitId";
	public static final String F_UPLIFTUNIT = "upliftUnit";
	public static final String F_TICKETNUMBER = "ticketNumber";
	public static final String F_DELIVERY = "delivery";
	public static final String F_UPLIFTVOLUME = "upliftVolume";
	public static final String F_FLIGHTNUMBER = "flightNumber";
	public static final String F_AIRLINEDESIGNATOR = "airlineDesignator";
	public static final String F_SUPPID = "suppId";
	public static final String F_SUPPLIER = "supplier";
	public static final String F_FUELLERID = "fuellerId";
	public static final String F_FUELLER = "fueller";
	public static final String F_DESTID = "destId";
	public static final String F_DESTINATION = "destination";
	public static final String F_AIRCRAFTID = "aircraftId";
	public static final String F_REGISTRATION = "registration";
	public static final String F_FINALDESTID = "finalDestId";
	public static final String F_FINALDESTINATION = "finalDestination";
	public static final String F_DOMINTINDICATOR = "domIntIndicator";
	public static final String F_NETUPLIFTVOLUME = "netUpliftVolume";
	public static final String F_NETUPLIFTUNITID = "netUpliftUnitId";
	public static final String F_NETUPLIFTUNIT = "netUpliftUnit";
	public static final String F_BEFOREFUELING = "beforeFueling";
	public static final String F_BEFOREFUELINGUNITID = "beforeFuelingUnitId";
	public static final String F_BEFOREFUELINGUNIT = "beforeFuelingUnit";
	public static final String F_AFTERFUELING = "afterFueling";
	public static final String F_AFTERFUELINGUNITID = "afterFuelingUnitId";
	public static final String F_AFTERFUELINGUNIT = "afterFuelingUnit";
	public static final String F_DENSITYUNITID = "densityUnitId";
	public static final String F_DENSITYMASSUNIT = "densityMassUnit";
	public static final String F_DENSITYVOLUMEID = "densityVolumeId";
	public static final String F_DENSITYVOLUMEUNIT = "densityVolumeUnit";
	public static final String F_FUELINGSTARTDATE = "fuelingStartDate";
	public static final String F_FUELINGENDDATE = "fuelingEndDate";
	public static final String F_ACTYPEID = "acTypeId";
	public static final String F_AIRCRAFTTYPE = "aircraftType";
	public static final String F_DELIVERYTYPE = "deliveryType";
	public static final String F_SOURCE = "source";
	public static final String F_TRANSPORT = "transport";
	public static final String F_TICKETSTATUS = "ticketStatus";
	public static final String F_TRANSMISSIONSTATUS = "transmissionStatus";
	public static final String F_TRANSMISSIONDETAILS = "transmissionDetails";
	public static final String F_DENSITY = "density";
	public static final String F_FUELINGOPERATION = "fuelingOperation";
	public static final String F_PRODUCTTYPE = "productType";
	public static final String F_CUSTOMSTATUS = "customStatus";
	public static final String F_SUFFIX = "suffix";
	public static final String F_TICKETHARDCOPY = "ticketHardCopy";
	public static final String F_BILLSTATUS = "billStatus";
	public static final String F_INVOICESTATUS = "invoiceStatus";

	@DsField(join = "left", path = "departure.id")
	private Integer depId;

	@DsField(join = "left", path = "departure.code")
	private String departure;

	@DsField(join = "left", path = "customer.id")
	private Integer custId;

	@DsField(join = "left", path = "customer.code")
	private String customer;

	@DsField(join = "left", path = "upliftUnit.id")
	private Integer upliftUnitId;

	@DsField(join = "left", path = "upliftUnit.code")
	private String upliftUnit;

	@DsField(path = "ticketNo")
	private String ticketNumber;

	@DsField(path = "deliveryDate")
	private Date delivery;

	@DsField
	private BigDecimal upliftVolume;

	@DsField(path = "flightNo")
	private String flightNumber;

	@DsField
	private String airlineDesignator;

	@DsField(join = "left", path = "supplier.id")
	private Integer suppId;

	@DsField(join = "left", path = "supplier.code")
	private String supplier;

	@DsField(join = "left", path = "fueller.id")
	private Integer fuellerId;

	@DsField(join = "left", path = "fueller.code")
	private String fueller;

	@DsField(join = "left", path = "destination.id")
	private Integer destId;

	@DsField(join = "left", path = "destination.code")
	private String destination;

	@DsField(join = "left", path = "registration.id")
	private Integer aircraftId;

	@DsField(join = "left", path = "registration.registration")
	private String registration;

	@DsField(join = "left", path = "finalDest.id")
	private Integer finalDestId;

	@DsField(join = "left", path = "finalDest.code")
	private String finalDestination;

	@DsField(path = "indicator")
	private FlightTypeIndicator domIntIndicator;

	@DsField(path = "netUpliftQuantity")
	private BigDecimal netUpliftVolume;

	@DsField(join = "left", path = "netUpliftUnit.id")
	private Integer netUpliftUnitId;

	@DsField(join = "left", path = "netUpliftUnit.code")
	private String netUpliftUnit;

	@DsField
	private BigDecimal beforeFueling;

	@DsField(join = "left", path = "beforeFuelingUnit.id")
	private Integer beforeFuelingUnitId;

	@DsField(join = "left", path = "beforeFuelingUnit.code")
	private String beforeFuelingUnit;

	@DsField
	private BigDecimal afterFueling;

	@DsField(join = "left", path = "afterFuelingUnit.id")
	private Integer afterFuelingUnitId;

	@DsField(join = "left", path = "afterFuelingUnit.code")
	private String afterFuelingUnit;

	@DsField(join = "left", path = "densityUnit.id")
	private Integer densityUnitId;

	@DsField(join = "left", path = "densityUnit.code")
	private String densityMassUnit;

	@DsField(join = "left", path = "densityVolume.id")
	private Integer densityVolumeId;

	@DsField(join = "left", path = "densityVolume.code")
	private String densityVolumeUnit;

	@DsField
	private Date fuelingStartDate;

	@DsField
	private Date fuelingEndDate;

	@DsField(join = "left", path = "acType.id")
	private Integer acTypeId;

	@DsField(join = "left", path = "acType.code")
	private String aircraftType;

	@DsField
	private String deliveryType;

	@DsField
	private FuelTicketSource source;

	@DsField
	private FuelingType transport;

	@DsField
	private FuelTicketStatus ticketStatus;

	@DsField
	private FuelTicketTransmissionStatus transmissionStatus;

	@DsField
	private String transmissionDetails;

	@DsField
	private BigDecimal density;

	@DsField
	private FuelTicketFuelingOperation fuelingOperation;

	@DsField
	private Product productType;

	@DsField(path = "customsStatus")
	private FuelTicketCustomsStatus customStatus;

	@DsField
	private Suffix suffix;

	@DsField(fetch = false)
	private String ticketHardCopy;

	@DsField
	private BillStatus billStatus;

	@DsField
	private OutgoingInvoiceStatus invoiceStatus;

	/**
	 * Default constructor
	 */
	public FuelTicketMobile_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelTicketMobile_Ds(FuelTicket e) {
		super(e);
	}

	public Integer getDepId() {
		return this.depId;
	}

	public void setDepId(Integer depId) {
		this.depId = depId;
	}

	public String getDeparture() {
		return this.departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public Integer getCustId() {
		return this.custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCustomer() {
		return this.customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public Integer getUpliftUnitId() {
		return this.upliftUnitId;
	}

	public void setUpliftUnitId(Integer upliftUnitId) {
		this.upliftUnitId = upliftUnitId;
	}

	public String getUpliftUnit() {
		return this.upliftUnit;
	}

	public void setUpliftUnit(String upliftUnit) {
		this.upliftUnit = upliftUnit;
	}

	public String getTicketNumber() {
		return this.ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public Date getDelivery() {
		return this.delivery;
	}

	public void setDelivery(Date delivery) {
		this.delivery = delivery;
	}

	public BigDecimal getUpliftVolume() {
		return this.upliftVolume;
	}

	public void setUpliftVolume(BigDecimal upliftVolume) {
		this.upliftVolume = upliftVolume;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getAirlineDesignator() {
		return this.airlineDesignator;
	}

	public void setAirlineDesignator(String airlineDesignator) {
		this.airlineDesignator = airlineDesignator;
	}

	public Integer getSuppId() {
		return this.suppId;
	}

	public void setSuppId(Integer suppId) {
		this.suppId = suppId;
	}

	public String getSupplier() {
		return this.supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public Integer getFuellerId() {
		return this.fuellerId;
	}

	public void setFuellerId(Integer fuellerId) {
		this.fuellerId = fuellerId;
	}

	public String getFueller() {
		return this.fueller;
	}

	public void setFueller(String fueller) {
		this.fueller = fueller;
	}

	public Integer getDestId() {
		return this.destId;
	}

	public void setDestId(Integer destId) {
		this.destId = destId;
	}

	public String getDestination() {
		return this.destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getAircraftId() {
		return this.aircraftId;
	}

	public void setAircraftId(Integer aircraftId) {
		this.aircraftId = aircraftId;
	}

	public String getRegistration() {
		return this.registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public Integer getFinalDestId() {
		return this.finalDestId;
	}

	public void setFinalDestId(Integer finalDestId) {
		this.finalDestId = finalDestId;
	}

	public String getFinalDestination() {
		return this.finalDestination;
	}

	public void setFinalDestination(String finalDestination) {
		this.finalDestination = finalDestination;
	}

	public FlightTypeIndicator getDomIntIndicator() {
		return this.domIntIndicator;
	}

	public void setDomIntIndicator(FlightTypeIndicator domIntIndicator) {
		this.domIntIndicator = domIntIndicator;
	}

	public BigDecimal getNetUpliftVolume() {
		return this.netUpliftVolume;
	}

	public void setNetUpliftVolume(BigDecimal netUpliftVolume) {
		this.netUpliftVolume = netUpliftVolume;
	}

	public Integer getNetUpliftUnitId() {
		return this.netUpliftUnitId;
	}

	public void setNetUpliftUnitId(Integer netUpliftUnitId) {
		this.netUpliftUnitId = netUpliftUnitId;
	}

	public String getNetUpliftUnit() {
		return this.netUpliftUnit;
	}

	public void setNetUpliftUnit(String netUpliftUnit) {
		this.netUpliftUnit = netUpliftUnit;
	}

	public BigDecimal getBeforeFueling() {
		return this.beforeFueling;
	}

	public void setBeforeFueling(BigDecimal beforeFueling) {
		this.beforeFueling = beforeFueling;
	}

	public Integer getBeforeFuelingUnitId() {
		return this.beforeFuelingUnitId;
	}

	public void setBeforeFuelingUnitId(Integer beforeFuelingUnitId) {
		this.beforeFuelingUnitId = beforeFuelingUnitId;
	}

	public String getBeforeFuelingUnit() {
		return this.beforeFuelingUnit;
	}

	public void setBeforeFuelingUnit(String beforeFuelingUnit) {
		this.beforeFuelingUnit = beforeFuelingUnit;
	}

	public BigDecimal getAfterFueling() {
		return this.afterFueling;
	}

	public void setAfterFueling(BigDecimal afterFueling) {
		this.afterFueling = afterFueling;
	}

	public Integer getAfterFuelingUnitId() {
		return this.afterFuelingUnitId;
	}

	public void setAfterFuelingUnitId(Integer afterFuelingUnitId) {
		this.afterFuelingUnitId = afterFuelingUnitId;
	}

	public String getAfterFuelingUnit() {
		return this.afterFuelingUnit;
	}

	public void setAfterFuelingUnit(String afterFuelingUnit) {
		this.afterFuelingUnit = afterFuelingUnit;
	}

	public Integer getDensityUnitId() {
		return this.densityUnitId;
	}

	public void setDensityUnitId(Integer densityUnitId) {
		this.densityUnitId = densityUnitId;
	}

	public String getDensityMassUnit() {
		return this.densityMassUnit;
	}

	public void setDensityMassUnit(String densityMassUnit) {
		this.densityMassUnit = densityMassUnit;
	}

	public Integer getDensityVolumeId() {
		return this.densityVolumeId;
	}

	public void setDensityVolumeId(Integer densityVolumeId) {
		this.densityVolumeId = densityVolumeId;
	}

	public String getDensityVolumeUnit() {
		return this.densityVolumeUnit;
	}

	public void setDensityVolumeUnit(String densityVolumeUnit) {
		this.densityVolumeUnit = densityVolumeUnit;
	}

	public Date getFuelingStartDate() {
		return this.fuelingStartDate;
	}

	public void setFuelingStartDate(Date fuelingStartDate) {
		this.fuelingStartDate = fuelingStartDate;
	}

	public Date getFuelingEndDate() {
		return this.fuelingEndDate;
	}

	public void setFuelingEndDate(Date fuelingEndDate) {
		this.fuelingEndDate = fuelingEndDate;
	}

	public Integer getAcTypeId() {
		return this.acTypeId;
	}

	public void setAcTypeId(Integer acTypeId) {
		this.acTypeId = acTypeId;
	}

	public String getAircraftType() {
		return this.aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getDeliveryType() {
		return this.deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public FuelTicketSource getSource() {
		return this.source;
	}

	public void setSource(FuelTicketSource source) {
		this.source = source;
	}

	public FuelingType getTransport() {
		return this.transport;
	}

	public void setTransport(FuelingType transport) {
		this.transport = transport;
	}

	public FuelTicketStatus getTicketStatus() {
		return this.ticketStatus;
	}

	public void setTicketStatus(FuelTicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public FuelTicketTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			FuelTicketTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public String getTransmissionDetails() {
		return this.transmissionDetails;
	}

	public void setTransmissionDetails(String transmissionDetails) {
		this.transmissionDetails = transmissionDetails;
	}

	public BigDecimal getDensity() {
		return this.density;
	}

	public void setDensity(BigDecimal density) {
		this.density = density;
	}

	public FuelTicketFuelingOperation getFuelingOperation() {
		return this.fuelingOperation;
	}

	public void setFuelingOperation(FuelTicketFuelingOperation fuelingOperation) {
		this.fuelingOperation = fuelingOperation;
	}

	public Product getProductType() {
		return this.productType;
	}

	public void setProductType(Product productType) {
		this.productType = productType;
	}

	public FuelTicketCustomsStatus getCustomStatus() {
		return this.customStatus;
	}

	public void setCustomStatus(FuelTicketCustomsStatus customStatus) {
		this.customStatus = customStatus;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public String getTicketHardCopy() {
		return this.ticketHardCopy;
	}

	public void setTicketHardCopy(String ticketHardCopy) {
		this.ticketHardCopy = ticketHardCopy;
	}

	public BillStatus getBillStatus() {
		return this.billStatus;
	}

	public void setBillStatus(BillStatus billStatus) {
		this.billStatus = billStatus;
	}

	public OutgoingInvoiceStatus getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(OutgoingInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
}
