/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.ops.presenter.ext.fuelTicket.qb;

import atraxo.ops.presenter.impl.fuelTicket.model.ArchivedFuelTicket_Ds;
import atraxo.ops.presenter.impl.fuelTicket.model.ArchivedFuelTicket_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class ArchivedFuelTicket_DsQb extends QueryBuilderWithJpql<ArchivedFuelTicket_Ds, Object, ArchivedFuelTicket_DsParam> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		// TODO Auto-generated method stub
		super.beforeBuildWhere();

		this.addFilterCondition("e.isDeleted = true");

	}
}
