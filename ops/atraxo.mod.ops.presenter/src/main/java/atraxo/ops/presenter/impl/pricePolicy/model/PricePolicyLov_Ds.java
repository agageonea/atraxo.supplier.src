/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.pricePolicy.model;

import atraxo.fmbas.presenter.impl.abstracts.model.AbstractLov_Ds;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = PricePolicy.class, sort = {@SortField(field = PricePolicyLov_Ds.F_NAME)})
public class PricePolicyLov_Ds extends AbstractLov_Ds<PricePolicy> {

	public static final String ALIAS = "ops_PricePolicyLov_Ds";

	public static final String F_NAME = "name";
	public static final String F_ACTIVE = "active";

	@DsField
	private String name;

	@DsField
	private Boolean active;

	/**
	 * Default constructor
	 */
	public PricePolicyLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PricePolicyLov_Ds(PricePolicy e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
