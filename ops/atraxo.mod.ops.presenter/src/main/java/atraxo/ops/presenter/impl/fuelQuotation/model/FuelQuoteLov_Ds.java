/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuotation.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatus;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelQuotation.class, sort = {@SortField(field = FuelQuoteLov_Ds.F_CODE)})
@RefLookups({@RefLookup(refId = FuelQuoteLov_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuoteLov_Ds.F_CUSTOMERCODE)})})
public class FuelQuoteLov_Ds extends AbstractDs_Ds<FuelQuotation> {

	public static final String ALIAS = "ops_FuelQuoteLov_Ds";

	public static final String F_CODE = "code";
	public static final String F_VALIDUNTIL = "validUntil";
	public static final String F_STATUS = "status";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";
	public static final String F_SUBSIDIARYNAME = "subsidiaryName";

	@DsField
	private String code;

	@DsField
	private Date validUntil;

	@DsField
	private FuelQuoteStatus status;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "holder.id")
	private Integer subsidiaryId;

	@DsField(join = "left", path = "holder.code")
	private String subsidiaryCode;

	@DsField(join = "left", path = "holder.name")
	private String subsidiaryName;

	/**
	 * Default constructor
	 */
	public FuelQuoteLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelQuoteLov_Ds(FuelQuotation e) {
		super(e);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getValidUntil() {
		return this.validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public FuelQuoteStatus getStatus() {
		return this.status;
	}

	public void setStatus(FuelQuoteStatus status) {
		this.status = status;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public String getSubsidiaryName() {
		return this.subsidiaryName;
	}

	public void setSubsidiaryName(String subsidiaryName) {
		this.subsidiaryName = subsidiaryName;
	}
}
