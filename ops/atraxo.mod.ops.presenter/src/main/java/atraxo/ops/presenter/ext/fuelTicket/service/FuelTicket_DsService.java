/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelTicket.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import atraxo.ops.business.api.fuelTicket.IFuelTicketHardCopyService;
import atraxo.ops.business.ext.exceptions.FuelTicketUpdateException;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.domain.ext.fuelTicket.ProvidedQuantityInforamtionResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import atraxo.ops.presenter.impl.fuelTicket.model.FuelTicket_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelTicket_DsService extends AbstractEntityDsService<FuelTicket_Ds, FuelTicket_Ds, Object, FuelTicket>
		implements IDsService<FuelTicket_Ds, FuelTicket_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(FuelTicket_DsService.class);

	@Autowired
	private IWorkflowInstanceEntityService workflowInstanceEntityService;
	@Autowired
	private IWorkflowBpmManager workflowBpmManager;
	@Autowired
	private IFuelTicketHardCopyService fthcService;
	@Autowired
	private ICustomerService customerService;

	@Override
	protected void preUpdateAfterEntity(FuelTicket_Ds ds, FuelTicket e, Object params) throws Exception {
		super.preUpdateAfterEntity(ds, e, params);
		FuelTicket persistedTicket = this.getEntityService().findById(e.getId());
		if (!e.getVersion().equals(persistedTicket.getVersion())) {
			throw new FuelTicketUpdateException();
		}
	}

	@Override
	protected void postUpdate(List<FuelTicket_Ds> list, Object params) throws Exception {
		super.postUpdate(list, params);
		this.collectToleranceResult(list);
		this.setFlightID(list);
	}

	@Override
	protected void postInsert(List<FuelTicket_Ds> list, Object params) throws Exception {
		super.postInsert(list, params);
		this.setFlightID(list);
		this.setSubsidiaryCode(list);
	}

	@Override
	protected void preInsert(List<FuelTicket_Ds> list, Object params) throws Exception {
		super.preInsert(list, params);
		this.setSubsidiaryCode(list);
	}

	@Override
	protected void postFind(IQueryBuilder<FuelTicket_Ds, FuelTicket_Ds, Object> builder, List<FuelTicket_Ds> result) throws Exception {
		super.postFind(builder, result);
		for (FuelTicket_Ds ds : result) {
			try {
				this.fthcService.findByBk(new Long(ds.getId()));
				ds.setIsHardCopy(true);
			} catch (Exception e) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Could not find FuelTicket, will set hardcopy as false!", e);
				}
				ds.setIsHardCopy(false);
			}

			ds.setCanBeCompleted(false);
			if (ds.getApprovalStatus().equals(FuelTicketApprovalStatus._AWAITING_APPROVAL_)) {
				try {
					WorkflowInstanceEntity wie = this.workflowInstanceEntityService.findByBusinessKey(
							WorkflowNames.FUEL_TICKET_APPROVAL.getWorkflowName(), ds.getId(), ds._getEntity_().getClass().getSimpleName());
					ds.setCanBeCompleted(this.workflowBpmManager.canComplete(wie.getWorkflowInstance().getId()));
				} catch (Exception e) {
					// do nothing
					LOG.warn("Could not retrieve a WorkflowInstanceEntity for workflow FUEL_TICKET_APPROVAL, will do nothing !", e);
				}
			}
		}
		this.setFlightID(result);
	}

	private void setSubsidiaryCode(List<FuelTicket_Ds> list) throws Exception {
		for (FuelTicket_Ds ds : list) {
			try {
				ds.setSubsidiaryCode(this.customerService.findByRefid(ds.getSubsidiaryId()).getCode());
			} catch (Exception e) {
				throw new BusinessException(OpsErrorCode.FUEL_TICKET_NO_SUBSIDIARY, OpsErrorCode.FUEL_TICKET_NO_SUBSIDIARY.getErrMsg(), e);
			}
		}
	}

	private void setFlightID(List<FuelTicket_Ds> list) {
		for (FuelTicket_Ds ds : list) {
			ds.setFlightID(ds.getAirlineDesignator() + ds.getFlightNo());
		}
	}

	protected void collectToleranceResult(List<FuelTicket_Ds> list) {
		for (FuelTicket_Ds ds : list) {
			if (ProvidedQuantityInforamtionResult.resultMap.get() != null) {
				ds.setToleranceMessage(ProvidedQuantityInforamtionResult.resultMap.get().remove(ds.getId()));
			}
		}
	}

}
