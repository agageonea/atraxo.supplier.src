/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelQuoteLocation.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.api.prices.IContractPriceCategoryService;
import atraxo.cmm.business.ext.exceptions.FuelPriceNotExistsException;
import atraxo.cmm.business.ext.exceptions.NoPriceComponentsException;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.domain.impl.fmbas_type.PriceInd;
import atraxo.ops.presenter.impl.fuelQuoteLocation.model.PriceCategories_Ds;
import atraxo.ops.presenter.impl.fuelQuoteLocation.model.PriceCategories_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class PriceCategories_DsService extends AbstractEntityDsService<PriceCategories_Ds, PriceCategories_Ds, Object, ContractPriceCategory>
		implements IDsService<PriceCategories_Ds, PriceCategories_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(PriceCategories_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<PriceCategories_Ds, PriceCategories_Ds, Object> builder, List<PriceCategories_Ds> result) throws Exception {
		IContractPriceCategoryService cpcservice = (IContractPriceCategoryService) this.findEntityService(ContractPriceCategory.class);
		PriceCategories_DsParam param = (PriceCategories_DsParam) builder.getParams();
		Map<String, PriceCategories_Ds> usedPriceCategories = new HashMap<>();
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		for (PriceCategories_Ds priceCategoryDs : result) {
			try {
				ContractPriceCategory priceCategory = priceCategoryDs._getEntity_();
				if (date.before(priceCategoryDs.getContractValidFrom())) {
					date = priceCategoryDs.getContractValidFrom();
				}
				if (date.after(priceCategoryDs.getContractValidTo())) {
					date = priceCategoryDs.getContractValidTo();
				}
				ContractPriceComponent priceComponent = cpcservice.getPriceComponent(priceCategory, date);
				this.setPriceDetails(priceCategoryDs, priceComponent);
				BigDecimal convertedPrice = cpcservice.getPriceInCurrencyUnit(priceCategory, param.getSettlementUnitCode(),
						param.getSettlementCurrencyCode(), date);
				priceCategoryDs.setConvertedPrice(convertedPrice);
			} catch (NoPriceComponentsException e) {
				LOG.warn("Could not find a price component, will set converted price as zero !", e);
				priceCategoryDs.setConvertedPrice(BigDecimal.ZERO);
			}
			this.markPriceCategories(usedPriceCategories, priceCategoryDs);

		}
	}

	@Override
	protected void postSummaries(IQueryBuilder<PriceCategories_Ds, PriceCategories_Ds, Object> builder, List<PriceCategories_Ds> findResult,
			Map<String, Object> sum) throws Exception {
		IContractService service = (IContractService) this.findEntityService(Contract.class);
		PriceCategories_DsParam params = (PriceCategories_DsParam) builder.getParams();
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		BigDecimal total = BigDecimal.ZERO;
		if (!findResult.isEmpty()) {
			try {
				total = service.getPrice(findResult.get(0).getContractId(), params.getSettlementUnitCode(), params.getSettlementCurrencyCode(), date);
			} catch (FuelPriceNotExistsException e) {
				LOG.warn(e.getMessage(), e);
			}
		}
		params.setTotal(total);
	}

	private void setPriceDetails(PriceCategories_Ds priceCategory, ContractPriceComponent priceComponent) {
		if (priceCategory != null) {
			priceCategory.setPrice(priceComponent.getPrice());
		} else {
			return;
		}
		if (priceComponent.getPrice() != null) {
			priceCategory.setPrice(priceComponent.getPrice());
		}
		if (priceComponent.getCurrency() != null && priceComponent.getCurrency().getCode() != null) {
			priceCategory.setCurrencyCode(priceComponent.getCurrency().getCode());
		}
		if (priceComponent.getCurrency() != null && priceComponent.getCurrency().getId() != null) {
			priceCategory.setCurrencyId(priceComponent.getCurrency().getId());
		}
		if (priceComponent.getUnit() != null && priceComponent.getUnit().getCode() != null) {
			priceCategory.setUnitCode(priceComponent.getUnit().getCode());
		}
		if (priceComponent.getUnit() != null && priceComponent.getUnit().getId() != null) {
			priceCategory.setUnitId(priceComponent.getUnit().getId());
		}
	}

	private void markPriceCategories(Map<String, PriceCategories_Ds> usedPriceCategories, PriceCategories_Ds priceCategoryDs) {
		ContractPriceCategory priceCategory = priceCategoryDs._getEntity_();
		PriceInd priceCategoryPer = priceCategory.getPriceCategory().getPricePer();
		if (!PriceInd._EVENT_.equals(priceCategoryPer)) {
			if (!usedPriceCategories.keySet().contains(priceCategory.getPriceCategory().getName())) {
				usedPriceCategories.put(priceCategory.getPriceCategory().getName(), priceCategoryDs);
				priceCategoryDs.setUsed(true);
			} else if (priceCategory.getDefaultPriceCtgy()) {
				PriceCategories_Ds tempContractPriceCategory_Ds = usedPriceCategories.get(priceCategory.getPriceCategory().getName());
				tempContractPriceCategory_Ds.setUsed(false);
				usedPriceCategories.put(priceCategory.getPriceCategory().getName(), priceCategoryDs);
				priceCategoryDs.setUsed(true);
			}
		}
	}

}
