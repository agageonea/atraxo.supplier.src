/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.pricePolicy.service;

import java.util.List;

import atraxo.cmm.domain.impl.cmm_type.YesNo;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import atraxo.ops.presenter.impl.pricePolicy.model.PricePolicy_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class PricePolicy_DsService extends AbstractEntityDsService<PricePolicy_Ds, PricePolicy_Ds, Object, PricePolicy> implements
		IDsService<PricePolicy_Ds, PricePolicy_Ds, Object> {

	@Override
	protected void preInsert(PricePolicy_Ds ds, Object params) throws Exception {
		super.preInsert(ds, params);
		ds.setActive(this.getBol(ds.getYesNo()));
	}

	@Override
	protected void preUpdate(PricePolicy_Ds ds, Object params) throws Exception {
		super.preUpdate(ds, params);
		ds.setActive(this.getBol(ds.getYesNo()));
	}

	@Override
	protected void postFind(IQueryBuilder<PricePolicy_Ds, PricePolicy_Ds, Object> builder, List<PricePolicy_Ds> result) throws Exception {
		super.postFind(builder, result);
		for (PricePolicy_Ds ds : result) {
			ds.setYesNo(YesNo.getByName(this.getStr(ds.getActive())));
		}
	}

	/**
	 * @param str
	 * @return
	 */
	private boolean getBol(YesNo str) {
		if (YesNo._True_.equals(str)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param b {@link Boolean}
	 * @return
	 */
	private String getStr(boolean b) {
		if (b) {
			return "Yes";
		} else {
			return "No";
		}
	}

}
