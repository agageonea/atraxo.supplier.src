/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelTicket.service;

import java.util.List;

import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.presenter.impl.fuelTicket.model.ArchivedFuelTicket_Ds;
import atraxo.ops.presenter.impl.fuelTicket.model.ArchivedFuelTicket_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class ArchivedFuelTicket_DsService
		extends AbstractEntityDsService<ArchivedFuelTicket_Ds, ArchivedFuelTicket_Ds, ArchivedFuelTicket_DsParam, FuelTicket>
		implements IDsService<ArchivedFuelTicket_Ds, ArchivedFuelTicket_Ds, ArchivedFuelTicket_DsParam> {

	@Override
	protected void postFind(IQueryBuilder<ArchivedFuelTicket_Ds, ArchivedFuelTicket_Ds, ArchivedFuelTicket_DsParam> builder,
			List<ArchivedFuelTicket_Ds> result) throws Exception {
		super.postFind(builder, result);
		for (ArchivedFuelTicket_Ds ds : result) {
			ds.setFlightID(ds.getAirlineDesignator() + ds.getFlightNo());

		}
	}

}
