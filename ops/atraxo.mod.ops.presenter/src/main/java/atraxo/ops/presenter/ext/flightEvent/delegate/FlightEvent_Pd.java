package atraxo.ops.presenter.ext.flightEvent.delegate;

import java.util.Date;
import java.util.List;

import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.domain.ext.flightEvent.AttachedItem;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.presenter.impl.flightEvent.model.FlightEvent_Ds;
import atraxo.ops.presenter.impl.flightEvent.model.FlightEvent_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class FlightEvent_Pd extends AbstractPresenterDelegate {

	public void updateOrDelete(List<FlightEvent_Ds> dsList, FlightEvent_DsParam param) throws Exception {
		IFlightEventService service = (IFlightEventService) this.findEntityService(FlightEvent.class);
		for (FlightEvent_Ds ds : dsList) {
			service.updateOrDelete(ds.getId(), AttachedItem.valueOf(param.getSrc()));
		}
	}

	public void reschedule(FlightEvent_Ds flightEvent, FlightEvent_DsParam param) throws Exception {
		IFlightEventService feS = (IFlightEventService) this.findEntityService(FlightEvent.class);
		FlightEvent fe = feS.findById(flightEvent.getId());
		feS.reschedule(fe, flightEvent.getNewArrivalDate(), flightEvent.getNewDepartureDate(), flightEvent.getNewUpliftDate(),
				flightEvent.getNewTimeReference(), param.getRemarks());
		String warningMessage = "";
		if ((flightEvent.getNewArrivalDate() != null && flightEvent.getNewArrivalDate().before(new Date()))
				|| (flightEvent.getNewDepartureDate() != null && flightEvent.getNewDepartureDate().before(new Date()))
				|| (flightEvent.getNewUpliftDate() != null && flightEvent.getNewUpliftDate().before(new Date()))) {
			warningMessage = "The new date is in the past!";
		}
		param.setWarningMessage(warningMessage);
	}

	public void cancel(FlightEvent_Ds flightEvent, FlightEvent_DsParam param) throws Exception {
		IFlightEventService fEService = (IFlightEventService) this.findEntityService(FlightEvent.class);
		FlightEvent fe = fEService.findById(flightEvent.getId());
		fEService.updateStatus(fe, param.getAction(), param.getRemarks(), param.getParamStatus());
	}

	public void reset(FlightEvent_Ds flightEvent, FlightEvent_DsParam param) throws Exception {
		IFlightEventService fEService = (IFlightEventService) this.findEntityService(FlightEvent.class);
		FlightEvent fe = fEService.findById(flightEvent.getId());
		fEService.updateStatus(fe, param.getAction(), param.getRemarks(), param.getParamStatus());
	}
}
