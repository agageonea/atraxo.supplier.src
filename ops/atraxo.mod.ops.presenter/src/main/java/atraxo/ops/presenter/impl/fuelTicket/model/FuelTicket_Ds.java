/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelTicket.model;

import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperation;
import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketCustomsStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketSource;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketTransmissionStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import atraxo.ops.domain.impl.ops_type.SendInvoiceBy;
import atraxo.ops.domain.impl.ops_type.Suffix;
import atraxo.ops.domain.impl.ops_type.TemperatureUnit;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelTicket.class, jpqlWhere = "e.isDeleted = false", sort = {@SortField(field = FuelTicket_Ds.F_DELIVERYDATE, desc = true)})
@RefLookups({
		@RefLookup(refId = FuelTicket_Ds.F_DEPID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_DEPCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_CUSTID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_CUSTCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_UPLIFTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_UPLIFTUNITCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_SUPPID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_SUPPCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_FUELLERID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_FUELLERCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_DESTID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_DESTCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_FINALDESTID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_FINALDESTCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_NETUPLIFTUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_NETUPLIFTUNITCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_BEFOREFUELINGUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_BEFOREFUELINGUNITCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_AFTERFUELINGUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_AFTERFUELINGUNITCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_DENSITYUNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_DENSITYUNITCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_DENSITYVOLUMEID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_DENSITYVOLUMECODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_ACTYPEID, namedQuery = AcTypes.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_ACTYPECODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_AIRCRAFTID, namedQuery = Aircraft.NQ_FIND_BY_REG_PRIMITIVE, params = {
				@Param(name = "customerId", field = FuelTicket_Ds.F_CUSTID),
				@Param(name = "registration", field = FuelTicket_Ds.F_AIRCRAFTREG)}),
		@RefLookup(refId = FuelTicket_Ds.F_RESELLERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_RESELLERCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_AMOUNTCURRID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelTicket_Ds.F_AMOUNTCURRCODE)}),
		@RefLookup(refId = FuelTicket_Ds.F_REFTICKETID)})
public class FuelTicket_Ds extends AbstractSubsidiaryDs_Ds<FuelTicket> {

	public static final String ALIAS = "ops_FuelTicket_Ds";

	public static final String F_DEPID = "depId";
	public static final String F_DEPCODE = "depCode";
	public static final String F_CUSTID = "custId";
	public static final String F_CUSTCODE = "custCode";
	public static final String F_CUSTNAME = "custName";
	public static final String F_AOC = "aoc";
	public static final String F_UPLIFTUNITID = "upliftUnitId";
	public static final String F_UPLIFTUNITCODE = "upliftUnitCode";
	public static final String F_SUPPID = "suppId";
	public static final String F_SUPPCODE = "suppCode";
	public static final String F_FUELLERID = "fuellerId";
	public static final String F_FUELLERCODE = "fuellerCode";
	public static final String F_DESTID = "destId";
	public static final String F_DESTCODE = "destCode";
	public static final String F_FINALDESTID = "finalDestId";
	public static final String F_FINALDESTCODE = "finalDestCode";
	public static final String F_NETUPLIFTUNITID = "netUpliftUnitId";
	public static final String F_NETUPLIFTUNITCODE = "netUpliftUnitCode";
	public static final String F_BEFOREFUELINGUNITID = "beforeFuelingUnitId";
	public static final String F_BEFOREFUELINGUNITCODE = "beforeFuelingUnitCode";
	public static final String F_AFTERFUELINGUNITID = "afterFuelingUnitId";
	public static final String F_AFTERFUELINGUNITCODE = "afterFuelingUnitCode";
	public static final String F_DENSITYUNITID = "densityUnitId";
	public static final String F_DENSITYUNITCODE = "densityUnitCode";
	public static final String F_DENSITYVOLUMEID = "densityVolumeId";
	public static final String F_DENSITYVOLUMECODE = "densityVolumeCode";
	public static final String F_ACTYPEID = "acTypeId";
	public static final String F_ACTYPECODE = "acTypeCode";
	public static final String F_AIRCRAFTID = "aircraftId";
	public static final String F_AIRCRAFTREG = "aircraftReg";
	public static final String F_RESELLERID = "resellerId";
	public static final String F_RESELLERCODE = "resellerCode";
	public static final String F_AMOUNTCURRID = "amountCurrId";
	public static final String F_AMOUNTCURRCODE = "amountCurrCode";
	public static final String F_REFTICKETID = "refTicketId";
	public static final String F_REFTICKETNO = "refTicketNo";
	public static final String F_UPLIFTVOLUME = "upliftVolume";
	public static final String F_TICKETNO = "ticketNo";
	public static final String F_DELIVERYDATE = "deliveryDate";
	public static final String F_AIRLINEDESIGNATOR = "airlineDesignator";
	public static final String F_FLIGHTNO = "flightNo";
	public static final String F_SUFFIX = "suffix";
	public static final String F_CUSTOMSSTATUS = "customsStatus";
	public static final String F_PRODUCTTYPE = "productType";
	public static final String F_FUELINGOPERATION = "fuelingOperation";
	public static final String F_INDICATOR = "indicator";
	public static final String F_NETUPLIFTQUANTITY = "netUpliftQuantity";
	public static final String F_BEFOREFUELING = "beforeFueling";
	public static final String F_AFTERFUELING = "afterFueling";
	public static final String F_DENSITY = "density";
	public static final String F_FUELINGSTARTDATE = "fuelingStartDate";
	public static final String F_FUELINGENDDATE = "fuelingEndDate";
	public static final String F_DELIVERYTYPE = "deliveryType";
	public static final String F_SOURCE = "source";
	public static final String F_TRANSPORT = "transport";
	public static final String F_TICKETSTATUS = "ticketStatus";
	public static final String F_TRANSMISSIONSTATUS = "transmissionStatus";
	public static final String F_TRANSMISSIONDETAILS = "transmissionDetails";
	public static final String F_APPROVALSTATUS = "approvalStatus";
	public static final String F_TRANSMISSIONDATE = "transmissionDate";
	public static final String F_CAPTUREDBY = "capturedBy";
	public static final String F_APPROVEDBY = "approvedBy";
	public static final String F_TOLERANCEMESSAGE = "toleranceMessage";
	public static final String F_BILLSTATUS = "billStatus";
	public static final String F_INVOICESTATUS = "invoiceStatus";
	public static final String F_METERSTARTINDEX = "meterStartIndex";
	public static final String F_METERENDINDEX = "meterEndIndex";
	public static final String F_TEMPERATURE = "temperature";
	public static final String F_TEMPERATUREUNIT = "temperatureUnit";
	public static final String F_REFUELERARRIVALTIME = "refuelerArrivalTime";
	public static final String F_PAYMENTTYPE = "paymentType";
	public static final String F_CARDNUMBER = "cardNumber";
	public static final String F_CARDEXPIRINGDATE = "cardExpiringDate";
	public static final String F_CARDHOLDER = "cardHolder";
	public static final String F_AMOUNTRECEIVED = "amountReceived";
	public static final String F_SENDINVOICEBY = "sendInvoiceBy";
	public static final String F_SENDINVOICETO = "sendInvoiceTo";
	public static final String F_ISDELETED = "isDeleted";
	public static final String F_FLIGHTSERVICETYPE = "flightServiceType";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";
	public static final String F_REVOCATIONREASON = "revocationReason";
	public static final String F_TMP = "tmp";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_FLIGHTID = "flightID";
	public static final String F_ISHARDCOPY = "isHardCopy";
	public static final String F_LABELFIELD = "labelField";
	public static final String F_CANBECOMPLETED = "canBeCompleted";
	public static final String F_CANCELWINDOWTEXT = "cancelWindowText";

	@DsField(join = "left", path = "departure.id")
	private Integer depId;

	@DsField(join = "left", path = "departure.code")
	private String depCode;

	@DsField(join = "left", path = "customer.id")
	private Integer custId;

	@DsField(join = "left", path = "customer.code")
	private String custCode;

	@DsField(join = "left", path = "customer.name")
	private String custName;

	@DsField(join = "left", path = "customer.aoc")
	private String aoc;

	@DsField(join = "left", path = "upliftUnit.id")
	private Integer upliftUnitId;

	@DsField(join = "left", path = "upliftUnit.code")
	private String upliftUnitCode;

	@DsField(join = "left", path = "supplier.id")
	private Integer suppId;

	@DsField(join = "left", path = "supplier.code")
	private String suppCode;

	@DsField(join = "left", path = "fueller.id")
	private Integer fuellerId;

	@DsField(join = "left", path = "fueller.code")
	private String fuellerCode;

	@DsField(join = "left", path = "destination.id")
	private Integer destId;

	@DsField(join = "left", path = "destination.code")
	private String destCode;

	@DsField(join = "left", path = "finalDest.id")
	private Integer finalDestId;

	@DsField(join = "left", path = "finalDest.code")
	private String finalDestCode;

	@DsField(join = "left", path = "netUpliftUnit.id")
	private Integer netUpliftUnitId;

	@DsField(join = "left", path = "netUpliftUnit.code")
	private String netUpliftUnitCode;

	@DsField(join = "left", path = "beforeFuelingUnit.id")
	private Integer beforeFuelingUnitId;

	@DsField(join = "left", path = "beforeFuelingUnit.code")
	private String beforeFuelingUnitCode;

	@DsField(join = "left", path = "afterFuelingUnit.id")
	private Integer afterFuelingUnitId;

	@DsField(join = "left", path = "afterFuelingUnit.code")
	private String afterFuelingUnitCode;

	@DsField(join = "left", path = "densityUnit.id")
	private Integer densityUnitId;

	@DsField(join = "left", path = "densityUnit.code")
	private String densityUnitCode;

	@DsField(join = "left", path = "densityVolume.id")
	private Integer densityVolumeId;

	@DsField(join = "left", path = "densityVolume.code")
	private String densityVolumeCode;

	@DsField(join = "left", path = "acType.id")
	private Integer acTypeId;

	@DsField(join = "left", path = "acType.code")
	private String acTypeCode;

	@DsField(join = "left", path = "registration.id")
	private Integer aircraftId;

	@DsField(join = "left", path = "registration.registration")
	private String aircraftReg;

	@DsField(join = "left", path = "reseller.id")
	private Integer resellerId;

	@DsField(join = "left", path = "reseller.code")
	private String resellerCode;

	@DsField(join = "left", path = "ammountCurr.id")
	private Integer amountCurrId;

	@DsField(join = "left", path = "ammountCurr.code")
	private String amountCurrCode;

	@DsField(join = "left", path = "referenceTicket.id")
	private Integer refTicketId;

	@DsField(join = "left", path = "referenceTicket.ticketNo")
	private String refTicketNo;

	@DsField
	private BigDecimal upliftVolume;

	@DsField
	private String ticketNo;

	@DsField
	private Date deliveryDate;

	@DsField
	private String airlineDesignator;

	@DsField
	private String flightNo;

	@DsField
	private Suffix suffix;

	@DsField
	private FuelTicketCustomsStatus customsStatus;

	@DsField
	private Product productType;

	@DsField
	private FuelTicketFuelingOperation fuelingOperation;

	@DsField
	private FlightTypeIndicator indicator;

	@DsField
	private BigDecimal netUpliftQuantity;

	@DsField
	private BigDecimal beforeFueling;

	@DsField
	private BigDecimal afterFueling;

	@DsField
	private BigDecimal density;

	@DsField
	private Date fuelingStartDate;

	@DsField
	private Date fuelingEndDate;

	@DsField
	private String deliveryType;

	@DsField
	private FuelTicketSource source;

	@DsField
	private FuelingType transport;

	@DsField
	private FuelTicketStatus ticketStatus;

	@DsField
	private FuelTicketTransmissionStatus transmissionStatus;

	@DsField
	private String transmissionDetails;

	@DsField
	private FuelTicketApprovalStatus approvalStatus;

	@DsField
	private Date transmissionDate;

	@DsField
	private String capturedBy;

	@DsField
	private String approvedBy;

	@DsField(fetch = false)
	private String toleranceMessage;

	@DsField
	private BillStatus billStatus;

	@DsField
	private OutgoingInvoiceStatus invoiceStatus;

	@DsField
	private Integer meterStartIndex;

	@DsField
	private Integer meterEndIndex;

	@DsField
	private Integer temperature;

	@DsField
	private TemperatureUnit temperatureUnit;

	@DsField
	private Date refuelerArrivalTime;

	@DsField
	private PaymentType paymentType;

	@DsField
	private String cardNumber;

	@DsField
	private Date cardExpiringDate;

	@DsField
	private String cardHolder;

	@DsField
	private BigDecimal amountReceived;

	@DsField
	private SendInvoiceBy sendInvoiceBy;

	@DsField
	private String sendInvoiceTo;

	@DsField
	private Boolean isDeleted;

	@DsField
	private FlightServiceType flightServiceType;

	@DsField
	private String subsidiaryId;

	@DsField
	private String subsidiaryCode;

	@DsField(path = "revocationRason")
	private RevocationReason revocationReason;

	@DsField(fetch = false)
	private String tmp;

	@DsField(fetch = false)
	private String formTitle;

	@DsField(fetch = false)
	private String flightID;

	@DsField(fetch = false)
	private Boolean isHardCopy;

	@DsField(fetch = false)
	private String labelField;

	@DsField(fetch = false)
	private Boolean canBeCompleted;

	@DsField(fetch = false)
	private String cancelWindowText;

	/**
	 * Default constructor
	 */
	public FuelTicket_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelTicket_Ds(FuelTicket e) {
		super(e);
	}

	public Integer getDepId() {
		return this.depId;
	}

	public void setDepId(Integer depId) {
		this.depId = depId;
	}

	public String getDepCode() {
		return this.depCode;
	}

	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}

	public Integer getCustId() {
		return this.custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getAoc() {
		return this.aoc;
	}

	public void setAoc(String aoc) {
		this.aoc = aoc;
	}

	public Integer getUpliftUnitId() {
		return this.upliftUnitId;
	}

	public void setUpliftUnitId(Integer upliftUnitId) {
		this.upliftUnitId = upliftUnitId;
	}

	public String getUpliftUnitCode() {
		return this.upliftUnitCode;
	}

	public void setUpliftUnitCode(String upliftUnitCode) {
		this.upliftUnitCode = upliftUnitCode;
	}

	public Integer getSuppId() {
		return this.suppId;
	}

	public void setSuppId(Integer suppId) {
		this.suppId = suppId;
	}

	public String getSuppCode() {
		return this.suppCode;
	}

	public void setSuppCode(String suppCode) {
		this.suppCode = suppCode;
	}

	public Integer getFuellerId() {
		return this.fuellerId;
	}

	public void setFuellerId(Integer fuellerId) {
		this.fuellerId = fuellerId;
	}

	public String getFuellerCode() {
		return this.fuellerCode;
	}

	public void setFuellerCode(String fuellerCode) {
		this.fuellerCode = fuellerCode;
	}

	public Integer getDestId() {
		return this.destId;
	}

	public void setDestId(Integer destId) {
		this.destId = destId;
	}

	public String getDestCode() {
		return this.destCode;
	}

	public void setDestCode(String destCode) {
		this.destCode = destCode;
	}

	public Integer getFinalDestId() {
		return this.finalDestId;
	}

	public void setFinalDestId(Integer finalDestId) {
		this.finalDestId = finalDestId;
	}

	public String getFinalDestCode() {
		return this.finalDestCode;
	}

	public void setFinalDestCode(String finalDestCode) {
		this.finalDestCode = finalDestCode;
	}

	public Integer getNetUpliftUnitId() {
		return this.netUpliftUnitId;
	}

	public void setNetUpliftUnitId(Integer netUpliftUnitId) {
		this.netUpliftUnitId = netUpliftUnitId;
	}

	public String getNetUpliftUnitCode() {
		return this.netUpliftUnitCode;
	}

	public void setNetUpliftUnitCode(String netUpliftUnitCode) {
		this.netUpliftUnitCode = netUpliftUnitCode;
	}

	public Integer getBeforeFuelingUnitId() {
		return this.beforeFuelingUnitId;
	}

	public void setBeforeFuelingUnitId(Integer beforeFuelingUnitId) {
		this.beforeFuelingUnitId = beforeFuelingUnitId;
	}

	public String getBeforeFuelingUnitCode() {
		return this.beforeFuelingUnitCode;
	}

	public void setBeforeFuelingUnitCode(String beforeFuelingUnitCode) {
		this.beforeFuelingUnitCode = beforeFuelingUnitCode;
	}

	public Integer getAfterFuelingUnitId() {
		return this.afterFuelingUnitId;
	}

	public void setAfterFuelingUnitId(Integer afterFuelingUnitId) {
		this.afterFuelingUnitId = afterFuelingUnitId;
	}

	public String getAfterFuelingUnitCode() {
		return this.afterFuelingUnitCode;
	}

	public void setAfterFuelingUnitCode(String afterFuelingUnitCode) {
		this.afterFuelingUnitCode = afterFuelingUnitCode;
	}

	public Integer getDensityUnitId() {
		return this.densityUnitId;
	}

	public void setDensityUnitId(Integer densityUnitId) {
		this.densityUnitId = densityUnitId;
	}

	public String getDensityUnitCode() {
		return this.densityUnitCode;
	}

	public void setDensityUnitCode(String densityUnitCode) {
		this.densityUnitCode = densityUnitCode;
	}

	public Integer getDensityVolumeId() {
		return this.densityVolumeId;
	}

	public void setDensityVolumeId(Integer densityVolumeId) {
		this.densityVolumeId = densityVolumeId;
	}

	public String getDensityVolumeCode() {
		return this.densityVolumeCode;
	}

	public void setDensityVolumeCode(String densityVolumeCode) {
		this.densityVolumeCode = densityVolumeCode;
	}

	public Integer getAcTypeId() {
		return this.acTypeId;
	}

	public void setAcTypeId(Integer acTypeId) {
		this.acTypeId = acTypeId;
	}

	public String getAcTypeCode() {
		return this.acTypeCode;
	}

	public void setAcTypeCode(String acTypeCode) {
		this.acTypeCode = acTypeCode;
	}

	public Integer getAircraftId() {
		return this.aircraftId;
	}

	public void setAircraftId(Integer aircraftId) {
		this.aircraftId = aircraftId;
	}

	public String getAircraftReg() {
		return this.aircraftReg;
	}

	public void setAircraftReg(String aircraftReg) {
		this.aircraftReg = aircraftReg;
	}

	public Integer getResellerId() {
		return this.resellerId;
	}

	public void setResellerId(Integer resellerId) {
		this.resellerId = resellerId;
	}

	public String getResellerCode() {
		return this.resellerCode;
	}

	public void setResellerCode(String resellerCode) {
		this.resellerCode = resellerCode;
	}

	public Integer getAmountCurrId() {
		return this.amountCurrId;
	}

	public void setAmountCurrId(Integer amountCurrId) {
		this.amountCurrId = amountCurrId;
	}

	public String getAmountCurrCode() {
		return this.amountCurrCode;
	}

	public void setAmountCurrCode(String amountCurrCode) {
		this.amountCurrCode = amountCurrCode;
	}

	public Integer getRefTicketId() {
		return this.refTicketId;
	}

	public void setRefTicketId(Integer refTicketId) {
		this.refTicketId = refTicketId;
	}

	public String getRefTicketNo() {
		return this.refTicketNo;
	}

	public void setRefTicketNo(String refTicketNo) {
		this.refTicketNo = refTicketNo;
	}

	public BigDecimal getUpliftVolume() {
		return this.upliftVolume;
	}

	public void setUpliftVolume(BigDecimal upliftVolume) {
		this.upliftVolume = upliftVolume;
	}

	public String getTicketNo() {
		return this.ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getAirlineDesignator() {
		return this.airlineDesignator;
	}

	public void setAirlineDesignator(String airlineDesignator) {
		this.airlineDesignator = airlineDesignator;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public FuelTicketCustomsStatus getCustomsStatus() {
		return this.customsStatus;
	}

	public void setCustomsStatus(FuelTicketCustomsStatus customsStatus) {
		this.customsStatus = customsStatus;
	}

	public Product getProductType() {
		return this.productType;
	}

	public void setProductType(Product productType) {
		this.productType = productType;
	}

	public FuelTicketFuelingOperation getFuelingOperation() {
		return this.fuelingOperation;
	}

	public void setFuelingOperation(FuelTicketFuelingOperation fuelingOperation) {
		this.fuelingOperation = fuelingOperation;
	}

	public FlightTypeIndicator getIndicator() {
		return this.indicator;
	}

	public void setIndicator(FlightTypeIndicator indicator) {
		this.indicator = indicator;
	}

	public BigDecimal getNetUpliftQuantity() {
		return this.netUpliftQuantity;
	}

	public void setNetUpliftQuantity(BigDecimal netUpliftQuantity) {
		this.netUpliftQuantity = netUpliftQuantity;
	}

	public BigDecimal getBeforeFueling() {
		return this.beforeFueling;
	}

	public void setBeforeFueling(BigDecimal beforeFueling) {
		this.beforeFueling = beforeFueling;
	}

	public BigDecimal getAfterFueling() {
		return this.afterFueling;
	}

	public void setAfterFueling(BigDecimal afterFueling) {
		this.afterFueling = afterFueling;
	}

	public BigDecimal getDensity() {
		return this.density;
	}

	public void setDensity(BigDecimal density) {
		this.density = density;
	}

	public Date getFuelingStartDate() {
		return this.fuelingStartDate;
	}

	public void setFuelingStartDate(Date fuelingStartDate) {
		this.fuelingStartDate = fuelingStartDate;
	}

	public Date getFuelingEndDate() {
		return this.fuelingEndDate;
	}

	public void setFuelingEndDate(Date fuelingEndDate) {
		this.fuelingEndDate = fuelingEndDate;
	}

	public String getDeliveryType() {
		return this.deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public FuelTicketSource getSource() {
		return this.source;
	}

	public void setSource(FuelTicketSource source) {
		this.source = source;
	}

	public FuelingType getTransport() {
		return this.transport;
	}

	public void setTransport(FuelingType transport) {
		this.transport = transport;
	}

	public FuelTicketStatus getTicketStatus() {
		return this.ticketStatus;
	}

	public void setTicketStatus(FuelTicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public FuelTicketTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			FuelTicketTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public String getTransmissionDetails() {
		return this.transmissionDetails;
	}

	public void setTransmissionDetails(String transmissionDetails) {
		this.transmissionDetails = transmissionDetails;
	}

	public FuelTicketApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(FuelTicketApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Date getTransmissionDate() {
		return this.transmissionDate;
	}

	public void setTransmissionDate(Date transmissionDate) {
		this.transmissionDate = transmissionDate;
	}

	public String getCapturedBy() {
		return this.capturedBy;
	}

	public void setCapturedBy(String capturedBy) {
		this.capturedBy = capturedBy;
	}

	public String getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getToleranceMessage() {
		return this.toleranceMessage;
	}

	public void setToleranceMessage(String toleranceMessage) {
		this.toleranceMessage = toleranceMessage;
	}

	public BillStatus getBillStatus() {
		return this.billStatus;
	}

	public void setBillStatus(BillStatus billStatus) {
		this.billStatus = billStatus;
	}

	public OutgoingInvoiceStatus getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(OutgoingInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public Integer getMeterStartIndex() {
		return this.meterStartIndex;
	}

	public void setMeterStartIndex(Integer meterStartIndex) {
		this.meterStartIndex = meterStartIndex;
	}

	public Integer getMeterEndIndex() {
		return this.meterEndIndex;
	}

	public void setMeterEndIndex(Integer meterEndIndex) {
		this.meterEndIndex = meterEndIndex;
	}

	public Integer getTemperature() {
		return this.temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public TemperatureUnit getTemperatureUnit() {
		return this.temperatureUnit;
	}

	public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
		this.temperatureUnit = temperatureUnit;
	}

	public Date getRefuelerArrivalTime() {
		return this.refuelerArrivalTime;
	}

	public void setRefuelerArrivalTime(Date refuelerArrivalTime) {
		this.refuelerArrivalTime = refuelerArrivalTime;
	}

	public PaymentType getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getCardExpiringDate() {
		return this.cardExpiringDate;
	}

	public void setCardExpiringDate(Date cardExpiringDate) {
		this.cardExpiringDate = cardExpiringDate;
	}

	public String getCardHolder() {
		return this.cardHolder;
	}

	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}

	public BigDecimal getAmountReceived() {
		return this.amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public SendInvoiceBy getSendInvoiceBy() {
		return this.sendInvoiceBy;
	}

	public void setSendInvoiceBy(SendInvoiceBy sendInvoiceBy) {
		this.sendInvoiceBy = sendInvoiceBy;
	}

	public String getSendInvoiceTo() {
		return this.sendInvoiceTo;
	}

	public void setSendInvoiceTo(String sendInvoiceTo) {
		this.sendInvoiceTo = sendInvoiceTo;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public FlightServiceType getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(FlightServiceType flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public String getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(String subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public RevocationReason getRevocationReason() {
		return this.revocationReason;
	}

	public void setRevocationReason(RevocationReason revocationReason) {
		this.revocationReason = revocationReason;
	}

	public String getTmp() {
		return this.tmp;
	}

	public void setTmp(String tmp) {
		this.tmp = tmp;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getFlightID() {
		return this.flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}

	public Boolean getIsHardCopy() {
		return this.isHardCopy;
	}

	public void setIsHardCopy(Boolean isHardCopy) {
		this.isHardCopy = isHardCopy;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}

	public Boolean getCanBeCompleted() {
		return this.canBeCompleted;
	}

	public void setCanBeCompleted(Boolean canBeCompleted) {
		this.canBeCompleted = canBeCompleted;
	}

	public String getCancelWindowText() {
		return this.cancelWindowText;
	}

	public void setCancelWindowText(String cancelWindowText) {
		this.cancelWindowText = cancelWindowText;
	}
}
