/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelOrderLocation.model;

import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.fmbas.domain.ext.json.message.SoneMessage;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.ops_type.FuelOrderStatus;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.PricePolicyType;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelOrderLocation.class, sort = {@SortField(field = FuelOrderLocation_Ds.F_LOCCODE)})
@RefLookups({@RefLookup(refId = FuelOrderLocation_Ds.F_CUSTOMERID)})
public class FuelOrderLocation_Ds extends AbstractDs_Ds<FuelOrderLocation> {

	public static final String ALIAS = "ops_FuelOrderLocation_Ds";

	public static final String F_FLIGHTTYPE = "flightType";
	public static final String F_FUELRELEASESTARTSON = "fuelReleaseStartsOn";
	public static final String F_FUELRELEASEENDSON = "fuelReleaseEndsOn";
	public static final String F_ORDERVERSION = "orderVersion";
	public static final String F_QUANTITY = "quantity";
	public static final String F_DIFFERENTIAL = "differential";
	public static final String F_FUELPRICE = "fuelPrice";
	public static final String F_BASEPRICE = "basePrice";
	public static final String F_SELLINGFUELPRICE = "sellingFuelPrice";
	public static final String F_INTOPLANEFEE = "intoPlaneFee";
	public static final String F_OTHERFEES = "otherFees";
	public static final String F_TAXES = "taxes";
	public static final String F_TOTALPRICEPERUOM = "totalPricePerUom";
	public static final String F_FLIGHTFEE = "flightFee";
	public static final String F_OPERATIONALTYPE = "operationalType";
	public static final String F_EVENTS = "events";
	public static final String F_PRODUCT = "product";
	public static final String F_PAYMENTTERMS = "paymentTerms";
	public static final String F_PAYMENTREFDATE = "paymentRefDate";
	public static final String F_INVOICEFREQ = "invoiceFreq";
	public static final String F_FOREX = "forex";
	public static final String F_EXCHANGERATEOFFSET = "exchangeRateOffset";
	public static final String F_WARNINGMESSAGE = "warningMessage";
	public static final String F_PRICEPOLICYTYPE = "pricePolicyType";
	public static final String F_PRICEPOLICYNAME = "pricePolicyName";
	public static final String F_PRICEPOLICYID = "pricePolicyId";
	public static final String F_AVERAGEMETHODID = "averageMethodId";
	public static final String F_AVERAGINGMETHODCODE = "averagingMethodCode";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FUELORDERID = "fuelOrderId";
	public static final String F_FUELORDERSTATUS = "fuelOrderStatus";
	public static final String F_ORDERCODE = "orderCode";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_SUPPID = "suppId";
	public static final String F_SUPPCODE = "suppCode";
	public static final String F_IPLID = "iplId";
	public static final String F_IPLAGENTCODE = "iplAgentCode";
	public static final String F_CURRID = "currId";
	public static final String F_CURRCODE = "currCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CONTRACTID = "contractId";
	public static final String F_CONTRACTCODE = "contractCode";
	public static final String F_PRICEBASIS = "priceBasis";
	public static final String F_FLIGHTEVENTID = "flightEventId";
	public static final String F_INDEXOFFSET = "indexOffset";
	public static final String F_DELIVERYMETHOD = "deliveryMethod";
	public static final String F_MARGIN = "margin";
	public static final String F_SEPARATOR = "separator";
	public static final String F_DAYS = "days";
	public static final String F_FUELRELEASEVP = "fuelReleaseVP";
	public static final String F_SUPPLYINFO = "supplyInfo";
	public static final String F_PRICINGINFO = "pricingInfo";
	public static final String F_PERCENT = "percent";
	public static final String F_INVOICESTATUS = "invoiceStatus";
	public static final String F_SUPPLIERACCRUALS = "supplierAccruals";
	public static final String F_CUSTOMERACCRUALS = "customerAccruals";
	public static final String F_SUPPLYOPT = "supplyOpt";

	@DsField
	private FlightTypeIndicator flightType;

	@DsField
	private Date fuelReleaseStartsOn;

	@DsField
	private Date fuelReleaseEndsOn;

	@DsField(path = "version")
	private Long orderVersion;

	@DsField
	private BigDecimal quantity;

	@DsField
	private BigDecimal differential;

	@DsField
	private BigDecimal fuelPrice;

	@DsField
	private BigDecimal basePrice;

	@DsField
	private BigDecimal sellingFuelPrice;

	@DsField(path = "intoPlaneFees")
	private BigDecimal intoPlaneFee;

	@DsField
	private BigDecimal otherFees;

	@DsField
	private BigDecimal taxes;

	@DsField
	private BigDecimal totalPricePerUom;

	@DsField(path = "perFlightFee")
	private BigDecimal flightFee;

	@DsField
	private OperationType operationalType;

	@DsField
	private Integer events;

	@DsField
	private Product product;

	@DsField
	private Integer paymentTerms;

	@DsField(path = "paymentRefDay")
	private PaymentDay paymentRefDate;

	@DsField(path = "invoiceFrequency")
	private InvoiceFreq invoiceFreq;

	@DsField(fetch = false)
	private String forex;

	@DsField
	private String exchangeRateOffset;

	@DsField(fetch = false)
	private SoneMessage warningMessage;

	@DsField
	private PricePolicyType pricePolicyType;

	@DsField(join = "left", path = "pricePolicy.name")
	private String pricePolicyName;

	@DsField(join = "left", path = "pricePolicy.id")
	private Integer pricePolicyId;

	@DsField(join = "left", path = "averageMethod.id")
	private Integer averageMethodId;

	@DsField(join = "left", path = "averageMethod.code")
	private String averagingMethodCode;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "fuelOrder.id")
	private Integer fuelOrderId;

	@DsField(join = "left", path = "fuelOrder.status")
	private FuelOrderStatus fuelOrderStatus;

	@DsField(join = "left", path = "fuelOrder.code")
	private String orderCode;

	@DsField(join = "left", path = "fuelOrder.customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "fuelOrder.customer.code")
	private String customerCode;

	@DsField(join = "left", path = "location.id")
	private Integer locId;

	@DsField(join = "left", path = "location.code")
	private String locCode;

	@DsField(join = "left", path = "supplier.id")
	private Integer suppId;

	@DsField(join = "left", path = "supplier.code")
	private String suppCode;

	@DsField(join = "left", path = "iplAgent.id")
	private Integer iplId;

	@DsField(join = "left", path = "iplAgent.code")
	private String iplAgentCode;

	@DsField(join = "left", path = "paymentCurrency.id")
	private Integer currId;

	@DsField(join = "left", path = "paymentCurrency.code")
	private String currCode;

	@DsField(join = "left", path = "paymentUnit.id")
	private Integer unitId;

	@DsField(join = "left", path = "paymentUnit.code")
	private String unitCode;

	@DsField(join = "left", path = "contract.id")
	private Integer contractId;

	@DsField(join = "left", path = "contract.code")
	private String contractCode;

	@DsField
	private PricingMethod priceBasis;

	@DsField(join = "left", path = "flightEvents.id")
	private Integer flightEventId;

	@DsField
	private String indexOffset;

	@DsField
	private ContractSubType deliveryMethod;

	@DsField
	private BigDecimal margin;

	@DsField(fetch = false)
	private String separator;

	@DsField(fetch = false)
	private String days;

	@DsField(fetch = false)
	private String fuelReleaseVP;

	@DsField(fetch = false)
	private String supplyInfo;

	@DsField(fetch = false)
	private String pricingInfo;

	@DsField(fetch = false)
	private String percent;

	@DsField(fetch = false)
	private String invoiceStatus;

	@DsField(fetch = false)
	private String supplierAccruals;

	@DsField(fetch = false)
	private String customerAccruals;

	@DsField(fetch = false)
	private String supplyOpt;

	/**
	 * Default constructor
	 */
	public FuelOrderLocation_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelOrderLocation_Ds(FuelOrderLocation e) {
		super(e);
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}

	public Date getFuelReleaseStartsOn() {
		return this.fuelReleaseStartsOn;
	}

	public void setFuelReleaseStartsOn(Date fuelReleaseStartsOn) {
		this.fuelReleaseStartsOn = fuelReleaseStartsOn;
	}

	public Date getFuelReleaseEndsOn() {
		return this.fuelReleaseEndsOn;
	}

	public void setFuelReleaseEndsOn(Date fuelReleaseEndsOn) {
		this.fuelReleaseEndsOn = fuelReleaseEndsOn;
	}

	public Long getOrderVersion() {
		return this.orderVersion;
	}

	public void setOrderVersion(Long orderVersion) {
		this.orderVersion = orderVersion;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getDifferential() {
		return this.differential;
	}

	public void setDifferential(BigDecimal differential) {
		this.differential = differential;
	}

	public BigDecimal getFuelPrice() {
		return this.fuelPrice;
	}

	public void setFuelPrice(BigDecimal fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	public BigDecimal getBasePrice() {
		return this.basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getSellingFuelPrice() {
		return this.sellingFuelPrice;
	}

	public void setSellingFuelPrice(BigDecimal sellingFuelPrice) {
		this.sellingFuelPrice = sellingFuelPrice;
	}

	public BigDecimal getIntoPlaneFee() {
		return this.intoPlaneFee;
	}

	public void setIntoPlaneFee(BigDecimal intoPlaneFee) {
		this.intoPlaneFee = intoPlaneFee;
	}

	public BigDecimal getOtherFees() {
		return this.otherFees;
	}

	public void setOtherFees(BigDecimal otherFees) {
		this.otherFees = otherFees;
	}

	public BigDecimal getTaxes() {
		return this.taxes;
	}

	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}

	public BigDecimal getTotalPricePerUom() {
		return this.totalPricePerUom;
	}

	public void setTotalPricePerUom(BigDecimal totalPricePerUom) {
		this.totalPricePerUom = totalPricePerUom;
	}

	public BigDecimal getFlightFee() {
		return this.flightFee;
	}

	public void setFlightFee(BigDecimal flightFee) {
		this.flightFee = flightFee;
	}

	public OperationType getOperationalType() {
		return this.operationalType;
	}

	public void setOperationalType(OperationType operationalType) {
		this.operationalType = operationalType;
	}

	public Integer getEvents() {
		return this.events;
	}

	public void setEvents(Integer events) {
		this.events = events;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentRefDate() {
		return this.paymentRefDate;
	}

	public void setPaymentRefDate(PaymentDay paymentRefDate) {
		this.paymentRefDate = paymentRefDate;
	}

	public InvoiceFreq getInvoiceFreq() {
		return this.invoiceFreq;
	}

	public void setInvoiceFreq(InvoiceFreq invoiceFreq) {
		this.invoiceFreq = invoiceFreq;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public String getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(String exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public SoneMessage getWarningMessage() {
		return this.warningMessage;
	}

	public void setWarningMessage(SoneMessage warningMessage) {
		this.warningMessage = warningMessage;
	}

	public PricePolicyType getPricePolicyType() {
		return this.pricePolicyType;
	}

	public void setPricePolicyType(PricePolicyType pricePolicyType) {
		this.pricePolicyType = pricePolicyType;
	}

	public String getPricePolicyName() {
		return this.pricePolicyName;
	}

	public void setPricePolicyName(String pricePolicyName) {
		this.pricePolicyName = pricePolicyName;
	}

	public Integer getPricePolicyId() {
		return this.pricePolicyId;
	}

	public void setPricePolicyId(Integer pricePolicyId) {
		this.pricePolicyId = pricePolicyId;
	}

	public Integer getAverageMethodId() {
		return this.averageMethodId;
	}

	public void setAverageMethodId(Integer averageMethodId) {
		this.averageMethodId = averageMethodId;
	}

	public String getAveragingMethodCode() {
		return this.averagingMethodCode;
	}

	public void setAveragingMethodCode(String averagingMethodCode) {
		this.averagingMethodCode = averagingMethodCode;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public Integer getFuelOrderId() {
		return this.fuelOrderId;
	}

	public void setFuelOrderId(Integer fuelOrderId) {
		this.fuelOrderId = fuelOrderId;
	}

	public FuelOrderStatus getFuelOrderStatus() {
		return this.fuelOrderStatus;
	}

	public void setFuelOrderStatus(FuelOrderStatus fuelOrderStatus) {
		this.fuelOrderStatus = fuelOrderStatus;
	}

	public String getOrderCode() {
		return this.orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public Integer getSuppId() {
		return this.suppId;
	}

	public void setSuppId(Integer suppId) {
		this.suppId = suppId;
	}

	public String getSuppCode() {
		return this.suppCode;
	}

	public void setSuppCode(String suppCode) {
		this.suppCode = suppCode;
	}

	public Integer getIplId() {
		return this.iplId;
	}

	public void setIplId(Integer iplId) {
		this.iplId = iplId;
	}

	public String getIplAgentCode() {
		return this.iplAgentCode;
	}

	public void setIplAgentCode(String iplAgentCode) {
		this.iplAgentCode = iplAgentCode;
	}

	public Integer getCurrId() {
		return this.currId;
	}

	public void setCurrId(Integer currId) {
		this.currId = currId;
	}

	public String getCurrCode() {
		return this.currCode;
	}

	public void setCurrCode(String currCode) {
		this.currCode = currCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public PricingMethod getPriceBasis() {
		return this.priceBasis;
	}

	public void setPriceBasis(PricingMethod priceBasis) {
		this.priceBasis = priceBasis;
	}

	public Integer getFlightEventId() {
		return this.flightEventId;
	}

	public void setFlightEventId(Integer flightEventId) {
		this.flightEventId = flightEventId;
	}

	public String getIndexOffset() {
		return this.indexOffset;
	}

	public void setIndexOffset(String indexOffset) {
		this.indexOffset = indexOffset;
	}

	public ContractSubType getDeliveryMethod() {
		return this.deliveryMethod;
	}

	public void setDeliveryMethod(ContractSubType deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public BigDecimal getMargin() {
		return this.margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getDays() {
		return this.days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getFuelReleaseVP() {
		return this.fuelReleaseVP;
	}

	public void setFuelReleaseVP(String fuelReleaseVP) {
		this.fuelReleaseVP = fuelReleaseVP;
	}

	public String getSupplyInfo() {
		return this.supplyInfo;
	}

	public void setSupplyInfo(String supplyInfo) {
		this.supplyInfo = supplyInfo;
	}

	public String getPricingInfo() {
		return this.pricingInfo;
	}

	public void setPricingInfo(String pricingInfo) {
		this.pricingInfo = pricingInfo;
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public String getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getSupplierAccruals() {
		return this.supplierAccruals;
	}

	public void setSupplierAccruals(String supplierAccruals) {
		this.supplierAccruals = supplierAccruals;
	}

	public String getCustomerAccruals() {
		return this.customerAccruals;
	}

	public void setCustomerAccruals(String customerAccruals) {
		this.customerAccruals = customerAccruals;
	}

	public String getSupplyOpt() {
		return this.supplyOpt;
	}

	public void setSupplyOpt(String supplyOpt) {
		this.supplyOpt = supplyOpt;
	}
}
