/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuoteLocation.model;

import atraxo.cmm.domain.impl.cmm_type.ContractScope;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractSubsidiaryDs_Ds;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = Contract.class, sort = {@SortField(field = CustomerLocationContracts_Ds.F_COUNTERPARTYCODE)})
@RefLookups({
		@RefLookup(refId = CustomerLocationContracts_Ds.F_LOCID),
		@RefLookup(refId = CustomerLocationContracts_Ds.F_COUNTERPARTYID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomerLocationContracts_Ds.F_COUNTERPARTYCODE)}),
		@RefLookup(refId = CustomerLocationContracts_Ds.F_CURRENCYID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomerLocationContracts_Ds.F_CURRENCYCODE)}),
		@RefLookup(refId = CustomerLocationContracts_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = CustomerLocationContracts_Ds.F_UNITCODE)})})
public class CustomerLocationContracts_Ds
		extends
			AbstractSubsidiaryDs_Ds<Contract> {

	public static final String ALIAS = "ops_CustomerLocationContracts_Ds";

	public static final String F_LOCID = "locId";
	public static final String F_LOCNAME = "locName";
	public static final String F_COUNTERPARTYID = "counterPartyId";
	public static final String F_COUNTERPARTYCODE = "counterPartyCode";
	public static final String F_CODE = "code";
	public static final String F_TYPE = "type";
	public static final String F_SCOPE = "scope";
	public static final String F_DELIVERY = "delivery";
	public static final String F_PAYMENTTERMS = "paymentTerms";
	public static final String F_CREDITTERMS = "creditTerms";
	public static final String F_LIMITEDTO = "limitedTo";
	public static final String F_IPLAGENTCODE = "iplAgentCode";
	public static final String F_IPLAGENTID = "iplAgentId";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_PRICINGBASES = "pricingBases";
	public static final String F_PRICINGBASEID = "pricingBaseId";
	public static final String F_DFT = "dft";
	public static final String F_TOTALPRICE = "totalPrice";
	public static final String F_UNIT = "unit";
	public static final String F_EXPOSURE = "exposure";
	public static final String F_QUOTATIONNAME = "quotationName";
	public static final String F_QUOTATIONID = "quotationId";
	public static final String F_EXCHANGERATEOFFSET = "exchangeRateOffset";
	public static final String F_BASEPRICE = "basePrice";
	public static final String F_FUELPRICE = "fuelPrice";
	public static final String F_DIFFERENTIAL = "differential";
	public static final String F_INTOPLANEFEE = "intoPlaneFee";
	public static final String F_OTHERFEES = "otherFees";
	public static final String F_TAXES = "taxes";
	public static final String F_PERFLIGHTFEE = "perFlightFee";

	@DsField(join = "left", path = "location.id")
	private Integer locId;

	@DsField(join = "left", path = "location.name")
	private String locName;

	@DsField(join = "left", path = "supplier.id")
	private Integer counterPartyId;

	@DsField(join = "left", path = "supplier.code")
	private String counterPartyCode;

	@DsField
	private String code;

	@DsField
	private ContractType type;

	@DsField
	private ContractScope scope;

	@DsField(path = "subType")
	private ContractSubType delivery;

	@DsField
	private Integer paymentTerms;

	@DsField
	private CreditTerm creditTerms;

	@DsField
	private FlightTypeIndicator limitedTo;

	@DsField(join = "left", path = "intoPlaneAgent.code")
	private String iplAgentCode;

	@DsField(join = "left", path = "intoPlaneAgent.id")
	private Integer iplAgentId;

	@DsField(join = "left", path = "settlementCurr.id")
	private Integer currencyId;

	@DsField(join = "left", path = "settlementCurr.code")
	private String currencyCode;

	@DsField(join = "left", path = "settlementUnit.id")
	private Integer unitId;

	@DsField(join = "left", path = "settlementUnit.code")
	private String unitCode;

	@DsField(fetch = false)
	private String pricingBases;

	@DsField(fetch = false)
	private Integer pricingBaseId;

	@DsField(fetch = false)
	private BigDecimal dft;

	@DsField(fetch = false)
	private BigDecimal totalPrice;

	@DsField(fetch = false)
	private String unit;

	@DsField(fetch = false)
	private Integer exposure;

	@DsField(fetch = false)
	private String quotationName;

	@DsField(fetch = false)
	private Integer quotationId;

	@DsField(fetch = false)
	private String exchangeRateOffset;

	@DsField(fetch = false)
	private BigDecimal basePrice;

	@DsField(fetch = false)
	private BigDecimal fuelPrice;

	@DsField(fetch = false)
	private BigDecimal differential;

	@DsField(fetch = false)
	private BigDecimal intoPlaneFee;

	@DsField(fetch = false)
	private BigDecimal otherFees;

	@DsField(fetch = false)
	private BigDecimal taxes;

	@DsField(fetch = false)
	private BigDecimal perFlightFee;

	/**
	 * Default constructor
	 */
	public CustomerLocationContracts_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public CustomerLocationContracts_Ds(Contract e) {
		super(e);
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocName() {
		return this.locName;
	}

	public void setLocName(String locName) {
		this.locName = locName;
	}

	public Integer getCounterPartyId() {
		return this.counterPartyId;
	}

	public void setCounterPartyId(Integer counterPartyId) {
		this.counterPartyId = counterPartyId;
	}

	public String getCounterPartyCode() {
		return this.counterPartyCode;
	}

	public void setCounterPartyCode(String counterPartyCode) {
		this.counterPartyCode = counterPartyCode;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ContractType getType() {
		return this.type;
	}

	public void setType(ContractType type) {
		this.type = type;
	}

	public ContractScope getScope() {
		return this.scope;
	}

	public void setScope(ContractScope scope) {
		this.scope = scope;
	}

	public ContractSubType getDelivery() {
		return this.delivery;
	}

	public void setDelivery(ContractSubType delivery) {
		this.delivery = delivery;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public CreditTerm getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(CreditTerm creditTerms) {
		this.creditTerms = creditTerms;
	}

	public FlightTypeIndicator getLimitedTo() {
		return this.limitedTo;
	}

	public void setLimitedTo(FlightTypeIndicator limitedTo) {
		this.limitedTo = limitedTo;
	}

	public String getIplAgentCode() {
		return this.iplAgentCode;
	}

	public void setIplAgentCode(String iplAgentCode) {
		this.iplAgentCode = iplAgentCode;
	}

	public Integer getIplAgentId() {
		return this.iplAgentId;
	}

	public void setIplAgentId(Integer iplAgentId) {
		this.iplAgentId = iplAgentId;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getPricingBases() {
		return this.pricingBases;
	}

	public void setPricingBases(String pricingBases) {
		this.pricingBases = pricingBases;
	}

	public Integer getPricingBaseId() {
		return this.pricingBaseId;
	}

	public void setPricingBaseId(Integer pricingBaseId) {
		this.pricingBaseId = pricingBaseId;
	}

	public BigDecimal getDft() {
		return this.dft;
	}

	public void setDft(BigDecimal dft) {
		this.dft = dft;
	}

	public BigDecimal getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getExposure() {
		return this.exposure;
	}

	public void setExposure(Integer exposure) {
		this.exposure = exposure;
	}

	public String getQuotationName() {
		return this.quotationName;
	}

	public void setQuotationName(String quotationName) {
		this.quotationName = quotationName;
	}

	public Integer getQuotationId() {
		return this.quotationId;
	}

	public void setQuotationId(Integer quotationId) {
		this.quotationId = quotationId;
	}

	public String getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(String exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public BigDecimal getBasePrice() {
		return this.basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getFuelPrice() {
		return this.fuelPrice;
	}

	public void setFuelPrice(BigDecimal fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	public BigDecimal getDifferential() {
		return this.differential;
	}

	public void setDifferential(BigDecimal differential) {
		this.differential = differential;
	}

	public BigDecimal getIntoPlaneFee() {
		return this.intoPlaneFee;
	}

	public void setIntoPlaneFee(BigDecimal intoPlaneFee) {
		this.intoPlaneFee = intoPlaneFee;
	}

	public BigDecimal getOtherFees() {
		return this.otherFees;
	}

	public void setOtherFees(BigDecimal otherFees) {
		this.otherFees = otherFees;
	}

	public BigDecimal getTaxes() {
		return this.taxes;
	}

	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}

	public BigDecimal getPerFlightFee() {
		return this.perFlightFee;
	}

	public void setPerFlightFee(BigDecimal perFlightFee) {
		this.perFlightFee = perFlightFee;
	}
}
