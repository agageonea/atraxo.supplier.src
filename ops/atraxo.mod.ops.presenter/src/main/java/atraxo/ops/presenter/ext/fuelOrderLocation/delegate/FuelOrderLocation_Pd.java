package atraxo.ops.presenter.ext.fuelOrderLocation.delegate;

import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.presenter.impl.fuelOrderLocation.model.FuelOrderLocation_Ds;
import atraxo.ops.presenter.impl.fuelOrderLocation.model.FuelOrderLocation_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class FuelOrderLocation_Pd extends AbstractPresenterDelegate {

	public void changeSupplier(FuelOrderLocation_Ds ds, FuelOrderLocation_DsParam param) throws Exception {
		IFuelOrderLocationService service = (IFuelOrderLocationService) this.findEntityService(FuelOrderLocation.class);
		FuelOrderLocation fol = service.findById(ds.getId());
		service.changeSupplier(fol, param.getNewContractId(), param.getRemarks());
	}
}
