/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuoteLocation.model;

import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.ext.json.message.SoneMessage;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatus;
import atraxo.ops.domain.impl.ops_type.OperationType;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelQuoteLocation.class, sort = {@SortField(field = FuelQuoteLocation_Ds.F_LOCCODE)})
@RefLookups({
		@RefLookup(refId = FuelQuoteLocation_Ds.F_QUOTEID),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_LOCID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuoteLocation_Ds.F_LOCCODE)}),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_SUPPID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuoteLocation_Ds.F_SUPPCODE)}),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_CONTRACTID, namedQuery = Contract.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuoteLocation_Ds.F_CONTRACTCODE)}),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuoteLocation_Ds.F_UNITCODE)}),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_IPLID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuoteLocation_Ds.F_IPLAGENTCODE)}),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_QUOTATIONID, namedQuery = Quotation.NQ_FIND_BY_NAME, params = {@Param(name = "name", field = FuelQuoteLocation_Ds.F_QUOTATIONNAME)}),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_CURRID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelQuoteLocation_Ds.F_CURRCODE)}),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_AVERAGEMETHODID),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_FINANCIALSOURCEID),
		@RefLookup(refId = FuelQuoteLocation_Ds.F_PRICINGPOLICYID)})
public class FuelQuoteLocation_Ds extends AbstractDs_Ds<FuelQuoteLocation> {

	public static final String ALIAS = "ops_FuelQuoteLocation_Ds";

	public static final String F_SEPARATOR = "separator";
	public static final String F_DAYS = "days";
	public static final String F_FUELRELEASEVP = "fuelReleaseVP";
	public static final String F_SUPPLYINFO = "supplyInfo";
	public static final String F_PRICINGINFO = "pricingInfo";
	public static final String F_PRICINGOPT = "pricingOpt";
	public static final String F_SUPPLYOPT = "supplyOpt";
	public static final String F_PAYMENTUNIT = "paymentUnit";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCECODE = "financialSourceCode";
	public static final String F_AVERAGEMETHODID = "averageMethodId";
	public static final String F_AVERAGEMETHODCODE = "averageMethodCode";
	public static final String F_AVERAGEMETHODNAME = "averageMethodName";
	public static final String F_QUOTEID = "quoteId";
	public static final String F_QUOTECODE = "quoteCode";
	public static final String F_QUOTESTATUS = "quoteStatus";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CONTRACTID = "contractId";
	public static final String F_CONTRACTCODE = "contractCode";
	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_SUPPID = "suppId";
	public static final String F_SUPPCODE = "suppCode";
	public static final String F_PRICINGPOLICYID = "pricingPolicyId";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_IPLID = "iplId";
	public static final String F_IPLAGENTCODE = "iplAgentCode";
	public static final String F_QUOTATIONID = "quotationId";
	public static final String F_QUOTATIONNAME = "quotationName";
	public static final String F_CURRID = "currId";
	public static final String F_CURRCODE = "currCode";
	public static final String F_PRICEBASIS = "priceBasis";
	public static final String F_FLIGHTTYPE = "flightType";
	public static final String F_OPERATIONALTYPE = "operationalType";
	public static final String F_PRODUCT = "product";
	public static final String F_QUOTEVERSION = "quoteVersion";
	public static final String F_DELIVERYMETHOD = "deliveryMethod";
	public static final String F_QUANTITY = "quantity";
	public static final String F_EVENTS = "events";
	public static final String F_INDEXOFFSET = "indexOffset";
	public static final String F_BASEPRICE = "basePrice";
	public static final String F_DIFFERENTIAL = "differential";
	public static final String F_FUELPRICE = "fuelPrice";
	public static final String F_INTOPLANEFEE = "intoplaneFee";
	public static final String F_OTHERFEES = "otherFees";
	public static final String F_TAXES = "taxes";
	public static final String F_FLIGHTFEE = "flightFee";
	public static final String F_TOTALPRICEPERUOM = "totalPricePerUom";
	public static final String F_PAYMENTTERMS = "paymentTerms";
	public static final String F_DISCLAIMER = "disclaimer";
	public static final String F_PAYMENTREFDATE = "paymentRefDate";
	public static final String F_INVOICEFREQ = "invoiceFreq";
	public static final String F_PERIOD = "period";
	public static final String F_FUELRELEASESTARTSON = "fuelReleaseStartsOn";
	public static final String F_FUELRELEASEENDSON = "fuelReleaseEndsOn";
	public static final String F_SETTLEMENTCURRENCYCODE = "settlementCurrencyCode";
	public static final String F_SETTLEMENTUNITCODE = "settlementUnitCode";
	public static final String F_FOREX = "forex";
	public static final String F_DFT = "dft";
	public static final String F_TOLERANCEMESSAGE = "toleranceMessage";
	public static final String F_CAPTUREDMARGIN = "capturedMargin";
	public static final String F_QUOTATION = "quotation";
	public static final String F_SUPPLIERUNITCODE = "supplierUnitCode";
	public static final String F_RESELLERDIFF = "resellerDiff";
	public static final String F_SELECTPRICEPOLICY = "selectPricePolicy";
	public static final String F_CAPTUREPRICEPOLICY = "capturePricePolicy";
	public static final String F_SELLINGPRICE = "sellingPrice";
	public static final String F_SELECTEDMARKUP = "selectedMarkup";
	public static final String F_CAPTUREDMARKUP = "capturedMarkup";
	public static final String F_SELECTEDPP = "selectedPP";
	public static final String F_CAPTUREDPP = "capturedPP";
	public static final String F_TEEXT = "teext";
	public static final String F_PRICEPOLICY = "pricePolicy";
	public static final String F_PERCENT = "percent";
	public static final String F_SELECTMARKUP = "selectMarkup";
	public static final String F_SELECTMARGIN = "selectMargin";
	public static final String F_ISFLIGHTEVENT = "isFlightEvent";

	@DsField(fetch = false)
	private String separator;

	@DsField(fetch = false)
	private String days;

	@DsField(fetch = false)
	private String fuelReleaseVP;

	@DsField(fetch = false)
	private String supplyInfo;

	@DsField(fetch = false)
	private String pricingInfo;

	@DsField(fetch = false)
	private String pricingOpt;

	@DsField(fetch = false)
	private String supplyOpt;

	@DsField(fetch = false)
	private String paymentUnit;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "financialSource.code")
	private String financialSourceCode;

	@DsField(join = "left", path = "averageMethod.id")
	private Integer averageMethodId;

	@DsField(join = "left", path = "averageMethod.code")
	private String averageMethodCode;

	@DsField(join = "left", path = "averageMethod.name")
	private String averageMethodName;

	@DsField(join = "left", path = "quote.id")
	private Integer quoteId;

	@DsField(join = "left", path = "quote.code")
	private String quoteCode;

	@DsField(join = "left", path = "quote.status")
	private FuelQuoteStatus quoteStatus;

	@DsField(join = "left", path = "quote.customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "quote.customer.code")
	private String customerCode;

	@DsField(join = "left", path = "contract.id")
	private Integer contractId;

	@DsField(join = "left", path = "contract.code")
	private String contractCode;

	@DsField(join = "left", path = "location.id")
	private Integer locId;

	@DsField(join = "left", path = "location.code")
	private String locCode;

	@DsField(join = "left", path = "supplier.id")
	private Integer suppId;

	@DsField(join = "left", path = "supplier.code")
	private String suppCode;

	@DsField(join = "left", path = "pricingPolicy.id")
	private Integer pricingPolicyId;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "intoPlaneAgent.id")
	private Integer iplId;

	@DsField(join = "left", path = "intoPlaneAgent.code")
	private String iplAgentCode;

	@DsField(join = "left", path = "quotation.id")
	private Integer quotationId;

	@DsField(join = "left", path = "quotation.name")
	private String quotationName;

	@DsField(join = "left", path = "currency.id")
	private Integer currId;

	@DsField(join = "left", path = "currency.code")
	private String currCode;

	@DsField
	private PricingMethod priceBasis;

	@DsField(path = "eventType")
	private FlightTypeIndicator flightType;

	@DsField
	private OperationType operationalType;

	@DsField
	private Product product;

	@DsField
	private String quoteVersion;

	@DsField
	private ContractSubType deliveryMethod;

	@DsField
	private BigDecimal quantity;

	@DsField
	private Integer events;

	@DsField
	private String indexOffset;

	@DsField
	private BigDecimal basePrice;

	@DsField
	private BigDecimal differential;

	@DsField
	private BigDecimal fuelPrice;

	@DsField
	private BigDecimal intoplaneFee;

	@DsField
	private BigDecimal otherFees;

	@DsField
	private BigDecimal taxes;

	@DsField
	private BigDecimal flightFee;

	@DsField
	private BigDecimal totalPricePerUom;

	@DsField
	private Integer paymentTerms;

	@DsField
	private String disclaimer;

	@DsField
	private PaymentDay paymentRefDate;

	@DsField
	private InvoiceFreq invoiceFreq;

	@DsField
	private MasterAgreementsPeriod period;

	@DsField
	private Date fuelReleaseStartsOn;

	@DsField
	private Date fuelReleaseEndsOn;

	@DsField
	private String settlementCurrencyCode;

	@DsField
	private String settlementUnitCode;

	@DsField(fetch = false)
	private String forex;

	@DsField
	private BigDecimal dft;

	@DsField(fetch = false)
	private SoneMessage toleranceMessage;

	@DsField(path = "margin")
	private BigDecimal capturedMargin;

	@DsField(fetch = false)
	private String quotation;

	@DsField(fetch = false)
	private String supplierUnitCode;

	@DsField(fetch = false)
	private BigDecimal resellerDiff;

	@DsField(fetch = false)
	private Boolean selectPricePolicy;

	@DsField(fetch = false)
	private Boolean capturePricePolicy;

	@DsField(fetch = false)
	private BigDecimal sellingPrice;

	@DsField(fetch = false)
	private BigDecimal selectedMarkup;

	@DsField(fetch = false)
	private BigDecimal capturedMarkup;

	@DsField(fetch = false)
	private String selectedPP;

	@DsField(fetch = false)
	private String capturedPP;

	@DsField(fetch = false)
	private String teext;

	@DsField(fetch = false)
	private String pricePolicy;

	@DsField(fetch = false)
	private String percent;

	@DsField(fetch = false)
	private Boolean selectMarkup;

	@DsField(fetch = false)
	private Boolean selectMargin;

	@DsField(fetch = false)
	private Boolean isFlightEvent;

	/**
	 * Default constructor
	 */
	public FuelQuoteLocation_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelQuoteLocation_Ds(FuelQuoteLocation e) {
		super(e);
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getDays() {
		return this.days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getFuelReleaseVP() {
		return this.fuelReleaseVP;
	}

	public void setFuelReleaseVP(String fuelReleaseVP) {
		this.fuelReleaseVP = fuelReleaseVP;
	}

	public String getSupplyInfo() {
		return this.supplyInfo;
	}

	public void setSupplyInfo(String supplyInfo) {
		this.supplyInfo = supplyInfo;
	}

	public String getPricingInfo() {
		return this.pricingInfo;
	}

	public void setPricingInfo(String pricingInfo) {
		this.pricingInfo = pricingInfo;
	}

	public String getPricingOpt() {
		return this.pricingOpt;
	}

	public void setPricingOpt(String pricingOpt) {
		this.pricingOpt = pricingOpt;
	}

	public String getSupplyOpt() {
		return this.supplyOpt;
	}

	public void setSupplyOpt(String supplyOpt) {
		this.supplyOpt = supplyOpt;
	}

	public String getPaymentUnit() {
		return this.paymentUnit;
	}

	public void setPaymentUnit(String paymentUnit) {
		this.paymentUnit = paymentUnit;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceCode() {
		return this.financialSourceCode;
	}

	public void setFinancialSourceCode(String financialSourceCode) {
		this.financialSourceCode = financialSourceCode;
	}

	public Integer getAverageMethodId() {
		return this.averageMethodId;
	}

	public void setAverageMethodId(Integer averageMethodId) {
		this.averageMethodId = averageMethodId;
	}

	public String getAverageMethodCode() {
		return this.averageMethodCode;
	}

	public void setAverageMethodCode(String averageMethodCode) {
		this.averageMethodCode = averageMethodCode;
	}

	public String getAverageMethodName() {
		return this.averageMethodName;
	}

	public void setAverageMethodName(String averageMethodName) {
		this.averageMethodName = averageMethodName;
	}

	public Integer getQuoteId() {
		return this.quoteId;
	}

	public void setQuoteId(Integer quoteId) {
		this.quoteId = quoteId;
	}

	public String getQuoteCode() {
		return this.quoteCode;
	}

	public void setQuoteCode(String quoteCode) {
		this.quoteCode = quoteCode;
	}

	public FuelQuoteStatus getQuoteStatus() {
		return this.quoteStatus;
	}

	public void setQuoteStatus(FuelQuoteStatus quoteStatus) {
		this.quoteStatus = quoteStatus;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public String getContractCode() {
		return this.contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public Integer getSuppId() {
		return this.suppId;
	}

	public void setSuppId(Integer suppId) {
		this.suppId = suppId;
	}

	public String getSuppCode() {
		return this.suppCode;
	}

	public void setSuppCode(String suppCode) {
		this.suppCode = suppCode;
	}

	public Integer getPricingPolicyId() {
		return this.pricingPolicyId;
	}

	public void setPricingPolicyId(Integer pricingPolicyId) {
		this.pricingPolicyId = pricingPolicyId;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getIplId() {
		return this.iplId;
	}

	public void setIplId(Integer iplId) {
		this.iplId = iplId;
	}

	public String getIplAgentCode() {
		return this.iplAgentCode;
	}

	public void setIplAgentCode(String iplAgentCode) {
		this.iplAgentCode = iplAgentCode;
	}

	public Integer getQuotationId() {
		return this.quotationId;
	}

	public void setQuotationId(Integer quotationId) {
		this.quotationId = quotationId;
	}

	public String getQuotationName() {
		return this.quotationName;
	}

	public void setQuotationName(String quotationName) {
		this.quotationName = quotationName;
	}

	public Integer getCurrId() {
		return this.currId;
	}

	public void setCurrId(Integer currId) {
		this.currId = currId;
	}

	public String getCurrCode() {
		return this.currCode;
	}

	public void setCurrCode(String currCode) {
		this.currCode = currCode;
	}

	public PricingMethod getPriceBasis() {
		return this.priceBasis;
	}

	public void setPriceBasis(PricingMethod priceBasis) {
		this.priceBasis = priceBasis;
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}

	public OperationType getOperationalType() {
		return this.operationalType;
	}

	public void setOperationalType(OperationType operationalType) {
		this.operationalType = operationalType;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getQuoteVersion() {
		return this.quoteVersion;
	}

	public void setQuoteVersion(String quoteVersion) {
		this.quoteVersion = quoteVersion;
	}

	public ContractSubType getDeliveryMethod() {
		return this.deliveryMethod;
	}

	public void setDeliveryMethod(ContractSubType deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Integer getEvents() {
		return this.events;
	}

	public void setEvents(Integer events) {
		this.events = events;
	}

	public String getIndexOffset() {
		return this.indexOffset;
	}

	public void setIndexOffset(String indexOffset) {
		this.indexOffset = indexOffset;
	}

	public BigDecimal getBasePrice() {
		return this.basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getDifferential() {
		return this.differential;
	}

	public void setDifferential(BigDecimal differential) {
		this.differential = differential;
	}

	public BigDecimal getFuelPrice() {
		return this.fuelPrice;
	}

	public void setFuelPrice(BigDecimal fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	public BigDecimal getIntoplaneFee() {
		return this.intoplaneFee;
	}

	public void setIntoplaneFee(BigDecimal intoplaneFee) {
		this.intoplaneFee = intoplaneFee;
	}

	public BigDecimal getOtherFees() {
		return this.otherFees;
	}

	public void setOtherFees(BigDecimal otherFees) {
		this.otherFees = otherFees;
	}

	public BigDecimal getTaxes() {
		return this.taxes;
	}

	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}

	public BigDecimal getFlightFee() {
		return this.flightFee;
	}

	public void setFlightFee(BigDecimal flightFee) {
		this.flightFee = flightFee;
	}

	public BigDecimal getTotalPricePerUom() {
		return this.totalPricePerUom;
	}

	public void setTotalPricePerUom(BigDecimal totalPricePerUom) {
		this.totalPricePerUom = totalPricePerUom;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getDisclaimer() {
		return this.disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public PaymentDay getPaymentRefDate() {
		return this.paymentRefDate;
	}

	public void setPaymentRefDate(PaymentDay paymentRefDate) {
		this.paymentRefDate = paymentRefDate;
	}

	public InvoiceFreq getInvoiceFreq() {
		return this.invoiceFreq;
	}

	public void setInvoiceFreq(InvoiceFreq invoiceFreq) {
		this.invoiceFreq = invoiceFreq;
	}

	public MasterAgreementsPeriod getPeriod() {
		return this.period;
	}

	public void setPeriod(MasterAgreementsPeriod period) {
		this.period = period;
	}

	public Date getFuelReleaseStartsOn() {
		return this.fuelReleaseStartsOn;
	}

	public void setFuelReleaseStartsOn(Date fuelReleaseStartsOn) {
		this.fuelReleaseStartsOn = fuelReleaseStartsOn;
	}

	public Date getFuelReleaseEndsOn() {
		return this.fuelReleaseEndsOn;
	}

	public void setFuelReleaseEndsOn(Date fuelReleaseEndsOn) {
		this.fuelReleaseEndsOn = fuelReleaseEndsOn;
	}

	public String getSettlementCurrencyCode() {
		return this.settlementCurrencyCode;
	}

	public void setSettlementCurrencyCode(String settlementCurrencyCode) {
		this.settlementCurrencyCode = settlementCurrencyCode;
	}

	public String getSettlementUnitCode() {
		return this.settlementUnitCode;
	}

	public void setSettlementUnitCode(String settlementUnitCode) {
		this.settlementUnitCode = settlementUnitCode;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public BigDecimal getDft() {
		return this.dft;
	}

	public void setDft(BigDecimal dft) {
		this.dft = dft;
	}

	public SoneMessage getToleranceMessage() {
		return this.toleranceMessage;
	}

	public void setToleranceMessage(SoneMessage toleranceMessage) {
		this.toleranceMessage = toleranceMessage;
	}

	public BigDecimal getCapturedMargin() {
		return this.capturedMargin;
	}

	public void setCapturedMargin(BigDecimal capturedMargin) {
		this.capturedMargin = capturedMargin;
	}

	public String getQuotation() {
		return this.quotation;
	}

	public void setQuotation(String quotation) {
		this.quotation = quotation;
	}

	public String getSupplierUnitCode() {
		return this.supplierUnitCode;
	}

	public void setSupplierUnitCode(String supplierUnitCode) {
		this.supplierUnitCode = supplierUnitCode;
	}

	public BigDecimal getResellerDiff() {
		return this.resellerDiff;
	}

	public void setResellerDiff(BigDecimal resellerDiff) {
		this.resellerDiff = resellerDiff;
	}

	public Boolean getSelectPricePolicy() {
		return this.selectPricePolicy;
	}

	public void setSelectPricePolicy(Boolean selectPricePolicy) {
		this.selectPricePolicy = selectPricePolicy;
	}

	public Boolean getCapturePricePolicy() {
		return this.capturePricePolicy;
	}

	public void setCapturePricePolicy(Boolean capturePricePolicy) {
		this.capturePricePolicy = capturePricePolicy;
	}

	public BigDecimal getSellingPrice() {
		return this.sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public BigDecimal getSelectedMarkup() {
		return this.selectedMarkup;
	}

	public void setSelectedMarkup(BigDecimal selectedMarkup) {
		this.selectedMarkup = selectedMarkup;
	}

	public BigDecimal getCapturedMarkup() {
		return this.capturedMarkup;
	}

	public void setCapturedMarkup(BigDecimal capturedMarkup) {
		this.capturedMarkup = capturedMarkup;
	}

	public String getSelectedPP() {
		return this.selectedPP;
	}

	public void setSelectedPP(String selectedPP) {
		this.selectedPP = selectedPP;
	}

	public String getCapturedPP() {
		return this.capturedPP;
	}

	public void setCapturedPP(String capturedPP) {
		this.capturedPP = capturedPP;
	}

	public String getTeext() {
		return this.teext;
	}

	public void setTeext(String teext) {
		this.teext = teext;
	}

	public String getPricePolicy() {
		return this.pricePolicy;
	}

	public void setPricePolicy(String pricePolicy) {
		this.pricePolicy = pricePolicy;
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public Boolean getSelectMarkup() {
		return this.selectMarkup;
	}

	public void setSelectMarkup(Boolean selectMarkup) {
		this.selectMarkup = selectMarkup;
	}

	public Boolean getSelectMargin() {
		return this.selectMargin;
	}

	public void setSelectMargin(Boolean selectMargin) {
		this.selectMargin = selectMargin;
	}

	public Boolean getIsFlightEvent() {
		return this.isFlightEvent;
	}

	public void setIsFlightEvent(Boolean isFlightEvent) {
		this.isFlightEvent = isFlightEvent;
	}
}
