/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelQuoteLocation.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.ops.domain.ext.fuelQuoteLocation.CapturedPriceInformationResult;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.presenter.impl.fuelQuoteLocation.model.FuelQuoteLocation_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelQuoteLocation_DsService extends AbstractEntityDsService<FuelQuoteLocation_Ds, FuelQuoteLocation_Ds, Object, FuelQuoteLocation>
		implements IDsService<FuelQuoteLocation_Ds, FuelQuoteLocation_Ds, Object> {

	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

	@Override
	protected void postInsert(List<FuelQuoteLocation_Ds> list, Object params) throws Exception {
		super.postInsert(list, params);
		this.setSpecificInfoOnDs(list);
	}

	@Override
	protected void postUpdate(List<FuelQuoteLocation_Ds> list, Object params) throws Exception {
		super.postUpdate(list, params);
		this.setSpecificInfoOnDs(list);
	}

	private void setSpecificInfoOnDs(List<FuelQuoteLocation_Ds> list) {
		for (FuelQuoteLocation_Ds ds : list) {
			if (CapturedPriceInformationResult.resultMap.get() != null && CapturedPriceInformationResult.resultMap.get().isEmpty()) {
				ds.setToleranceMessage(CapturedPriceInformationResult.resultMap.get().remove(ds.getId()));
			} else {
				ds.setToleranceMessage(null);
			}
			this.calculateContractDifferential(ds);
			this.fillPricingInformation(ds);
		}
	}

	@Override
	protected void preUpdate(FuelQuoteLocation_Ds ds, Object params) throws Exception {
		super.preUpdate(ds, params);
		this.unsetPricePolicy(ds);
		this.calculateDifferential(ds);
	}

	private void unsetPricePolicy(FuelQuoteLocation_Ds ds) {
		if (!ds.getSelectPricePolicy()) {
			ds.setPricingPolicyId(null);
		}
	}

	private void calculateDifferential(FuelQuoteLocation_Ds ds) {
		if (ds.getPricingPolicyId() != null) {
			return;
		}
		if (ds.getSelectMarkup()) {
			ds.setDifferential(ds.getCapturedMarkup());
			ds.setCapturedMargin(null);
		} else if (ds.getFuelPrice() != null && ds.getSelectMargin()) {
			if (ds.getCapturedMargin() != null) {
				ds.setDifferential(ds.getFuelPrice().multiply(ds.getCapturedMargin().divide(ONE_HUNDRED, MathContext.DECIMAL64)));
			} else {
				ds.setDifferential(BigDecimal.ZERO);
			}
		}

	}

	@Override
	protected void postFind(IQueryBuilder<FuelQuoteLocation_Ds, FuelQuoteLocation_Ds, Object> builder, List<FuelQuoteLocation_Ds> result)
			throws Exception {
		super.postFind(builder, result);
		for (FuelQuoteLocation_Ds fql : result) {
			this.calculateContractDifferential(fql);
			this.fillPricingInformation(fql);
			FuelQuoteLocation fuelQuoteLocation = fql._getEntity_();
			if (fuelQuoteLocation != null) {
				fql.setIsFlightEvent(fuelQuoteLocation.getFlightEvents() != null && fuelQuoteLocation.getFlightEvents().size() > 0 ? true : false);
			}
		}
	}

	private void calculateContractDifferential(FuelQuoteLocation_Ds fql) {
		if (fql.getFuelPrice() != null && fql.getBasePrice() != null) {
			fql.setResellerDiff(fql.getDifferential() != null ? fql.getDifferential() : BigDecimal.ZERO);
			if (!PricingMethod._EMPTY_.equals(fql.getPriceBasis()) && PricingMethod._INDEX_.equals(fql.getPriceBasis())) {
				fql.setResellerDiff(fql.getResellerDiff().add(fql.getFuelPrice().subtract(fql.getBasePrice())));
			}
		} else {
			fql.setResellerDiff(BigDecimal.ZERO);
		}
	}

	private void fillPricingInformation(FuelQuoteLocation_Ds fql) {
		if (fql.getDifferential() != null) {
			if (fql.getPricingPolicyId() != null) {
				fql.setSelectedMarkup(fql.getDifferential());
				fql.setSelectPricePolicy(true);
				fql.setSelectMarkup(false);
				fql.setSelectMargin(false);
				fql.setSelectMarkup(true);
			} else {
				fql.setSelectPricePolicy(false);
				fql.setCapturePricePolicy(true);
				if (fql.getCapturedMargin() != null) {
					fql.setSelectMargin(true);
					fql.setSelectMarkup(false);
				} else {
					fql.setSelectMargin(false);
					fql.setSelectMarkup(true);
					fql.setCapturedMarkup(fql.getDifferential());
				}
			}
			if (fql.getBasePrice() != null) {
				fql.setSellingPrice(fql.getBasePrice().add(fql.getResellerDiff()));
			}
		} else {
			fql.setSelectPricePolicy(true);
			fql.setSelectMarkup(false);
			fql.setSelectMargin(false);
			fql.setSelectMarkup(true);
		}
	}

}
