package atraxo.ops.presenter.ext.fuelQuotation.delegate;

import java.util.List;

import atraxo.ops.business.api.fuelQuotation.IFuelQuotationService;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatus;
import atraxo.ops.presenter.impl.fuelQuotation.model.FuelQuotation_Ds;
import atraxo.ops.presenter.impl.fuelQuotation.model.FuelQuotation_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class UpdateStatus_Pd extends AbstractPresenterDelegate {

	public void expire(FuelQuotation_Ds data) throws Exception {
		IFuelQuotationService service = (IFuelQuotationService) this.findEntityService(FuelQuotation.class);
		service.expire();
	}

	public void updateStatus(List<FuelQuotation_Ds> list, FuelQuotation_DsParam param) throws Exception {
		for (FuelQuotation_Ds ds : list) {
			this.updateStatus(ds, param);
		}
	}

	public void updateStatus(FuelQuotation_Ds ds, FuelQuotation_DsParam param) throws Exception {
		IFuelQuotationService service = (IFuelQuotationService) this.findEntityService(FuelQuotation.class);
		FuelQuotation fuelQuotation = service.findById(ds.getId());
		fuelQuotation.setStatus(FuelQuoteStatus.getByName(param.getAction()));
		service.updateStatus(fuelQuotation, param.getAction(), param.getRemarks());

	}

}
