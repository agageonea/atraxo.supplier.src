/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelOrder.model;

import atraxo.ops.domain.impl.ops_type.FuelOrderStatus;
/**
 * Generated code. Do not modify in this file.
 */
public class FuelOrder_DsParam {

	public static final String f_dummyField = "dummyField";
	public static final String f_action = "action";
	public static final String f_remarks = "remarks";
	public static final String f_paramStatus = "paramStatus";

	private String dummyField;

	private String action;

	private String remarks;

	private FuelOrderStatus paramStatus;

	public String getDummyField() {
		return this.dummyField;
	}

	public void setDummyField(String dummyField) {
		this.dummyField = dummyField;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public FuelOrderStatus getParamStatus() {
		return this.paramStatus;
	}

	public void setParamStatus(FuelOrderStatus paramStatus) {
		this.paramStatus = paramStatus;
	}
}
