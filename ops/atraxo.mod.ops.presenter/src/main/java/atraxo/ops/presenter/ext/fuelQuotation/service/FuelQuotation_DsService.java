/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelQuotation.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.exposure.IExposureService;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.presenter.impl.fuelQuotation.model.FuelQuotation_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelQuotation_DsService extends AbstractEntityDsService<FuelQuotation_Ds, FuelQuotation_Ds, Object, FuelQuotation>
		implements IDsService<FuelQuotation_Ds, FuelQuotation_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(FuelQuotation_DsService.class);

	@Override
	protected void preInsert(List<FuelQuotation_Ds> list, Object params) throws Exception {
		super.preInsert(list, params);
		this.setPaymentTerms(list, false);
	}

	@Override
	protected void postFind(IQueryBuilder<FuelQuotation_Ds, FuelQuotation_Ds, Object> builder, List<FuelQuotation_Ds> result) throws Exception {
		super.postFind(builder, result);

		ICustomerService custServ = (ICustomerService) this.findEntityService(Customer.class);
		Date date = Date.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
		// Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH); - Old code

		for (FuelQuotation_Ds quoteDs : result) {
			Customer cust = custServ.findById(quoteDs.getCustomerId());
			try {
				CreditLines cl = custServ.getValidCreditLine(cust, date);
				quoteDs.setAlertLimit(cl.getAlertPercentage());
			} catch (BusinessException e) {
				LOG.warn("Valid master agrrement not found for customer " + cust.getName() + " for date: " + date, e);
			}
		}
		this.setPaymentTerms(result, true);
		this.setPrimaryContact(result);
		this.setExposure(result);
	}

	private void setExposure(List<FuelQuotation_Ds> list) throws Exception {

		for (FuelQuotation_Ds fqDs : list) {
			try {
				FuelQuotation fuelQuotation = fqDs._getEntity_();
				Customer customer = fuelQuotation.getCustomer();
				ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);
				CreditLines cl = customerService.getValidCreditLine(customer, new Date());
				IExposureService exposureService = (IExposureService) this.findEntityService(Exposure.class);
				Exposure exposure = exposureService.getExposure(cl);
				BigDecimal creditLimit = fqDs.getCreditLimit();
				if (creditLimit == null || fqDs.getCreditCurrency() == null) {
					continue;
				}
				IExchangeRateService exRate = (IExchangeRateService) this.findEntityService(ExchangeRate.class);
				ICurrenciesService currService = (ICurrenciesService) this.findEntityService(Currencies.class);
				Currencies toCurrency = currService.findByCode(fqDs.getCreditCurrency());
				ConversionResult conversionResult = exRate.convertExchangeRate(this.getSysCurrencies(), toCurrency, new Date(),
						exposure.getActualUtilization(), false);

				// Added if check in case the credit limit is 0, to avoid DivideByZero and Division undefined
				if (creditLimit.compareTo(BigDecimal.ZERO) != 0) {
					BigDecimal result1 = conversionResult.getValue().multiply(BigDecimal.valueOf(100)).divide(creditLimit, MathContext.DECIMAL64);
					result1 = result1.setScale(2, BigDecimal.ROUND_CEILING);
					fqDs.setExposure(result1);
				}
			} catch (BusinessException e) {
				LOG.warn("Exposure calculation on fuel quotation ds service, fuel quotation " + fqDs.getCode(), e);
			} catch (NullPointerException e) {
				LOG.warn("Exposure calculation on fuel quotation ds service, null point, fuel quotation " + fqDs.getCode(), e);
			}
		}

	}

	private Currencies getSysCurrencies() throws Exception {
		ICurrenciesService service = (ICurrenciesService) this.findEntityService(Currencies.class);
		return service.getSystemCurrency();
	}

	private void setPrimaryContact(List<FuelQuotation_Ds> result) throws Exception {
		ICustomerService service = (ICustomerService) this.findEntityService(Customer.class);
		for (FuelQuotation_Ds fuelQuotation : result) {
			Customer customer = fuelQuotation._getEntity_().getCustomer();
			try {
				Contacts primaryContact = service.getPrimaryContact(customer);
				fuelQuotation.setPrimaryClientId(primaryContact.getId());
				fuelQuotation.setPrimaryClientName(primaryContact.getFirstName() + " " + primaryContact.getLastName());
			} catch (BusinessException e) {
				if (LOG.isDebugEnabled()) {
					LOG.debug(e.getMessage(), e);
				}
				LOG.info("Primary contact not found!");

			}
		}

	}

	private void setPaymentTerms(List<FuelQuotation_Ds> result, boolean isPostFind) throws Exception {
		ICustomerService service = (ICustomerService) this.findEntityService(Customer.class);

		for (FuelQuotation_Ds pq : result) {
			Date date = new Date();
			if (pq.getPostedOn() != null) {
				date = pq.getPostedOn();
			}
			if (isPostFind) {
				try {
					Customer customer = service.findById(pq._getEntity_().getCustomer().getId());
					MasterAgreement agreement = service.getValidMasterAgreement(customer, date);
					CreditLines cl = service.getValidCreditLine(customer, date);
					this.setFuelQuotation(pq, agreement, cl);
				} catch (BusinessException be) {
					if (LOG.isDebugEnabled()) {
						LOG.debug(be.getMessage(), be);
					}
				}
			} else {
				ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);
				Customer customer = customerService.findById(pq.getCustomerId());
				MasterAgreement agreement = service.getValidMasterAgreement(customer, date);
				CreditLines cl = null;
				try {
					cl = service.getValidCreditLine(customer, date);
				} catch (BusinessException e) {
					LOG.info("Credit terms not exists. " + e.getMessage(), e);
				}
				this.setFuelQuotation(pq, agreement, cl);
			}
		}
	}

	private void setFuelQuotation(FuelQuotation_Ds pq, MasterAgreement agreement, CreditLines cl) {
		pq.setPaymentTerm(agreement.getPaymentTerms());
		pq.setReferenceTo(agreement.getReferenceTo().getName());
		pq.setInvFreq(agreement.getInvoiceFrequency().getName());
		pq.setPayCurr(agreement.getCurrency().getCode());
		pq.setPayCurrId(agreement.getCurrency().getId());
		pq.setAvgMthdCode(agreement.getAverageMethod().getName());
		pq.setAvgMthdId(agreement.getAverageMethod().getId());
		pq.setFinSrcCode(agreement.getFinancialsource().getCode());
		pq.setFinSrcId(agreement.getFinancialsource().getId());
		pq.setPeriod(agreement.getPeriod().getName());
		if (cl != null) {
			pq.setCreditTerms(cl.getCreditTerm().getName());
			pq.setCreditLimit(BigDecimal.valueOf(cl.getAmount()));
			pq.setCreditCurrency(cl.getCurrency().getCode());
		}
		pq.setCreditCard(agreement.getCreditCard());
		pq.setExposure(BigDecimal.ZERO);
	}

}
