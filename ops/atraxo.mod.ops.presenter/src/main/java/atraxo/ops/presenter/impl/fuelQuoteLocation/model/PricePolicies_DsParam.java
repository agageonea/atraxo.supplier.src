/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuoteLocation.model;

import java.math.BigDecimal;
/**
 * Generated code. Do not modify in this file.
 */
public class PricePolicies_DsParam {

	public static final String f_productPrice = "productPrice";
	public static final String f_dft = "dft";
	public static final String f_fuelPrice = "fuelPrice";
	public static final String f_fqLocationId = "fqLocationId";
	public static final String f_fqCustomerId = "fqCustomerId";
	public static final String f_unitCode = "unitCode";
	public static final String f_unitId = "unitId";
	public static final String f_currencyCode = "currencyCode";
	public static final String f_currecnyId = "currecnyId";
	public static final String f_finSrcId = "finSrcId";
	public static final String f_avgMthdId = "avgMthdId";
	public static final String f_period = "period";

	private BigDecimal productPrice;

	private BigDecimal dft;

	private BigDecimal fuelPrice;

	private Integer fqLocationId;

	private Integer fqCustomerId;

	private String unitCode;

	private Integer unitId;

	private String currencyCode;

	private Integer currecnyId;

	private Integer finSrcId;

	private Integer avgMthdId;

	private String period;

	public BigDecimal getProductPrice() {
		return this.productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		this.productPrice = productPrice;
	}

	public BigDecimal getDft() {
		return this.dft;
	}

	public void setDft(BigDecimal dft) {
		this.dft = dft;
	}

	public BigDecimal getFuelPrice() {
		return this.fuelPrice;
	}

	public void setFuelPrice(BigDecimal fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	public Integer getFqLocationId() {
		return this.fqLocationId;
	}

	public void setFqLocationId(Integer fqLocationId) {
		this.fqLocationId = fqLocationId;
	}

	public Integer getFqCustomerId() {
		return this.fqCustomerId;
	}

	public void setFqCustomerId(Integer fqCustomerId) {
		this.fqCustomerId = fqCustomerId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getCurrecnyId() {
		return this.currecnyId;
	}

	public void setCurrecnyId(Integer currecnyId) {
		this.currecnyId = currecnyId;
	}

	public Integer getFinSrcId() {
		return this.finSrcId;
	}

	public void setFinSrcId(Integer finSrcId) {
		this.finSrcId = finSrcId;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public String getPeriod() {
		return this.period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}
}
