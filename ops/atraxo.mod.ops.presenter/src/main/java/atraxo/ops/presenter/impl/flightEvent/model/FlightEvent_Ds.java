/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.flightEvent.model;

import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.ops_type.FlightEventStatus;
import atraxo.ops.domain.impl.ops_type.FuelRequestStatus;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.Suffix;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FlightEvent.class, sort = {@SortField(field = FlightEvent_Ds.F_UPLIFTDATE, desc = true)})
@RefLookups({
		@RefLookup(refId = FlightEvent_Ds.F_LOCQUOTEID),
		@RefLookup(refId = FlightEvent_Ds.F_FUELREQID),
		@RefLookup(refId = FlightEvent_Ds.F_LOCID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FlightEvent_Ds.F_LOCCODE)}),
		@RefLookup(refId = FlightEvent_Ds.F_DESTID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FlightEvent_Ds.F_DESTCODE)}),
		@RefLookup(refId = FlightEvent_Ds.F_ACTYPEID, namedQuery = AcTypes.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FlightEvent_Ds.F_ACTYPECODE)}),
		@RefLookup(refId = FlightEvent_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FlightEvent_Ds.F_UNITCODE)}),
		@RefLookup(refId = FlightEvent_Ds.F_ARRIVALFROMID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FlightEvent_Ds.F_ARRIVALFROMCODE)}),
		@RefLookup(refId = FlightEvent_Ds.F_OPERATORID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FlightEvent_Ds.F_OPERATORCODE)}),
		@RefLookup(refId = FlightEvent_Ds.F_HANDLERID, namedQuery = Suppliers.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FlightEvent_Ds.F_HANDLERCODE)}),
		@RefLookup(refId = FlightEvent_Ds.F_AIRCRAFTHBID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FlightEvent_Ds.F_AIRCRAFTHBCODE)})})
public class FlightEvent_Ds extends AbstractDs_Ds<FlightEvent> {

	public static final String ALIAS = "ops_FlightEvent_Ds";

	public static final String F_LOCQUOTEID = "locQuoteId";
	public static final String F_EVENTTP = "eventTp";
	public static final String F_OPERATIONTP = "operationTp";
	public static final String F_LOCATIONORDERID = "locationOrderId";
	public static final String F_LOCID = "locId";
	public static final String F_LOCCODE = "locCode";
	public static final String F_FUELREQID = "fuelReqId";
	public static final String F_FUELREQUESTCODE = "fuelRequestCode";
	public static final String F_FUELREQUESTSTATUS = "fuelRequestStatus";
	public static final String F_DESTID = "destId";
	public static final String F_DESTCODE = "destCode";
	public static final String F_ACTYPEID = "acTypeId";
	public static final String F_ACTYPECODE = "acTypeCode";
	public static final String F_TANKCAP = "tankCap";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_ARRIVALFROMID = "arrivalFromId";
	public static final String F_ARRIVALFROMCODE = "arrivalFromCode";
	public static final String F_OPERATORID = "operatorId";
	public static final String F_OPERATORCODE = "operatorCode";
	public static final String F_HANDLERID = "handlerId";
	public static final String F_HANDLERCODE = "handlerCode";
	public static final String F_AIRCRAFTHBID = "aircraftHBId";
	public static final String F_AIRCRAFTHBCODE = "aircraftHBCode";
	public static final String F_TANKCAPACITY = "tankCapacity";
	public static final String F_FLIGHTNO = "flightNo";
	public static final String F_REGISTRATION = "registration";
	public static final String F_QUANTITY = "quantity";
	public static final String F_PRODUCT = "product";
	public static final String F_PERCAPTAIN = "perCaptain";
	public static final String F_UPLIFTDATE = "upliftDate";
	public static final String F_ARRIVALDATE = "arrivalDate";
	public static final String F_DEPARTERDATE = "departerDate";
	public static final String F_CAPTAIN = "captain";
	public static final String F_EVENTTYPE = "eventType";
	public static final String F_OPERATIONTYPE = "operationType";
	public static final String F_AIRCRAFTCALLSIGN = "aircraftCallSign";
	public static final String F_TIMEREFERENCE = "timeReference";
	public static final String F_TOLERANCEMESSAGE = "toleranceMessage";
	public static final String F_STATUS = "status";
	public static final String F_EVENTS = "events";
	public static final String F_TOTALQUANTITY = "totalQuantity";
	public static final String F_SUFFIX = "suffix";
	public static final String F_BILLSTATUS = "billStatus";
	public static final String F_INVOICESTATUS = "invoiceStatus";
	public static final String F_NEWARRIVALDATE = "newArrivalDate";
	public static final String F_NEWDEPARTUREDATE = "newDepartureDate";
	public static final String F_NEWUPLIFTDATE = "newUpliftDate";
	public static final String F_NEWTIMEREFERENCE = "newTimeReference";
	public static final String F_SEPARATOR = "separator";
	public static final String F_FLIGHTDETAILS = "flightDetails";
	public static final String F_SCHEDULEDETAILS = "scheduleDetails";
	public static final String F_FUELINGDETAILS = "fuelingDetails";
	public static final String F_NEWSCHEDULE = "newSchedule";
	public static final String F_OLDSCHEDULE = "oldSchedule";

	@DsField(join = "left", path = "locQuote.id")
	private Integer locQuoteId;

	@DsField(join = "left", path = "locQuote.eventType")
	private FlightTypeIndicator eventTp;

	@DsField(join = "left", path = "locQuote.operationalType")
	private OperationType operationTp;

	@DsField(join = "left", path = "locOrder.id")
	private Integer locationOrderId;

	@DsField(join = "left", path = "departure.id")
	private Integer locId;

	@DsField(join = "left", path = "departure.code")
	private String locCode;

	@DsField(join = "left", path = "fuelRequest.id")
	private Integer fuelReqId;

	@DsField(join = "left", path = "fuelRequest.requestCode")
	private String fuelRequestCode;

	@DsField(join = "left", path = "fuelRequest.requestStatus")
	private FuelRequestStatus fuelRequestStatus;

	@DsField(join = "left", path = "destination.id")
	private Integer destId;

	@DsField(join = "left", path = "destination.code")
	private String destCode;

	@DsField(join = "left", path = "aircraftType.id")
	private Integer acTypeId;

	@DsField(join = "left", path = "aircraftType.code")
	private String acTypeCode;

	@DsField(join = "left", path = "aircraftType.tankCapacity")
	private Integer tankCap;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "arrivalFrom.id")
	private Integer arrivalFromId;

	@DsField(join = "left", path = "arrivalFrom.code")
	private String arrivalFromCode;

	@DsField(join = "left", path = "operator.id")
	private Integer operatorId;

	@DsField(join = "left", path = "operator.code")
	private String operatorCode;

	@DsField(join = "left", path = "handler.id")
	private Integer handlerId;

	@DsField(join = "left", path = "handler.code")
	private String handlerCode;

	@DsField(join = "left", path = "aircraftHomeBase.id")
	private Integer aircraftHBId;

	@DsField(join = "left", path = "aircraftHomeBase.code")
	private String aircraftHBCode;

	@DsField(join = "left", path = "aircraftType.tankCapacity")
	private Integer tankCapacity;

	@DsField(path = "flighNo")
	private String flightNo;

	@DsField
	private String registration;

	@DsField
	private BigDecimal quantity;

	@DsField
	private Product product;

	@DsField
	private Boolean perCaptain;

	@DsField
	private Date upliftDate;

	@DsField
	private Date arrivalDate;

	@DsField
	private Date departerDate;

	@DsField
	private String captain;

	@DsField
	private FlightTypeIndicator eventType;

	@DsField
	private OperationType operationType;

	@DsField
	private String aircraftCallSign;

	@DsField
	private Boolean timeReference;

	@DsField(fetch = false)
	private String toleranceMessage;

	@DsField
	private FlightEventStatus status;

	@DsField
	private Integer events;

	@DsField
	private BigDecimal totalQuantity;

	@DsField
	private Suffix suffix;

	@DsField
	private BillStatus billStatus;

	@DsField
	private OutgoingInvoiceStatus invoiceStatus;

	@DsField(fetch = false)
	private Date newArrivalDate;

	@DsField(fetch = false)
	private Date newDepartureDate;

	@DsField(fetch = false)
	private Date newUpliftDate;

	@DsField(fetch = false)
	private Boolean newTimeReference;

	@DsField(fetch = false)
	private String separator;

	@DsField(fetch = false)
	private String flightDetails;

	@DsField(fetch = false)
	private String scheduleDetails;

	@DsField(fetch = false)
	private String fuelingDetails;

	@DsField(fetch = false)
	private String newSchedule;

	@DsField(fetch = false)
	private String oldSchedule;

	/**
	 * Default constructor
	 */
	public FlightEvent_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FlightEvent_Ds(FlightEvent e) {
		super(e);
	}

	public Integer getLocQuoteId() {
		return this.locQuoteId;
	}

	public void setLocQuoteId(Integer locQuoteId) {
		this.locQuoteId = locQuoteId;
	}

	public FlightTypeIndicator getEventTp() {
		return this.eventTp;
	}

	public void setEventTp(FlightTypeIndicator eventTp) {
		this.eventTp = eventTp;
	}

	public OperationType getOperationTp() {
		return this.operationTp;
	}

	public void setOperationTp(OperationType operationTp) {
		this.operationTp = operationTp;
	}

	public Integer getLocationOrderId() {
		return this.locationOrderId;
	}

	public void setLocationOrderId(Integer locationOrderId) {
		this.locationOrderId = locationOrderId;
	}

	public Integer getLocId() {
		return this.locId;
	}

	public void setLocId(Integer locId) {
		this.locId = locId;
	}

	public String getLocCode() {
		return this.locCode;
	}

	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}

	public Integer getFuelReqId() {
		return this.fuelReqId;
	}

	public void setFuelReqId(Integer fuelReqId) {
		this.fuelReqId = fuelReqId;
	}

	public String getFuelRequestCode() {
		return this.fuelRequestCode;
	}

	public void setFuelRequestCode(String fuelRequestCode) {
		this.fuelRequestCode = fuelRequestCode;
	}

	public FuelRequestStatus getFuelRequestStatus() {
		return this.fuelRequestStatus;
	}

	public void setFuelRequestStatus(FuelRequestStatus fuelRequestStatus) {
		this.fuelRequestStatus = fuelRequestStatus;
	}

	public Integer getDestId() {
		return this.destId;
	}

	public void setDestId(Integer destId) {
		this.destId = destId;
	}

	public String getDestCode() {
		return this.destCode;
	}

	public void setDestCode(String destCode) {
		this.destCode = destCode;
	}

	public Integer getAcTypeId() {
		return this.acTypeId;
	}

	public void setAcTypeId(Integer acTypeId) {
		this.acTypeId = acTypeId;
	}

	public String getAcTypeCode() {
		return this.acTypeCode;
	}

	public void setAcTypeCode(String acTypeCode) {
		this.acTypeCode = acTypeCode;
	}

	public Integer getTankCap() {
		return this.tankCap;
	}

	public void setTankCap(Integer tankCap) {
		this.tankCap = tankCap;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getArrivalFromId() {
		return this.arrivalFromId;
	}

	public void setArrivalFromId(Integer arrivalFromId) {
		this.arrivalFromId = arrivalFromId;
	}

	public String getArrivalFromCode() {
		return this.arrivalFromCode;
	}

	public void setArrivalFromCode(String arrivalFromCode) {
		this.arrivalFromCode = arrivalFromCode;
	}

	public Integer getOperatorId() {
		return this.operatorId;
	}

	public void setOperatorId(Integer operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorCode() {
		return this.operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public Integer getHandlerId() {
		return this.handlerId;
	}

	public void setHandlerId(Integer handlerId) {
		this.handlerId = handlerId;
	}

	public String getHandlerCode() {
		return this.handlerCode;
	}

	public void setHandlerCode(String handlerCode) {
		this.handlerCode = handlerCode;
	}

	public Integer getAircraftHBId() {
		return this.aircraftHBId;
	}

	public void setAircraftHBId(Integer aircraftHBId) {
		this.aircraftHBId = aircraftHBId;
	}

	public String getAircraftHBCode() {
		return this.aircraftHBCode;
	}

	public void setAircraftHBCode(String aircraftHBCode) {
		this.aircraftHBCode = aircraftHBCode;
	}

	public Integer getTankCapacity() {
		return this.tankCapacity;
	}

	public void setTankCapacity(Integer tankCapacity) {
		this.tankCapacity = tankCapacity;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getRegistration() {
		return this.registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Boolean getPerCaptain() {
		return this.perCaptain;
	}

	public void setPerCaptain(Boolean perCaptain) {
		this.perCaptain = perCaptain;
	}

	public Date getUpliftDate() {
		return this.upliftDate;
	}

	public void setUpliftDate(Date upliftDate) {
		this.upliftDate = upliftDate;
	}

	public Date getArrivalDate() {
		return this.arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Date getDeparterDate() {
		return this.departerDate;
	}

	public void setDeparterDate(Date departerDate) {
		this.departerDate = departerDate;
	}

	public String getCaptain() {
		return this.captain;
	}

	public void setCaptain(String captain) {
		this.captain = captain;
	}

	public FlightTypeIndicator getEventType() {
		return this.eventType;
	}

	public void setEventType(FlightTypeIndicator eventType) {
		this.eventType = eventType;
	}

	public OperationType getOperationType() {
		return this.operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public String getAircraftCallSign() {
		return this.aircraftCallSign;
	}

	public void setAircraftCallSign(String aircraftCallSign) {
		this.aircraftCallSign = aircraftCallSign;
	}

	public Boolean getTimeReference() {
		return this.timeReference;
	}

	public void setTimeReference(Boolean timeReference) {
		this.timeReference = timeReference;
	}

	public String getToleranceMessage() {
		return this.toleranceMessage;
	}

	public void setToleranceMessage(String toleranceMessage) {
		this.toleranceMessage = toleranceMessage;
	}

	public FlightEventStatus getStatus() {
		return this.status;
	}

	public void setStatus(FlightEventStatus status) {
		this.status = status;
	}

	public Integer getEvents() {
		return this.events;
	}

	public void setEvents(Integer events) {
		this.events = events;
	}

	public BigDecimal getTotalQuantity() {
		return this.totalQuantity;
	}

	public void setTotalQuantity(BigDecimal totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public BillStatus getBillStatus() {
		return this.billStatus;
	}

	public void setBillStatus(BillStatus billStatus) {
		this.billStatus = billStatus;
	}

	public OutgoingInvoiceStatus getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(OutgoingInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public Date getNewArrivalDate() {
		return this.newArrivalDate;
	}

	public void setNewArrivalDate(Date newArrivalDate) {
		this.newArrivalDate = newArrivalDate;
	}

	public Date getNewDepartureDate() {
		return this.newDepartureDate;
	}

	public void setNewDepartureDate(Date newDepartureDate) {
		this.newDepartureDate = newDepartureDate;
	}

	public Date getNewUpliftDate() {
		return this.newUpliftDate;
	}

	public void setNewUpliftDate(Date newUpliftDate) {
		this.newUpliftDate = newUpliftDate;
	}

	public Boolean getNewTimeReference() {
		return this.newTimeReference;
	}

	public void setNewTimeReference(Boolean newTimeReference) {
		this.newTimeReference = newTimeReference;
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getFlightDetails() {
		return this.flightDetails;
	}

	public void setFlightDetails(String flightDetails) {
		this.flightDetails = flightDetails;
	}

	public String getScheduleDetails() {
		return this.scheduleDetails;
	}

	public void setScheduleDetails(String scheduleDetails) {
		this.scheduleDetails = scheduleDetails;
	}

	public String getFuelingDetails() {
		return this.fuelingDetails;
	}

	public void setFuelingDetails(String fuelingDetails) {
		this.fuelingDetails = fuelingDetails;
	}

	public String getNewSchedule() {
		return this.newSchedule;
	}

	public void setNewSchedule(String newSchedule) {
		this.newSchedule = newSchedule;
	}

	public String getOldSchedule() {
		return this.oldSchedule;
	}

	public void setOldSchedule(String oldSchedule) {
		this.oldSchedule = oldSchedule;
	}
}
