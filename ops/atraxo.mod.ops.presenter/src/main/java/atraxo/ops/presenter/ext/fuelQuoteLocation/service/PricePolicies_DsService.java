/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelQuoteLocation.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import atraxo.ops.business.api.pricePolicy.IPricePolicyService;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import atraxo.ops.presenter.impl.fuelQuoteLocation.model.PricePolicies_Ds;
import atraxo.ops.presenter.impl.fuelQuoteLocation.model.PricePolicies_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class PricePolicies_DsService extends AbstractEntityDsService<PricePolicies_Ds, PricePolicies_Ds, Object, PricePolicy> implements
		IDsService<PricePolicies_Ds, PricePolicies_Ds, Object> {

	private static final String DEFAULT_MARGIN = "Default margin";
	private static final String DEFAULT_MARKUP = "Default markup";
	private static final String ANY = "ANY";
	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	private static final String PERCENT_SIGN = "%";

	@Override
	protected void postFind(IQueryBuilder<PricePolicies_Ds, PricePolicies_Ds, Object> builder, List<PricePolicies_Ds> result) throws Exception {
		super.postFind(builder, result);
		List<PricePolicies_Ds> displayedResult = new ArrayList<PricePolicies_Ds>();
		PricePolicies_DsParam dsParam = (PricePolicies_DsParam) builder.getParams();
		for (PricePolicies_Ds pricePolicyDs : result) {
			PricePolicy pricePolicy = pricePolicyDs._getEntity_();
			if (this.shouldDisplay(pricePolicy, dsParam.getFqLocationId(), dsParam.getFqCustomerId())) {
				if (pricePolicy.getDefaultMargin() != null) {
					PricePolicies_Ds displayedPricePolicy = this.buildDisplayedPricePolicy(dsParam, pricePolicy, true);
					displayedResult.add(displayedPricePolicy);
				}
				if (pricePolicy.getDefaultMarkup() != null) {
					PricePolicies_Ds displayedPricePolicy = this.buildDisplayedPricePolicy(dsParam, pricePolicy, false);
					displayedResult.add(displayedPricePolicy);
				}
			}
		}
		result.clear();
		result.addAll(displayedResult);
		builder.putExtraAttribute("rec-count", new Long(displayedResult.size()));
	}

	@Override
	public Long count(IQueryBuilder<PricePolicies_Ds, PricePolicies_Ds, Object> builder) throws Exception {
		return (Long) builder.getExtraAttribute("rec-count");
	}

	private PricePolicies_Ds buildDisplayedPricePolicy(PricePolicies_DsParam dsParam, PricePolicy pricePolicy, boolean margin) throws Exception {
		PricePolicies_Ds displayedPricePolicy = new PricePolicies_Ds();
		displayedPricePolicy.setPricePolicyId(pricePolicy.getId());
		displayedPricePolicy.setPolicyName(pricePolicy.getName());
		if (pricePolicy.getLocation() != null) {
			displayedPricePolicy.setLocationCode(pricePolicy.getLocation().getCode());
			displayedPricePolicy.setLocationId(pricePolicy.getLocation().getId());
		} else {
			displayedPricePolicy.setLocationCode(ANY);

		}
		if (pricePolicy.getCustomer() != null) {
			displayedPricePolicy.setCustomerCode(pricePolicy.getCustomer().getCode());
			displayedPricePolicy.setCustomerId(pricePolicy.getCustomer().getId());
		} else {
			displayedPricePolicy.setCustomerCode(ANY);

		}
		if (pricePolicy.getAircraft() != null) {
			displayedPricePolicy.setAircraftType(pricePolicy.getAircraft().getCode());
			displayedPricePolicy.setAircraftTypeId(pricePolicy.getAircraft().getId());
		} else {
			displayedPricePolicy.setAircraftType(ANY);
		}
		if (margin) {
			displayedPricePolicy.setPolicyType(DEFAULT_MARGIN);
			displayedPricePolicy.setValue(pricePolicy.getDefaultMargin());
			displayedPricePolicy.setPolicyUnit(PERCENT_SIGN);
			displayedPricePolicy.setMarkup(dsParam.getFuelPrice().multiply(
					displayedPricePolicy.getValue().divide(ONE_HUNDRED, MathContext.DECIMAL64), MathContext.DECIMAL64));
		} else {
			displayedPricePolicy.setPolicyType(DEFAULT_MARKUP);
			displayedPricePolicy.setValue(pricePolicy.getDefaultMarkup());
			displayedPricePolicy.setPolicyUnit(pricePolicy.getCurrency().getCode() + "/" + pricePolicy.getUnit().getCode());
			IPricePolicyService service = (IPricePolicyService) this.findEntityService(PricePolicy.class);
			BigDecimal convertedMarkup = service.convert(pricePolicy.getUnit().getId(), dsParam.getUnitId(), pricePolicy.getCurrency().getId(),
					dsParam.getCurrecnyId(), displayedPricePolicy.getValue(), dsParam.getFinSrcId(), dsParam.getAvgMthdId(), dsParam.getPeriod());
			displayedPricePolicy.setMarkup(convertedMarkup);

		}
		displayedPricePolicy.setGridProductPrice(dsParam.getProductPrice());
		displayedPricePolicy.setSellingFuelPrice(displayedPricePolicy.getMarkup().add(dsParam.getFuelPrice()));
		displayedPricePolicy.setDifferential(dsParam.getFuelPrice().subtract(dsParam.getProductPrice()));
		displayedPricePolicy.setGridDft(dsParam.getDft());
		displayedPricePolicy.setTotalSellingPrice(displayedPricePolicy.getSellingFuelPrice().add(
				dsParam.getDft() != null ? dsParam.getDft() : BigDecimal.ZERO));
		displayedPricePolicy.setSellingUnit(dsParam.getCurrencyCode() + "/" + dsParam.getUnitCode());
		return displayedPricePolicy;
	}

	private boolean shouldDisplay(PricePolicy pricePolicy, int locationId, int customerId) {
		Calendar cal = GregorianCalendar.getInstance();
		if (!(cal.getTime().compareTo(pricePolicy.getValidFrom()) >= 0 && cal.getTime().compareTo(pricePolicy.getValidTo()) <= 0)) {
			return false;
		}
		if (!(pricePolicy.getLocation() == null || pricePolicy.getLocation().getId().equals(locationId))) {
			return false;
		}
		if (!(pricePolicy.getCustomer() == null || pricePolicy.getCustomer().getId().equals(customerId))) {
			return false;
		}
		return true;
	}
}
