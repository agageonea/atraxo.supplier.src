/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelOrder.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.exchangerate.IExchangeRateService;
import atraxo.fmbas.business.api.exposure.IExposureService;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.impl.creditLines.CreditLines;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.exchangerate.ExchangeRate;
import atraxo.fmbas.domain.impl.exposure.Exposure;
import atraxo.fmbas.presenter.ext.customer.service.Customers_DsService;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.presenter.impl.fuelOrder.model.FuelOrder_Ds;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelOrder_DsService extends AbstractEntityDsService<FuelOrder_Ds, FuelOrder_Ds, Object, FuelOrder>
		implements IDsService<FuelOrder_Ds, FuelOrder_Ds, Object> {

	private static final Logger LOG = LoggerFactory.getLogger(Customers_DsService.class);

	@Override
	protected void preInsert(List<FuelOrder_Ds> list, Object params) throws Exception {
		super.preInsert(list, params);
		this.setPaymentTerms(list, false);
	}

	@Override
	protected void preUpdate(List<FuelOrder_Ds> list, Object params) throws Exception {
		super.preUpdate(list, params);
		this.setPaymentTerms(list, false);
	}

	@Override
	protected void postFind(IQueryBuilder<FuelOrder_Ds, FuelOrder_Ds, Object> builder, List<FuelOrder_Ds> result) throws Exception {
		super.postFind(builder, result);
		this.setPaymentTerms(result, true);
		this.setExposure(result);

	}

	private void setPaymentTerms(List<FuelOrder_Ds> result, boolean isPostFind) throws Exception {
		ICustomerService service = (ICustomerService) this.findEntityService(Customer.class);

		for (FuelOrder_Ds pq : result) {
			Date date = new Date();
			if (pq.getPostedOn() != null) {
				date = pq.getPostedOn();
			}
			if (isPostFind) {
				try {
					CreditLines cl = service.getValidCreditLine(pq._getEntity_().getCustomer(), date);
					this.setFuelOrder(pq, cl);
					pq.setAlertLimit(cl.getAlertPercentage());
				} catch (BusinessException be) {
					LOG.error("Error", be);
				}
			} else {
				ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);
				Customer customer = customerService.findById(pq.getCustomerId());
				CreditLines cl = service.getValidCreditLine(customer, date);
				this.setFuelOrder(pq, cl);
			}
		}
	}

	private void setFuelOrder(FuelOrder_Ds pq, CreditLines cl) {
		pq.setCreditTerms(cl.getCreditTerm().getName());
		pq.setCreditLimit(BigDecimal.valueOf(cl.getAlertPercentage()));
		pq.setCreditCurrency(cl.getCurrency() == null ? "" : cl.getCurrency().getCode());
		// pq.setCreditCard(cl.getCreditCard());
		pq.setExposure(BigDecimal.ZERO);
	}

	private void setExposure(List<FuelOrder_Ds> dsResult) throws Exception {

		for (FuelOrder_Ds foDs : dsResult) {
			try {
				FuelOrder fuelOrder = foDs._getEntity_();
				Customer customer = fuelOrder.getCustomer();
				ICustomerService customerService = (ICustomerService) this.findEntityService(Customer.class);
				CreditLines masterAgreement = customerService.getValidCreditLine(customer, GregorianCalendar.getInstance().getTime());
				IExposureService exposureService = (IExposureService) this.findEntityService(Exposure.class);
				Exposure exposure = exposureService.getExposure(masterAgreement);
				BigDecimal creditLimit = foDs.getCreditLimit();
				if (creditLimit == null || foDs.getCreditCurrency() == null) {
					continue;
				}
				IExchangeRateService exRate = (IExchangeRateService) this.findEntityService(ExchangeRate.class);
				ICurrenciesService currService = (ICurrenciesService) this.findEntityService(Currencies.class);
				Currencies toCurrency = currService.findByCode(foDs.getCreditCurrency());
				ConversionResult conversionResult = exRate.convertExchangeRate(this.getSysCurrencies(), toCurrency, new Date(),
						exposure.getActualUtilization(), false);
				BigDecimal result1 = conversionResult.getValue().multiply(BigDecimal.valueOf(100)).divide(creditLimit, MathContext.DECIMAL64);
				result1 = result1.setScale(2, BigDecimal.ROUND_CEILING);
				foDs.setExposure(result1);
			} catch (BusinessException e) {
				LOG.warn("Exposure calculation on fuel order ds service, fuel order " + foDs.getCode(), e);
			} catch (NullPointerException e) {
				LOG.warn("Exposure calculation on fuel order ds service, null point, fuel order " + foDs.getCode(), e);
			}
		}

	}

	private Currencies getSysCurrencies() throws Exception {
		ICurrenciesService service = (ICurrenciesService) this.findEntityService(Currencies.class);
		return service.getSystemCurrency();
	}

}
