/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuoteLocation.model;

import atraxo.cmm.domain.impl.cmm_type.QuantityType;
import atraxo.cmm.domain.impl.cmm_type.VatApplicability;
import atraxo.cmm.domain.impl.prices.ContractPriceCategory;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.PriceType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = ContractPriceCategory.class, sort = {@SortField(field = PriceCategories_Ds.F_PRICECTGRYNAME)})
@RefLookups({@RefLookup(refId = PriceCategories_Ds.F_CONTRACTID),
		@RefLookup(refId = PriceCategories_Ds.F_PRICECATEGORY),
		@RefLookup(refId = PriceCategories_Ds.F_FINANCIALSOURCEID),
		@RefLookup(refId = PriceCategories_Ds.F_AVGMTHDID)})
public class PriceCategories_Ds extends AbstractDs_Ds<ContractPriceCategory> {

	public static final String ALIAS = "ops_PriceCategories_Ds";

	public static final String F_NAME = "name";
	public static final String F_CONTINOUS = "continous";
	public static final String F_INCLUDEINAVERAGE = "includeInAverage";
	public static final String F_QUANTITYTYPE = "quantityType";
	public static final String F_EXCHANGERATEOFFSET = "exchangeRateOffset";
	public static final String F_RESTRICTION = "restriction";
	public static final String F_VAT = "vat";
	public static final String F_DEFAULTPRICECTGY = "defaultPriceCtgy";
	public static final String F_COMMENTS = "comments";
	public static final String F_FOREX = "forex";
	public static final String F_CONTRACTID = "contractId";
	public static final String F_CONTRACTVALIDFROM = "contractValidFrom";
	public static final String F_CONTRACTVALIDTO = "contractValidTo";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_VALUE = "value";
	public static final String F_CURRENCYID = "currencyId";
	public static final String F_CURRENCYCODE = "currencyCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_PRICECATEGORY = "priceCategory";
	public static final String F_PRICECTGRYNAME = "priceCtgryName";
	public static final String F_PCTYPE = "pcType";
	public static final String F_FINANCIALSOURCEID = "financialSourceId";
	public static final String F_FINANCIALSOURCENAME = "financialSourceName";
	public static final String F_AVGMETHODINDICATORDEFMETH = "avgMethodIndicatorDefMeth";
	public static final String F_AVGMETHODINDICATORNAME = "avgMethodIndicatorName";
	public static final String F_AVGMTHDID = "avgMthdId";
	public static final String F_PRICE = "price";
	public static final String F_CONVERTEDPRICE = "convertedPrice";
	public static final String F_CONVERSION = "conversion";
	public static final String F_USED = "used";
	public static final String F_SLASH = "slash";

	@DsField
	private String name;

	@DsField
	private Boolean continous;

	@DsField
	private Boolean includeInAverage;

	@DsField
	private QuantityType quantityType;

	@DsField
	private MasterAgreementsPeriod exchangeRateOffset;

	@DsField
	private Boolean restriction;

	@DsField
	private VatApplicability vat;

	@DsField
	private Boolean defaultPriceCtgy;

	@DsField
	private String comments;

	@DsField(fetch = false)
	private String forex;

	@DsField(join = "left", path = "contract.id")
	private Integer contractId;

	@DsField(join = "left", path = "contract.validFrom")
	private Date contractValidFrom;

	@DsField(join = "left", path = "contract.validTo")
	private Date contractValidTo;

	@DsField(fetch = false)
	private Date validFrom;

	@DsField(fetch = false)
	private Date validTo;

	@DsField(fetch = false)
	private BigDecimal value;

	@DsField(join = "left", path = "contract.settlementCurr.id")
	private Integer currencyId;

	@DsField(join = "left", path = "contract.settlementCurr.code")
	private String currencyCode;

	@DsField(join = "left", path = "contract.settlementUnit.id")
	private Integer unitId;

	@DsField(join = "left", path = "contract.settlementUnit.code")
	private String unitCode;

	@DsField(join = "left", path = "priceCategory.id")
	private Integer priceCategory;

	@DsField(join = "left", path = "priceCategory.name")
	private String priceCtgryName;

	@DsField(join = "left", path = "priceCategory.type")
	private PriceType pcType;

	@DsField(join = "left", path = "financialSource.id")
	private Integer financialSourceId;

	@DsField(join = "left", path = "financialSource.name")
	private String financialSourceName;

	@DsField(join = "left", path = "averageMethod.defaultMethod")
	private Boolean avgMethodIndicatorDefMeth;

	@DsField(join = "left", path = "averageMethod.name")
	private String avgMethodIndicatorName;

	@DsField(join = "left", path = "averageMethod.id")
	private Integer avgMthdId;

	@DsField(fetch = false)
	private BigDecimal price;

	@DsField(fetch = false)
	private BigDecimal convertedPrice;

	@DsField(fetch = false)
	private String conversion;

	@DsField(fetch = false)
	private Boolean used;

	@DsField(fetch = false)
	private String slash;

	/**
	 * Default constructor
	 */
	public PriceCategories_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PriceCategories_Ds(ContractPriceCategory e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getContinous() {
		return this.continous;
	}

	public void setContinous(Boolean continous) {
		this.continous = continous;
	}

	public Boolean getIncludeInAverage() {
		return this.includeInAverage;
	}

	public void setIncludeInAverage(Boolean includeInAverage) {
		this.includeInAverage = includeInAverage;
	}

	public QuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(QuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public MasterAgreementsPeriod getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(MasterAgreementsPeriod exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public Boolean getRestriction() {
		return this.restriction;
	}

	public void setRestriction(Boolean restriction) {
		this.restriction = restriction;
	}

	public VatApplicability getVat() {
		return this.vat;
	}

	public void setVat(VatApplicability vat) {
		this.vat = vat;
	}

	public Boolean getDefaultPriceCtgy() {
		return this.defaultPriceCtgy;
	}

	public void setDefaultPriceCtgy(Boolean defaultPriceCtgy) {
		this.defaultPriceCtgy = defaultPriceCtgy;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getForex() {
		return this.forex;
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public Integer getContractId() {
		return this.contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public Date getContractValidFrom() {
		return this.contractValidFrom;
	}

	public void setContractValidFrom(Date contractValidFrom) {
		this.contractValidFrom = contractValidFrom;
	}

	public Date getContractValidTo() {
		return this.contractValidTo;
	}

	public void setContractValidTo(Date contractValidTo) {
		this.contractValidTo = contractValidTo;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Integer getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getPriceCategory() {
		return this.priceCategory;
	}

	public void setPriceCategory(Integer priceCategory) {
		this.priceCategory = priceCategory;
	}

	public String getPriceCtgryName() {
		return this.priceCtgryName;
	}

	public void setPriceCtgryName(String priceCtgryName) {
		this.priceCtgryName = priceCtgryName;
	}

	public PriceType getPcType() {
		return this.pcType;
	}

	public void setPcType(PriceType pcType) {
		this.pcType = pcType;
	}

	public Integer getFinancialSourceId() {
		return this.financialSourceId;
	}

	public void setFinancialSourceId(Integer financialSourceId) {
		this.financialSourceId = financialSourceId;
	}

	public String getFinancialSourceName() {
		return this.financialSourceName;
	}

	public void setFinancialSourceName(String financialSourceName) {
		this.financialSourceName = financialSourceName;
	}

	public Boolean getAvgMethodIndicatorDefMeth() {
		return this.avgMethodIndicatorDefMeth;
	}

	public void setAvgMethodIndicatorDefMeth(Boolean avgMethodIndicatorDefMeth) {
		this.avgMethodIndicatorDefMeth = avgMethodIndicatorDefMeth;
	}

	public String getAvgMethodIndicatorName() {
		return this.avgMethodIndicatorName;
	}

	public void setAvgMethodIndicatorName(String avgMethodIndicatorName) {
		this.avgMethodIndicatorName = avgMethodIndicatorName;
	}

	public Integer getAvgMthdId() {
		return this.avgMthdId;
	}

	public void setAvgMthdId(Integer avgMthdId) {
		this.avgMthdId = avgMthdId;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getConvertedPrice() {
		return this.convertedPrice;
	}

	public void setConvertedPrice(BigDecimal convertedPrice) {
		this.convertedPrice = convertedPrice;
	}

	public String getConversion() {
		return this.conversion;
	}

	public void setConversion(String conversion) {
		this.conversion = conversion;
	}

	public Boolean getUsed() {
		return this.used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	public String getSlash() {
		return this.slash;
	}

	public void setSlash(String slash) {
		this.slash = slash;
	}
}
