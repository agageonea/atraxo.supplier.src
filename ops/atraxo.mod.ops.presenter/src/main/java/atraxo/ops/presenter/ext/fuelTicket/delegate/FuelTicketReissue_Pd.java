package atraxo.ops.presenter.ext.fuelTicket.delegate;

import java.util.Iterator;
import java.util.List;

import atraxo.fmbas.business.ext.util.EntityCloner;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.presenter.impl.fuelTicket.model.FuelTicket_Ds;
import atraxo.ops.presenter.impl.fuelTicket.model.FuelTicket_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

/**
 *
 * @author zspeter
 *
 */
public class FuelTicketReissue_Pd extends AbstractPresenterDelegate {

	/**
	 *
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void reissue(FuelTicket_Ds ds, FuelTicket_DsParam params) throws Exception {
		IFuelTicketService ticketService = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		FuelTicket oldFuelTicket = ticketService.findById(ds.getId());
		if (!FuelTicketStatus._CANCELED_.equals(oldFuelTicket.getTicketStatus())) {
			throw new BusinessException(OpsErrorCode.FUEL_TICKET_STATUS_MUST_BE_CANCELLED,
					OpsErrorCode.FUEL_TICKET_STATUS_MUST_BE_CANCELLED.getErrMsg());
		}

		String newTicketNumber = params.getNewFuelTicketNumber();
		List<FuelTicket> refTickets = ticketService.findByReferenceTicketId(ds.getId());
		if (refTickets != null) {
			Iterator<FuelTicket> iter = refTickets.iterator();
			while (iter.hasNext()) {
				FuelTicket tkt = iter.next();
				if (tkt.getIsDeleted()) {
					iter.remove();
				}
			}
			if (!refTickets.isEmpty()) {
				throw new BusinessException(OpsErrorCode.FUEL_TICKET_CAN_NOT_BE_REISSUED, OpsErrorCode.FUEL_TICKET_CAN_NOT_BE_REISSUED.getErrMsg());
			}
		}
		FuelTicket newFuelTicket = EntityCloner.cloneEntity(oldFuelTicket);
		newFuelTicket.setTicketNo(newTicketNumber);
		newFuelTicket.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
		newFuelTicket.setReferenceTicket(oldFuelTicket);
		newFuelTicket.setTicketStatus(FuelTicketStatus._REISSUE_);
		newFuelTicket.setInvoiceStatus(OutgoingInvoiceStatus._NOT_INVOICED_);
		ticketService.insert(newFuelTicket);
		FuelTicket savedFuelTicket = ticketService.findByRefid(newFuelTicket.getRefid());
		params.setNewFuelTicketId(savedFuelTicket.getId());
	}

}
