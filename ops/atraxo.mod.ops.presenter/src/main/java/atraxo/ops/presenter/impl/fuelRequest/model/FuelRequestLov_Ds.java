/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelRequest.model;

import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FuelRequestStatus;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelRequest.class, sort = {@SortField(field = FuelRequestLov_Ds.F_REQUESTCODE)})
public class FuelRequestLov_Ds extends AbstractDs_Ds<FuelRequest> {

	public static final String ALIAS = "ops_FuelRequestLov_Ds";

	public static final String F_REQUESTCODE = "requestCode";
	public static final String F_REQUESTDATE = "requestDate";
	public static final String F_CUSTOMERREFERENCENO = "customerReferenceNo";
	public static final String F_REQUESTEDSERVICE = "requestedService";
	public static final String F_ISOPENRELEASE = "isOpenRelease";
	public static final String F_STARTDATE = "startDate";
	public static final String F_ENDDATE = "endDate";
	public static final String F_REQUESTSTATUS = "requestStatus";
	public static final String F_PROCESSEDBY = "processedBy";

	@DsField
	private String requestCode;

	@DsField
	private Date requestDate;

	@DsField
	private String customerReferenceNo;

	@DsField
	private String requestedService;

	@DsField
	private Boolean isOpenRelease;

	@DsField
	private Date startDate;

	@DsField
	private Date endDate;

	@DsField
	private FuelRequestStatus requestStatus;

	@DsField
	private String processedBy;

	/**
	 * Default constructor
	 */
	public FuelRequestLov_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelRequestLov_Ds(FuelRequest e) {
		super(e);
	}

	public String getRequestCode() {
		return this.requestCode;
	}

	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getCustomerReferenceNo() {
		return this.customerReferenceNo;
	}

	public void setCustomerReferenceNo(String customerReferenceNo) {
		this.customerReferenceNo = customerReferenceNo;
	}

	public String getRequestedService() {
		return this.requestedService;
	}

	public void setRequestedService(String requestedService) {
		this.requestedService = requestedService;
	}

	public Boolean getIsOpenRelease() {
		return this.isOpenRelease;
	}

	public void setIsOpenRelease(Boolean isOpenRelease) {
		this.isOpenRelease = isOpenRelease;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public FuelRequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	public void setRequestStatus(FuelRequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}
}
