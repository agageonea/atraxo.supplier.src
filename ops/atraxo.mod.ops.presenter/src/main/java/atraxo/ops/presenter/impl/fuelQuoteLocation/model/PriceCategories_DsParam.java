/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuoteLocation.model;

import java.math.BigDecimal;
/**
 * Generated code. Do not modify in this file.
 */
public class PriceCategories_DsParam {

	public static final String f_settlementCurrencyCode = "settlementCurrencyCode";
	public static final String f_settlementUnitCode = "settlementUnitCode";
	public static final String f_total = "total";

	private String settlementCurrencyCode;

	private String settlementUnitCode;

	private BigDecimal total;

	public String getSettlementCurrencyCode() {
		return this.settlementCurrencyCode;
	}

	public void setSettlementCurrencyCode(String settlementCurrencyCode) {
		this.settlementCurrencyCode = settlementCurrencyCode;
	}

	public String getSettlementUnitCode() {
		return this.settlementUnitCode;
	}

	public void setSettlementUnitCode(String settlementUnitCode) {
		this.settlementUnitCode = settlementUnitCode;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}
