/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelTicket.model;

/**
 * Generated code. Do not modify in this file.
 */
public class ArchivedFuelTicket_DsParam {

	public static final String f_locationCode = "locationCode";
	public static final String f_locationId = "locationId";
	public static final String f_remark = "remark";
	public static final String f_customer = "customer";
	public static final String f_hardCopy = "hardCopy";
	public static final String f_ticketIdToHC = "ticketIdToHC";
	public static final String f_newFuelTicketNumber = "newFuelTicketNumber";
	public static final String f_newFuelTicketId = "newFuelTicketId";

	private String locationCode;

	private Integer locationId;

	private String remark;

	private String customer;

	private String hardCopy;

	private Integer ticketIdToHC;

	private String newFuelTicketNumber;

	private Integer newFuelTicketId;

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCustomer() {
		return this.customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getHardCopy() {
		return this.hardCopy;
	}

	public void setHardCopy(String hardCopy) {
		this.hardCopy = hardCopy;
	}

	public Integer getTicketIdToHC() {
		return this.ticketIdToHC;
	}

	public void setTicketIdToHC(Integer ticketIdToHC) {
		this.ticketIdToHC = ticketIdToHC;
	}

	public String getNewFuelTicketNumber() {
		return this.newFuelTicketNumber;
	}

	public void setNewFuelTicketNumber(String newFuelTicketNumber) {
		this.newFuelTicketNumber = newFuelTicketNumber;
	}

	public Integer getNewFuelTicketId() {
		return this.newFuelTicketId;
	}

	public void setNewFuelTicketId(Integer newFuelTicketId) {
		this.newFuelTicketId = newFuelTicketId;
	}
}
