package atraxo.ops.presenter.ext.fuelRequest.delegate;

import java.util.GregorianCalendar;
import java.util.List;

import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.ops.business.api.fuelRequest.IFuelRequestService;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FuelRequestStatus;
import atraxo.ops.presenter.impl.fuelRequest.model.FuelRequest_Ds;
import atraxo.ops.presenter.impl.fuelRequest.model.FuelRequest_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class FuelRequest_Pd extends AbstractPresenterDelegate {

	public void check(FuelRequest_Ds ds) throws Exception {
		if (ds != null && ds.getId() != null) {
			IFuelRequestService fuelRequestService = (IFuelRequestService) this.findEntityService(FuelRequest.class);
			FuelRequest fr = fuelRequestService.findById(ds.getId());
			fuelRequestService.check(fr);
		}
	}

	/**
	 * @param frDs
	 * @throws Exception
	 */
	public void generateFuelQuotation(FuelRequest_Ds frDs) throws Exception {
		if (this.canGenerateFuelQuote(frDs)) {
			this.checkMasterAgreement(frDs);

			IFuelRequestService fuelRequestService = (IFuelRequestService) this.findEntityService(FuelRequest.class);
			FuelRequest fr = fuelRequestService.findById(frDs.getId());
			FuelQuotation fuelQuotation = fuelRequestService.generateFuelQuotation(fr);
			if (fuelQuotation != null) {
				frDs.setFuelQuotationId(fuelQuotation.getId());
				frDs.setFuelQuotationCode(fuelQuotation.getCode());
			}
		}
	}

	private boolean canGenerateFuelQuote(FuelRequest_Ds frDs) {
		return frDs != null && frDs.getId() != null && frDs.getRequestStatus().equals(FuelRequestStatus._PROCESSED_)
				&& frDs.getFuelQuotationId() == null;
	}

	private void checkMasterAgreement(FuelRequest_Ds frDs) throws Exception {
		ICustomerService customerSrv = (ICustomerService) this.findEntityService(Customer.class);
		Customer customer = customerSrv.findByCode(frDs.getCustomerCode());
		customerSrv.getValidMasterAgreement(customer, GregorianCalendar.getInstance().getTime());
	}

	/**
	 * @param list
	 * @param param
	 * @throws Exception
	 */
	public void updateStatus(List<FuelRequest_Ds> list, FuelRequest_DsParam param) throws Exception {
		IFuelRequestService fuelRequestService = (IFuelRequestService) this.findEntityService(FuelRequest.class);
		for (FuelRequest_Ds ds : list) {
			FuelRequest fuelRequest = fuelRequestService.findById(ds.getId());
			fuelRequest.setRequestStatus(FuelRequestStatus.getByName(param.getAction()));
			fuelRequestService.updateStatus(fuelRequest, param.getAction(), param.getRemarks());
		}

	}
}
