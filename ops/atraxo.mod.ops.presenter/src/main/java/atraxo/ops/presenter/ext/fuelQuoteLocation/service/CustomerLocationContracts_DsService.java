/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelQuoteLocation.service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.exceptions.SupplierContractNotFoundException;
import atraxo.cmm.domain.ext.contracts.SupplierContract;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.fmbas_type.CreditTerm;
import atraxo.ops.presenter.impl.fuelQuoteLocation.model.CustomerLocationContracts_Ds;
import atraxo.ops.presenter.impl.fuelQuoteLocation.model.CustomerLocationContracts_DsParam;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class CustomerLocationContracts_DsService
		extends AbstractEntityDsService<CustomerLocationContracts_Ds, CustomerLocationContracts_Ds, Object, Contract>
		implements IDsService<CustomerLocationContracts_Ds, CustomerLocationContracts_Ds, Object> {

	private static final Logger logger = LoggerFactory.getLogger(CustomerLocationContracts_DsService.class);

	@Override
	protected void postFind(IQueryBuilder<CustomerLocationContracts_Ds, CustomerLocationContracts_Ds, Object> builder,
			List<CustomerLocationContracts_Ds> result) throws Exception {
		super.postFind(builder, result);
		IContractService service = (IContractService) this.findEntityService(Contract.class);
		Set<CustomerLocationContracts_Ds> filteredResult = new HashSet<CustomerLocationContracts_Ds>();
		CustomerLocationContracts_DsParam params = (CustomerLocationContracts_DsParam) builder.getParams();
		for (CustomerLocationContracts_Ds ds : result) {
			try {
				SupplierContract supplierContract = service.getSupplierContract(ds.getId(), params.getStartsOn(), params.getEndsOn(),
						params.getFqlCurrency(), params.getFqlUnit(), params.getAvgMthId(), params.getFinSrcId(), params.getPeriod());
				if (supplierContract != null) {
					ds.setUnit(supplierContract.getUnit());
					ds.setBasePrice(supplierContract.getBasePrice());
					ds.setDifferential(supplierContract.getDifferential());
					ds.setFuelPrice(supplierContract.getBasePrice().add(ds.getDifferential() != null ? ds.getDifferential() : BigDecimal.ZERO));
					ds.setIntoPlaneFee(supplierContract.getIntoPlaneFee());
					ds.setOtherFees(supplierContract.getOtherFees());
					ds.setTaxes(supplierContract.getTaxes());
					ds.setPerFlightFee(supplierContract.getPerFlightFee());
					ds.setTotalPrice(ds.getFuelPrice().add(ds.getIntoPlaneFee()).add(ds.getOtherFees()).add(ds.getTaxes()));
					ds.setDft(ds.getIntoPlaneFee().add(ds.getOtherFees()).add(ds.getTaxes()));
					ds.setExposure(supplierContract.getExposure());
					ds.setPaymentTerms(supplierContract.getPaymentTerms());
					ds.setCreditTerms(CreditTerm.getByName(supplierContract.getCreditTerms()));
					if (supplierContract.getPriceBasis() != null) {
						ds.setPricingBases(supplierContract.getPriceBasis().getName());
					}
					ds.setExchangeRateOffset(supplierContract.getExchangeRateOffset());
					if (supplierContract.getQuotation() != null) {
						ds.setQuotationName(supplierContract.getQuotation().getName());
						ds.setQuotationId(supplierContract.getQuotation().getId());
					}
					if (supplierContract.getIntoPlaneAgent() != null) {
						ds.setIplAgentCode(supplierContract.getIntoPlaneAgent().getCode());
						ds.setIplAgentId(supplierContract.getIntoPlaneAgent().getId());
					}
					if (ds.getFuelPrice().compareTo(BigDecimal.ZERO) > 0) {
						filteredResult.add(ds);
					}
				}
			} catch (SupplierContractNotFoundException e) {
				logger.info("Could not find a supplier contract, will do nothing !", e);
			}

		}
		result.removeAll(result);
		result.addAll(filteredResult);
		builder.putExtraAttribute("rec-count", new Long(filteredResult.size()));
	}

	@Override
	public Long count(IQueryBuilder<CustomerLocationContracts_Ds, CustomerLocationContracts_Ds, Object> builder) throws Exception {
		return (Long) builder.getExtraAttribute("rec-count");
	}
}
