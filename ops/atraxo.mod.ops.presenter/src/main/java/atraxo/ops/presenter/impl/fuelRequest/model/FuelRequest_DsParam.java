/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelRequest.model;

/**
 * Generated code. Do not modify in this file.
 */
public class FuelRequest_DsParam {

	public static final String f_tmpField = "tmpField";
	public static final String f_action = "action";
	public static final String f_remarks = "remarks";

	private String tmpField;

	private String action;

	private String remarks;

	public String getTmpField() {
		return this.tmpField;
	}

	public void setTmpField(String tmpField) {
		this.tmpField = tmpField;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
