/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelTicket.service;

import atraxo.ops.domain.impl.fuelTicket.FuelTicketHardCopy;
import atraxo.ops.presenter.impl.fuelTicket.model.FuelTicketHardCopy_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelTicketHardCopy_DsService
		extends
			AbstractEntityDsService<FuelTicketHardCopy_Ds, FuelTicketHardCopy_Ds, Object, FuelTicketHardCopy>
		implements
			IDsService<FuelTicketHardCopy_Ds, FuelTicketHardCopy_Ds, Object> {

	// Implement me ...

}
