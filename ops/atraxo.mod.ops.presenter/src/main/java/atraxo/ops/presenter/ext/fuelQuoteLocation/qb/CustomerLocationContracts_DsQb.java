/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */

package atraxo.ops.presenter.ext.fuelQuoteLocation.qb;

import atraxo.cmm.domain.impl.cmm_type.DealType;
import atraxo.ops.presenter.impl.fuelQuoteLocation.model.CustomerLocationContracts_Ds;
import atraxo.ops.presenter.impl.fuelQuoteLocation.model.CustomerLocationContracts_DsParam;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class CustomerLocationContracts_DsQb
		extends QueryBuilderWithJpql<CustomerLocationContracts_Ds, CustomerLocationContracts_Ds, CustomerLocationContracts_DsParam> {

	@Override
	protected void beforeBuildWhere() throws Exception {
		this.addFilterCondition("e.dealType = :dealType");

		this.addCustomFilterItem("dealType", DealType._BUY_);

	};
}
