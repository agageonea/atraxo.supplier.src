/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelRequest.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FuelRequestStatus;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelRequest.class, sort = {@SortField(field = FuelRequest_Ds.F_CUSTOMERCODE)})
@RefLookups({
		@RefLookup(refId = FuelRequest_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelRequest_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = FuelRequest_Ds.F_CONTACTID),
		@RefLookup(refId = FuelRequest_Ds.F_FUELQUOTATIONID, namedQuery = FuelQuotation.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelRequest_Ds.F_FUELQUOTATIONCODE)}),
		@RefLookup(refId = FuelRequest_Ds.F_SUBSIDIARYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelRequest_Ds.F_SUBSIDIARYCODE)})})
public class FuelRequest_Ds extends AbstractDs_Ds<FuelRequest> {

	public static final String ALIAS = "ops_FuelRequest_Ds";

	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CUSTOMERREFID = "customerRefId";
	public static final String F_CUSTOMERENTITYALIAS = "customerEntityAlias";
	public static final String F_CONTACTID = "contactId";
	public static final String F_CONTACTNAME = "contactName";
	public static final String F_REQUESTCODE = "requestCode";
	public static final String F_REQUESTDATE = "requestDate";
	public static final String F_CUSTOMERREFERENCENO = "customerReferenceNo";
	public static final String F_REQUESTEDSERVICE = "requestedService";
	public static final String F_ISOPENRELEASE = "isOpenRelease";
	public static final String F_STARTDATE = "startDate";
	public static final String F_ENDDATE = "endDate";
	public static final String F_REQUESTSTATUS = "requestStatus";
	public static final String F_PROCESSEDBY = "processedBy";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";
	public static final String F_SUBSIDIARYNAME = "subsidiaryName";
	public static final String F_FUELQUOTATIONID = "fuelQuotationId";
	public static final String F_FUELQUOTATIONCODE = "fuelQuotationCode";
	public static final String F_QUOTNO = "quotNo";
	public static final String F_LABELFIELD = "labelField";

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "customer.refid")
	private String customerRefId;

	@DsField(join = "left", fetch = false, path = "customer.entityAlias")
	private String customerEntityAlias;

	@DsField(join = "left", path = "contact.id")
	private Integer contactId;

	@DsField(join = "left", fetch = false, path = "contact.fullNameField")
	private String contactName;

	@DsField
	private String requestCode;

	@DsField
	private Date requestDate;

	@DsField
	private String customerReferenceNo;

	@DsField
	private String requestedService;

	@DsField
	private Boolean isOpenRelease;

	@DsField
	private Date startDate;

	@DsField
	private Date endDate;

	@DsField
	private FuelRequestStatus requestStatus;

	@DsField
	private String processedBy;

	@DsField(join = "left", path = "subsidiary.id")
	private Integer subsidiaryId;

	@DsField(join = "left", path = "subsidiary.code")
	private String subsidiaryCode;

	@DsField(join = "left", path = "subsidiary.name")
	private String subsidiaryName;

	@DsField(join = "left", path = "fuelQuotation.id")
	private Integer fuelQuotationId;

	@DsField(join = "left", path = "fuelQuotation.code")
	private String fuelQuotationCode;

	@DsField(fetch = false)
	private String quotNo;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public FuelRequest_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelRequest_Ds(FuelRequest e) {
		super(e);
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerRefId() {
		return this.customerRefId;
	}

	public void setCustomerRefId(String customerRefId) {
		this.customerRefId = customerRefId;
	}

	public String getCustomerEntityAlias() {
		return this.customerEntityAlias;
	}

	public void setCustomerEntityAlias(String customerEntityAlias) {
		this.customerEntityAlias = customerEntityAlias;
	}

	public Integer getContactId() {
		return this.contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getRequestCode() {
		return this.requestCode;
	}

	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getCustomerReferenceNo() {
		return this.customerReferenceNo;
	}

	public void setCustomerReferenceNo(String customerReferenceNo) {
		this.customerReferenceNo = customerReferenceNo;
	}

	public String getRequestedService() {
		return this.requestedService;
	}

	public void setRequestedService(String requestedService) {
		this.requestedService = requestedService;
	}

	public Boolean getIsOpenRelease() {
		return this.isOpenRelease;
	}

	public void setIsOpenRelease(Boolean isOpenRelease) {
		this.isOpenRelease = isOpenRelease;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public FuelRequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	public void setRequestStatus(FuelRequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public String getSubsidiaryName() {
		return this.subsidiaryName;
	}

	public void setSubsidiaryName(String subsidiaryName) {
		this.subsidiaryName = subsidiaryName;
	}

	public Integer getFuelQuotationId() {
		return this.fuelQuotationId;
	}

	public void setFuelQuotationId(Integer fuelQuotationId) {
		this.fuelQuotationId = fuelQuotationId;
	}

	public String getFuelQuotationCode() {
		return this.fuelQuotationCode;
	}

	public void setFuelQuotationCode(String fuelQuotationCode) {
		this.fuelQuotationCode = fuelQuotationCode;
	}

	public String getQuotNo() {
		return this.quotNo;
	}

	public void setQuotNo(String quotNo) {
		this.quotNo = quotNo;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
