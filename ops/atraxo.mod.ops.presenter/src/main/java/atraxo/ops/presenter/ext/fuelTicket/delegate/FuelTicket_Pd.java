package atraxo.ops.presenter.ext.fuelTicket.delegate;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.cmm.business.ext.exceptions.CmmErrorCode;
import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.business.api.documentNumberSeries.IDocumentNumberSeriesService;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.workflow.IWorkflowInstanceEntityService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.exceptions.workflow.ClaimedWorkflowException;
import atraxo.fmbas.business.ext.util.StringUtil;
import atraxo.fmbas.domain.impl.documentNumberSeries.DocumentNumberSeries;
import atraxo.fmbas.domain.impl.fmbas_type.DocumentNumberSeriesType;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import atraxo.ops.business.api.ext.fuelTicket.IFuelTicketWorkflowStarter;
import atraxo.ops.business.api.fuelTicket.IFuelTicketHardCopyService;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.domain.ext.fuelTicketValidator.FuelTicketWorkflowStarterValidatorResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.fuelTicket.FuelTicketHardCopy;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketTransmissionStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.presenter.impl.fuelTicket.model.FuelTicket_Ds;
import atraxo.ops.presenter.impl.fuelTicket.model.FuelTicket_DsParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class FuelTicket_Pd extends AbstractPresenterDelegate {

	private static final String GENERATION_MESSAGE = "The system automatically created a credit memo number as there is no valid document number series defined for credit memos. Please configure a document number series for credit memos in the Accounting settings.";

	private static final Logger LOGGER = LoggerFactory.getLogger(FuelTicket_Pd.class);

	private static final String HTML_TAG_UL_START = "<ul>";
	private static final String HTML_TAG_UL_END = "</ul>";
	private static final String HTML_TAG_LI_START = "<li>";
	private static final String HTML_TAG_LI_END = "</li>";

	/**
	 * IF ticket status is equal with ORIGINAL, UPDATED or REISSUED and the approval status is FOR APPROVAL, REJECTED, RECHECK we can set status to
	 * OK.
	 *
	 * @param fuelTicketDsList
	 * @throws Exception
	 */
	public void releaseList(List<FuelTicket_Ds> fuelTicketDsList, FuelTicket_DsParam params) throws Exception {
		// first, check the 'Use fuel ticket approval workflow' system parameter (SYSFUELTICKETAPPROVAL)
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		if (paramSrv.getFuelTicketApprovalWorkflow()) {
			params.setApproveErrorDescription(OpsErrorCode.FUEL_TICKET_RELEASE_NO_SYS_PARAM.getErrMsg());
			return;
		}

		IFuelTicketService fuelTicketService = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		IFuelTicketWorkflowStarter fuelTicketWkfStarter = this.getApplicationContext().getBean(IFuelTicketWorkflowStarter.class);
		List<FuelTicket> fuelTickets = new ArrayList<>();
		StringBuilder sb = new StringBuilder();

		sb.append(HTML_TAG_UL_START);
		for (FuelTicket_Ds fuelTicketDs : fuelTicketDsList) {
			try {

				FuelTicket fuelTicket = fuelTicketService.findById(fuelTicketDs.getId());

				// check concurrency
				if (fuelTicket.getApprovalStatus().equals(FuelTicketApprovalStatus._OK_) || fuelTicket.getIsDeleted()) {
					sb.append(HTML_TAG_LI_START).append(fuelTicket.getTicketNo()).append(OpsErrorCode.NOT_SYNCRONIZED_WITH_DATABASE.getErrMsg())
							.append(HTML_TAG_LI_END);
				} else {
					// proceed with validation check
					FuelTicketWorkflowStarterValidatorResult validationStartResult = fuelTicketWkfStarter.validateWorkflowStart(fuelTicket,
							WorkflowNames.FUEL_TICKET_RELEASE);
					if (validationStartResult.isValid()) {
						fuelTickets.add(fuelTicket);
					} else {
						sb.append(HTML_TAG_LI_START).append(fuelTicket.getTicketNo()).append(validationStartResult.getValidationMessage())
								.append(HTML_TAG_LI_END);
						fuelTicketService.insertToHistory(fuelTicket, WorkflowMsgConstants.RELEASE_FAILED,
								validationStartResult.getValidationMessage());
					}

				}

			} catch (IllegalArgumentException e) {
				LOGGER.error("FUEL TICKET NOT FOUND!", e);
			}
		}
		sb.append(HTML_TAG_UL_END);

		WorkflowStartResult result = new WorkflowStartResult();
		if (!CollectionUtils.isEmpty(fuelTickets)) {
			result = fuelTicketWkfStarter.startFuelTicketReleaseWorkflow(fuelTickets, true);
		}
		if (result != null) {
			params.setApproveResult(result.getNumberOfSuccesInstances() == fuelTicketDsList.size());
			params.setApproveErrorDescription(String.format(BusinessErrorCode.WORKFLOW_FAILED_NUMBER.getErrMsg(),
					fuelTicketDsList.size() - result.getNumberOfSuccesInstances(), fuelTicketDsList.size()));
		}

		String msg = sb.toString();
		if (msg.contains(HTML_TAG_LI_START)) {
			String approveErrorDescription = params.getApproveErrorDescription();
			params.setApproveErrorDescription(new StringBuilder(approveErrorDescription).append("\n")
					.append(String.format(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR.getErrMsg(), msg)).toString());
		}
	}

	/**
	 * IF ticket status is equal with ORIGINAL, UPDATED or REISSUED and the approval status is FOR APPROVAL, REJECTED, RECHECK we can set status to
	 * OK.
	 *
	 * @param fuelTicketDsList
	 * @throws Exception
	 */
	public void release(FuelTicket_Ds fuelTicketDs, FuelTicket_DsParam params) throws Exception {
		List<FuelTicket_Ds> tickets = new ArrayList<>();
		tickets.add(fuelTicketDs);
		this.releaseList(tickets, params);
	}

	/**
	 * Approves current Fuel Ticket workflow task
	 *
	 * @param fuelTicketDs
	 * @param params
	 * @throws Exception
	 */
	public void approve(FuelTicket_Ds fuelTicketDs, FuelTicket_DsParam params) throws Exception {
		ISystemParameterService paramService = (ISystemParameterService) this.findEntityService(SystemParameter.class);

		StringBuilder sb = new StringBuilder();
		sb.append(HTML_TAG_UL_START);

		if (!paramService.getFuelTicketApprovalWorkflow()) {
			sb.append(HTML_TAG_LI_START).append(fuelTicketDs.getTicketNo()).append(HTML_TAG_LI_END);
		}

		this.completeCurrentTask(fuelTicketDs.getId(), true, params.getApprovalNote());

		String msg = sb.toString();
		if (msg.contains(HTML_TAG_LI_START)) {
			throw new BusinessException(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR,
					String.format(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR.getErrMsg(), msg));
		}
	}

	/**
	 * Rejects current Fuel Ticket workflow task
	 *
	 * @param fuelTicketDs
	 * @param params
	 * @throws Exception
	 */
	public void reject(FuelTicket_Ds fuelTicketDs, FuelTicket_DsParam params) throws Exception {
		ISystemParameterService paramService = (ISystemParameterService) this.findEntityService(SystemParameter.class);

		StringBuilder sb = new StringBuilder();
		sb.append(HTML_TAG_UL_START);

		if (!paramService.getFuelTicketApprovalWorkflow()) {
			sb.append(HTML_TAG_LI_START).append(fuelTicketDs.getTicketNo()).append(HTML_TAG_LI_END);
		}

		this.completeCurrentTask(fuelTicketDs.getId(), false, params.getApprovalNote());

		String msg = sb.toString();
		if (msg.contains(HTML_TAG_LI_START)) {
			throw new BusinessException(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR,
					String.format(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR.getErrMsg(), msg));
		}
	}

	/**
	 * @param ticketId
	 * @param accept
	 * @param note
	 * @throws Exception
	 */
	private void completeCurrentTask(Integer ticketId, boolean accept, String note) throws Exception {
		IFuelTicketService fuelTickeSrv = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		IWorkflowInstanceEntityService workflowInstanceEntitySrv = (IWorkflowInstanceEntityService) this
				.findEntityService(WorkflowInstanceEntity.class);

		FuelTicket ticket = fuelTickeSrv.findById(ticketId);
		if (!ticket.getApprovalStatus().equals(FuelTicketApprovalStatus._AWAITING_APPROVAL_)) {
			throw new BusinessException(OpsErrorCode.FUEL_TICKET_WORKFLOW_APPROVED, OpsErrorCode.FUEL_TICKET_WORKFLOW_APPROVED.getErrMsg());
		}

		IWorkflowBpmManager workflowBpmManager = (IWorkflowBpmManager) this.getApplicationContext().getBean("workflowBpmManager");
		WorkflowInstanceEntity workflowInstanceEntity = workflowInstanceEntitySrv
				.findByBusinessKey(WorkflowNames.FUEL_TICKET_APPROVAL.getWorkflowName(), ticket.getId(), ticket.getClass().getSimpleName());

		try {
			workflowBpmManager.claimCompleteTask(workflowInstanceEntity.getWorkflowInstance().getId(), accept, note);
		} catch (ClaimedWorkflowException e) {
			throw e;
		} catch (Exception e) {
			workflowBpmManager.terminateWorkflow(workflowInstanceEntity.getWorkflowInstance().getId(),
					OpsErrorCode.FUEL_TICKET_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage());
			throw new BusinessException(OpsErrorCode.FUEL_TICKET_WORKFLOW_IDENTIFICATION_ERROR,
					OpsErrorCode.FUEL_TICKET_WORKFLOW_IDENTIFICATION_ERROR.getErrMsg() + ":" + e.getMessage(), e);
		}
	}

	/**
	 * IF ticket status is equal with ORIGINAL or UPDATED and the transmission status is transmitted can we set status to cancelled and transmission
	 * status to new.
	 *
	 * @param fuelTicketDs
	 * @param param
	 * @throws Exception
	 */
	public void cancel(FuelTicket_Ds fuelTicketDs, FuelTicket_DsParam param) throws Exception {
		List<FuelTicket_Ds> tickets = new ArrayList<>();
		tickets.add(fuelTicketDs);
		this.cancelList(tickets, param);
	}

	/**
	 * IF ticket status is equal with ORIGINAL or UPDATED and the transmission status is transmitted can we set status to cancelled and transmission
	 * status to new.
	 *
	 * @param fuelTicketDsList
	 * @param param
	 * @throws Exception
	 */
	public void cancelList(List<FuelTicket_Ds> fuelTicketDsList, FuelTicket_DsParam param) throws Exception {
		if (StringUtils.isEmpty(param.getRemark())) {
			throw new BusinessException(OpsErrorCode.FUEL_TICKET_CANCEL_REASON_IS_MISSING,
					OpsErrorCode.FUEL_TICKET_CANCEL_REASON_IS_MISSING.getErrMsg());
		}

		List<String> dbSyncValidation = new ArrayList<>();
		List<String> dbWorkflowValidation = new ArrayList<>();
		StringBuilder validationMessages = new StringBuilder();

		List<FuelTicket> fuelTickets = new ArrayList<>();
		IFuelTicketService fuelTicketService = this.getApplicationContext().getBean(IFuelTicketService.class);

		for (FuelTicket_Ds ds : fuelTicketDsList) {
			try {
				FuelTicket fuelTicket = fuelTicketService.findByBusiness(ds.getId());
				if (this.canCancel(fuelTicket)) {
					fuelTickets.add(fuelTicket);
					if (param.getGeneratorMessage().isEmpty()) {
						this.checkIfDocumentNumberSeriesExistsAndInvoiced(param, fuelTicket.getInvoiceStatus());
					}

					IFuelTicketWorkflowStarter fuelTicketWkfStarter = this.getApplicationContext().getBean(IFuelTicketWorkflowStarter.class);
					WorkflowStartResult cancelWorkflowResult = fuelTicketWkfStarter.startFuelTicketCancelWorkflow(fuelTicket, param.getRemark());

					if (!cancelWorkflowResult.isStarted()) {
						dbWorkflowValidation.add("Fuel Ticket # " + fuelTicket.getTicketNo() + ": " + cancelWorkflowResult.getReason());
					}
				} else {
					dbSyncValidation.add(fuelTicket.getTicketNo());
				}
			} catch (Exception e) {
				LOGGER.warn("Could not start Fuel Ticket Cancel for {}", ds.getTicketNo(), e);
				dbWorkflowValidation.add("Fuel Ticket # " + ds.getTicketNo() + ": " + e.getMessage());
			}
		}

		if (!dbSyncValidation.isEmpty()) {
			validationMessages.append(
					String.format(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR.getErrMsg(), StringUtil.unorderedList(dbSyncValidation.toArray())));
		}

		if (!dbWorkflowValidation.isEmpty()) {
			validationMessages.append(String.format(OpsErrorCode.FUEL_TICKET_CANCEL_WORKFLOW_NOT_START.getErrMsg(),
					StringUtil.unorderedList(dbWorkflowValidation.toArray())));
		}

		if (validationMessages.length() > 0) {
			throw new BusinessException(CmmErrorCode.VALIDATION_ERRORS, validationMessages.toString());
		}

	}

	private boolean canCancel(FuelTicket fuelTicket) {
		return (!FuelTicketApprovalStatus._FOR_APPROVAL_.equals(fuelTicket.getApprovalStatus())
				&& !FuelTicketApprovalStatus._AWAITING_APPROVAL_.equals(fuelTicket.getApprovalStatus()))
				&& (!FuelTicketStatus._CANCELED_.equals(fuelTicket.getTicketStatus()));
	}

	private void checkIfDocumentNumberSeriesExistsAndInvoiced(FuelTicket_DsParam param, OutgoingInvoiceStatus status) throws Exception {
		IDocumentNumberSeriesService srv = (IDocumentNumberSeriesService) this.findEntityService(DocumentNumberSeries.class);
		Map<String, Object> params = new HashMap<>();
		params.put("enabled", true);
		params.put("type", DocumentNumberSeriesType._CREDIT_MEMO_);
		try {
			srv.findEntityByAttributes(DocumentNumberSeries.class, params);
		} catch (Exception e) {
			LOGGER.warn("WARNING:could not find Entity by attributes, proceed further on.", e);
			if (status == OutgoingInvoiceStatus._PAID_ || status == OutgoingInvoiceStatus._AWAITING_PAYMENT_) {
				param.setGeneratorMessage(GENERATION_MESSAGE);
			}
		}
	}

	/**
	 * I can submit approved fuel tickets having as ticket status: Original, Updated, Canceled and transmission status New or Failed.
	 *
	 * @param fuelTicketDs
	 * @throws Exception
	 */
	public void submit(FuelTicket_Ds fuelTicketDs) throws Exception {
		List<FuelTicket_Ds> tickets = new ArrayList<>();
		tickets.add(fuelTicketDs);
		this.submitList(tickets);
	}

	/**
	 * I can submit approved fuel tickets having as ticket status: Original, Updated, Canceled and transmission status New or Failed.
	 *
	 * @param fuelTicketDsList
	 * @throws Exception
	 */
	public void submitList(List<FuelTicket_Ds> fuelTicketDsList) throws Exception {
		IFuelTicketService fuelTicketService = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		List<FuelTicket> fuelTickets = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		sb.append(HTML_TAG_UL_START);
		for (FuelTicket_Ds fuelTicketDs : fuelTicketDsList) {
			FuelTicket fuelTicket = fuelTicketService.findById(fuelTicketDs.getId());
			if (this.canSubmit(fuelTicket)) {
				fuelTickets.add(fuelTicket);
			} else {
				sb.append(HTML_TAG_LI_START).append(fuelTicket.getTicketNo()).append(HTML_TAG_LI_END);
			}
		}
		sb.append(HTML_TAG_UL_END);
		if (!CollectionUtils.isEmpty(fuelTickets)) {
			fuelTicketService.submitFuelTicket(fuelTickets);
		}
		String msg = sb.toString();
		if (msg.contains(HTML_TAG_LI_START)) {
			throw new BusinessException(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR,
					String.format(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR.getErrMsg(), msg));
		}

	}

	private boolean canSubmit(FuelTicket fuelTicket) {
		boolean ticketStatusFlag = FuelTicketStatus._ORIGINAL_.equals(fuelTicket.getTicketStatus())
				|| FuelTicketStatus._CANCELED_.equals(fuelTicket.getTicketStatus())
				|| FuelTicketStatus._UPDATED_.equals(fuelTicket.getTicketStatus());
		boolean transmissionStatusFlag = FuelTicketTransmissionStatus._NEW_.equals(fuelTicket.getTransmissionStatus())
				|| FuelTicketTransmissionStatus._FAILED_.equals(fuelTicket.getTransmissionStatus());

		return FuelTicketApprovalStatus._OK_.equals(fuelTicket.getApprovalStatus()) && ticketStatusFlag && transmissionStatusFlag;
	}

	/**
	 * I can reset fuel tickets having the fuel ticket status Cancelled. After I reset the fuel ticket the fuel ticket status is changed to Original
	 * and the transmission status to New. I do have to provide a reason for my action and this is capture in the fuel ticket history.
	 *
	 * @param fuelTicketDs
	 * @param param
	 * @throws Exception
	 */
	public void reset(FuelTicket_Ds fuelTicketDs, FuelTicket_DsParam param) throws Exception {
		List<FuelTicket_Ds> tickets = new ArrayList<>();
		tickets.add(fuelTicketDs);
		this.resetList(tickets, param);
	}

	/**
	 * I can reset fuel tickets having the fuel ticket status Cancelled. After I reset the fuel ticket the fuel ticket status is changed to Original
	 * and the transmission status to New. I do have to provide a reason for my action and this is capture in the fuel ticket history.
	 *
	 * @param fuelTicketDs
	 * @param param
	 * @throws Exception
	 */
	public void resetList(List<FuelTicket_Ds> fuelTicketDsList, FuelTicket_DsParam param) throws Exception {
		IFuelTicketService fuelTicketService = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		List<FuelTicket> fuelTickets = new ArrayList<>();
		for (FuelTicket_Ds fuelTicketDs : fuelTicketDsList) {
			FuelTicket fuelTicket = fuelTicketService.findById(fuelTicketDs.getId());
			fuelTickets.add(fuelTicket);
		}
		fuelTicketService.resetFuelTicket(fuelTickets, param.getRemark());
	}

	/**
	 * @param fuelTicketDs
	 * @param param
	 * @throws Exception
	 */
	public void getDefaultValues(FuelTicket_Ds fuelTicketDs, FuelTicket_DsParam param) throws Exception {
		IFuelTicketService fuelTicketService = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		String name = Session.user.get().getLoginName();
		Locations location = fuelTicketService.getUserLocation(name);
		if (location != null) {
			fuelTicketDs.setDepCode(location.getIataCode());
			fuelTicketDs.setDepId(location.getId());
		}
	}

	/**
	 * @param fuelTicketDs
	 * @param param
	 * @throws Exception
	 */
	public void getHardCopy(FuelTicket_Ds fuelTicketDs, FuelTicket_DsParam param) throws Exception {
		IFuelTicketHardCopyService fthcService = (IFuelTicketHardCopyService) this.findEntityService(FuelTicketHardCopy.class);
		FuelTicketHardCopy fthc = fthcService.findByBk(Long.valueOf(param.getTicketIdToHC().longValue()));
		String path = Session.user.get().getWorkspace().getTempPath();
		String fileName = UUID.randomUUID().toString();
		File file = new File(path + File.separator + fileName + ".png");
		FileUtils.writeByteArrayToFile(file.getAbsoluteFile(), fthc.getFile());
		param.setHardCopy(fileName + ".png");
	}

	/**
	 * @param fuelTicketDsList
	 * @throws Exception
	 */
	public void delete(List<FuelTicket_Ds> fuelTicketDsList) throws Exception {
		List<FuelTicket> fuelTickets = new ArrayList<>();
		IFuelTicketService service = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		StringBuilder sb = new StringBuilder();
		sb.append(HTML_TAG_UL_START);
		for (FuelTicket_Ds ds : fuelTicketDsList) {
			List<FuelTicket> refTickets = service.findByReferenceTicketId(ds.getId());
			FuelTicket ft = service.findById(ds.getId());
			if (ft.getIsDeleted()) {
				sb.append(HTML_TAG_LI_START).append(ft.getTicketNo()).append(" - ").append(OpsErrorCode.FUEL_TICKET_ALREADY_DELETED.getErrMsg())
						.append(HTML_TAG_LI_END);
				continue;
			}
			if (!CollectionUtils.isEmpty(refTickets)) {
				sb.append(HTML_TAG_LI_START).append(ft.getTicketNo()).append(" - ").append(OpsErrorCode.FUEL_TICKET_CAN_NOT_BE_DELETED.getErrMsg())
						.append(HTML_TAG_LI_END);
			} else {
				if (!BillStatus._NOT_BILLED_.equals(ft.getBillStatus()) || !OutgoingInvoiceStatus._NOT_INVOICED_.equals(ft.getInvoiceStatus())) {
					sb.append(HTML_TAG_LI_START).append(ft.getTicketNo()).append(" - ").append(OpsErrorCode.FUEL_TICKET_ALREADY_INVOICED.getErrMsg())
							.append(HTML_TAG_LI_END);
				} else if (FuelTicketApprovalStatus._OK_.equals(ft.getApprovalStatus())
						&& !FuelTicketStatus._CANCELED_.equals(ft.getTicketStatus())) {
					sb.append(HTML_TAG_LI_START).append(ft.getTicketNo()).append(" - ").append(OpsErrorCode.FUEL_TICKET_ORIGINAL_APPROVED.getErrMsg())
							.append(HTML_TAG_LI_END);
				} else {
					fuelTickets.add(ft);
				}
			}
		}
		sb.append(HTML_TAG_UL_END);
		if (!CollectionUtils.isEmpty(fuelTickets)) {
			service.setFlagDelete(fuelTickets, true);
		}
		String msg = sb.toString();
		if (msg.contains(HTML_TAG_LI_START)) {
			throw new BusinessException(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR,
					String.format(OpsErrorCode.FUEL_TICKET_RPC_ACTION_ERROR.getErrMsg(), msg));
		}

	}

	/**
	 * Submits for approval a <code>FuelTicket</code>
	 *
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void submitForApproval(FuelTicket_Ds ds, FuelTicket_DsParam params) throws Exception {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START submitForApproval()");
		}

		// assume false, ( the operation didn't complete ok )
		params.setSubmitForApprovalResult(false);

		// first, check the 'Use fuel ticket approval workflow' system parameter (SYSFUELTICKETAPPROVAL)
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		boolean useWorkflow = paramSrv.getFuelTicketApprovalWorkflow();
		if (!useWorkflow) {
			params.setSubmitForApprovalDescription(OpsErrorCode.FUEL_TICKET_WORKFLOW_NO_SYS_PARAM.getErrMsg());
			return;
		}

		// if ok, proceed forward and retrieve the fuel ticket object from DB
		IFuelTicketService fuelTicketService = this.getApplicationContext().getBean(IFuelTicketService.class);
		FuelTicket fuelTicket = null;
		try {
			fuelTicket = fuelTicketService.findByBusiness(ds.getId());
		} catch (Exception e) {
			LOGGER.warn("WARNING:could not locate Fuel Ticket for ID " + ds.getId(), e);
			params.setSubmitForApprovalDescription(
					String.format(OpsErrorCode.FUEL_TICKET_WORKFLOW_ENTITY_NOT_EXISTING.getErrMsg(), ds.getId().toString()));
		}

		// if object exists, proceed
		if (fuelTicket != null) {

			// check for concurrency (maybe object changed in the meantime)
			if (this.canSubmitForApproval(fuelTicket)) {

				IFuelTicketWorkflowStarter fuelTicketWkfStarter = this.getApplicationContext().getBean(IFuelTicketWorkflowStarter.class);
				FuelTicketWorkflowStarterValidatorResult validationStartResult = fuelTicketWkfStarter.validateWorkflowStart(fuelTicket,
						WorkflowNames.FUEL_TICKET_APPROVAL);

				// check net quantity
				if (validationStartResult.isValid()) {
					// if OK, then procede and start submit for approval
					WorkflowStartResult result = fuelTicketWkfStarter.startFuelTicketApprovalWorkflow(fuelTicket, params.getApprovalNote(),
							params.getSelectedAttachments(), true);
					if (result.isStarted()) {
						params.setSubmitForApprovalResult(true);
					} else {
						params.setSubmitForApprovalDescription(OpsErrorCode.FUEL_TICKET_WORKFLOW_NOT_START_REASON.getErrMsg() + result.getReason());
					}
				} else {
					// if not OK (ney quantity check was not valid) set the response to the UI and insert result in history
					params.setSubmitForApprovalDescription(validationStartResult.getValidationMessage());
					fuelTicketService.insertToHistory(fuelTicket, WorkflowMsgConstants.SUBMIT_FOR_APPROVAL_FAILED,
							validationStartResult.getValidationMessage());
				}
			} else {
				params.setSubmitForApprovalDescription(OpsErrorCode.FUEL_TICKET_WORKFLOW_CONCURRENCY_NOT_START_ERROR.getErrMsg());
			}

		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END submitForApproval()");
		}

	}

	/**
	 * Re-check the conditions that allows a user to submit for approval a fuel ticket
	 *
	 * @param fuelTicket
	 * @return
	 */
	private boolean canSubmitForApproval(FuelTicket fuelTicket) {
		boolean canSubmit = false;
		if (fuelTicket.getApprovalStatus().equals(FuelTicketApprovalStatus._REJECTED_)
				|| fuelTicket.getApprovalStatus().equals(FuelTicketApprovalStatus._RECHECK_)
				|| fuelTicket.getApprovalStatus().equals(FuelTicketApprovalStatus._FOR_APPROVAL_)) {
			canSubmit = true;
		}
		return canSubmit;
	}

}
