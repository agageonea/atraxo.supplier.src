/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.flightEvent.model;

/**
 * Generated code. Do not modify in this file.
 */
public class FlightEvent_DsParam {

	public static final String f_src = "src";
	public static final String f_remarks = "remarks";
	public static final String f_action = "action";
	public static final String f_paramStatus = "paramStatus";
	public static final String f_warningMessage = "warningMessage";
	public static final String f_isEnabled = "isEnabled";

	private String src;

	private String remarks;

	private String action;

	private String paramStatus;

	private String warningMessage;

	private Boolean isEnabled;

	public String getSrc() {
		return this.src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getParamStatus() {
		return this.paramStatus;
	}

	public void setParamStatus(String paramStatus) {
		this.paramStatus = paramStatus;
	}

	public String getWarningMessage() {
		return this.warningMessage;
	}

	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}

	public Boolean getIsEnabled() {
		return this.isEnabled;
	}

	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
}
