/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelTransaction.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.presenter.impl.fuelTransaction.model.FuelTransaction_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelTransaction_DsService extends AbstractEntityDsService<FuelTransaction_Ds, FuelTransaction_Ds, Object, FuelTransaction>
		implements IDsService<FuelTransaction_Ds, FuelTransaction_Ds, Object> {

	@Override
	@Transactional
	public void deleteByIds(List<Object> ids) throws Exception {
		List<FuelTransaction> list = this.getEntityService().findByIds(ids);
		this.getEntityService().delete(list);
	}

	@Override
	@Transactional
	public void deleteById(Object id) throws Exception {
		this.deleteByIds(Arrays.asList(id));
	}
}
