/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuoteLocation.model;

import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import java.util.Date;
/**
 * Generated code. Do not modify in this file.
 */
public class CustomerLocationContracts_DsParam {

	public static final String f_startsOn = "startsOn";
	public static final String f_endsOn = "endsOn";
	public static final String f_fqlUnit = "fqlUnit";
	public static final String f_fqlCurrency = "fqlCurrency";
	public static final String f_finSrcId = "finSrcId";
	public static final String f_avgMthId = "avgMthId";
	public static final String f_period = "period";

	private Date startsOn;

	private Date endsOn;

	private Integer fqlUnit;

	private Integer fqlCurrency;

	private Integer finSrcId;

	private Integer avgMthId;

	private MasterAgreementsPeriod period;

	public Date getStartsOn() {
		return this.startsOn;
	}

	public void setStartsOn(Date startsOn) {
		this.startsOn = startsOn;
	}

	public Date getEndsOn() {
		return this.endsOn;
	}

	public void setEndsOn(Date endsOn) {
		this.endsOn = endsOn;
	}

	public Integer getFqlUnit() {
		return this.fqlUnit;
	}

	public void setFqlUnit(Integer fqlUnit) {
		this.fqlUnit = fqlUnit;
	}

	public Integer getFqlCurrency() {
		return this.fqlCurrency;
	}

	public void setFqlCurrency(Integer fqlCurrency) {
		this.fqlCurrency = fqlCurrency;
	}

	public Integer getFinSrcId() {
		return this.finSrcId;
	}

	public void setFinSrcId(Integer finSrcId) {
		this.finSrcId = finSrcId;
	}

	public Integer getAvgMthId() {
		return this.avgMthId;
	}

	public void setAvgMthId(Integer avgMthId) {
		this.avgMthId = avgMthId;
	}

	public MasterAgreementsPeriod getPeriod() {
		return this.period;
	}

	public void setPeriod(MasterAgreementsPeriod period) {
		this.period = period;
	}
}
