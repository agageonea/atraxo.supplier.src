/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.pricePolicy.model;

import atraxo.cmm.domain.impl.cmm_type.YesNo;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = PricePolicy.class, sort = {@SortField(field = PricePolicy_Ds.F_NAME)})
@RefLookups({
		@RefLookup(refId = PricePolicy_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PricePolicy_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = PricePolicy_Ds.F_LOCATIONID, namedQuery = Locations.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PricePolicy_Ds.F_LOCATIONCODE)}),
		@RefLookup(refId = PricePolicy_Ds.F_ACTYPEID, namedQuery = AcTypes.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PricePolicy_Ds.F_ACTYPECODE)}),
		@RefLookup(refId = PricePolicy_Ds.F_UNITID, namedQuery = Unit.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PricePolicy_Ds.F_UNITCODE)}),
		@RefLookup(refId = PricePolicy_Ds.F_CURRID, namedQuery = Currencies.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = PricePolicy_Ds.F_CURRCODE)})})
public class PricePolicy_Ds extends AbstractDs_Ds<PricePolicy> {

	public static final String ALIAS = "ops_PricePolicy_Ds";

	public static final String F_MARGIN = "margin";
	public static final String F_MARKUP = "markup";
	public static final String F_PERCENT = "percent";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_LOCATIONID = "locationId";
	public static final String F_LOCATIONCODE = "locationCode";
	public static final String F_ACTYPEID = "acTypeId";
	public static final String F_ACTYPECODE = "acTypeCode";
	public static final String F_UNITID = "unitId";
	public static final String F_UNITCODE = "unitCode";
	public static final String F_CURRID = "currId";
	public static final String F_CURRCODE = "currCode";
	public static final String F_NAME = "name";
	public static final String F_DEFAULTMARGIN = "defaultMargin";
	public static final String F_MINIMALMARGIN = "minimalMargin";
	public static final String F_DEFAULTMARKUP = "defaultMarkup";
	public static final String F_MINIMALMARKUP = "minimalMarkup";
	public static final String F_VALIDFROM = "validFrom";
	public static final String F_VALIDTO = "validTo";
	public static final String F_ACTIVE = "active";
	public static final String F_COMMENTS = "comments";
	public static final String F_YESNO = "yesNo";

	@DsField(fetch = false)
	private String margin;

	@DsField(fetch = false)
	private String markup;

	@DsField(fetch = false)
	private String percent;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "location.id")
	private Integer locationId;

	@DsField(join = "left", path = "location.code")
	private String locationCode;

	@DsField(join = "left", path = "aircraft.id")
	private Integer acTypeId;

	@DsField(join = "left", path = "aircraft.code")
	private String acTypeCode;

	@DsField(join = "left", path = "unit.id")
	private Integer unitId;

	@DsField(join = "left", path = "unit.code")
	private String unitCode;

	@DsField(join = "left", path = "currency.id")
	private Integer currId;

	@DsField(join = "left", path = "currency.code")
	private String currCode;

	@DsField
	private String name;

	@DsField
	private BigDecimal defaultMargin;

	@DsField
	private BigDecimal minimalMargin;

	@DsField
	private BigDecimal defaultMarkup;

	@DsField
	private BigDecimal minimalMarkup;

	@DsField(jpqlFilter = " e.validTo >= :validFrom ")
	private Date validFrom;

	@DsField(jpqlFilter = " e.validFrom <= :validTo ")
	private Date validTo;

	@DsField
	private Boolean active;

	@DsField
	private String comments;

	@DsField(fetch = false)
	private YesNo yesNo;

	/**
	 * Default constructor
	 */
	public PricePolicy_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public PricePolicy_Ds(PricePolicy e) {
		super(e);
	}

	public String getMargin() {
		return this.margin;
	}

	public void setMargin(String margin) {
		this.margin = margin;
	}

	public String getMarkup() {
		return this.markup;
	}

	public void setMarkup(String markup) {
		this.markup = markup;
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public Integer getAcTypeId() {
		return this.acTypeId;
	}

	public void setAcTypeId(Integer acTypeId) {
		this.acTypeId = acTypeId;
	}

	public String getAcTypeCode() {
		return this.acTypeCode;
	}

	public void setAcTypeCode(String acTypeCode) {
		this.acTypeCode = acTypeCode;
	}

	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getUnitCode() {
		return this.unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public Integer getCurrId() {
		return this.currId;
	}

	public void setCurrId(Integer currId) {
		this.currId = currId;
	}

	public String getCurrCode() {
		return this.currCode;
	}

	public void setCurrCode(String currCode) {
		this.currCode = currCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getDefaultMargin() {
		return this.defaultMargin;
	}

	public void setDefaultMargin(BigDecimal defaultMargin) {
		this.defaultMargin = defaultMargin;
	}

	public BigDecimal getMinimalMargin() {
		return this.minimalMargin;
	}

	public void setMinimalMargin(BigDecimal minimalMargin) {
		this.minimalMargin = minimalMargin;
	}

	public BigDecimal getDefaultMarkup() {
		return this.defaultMarkup;
	}

	public void setDefaultMarkup(BigDecimal defaultMarkup) {
		this.defaultMarkup = defaultMarkup;
	}

	public BigDecimal getMinimalMarkup() {
		return this.minimalMarkup;
	}

	public void setMinimalMarkup(BigDecimal minimalMarkup) {
		this.minimalMarkup = minimalMarkup;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public YesNo getYesNo() {
		return this.yesNo;
	}

	public void setYesNo(YesNo yesNo) {
		this.yesNo = yesNo;
	}
}
