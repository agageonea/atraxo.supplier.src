/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.pricePolicy.qb;

import atraxo.ops.presenter.impl.pricePolicy.model.PricePolicy_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.api.session.Session;

/**
 * Generated code. Do not modify in this file.
 */
public class PricePolicy_DsQb
		extends
			QueryBuilderWithJpql<PricePolicy_Ds, PricePolicy_Ds, Object> {
}
