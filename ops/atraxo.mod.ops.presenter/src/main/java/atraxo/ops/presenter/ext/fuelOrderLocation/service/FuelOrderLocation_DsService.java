/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelOrderLocation.service;

import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.presenter.impl.fuelOrderLocation.model.FuelOrderLocation_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelOrderLocation_DsService extends AbstractEntityDsService<FuelOrderLocation_Ds, FuelOrderLocation_Ds, Object, FuelOrderLocation>
		implements IDsService<FuelOrderLocation_Ds, FuelOrderLocation_Ds, Object> {

	// Implement me ...

}
