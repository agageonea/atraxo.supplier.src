/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelTransaction.model;

import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionEquipment;
import atraxo.ops.domain.impl.ops_type.DensityType;
import atraxo.ops.domain.impl.ops_type.DensityUOM;
import atraxo.ops.domain.impl.ops_type.TemperatureUnit;
import java.math.BigDecimal;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelTransactionEquipment.class)
@RefLookups({@RefLookup(refId = FuelTransactionEquipment_Ds.F_FUELTRANSID)})
public class FuelTransactionEquipment_Ds
		extends
			AbstractDs_Ds<FuelTransactionEquipment> {

	public static final String ALIAS = "ops_FuelTransactionEquipment_Ds";

	public static final String F_FUELTRANSID = "fuelTransId";
	public static final String F_FUELINGEQUIPMENTID = "fuelingEquipmentId";
	public static final String F_FUELINGTYPE = "fuelingType";
	public static final String F_AVGFUELTEMPERATURE = "avgFuelTemperature";
	public static final String F_TEMPERATUREUOM = "temperatureUOM";
	public static final String F_DENSITYTYPE = "densityType";
	public static final String F_DENSITY = "density";
	public static final String F_DENSITYUOM = "densityUOM";
	public static final String F_METERREADINGSTART = "meterReadingStart";
	public static final String F_METERREADINGEND = "meterReadingEnd";
	public static final String F_METERQUANTITYDELIVERED = "meterQuantityDelivered";
	public static final String F_METERQUANTITYUOM = "meterQuantityUOM";
	public static final String F_LABELFIELD = "labelField";

	@DsField(join = "left", path = "fuelTransaction.id")
	private Integer fuelTransId;

	@DsField
	private String fuelingEquipmentId;

	@DsField
	private FuelingType fuelingType;

	@DsField
	private BigDecimal avgFuelTemperature;

	@DsField
	private TemperatureUnit temperatureUOM;

	@DsField
	private DensityType densityType;

	@DsField
	private BigDecimal density;

	@DsField
	private DensityUOM densityUOM;

	@DsField
	private BigDecimal meterReadingStart;

	@DsField
	private BigDecimal meterReadingEnd;

	@DsField
	private BigDecimal meterQuantityDelivered;

	@DsField
	private String meterQuantityUOM;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public FuelTransactionEquipment_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelTransactionEquipment_Ds(FuelTransactionEquipment e) {
		super(e);
	}

	public Integer getFuelTransId() {
		return this.fuelTransId;
	}

	public void setFuelTransId(Integer fuelTransId) {
		this.fuelTransId = fuelTransId;
	}

	public String getFuelingEquipmentId() {
		return this.fuelingEquipmentId;
	}

	public void setFuelingEquipmentId(String fuelingEquipmentId) {
		this.fuelingEquipmentId = fuelingEquipmentId;
	}

	public FuelingType getFuelingType() {
		return this.fuelingType;
	}

	public void setFuelingType(FuelingType fuelingType) {
		this.fuelingType = fuelingType;
	}

	public BigDecimal getAvgFuelTemperature() {
		return this.avgFuelTemperature;
	}

	public void setAvgFuelTemperature(BigDecimal avgFuelTemperature) {
		this.avgFuelTemperature = avgFuelTemperature;
	}

	public TemperatureUnit getTemperatureUOM() {
		return this.temperatureUOM;
	}

	public void setTemperatureUOM(TemperatureUnit temperatureUOM) {
		this.temperatureUOM = temperatureUOM;
	}

	public DensityType getDensityType() {
		return this.densityType;
	}

	public void setDensityType(DensityType densityType) {
		this.densityType = densityType;
	}

	public BigDecimal getDensity() {
		return this.density;
	}

	public void setDensity(BigDecimal density) {
		this.density = density;
	}

	public DensityUOM getDensityUOM() {
		return this.densityUOM;
	}

	public void setDensityUOM(DensityUOM densityUOM) {
		this.densityUOM = densityUOM;
	}

	public BigDecimal getMeterReadingStart() {
		return this.meterReadingStart;
	}

	public void setMeterReadingStart(BigDecimal meterReadingStart) {
		this.meterReadingStart = meterReadingStart;
	}

	public BigDecimal getMeterReadingEnd() {
		return this.meterReadingEnd;
	}

	public void setMeterReadingEnd(BigDecimal meterReadingEnd) {
		this.meterReadingEnd = meterReadingEnd;
	}

	public BigDecimal getMeterQuantityDelivered() {
		return this.meterQuantityDelivered;
	}

	public void setMeterQuantityDelivered(BigDecimal meterQuantityDelivered) {
		this.meterQuantityDelivered = meterQuantityDelivered;
	}

	public String getMeterQuantityUOM() {
		return this.meterQuantityUOM;
	}

	public void setMeterQuantityUOM(String meterQuantityUOM) {
		this.meterQuantityUOM = meterQuantityUOM;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
