package atraxo.ops.presenter.ext.fuelTicket.delegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.presenter.ext.fuelTicket.service.FuelTicketMobile_DsService;
import atraxo.ops.presenter.impl.fuelTicket.model.FuelTicketMobile_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.converter.DefaultDsConverter;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class FuelTicketMobile_Pd extends AbstractPresenterDelegate {

	public void synchronize(List<FuelTicketMobile_Ds> list) throws Exception {
		IDsService<FuelTicketMobile_Ds, FuelTicketMobile_Ds, Object> dsService = this.getServiceLocator().findDsService(FuelTicketMobile_Ds.class);
		IFuelTicketService service = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		DefaultDsConverter<FuelTicketMobile_Ds, FuelTicket> converter = DefaultDsConverter.class.newInstance();
		converter.setModelClass(dsService.getModelClass());
		converter.setEntityClass(FuelTicket.class);
		converter.setApplicationContext(this.getApplicationContext());
		FuelTicketMobile_DsService ftDsService = (FuelTicketMobile_DsService) dsService;
		converter.setDescriptor(ftDsService.getDescriptor());
		converter.setServiceLocator(this.getServiceLocator());
		List<FuelTicket> fuelTickets = new ArrayList<FuelTicket>();
		HashMap<FuelTicket, String> hardCopyMap = new HashMap<FuelTicket, String>();
		for (FuelTicketMobile_Ds ds : list) {
			FuelTicket fuelTicket = service.create();
			fuelTickets.add(fuelTicket);
			ds._setEntity_(fuelTicket);
			converter.modelToEntity(ds, fuelTicket, false, service.getEntityManager());
			hardCopyMap.put(fuelTicket, ds.getTicketHardCopy());
		}
		List<FuelTicket> ticketList = service.synchronize(fuelTickets);
		for (Map.Entry<FuelTicket, String> entry : hardCopyMap.entrySet()) {
			service.saveFuelTicketHardCopy(entry.getKey(), entry.getValue());
		}
		converter.entitiesToModels(ticketList, service.getEntityManager(), null);
	}
}
