/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelTicket.model;

/**
 * Generated code. Do not modify in this file.
 */
public class FuelTicket_DsParam {

	public static final String f_locationCode = "locationCode";
	public static final String f_locationId = "locationId";
	public static final String f_remark = "remark";
	public static final String f_customerr = "customerr";
	public static final String f_hardCopy = "hardCopy";
	public static final String f_ticketIdToHC = "ticketIdToHC";
	public static final String f_newFuelTicketNumber = "newFuelTicketNumber";
	public static final String f_newFuelTicketId = "newFuelTicketId";
	public static final String f_generatorMessage = "generatorMessage";
	public static final String f_approveErrorDescription = "approveErrorDescription";
	public static final String f_approveResult = "approveResult";
	public static final String f_approvalNote = "approvalNote";
	public static final String f_submitForApprovalDescription = "submitForApprovalDescription";
	public static final String f_submitForApprovalResult = "submitForApprovalResult";
	public static final String f_selectedAttachments = "selectedAttachments";
	public static final String f_cancelResult = "cancelResult";

	private String locationCode;

	private Integer locationId;

	private String remark;

	private String customerr;

	private String hardCopy;

	private Integer ticketIdToHC;

	private String newFuelTicketNumber;

	private Integer newFuelTicketId;

	private String generatorMessage;

	private String approveErrorDescription;

	private Boolean approveResult;

	private String approvalNote;

	private String submitForApprovalDescription;

	private Boolean submitForApprovalResult;

	private String selectedAttachments;

	private String cancelResult;

	public String getLocationCode() {
		return this.locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCustomerr() {
		return this.customerr;
	}

	public void setCustomerr(String customerr) {
		this.customerr = customerr;
	}

	public String getHardCopy() {
		return this.hardCopy;
	}

	public void setHardCopy(String hardCopy) {
		this.hardCopy = hardCopy;
	}

	public Integer getTicketIdToHC() {
		return this.ticketIdToHC;
	}

	public void setTicketIdToHC(Integer ticketIdToHC) {
		this.ticketIdToHC = ticketIdToHC;
	}

	public String getNewFuelTicketNumber() {
		return this.newFuelTicketNumber;
	}

	public void setNewFuelTicketNumber(String newFuelTicketNumber) {
		this.newFuelTicketNumber = newFuelTicketNumber;
	}

	public Integer getNewFuelTicketId() {
		return this.newFuelTicketId;
	}

	public void setNewFuelTicketId(Integer newFuelTicketId) {
		this.newFuelTicketId = newFuelTicketId;
	}

	public String getGeneratorMessage() {
		return this.generatorMessage;
	}

	public void setGeneratorMessage(String generatorMessage) {
		this.generatorMessage = generatorMessage;
	}

	public String getApproveErrorDescription() {
		return this.approveErrorDescription;
	}

	public void setApproveErrorDescription(String approveErrorDescription) {
		this.approveErrorDescription = approveErrorDescription;
	}

	public Boolean getApproveResult() {
		return this.approveResult;
	}

	public void setApproveResult(Boolean approveResult) {
		this.approveResult = approveResult;
	}

	public String getApprovalNote() {
		return this.approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}

	public String getSubmitForApprovalDescription() {
		return this.submitForApprovalDescription;
	}

	public void setSubmitForApprovalDescription(
			String submitForApprovalDescription) {
		this.submitForApprovalDescription = submitForApprovalDescription;
	}

	public Boolean getSubmitForApprovalResult() {
		return this.submitForApprovalResult;
	}

	public void setSubmitForApprovalResult(Boolean submitForApprovalResult) {
		this.submitForApprovalResult = submitForApprovalResult;
	}

	public String getSelectedAttachments() {
		return this.selectedAttachments;
	}

	public void setSelectedAttachments(String selectedAttachments) {
		this.selectedAttachments = selectedAttachments;
	}

	public String getCancelResult() {
		return this.cancelResult;
	}

	public void setCancelResult(String cancelResult) {
		this.cancelResult = cancelResult;
	}
}
