/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelTransaction.model;

import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelTransactionSource.class)
public class FuelTransactionSource_Ds
		extends
			AbstractDs_Ds<FuelTransactionSource> {

	public static final String ALIAS = "ops_FuelTransactionSource_Ds";

	public static final String F_NAME = "name";
	public static final String F_PROCESSINGDATE = "processingDate";
	public static final String F_FUELTRANSACTIONS = "fuelTransactions";

	@DsField
	private String name;

	@DsField
	private Date processingDate;

	@DsField
	private Integer fuelTransactions;

	/**
	 * Default constructor
	 */
	public FuelTransactionSource_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelTransactionSource_Ds(FuelTransactionSource e) {
		super(e);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getProcessingDate() {
		return this.processingDate;
	}

	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}

	public Integer getFuelTransactions() {
		return this.fuelTransactions;
	}

	public void setFuelTransactions(Integer fuelTransactions) {
		this.fuelTransactions = fuelTransactions;
	}
}
