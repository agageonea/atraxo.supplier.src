/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelTransaction.model;

import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.ProductIATA;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.ops_type.DeliveryType;
import atraxo.ops.domain.impl.ops_type.FuelTicketCustomsStatus;
import atraxo.ops.domain.impl.ops_type.IPTransactionCode;
import atraxo.ops.domain.impl.ops_type.InterfaceType;
import atraxo.ops.domain.impl.ops_type.InternationalFlight;
import atraxo.ops.domain.impl.ops_type.TicketSource;
import atraxo.ops.domain.impl.ops_type.TicketType;
import atraxo.ops.domain.impl.ops_type.ValidationState;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelTransaction.class)
@RefLookups({@RefLookup(refId = FuelTransaction_Ds.F_IMPORTFILEID)})
public class FuelTransaction_Ds extends AbstractDs_Ds<FuelTransaction> {

	public static final String ALIAS = "ops_FuelTransaction_Ds";

	public static final String F_IMPORTFILEID = "importFileId";
	public static final String F_IMPORTFILENAME = "importFileName";
	public static final String F_PROCESSINGDATE = "processingDate";
	public static final String F_INTERFACETYPE = "interfaceType";
	public static final String F_DELIVERYTYPE = "deliveryType";
	public static final String F_INTOPLANECODE = "intoPlaneCode";
	public static final String F_INTOPLANENAME = "intoPlaneName";
	public static final String F_AIRPORTCODE = "airportCode";
	public static final String F_TICKETNUMBER = "ticketNumber";
	public static final String F_TICKETTYPE = "ticketType";
	public static final String F_TICKETSOURCE = "ticketSource";
	public static final String F_PREVIOUSTICKET = "previousTicket";
	public static final String F_TRANSACTIONDATE = "transactionDate";
	public static final String F_AIRLINEFLIGHTID = "airlineFlightId";
	public static final String F_AIRCRAFTIDENTIFICATION = "aircraftIdentification";
	public static final String F_INTERNATIONALFLIGHT = "internationalFlight";
	public static final String F_AIRCRAFTTYPE = "aircraftType";
	public static final String F_NEXTDESTINATION = "nextDestination";
	public static final String F_FINALDESTINATION = "finalDestination";
	public static final String F_FLIGHTSERVICETYPE = "flightServiceType";
	public static final String F_PAYMENTTYPE = "paymentType";
	public static final String F_CARDNAME = "cardName";
	public static final String F_CARDNUMBER = "cardNumber";
	public static final String F_CARDEXPIRY = "cardExpiry";
	public static final String F_AMOUNTRECEIVED = "amountReceived";
	public static final String F_AMOUNTRECEIVEDCURR = "amountReceivedCurr";
	public static final String F_IPTRANSACTIONCODE = "ipTransactionCode";
	public static final String F_RECEIVERCODE = "receiverCode";
	public static final String F_RECEIVERNAME = "receiverName";
	public static final String F_RECEIVERACCOUNTNUMBER = "receiverAccountNumber";
	public static final String F_SUPPLIERCODE = "supplierCode";
	public static final String F_SUPPLIERNAME = "supplierName";
	public static final String F_PRODUCTID = "productId";
	public static final String F_CUSTOMSTATUS = "customStatus";
	public static final String F_TRANSACTIONSTARTDATE = "transactionStartDate";
	public static final String F_TRANSACTIONENDDATE = "transactionEndDate";
	public static final String F_VALIDATIONSTATE = "validationState";
	public static final String F_VALIDATIONRESULT = "validationResult";
	public static final String F_TICKETISSUERCODE = "ticketIssuerCode";
	public static final String F_TICKETISSUERNAME = "ticketIssuerName";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_LABELFIELD = "labelField";

	@DsField(join = "left", path = "importSource.id")
	private Integer importFileId;

	@DsField(join = "left", path = "importSource.name")
	private String importFileName;

	@DsField(join = "left", path = "importSource.processingDate")
	private Date processingDate;

	@DsField
	private InterfaceType interfaceType;

	@DsField
	private DeliveryType deliveryType;

	@DsField
	private String intoPlaneCode;

	@DsField
	private String intoPlaneName;

	@DsField
	private String airportCode;

	@DsField
	private String ticketNumber;

	@DsField
	private TicketType ticketType;

	@DsField
	private TicketSource ticketSource;

	@DsField
	private String previousTicket;

	@DsField
	private Date transactionDate;

	@DsField
	private String airlineFlightId;

	@DsField
	private String aircraftIdentification;

	@DsField
	private InternationalFlight internationalFlight;

	@DsField
	private String aircraftType;

	@DsField
	private String nextDestination;

	@DsField
	private String finalDestination;

	@DsField
	private FlightServiceType flightServiceType;

	@DsField
	private PaymentType paymentType;

	@DsField
	private String cardName;

	@DsField
	private String cardNumber;

	@DsField
	private Date cardExpiry;

	@DsField
	private BigDecimal amountReceived;

	@DsField
	private String amountReceivedCurr;

	@DsField
	private IPTransactionCode ipTransactionCode;

	@DsField
	private String receiverCode;

	@DsField
	private String receiverName;

	@DsField
	private String receiverAccountNumber;

	@DsField
	private String supplierCode;

	@DsField
	private String supplierName;

	@DsField
	private ProductIATA productId;

	@DsField
	private FuelTicketCustomsStatus customStatus;

	@DsField
	private Date transactionStartDate;

	@DsField
	private Date transactionEndDate;

	@DsField
	private ValidationState validationState;

	@DsField
	private String validationResult;

	@DsField
	private String ticketIssuerCode;

	@DsField
	private String ticketIssuerName;

	@DsField(fetch = false)
	private String formTitle;

	@DsField(fetch = false)
	private String labelField;

	/**
	 * Default constructor
	 */
	public FuelTransaction_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelTransaction_Ds(FuelTransaction e) {
		super(e);
	}

	public Integer getImportFileId() {
		return this.importFileId;
	}

	public void setImportFileId(Integer importFileId) {
		this.importFileId = importFileId;
	}

	public String getImportFileName() {
		return this.importFileName;
	}

	public void setImportFileName(String importFileName) {
		this.importFileName = importFileName;
	}

	public Date getProcessingDate() {
		return this.processingDate;
	}

	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}

	public InterfaceType getInterfaceType() {
		return this.interfaceType;
	}

	public void setInterfaceType(InterfaceType interfaceType) {
		this.interfaceType = interfaceType;
	}

	public DeliveryType getDeliveryType() {
		return this.deliveryType;
	}

	public void setDeliveryType(DeliveryType deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getIntoPlaneCode() {
		return this.intoPlaneCode;
	}

	public void setIntoPlaneCode(String intoPlaneCode) {
		this.intoPlaneCode = intoPlaneCode;
	}

	public String getIntoPlaneName() {
		return this.intoPlaneName;
	}

	public void setIntoPlaneName(String intoPlaneName) {
		this.intoPlaneName = intoPlaneName;
	}

	public String getAirportCode() {
		return this.airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getTicketNumber() {
		return this.ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public TicketType getTicketType() {
		return this.ticketType;
	}

	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}

	public TicketSource getTicketSource() {
		return this.ticketSource;
	}

	public void setTicketSource(TicketSource ticketSource) {
		this.ticketSource = ticketSource;
	}

	public String getPreviousTicket() {
		return this.previousTicket;
	}

	public void setPreviousTicket(String previousTicket) {
		this.previousTicket = previousTicket;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getAirlineFlightId() {
		return this.airlineFlightId;
	}

	public void setAirlineFlightId(String airlineFlightId) {
		this.airlineFlightId = airlineFlightId;
	}

	public String getAircraftIdentification() {
		return this.aircraftIdentification;
	}

	public void setAircraftIdentification(String aircraftIdentification) {
		this.aircraftIdentification = aircraftIdentification;
	}

	public InternationalFlight getInternationalFlight() {
		return this.internationalFlight;
	}

	public void setInternationalFlight(InternationalFlight internationalFlight) {
		this.internationalFlight = internationalFlight;
	}

	public String getAircraftType() {
		return this.aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getNextDestination() {
		return this.nextDestination;
	}

	public void setNextDestination(String nextDestination) {
		this.nextDestination = nextDestination;
	}

	public String getFinalDestination() {
		return this.finalDestination;
	}

	public void setFinalDestination(String finalDestination) {
		this.finalDestination = finalDestination;
	}

	public FlightServiceType getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(FlightServiceType flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public PaymentType getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getCardName() {
		return this.cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getCardExpiry() {
		return this.cardExpiry;
	}

	public void setCardExpiry(Date cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	public BigDecimal getAmountReceived() {
		return this.amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public String getAmountReceivedCurr() {
		return this.amountReceivedCurr;
	}

	public void setAmountReceivedCurr(String amountReceivedCurr) {
		this.amountReceivedCurr = amountReceivedCurr;
	}

	public IPTransactionCode getIpTransactionCode() {
		return this.ipTransactionCode;
	}

	public void setIpTransactionCode(IPTransactionCode ipTransactionCode) {
		this.ipTransactionCode = ipTransactionCode;
	}

	public String getReceiverCode() {
		return this.receiverCode;
	}

	public void setReceiverCode(String receiverCode) {
		this.receiverCode = receiverCode;
	}

	public String getReceiverName() {
		return this.receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverAccountNumber() {
		return this.receiverAccountNumber;
	}

	public void setReceiverAccountNumber(String receiverAccountNumber) {
		this.receiverAccountNumber = receiverAccountNumber;
	}

	public String getSupplierCode() {
		return this.supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public ProductIATA getProductId() {
		return this.productId;
	}

	public void setProductId(ProductIATA productId) {
		this.productId = productId;
	}

	public FuelTicketCustomsStatus getCustomStatus() {
		return this.customStatus;
	}

	public void setCustomStatus(FuelTicketCustomsStatus customStatus) {
		this.customStatus = customStatus;
	}

	public Date getTransactionStartDate() {
		return this.transactionStartDate;
	}

	public void setTransactionStartDate(Date transactionStartDate) {
		this.transactionStartDate = transactionStartDate;
	}

	public Date getTransactionEndDate() {
		return this.transactionEndDate;
	}

	public void setTransactionEndDate(Date transactionEndDate) {
		this.transactionEndDate = transactionEndDate;
	}

	public ValidationState getValidationState() {
		return this.validationState;
	}

	public void setValidationState(ValidationState validationState) {
		this.validationState = validationState;
	}

	public String getValidationResult() {
		return this.validationResult;
	}

	public void setValidationResult(String validationResult) {
		this.validationResult = validationResult;
	}

	public String getTicketIssuerCode() {
		return this.ticketIssuerCode;
	}

	public void setTicketIssuerCode(String ticketIssuerCode) {
		this.ticketIssuerCode = ticketIssuerCode;
	}

	public String getTicketIssuerName() {
		return this.ticketIssuerName;
	}

	public void setTicketIssuerName(String ticketIssuerName) {
		this.ticketIssuerName = ticketIssuerName;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getLabelField() {
		return this.labelField;
	}

	public void setLabelField(String labelField) {
		this.labelField = labelField;
	}
}
