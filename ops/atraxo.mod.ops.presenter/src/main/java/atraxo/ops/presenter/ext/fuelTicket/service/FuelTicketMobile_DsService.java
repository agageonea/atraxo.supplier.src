/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelTicket.service;

import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.presenter.impl.fuelTicket.model.FuelTicketMobile_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelTicketMobile_DsService extends AbstractEntityDsService<FuelTicketMobile_Ds, FuelTicketMobile_Ds, Object, FuelTicket> implements
		IDsService<FuelTicketMobile_Ds, FuelTicketMobile_Ds, Object> {

	// Implement me ...

}
