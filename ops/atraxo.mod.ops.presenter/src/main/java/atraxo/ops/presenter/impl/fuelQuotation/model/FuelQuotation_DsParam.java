/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelQuotation.model;

import java.util.Date;
/**
 * Generated code. Do not modify in this file.
 */
public class FuelQuotation_DsParam {

	public static final String f_action = "action";
	public static final String f_remarks = "remarks";
	public static final String f_orderPostedOn = "orderPostedOn";
	public static final String f_orderSubmittedBy = "orderSubmittedBy";
	public static final String f_orderSubmittedName = "orderSubmittedName";
	public static final String f_orderCustomerRef = "orderCustomerRef";

	private String action;

	private String remarks;

	private Date orderPostedOn;

	private Integer orderSubmittedBy;

	private String orderSubmittedName;

	private String orderCustomerRef;

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getOrderPostedOn() {
		return this.orderPostedOn;
	}

	public void setOrderPostedOn(Date orderPostedOn) {
		this.orderPostedOn = orderPostedOn;
	}

	public Integer getOrderSubmittedBy() {
		return this.orderSubmittedBy;
	}

	public void setOrderSubmittedBy(Integer orderSubmittedBy) {
		this.orderSubmittedBy = orderSubmittedBy;
	}

	public String getOrderSubmittedName() {
		return this.orderSubmittedName;
	}

	public void setOrderSubmittedName(String orderSubmittedName) {
		this.orderSubmittedName = orderSubmittedName;
	}

	public String getOrderCustomerRef() {
		return this.orderCustomerRef;
	}

	public void setOrderCustomerRef(String orderCustomerRef) {
		this.orderCustomerRef = orderCustomerRef;
	}
}
