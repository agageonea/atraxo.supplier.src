/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelOrder.model;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.ops_type.FuelOrderStatus;
import atraxo.ops.domain.impl.ops_type.FuelQuoteScope;
import atraxo.ops.domain.impl.ops_type.FuelQuoteType;
import java.math.BigDecimal;
import java.util.Date;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.Param;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;
import seava.j4e.api.annotation.SortField;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelOrder.class, sort = {@SortField(field = FuelOrder_Ds.F_POSTEDON, desc = true)})
@RefLookups({
		@RefLookup(refId = FuelOrder_Ds.F_CUSTOMERID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelOrder_Ds.F_CUSTOMERCODE)}),
		@RefLookup(refId = FuelOrder_Ds.F_FUELQUOTEID, namedQuery = FuelQuotation.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelOrder_Ds.F_FUELQUOTECODE)}),
		@RefLookup(refId = FuelOrder_Ds.F_CONTACTID),
		@RefLookup(refId = FuelOrder_Ds.F_USERID, namedQuery = UserSupp.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelOrder_Ds.F_USERCODE)}),
		@RefLookup(refId = FuelOrder_Ds.F_SUBSIDIARYID, namedQuery = Customer.NQ_FIND_BY_CODE, params = {@Param(name = "code", field = FuelOrder_Ds.F_SUBSIDIARYCODE)})})
public class FuelOrder_Ds extends AbstractDs_Ds<FuelOrder> {

	public static final String ALIAS = "ops_FuelOrder_Ds";

	public static final String F_SEPARATOR = "separator";
	public static final String F_CREDITTERMS = "creditTerms";
	public static final String F_CREDITLIMIT = "creditLimit";
	public static final String F_CREDITCURRENCY = "creditCurrency";
	public static final String F_DUMMYFIELD = "dummyField";
	public static final String F_CREDITCARD = "creditCard";
	public static final String F_EXPOSURE = "exposure";
	public static final String F_PERCENT = "percent";
	public static final String F_CUSTOMERID = "customerId";
	public static final String F_CUSTOMERCODE = "customerCode";
	public static final String F_CUSTOMERSTATUS = "customerStatus";
	public static final String F_FUELQUOTEID = "fuelQuoteId";
	public static final String F_FUELQUOTECODE = "fuelQuoteCode";
	public static final String F_FUELQUOTEISSUEDATE = "fuelQuoteIssueDate";
	public static final String F_FUELQUOTEVALIDUNTIL = "fuelQuoteValidUntil";
	public static final String F_CONTACTID = "contactId";
	public static final String F_CONTACTNAME = "contactName";
	public static final String F_CONTACTOBJECTID = "contactObjectId";
	public static final String F_CODE = "code";
	public static final String F_CUSTOMERREFERENCENO = "customerReferenceNo";
	public static final String F_POSTEDON = "postedOn";
	public static final String F_USERID = "userId";
	public static final String F_USERCODE = "userCode";
	public static final String F_USERNAME = "userName";
	public static final String F_STATUS = "status";
	public static final String F_OPENRELEASE = "openRelease";
	public static final String F_CONTRACTTYPE = "contractType";
	public static final String F_SCOPE = "scope";
	public static final String F_SUBSIDIARYID = "subsidiaryId";
	public static final String F_SUBSIDIARYCODE = "subsidiaryCode";
	public static final String F_SUBSIDIARYNAME = "subsidiaryName";
	public static final String F_FORMTITLE = "formTitle";
	public static final String F_ALERTLIMIT = "alertLimit";

	@DsField(fetch = false)
	private String separator;

	@DsField(fetch = false)
	private String creditTerms;

	@DsField(fetch = false)
	private BigDecimal creditLimit;

	@DsField(fetch = false)
	private String creditCurrency;

	@DsField(fetch = false)
	private String dummyField;

	@DsField(fetch = false)
	private Boolean creditCard;

	@DsField(fetch = false)
	private BigDecimal exposure;

	@DsField(fetch = false)
	private String percent;

	@DsField(join = "left", path = "customer.id")
	private Integer customerId;

	@DsField(join = "left", path = "customer.code")
	private String customerCode;

	@DsField(join = "left", path = "customer.status")
	private CustomerStatus customerStatus;

	@DsField(join = "left", path = "fuelQuote.id")
	private Integer fuelQuoteId;

	@DsField(join = "left", path = "fuelQuote.code")
	private String fuelQuoteCode;

	@DsField(join = "left", path = "fuelQuote.createdAt")
	private Date fuelQuoteIssueDate;

	@DsField(join = "left", path = "fuelQuote.validUntil")
	private Date fuelQuoteValidUntil;

	@DsField(join = "left", path = "contact.id")
	private Integer contactId;

	@DsField(join = "left", fetch = false, path = "contact.fullNameField")
	private String contactName;

	@DsField(join = "left", path = "contact.objectId")
	private Integer contactObjectId;

	@DsField
	private String code;

	@DsField
	private String customerReferenceNo;

	@DsField
	private Date postedOn;

	@DsField(join = "left", path = "registeredBy.id")
	private String userId;

	@DsField(join = "left", path = "registeredBy.code")
	private String userCode;

	@DsField(join = "left", path = "registeredBy.name")
	private String userName;

	@DsField
	private FuelOrderStatus status;

	@DsField
	private Boolean openRelease;

	@DsField
	private FuelQuoteType contractType;

	@DsField
	private FuelQuoteScope scope;

	@DsField(join = "left", path = "subsidiary.id")
	private Integer subsidiaryId;

	@DsField(join = "left", path = "subsidiary.code")
	private String subsidiaryCode;

	@DsField(join = "left", path = "subsidiary.name")
	private String subsidiaryName;

	@DsField(fetch = false)
	private String formTitle;

	@DsField(fetch = false)
	private Integer alertLimit;

	/**
	 * Default constructor
	 */
	public FuelOrder_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelOrder_Ds(FuelOrder e) {
		super(e);
	}

	public String getSeparator() {
		return this.separator;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public String getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}

	public BigDecimal getCreditLimit() {
		return this.creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCreditCurrency() {
		return this.creditCurrency;
	}

	public void setCreditCurrency(String creditCurrency) {
		this.creditCurrency = creditCurrency;
	}

	public String getDummyField() {
		return this.dummyField;
	}

	public void setDummyField(String dummyField) {
		this.dummyField = dummyField;
	}

	public Boolean getCreditCard() {
		return this.creditCard;
	}

	public void setCreditCard(Boolean creditCard) {
		this.creditCard = creditCard;
	}

	public BigDecimal getExposure() {
		return this.exposure;
	}

	public void setExposure(BigDecimal exposure) {
		this.exposure = exposure;
	}

	public String getPercent() {
		return this.percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public CustomerStatus getCustomerStatus() {
		return this.customerStatus;
	}

	public void setCustomerStatus(CustomerStatus customerStatus) {
		this.customerStatus = customerStatus;
	}

	public Integer getFuelQuoteId() {
		return this.fuelQuoteId;
	}

	public void setFuelQuoteId(Integer fuelQuoteId) {
		this.fuelQuoteId = fuelQuoteId;
	}

	public String getFuelQuoteCode() {
		return this.fuelQuoteCode;
	}

	public void setFuelQuoteCode(String fuelQuoteCode) {
		this.fuelQuoteCode = fuelQuoteCode;
	}

	public Date getFuelQuoteIssueDate() {
		return this.fuelQuoteIssueDate;
	}

	public void setFuelQuoteIssueDate(Date fuelQuoteIssueDate) {
		this.fuelQuoteIssueDate = fuelQuoteIssueDate;
	}

	public Date getFuelQuoteValidUntil() {
		return this.fuelQuoteValidUntil;
	}

	public void setFuelQuoteValidUntil(Date fuelQuoteValidUntil) {
		this.fuelQuoteValidUntil = fuelQuoteValidUntil;
	}

	public Integer getContactId() {
		return this.contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public Integer getContactObjectId() {
		return this.contactObjectId;
	}

	public void setContactObjectId(Integer contactObjectId) {
		this.contactObjectId = contactObjectId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCustomerReferenceNo() {
		return this.customerReferenceNo;
	}

	public void setCustomerReferenceNo(String customerReferenceNo) {
		this.customerReferenceNo = customerReferenceNo;
	}

	public Date getPostedOn() {
		return this.postedOn;
	}

	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public FuelOrderStatus getStatus() {
		return this.status;
	}

	public void setStatus(FuelOrderStatus status) {
		this.status = status;
	}

	public Boolean getOpenRelease() {
		return this.openRelease;
	}

	public void setOpenRelease(Boolean openRelease) {
		this.openRelease = openRelease;
	}

	public FuelQuoteType getContractType() {
		return this.contractType;
	}

	public void setContractType(FuelQuoteType contractType) {
		this.contractType = contractType;
	}

	public FuelQuoteScope getScope() {
		return this.scope;
	}

	public void setScope(FuelQuoteScope scope) {
		this.scope = scope;
	}

	public Integer getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public String getSubsidiaryName() {
		return this.subsidiaryName;
	}

	public void setSubsidiaryName(String subsidiaryName) {
		this.subsidiaryName = subsidiaryName;
	}

	public String getFormTitle() {
		return this.formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public Integer getAlertLimit() {
		return this.alertLimit;
	}

	public void setAlertLimit(Integer alertLimit) {
		this.alertLimit = alertLimit;
	}
}
