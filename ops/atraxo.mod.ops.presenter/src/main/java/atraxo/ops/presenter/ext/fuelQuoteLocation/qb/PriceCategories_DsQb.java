/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelQuoteLocation.qb;

import atraxo.ops.presenter.impl.fuelQuoteLocation.model.PriceCategories_Ds;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;

public class PriceCategories_DsQb extends QueryBuilderWithJpql<PriceCategories_Ds, PriceCategories_Ds, Object> {

	@Override
	public void setFilter(PriceCategories_Ds filter) {

		if (filter.getContractId() == null) {
			filter.setContractId(0);
		}
		super.setFilter(filter);
	}

}
