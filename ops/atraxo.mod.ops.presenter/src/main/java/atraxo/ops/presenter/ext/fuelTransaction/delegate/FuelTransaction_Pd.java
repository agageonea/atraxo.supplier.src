package atraxo.ops.presenter.ext.fuelTransaction.delegate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import atraxo.ad.domain.impl.ad.Category;
import atraxo.ad.domain.impl.ad.NotificationAction;
import atraxo.ad.domain.impl.ad.Priority;
import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.ops.business.api.ext.fuelTicket.IFuelTicketWorkflowStarter;
import atraxo.ops.business.api.fuelTransaction.IFuelTransactionService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.business.ext.fuelTicket.service.FuelTicketWorkflowStarter;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.ops_type.ValidationState;
import atraxo.ops.presenter.impl.fuelTransaction.model.FuelTransaction_Ds;
import seava.j4e.api.model.AppNotification;
import seava.j4e.api.service.notification.IAppNotificationService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class FuelTransaction_Pd extends AbstractPresenterDelegate {

	public void process(FuelTransaction_Ds ds) throws Exception {
		IFuelTransactionService srv = (IFuelTransactionService) this.findEntityService(FuelTransaction.class);
		FuelTicketWorkflowStarter fuelTicketWkfStarter = this.getApplicationContext().getBean(FuelTicketWorkflowStarter.class);

		FuelTransaction ft = srv.findById(ds.getId());
		FuelTicket ticket = srv.process(ft);

		fuelTicketWkfStarter.startFuelTicketWorkflow(Arrays.asList(ticket));
	}

	public void process(List<FuelTransaction_Ds> list) throws Exception {
		int failed = 0;
		int notFailed = 0;

		IFuelTransactionService srv = (IFuelTransactionService) this.findEntityService(FuelTransaction.class);
		IFuelTicketWorkflowStarter fuelTicketWkfStarter = this.getApplicationContext().getBean("fuelTicketWorkflowStarter",
				IFuelTicketWorkflowStarter.class);
		List<Object> ids = new ArrayList<>();
		for (FuelTransaction_Ds ds : list) {
			ids.add(ds.getId());
		}
		List<FuelTransaction> transactions = srv.findByIds(ids);
		List<FuelTicket> ftList = new ArrayList<>();
		srv.process(transactions, ftList);
		fuelTicketWkfStarter.startFuelTicketWorkflow(ftList);
		for (FuelTransaction ft : transactions) {
			if (ValidationState._EMPTY_.equals(ft.getValidationState())) {
				notFailed++;
			} else {
				failed++;
			}
		}
		this.sendNotification(failed, notFailed);
	}

	private void sendNotification(int failed, int notFailed) throws Exception {
		INotificationService srv = (INotificationService) this.findEntityService(Notification.class);
		Notification e = new Notification();
		e.setAction(NotificationAction._NOACTION_);
		e.setCategory(Category._JOB_);
		e.setDescripion(String.format(OpsErrorCode.FT_PROCESS_NT_MSG_DESC.getErrMsg(), notFailed, failed));
		e.setEventDate(GregorianCalendar.getInstance().getTime());
		e.setLifeTime(GregorianCalendar.getInstance().getTime());
		e.setPriority(Priority._DEFAULT_);
		e.setTitle(OpsErrorCode.FT_PROCESS_NT_MSG_TITLE.getErrMsg());
		srv.insert(e);
		this.sendNotificationToClient(e);
	}

	private void sendNotificationToClient(Notification e) {
		AppNotification appNotification = new AppNotification(e.getId(), e.getTitle(), e.getDescripion(), Session.user.get().getClientId());
		appNotification.setAction(e.getAction().getName());
		appNotification.setCategory(e.getCategory().getName());
		appNotification.setEventDate(e.getEventDate());
		appNotification.setLifeTime(e.getLifeTime());
		appNotification.setLink(e.getLink());
		appNotification.setPriority(e.getPriority().getName());
		IAppNotificationService appNotifSrv = (IAppNotificationService) this.getApplicationContext().getBean("notificationBusinessService");
		appNotifSrv.add(appNotification);
	}

}
