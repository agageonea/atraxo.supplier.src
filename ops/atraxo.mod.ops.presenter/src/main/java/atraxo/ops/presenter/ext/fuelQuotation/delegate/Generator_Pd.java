package atraxo.ops.presenter.ext.fuelQuotation.delegate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.ad.business.api.externalReport.IExternalReportService;
import atraxo.ad.domain.impl.externalReport.ExternalReport;
import atraxo.fmbas.business.api.contacts.IContactsService;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.ops.business.api.fuelOrder.IFuelOrderService;
import atraxo.ops.business.api.fuelQuotation.IFuelQuotationService;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.presenter.impl.fuelQuotation.model.FuelQuotation_Ds;
import atraxo.ops.presenter.impl.fuelQuotation.model.FuelQuotation_DsParam;
import seava.j4e.presenter.service.AbstractPresenterDelegate;

public class Generator_Pd extends AbstractPresenterDelegate {

	private static final Logger logger = LoggerFactory.getLogger(Generator_Pd.class);

	public void generate(FuelQuotation_Ds data, FuelQuotation_DsParam param) throws Exception {
		IFuelOrderService foService = (IFuelOrderService) this.findEntityService(FuelOrder.class);
		IFuelQuotationService fqService = (IFuelQuotationService) this.findEntityService(FuelQuotation.class);
		IContactsService cService = (IContactsService) this.findEntityService(Contacts.class);
		Contacts contact = null;
		try {
			contact = cService.findById(param.getOrderSubmittedBy());
		} catch (Exception e) {
			logger.warn("Could not fin Contacts by id, will do nothing !", e);
		}
		FuelQuotation fq = fqService.findById(data.getId());
		foService.generate(fq, param.getOrderPostedOn(), contact, param.getOrderCustomerRef());
	}

	public void generateReport(FuelQuotation_Ds ds, FuelQuotation_DsParam param) throws Exception {
		IExternalReportService reportService = (IExternalReportService) this.findEntityService(ExternalReport.class);
		reportService.runReport(ds, "Fuel Quote Report", "PDF", ds.getCode());
	}

}
