/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.ext.fuelRequest.service;

import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.presenter.impl.fuelRequest.model.FuelRequest_Ds;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.presenter.service.ds.AbstractEntityDsService;

public class FuelRequest_DsService extends AbstractEntityDsService<FuelRequest_Ds, FuelRequest_Ds, Object, FuelRequest> implements
		IDsService<FuelRequest_Ds, FuelRequest_Ds, Object> {

	// Implement me ...

}
