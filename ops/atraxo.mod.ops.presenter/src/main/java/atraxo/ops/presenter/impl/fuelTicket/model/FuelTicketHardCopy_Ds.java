/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.presenter.impl.fuelTicket.model;

import atraxo.fmbas.presenter.impl.abstracts.model.AbstractDs_Ds;
import atraxo.ops.domain.impl.fuelTicket.FuelTicketHardCopy;
import seava.j4e.api.annotation.Ds;
import seava.j4e.api.annotation.DsField;
import seava.j4e.api.annotation.RefLookup;
import seava.j4e.api.annotation.RefLookups;

/**
 * Generated code. Do not modify in this file.
 */
@Ds(entity = FuelTicketHardCopy.class)
@RefLookups({@RefLookup(refId = FuelTicketHardCopy_Ds.F_FUELTICKETID)})
public class FuelTicketHardCopy_Ds extends AbstractDs_Ds<FuelTicketHardCopy> {

	public static final String ALIAS = "ops_FuelTicketHardCopy_Ds";

	public static final String F_FILE = "file";
	public static final String F_FUELTICKETID = "fuelTicketId";
	public static final String F_FUELTICKETNR = "fuelTicketNr";

	@DsField
	private byte[] file;

	@DsField(join = "left", path = "fuelTicket.id")
	private Integer fuelTicketId;

	@DsField(join = "left", path = "fuelTicket.ticketNo")
	private String fuelTicketNr;

	/**
	 * Default constructor
	 */
	public FuelTicketHardCopy_Ds() {
		super();
	}

	/**
	 * @param e
	 */
	public FuelTicketHardCopy_Ds(FuelTicketHardCopy e) {
		super(e);
	}

	public byte[] getFile() {
		return this.file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public Integer getFuelTicketId() {
		return this.fuelTicketId;
	}

	public void setFuelTicketId(Integer fuelTicketId) {
		this.fuelTicketId = fuelTicketId;
	}

	public String getFuelTicketNr() {
		return this.fuelTicketNr;
	}

	public void setFuelTicketNr(String fuelTicketNr) {
		this.fuelTicketNr = fuelTicketNr;
	}
}
