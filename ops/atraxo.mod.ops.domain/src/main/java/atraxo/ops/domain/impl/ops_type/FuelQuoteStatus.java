/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelQuoteStatus {

	_EMPTY_(""), _NEW_("New"), _SUBMITTED_("Submitted"), _NEGOTIATING_(
			"Negotiating"), _ORDERED_("Ordered"), _CLOSED_("Closed"), _CANCELED_(
			"Canceled"), _EXPIRED_("Expired");

	private String name;

	private FuelQuoteStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelQuoteStatus getByName(String name) {
		for (FuelQuoteStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FuelQuoteStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
