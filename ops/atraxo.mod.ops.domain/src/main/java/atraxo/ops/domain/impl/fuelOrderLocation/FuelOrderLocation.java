/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelOrderLocation;

import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractSubTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicatorConverter;
import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.cmm.domain.impl.cmm_type.PricingMethodConverter;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.ProductConverter;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.ext.json.message.SoneMessage;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreqConverter;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDayConverter;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.fuelOrderLocation.FuelOrderLocationMessage;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.ops_type.InvoiceStatus;
import atraxo.ops.domain.impl.ops_type.InvoiceStatusConverter;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.OperationTypeConverter;
import atraxo.ops.domain.impl.ops_type.PricePolicyType;
import atraxo.ops.domain.impl.ops_type.PricePolicyTypeConverter;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelOrderLocation} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = FuelOrderLocation.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelOrderLocation e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelOrderLocation.TABLE_NAME)
public class FuelOrderLocation extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "OPS_LOCATION_PURCHASE_ORDERS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelOrderLocation.findByBusiness";

	@Column(name = "price_basis", length = 32)
	@Convert(converter = PricingMethodConverter.class)
	private PricingMethod priceBasis;

	@Column(name = "flight_type", length = 16)
	@Convert(converter = FlightTypeIndicatorConverter.class)
	private FlightTypeIndicator flightType;

	@Column(name = "operational_type", length = 32)
	@Convert(converter = OperationTypeConverter.class)
	private OperationType operationalType;

	@Column(name = "product", length = 32)
	@Convert(converter = ProductConverter.class)
	private Product product;

	@Column(name = "delivery_method", length = 32)
	@Convert(converter = ContractSubTypeConverter.class)
	private ContractSubType deliveryMethod;

	@Column(name = "quantity", precision = 19, scale = 6)
	private BigDecimal quantity;

	@Column(name = "events", precision = 11)
	private Integer events;

	@Column(name = "index_offset", length = 32)
	private String indexOffset;

	@Column(name = "base_price", precision = 19, scale = 6)
	private BigDecimal basePrice;

	@Column(name = "differential", precision = 19, scale = 6)
	private BigDecimal differential;

	@Column(name = "margin", precision = 19, scale = 6)
	private BigDecimal margin;

	@Column(name = "fuel_price", precision = 19, scale = 6)
	private BigDecimal fuelPrice;

	@Column(name = "selling_fuel_price", precision = 19, scale = 6)
	private BigDecimal sellingFuelPrice;

	@Column(name = "into_plane_fees", precision = 19, scale = 6)
	private BigDecimal intoPlaneFees;

	@Column(name = "other_fees", precision = 19, scale = 6)
	private BigDecimal otherFees;

	@Column(name = "taxes", precision = 19, scale = 6)
	private BigDecimal taxes;

	@Column(name = "per_flight_fees", precision = 19, scale = 6)
	private BigDecimal perFlightFee;

	@Column(name = "dft", precision = 19, scale = 6)
	private BigDecimal dft;

	@Column(name = "total_price_per_uom", precision = 19, scale = 6)
	private BigDecimal totalPricePerUom;

	@Column(name = "payment_terms", precision = 4)
	private Integer paymentTerms;

	@Column(name = "payment_reference_day", length = 32)
	@Convert(converter = PaymentDayConverter.class)
	private PaymentDay paymentRefDay;

	@Column(name = "invoice_frequency", length = 32)
	@Convert(converter = InvoiceFreqConverter.class)
	private InvoiceFreq invoiceFrequency;

	@Column(name = "exchange_rate_offset", length = 32)
	private String exchangeRateOffset;

	@Temporal(TemporalType.DATE)
	@Column(name = "fuel_release_starts_on")
	private Date fuelReleaseStartsOn;

	@Temporal(TemporalType.DATE)
	@Column(name = "fuel_release_ends_on")
	private Date fuelReleaseEndsOn;

	@Column(name = "price_policy_type", length = 32)
	@Convert(converter = PricePolicyTypeConverter.class)
	private PricePolicyType pricePolicyType;

	@Column(name = "buy_invoice_status", length = 32)
	@Convert(converter = InvoiceStatusConverter.class)
	private InvoiceStatus buyInvoiceStatus;

	@Column(name = "sales_invoice_status", length = 32)
	@Convert(converter = InvoiceStatusConverter.class)
	private InvoiceStatus salesInvoiceStatus;

	@Transient
	private String forex;

	@Transient
	private SoneMessage warningMessage;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelOrder.class)
	@JoinColumn(name = "purchase_order_id", referencedColumnName = "id")
	private FuelOrder fuelOrder;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Locations location;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "supplier_id", referencedColumnName = "id")
	private Suppliers supplier;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contract;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "into_plane_agent_id", referencedColumnName = "id")
	private Suppliers iplAgent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "payment_currency_id", referencedColumnName = "id")
	private Currencies paymentCurrency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "payment_unit_id", referencedColumnName = "id")
	private Unit paymentUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FinancialSources.class)
	@JoinColumn(name = "financial_source_id", referencedColumnName = "id")
	private FinancialSources financialSource;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "average_method_id", referencedColumnName = "id")
	private AverageMethod averageMethod;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PricePolicy.class)
	@JoinColumn(name = "price_policy_id", referencedColumnName = "id")
	private PricePolicy pricePolicy;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FlightEvent.class, mappedBy = "locOrder")
	private Collection<FlightEvent> flightEvents;

	public PricingMethod getPriceBasis() {
		return this.priceBasis;
	}

	public void setPriceBasis(PricingMethod priceBasis) {
		this.priceBasis = priceBasis;
	}

	public FlightTypeIndicator getFlightType() {
		return this.flightType;
	}

	public void setFlightType(FlightTypeIndicator flightType) {
		this.flightType = flightType;
	}

	public OperationType getOperationalType() {
		return this.operationalType;
	}

	public void setOperationalType(OperationType operationalType) {
		this.operationalType = operationalType;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ContractSubType getDeliveryMethod() {
		return this.deliveryMethod;
	}

	public void setDeliveryMethod(ContractSubType deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Integer getEvents() {
		return this.events;
	}

	public void setEvents(Integer events) {
		this.events = events;
	}

	public String getIndexOffset() {
		return this.indexOffset;
	}

	public void setIndexOffset(String indexOffset) {
		this.indexOffset = indexOffset;
	}

	public BigDecimal getBasePrice() {
		return this.basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getDifferential() {
		return this.differential;
	}

	public void setDifferential(BigDecimal differential) {
		this.differential = differential;
	}

	public BigDecimal getMargin() {
		return this.margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public BigDecimal getFuelPrice() {
		return this.fuelPrice;
	}

	public void setFuelPrice(BigDecimal fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	public BigDecimal getSellingFuelPrice() {
		return this.sellingFuelPrice;
	}

	public void setSellingFuelPrice(BigDecimal sellingFuelPrice) {
		this.sellingFuelPrice = sellingFuelPrice;
	}

	public BigDecimal getIntoPlaneFees() {
		return this.intoPlaneFees;
	}

	public void setIntoPlaneFees(BigDecimal intoPlaneFees) {
		this.intoPlaneFees = intoPlaneFees;
	}

	public BigDecimal getOtherFees() {
		return this.otherFees;
	}

	public void setOtherFees(BigDecimal otherFees) {
		this.otherFees = otherFees;
	}

	public BigDecimal getTaxes() {
		return this.taxes;
	}

	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}

	public BigDecimal getPerFlightFee() {
		return this.perFlightFee;
	}

	public void setPerFlightFee(BigDecimal perFlightFee) {
		this.perFlightFee = perFlightFee;
	}

	public BigDecimal getDft() {
		return this.dft;
	}

	public void setDft(BigDecimal dft) {
		this.dft = dft;
	}

	public BigDecimal getTotalPricePerUom() {
		return this.totalPricePerUom;
	}

	public void setTotalPricePerUom(BigDecimal totalPricePerUom) {
		this.totalPricePerUom = totalPricePerUom;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentRefDay() {
		return this.paymentRefDay;
	}

	public void setPaymentRefDay(PaymentDay paymentRefDay) {
		this.paymentRefDay = paymentRefDay;
	}

	public InvoiceFreq getInvoiceFrequency() {
		return this.invoiceFrequency;
	}

	public void setInvoiceFrequency(InvoiceFreq invoiceFrequency) {
		this.invoiceFrequency = invoiceFrequency;
	}

	public String getExchangeRateOffset() {
		return this.exchangeRateOffset;
	}

	public void setExchangeRateOffset(String exchangeRateOffset) {
		this.exchangeRateOffset = exchangeRateOffset;
	}

	public Date getFuelReleaseStartsOn() {
		return this.fuelReleaseStartsOn;
	}

	public void setFuelReleaseStartsOn(Date fuelReleaseStartsOn) {
		this.fuelReleaseStartsOn = fuelReleaseStartsOn;
	}

	public Date getFuelReleaseEndsOn() {
		return this.fuelReleaseEndsOn;
	}

	public void setFuelReleaseEndsOn(Date fuelReleaseEndsOn) {
		this.fuelReleaseEndsOn = fuelReleaseEndsOn;
	}

	public PricePolicyType getPricePolicyType() {
		return this.pricePolicyType;
	}

	public void setPricePolicyType(PricePolicyType pricePolicyType) {
		this.pricePolicyType = pricePolicyType;
	}

	public InvoiceStatus getBuyInvoiceStatus() {
		return this.buyInvoiceStatus;
	}

	public void setBuyInvoiceStatus(InvoiceStatus buyInvoiceStatus) {
		this.buyInvoiceStatus = buyInvoiceStatus;
	}

	public InvoiceStatus getSalesInvoiceStatus() {
		return this.salesInvoiceStatus;
	}

	public void setSalesInvoiceStatus(InvoiceStatus salesInvoiceStatus) {
		this.salesInvoiceStatus = salesInvoiceStatus;
	}

	public String getForex() {
		return this.getFinancialSource().getCode() + "/"
				+ this.getAverageMethod().getName();
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public SoneMessage getWarningMessage() {
		return FuelOrderLocationMessage.resultMap.get() != null
				? FuelOrderLocationMessage.resultMap.get().remove(this.getId())
				: null;
	}

	public void setWarningMessage(SoneMessage warningMessage) {
		if (FuelOrderLocationMessage.resultMap.get() == null) {
			FuelOrderLocationMessage.resultMap
					.set(new HashMap<Integer, SoneMessage>());
		}
		FuelOrderLocationMessage.resultMap.get().put(this.getId(),
				warningMessage);
	}

	public FuelOrder getFuelOrder() {
		return this.fuelOrder;
	}

	public void setFuelOrder(FuelOrder fuelOrder) {
		if (fuelOrder != null) {
			this.__validate_client_context__(fuelOrder.getClientId());
		}
		this.fuelOrder = fuelOrder;
	}
	public Locations getLocation() {
		return this.location;
	}

	public void setLocation(Locations location) {
		if (location != null) {
			this.__validate_client_context__(location.getClientId());
		}
		this.location = location;
	}
	public Suppliers getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Suppliers supplier) {
		if (supplier != null) {
			this.__validate_client_context__(supplier.getClientId());
		}
		this.supplier = supplier;
	}
	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		if (contract != null) {
			this.__validate_client_context__(contract.getClientId());
		}
		this.contract = contract;
	}
	public Suppliers getIplAgent() {
		return this.iplAgent;
	}

	public void setIplAgent(Suppliers iplAgent) {
		if (iplAgent != null) {
			this.__validate_client_context__(iplAgent.getClientId());
		}
		this.iplAgent = iplAgent;
	}
	public Currencies getPaymentCurrency() {
		return this.paymentCurrency;
	}

	public void setPaymentCurrency(Currencies paymentCurrency) {
		if (paymentCurrency != null) {
			this.__validate_client_context__(paymentCurrency.getClientId());
		}
		this.paymentCurrency = paymentCurrency;
	}
	public Unit getPaymentUnit() {
		return this.paymentUnit;
	}

	public void setPaymentUnit(Unit paymentUnit) {
		if (paymentUnit != null) {
			this.__validate_client_context__(paymentUnit.getClientId());
		}
		this.paymentUnit = paymentUnit;
	}
	public FinancialSources getFinancialSource() {
		return this.financialSource;
	}

	public void setFinancialSource(FinancialSources financialSource) {
		if (financialSource != null) {
			this.__validate_client_context__(financialSource.getClientId());
		}
		this.financialSource = financialSource;
	}
	public AverageMethod getAverageMethod() {
		return this.averageMethod;
	}

	public void setAverageMethod(AverageMethod averageMethod) {
		if (averageMethod != null) {
			this.__validate_client_context__(averageMethod.getClientId());
		}
		this.averageMethod = averageMethod;
	}
	public PricePolicy getPricePolicy() {
		return this.pricePolicy;
	}

	public void setPricePolicy(PricePolicy pricePolicy) {
		if (pricePolicy != null) {
			this.__validate_client_context__(pricePolicy.getClientId());
		}
		this.pricePolicy = pricePolicy;
	}

	public Collection<FlightEvent> getFlightEvents() {
		return this.flightEvents;
	}

	public void setFlightEvents(Collection<FlightEvent> flightEvents) {
		this.flightEvents = flightEvents;
	}

	/**
	 * @param e
	 */
	public void addToFlightEvents(FlightEvent e) {
		if (this.flightEvents == null) {
			this.flightEvents = new ArrayList<>();
		}
		e.setLocOrder(this);
		this.flightEvents.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
