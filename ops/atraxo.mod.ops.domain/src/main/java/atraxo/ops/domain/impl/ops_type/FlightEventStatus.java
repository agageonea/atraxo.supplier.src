/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FlightEventStatus {

	_EMPTY_(""), _SCHEDULED_("Scheduled"), _FULFILLED_("Fulfilled"), _CANCELED_(
			"Canceled"), _INVOICED_("Invoiced");

	private String name;

	private FlightEventStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FlightEventStatus getByName(String name) {
		for (FlightEventStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FlightEventStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
