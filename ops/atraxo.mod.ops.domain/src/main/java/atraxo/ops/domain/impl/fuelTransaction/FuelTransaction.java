/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelTransaction;

import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.PaymentTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.ProductIATA;
import atraxo.cmm.domain.impl.cmm_type.ProductIATAConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionEquipment;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import atraxo.ops.domain.impl.ops_type.DeliveryType;
import atraxo.ops.domain.impl.ops_type.DeliveryTypeConverter;
import atraxo.ops.domain.impl.ops_type.FuelTicketCustomsStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketCustomsStatusConverter;
import atraxo.ops.domain.impl.ops_type.IPTransactionCode;
import atraxo.ops.domain.impl.ops_type.IPTransactionCodeConverter;
import atraxo.ops.domain.impl.ops_type.InterfaceType;
import atraxo.ops.domain.impl.ops_type.InterfaceTypeConverter;
import atraxo.ops.domain.impl.ops_type.InternationalFlight;
import atraxo.ops.domain.impl.ops_type.InternationalFlightConverter;
import atraxo.ops.domain.impl.ops_type.TicketSource;
import atraxo.ops.domain.impl.ops_type.TicketSourceConverter;
import atraxo.ops.domain.impl.ops_type.TicketType;
import atraxo.ops.domain.impl.ops_type.TicketTypeConverter;
import atraxo.ops.domain.impl.ops_type.ValidationState;
import atraxo.ops.domain.impl.ops_type.ValidationStateConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelTransaction} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelTransaction.NQ_FIND_BY_BUSINESSKEY, query = "SELECT e FROM FuelTransaction e WHERE e.clientId = :clientId and e.airportCode = :airportCode and e.supplierCode = :supplierCode and e.transactionDate = :transactionDate and e.ticketNumber = :ticketNumber and e.ticketType = :ticketType", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelTransaction.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelTransaction e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelTransaction.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelTransaction.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "airport_code", "supplier_code",
		"transaction_date", "ticket_number", "ticket_type"})})
public class FuelTransaction extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_TRANSACTION";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: BusinessKey.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY = "FuelTransaction.findByBusinessKey";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelTransaction.findByBusiness";

	@Column(name = "interface_type", length = 16)
	@Convert(converter = InterfaceTypeConverter.class)
	private InterfaceType interfaceType;

	@Column(name = "delivery_type", length = 16)
	@Convert(converter = DeliveryTypeConverter.class)
	private DeliveryType deliveryType;

	@Column(name = "into_plane_code", length = 32)
	private String intoPlaneCode;

	@Column(name = "into_plane_name", length = 50)
	private String intoPlaneName;

	@Column(name = "airport_code", length = 5)
	private String airportCode;

	@Column(name = "receiver_code", length = 32)
	private String receiverCode;

	@Column(name = "receiver_name", length = 50)
	private String receiverName;

	@Column(name = "receiver_account_number", length = 50)
	private String receiverAccountNumber;

	@Column(name = "supplier_code", length = 32)
	private String supplierCode;

	@Column(name = "supplier_name", length = 50)
	private String supplierName;

	@Column(name = "ticket_issuer_code", length = 32)
	private String ticketIssuerCode;

	@Column(name = "ticket_issuer_name", length = 50)
	private String ticketIssuerName;

	@Column(name = "ticket_number", length = 20)
	private String ticketNumber;

	@Column(name = "ticket_type", length = 16)
	@Convert(converter = TicketTypeConverter.class)
	private TicketType ticketType;

	@Column(name = "ticket_source", length = 16)
	@Convert(converter = TicketSourceConverter.class)
	private TicketSource ticketSource;

	@Column(name = "previous_ticket", length = 16)
	private String previousTicket;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "transaction_date")
	private Date transactionDate;

	@Column(name = "airline_flight_id", length = 10)
	private String airlineFlightId;

	@Column(name = "aircraft_identification", length = 10)
	private String aircraftIdentification;

	@Column(name = "international_flight", length = 20)
	@Convert(converter = InternationalFlightConverter.class)
	private InternationalFlight internationalFlight;

	@Column(name = "aircraft_type", length = 4)
	private String aircraftType;

	@Column(name = "next_destination", length = 5)
	private String nextDestination;

	@Column(name = "final_destination", length = 5)
	private String finalDestination;

	@Column(name = "flight_service_type", length = 16)
	@Convert(converter = FlightServiceTypeConverter.class)
	private FlightServiceType flightServiceType;

	@Column(name = "payment_type", length = 32)
	@Convert(converter = PaymentTypeConverter.class)
	private PaymentType paymentType;

	@Column(name = "card_name", length = 35)
	private String cardName;

	@Column(name = "card_number", length = 20)
	private String cardNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "card_expiry")
	private Date cardExpiry;

	@Column(name = "amount_received", precision = 19, scale = 6)
	private BigDecimal amountReceived;

	@Column(name = "amount_received_currency", length = 3)
	private String amountReceivedCurr;

	@Column(name = "ip_transaction_code", length = 16)
	@Convert(converter = IPTransactionCodeConverter.class)
	private IPTransactionCode ipTransactionCode;

	@Column(name = "product_id", length = 32)
	@Convert(converter = ProductIATAConverter.class)
	private ProductIATA productId;

	@Column(name = "custom_status", length = 16)
	@Convert(converter = FuelTicketCustomsStatusConverter.class)
	private FuelTicketCustomsStatus customStatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "transaction_start_date")
	private Date transactionStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "transaction_end_date")
	private Date transactionEndDate;

	@Column(name = "validation_state", length = 16)
	@Convert(converter = ValidationStateConverter.class)
	private ValidationState validationState;

	@Column(name = "validation_result", length = 2500)
	private String validationResult;

	@Column(name = "unique_ticket_id", length = 50)
	private String uniqueTicketID;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelTransactionSource.class)
	@JoinColumn(name = "import_source_id", referencedColumnName = "id")
	private FuelTransactionSource importSource;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FuelTransactionEquipment.class, mappedBy = "fuelTransaction", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<FuelTransactionEquipment> equipment;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FuelTransactionQuantity.class, mappedBy = "fuelTransaction", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<FuelTransactionQuantity> quantity;

	public InterfaceType getInterfaceType() {
		return this.interfaceType;
	}

	public void setInterfaceType(InterfaceType interfaceType) {
		this.interfaceType = interfaceType;
	}

	public DeliveryType getDeliveryType() {
		return this.deliveryType;
	}

	public void setDeliveryType(DeliveryType deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getIntoPlaneCode() {
		return this.intoPlaneCode;
	}

	public void setIntoPlaneCode(String intoPlaneCode) {
		this.intoPlaneCode = intoPlaneCode;
	}

	public String getIntoPlaneName() {
		return this.intoPlaneName;
	}

	public void setIntoPlaneName(String intoPlaneName) {
		this.intoPlaneName = intoPlaneName;
	}

	public String getAirportCode() {
		return this.airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getReceiverCode() {
		return this.receiverCode;
	}

	public void setReceiverCode(String receiverCode) {
		this.receiverCode = receiverCode;
	}

	public String getReceiverName() {
		return this.receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverAccountNumber() {
		return this.receiverAccountNumber;
	}

	public void setReceiverAccountNumber(String receiverAccountNumber) {
		this.receiverAccountNumber = receiverAccountNumber;
	}

	public String getSupplierCode() {
		return this.supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getTicketIssuerCode() {
		return this.ticketIssuerCode;
	}

	public void setTicketIssuerCode(String ticketIssuerCode) {
		this.ticketIssuerCode = ticketIssuerCode;
	}

	public String getTicketIssuerName() {
		return this.ticketIssuerName;
	}

	public void setTicketIssuerName(String ticketIssuerName) {
		this.ticketIssuerName = ticketIssuerName;
	}

	public String getTicketNumber() {
		return this.ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public TicketType getTicketType() {
		return this.ticketType;
	}

	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}

	public TicketSource getTicketSource() {
		return this.ticketSource;
	}

	public void setTicketSource(TicketSource ticketSource) {
		this.ticketSource = ticketSource;
	}

	public String getPreviousTicket() {
		return this.previousTicket;
	}

	public void setPreviousTicket(String previousTicket) {
		this.previousTicket = previousTicket;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getAirlineFlightId() {
		return this.airlineFlightId;
	}

	public void setAirlineFlightId(String airlineFlightId) {
		this.airlineFlightId = airlineFlightId;
	}

	public String getAircraftIdentification() {
		return this.aircraftIdentification;
	}

	public void setAircraftIdentification(String aircraftIdentification) {
		this.aircraftIdentification = aircraftIdentification;
	}

	public InternationalFlight getInternationalFlight() {
		return this.internationalFlight;
	}

	public void setInternationalFlight(InternationalFlight internationalFlight) {
		this.internationalFlight = internationalFlight;
	}

	public String getAircraftType() {
		return this.aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getNextDestination() {
		return this.nextDestination;
	}

	public void setNextDestination(String nextDestination) {
		this.nextDestination = nextDestination;
	}

	public String getFinalDestination() {
		return this.finalDestination;
	}

	public void setFinalDestination(String finalDestination) {
		this.finalDestination = finalDestination;
	}

	public FlightServiceType getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(FlightServiceType flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public PaymentType getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getCardName() {
		return this.cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getCardExpiry() {
		return this.cardExpiry;
	}

	public void setCardExpiry(Date cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	public BigDecimal getAmountReceived() {
		return this.amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public String getAmountReceivedCurr() {
		return this.amountReceivedCurr;
	}

	public void setAmountReceivedCurr(String amountReceivedCurr) {
		this.amountReceivedCurr = amountReceivedCurr;
	}

	public IPTransactionCode getIpTransactionCode() {
		return this.ipTransactionCode;
	}

	public void setIpTransactionCode(IPTransactionCode ipTransactionCode) {
		this.ipTransactionCode = ipTransactionCode;
	}

	public ProductIATA getProductId() {
		return this.productId;
	}

	public void setProductId(ProductIATA productId) {
		this.productId = productId;
	}

	public FuelTicketCustomsStatus getCustomStatus() {
		return this.customStatus;
	}

	public void setCustomStatus(FuelTicketCustomsStatus customStatus) {
		this.customStatus = customStatus;
	}

	public Date getTransactionStartDate() {
		return this.transactionStartDate;
	}

	public void setTransactionStartDate(Date transactionStartDate) {
		this.transactionStartDate = transactionStartDate;
	}

	public Date getTransactionEndDate() {
		return this.transactionEndDate;
	}

	public void setTransactionEndDate(Date transactionEndDate) {
		this.transactionEndDate = transactionEndDate;
	}

	public ValidationState getValidationState() {
		return this.validationState;
	}

	public void setValidationState(ValidationState validationState) {
		this.validationState = validationState;
	}

	public String getValidationResult() {
		return this.validationResult;
	}

	public void setValidationResult(String validationResult) {
		this.validationResult = validationResult;
	}

	public String getUniqueTicketID() {
		return this.uniqueTicketID;
	}

	public void setUniqueTicketID(String uniqueTicketID) {
		this.uniqueTicketID = uniqueTicketID;
	}

	public FuelTransactionSource getImportSource() {
		return this.importSource;
	}

	public void setImportSource(FuelTransactionSource importSource) {
		if (importSource != null) {
			this.__validate_client_context__(importSource.getClientId());
		}
		this.importSource = importSource;
	}

	public Collection<FuelTransactionEquipment> getEquipment() {
		return this.equipment;
	}

	public void setEquipment(Collection<FuelTransactionEquipment> equipment) {
		this.equipment = equipment;
	}

	/**
	 * @param e
	 */
	public void addToEquipment(FuelTransactionEquipment e) {
		if (this.equipment == null) {
			this.equipment = new ArrayList<>();
		}
		e.setFuelTransaction(this);
		this.equipment.add(e);
	}
	public Collection<FuelTransactionQuantity> getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Collection<FuelTransactionQuantity> quantity) {
		this.quantity = quantity;
	}

	/**
	 * @param e
	 */
	public void addToQuantity(FuelTransactionQuantity e) {
		if (this.quantity == null) {
			this.quantity = new ArrayList<>();
		}
		e.setFuelTransaction(this);
		this.quantity.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.airportCode == null) ? 0 : this.airportCode.hashCode());
		result = prime
				* result
				+ ((this.supplierCode == null) ? 0 : this.supplierCode
						.hashCode());
		result = prime
				* result
				+ ((this.transactionDate == null) ? 0 : this.transactionDate
						.hashCode());
		result = prime
				* result
				+ ((this.ticketNumber == null) ? 0 : this.ticketNumber
						.hashCode());
		result = prime * result
				+ ((this.ticketType == null) ? 0 : this.ticketType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		FuelTransaction other = (FuelTransaction) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.airportCode == null) {
			if (other.airportCode != null) {
				return false;
			}
		} else if (!this.airportCode.equals(other.airportCode)) {
			return false;
		}
		if (this.supplierCode == null) {
			if (other.supplierCode != null) {
				return false;
			}
		} else if (!this.supplierCode.equals(other.supplierCode)) {
			return false;
		}
		if (this.transactionDate == null) {
			if (other.transactionDate != null) {
				return false;
			}
		} else if (!this.transactionDate.equals(other.transactionDate)) {
			return false;
		}
		if (this.ticketNumber == null) {
			if (other.ticketNumber != null) {
				return false;
			}
		} else if (!this.ticketNumber.equals(other.ticketNumber)) {
			return false;
		}
		if (this.ticketType == null) {
			if (other.ticketType != null) {
				return false;
			}
		} else if (!this.ticketType.equals(other.ticketType)) {
			return false;
		}
		return true;
	}
}
