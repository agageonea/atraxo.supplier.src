/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelTicket;

import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicatorConverter;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperation;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperationConverter;
import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.FuelingTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.PaymentTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.ProductConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractSubsidiary;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.fuelTicket.CancelFuelTicketResult;
import atraxo.ops.domain.ext.fuelTicket.ProvidedQuantityInforamtionResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatusConverter;
import atraxo.ops.domain.impl.ops_type.FuelTicketCustomsStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketCustomsStatusConverter;
import atraxo.ops.domain.impl.ops_type.FuelTicketSource;
import atraxo.ops.domain.impl.ops_type.FuelTicketSourceConverter;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatusConverter;
import atraxo.ops.domain.impl.ops_type.FuelTicketTransmissionStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketTransmissionStatusConverter;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatusConverter;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import atraxo.ops.domain.impl.ops_type.RevocationReasonConverter;
import atraxo.ops.domain.impl.ops_type.SendInvoiceBy;
import atraxo.ops.domain.impl.ops_type.SendInvoiceByConverter;
import atraxo.ops.domain.impl.ops_type.Suffix;
import atraxo.ops.domain.impl.ops_type.SuffixConverter;
import atraxo.ops.domain.impl.ops_type.TemperatureUnit;
import atraxo.ops.domain.impl.ops_type.TemperatureUnitConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelTicket} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelTicket.NQ_FIND_BY_BUSINESSKEY, query = "SELECT e FROM FuelTicket e WHERE e.clientId = :clientId and e.departure = :departure and e.ticketNo = :ticketNo and e.customer = :customer and e.deliveryDate = :deliveryDate and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelTicket.NQ_FIND_BY_BUSINESSKEY_PRIMITIVE, query = "SELECT e FROM FuelTicket e WHERE e.clientId = :clientId and e.departure.id = :departureId and e.ticketNo = :ticketNo and e.customer.id = :customerId and e.deliveryDate = :deliveryDate and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelTicket.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelTicket e WHERE e.clientId = :clientId and e.id = :id and e.subsidiaryId in :subsidiaryIds", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelTicket.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelTicket.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "departure_id", "ticket_number",
		"customer_id", "delivery_date"})})
public class FuelTicket extends AbstractSubsidiary implements Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_TICKETS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: BusinessKey.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY = "FuelTicket.findByBusinessKey";
	/**
	 * Named query find by unique key: BusinessKey using the ID field for references.
	 */
	public static final String NQ_FIND_BY_BUSINESSKEY_PRIMITIVE = "FuelTicket.findByBusinessKey_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelTicket.findByBusiness";

	@Column(name = "uplift_volume", precision = 19, scale = 6)
	private BigDecimal upliftVolume;

	@Column(name = "ticket_number", length = 32)
	private String ticketNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "delivery_date")
	private Date deliveryDate;

	@Column(name = "airline_designator", length = 3)
	private String airlineDesignator;

	@Column(name = "flight_number", length = 4)
	private String flightNo;

	@Column(name = "suffix", length = 16)
	@Convert(converter = SuffixConverter.class)
	private Suffix suffix;

	@Column(name = "custom_status", length = 16)
	@Convert(converter = FuelTicketCustomsStatusConverter.class)
	private FuelTicketCustomsStatus customsStatus;

	@Column(name = "product_type", length = 32)
	@Convert(converter = ProductConverter.class)
	private Product productType;

	@Column(name = "fueling_operation", length = 16)
	@Convert(converter = FuelTicketFuelingOperationConverter.class)
	private FuelTicketFuelingOperation fuelingOperation;

	@Column(name = "dom_int_indicator", length = 16)
	@Convert(converter = FlightTypeIndicatorConverter.class)
	private FlightTypeIndicator indicator;

	@Column(name = "net_uplift_quantity", precision = 19, scale = 6)
	private BigDecimal netUpliftQuantity;

	@Column(name = "before_fueling", precision = 19, scale = 6)
	private BigDecimal beforeFueling;

	@Column(name = "after_fueling", precision = 19, scale = 6)
	private BigDecimal afterFueling;

	@Column(name = "density", precision = 19, scale = 6)
	private BigDecimal density;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fueling_start_date")
	private Date fuelingStartDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fueling_end_date")
	private Date fuelingEndDate;

	@Column(name = "delivery_type", length = 16)
	private String deliveryType;

	@Column(name = "source", length = 16)
	@Convert(converter = FuelTicketSourceConverter.class)
	private FuelTicketSource source;

	@Column(name = "transport", length = 16)
	@Convert(converter = FuelingTypeConverter.class)
	private FuelingType transport;

	@Column(name = "ticket_status", length = 16)
	@Convert(converter = FuelTicketStatusConverter.class)
	private FuelTicketStatus ticketStatus;

	@Column(name = "transmission_status", length = 16)
	@Convert(converter = FuelTicketTransmissionStatusConverter.class)
	private FuelTicketTransmissionStatus transmissionStatus;

	@Column(name = "transmission_details", length = 255)
	private String transmissionDetails;

	@Column(name = "approval_status", length = 32)
	@Convert(converter = FuelTicketApprovalStatusConverter.class)
	private FuelTicketApprovalStatus approvalStatus;

	@Temporal(TemporalType.DATE)
	@Column(name = "transmission_date")
	private Date transmissionDate;

	@Column(name = "captured_by", length = 255)
	private String capturedBy;

	@Column(name = "approved_by", length = 255)
	private String approvedBy;

	@Column(name = "bill_status", length = 32)
	@Convert(converter = BillStatusConverter.class)
	private BillStatus billStatus;

	@Column(name = "invoice_status", length = 32)
	@Convert(converter = OutgoingInvoiceStatusConverter.class)
	private OutgoingInvoiceStatus invoiceStatus;

	@Column(name = "meter_reading_start", precision = 11)
	private Integer meterStartIndex;

	@Column(name = "meter_reading_end", precision = 11)
	private Integer meterEndIndex;

	@Column(name = "temperature", precision = 11)
	private Integer temperature;

	@Column(name = "temperature_unit", length = 32)
	@Convert(converter = TemperatureUnitConverter.class)
	private TemperatureUnit temperatureUnit;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "refueler_arrival_time")
	private Date refuelerArrivalTime;

	@Column(name = "payment_type", length = 32)
	@Convert(converter = PaymentTypeConverter.class)
	private PaymentType paymentType;

	@Column(name = "card_number", length = 20)
	private String cardNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "card_expiring_date")
	private Date cardExpiringDate;

	@Column(name = "card_holder", length = 64)
	private String cardHolder;

	@Column(name = "amount_received", precision = 19, scale = 6)
	private BigDecimal amountReceived;

	@Column(name = "send_invoice_by", length = 32)
	@Convert(converter = SendInvoiceByConverter.class)
	private SendInvoiceBy sendInvoiceBy;

	@Column(name = "send_invoice_to", length = 64)
	private String sendInvoiceTo;

	@Column(name = "is_deleted")
	private Boolean isDeleted;

	@Column(name = "flight_service_type", length = 16)
	@Convert(converter = FlightServiceTypeConverter.class)
	private FlightServiceType flightServiceType;

	@Column(name = "subsidiary_code", length = 64)
	private String subsidiaryCode;

	@Column(name = "revocation_reason", length = 64)
	@Convert(converter = RevocationReasonConverter.class)
	private RevocationReason revocationRason;

	@Transient
	private String toleranceMessage;

	@Transient
	private String cancelationResult;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "departure_id", referencedColumnName = "id")
	private Locations departure;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "uplift_unit_id", referencedColumnName = "id")
	private Unit upliftUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "supplier_id", referencedColumnName = "id")
	private Suppliers supplier;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "fueller_id", referencedColumnName = "id")
	private Suppliers fueller;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "destination_id", referencedColumnName = "id")
	private Locations destination;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Aircraft.class)
	@JoinColumn(name = "registration_id", referencedColumnName = "id")
	private Aircraft registration;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "final_destination_id", referencedColumnName = "id")
	private Locations finalDest;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "net_uplift_unit_id", referencedColumnName = "id")
	private Unit netUpliftUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "before_fueling_unit_id", referencedColumnName = "id")
	private Unit beforeFuelingUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "after_fueling_unit_id", referencedColumnName = "id")
	private Unit afterFuelingUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "density_mass_unit_id", referencedColumnName = "id")
	private Unit densityUnit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "density_volume_unit_id", referencedColumnName = "id")
	private Unit densityVolume;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AcTypes.class)
	@JoinColumn(name = "aircraft_type_id", referencedColumnName = "id")
	private AcTypes acType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "reseller_id", referencedColumnName = "id")
	private Customer reseller;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "amount_currency_id", referencedColumnName = "id")
	private Currencies ammountCurr;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelTicket.class)
	@JoinColumn(name = "reference_ticket_id", referencedColumnName = "id")
	private FuelTicket referenceTicket;

	public BigDecimal getUpliftVolume() {
		return this.upliftVolume;
	}

	public void setUpliftVolume(BigDecimal upliftVolume) {
		this.upliftVolume = upliftVolume;
	}

	public String getTicketNo() {
		return this.ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getAirlineDesignator() {
		return this.airlineDesignator;
	}

	public void setAirlineDesignator(String airlineDesignator) {
		this.airlineDesignator = airlineDesignator;
	}

	public String getFlightNo() {
		return this.flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public FuelTicketCustomsStatus getCustomsStatus() {
		return this.customsStatus;
	}

	public void setCustomsStatus(FuelTicketCustomsStatus customsStatus) {
		this.customsStatus = customsStatus;
	}

	public Product getProductType() {
		return this.productType;
	}

	public void setProductType(Product productType) {
		this.productType = productType;
	}

	public FuelTicketFuelingOperation getFuelingOperation() {
		return this.fuelingOperation;
	}

	public void setFuelingOperation(FuelTicketFuelingOperation fuelingOperation) {
		this.fuelingOperation = fuelingOperation;
	}

	public FlightTypeIndicator getIndicator() {
		return this.indicator;
	}

	public void setIndicator(FlightTypeIndicator indicator) {
		this.indicator = indicator;
	}

	public BigDecimal getNetUpliftQuantity() {
		return this.netUpliftQuantity;
	}

	public void setNetUpliftQuantity(BigDecimal netUpliftQuantity) {
		this.netUpliftQuantity = netUpliftQuantity;
	}

	public BigDecimal getBeforeFueling() {
		return this.beforeFueling;
	}

	public void setBeforeFueling(BigDecimal beforeFueling) {
		this.beforeFueling = beforeFueling;
	}

	public BigDecimal getAfterFueling() {
		return this.afterFueling;
	}

	public void setAfterFueling(BigDecimal afterFueling) {
		this.afterFueling = afterFueling;
	}

	public BigDecimal getDensity() {
		return this.density;
	}

	public void setDensity(BigDecimal density) {
		this.density = density;
	}

	public Date getFuelingStartDate() {
		return this.fuelingStartDate;
	}

	public void setFuelingStartDate(Date fuelingStartDate) {
		this.fuelingStartDate = fuelingStartDate;
	}

	public Date getFuelingEndDate() {
		return this.fuelingEndDate;
	}

	public void setFuelingEndDate(Date fuelingEndDate) {
		this.fuelingEndDate = fuelingEndDate;
	}

	public String getDeliveryType() {
		return this.deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public FuelTicketSource getSource() {
		return this.source;
	}

	public void setSource(FuelTicketSource source) {
		this.source = source;
	}

	public FuelingType getTransport() {
		return this.transport;
	}

	public void setTransport(FuelingType transport) {
		this.transport = transport;
	}

	public FuelTicketStatus getTicketStatus() {
		return this.ticketStatus;
	}

	public void setTicketStatus(FuelTicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public FuelTicketTransmissionStatus getTransmissionStatus() {
		return this.transmissionStatus;
	}

	public void setTransmissionStatus(
			FuelTicketTransmissionStatus transmissionStatus) {
		this.transmissionStatus = transmissionStatus;
	}

	public String getTransmissionDetails() {
		return this.transmissionDetails;
	}

	public void setTransmissionDetails(String transmissionDetails) {
		this.transmissionDetails = transmissionDetails;
	}

	public FuelTicketApprovalStatus getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(FuelTicketApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Date getTransmissionDate() {
		return this.transmissionDate;
	}

	public void setTransmissionDate(Date transmissionDate) {
		this.transmissionDate = transmissionDate;
	}

	public String getCapturedBy() {
		return this.capturedBy;
	}

	public void setCapturedBy(String capturedBy) {
		this.capturedBy = capturedBy;
	}

	public String getApprovedBy() {
		return this.approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public BillStatus getBillStatus() {
		return this.billStatus;
	}

	public void setBillStatus(BillStatus billStatus) {
		this.billStatus = billStatus;
	}

	public OutgoingInvoiceStatus getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(OutgoingInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public Integer getMeterStartIndex() {
		return this.meterStartIndex;
	}

	public void setMeterStartIndex(Integer meterStartIndex) {
		this.meterStartIndex = meterStartIndex;
	}

	public Integer getMeterEndIndex() {
		return this.meterEndIndex;
	}

	public void setMeterEndIndex(Integer meterEndIndex) {
		this.meterEndIndex = meterEndIndex;
	}

	public Integer getTemperature() {
		return this.temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public TemperatureUnit getTemperatureUnit() {
		return this.temperatureUnit;
	}

	public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
		this.temperatureUnit = temperatureUnit;
	}

	public Date getRefuelerArrivalTime() {
		return this.refuelerArrivalTime;
	}

	public void setRefuelerArrivalTime(Date refuelerArrivalTime) {
		this.refuelerArrivalTime = refuelerArrivalTime;
	}

	public PaymentType getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public String getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getCardExpiringDate() {
		return this.cardExpiringDate;
	}

	public void setCardExpiringDate(Date cardExpiringDate) {
		this.cardExpiringDate = cardExpiringDate;
	}

	public String getCardHolder() {
		return this.cardHolder;
	}

	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}

	public BigDecimal getAmountReceived() {
		return this.amountReceived;
	}

	public void setAmountReceived(BigDecimal amountReceived) {
		this.amountReceived = amountReceived;
	}

	public SendInvoiceBy getSendInvoiceBy() {
		return this.sendInvoiceBy;
	}

	public void setSendInvoiceBy(SendInvoiceBy sendInvoiceBy) {
		this.sendInvoiceBy = sendInvoiceBy;
	}

	public String getSendInvoiceTo() {
		return this.sendInvoiceTo;
	}

	public void setSendInvoiceTo(String sendInvoiceTo) {
		this.sendInvoiceTo = sendInvoiceTo;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public FlightServiceType getFlightServiceType() {
		return this.flightServiceType;
	}

	public void setFlightServiceType(FlightServiceType flightServiceType) {
		this.flightServiceType = flightServiceType;
	}

	public String getSubsidiaryCode() {
		return this.subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public RevocationReason getRevocationRason() {
		return this.revocationRason;
	}

	public void setRevocationRason(RevocationReason revocationRason) {
		this.revocationRason = revocationRason;
	}

	public String getToleranceMessage() {
		if (ProvidedQuantityInforamtionResult.resultMap.get() == null) {
			ProvidedQuantityInforamtionResult.resultMap
					.set(new HashMap<Integer, String>());
		}
		return ProvidedQuantityInforamtionResult.resultMap.get().get(
				this.getId());
	}

	public void setToleranceMessage(String toleranceMessage) {
		if (ProvidedQuantityInforamtionResult.resultMap.get() == null) {
			ProvidedQuantityInforamtionResult.resultMap
					.set(new HashMap<Integer, String>());
		}
		int key = 0;
		if (this.getId() != null) {
			key = this.getId();
		}
		ProvidedQuantityInforamtionResult.resultMap.get().put(key,
				toleranceMessage);
	}

	public String getCancelationResult() {
		return this.cancelationResult;
	}

	public void setCancelationResult(String cancelationResult) {
		this.cancelationResult = cancelationResult;
	}

	public Locations getDeparture() {
		return this.departure;
	}

	public void setDeparture(Locations departure) {
		if (departure != null) {
			this.__validate_client_context__(departure.getClientId());
		}
		this.departure = departure;
	}
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Unit getUpliftUnit() {
		return this.upliftUnit;
	}

	public void setUpliftUnit(Unit upliftUnit) {
		if (upliftUnit != null) {
			this.__validate_client_context__(upliftUnit.getClientId());
		}
		this.upliftUnit = upliftUnit;
	}
	public Suppliers getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Suppliers supplier) {
		if (supplier != null) {
			this.__validate_client_context__(supplier.getClientId());
		}
		this.supplier = supplier;
	}
	public Suppliers getFueller() {
		return this.fueller;
	}

	public void setFueller(Suppliers fueller) {
		if (fueller != null) {
			this.__validate_client_context__(fueller.getClientId());
		}
		this.fueller = fueller;
	}
	public Locations getDestination() {
		return this.destination;
	}

	public void setDestination(Locations destination) {
		if (destination != null) {
			this.__validate_client_context__(destination.getClientId());
		}
		this.destination = destination;
	}
	public Aircraft getRegistration() {
		return this.registration;
	}

	public void setRegistration(Aircraft registration) {
		if (registration != null) {
			this.__validate_client_context__(registration.getClientId());
		}
		this.registration = registration;
	}
	public Locations getFinalDest() {
		return this.finalDest;
	}

	public void setFinalDest(Locations finalDest) {
		if (finalDest != null) {
			this.__validate_client_context__(finalDest.getClientId());
		}
		this.finalDest = finalDest;
	}
	public Unit getNetUpliftUnit() {
		return this.netUpliftUnit;
	}

	public void setNetUpliftUnit(Unit netUpliftUnit) {
		if (netUpliftUnit != null) {
			this.__validate_client_context__(netUpliftUnit.getClientId());
		}
		this.netUpliftUnit = netUpliftUnit;
	}
	public Unit getBeforeFuelingUnit() {
		return this.beforeFuelingUnit;
	}

	public void setBeforeFuelingUnit(Unit beforeFuelingUnit) {
		if (beforeFuelingUnit != null) {
			this.__validate_client_context__(beforeFuelingUnit.getClientId());
		}
		this.beforeFuelingUnit = beforeFuelingUnit;
	}
	public Unit getAfterFuelingUnit() {
		return this.afterFuelingUnit;
	}

	public void setAfterFuelingUnit(Unit afterFuelingUnit) {
		if (afterFuelingUnit != null) {
			this.__validate_client_context__(afterFuelingUnit.getClientId());
		}
		this.afterFuelingUnit = afterFuelingUnit;
	}
	public Unit getDensityUnit() {
		return this.densityUnit;
	}

	public void setDensityUnit(Unit densityUnit) {
		if (densityUnit != null) {
			this.__validate_client_context__(densityUnit.getClientId());
		}
		this.densityUnit = densityUnit;
	}
	public Unit getDensityVolume() {
		return this.densityVolume;
	}

	public void setDensityVolume(Unit densityVolume) {
		if (densityVolume != null) {
			this.__validate_client_context__(densityVolume.getClientId());
		}
		this.densityVolume = densityVolume;
	}
	public AcTypes getAcType() {
		return this.acType;
	}

	public void setAcType(AcTypes acType) {
		if (acType != null) {
			this.__validate_client_context__(acType.getClientId());
		}
		this.acType = acType;
	}
	public Customer getReseller() {
		return this.reseller;
	}

	public void setReseller(Customer reseller) {
		if (reseller != null) {
			this.__validate_client_context__(reseller.getClientId());
		}
		this.reseller = reseller;
	}
	public Currencies getAmmountCurr() {
		return this.ammountCurr;
	}

	public void setAmmountCurr(Currencies ammountCurr) {
		if (ammountCurr != null) {
			this.__validate_client_context__(ammountCurr.getClientId());
		}
		this.ammountCurr = ammountCurr;
	}
	public FuelTicket getReferenceTicket() {
		return this.referenceTicket;
	}

	public void setReferenceTicket(FuelTicket referenceTicket) {
		if (referenceTicket != null) {
			this.__validate_client_context__(referenceTicket.getClientId());
		}
		this.referenceTicket = referenceTicket;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
