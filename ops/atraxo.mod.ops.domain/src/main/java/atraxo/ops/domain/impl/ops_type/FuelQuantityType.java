/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelQuantityType {

	_EMPTY_("", ""), _GROSS_("Gross", "GR"), _NET_("Net", "NT");

	private String name;
	private String code;

	private FuelQuantityType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelQuantityType getByName(String name) {
		for (FuelQuantityType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelQuantityType with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static FuelQuantityType getByCode(String code) {
		for (FuelQuantityType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelQuantityType with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
