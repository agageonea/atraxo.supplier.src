package atraxo.ops.domain.ext.fuelTicket;

public enum ApprovalStatus {
	FOR_APPROVAL("For approval"), OK("OK"), RECHECK("Recheck"), REJECTED("Rejected");

	private String name;

	private ApprovalStatus(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public static ApprovalStatus getByName(String name) {
		for (ApprovalStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new RuntimeException("Inexistent approval status with name: " + name);
	}
}
