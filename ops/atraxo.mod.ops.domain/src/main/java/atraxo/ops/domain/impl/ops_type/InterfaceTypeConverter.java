/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InterfaceType} enum.
 * Generated code. Do not modify in this file.
 */
public class InterfaceTypeConverter
		implements
			AttributeConverter<InterfaceType, String> {

	@Override
	public String convertToDatabaseColumn(InterfaceType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null InterfaceType.");
		}
		return value.getName();
	}

	@Override
	public InterfaceType convertToEntityAttribute(String value) {
		return InterfaceType.getByName(value);
	}

}
