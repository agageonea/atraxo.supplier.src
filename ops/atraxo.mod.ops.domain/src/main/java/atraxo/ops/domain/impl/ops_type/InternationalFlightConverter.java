/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InternationalFlight} enum.
 * Generated code. Do not modify in this file.
 */
public class InternationalFlightConverter
		implements
			AttributeConverter<InternationalFlight, String> {

	@Override
	public String convertToDatabaseColumn(InternationalFlight value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null InternationalFlight.");
		}
		return value.getName();
	}

	@Override
	public InternationalFlight convertToEntityAttribute(String value) {
		return InternationalFlight.getByName(value);
	}

}
