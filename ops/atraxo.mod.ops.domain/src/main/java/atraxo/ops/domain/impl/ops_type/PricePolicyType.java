/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum PricePolicyType {

	_EMPTY_(""), _MARGIN_("Margin"), _MARKUP_("Markup");

	private String name;

	private PricePolicyType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static PricePolicyType getByName(String name) {
		for (PricePolicyType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent PricePolicyType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
