/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link IPTransactionCode} enum.
 * Generated code. Do not modify in this file.
 */
public class IPTransactionCodeConverter
		implements
			AttributeConverter<IPTransactionCode, String> {

	@Override
	public String convertToDatabaseColumn(IPTransactionCode value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null IPTransactionCode.");
		}
		return value.getName();
	}

	@Override
	public IPTransactionCode convertToEntityAttribute(String value) {
		return IPTransactionCode.getByName(value);
	}

}
