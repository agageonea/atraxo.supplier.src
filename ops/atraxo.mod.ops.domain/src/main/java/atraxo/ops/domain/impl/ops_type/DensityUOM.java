/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum DensityUOM {

	_EMPTY_(""), _KGM_("KGM"), _KGL_("KGL"), _LGH_("LGH");

	private String name;

	private DensityUOM(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static DensityUOM getByName(String name) {
		for (DensityUOM status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent DensityUOM with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
