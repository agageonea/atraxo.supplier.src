/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Suffix {

	_EMPTY_("", ""), _D_DIVERTED_("D-Diverted", "D"), _F_FERRY_("F-Ferry", "F"), _G_GROUND_RETURN_(
			"G-Ground return", "G");

	private String name;
	private String code;

	private Suffix(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static Suffix getByName(String name) {
		for (Suffix status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Suffix with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static Suffix getByCode(String code) {
		for (Suffix status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Suffix with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
