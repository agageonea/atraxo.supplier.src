/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TemperatureUnit} enum.
 * Generated code. Do not modify in this file.
 */
public class TemperatureUnitConverter
		implements
			AttributeConverter<TemperatureUnit, String> {

	@Override
	public String convertToDatabaseColumn(TemperatureUnit value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TemperatureUnit.");
		}
		return value.getName();
	}

	@Override
	public TemperatureUnit convertToEntityAttribute(String value) {
		return TemperatureUnit.getByName(value);
	}

}
