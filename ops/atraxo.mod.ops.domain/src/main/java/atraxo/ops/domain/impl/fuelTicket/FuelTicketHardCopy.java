/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelTicket;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelTicketHardCopy} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelTicketHardCopy.NQ_FIND_BY_BK, query = "SELECT e FROM FuelTicketHardCopy e WHERE e.clientId = :clientId and e.fuelTicket = :fuelTicket", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelTicketHardCopy.NQ_FIND_BY_BK_PRIMITIVE, query = "SELECT e FROM FuelTicketHardCopy e WHERE e.clientId = :clientId and e.fuelTicket.id = :fuelTicketId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelTicketHardCopy.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelTicketHardCopy e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelTicketHardCopy.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelTicketHardCopy.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "fuel_ticket_id"})})
public class FuelTicketHardCopy extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_TICKET_HARD_COPY";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Bk.
	 */
	public static final String NQ_FIND_BY_BK = "FuelTicketHardCopy.findByBk";
	/**
	 * Named query find by unique key: Bk using the ID field for references.
	 */
	public static final String NQ_FIND_BY_BK_PRIMITIVE = "FuelTicketHardCopy.findByBk_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelTicketHardCopy.findByBusiness";

	@NotNull
	@Lob
	@Column(name = "file", nullable = false)
	private byte[] file;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelTicket.class)
	@JoinColumn(name = "fuel_ticket_id", referencedColumnName = "id")
	private FuelTicket fuelTicket;

	public byte[] getFile() {
		return this.file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public FuelTicket getFuelTicket() {
		return this.fuelTicket;
	}

	public void setFuelTicket(FuelTicket fuelTicket) {
		if (fuelTicket != null) {
			this.__validate_client_context__(fuelTicket.getClientId());
		}
		this.fuelTicket = fuelTicket;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
