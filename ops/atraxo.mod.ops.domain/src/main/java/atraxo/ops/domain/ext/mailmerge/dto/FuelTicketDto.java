package atraxo.ops.domain.ext.mailmerge.dto;

import java.math.BigDecimal;
import java.util.Date;

import atraxo.fmbas.domain.ext.mailmerge.dto.AbstractDto;

/**
 * @author abolindu
 */
public class FuelTicketDto extends AbstractDto {

	/**
	 *
	 */
	private static final long serialVersionUID = -5916975978503271840L;

	private String title;
	private String fullName;

	private String fullnameOfRequester;
	private String ticketNumber;
	private String departure;
	private Date deliveryDate;
	private String customerName;
	private String supplierCode;
	private BigDecimal quantity;
	private String unitOfMeasure;
	private String approveNote;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullnameOfRequester() {
		return this.fullnameOfRequester;
	}

	public void setFullnameOfRequester(String fullnameOfRequester) {
		this.fullnameOfRequester = fullnameOfRequester;
	}

	public String getTicketNumber() {
		return this.ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getDeparture() {
		return this.departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSupplierCode() {
		return this.supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getUnitOfMeasure() {
		return this.unitOfMeasure;
	}

	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	public String getApproveNote() {
		return this.approveNote;
	}

	public void setApproveNote(String approveNote) {
		this.approveNote = approveNote;
	}

	@Override
	public String toString() {
		return "FuelTicketDto [title=" + this.title + ", fullName=" + this.fullName + ", fullnameOfRequester=" + this.fullnameOfRequester;
	}

}
