package atraxo.ops.domain.ext.fuelTicket;

public class FuelTicketInvoice {

	private String ticketNumber;
	private String invoiceNumber;

	public FuelTicketInvoice(String ticketNumber, String invoiceNumber) {
		super();
		this.ticketNumber = ticketNumber;
		this.invoiceNumber = invoiceNumber;
	}

	public String getTicketNumber() {
		return this.ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoicenumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

}
