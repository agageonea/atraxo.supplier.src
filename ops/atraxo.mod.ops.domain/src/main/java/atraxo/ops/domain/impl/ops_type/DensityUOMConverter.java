/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link DensityUOM} enum.
 * Generated code. Do not modify in this file.
 */
public class DensityUOMConverter
		implements
			AttributeConverter<DensityUOM, String> {

	@Override
	public String convertToDatabaseColumn(DensityUOM value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null DensityUOM.");
		}
		return value.getName();
	}

	@Override
	public DensityUOM convertToEntityAttribute(String value) {
		return DensityUOM.getByName(value);
	}

}
