/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum DeliveryType {

	_EMPTY_("", ""), _INTO_PLANE_("Into Plane", "IP"), _BULK_MOVEMENT_(
			"Bulk Movement", "BM");

	private String name;
	private String code;

	private DeliveryType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static DeliveryType getByName(String name) {
		for (DeliveryType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent DeliveryType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static DeliveryType getByCode(String code) {
		for (DeliveryType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent DeliveryType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
