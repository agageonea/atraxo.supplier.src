/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TicketSource} enum.
 * Generated code. Do not modify in this file.
 */
public class TicketSourceConverter
		implements
			AttributeConverter<TicketSource, String> {

	@Override
	public String convertToDatabaseColumn(TicketSource value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TicketSource.");
		}
		return value.getName();
	}

	@Override
	public TicketSource convertToEntityAttribute(String value) {
		return TicketSource.getByName(value);
	}

}
