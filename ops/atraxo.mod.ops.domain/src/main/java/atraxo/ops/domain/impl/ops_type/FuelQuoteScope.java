/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelQuoteScope {

	_EMPTY_(""), _INTO_PLANE_("Into plane"), _EX_HYDRANT_("Ex-hydrant"), _INTO_STORAGE_(
			"Into storage");

	private String name;

	private FuelQuoteScope(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelQuoteScope getByName(String name) {
		for (FuelQuoteScope status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FuelQuoteScope with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
