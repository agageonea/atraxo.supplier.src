/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelTicketStatus {

	_EMPTY_("", ""), _ORIGINAL_("Original", "C"), _UPDATED_("Updated", "U"), _CANCELED_(
			"Canceled", "D"), _REISSUE_("Reissue", "R");

	private String name;
	private String code;

	private FuelTicketStatus(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelTicketStatus getByName(String name) {
		for (FuelTicketStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelTicketStatus with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static FuelTicketStatus getByCode(String code) {
		for (FuelTicketStatus status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelTicketStatus with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
