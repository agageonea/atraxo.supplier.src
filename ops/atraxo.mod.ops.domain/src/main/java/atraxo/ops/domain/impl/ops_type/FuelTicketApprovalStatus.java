/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelTicketApprovalStatus {

	_EMPTY_(""), _FOR_APPROVAL_("For approval"), _AWAITING_APPROVAL_(
			"Awaiting approval"), _OK_("OK"), _REJECTED_("Rejected"), _RECHECK_(
			"Recheck");

	private String name;

	private FuelTicketApprovalStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelTicketApprovalStatus getByName(String name) {
		for (FuelTicketApprovalStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelTicketApprovalStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
