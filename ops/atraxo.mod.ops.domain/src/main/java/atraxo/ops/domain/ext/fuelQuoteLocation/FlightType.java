package atraxo.ops.domain.ext.fuelQuoteLocation;

public enum FlightType {
	DOMESTIC("Domestic"), INTERNATIONAL("International"), UNSPECIFIED("Unspecified");

	private String displayName;

	private FlightType(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public FlightType getByDisplayName(String displayName) {
		for (FlightType flightType : values()) {
			if (flightType.getDisplayName().equalsIgnoreCase(displayName)) {
				return flightType;
			}
		}
		throw new RuntimeException("Inexistent flight type with name: " + displayName);
	}

}
