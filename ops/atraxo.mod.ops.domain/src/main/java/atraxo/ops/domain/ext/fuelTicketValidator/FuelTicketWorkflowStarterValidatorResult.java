package atraxo.ops.domain.ext.fuelTicketValidator;

public class FuelTicketWorkflowStarterValidatorResult {

	private boolean valid;
	private String validationMessage;

	public boolean isValid() {
		return this.valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getValidationMessage() {
		return this.validationMessage;
	}

	public void setValidationMessage(String validationMessage) {
		this.validationMessage = validationMessage;
	}

}
