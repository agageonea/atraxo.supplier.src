/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelQuantityType} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelQuantityTypeConverter
		implements
			AttributeConverter<FuelQuantityType, String> {

	@Override
	public String convertToDatabaseColumn(FuelQuantityType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelQuantityType.");
		}
		return value.getName();
	}

	@Override
	public FuelQuantityType convertToEntityAttribute(String value) {
		return FuelQuantityType.getByName(value);
	}

}
