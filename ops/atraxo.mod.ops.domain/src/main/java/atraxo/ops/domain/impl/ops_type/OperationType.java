/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum OperationType {

	_EMPTY_(""), _COMMERCIAL_("Commercial"), _BUSINESS_("Business"), _PRIVATE_(
			"Private");

	private String name;

	private OperationType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static OperationType getByName(String name) {
		for (OperationType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent OperationType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
