/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TemperatureUnit {

	_EMPTY_(""), _C_("C"), _F_("F");

	private String name;

	private TemperatureUnit(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TemperatureUnit getByName(String name) {
		for (TemperatureUnit status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TemperatureUnit with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
