/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum OutgoingInvoiceStatus {

	_EMPTY_(""), _NOT_INVOICED_("Not invoiced"), _AWAITING_APPROVAL_(
			"Awaiting approval"), _AWAITING_PAYMENT_("Awaiting payment"), _PAID_(
			"Paid"), _CREDITED_("Credited");

	private String name;

	private OutgoingInvoiceStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static OutgoingInvoiceStatus getByName(String name) {
		for (OutgoingInvoiceStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent OutgoingInvoiceStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
