package atraxo.ops.domain.ext.fuelQuoteLocation;

import java.util.Map;

import atraxo.fmbas.domain.ext.json.message.SoneMessage;

public class CapturedPriceInformationResult {

	public static final ThreadLocal<Map<Integer, SoneMessage>> resultMap = new ThreadLocal<>();

	private CapturedPriceInformationResult() {
		// private constructor
	}
}
