/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelTicketCustomsStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelTicketCustomsStatusConverter
		implements
			AttributeConverter<FuelTicketCustomsStatus, String> {

	@Override
	public String convertToDatabaseColumn(FuelTicketCustomsStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null FuelTicketCustomsStatus.");
		}
		return value.getName();
	}

	@Override
	public FuelTicketCustomsStatus convertToEntityAttribute(String value) {
		return FuelTicketCustomsStatus.getByName(value);
	}

}
