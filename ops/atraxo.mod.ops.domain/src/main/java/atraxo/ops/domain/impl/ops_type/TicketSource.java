/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TicketSource {

	_EMPTY_("", ""), _MANUAL_("Manual", "M"), _ELECTRONIC_("Electronic", "E");

	private String name;
	private String code;

	private TicketSource(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TicketSource getByName(String name) {
		for (TicketSource status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TicketSource with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static TicketSource getByCode(String code) {
		for (TicketSource status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TicketSource with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
