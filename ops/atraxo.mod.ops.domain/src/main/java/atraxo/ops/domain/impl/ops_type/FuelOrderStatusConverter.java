/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelOrderStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelOrderStatusConverter
		implements
			AttributeConverter<FuelOrderStatus, String> {

	@Override
	public String convertToDatabaseColumn(FuelOrderStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelOrderStatus.");
		}
		return value.getName();
	}

	@Override
	public FuelOrderStatus convertToEntityAttribute(String value) {
		return FuelOrderStatus.getByName(value);
	}

}
