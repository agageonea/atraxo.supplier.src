/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelRequestStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelRequestStatusConverter
		implements
			AttributeConverter<FuelRequestStatus, String> {

	@Override
	public String convertToDatabaseColumn(FuelRequestStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelRequestStatus.");
		}
		return value.getName();
	}

	@Override
	public FuelRequestStatus convertToEntityAttribute(String value) {
		return FuelRequestStatus.getByName(value);
	}

}
