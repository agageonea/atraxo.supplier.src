/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelTransaction;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSourceParameters;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelTransactionSource} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelTransactionSource.NQ_FIND_BY_NAME, query = "SELECT e FROM FuelTransactionSource e WHERE e.clientId = :clientId and e.name = :name", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelTransactionSource.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelTransactionSource e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelTransactionSource.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelTransactionSource.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "name"})})
public class FuelTransactionSource extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_TRANSACTION_SOURCE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Name.
	 */
	public static final String NQ_FIND_BY_NAME = "FuelTransactionSource.findByName";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelTransactionSource.findByBusiness";

	@Column(name = "name", length = 255)
	private String name;

	@Temporal(TemporalType.DATE)
	@Column(name = "processing_date")
	private Date processingDate;

	@Column(name = "fuel_transactions", precision = 4)
	private Integer fuelTransactions;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FuelTransactionSourceParameters.class, mappedBy = "fuelTransactionSource", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<FuelTransactionSourceParameters> parameters;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FuelTransaction.class, mappedBy = "importSource", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private Collection<FuelTransaction> fueltransaction;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getProcessingDate() {
		return this.processingDate;
	}

	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}

	public Integer getFuelTransactions() {
		return this.fuelTransactions;
	}

	public void setFuelTransactions(Integer fuelTransactions) {
		this.fuelTransactions = fuelTransactions;
	}

	public Collection<FuelTransactionSourceParameters> getParameters() {
		return this.parameters;
	}

	public void setParameters(
			Collection<FuelTransactionSourceParameters> parameters) {
		this.parameters = parameters;
	}

	/**
	 * @param e
	 */
	public void addToParameters(FuelTransactionSourceParameters e) {
		if (this.parameters == null) {
			this.parameters = new ArrayList<>();
		}
		e.setFuelTransactionSource(this);
		this.parameters.add(e);
	}
	public Collection<FuelTransaction> getFueltransaction() {
		return this.fueltransaction;
	}

	public void setFueltransaction(Collection<FuelTransaction> fueltransaction) {
		this.fueltransaction = fueltransaction;
	}

	/**
	 * @param e
	 */
	public void addToFueltransaction(FuelTransaction e) {
		if (this.fueltransaction == null) {
			this.fueltransaction = new ArrayList<>();
		}
		e.setImportSource(this);
		this.fueltransaction.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		FuelTransactionSource other = (FuelTransactionSource) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!this.name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
