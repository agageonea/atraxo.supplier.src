/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelTicketCustomsStatus {

	_EMPTY_("", ""), _BONDED_("Bonded", "Bonded"), _DOMESTIC_("Domestic",
			"Domestic"), _FTZ_("FTZ", "FTZ"), _UNSPECIFIED_("Unspecified",
			"Uns"), _DUTY_PAID_("Duty Paid", "DP"), _DUTY_FREE_("Duty Free",
			"DF"), _NOT_APPLICABLE_("Not Applicable", "NA");

	private String name;
	private String code;

	private FuelTicketCustomsStatus(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelTicketCustomsStatus getByName(String name) {
		for (FuelTicketCustomsStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelTicketCustomsStatus with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static FuelTicketCustomsStatus getByCode(String code) {
		for (FuelTicketCustomsStatus status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelTicketCustomsStatus with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
