/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelQuoteCycle} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelQuoteCycleConverter
		implements
			AttributeConverter<FuelQuoteCycle, String> {

	@Override
	public String convertToDatabaseColumn(FuelQuoteCycle value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelQuoteCycle.");
		}
		return value.getName();
	}

	@Override
	public FuelQuoteCycle convertToEntityAttribute(String value) {
		return FuelQuoteCycle.getByName(value);
	}

}
