package atraxo.ops.domain.ext.fuelTicket;

/**
 *
 * @author zspeter
 *
 */
public class FetchResult {
	private final int updated;
	private final int inserted;

	public FetchResult(int updated, int inserted) {
		super();
		this.updated = updated;
		this.inserted = inserted;
	}

	public int getUpdated() {
		return this.updated;
	}

	public int getInserted() {
		return this.inserted;
	}

}
