package atraxo.ops.domain.ext.DeliveryNoteMethod;

public enum DeliveryNoteMethod {

	GENERATE, UPDATE, DELETE;

}
