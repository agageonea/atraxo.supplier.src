/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link ValidationState} enum.
 * Generated code. Do not modify in this file.
 */
public class ValidationStateConverter
		implements
			AttributeConverter<ValidationState, String> {

	@Override
	public String convertToDatabaseColumn(ValidationState value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null ValidationState.");
		}
		return value.getName();
	}

	@Override
	public ValidationState convertToEntityAttribute(String value) {
		return ValidationState.getByName(value);
	}

}
