/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.flightEvent;

import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.BillStatusConverter;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicatorConverter;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.ProductConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.flightEvent.ProvidedQuantityInforamtionResult;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FlightEventStatus;
import atraxo.ops.domain.impl.ops_type.FlightEventStatusConverter;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.OperationTypeConverter;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatusConverter;
import atraxo.ops.domain.impl.ops_type.Suffix;
import atraxo.ops.domain.impl.ops_type.SuffixConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link FlightEvent} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = FlightEvent.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FlightEvent e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FlightEvent.TABLE_NAME)
public class FlightEvent extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "OPS_FLIGHT_EVENTS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FlightEvent.findByBusiness";

	@Column(name = "flight_number", length = 4)
	private String flighNo;

	@Column(name = "registration", length = 10)
	private String registration;

	@Column(name = "quantity", precision = 19, scale = 6)
	private BigDecimal quantity;

	@Column(name = "product", length = 32)
	@Convert(converter = ProductConverter.class)
	private Product product;

	@Column(name = "per_captain")
	private Boolean perCaptain;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "uplift_date")
	private Date upliftDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "arrival_date")
	private Date arrivalDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "departer_date")
	private Date departerDate;

	@Column(name = "captain", length = 64)
	private String captain;

	@Column(name = "event_type", length = 16)
	@Convert(converter = FlightTypeIndicatorConverter.class)
	private FlightTypeIndicator eventType;

	@Column(name = "operation_type", length = 32)
	@Convert(converter = OperationTypeConverter.class)
	private OperationType operationType;

	@Column(name = "aircraft_call_sign", length = 32)
	private String aircraftCallSign;

	@Column(name = "display_utc")
	private Boolean timeReference;

	@NotBlank
	@Column(name = "status", nullable = false, length = 16)
	@Convert(converter = FlightEventStatusConverter.class)
	private FlightEventStatus status;

	@Column(name = "events", precision = 11)
	private Integer events;

	@Column(name = "total_quantity", precision = 19, scale = 6)
	private BigDecimal totalQuantity;

	@Column(name = "suffix", length = 16)
	@Convert(converter = SuffixConverter.class)
	private Suffix suffix;

	@Column(name = "bill_status", length = 32)
	@Convert(converter = BillStatusConverter.class)
	private BillStatus billStatus;

	@Column(name = "invoice_status", length = 32)
	@Convert(converter = OutgoingInvoiceStatusConverter.class)
	private OutgoingInvoiceStatus invoiceStatus;

	@Transient
	private String toleranceMessage;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelQuoteLocation.class)
	@JoinColumn(name = "location_quote_id", referencedColumnName = "id")
	private FuelQuoteLocation locQuote;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelRequest.class)
	@JoinColumn(name = "fuel_request_id", referencedColumnName = "id")
	private FuelRequest fuelRequest;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "departure_id", referencedColumnName = "id")
	private Locations departure;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "destination_id", referencedColumnName = "id")
	private Locations destination;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AcTypes.class)
	@JoinColumn(name = "aircraft_type_id", referencedColumnName = "id")
	private AcTypes aircraftType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "arrival_from_id", referencedColumnName = "id")
	private Locations arrivalFrom;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "operator_id", referencedColumnName = "id")
	private Customer operator;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "handler_id", referencedColumnName = "id")
	private Suppliers handler;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "aircraft_home_base_id", referencedColumnName = "id")
	private Locations aircraftHomeBase;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelOrderLocation.class)
	@JoinColumn(name = "location_purchase_order_id", referencedColumnName = "id")
	private FuelOrderLocation locOrder;

	public String getFlighNo() {
		return this.flighNo;
	}

	public void setFlighNo(String flighNo) {
		this.flighNo = flighNo;
	}

	public String getRegistration() {
		return this.registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Boolean getPerCaptain() {
		return this.perCaptain;
	}

	public void setPerCaptain(Boolean perCaptain) {
		this.perCaptain = perCaptain;
	}

	public Date getUpliftDate() {
		return this.upliftDate;
	}

	public void setUpliftDate(Date upliftDate) {
		this.upliftDate = upliftDate;
	}

	public Date getArrivalDate() {
		return this.arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Date getDeparterDate() {
		return this.departerDate;
	}

	public void setDeparterDate(Date departerDate) {
		this.departerDate = departerDate;
	}

	public String getCaptain() {
		return this.captain;
	}

	public void setCaptain(String captain) {
		this.captain = captain;
	}

	public FlightTypeIndicator getEventType() {
		return this.eventType;
	}

	public void setEventType(FlightTypeIndicator eventType) {
		this.eventType = eventType;
	}

	public OperationType getOperationType() {
		return this.operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public String getAircraftCallSign() {
		return this.aircraftCallSign;
	}

	public void setAircraftCallSign(String aircraftCallSign) {
		this.aircraftCallSign = aircraftCallSign;
	}

	public Boolean getTimeReference() {
		return this.timeReference;
	}

	public void setTimeReference(Boolean timeReference) {
		this.timeReference = timeReference;
	}

	public FlightEventStatus getStatus() {
		return this.status;
	}

	public void setStatus(FlightEventStatus status) {
		this.status = status;
	}

	public Integer getEvents() {
		return this.events;
	}

	public void setEvents(Integer events) {
		this.events = events;
	}

	public BigDecimal getTotalQuantity() {
		return this.totalQuantity;
	}

	public void setTotalQuantity(BigDecimal totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Suffix getSuffix() {
		return this.suffix;
	}

	public void setSuffix(Suffix suffix) {
		this.suffix = suffix;
	}

	public BillStatus getBillStatus() {
		return this.billStatus;
	}

	public void setBillStatus(BillStatus billStatus) {
		this.billStatus = billStatus;
	}

	public OutgoingInvoiceStatus getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(OutgoingInvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getToleranceMessage() {
		return ProvidedQuantityInforamtionResult.resultMap.get().get(
				this.getId());
	}

	public void setToleranceMessage(String toleranceMessage) {
		if (ProvidedQuantityInforamtionResult.resultMap.get() == null) {
			ProvidedQuantityInforamtionResult.resultMap
					.set(new HashMap<Integer, String>());
		}
		int key = 0;
		if (this.getId() != null) {
			key = this.getId();
		}
		ProvidedQuantityInforamtionResult.resultMap.get().put(key,
				toleranceMessage);
	}

	public FuelQuoteLocation getLocQuote() {
		return this.locQuote;
	}

	public void setLocQuote(FuelQuoteLocation locQuote) {
		if (locQuote != null) {
			this.__validate_client_context__(locQuote.getClientId());
		}
		this.locQuote = locQuote;
	}
	public FuelRequest getFuelRequest() {
		return this.fuelRequest;
	}

	public void setFuelRequest(FuelRequest fuelRequest) {
		if (fuelRequest != null) {
			this.__validate_client_context__(fuelRequest.getClientId());
		}
		this.fuelRequest = fuelRequest;
	}
	public Locations getDeparture() {
		return this.departure;
	}

	public void setDeparture(Locations departure) {
		if (departure != null) {
			this.__validate_client_context__(departure.getClientId());
		}
		this.departure = departure;
	}
	public Locations getDestination() {
		return this.destination;
	}

	public void setDestination(Locations destination) {
		if (destination != null) {
			this.__validate_client_context__(destination.getClientId());
		}
		this.destination = destination;
	}
	public AcTypes getAircraftType() {
		return this.aircraftType;
	}

	public void setAircraftType(AcTypes aircraftType) {
		if (aircraftType != null) {
			this.__validate_client_context__(aircraftType.getClientId());
		}
		this.aircraftType = aircraftType;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public Locations getArrivalFrom() {
		return this.arrivalFrom;
	}

	public void setArrivalFrom(Locations arrivalFrom) {
		if (arrivalFrom != null) {
			this.__validate_client_context__(arrivalFrom.getClientId());
		}
		this.arrivalFrom = arrivalFrom;
	}
	public Customer getOperator() {
		return this.operator;
	}

	public void setOperator(Customer operator) {
		if (operator != null) {
			this.__validate_client_context__(operator.getClientId());
		}
		this.operator = operator;
	}
	public Suppliers getHandler() {
		return this.handler;
	}

	public void setHandler(Suppliers handler) {
		if (handler != null) {
			this.__validate_client_context__(handler.getClientId());
		}
		this.handler = handler;
	}
	public Locations getAircraftHomeBase() {
		return this.aircraftHomeBase;
	}

	public void setAircraftHomeBase(Locations aircraftHomeBase) {
		if (aircraftHomeBase != null) {
			this.__validate_client_context__(aircraftHomeBase.getClientId());
		}
		this.aircraftHomeBase = aircraftHomeBase;
	}
	public FuelOrderLocation getLocOrder() {
		return this.locOrder;
	}

	public void setLocOrder(FuelOrderLocation locOrder) {
		if (locOrder != null) {
			this.__validate_client_context__(locOrder.getClientId());
		}
		this.locOrder = locOrder;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
