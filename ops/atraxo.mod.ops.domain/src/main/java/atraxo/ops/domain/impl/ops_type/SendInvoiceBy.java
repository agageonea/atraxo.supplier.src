/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum SendInvoiceBy {

	_EMPTY_(""), _E_MAIL_("E-mail"), _FAX_("Fax");

	private String name;

	private SendInvoiceBy(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static SendInvoiceBy getByName(String name) {
		for (SendInvoiceBy status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent SendInvoiceBy with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
