/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link InvoiceStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class InvoiceStatusConverter
		implements
			AttributeConverter<InvoiceStatus, String> {

	@Override
	public String convertToDatabaseColumn(InvoiceStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null InvoiceStatus.");
		}
		return value.getName();
	}

	@Override
	public InvoiceStatus convertToEntityAttribute(String value) {
		return InvoiceStatus.getByName(value);
	}

}
