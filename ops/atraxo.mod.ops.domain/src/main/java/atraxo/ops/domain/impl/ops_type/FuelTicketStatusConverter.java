/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelTicketStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelTicketStatusConverter
		implements
			AttributeConverter<FuelTicketStatus, String> {

	@Override
	public String convertToDatabaseColumn(FuelTicketStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelTicketStatus.");
		}
		return value.getName();
	}

	@Override
	public FuelTicketStatus convertToEntityAttribute(String value) {
		return FuelTicketStatus.getByName(value);
	}

}
