/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelTicketSource {

	_EMPTY_(""), _MANUAL_("Manual"), _IMPORT_("Import"), _SCAN_("Scan");

	private String name;

	private FuelTicketSource(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelTicketSource getByName(String name) {
		for (FuelTicketSource status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelTicketSource with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
