/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelQuoteLocation;

import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.ContractSubTypeConverter;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicatorConverter;
import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.cmm.domain.impl.cmm_type.PricingMethodConverter;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.cmm.domain.impl.cmm_type.ProductConverter;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.ext.json.message.SoneMessage;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreq;
import atraxo.fmbas.domain.impl.fmbas_type.InvoiceFreqConverter;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriodConverter;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDay;
import atraxo.fmbas.domain.impl.fmbas_type.PaymentDayConverter;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.fuelQuoteLocation.CapturedPriceInformationResult;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.ops_type.OperationType;
import atraxo.ops.domain.impl.ops_type.OperationTypeConverter;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelQuoteLocation} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = FuelQuoteLocation.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelQuoteLocation e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelQuoteLocation.TABLE_NAME)
public class FuelQuoteLocation extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_PRICE_LOCATION_QUOTE";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelQuoteLocation.findByBusiness";

	@Column(name = "price_basis", length = 32)
	@Convert(converter = PricingMethodConverter.class)
	private PricingMethod priceBasis;

	@Column(name = "flight_type", length = 16)
	@Convert(converter = FlightTypeIndicatorConverter.class)
	private FlightTypeIndicator eventType;

	@Column(name = "operational_type", length = 32)
	@Convert(converter = OperationTypeConverter.class)
	private OperationType operationalType;

	@Column(name = "product", length = 32)
	@Convert(converter = ProductConverter.class)
	private Product product;

	@Column(name = "quote_version", length = 32)
	private String quoteVersion;

	@Column(name = "delivery_method", length = 32)
	@Convert(converter = ContractSubTypeConverter.class)
	private ContractSubType deliveryMethod;

	@Column(name = "quantity", precision = 19, scale = 6)
	private BigDecimal quantity;

	@Column(name = "events", precision = 11)
	private Integer events;

	@Column(name = "index_offset", length = 32)
	private String indexOffset;

	@Column(name = "base_price", precision = 19, scale = 6)
	private BigDecimal basePrice;

	@Column(name = "differential", precision = 19, scale = 6)
	private BigDecimal differential;

	@Column(name = "margin", precision = 19, scale = 6)
	private BigDecimal margin;

	@Column(name = "fuel_price", precision = 19, scale = 6)
	private BigDecimal fuelPrice;

	@Column(name = "intoplane_fee", precision = 19, scale = 6)
	private BigDecimal intoplaneFee;

	@Column(name = "other_fees", precision = 19, scale = 6)
	private BigDecimal otherFees;

	@Column(name = "taxes", precision = 19, scale = 6)
	private BigDecimal taxes;

	@Column(name = "per_flight_fee", precision = 19, scale = 6)
	private BigDecimal flightFee;

	@Column(name = "total_price_per_uom", precision = 19, scale = 6)
	private BigDecimal totalPricePerUom;

	@Column(name = "payment_terms", precision = 4)
	private Integer paymentTerms;

	@Column(name = "payment_reference_date", length = 32)
	@Convert(converter = PaymentDayConverter.class)
	private PaymentDay paymentRefDate;

	@Column(name = "invoice_frequency", length = 32)
	@Convert(converter = InvoiceFreqConverter.class)
	private InvoiceFreq invoiceFreq;

	@Column(name = "exchange_rate_offset", length = 32)
	@Convert(converter = MasterAgreementsPeriodConverter.class)
	private MasterAgreementsPeriod period;

	@Temporal(TemporalType.DATE)
	@Column(name = "fuel_release_starts_on")
	private Date fuelReleaseStartsOn;

	@Temporal(TemporalType.DATE)
	@Column(name = "fuel_release_ends_on")
	private Date fuelReleaseEndsOn;

	@Column(name = "settlement_crncy_code", length = 3)
	private String settlementCurrencyCode;

	@Column(name = "settlement_unit_code", length = 2)
	private String settlementUnitCode;

	@Column(name = "contr_index_offset", length = 32)
	private String contractIndexOffset;

	@Column(name = "contr_fuel_price", precision = 19, scale = 6)
	private BigDecimal contrFuelPrice;

	@Transient
	private String forex;

	@Column(name = "dft", precision = 19, scale = 6)
	private BigDecimal dft;

	@Column(name = "disclaimer", length = 1000)
	private String disclaimer;

	@Transient
	private SoneMessage toleranceMessage;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelQuotation.class)
	@JoinColumn(name = "quote_id", referencedColumnName = "id")
	private FuelQuotation quote;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Locations location;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "supplier_id", referencedColumnName = "id")
	private Suppliers supplier;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contract.class)
	@JoinColumn(name = "contract_id", referencedColumnName = "id")
	private Contract contract;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Suppliers.class)
	@JoinColumn(name = "intoplane_agent_id", referencedColumnName = "id")
	private Suppliers intoPlaneAgent;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Quotation.class)
	@JoinColumn(name = "quotation_id", referencedColumnName = "id")
	private Quotation quotation;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currencies currency;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AverageMethod.class)
	@JoinColumn(name = "average_method_id", referencedColumnName = "id")
	private AverageMethod averageMethod;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FinancialSources.class)
	@JoinColumn(name = "financial_source_id", referencedColumnName = "id")
	private FinancialSources financialSource;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = PricePolicy.class)
	@JoinColumn(name = "prices_policy_id", referencedColumnName = "id")
	private PricePolicy pricingPolicy;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FlightEvent.class, mappedBy = "locQuote")
	private Collection<FlightEvent> flightEvents;

	public PricingMethod getPriceBasis() {
		return this.priceBasis;
	}

	public void setPriceBasis(PricingMethod priceBasis) {
		this.priceBasis = priceBasis;
	}

	public FlightTypeIndicator getEventType() {
		return this.eventType;
	}

	public void setEventType(FlightTypeIndicator eventType) {
		this.eventType = eventType;
	}

	public OperationType getOperationalType() {
		return this.operationalType;
	}

	public void setOperationalType(OperationType operationalType) {
		this.operationalType = operationalType;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getQuoteVersion() {
		return this.quoteVersion;
	}

	public void setQuoteVersion(String quoteVersion) {
		this.quoteVersion = quoteVersion;
	}

	public ContractSubType getDeliveryMethod() {
		return this.deliveryMethod;
	}

	public void setDeliveryMethod(ContractSubType deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Integer getEvents() {
		return this.events;
	}

	public void setEvents(Integer events) {
		this.events = events;
	}

	public String getIndexOffset() {
		return this.indexOffset;
	}

	public void setIndexOffset(String indexOffset) {
		this.indexOffset = indexOffset;
	}

	public BigDecimal getBasePrice() {
		return this.basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getDifferential() {
		return this.differential;
	}

	public void setDifferential(BigDecimal differential) {
		this.differential = differential;
	}

	public BigDecimal getMargin() {
		return this.margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public BigDecimal getFuelPrice() {
		return this.fuelPrice;
	}

	public void setFuelPrice(BigDecimal fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	public BigDecimal getIntoplaneFee() {
		return this.intoplaneFee;
	}

	public void setIntoplaneFee(BigDecimal intoplaneFee) {
		this.intoplaneFee = intoplaneFee;
	}

	public BigDecimal getOtherFees() {
		return this.otherFees;
	}

	public void setOtherFees(BigDecimal otherFees) {
		this.otherFees = otherFees;
	}

	public BigDecimal getTaxes() {
		return this.taxes;
	}

	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}

	public BigDecimal getFlightFee() {
		return this.flightFee;
	}

	public void setFlightFee(BigDecimal flightFee) {
		this.flightFee = flightFee;
	}

	public BigDecimal getTotalPricePerUom() {
		return this.totalPricePerUom;
	}

	public void setTotalPricePerUom(BigDecimal totalPricePerUom) {
		this.totalPricePerUom = totalPricePerUom;
	}

	public Integer getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(Integer paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public PaymentDay getPaymentRefDate() {
		return this.paymentRefDate;
	}

	public void setPaymentRefDate(PaymentDay paymentRefDate) {
		this.paymentRefDate = paymentRefDate;
	}

	public InvoiceFreq getInvoiceFreq() {
		return this.invoiceFreq;
	}

	public void setInvoiceFreq(InvoiceFreq invoiceFreq) {
		this.invoiceFreq = invoiceFreq;
	}

	public MasterAgreementsPeriod getPeriod() {
		return this.period;
	}

	public void setPeriod(MasterAgreementsPeriod period) {
		this.period = period;
	}

	public Date getFuelReleaseStartsOn() {
		return this.fuelReleaseStartsOn;
	}

	public void setFuelReleaseStartsOn(Date fuelReleaseStartsOn) {
		this.fuelReleaseStartsOn = fuelReleaseStartsOn;
	}

	public Date getFuelReleaseEndsOn() {
		return this.fuelReleaseEndsOn;
	}

	public void setFuelReleaseEndsOn(Date fuelReleaseEndsOn) {
		this.fuelReleaseEndsOn = fuelReleaseEndsOn;
	}

	public String getSettlementCurrencyCode() {
		return this.settlementCurrencyCode;
	}

	public void setSettlementCurrencyCode(String settlementCurrencyCode) {
		this.settlementCurrencyCode = settlementCurrencyCode;
	}

	public String getSettlementUnitCode() {
		return this.settlementUnitCode;
	}

	public void setSettlementUnitCode(String settlementUnitCode) {
		this.settlementUnitCode = settlementUnitCode;
	}

	public String getContractIndexOffset() {
		return this.contractIndexOffset;
	}

	public void setContractIndexOffset(String contractIndexOffset) {
		this.contractIndexOffset = contractIndexOffset;
	}

	public BigDecimal getContrFuelPrice() {
		return this.contrFuelPrice;
	}

	public void setContrFuelPrice(BigDecimal contrFuelPrice) {
		this.contrFuelPrice = contrFuelPrice;
	}

	public String getForex() {
		return this.getFinancialSource().getCode() + "/"
				+ this.getAverageMethod().getName();
	}

	public void setForex(String forex) {
		this.forex = forex;
	}

	public BigDecimal getDft() {
		return this.dft;
	}

	public void setDft(BigDecimal dft) {
		this.dft = dft;
	}

	public String getDisclaimer() {
		return this.disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public SoneMessage getToleranceMessage() {
		return CapturedPriceInformationResult.resultMap.get().get(this.getId());
	}

	public void setToleranceMessage(SoneMessage toleranceMessage) {
		if (CapturedPriceInformationResult.resultMap.get() == null) {
			CapturedPriceInformationResult.resultMap
					.set(new HashMap<Integer, SoneMessage>());
		}
		CapturedPriceInformationResult.resultMap.get().put(this.getId(),
				toleranceMessage);
	}

	public FuelQuotation getQuote() {
		return this.quote;
	}

	public void setQuote(FuelQuotation quote) {
		if (quote != null) {
			this.__validate_client_context__(quote.getClientId());
		}
		this.quote = quote;
	}
	public Locations getLocation() {
		return this.location;
	}

	public void setLocation(Locations location) {
		if (location != null) {
			this.__validate_client_context__(location.getClientId());
		}
		this.location = location;
	}
	public Suppliers getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Suppliers supplier) {
		if (supplier != null) {
			this.__validate_client_context__(supplier.getClientId());
		}
		this.supplier = supplier;
	}
	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		if (contract != null) {
			this.__validate_client_context__(contract.getClientId());
		}
		this.contract = contract;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public Suppliers getIntoPlaneAgent() {
		return this.intoPlaneAgent;
	}

	public void setIntoPlaneAgent(Suppliers intoPlaneAgent) {
		if (intoPlaneAgent != null) {
			this.__validate_client_context__(intoPlaneAgent.getClientId());
		}
		this.intoPlaneAgent = intoPlaneAgent;
	}
	public Quotation getQuotation() {
		return this.quotation;
	}

	public void setQuotation(Quotation quotation) {
		if (quotation != null) {
			this.__validate_client_context__(quotation.getClientId());
		}
		this.quotation = quotation;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}
	public AverageMethod getAverageMethod() {
		return this.averageMethod;
	}

	public void setAverageMethod(AverageMethod averageMethod) {
		if (averageMethod != null) {
			this.__validate_client_context__(averageMethod.getClientId());
		}
		this.averageMethod = averageMethod;
	}
	public FinancialSources getFinancialSource() {
		return this.financialSource;
	}

	public void setFinancialSource(FinancialSources financialSource) {
		if (financialSource != null) {
			this.__validate_client_context__(financialSource.getClientId());
		}
		this.financialSource = financialSource;
	}
	public PricePolicy getPricingPolicy() {
		return this.pricingPolicy;
	}

	public void setPricingPolicy(PricePolicy pricingPolicy) {
		if (pricingPolicy != null) {
			this.__validate_client_context__(pricingPolicy.getClientId());
		}
		this.pricingPolicy = pricingPolicy;
	}

	public Collection<FlightEvent> getFlightEvents() {
		return this.flightEvents;
	}

	public void setFlightEvents(Collection<FlightEvent> flightEvents) {
		this.flightEvents = flightEvents;
	}

	/**
	 * @param e
	 */
	public void addToFlightEvents(FlightEvent e) {
		if (this.flightEvents == null) {
			this.flightEvents = new ArrayList<>();
		}
		e.setLocQuote(this);
		this.flightEvents.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
