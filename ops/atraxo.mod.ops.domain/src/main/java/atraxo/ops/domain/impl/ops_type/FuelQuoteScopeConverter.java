/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelQuoteScope} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelQuoteScopeConverter
		implements
			AttributeConverter<FuelQuoteScope, String> {

	@Override
	public String convertToDatabaseColumn(FuelQuoteScope value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelQuoteScope.");
		}
		return value.getName();
	}

	@Override
	public FuelQuoteScope convertToEntityAttribute(String value) {
		return FuelQuoteScope.getByName(value);
	}

}
