package atraxo.ops.domain.ext.fuelTicket;

import java.util.ArrayList;
import java.util.List;

/**
 * @author apetho
 */
public final class CancelFuelTicketResult {

	private List<FuelTicketInvoice> generatedCreditMemos;
	private List<FuelTicketInvoice> removedFromInvoices;

	public void addCreditMemo(FuelTicketInvoice fti) {
		if (this.generatedCreditMemos == null) {
			this.generatedCreditMemos = new ArrayList<>();
		}
		this.generatedCreditMemos.add(fti);
	}

	public void addInvoice(FuelTicketInvoice fti) {
		if (this.removedFromInvoices == null) {
			this.removedFromInvoices = new ArrayList<>();
		}
		this.removedFromInvoices.add(fti);
	}

	public List<FuelTicketInvoice> getGeneratedCreditMemos() {
		return this.generatedCreditMemos;
	}

	public void setGeneratedCreditMemos(List<FuelTicketInvoice> generatedCreditMemos) {
		this.generatedCreditMemos = generatedCreditMemos;
	}

	public List<FuelTicketInvoice> getRemovedFromInvoices() {
		return this.removedFromInvoices;
	}

	public void setRemovedFromInvoices(List<FuelTicketInvoice> removedFromInvoices) {
		this.removedFromInvoices = removedFromInvoices;
	}

}
