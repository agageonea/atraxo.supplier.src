/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelOrder;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.ops_type.FuelOrderStatus;
import atraxo.ops.domain.impl.ops_type.FuelOrderStatusConverter;
import atraxo.ops.domain.impl.ops_type.FuelQuoteScope;
import atraxo.ops.domain.impl.ops_type.FuelQuoteScopeConverter;
import atraxo.ops.domain.impl.ops_type.FuelQuoteType;
import atraxo.ops.domain.impl.ops_type.FuelQuoteTypeConverter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelOrder} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelOrder.NQ_FIND_BY_CODE, query = "SELECT e FROM FuelOrder e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelOrder.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelOrder e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelOrder.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelOrder.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "order_code"})})
public class FuelOrder extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "OPS_PURCHASE_ORDERS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "FuelOrder.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelOrder.findByBusiness";

	@Column(name = "order_code", length = 32)
	private String code;

	@Column(name = "customer_reference_no", length = 32)
	private String customerReferenceNo;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "posting_date", nullable = false)
	private Date postedOn;

	@Column(name = "order_status", length = 32)
	@Convert(converter = FuelOrderStatusConverter.class)
	private FuelOrderStatus status;

	@NotNull
	@Column(name = "open_release", nullable = false)
	private Boolean openRelease;

	@Column(name = "contract_type", length = 32)
	@Convert(converter = FuelQuoteTypeConverter.class)
	private FuelQuoteType contractType;

	@Column(name = "scope", length = 32)
	@Convert(converter = FuelQuoteScopeConverter.class)
	private FuelQuoteScope scope;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelQuotation.class)
	@JoinColumn(name = "fuel_quote_id", referencedColumnName = "id")
	private FuelQuotation fuelQuote;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contacts.class)
	@JoinColumn(name = "contact_person_id", referencedColumnName = "id")
	private Contacts contact;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = UserSupp.class)
	@JoinColumn(name = "registered_by", referencedColumnName = "id")
	private UserSupp registeredBy;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "receiver_id", referencedColumnName = "id")
	private Customer subsidiary;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FuelOrderLocation.class, mappedBy = "fuelOrder")
	private Collection<FuelOrderLocation> fuelOrderLocations;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCustomerReferenceNo() {
		return this.customerReferenceNo;
	}

	public void setCustomerReferenceNo(String customerReferenceNo) {
		this.customerReferenceNo = customerReferenceNo;
	}

	public Date getPostedOn() {
		return this.postedOn;
	}

	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	public FuelOrderStatus getStatus() {
		return this.status;
	}

	public void setStatus(FuelOrderStatus status) {
		this.status = status;
	}

	public Boolean getOpenRelease() {
		return this.openRelease;
	}

	public void setOpenRelease(Boolean openRelease) {
		this.openRelease = openRelease;
	}

	public FuelQuoteType getContractType() {
		return this.contractType;
	}

	public void setContractType(FuelQuoteType contractType) {
		this.contractType = contractType;
	}

	public FuelQuoteScope getScope() {
		return this.scope;
	}

	public void setScope(FuelQuoteScope scope) {
		this.scope = scope;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public FuelQuotation getFuelQuote() {
		return this.fuelQuote;
	}

	public void setFuelQuote(FuelQuotation fuelQuote) {
		if (fuelQuote != null) {
			this.__validate_client_context__(fuelQuote.getClientId());
		}
		this.fuelQuote = fuelQuote;
	}
	public Contacts getContact() {
		return this.contact;
	}

	public void setContact(Contacts contact) {
		if (contact != null) {
			this.__validate_client_context__(contact.getClientId());
		}
		this.contact = contact;
	}
	public UserSupp getRegisteredBy() {
		return this.registeredBy;
	}

	public void setRegisteredBy(UserSupp registeredBy) {
		if (registeredBy != null) {
			this.__validate_client_context__(registeredBy.getClientId());
		}
		this.registeredBy = registeredBy;
	}
	public Customer getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(Customer subsidiary) {
		if (subsidiary != null) {
			this.__validate_client_context__(subsidiary.getClientId());
		}
		this.subsidiary = subsidiary;
	}

	public Collection<FuelOrderLocation> getFuelOrderLocations() {
		return this.fuelOrderLocations;
	}

	public void setFuelOrderLocations(
			Collection<FuelOrderLocation> fuelOrderLocations) {
		this.fuelOrderLocations = fuelOrderLocations;
	}

	/**
	 * @param e
	 */
	public void addToFuelOrderLocations(FuelOrderLocation e) {
		if (this.fuelOrderLocations == null) {
			this.fuelOrderLocations = new ArrayList<>();
		}
		e.setFuelOrder(this);
		this.fuelOrderLocations.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.openRelease == null) {
			this.openRelease = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
