package atraxo.ops.domain.ext.fuelTicket;

import java.util.List;

import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;

public class ProcessResult {
	private List<FuelTicket> insList;
	private List<FuelTicket> updList;
	private List<FuelTransaction> delFtList;
	private List<FuelTransaction> updFtList;

	public ProcessResult(List<FuelTicket> insList, List<FuelTicket> updList, List<FuelTransaction> delFtList, List<FuelTransaction> updFtList) {
		super();
		this.insList = insList;
		this.updList = updList;
		this.delFtList = delFtList;
		this.updFtList = updFtList;
	}

	public List<FuelTicket> getInsList() {
		return this.insList;
	}

	public List<FuelTicket> getUpdList() {
		return this.updList;
	}

	public List<FuelTransaction> getDelFtList() {
		return this.delFtList;
	}

	public List<FuelTransaction> getUpdFtList() {
		return this.updFtList;
	}

}
