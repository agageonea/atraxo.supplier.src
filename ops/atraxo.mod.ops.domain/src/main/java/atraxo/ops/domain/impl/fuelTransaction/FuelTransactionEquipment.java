/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelTransaction;

import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.FuelingTypeConverter;
import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.ops_type.DensityType;
import atraxo.ops.domain.impl.ops_type.DensityTypeConverter;
import atraxo.ops.domain.impl.ops_type.DensityUOM;
import atraxo.ops.domain.impl.ops_type.DensityUOMConverter;
import atraxo.ops.domain.impl.ops_type.TemperatureUnit;
import atraxo.ops.domain.impl.ops_type.TemperatureUnitConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelTransactionEquipment} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = FuelTransactionEquipment.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelTransactionEquipment e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelTransactionEquipment.TABLE_NAME)
public class FuelTransactionEquipment extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_TRANSACTION_EQUIPMENT";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelTransactionEquipment.findByBusiness";

	@Column(name = "fueling_equipment_id", length = 15)
	private String fuelingEquipmentId;

	@Column(name = "fueling_type", length = 16)
	@Convert(converter = FuelingTypeConverter.class)
	private FuelingType fuelingType;

	@Column(name = "average_fuel_temperature", precision = 19, scale = 6)
	private BigDecimal avgFuelTemperature;

	@Column(name = "temperature_uom", length = 32)
	@Convert(converter = TemperatureUnitConverter.class)
	private TemperatureUnit temperatureUOM;

	@Column(name = "density_type", length = 16)
	@Convert(converter = DensityTypeConverter.class)
	private DensityType densityType;

	@Column(name = "density", precision = 19, scale = 6)
	private BigDecimal density;

	@Column(name = "density_uom", length = 16)
	@Convert(converter = DensityUOMConverter.class)
	private DensityUOM densityUOM;

	@Column(name = "meter_reading_start", precision = 19, scale = 6)
	private BigDecimal meterReadingStart;

	@Column(name = "meter_reading_end", precision = 19, scale = 6)
	private BigDecimal meterReadingEnd;

	@Column(name = "meter_quantity_delivered", precision = 19, scale = 6)
	private BigDecimal meterQuantityDelivered;

	@Column(name = "meter_quantity_uom", length = 3)
	private String meterQuantityUOM;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelTransaction.class)
	@JoinColumn(name = "fuel_transaction_id", referencedColumnName = "id")
	private FuelTransaction fuelTransaction;

	public String getFuelingEquipmentId() {
		return this.fuelingEquipmentId;
	}

	public void setFuelingEquipmentId(String fuelingEquipmentId) {
		this.fuelingEquipmentId = fuelingEquipmentId;
	}

	public FuelingType getFuelingType() {
		return this.fuelingType;
	}

	public void setFuelingType(FuelingType fuelingType) {
		this.fuelingType = fuelingType;
	}

	public BigDecimal getAvgFuelTemperature() {
		return this.avgFuelTemperature;
	}

	public void setAvgFuelTemperature(BigDecimal avgFuelTemperature) {
		this.avgFuelTemperature = avgFuelTemperature;
	}

	public TemperatureUnit getTemperatureUOM() {
		return this.temperatureUOM;
	}

	public void setTemperatureUOM(TemperatureUnit temperatureUOM) {
		this.temperatureUOM = temperatureUOM;
	}

	public DensityType getDensityType() {
		return this.densityType;
	}

	public void setDensityType(DensityType densityType) {
		this.densityType = densityType;
	}

	public BigDecimal getDensity() {
		return this.density;
	}

	public void setDensity(BigDecimal density) {
		this.density = density;
	}

	public DensityUOM getDensityUOM() {
		return this.densityUOM;
	}

	public void setDensityUOM(DensityUOM densityUOM) {
		this.densityUOM = densityUOM;
	}

	public BigDecimal getMeterReadingStart() {
		return this.meterReadingStart;
	}

	public void setMeterReadingStart(BigDecimal meterReadingStart) {
		this.meterReadingStart = meterReadingStart;
	}

	public BigDecimal getMeterReadingEnd() {
		return this.meterReadingEnd;
	}

	public void setMeterReadingEnd(BigDecimal meterReadingEnd) {
		this.meterReadingEnd = meterReadingEnd;
	}

	public BigDecimal getMeterQuantityDelivered() {
		return this.meterQuantityDelivered;
	}

	public void setMeterQuantityDelivered(BigDecimal meterQuantityDelivered) {
		this.meterQuantityDelivered = meterQuantityDelivered;
	}

	public String getMeterQuantityUOM() {
		return this.meterQuantityUOM;
	}

	public void setMeterQuantityUOM(String meterQuantityUOM) {
		this.meterQuantityUOM = meterQuantityUOM;
	}

	public FuelTransaction getFuelTransaction() {
		return this.fuelTransaction;
	}

	public void setFuelTransaction(FuelTransaction fuelTransaction) {
		if (fuelTransaction != null) {
			this.__validate_client_context__(fuelTransaction.getClientId());
		}
		this.fuelTransaction = fuelTransaction;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
