/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InvoiceStatus {

	_EMPTY_(""), _NO_ACCRUALS_("No Accruals"), _PARTIALLY_("Partially"), _OK_(
			"OK"), _OPEN_ISSUES_("Open Issues");

	private String name;

	private InvoiceStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InvoiceStatus getByName(String name) {
		for (InvoiceStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent InvoiceStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
