/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link OperationType} enum.
 * Generated code. Do not modify in this file.
 */
public class OperationTypeConverter
		implements
			AttributeConverter<OperationType, String> {

	@Override
	public String convertToDatabaseColumn(OperationType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null OperationType.");
		}
		return value.getName();
	}

	@Override
	public OperationType convertToEntityAttribute(String value) {
		return OperationType.getByName(value);
	}

}
