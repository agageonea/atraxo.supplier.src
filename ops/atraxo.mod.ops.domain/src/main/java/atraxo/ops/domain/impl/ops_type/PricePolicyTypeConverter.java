/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link PricePolicyType} enum.
 * Generated code. Do not modify in this file.
 */
public class PricePolicyTypeConverter
		implements
			AttributeConverter<PricePolicyType, String> {

	@Override
	public String convertToDatabaseColumn(PricePolicyType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null PricePolicyType.");
		}
		return value.getName();
	}

	@Override
	public PricePolicyType convertToEntityAttribute(String value) {
		return PricePolicyType.getByName(value);
	}

}
