/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelTransaction;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelTransactionSourceParameters} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelTransactionSourceParameters.NQ_FIND_BY_CODE, query = "SELECT e FROM FuelTransactionSourceParameters e WHERE e.clientId = :clientId and e.fuelTransactionSource = :fuelTransactionSource and e.paramName = :paramName", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelTransactionSourceParameters.NQ_FIND_BY_CODE_PRIMITIVE, query = "SELECT e FROM FuelTransactionSourceParameters e WHERE e.clientId = :clientId and e.fuelTransactionSource.id = :fuelTransactionSourceId and e.paramName = :paramName", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelTransactionSourceParameters.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelTransactionSourceParameters e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelTransactionSourceParameters.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelTransactionSourceParameters.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "fuel_transaction_source_id",
		"param_name"})})
public class FuelTransactionSourceParameters extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_TRANSACTION_SOURCE_PARAM";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "FuelTransactionSourceParameters.findByCode";
	/**
	 * Named query find by unique key: Code using the ID field for references.
	 */
	public static final String NQ_FIND_BY_CODE_PRIMITIVE = "FuelTransactionSourceParameters.findByCode_PRIMITIVE";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelTransactionSourceParameters.findByBusiness";

	@Column(name = "param_name", length = 64)
	private String paramName;

	@Column(name = "param_value", length = 64)
	private String paramValue;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelTransactionSource.class)
	@JoinColumn(name = "fuel_transaction_source_id", referencedColumnName = "id")
	private FuelTransactionSource fuelTransactionSource;

	public String getParamName() {
		return this.paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return this.paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public FuelTransactionSource getFuelTransactionSource() {
		return this.fuelTransactionSource;
	}

	public void setFuelTransactionSource(
			FuelTransactionSource fuelTransactionSource) {
		if (fuelTransactionSource != null) {
			this.__validate_client_context__(fuelTransactionSource
					.getClientId());
		}
		this.fuelTransactionSource = fuelTransactionSource;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

	@Override
	public int hashCode() {
		final int prime = 13;
		int result = prime
				+ ((this.getClientId() == null) ? 0 : this.getClientId()
						.hashCode());
		result = prime
				* result
				+ ((this.fuelTransactionSource == null)
						? 0
						: this.fuelTransactionSource.hashCode());
		result = prime * result
				+ ((this.paramName == null) ? 0 : this.paramName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		FuelTransactionSourceParameters other = (FuelTransactionSourceParameters) obj;
		if (this.getClientId() == null) {
			if (other.getClientId() != null) {
				return false;
			}
		} else if (!this.getClientId().equals(other.getClientId())) {
			return false;
		}
		if (this.fuelTransactionSource == null) {
			if (other.fuelTransactionSource != null) {
				return false;
			}
		} else if (!this.fuelTransactionSource
				.equals(other.fuelTransactionSource)) {
			return false;
		}
		if (this.paramName == null) {
			if (other.paramName != null) {
				return false;
			}
		} else if (!this.paramName.equals(other.paramName)) {
			return false;
		}
		return true;
	}
}
