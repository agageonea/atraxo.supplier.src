/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelOrderStatus {

	_EMPTY_(""), _NEW_("New"), _REJECTED_("Rejected"), _CONFIRMED_("Confirmed"), _CANCELED_(
			"Canceled"), _RELEASED_("Released");

	private String name;

	private FuelOrderStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelOrderStatus getByName(String name) {
		for (FuelOrderStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FuelOrderStatus with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
