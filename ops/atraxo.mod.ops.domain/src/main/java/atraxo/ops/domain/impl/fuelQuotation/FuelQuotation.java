/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelQuotation;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FuelQuoteCycle;
import atraxo.ops.domain.impl.ops_type.FuelQuoteCycleConverter;
import atraxo.ops.domain.impl.ops_type.FuelQuoteScope;
import atraxo.ops.domain.impl.ops_type.FuelQuoteScopeConverter;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatus;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatusConverter;
import atraxo.ops.domain.impl.ops_type.FuelQuoteType;
import atraxo.ops.domain.impl.ops_type.FuelQuoteTypeConverter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link FuelQuotation} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelQuotation.NQ_FIND_BY_CODE, query = "SELECT e FROM FuelQuotation e WHERE e.clientId = :clientId and e.code = :code", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelQuotation.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelQuotation e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelQuotation.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelQuotation.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "code"})})
public class FuelQuotation extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_PRICES_QUOTES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "FuelQuotation.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelQuotation.findByBusiness";

	@Column(name = "code", length = 32)
	private String code;

	@NotBlank
	@Column(name = "quotation_type", nullable = false, length = 32)
	@Convert(converter = FuelQuoteTypeConverter.class)
	private FuelQuoteType type;

	@Column(name = "scope", length = 32)
	@Convert(converter = FuelQuoteScopeConverter.class)
	private FuelQuoteScope scope;

	@Column(name = "quote_status", length = 32)
	@Convert(converter = FuelQuoteStatusConverter.class)
	private FuelQuoteStatus status;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "posting_date", nullable = false)
	private Date postedOn;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "valid_until", nullable = false)
	private Date validUntil;

	@Column(name = "pricing_cycle", length = 32)
	@Convert(converter = FuelQuoteCycleConverter.class)
	private FuelQuoteCycle pricingCycle;

	@Column(name = "open_release")
	private Boolean openRelease;

	@Column(name = "owner", length = 64)
	private String owner;

	@Column(name = "disclaimer", length = 255)
	private String disclaimer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelRequest.class)
	@JoinColumn(name = "fuel_request_id", referencedColumnName = "id")
	private FuelRequest fuelRequest;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelOrder.class)
	@JoinColumn(name = "fuel_order_id", referencedColumnName = "id")
	private FuelOrder fuelOrder;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "holder_id", referencedColumnName = "id")
	private Customer holder;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FuelQuoteLocation.class, mappedBy = "quote")
	private Collection<FuelQuoteLocation> fuelQuoteLocations;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public FuelQuoteType getType() {
		return this.type;
	}

	public void setType(FuelQuoteType type) {
		this.type = type;
	}

	public FuelQuoteScope getScope() {
		return this.scope;
	}

	public void setScope(FuelQuoteScope scope) {
		this.scope = scope;
	}

	public FuelQuoteStatus getStatus() {
		return this.status;
	}

	public void setStatus(FuelQuoteStatus status) {
		this.status = status;
	}

	public Date getPostedOn() {
		return this.postedOn;
	}

	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	public Date getValidUntil() {
		return this.validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public FuelQuoteCycle getPricingCycle() {
		return this.pricingCycle;
	}

	public void setPricingCycle(FuelQuoteCycle pricingCycle) {
		this.pricingCycle = pricingCycle;
	}

	public Boolean getOpenRelease() {
		return this.openRelease;
	}

	public void setOpenRelease(Boolean openRelease) {
		this.openRelease = openRelease;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getDisclaimer() {
		return this.disclaimer;
	}

	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public FuelRequest getFuelRequest() {
		return this.fuelRequest;
	}

	public void setFuelRequest(FuelRequest fuelRequest) {
		if (fuelRequest != null) {
			this.__validate_client_context__(fuelRequest.getClientId());
		}
		this.fuelRequest = fuelRequest;
	}
	public FuelOrder getFuelOrder() {
		return this.fuelOrder;
	}

	public void setFuelOrder(FuelOrder fuelOrder) {
		if (fuelOrder != null) {
			this.__validate_client_context__(fuelOrder.getClientId());
		}
		this.fuelOrder = fuelOrder;
	}
	public Customer getHolder() {
		return this.holder;
	}

	public void setHolder(Customer holder) {
		if (holder != null) {
			this.__validate_client_context__(holder.getClientId());
		}
		this.holder = holder;
	}

	public Collection<FuelQuoteLocation> getFuelQuoteLocations() {
		return this.fuelQuoteLocations;
	}

	public void setFuelQuoteLocations(
			Collection<FuelQuoteLocation> fuelQuoteLocations) {
		this.fuelQuoteLocations = fuelQuoteLocations;
	}

	/**
	 * @param e
	 */
	public void addToFuelQuoteLocations(FuelQuoteLocation e) {
		if (this.fuelQuoteLocations == null) {
			this.fuelQuoteLocations = new ArrayList<>();
		}
		e.setQuote(this);
		this.fuelQuoteLocations.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
