/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InterfaceType {

	_EMPTY_(""), _IATA_("IATA"), _JETA_COM_("JETA.COM");

	private String name;

	private InterfaceType(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InterfaceType getByName(String name) {
		for (InterfaceType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent InterfaceType with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
