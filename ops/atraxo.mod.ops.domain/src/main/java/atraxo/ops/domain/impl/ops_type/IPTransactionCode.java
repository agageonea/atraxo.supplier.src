/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum IPTransactionCode {

	_EMPTY_("", ""), _UPLIFT_("Uplift", "FU"), _DE_FUEL_("De-fuel", "DE"), _RE_FUEL_(
			"Re-fuel", "TO");

	private String name;
	private String code;

	private IPTransactionCode(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static IPTransactionCode getByName(String name) {
		for (IPTransactionCode status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent IPTransactionCode with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static IPTransactionCode getByCode(String code) {
		for (IPTransactionCode status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent IPTransactionCode with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
