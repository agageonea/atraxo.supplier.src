/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link SendInvoiceBy} enum.
 * Generated code. Do not modify in this file.
 */
public class SendInvoiceByConverter
		implements
			AttributeConverter<SendInvoiceBy, String> {

	@Override
	public String convertToDatabaseColumn(SendInvoiceBy value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null SendInvoiceBy.");
		}
		return value.getName();
	}

	@Override
	public SendInvoiceBy convertToEntityAttribute(String value) {
		return SendInvoiceBy.getByName(value);
	}

}
