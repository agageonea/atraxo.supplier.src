/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelQuoteType} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelQuoteTypeConverter
		implements
			AttributeConverter<FuelQuoteType, String> {

	@Override
	public String convertToDatabaseColumn(FuelQuoteType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelQuoteType.");
		}
		return value.getName();
	}

	@Override
	public FuelQuoteType convertToEntityAttribute(String value) {
		return FuelQuoteType.getByName(value);
	}

}
