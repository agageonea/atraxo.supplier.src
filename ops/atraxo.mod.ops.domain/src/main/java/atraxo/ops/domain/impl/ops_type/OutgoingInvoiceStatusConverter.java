/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link OutgoingInvoiceStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class OutgoingInvoiceStatusConverter
		implements
			AttributeConverter<OutgoingInvoiceStatus, String> {

	@Override
	public String convertToDatabaseColumn(OutgoingInvoiceStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null OutgoingInvoiceStatus.");
		}
		return value.getName();
	}

	@Override
	public OutgoingInvoiceStatus convertToEntityAttribute(String value) {
		return OutgoingInvoiceStatus.getByName(value);
	}

}
