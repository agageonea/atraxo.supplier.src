/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum InternationalFlight {

	_EMPTY_("", ""), _INTERNATIONAL_("International", "1"), _DOMESTIC_(
			"Domestic", "0");

	private String name;
	private String code;

	private InternationalFlight(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static InternationalFlight getByName(String name) {
		for (InternationalFlight status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InternationalFlight with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static InternationalFlight getByCode(String code) {
		for (InternationalFlight status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent InternationalFlight with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
