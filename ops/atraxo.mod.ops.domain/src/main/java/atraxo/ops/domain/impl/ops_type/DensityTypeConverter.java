/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link DensityType} enum.
 * Generated code. Do not modify in this file.
 */
public class DensityTypeConverter
		implements
			AttributeConverter<DensityType, String> {

	@Override
	public String convertToDatabaseColumn(DensityType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null DensityType.");
		}
		return value.getName();
	}

	@Override
	public DensityType convertToEntityAttribute(String value) {
		return DensityType.getByName(value);
	}

}
