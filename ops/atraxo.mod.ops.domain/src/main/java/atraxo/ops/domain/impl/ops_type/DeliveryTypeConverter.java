/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link DeliveryType} enum.
 * Generated code. Do not modify in this file.
 */
public class DeliveryTypeConverter
		implements
			AttributeConverter<DeliveryType, String> {

	@Override
	public String convertToDatabaseColumn(DeliveryType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null DeliveryType.");
		}
		return value.getName();
	}

	@Override
	public DeliveryType convertToEntityAttribute(String value) {
		return DeliveryType.getByName(value);
	}

}
