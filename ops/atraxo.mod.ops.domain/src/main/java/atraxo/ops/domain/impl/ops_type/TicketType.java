/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum TicketType {

	_EMPTY_("", ""), _ORIGINAL_("Original", "O"), _REISSUE_("Reissue", "R"), _CANCEL_(
			"Cancel", "C"), _DELETE_("Delete", "D");

	private String name;
	private String code;

	private TicketType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static TicketType getByName(String name) {
		for (TicketType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TicketType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static TicketType getByCode(String code) {
		for (TicketType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent TicketType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
