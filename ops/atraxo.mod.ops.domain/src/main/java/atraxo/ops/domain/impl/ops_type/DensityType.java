/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum DensityType {

	_EMPTY_("", ""), _MEASURED_("Measured", "MEA"), _STANDARD_("Standard",
			"STD");

	private String name;
	private String code;

	private DensityType(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static DensityType getByName(String name) {
		for (DensityType status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent DensityType with name: "
				+ name);
	}

	public String getCode() {
		return this.code;
	}

	public static DensityType getByCode(String code) {
		for (DensityType status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent DensityType with code: "
				+ code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
