/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelTicketTransmissionStatus {

	_EMPTY_(""), _NEW_("New"), _SUBMITTED_("Submitted"), _FAILED_("Failed"), _EXPORTED_(
			"Exported"), _TRANSMITTED_("Transmitted");

	private String name;

	private FuelTicketTransmissionStatus(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelTicketTransmissionStatus getByName(String name) {
		for (FuelTicketTransmissionStatus status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent FuelTicketTransmissionStatus with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
