/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelTicketSource} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelTicketSourceConverter
		implements
			AttributeConverter<FuelTicketSource, String> {

	@Override
	public String convertToDatabaseColumn(FuelTicketSource value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelTicketSource.");
		}
		return value.getName();
	}

	@Override
	public FuelTicketSource convertToEntityAttribute(String value) {
		return FuelTicketSource.getByName(value);
	}

}
