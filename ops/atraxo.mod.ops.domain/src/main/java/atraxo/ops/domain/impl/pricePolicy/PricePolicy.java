/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.pricePolicy;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link PricePolicy} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = PricePolicy.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM PricePolicy e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = PricePolicy.TABLE_NAME)
public class PricePolicy extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "OPS_PRICES_POLICIES";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "PricePolicy.findByBusiness";

	@Column(name = "name", length = 64)
	private String name;

	@Column(name = "default_margin", precision = 19, scale = 6)
	private BigDecimal defaultMargin;

	@Column(name = "minimal_margin", precision = 19, scale = 6)
	private BigDecimal minimalMargin;

	@Column(name = "default_markup", precision = 19, scale = 6)
	private BigDecimal defaultMarkup;

	@Column(name = "minimal_markup", precision = 19, scale = 6)
	private BigDecimal minimalMarkup;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from")
	private Date validFrom;

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to")
	private Date validTo;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "comments", length = 255)
	private String comments;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Locations.class)
	@JoinColumn(name = "location_id", referencedColumnName = "id")
	private Locations location;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = AcTypes.class)
	@JoinColumn(name = "aircraft_type_id", referencedColumnName = "id")
	private AcTypes aircraft;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Unit.class)
	@JoinColumn(name = "markup_unit_id", referencedColumnName = "id")
	private Unit unit;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Currencies.class)
	@JoinColumn(name = "markup_currency_id", referencedColumnName = "id")
	private Currencies currency;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getDefaultMargin() {
		return this.defaultMargin;
	}

	public void setDefaultMargin(BigDecimal defaultMargin) {
		this.defaultMargin = defaultMargin;
	}

	public BigDecimal getMinimalMargin() {
		return this.minimalMargin;
	}

	public void setMinimalMargin(BigDecimal minimalMargin) {
		this.minimalMargin = minimalMargin;
	}

	public BigDecimal getDefaultMarkup() {
		return this.defaultMarkup;
	}

	public void setDefaultMarkup(BigDecimal defaultMarkup) {
		this.defaultMarkup = defaultMarkup;
	}

	public BigDecimal getMinimalMarkup() {
		return this.minimalMarkup;
	}

	public void setMinimalMarkup(BigDecimal minimalMarkup) {
		this.minimalMarkup = minimalMarkup;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Locations getLocation() {
		return this.location;
	}

	public void setLocation(Locations location) {
		if (location != null) {
			this.__validate_client_context__(location.getClientId());
		}
		this.location = location;
	}
	public AcTypes getAircraft() {
		return this.aircraft;
	}

	public void setAircraft(AcTypes aircraft) {
		if (aircraft != null) {
			this.__validate_client_context__(aircraft.getClientId());
		}
		this.aircraft = aircraft;
	}
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		if (unit != null) {
			this.__validate_client_context__(unit.getClientId());
		}
		this.unit = unit;
	}
	public Currencies getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currencies currency) {
		if (currency != null) {
			this.__validate_client_context__(currency.getClientId());
		}
		this.currency = currency;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
