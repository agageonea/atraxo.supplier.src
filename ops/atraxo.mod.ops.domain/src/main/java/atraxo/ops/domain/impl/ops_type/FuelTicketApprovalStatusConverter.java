/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelTicketApprovalStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelTicketApprovalStatusConverter
		implements
			AttributeConverter<FuelTicketApprovalStatus, String> {

	@Override
	public String convertToDatabaseColumn(FuelTicketApprovalStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null FuelTicketApprovalStatus.");
		}
		return value.getName();
	}

	@Override
	public FuelTicketApprovalStatus convertToEntityAttribute(String value) {
		return FuelTicketApprovalStatus.getByName(value);
	}

}
