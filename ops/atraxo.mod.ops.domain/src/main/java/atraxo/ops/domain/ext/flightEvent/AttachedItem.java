package atraxo.ops.domain.ext.flightEvent;

public enum AttachedItem {

	FUEL_REQUEST,
	FUEL_QUOTATION;
}
