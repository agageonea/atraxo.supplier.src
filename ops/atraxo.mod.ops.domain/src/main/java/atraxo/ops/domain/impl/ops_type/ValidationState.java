/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum ValidationState {

	_EMPTY_(""), _NOT_CHECKED_("Not checked"), _FAILED_("Failed");

	private String name;

	private ValidationState(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static ValidationState getByName(String name) {
		for (ValidationState status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent ValidationState with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
