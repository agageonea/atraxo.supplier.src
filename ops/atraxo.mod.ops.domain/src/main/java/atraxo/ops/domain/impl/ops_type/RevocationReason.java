/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum RevocationReason {

	_EMPTY_("", ""), _INCORRECT_CUSTOMER_INFORMATION_(
			"Incorrect Customer Information", "RC01"), _INCORRECT_FLIGHT_AIRCRAFT_INFORMATION_(
			"Incorrect Flight/Aircraft Information", "RC01"), _INCORRECT_VOLUME_(
			"Incorrect Volume", "RC01"), _INCORRECT_PRICE___DISCOUNT___TAX___FREIGHT_(
			"Incorrect Price / Discount / Tax / Freight", "RC03"), _COMMERCIAL_REASON_(
			"Commercial Reason", "RC09");

	private String name;
	private String code;

	private RevocationReason(String name, String code) {
		this.name = name;
		this.code = code;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static RevocationReason getByName(String name) {
		for (RevocationReason status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent RevocationReason with name: " + name);
	}

	public String getCode() {
		return this.code;
	}

	public static RevocationReason getByCode(String code) {
		for (RevocationReason status : values()) {
			if (status.getCode().equalsIgnoreCase(code)) {
				return status;
			}
		}
		throw new InvalidEnumException(
				"Inexistent RevocationReason with code: " + code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
