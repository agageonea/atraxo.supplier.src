/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelRequest;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.ops_type.FuelRequestStatus;
import atraxo.ops.domain.impl.ops_type.FuelRequestStatusConverter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity class for {@link FuelRequest} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({
		@NamedQuery(name = FuelRequest.NQ_FIND_BY_CODE, query = "SELECT e FROM FuelRequest e WHERE e.clientId = :clientId and e.requestCode = :requestCode", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)),
		@NamedQuery(name = FuelRequest.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelRequest e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelRequest.TABLE_NAME, uniqueConstraints = {@UniqueConstraint(name = FuelRequest.TABLE_NAME
		+ "_UK1", columnNames = {"clientid", "request_code"})})
public class FuelRequest extends AbstractEntity implements Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_REQUESTS";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Code.
	 */
	public static final String NQ_FIND_BY_CODE = "FuelRequest.findByCode";
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelRequest.findByBusiness";

	@NotBlank
	@Column(name = "request_code", nullable = false, length = 32)
	private String requestCode;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "date", nullable = false)
	private Date requestDate;

	@Column(name = "customer_reference_no", length = 32)
	private String customerReferenceNo;

	@Column(name = "requested_service", length = 32)
	private String requestedService;

	@NotNull
	@Column(name = "isopenrelease", nullable = false)
	private Boolean isOpenRelease;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@NotBlank
	@Column(name = "request_status", nullable = false, length = 32)
	@Convert(converter = FuelRequestStatusConverter.class)
	private FuelRequestStatus requestStatus;

	@NotBlank
	@Column(name = "proccessed_by", nullable = false, length = 64)
	private String processedBy;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id")
	private Customer customer;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Contacts.class)
	@JoinColumn(name = "contact_person_id", referencedColumnName = "id")
	private Contacts contact;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelQuotation.class)
	@JoinColumn(name = "fuel_quote_id", referencedColumnName = "id")
	private FuelQuotation fuelQuotation;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "receiver_id", referencedColumnName = "id")
	private Customer subsidiary;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = FlightEvent.class, mappedBy = "fuelRequest")
	private Collection<FlightEvent> flightEvent;

	public String getRequestCode() {
		return this.requestCode;
	}

	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getCustomerReferenceNo() {
		return this.customerReferenceNo;
	}

	public void setCustomerReferenceNo(String customerReferenceNo) {
		this.customerReferenceNo = customerReferenceNo;
	}

	public String getRequestedService() {
		return this.requestedService;
	}

	public void setRequestedService(String requestedService) {
		this.requestedService = requestedService;
	}

	public Boolean getIsOpenRelease() {
		return this.isOpenRelease;
	}

	public void setIsOpenRelease(Boolean isOpenRelease) {
		this.isOpenRelease = isOpenRelease;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public FuelRequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	public void setRequestStatus(FuelRequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		if (customer != null) {
			this.__validate_client_context__(customer.getClientId());
		}
		this.customer = customer;
	}
	public Contacts getContact() {
		return this.contact;
	}

	public void setContact(Contacts contact) {
		if (contact != null) {
			this.__validate_client_context__(contact.getClientId());
		}
		this.contact = contact;
	}
	public FuelQuotation getFuelQuotation() {
		return this.fuelQuotation;
	}

	public void setFuelQuotation(FuelQuotation fuelQuotation) {
		if (fuelQuotation != null) {
			this.__validate_client_context__(fuelQuotation.getClientId());
		}
		this.fuelQuotation = fuelQuotation;
	}
	public Customer getSubsidiary() {
		return this.subsidiary;
	}

	public void setSubsidiary(Customer subsidiary) {
		if (subsidiary != null) {
			this.__validate_client_context__(subsidiary.getClientId());
		}
		this.subsidiary = subsidiary;
	}

	public Collection<FlightEvent> getFlightEvent() {
		return this.flightEvent;
	}

	public void setFlightEvent(Collection<FlightEvent> flightEvent) {
		this.flightEvent = flightEvent;
	}

	/**
	 * @param e
	 */
	public void addToFlightEvent(FlightEvent e) {
		if (this.flightEvent == null) {
			this.flightEvent = new ArrayList<>();
		}
		e.setFuelRequest(this);
		this.flightEvent.add(e);
	}
	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
		if (this.isOpenRelease == null) {
			this.isOpenRelease = new Boolean(false);
		}
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
