/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link TicketType} enum.
 * Generated code. Do not modify in this file.
 */
public class TicketTypeConverter
		implements
			AttributeConverter<TicketType, String> {

	@Override
	public String convertToDatabaseColumn(TicketType value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null TicketType.");
		}
		return value.getName();
	}

	@Override
	public TicketType convertToEntityAttribute(String value) {
		return TicketType.getByName(value);
	}

}
