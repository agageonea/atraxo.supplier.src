/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum FuelQuoteCycle {

	_EMPTY_(""), _NOTICE_("Notice"), _WEEKLY_("Weekly"), _BI_WEEKLY_(
			"Bi-weekly"), _MONTHLY_("Monthly");

	private String name;

	private FuelQuoteCycle(String name) {
		this.name = name;
	}

	@JsonValue
	public String getName() {
		return this.name;
	}

	@JsonCreator
	public static FuelQuoteCycle getByName(String name) {
		for (FuelQuoteCycle status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent FuelQuoteCycle with name: "
				+ name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
