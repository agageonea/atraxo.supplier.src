/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FlightEventStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FlightEventStatusConverter
		implements
			AttributeConverter<FlightEventStatus, String> {

	@Override
	public String convertToDatabaseColumn(FlightEventStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FlightEventStatus.");
		}
		return value.getName();
	}

	@Override
	public FlightEventStatus convertToEntityAttribute(String value) {
		return FlightEventStatus.getByName(value);
	}

}
