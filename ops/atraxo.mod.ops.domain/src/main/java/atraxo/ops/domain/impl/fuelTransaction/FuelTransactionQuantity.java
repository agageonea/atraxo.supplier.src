/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.fuelTransaction;

import atraxo.fmbas.domain.impl.abstracts.AbstractEntity;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.ops_type.FuelQuantityType;
import atraxo.ops.domain.impl.ops_type.FuelQuantityTypeConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Entity class for {@link FuelTransactionQuantity} domain entity.
 * Generated code. Do not modify in this file.
 */
@NamedQueries({@NamedQuery(name = FuelTransactionQuantity.NQ_FIND_BY_BUSINESS, query = "SELECT e FROM FuelTransactionQuantity e WHERE e.clientId = :clientId and e.id = :id", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@Entity
@Table(name = FuelTransactionQuantity.TABLE_NAME)
public class FuelTransactionQuantity extends AbstractEntity
		implements
			Serializable {

	public static final String TABLE_NAME = "OPS_FUEL_TRANSACTION_QUANTITY";

	private static final long serialVersionUID = -8865917134914502125L;
	/**
	 * Named query find by unique key: Business.
	 */
	public static final String NQ_FIND_BY_BUSINESS = "FuelTransactionQuantity.findByBusiness";

	@Column(name = "fuel_quantity", precision = 19, scale = 6)
	private BigDecimal fuelQuantity;

	@Column(name = "quantity_uom", length = 3)
	private String quantityUOM;

	@Column(name = "quantity_type", length = 16)
	@Convert(converter = FuelQuantityTypeConverter.class)
	private FuelQuantityType quantityType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = FuelTransaction.class)
	@JoinColumn(name = "fuel_transaction_id", referencedColumnName = "id")
	private FuelTransaction fuelTransaction;

	public BigDecimal getFuelQuantity() {
		return this.fuelQuantity;
	}

	public void setFuelQuantity(BigDecimal fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}

	public String getQuantityUOM() {
		return this.quantityUOM;
	}

	public void setQuantityUOM(String quantityUOM) {
		this.quantityUOM = quantityUOM;
	}

	public FuelQuantityType getQuantityType() {
		return this.quantityType;
	}

	public void setQuantityType(FuelQuantityType quantityType) {
		this.quantityType = quantityType;
	}

	public FuelTransaction getFuelTransaction() {
		return this.fuelTransaction;
	}

	public void setFuelTransaction(FuelTransaction fuelTransaction) {
		if (fuelTransaction != null) {
			this.__validate_client_context__(fuelTransaction.getClientId());
		}
		this.fuelTransaction = fuelTransaction;
	}

	/**
	 * called before persist
	 */
	@Override
	@PrePersist
	public void prePersist() {
		super.prePersist();
	}

	/**
	 * called before update
	 */
	@Override
	@PreUpdate
	public void preUpdate() {
		super.preUpdate();
	}

}
