/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link RevocationReason} enum.
 * Generated code. Do not modify in this file.
 */
public class RevocationReasonConverter
		implements
			AttributeConverter<RevocationReason, String> {

	@Override
	public String convertToDatabaseColumn(RevocationReason value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null RevocationReason.");
		}
		return value.getName();
	}

	@Override
	public RevocationReason convertToEntityAttribute(String value) {
		return RevocationReason.getByName(value);
	}

}
