/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelTicketTransmissionStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelTicketTransmissionStatusConverter
		implements
			AttributeConverter<FuelTicketTransmissionStatus, String> {

	@Override
	public String convertToDatabaseColumn(FuelTicketTransmissionStatus value) {
		if (value == null) {
			throw new InvalidEnumException(
					"Inexistent null FuelTicketTransmissionStatus.");
		}
		return value.getName();
	}

	@Override
	public FuelTicketTransmissionStatus convertToEntityAttribute(String value) {
		return FuelTicketTransmissionStatus.getByName(value);
	}

}
