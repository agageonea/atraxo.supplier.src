/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link Suffix} enum.
 * Generated code. Do not modify in this file.
 */
public class SuffixConverter implements AttributeConverter<Suffix, String> {

	@Override
	public String convertToDatabaseColumn(Suffix value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null Suffix.");
		}
		return value.getName();
	}

	@Override
	public Suffix convertToEntityAttribute(String value) {
		return Suffix.getByName(value);
	}

}
