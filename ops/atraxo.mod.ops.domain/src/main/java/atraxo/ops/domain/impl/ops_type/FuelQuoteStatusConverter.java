/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.domain.impl.ops_type;

import javax.persistence.AttributeConverter;
import seava.j4e.api.exceptions.InvalidEnumException;

/**
 * Converter class for {@link FuelQuoteStatus} enum.
 * Generated code. Do not modify in this file.
 */
public class FuelQuoteStatusConverter
		implements
			AttributeConverter<FuelQuoteStatus, String> {

	@Override
	public String convertToDatabaseColumn(FuelQuoteStatus value) {
		if (value == null) {
			throw new InvalidEnumException("Inexistent null FuelQuoteStatus.");
		}
		return value.getName();
	}

	@Override
	public FuelQuoteStatus convertToEntityAttribute(String value) {
		return FuelQuoteStatus.getByName(value);
	}

}
