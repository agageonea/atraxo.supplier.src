/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.fuelOrder.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.business.api.fuelOrder.IFuelOrderService;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.business.api.fuelQuotation.IFuelQuotationService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.business.ext.fuelOrder.delete.DeleteFuelOrder_Bd;
import atraxo.ops.business.ext.integration.FuelEventSource;
import atraxo.ops.domain.ext.DeliveryNoteMethod.DeliveryNoteMethod;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.ops_type.FlightEventStatus;
import atraxo.ops.domain.impl.ops_type.FuelOrderStatus;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatus;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link FuelOrder} domain entity.
 */
public class FuelOrder_Service extends atraxo.ops.business.impl.fuelOrder.FuelOrder_Service implements IFuelOrderService {

	@Override
	protected void postInsert(FuelOrder e) throws BusinessException {
		super.postInsert(e);
		FuelOrder savedFo = this.findByRefid(e.getRefid());
		this.calculateCode(savedFo);
		this.update(savedFo);
		this.updateQuotation(savedFo.getFuelQuote(), savedFo);
		this.generateFuelOrderLocations(savedFo.getFuelQuote(), savedFo);
	}

	public void calculateCode(FuelOrder fo) {
		int n = String.valueOf(fo.getId()).length();
		String code = "FO";
		for (int i = 1; i <= 5 - n; ++i) {
			code += "0";
		}
		fo.setCode(code + fo.getId());
	}

	private void updateQuotation(FuelQuotation fq, FuelOrder fo) throws BusinessException {
		IFuelQuotationService fqService = (IFuelQuotationService) this.findEntityService(FuelQuotation.class);
		fq.setFuelOrder(fo);
		fq.setStatus(FuelQuoteStatus._ORDERED_);
		fqService.update(fq);
	}

	@Override
	@History(value = "New fuel order", type = FuelOrder.class)
	@Transactional
	public void insert(List<FuelOrder> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	@Transactional
	@History(type = FuelOrder.class)
	public void updateStatus(FuelOrder e, @Action String action, @Reason String reason, FuelOrderStatus status) throws BusinessException {
		e.setStatus(status);
		this.onUpdate(e);
		if (status.equals(FuelOrderStatus._CONFIRMED_)) {
			this.sendMessage(e, DeliveryNoteMethod.GENERATE);
		}
	}

	private void sendMessage(FuelOrder e, DeliveryNoteMethod method) {
		FuelEventSource source = new FuelEventSource(e, method);
		this.sendMessage("maintainFuelEvents", source);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	@History(type = FuelOrder.class, value = "New fuel order")
	public void generate(FuelQuotation fq, Date postedOn, Contacts submited, String customerRefNo) throws BusinessException {
		FuelOrder fo = this.buildFuelOrder(fq, postedOn, submited, customerRefNo);
		this.insert(fo);
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		DeleteFuelOrder_Bd bd = this.getBusinessDelegate(DeleteFuelOrder_Bd.class);
		bd.removeQuotationReference(ids);
		bd.deleteFuelOrderLocations(ids);
	}

	private FuelOrder buildFuelOrder(FuelQuotation fq, Date postedOn, Contacts submited, String customerRefNo) throws BusinessException {
		FuelOrder fo = new FuelOrder();
		fo.setContact(submited);
		fo.setContractType(fq.getType());
		fo.setCustomer(fq.getCustomer());
		fo.setCustomerReferenceNo(customerRefNo);
		fo.setFuelQuote(fq);
		fo.setOpenRelease(fq.getOpenRelease());
		fo.setPostedOn(postedOn);
		fo.setScope(fq.getScope());
		fo.setStatus(FuelOrderStatus._NEW_);
		fo.setSubsidiary(fq.getHolder());
		IUserSuppService usersService = (IUserSuppService) this.findEntityService(UserSupp.class);
		UserSupp user = usersService.findByCode(Session.user.get().getCode());
		fo.setRegisteredBy(user);
		return fo;
	}

	private void generateFuelOrderLocations(FuelQuotation fq, FuelOrder fo) throws BusinessException {
		IFuelOrderLocationService service = (IFuelOrderLocationService) this.findEntityService(FuelOrderLocation.class);
		service.generate(new ArrayList<FuelQuoteLocation>(fq.getFuelQuoteLocations()), fo);
	}

	@Override
	@Transactional
	public void cancel(FuelOrder fo, String remark) throws BusinessException {
		IFlightEventService flightEventService = (IFlightEventService) this.findEntityService(FlightEvent.class);
		List<FlightEvent> flightEvents = flightEventService.findByFuelOrder(fo);
		FuelOrderStatus fos = fo.getStatus();
		for (FlightEvent fe : flightEvents) {
			if (!fe.getStatus().equals(FlightEventStatus._SCHEDULED_)) {
				throw new BusinessException(OpsErrorCode.CAN_NOT_CANCEL_FUEL_ORDER, OpsErrorCode.CAN_NOT_CANCEL_FUEL_ORDER.getErrMsg());
			}
		}
		fo.setStatus(FuelOrderStatus._CANCELED_);
		this.update(fo);
		this.addHistory(fo, remark);
		this.sendMessage(fo, DeliveryNoteMethod.DELETE);
	}

	private void addHistory(FuelOrder fo, String remark) throws BusinessException {
		IChangeHistoryService historyService = (IChangeHistoryService) this.findEntityService(ChangeHistory.class);
		ChangeHistory ch = historyService.create();
		ch.setClientId(fo.getClientId());
		ch.setObjectId(fo.getId());
		ch.setObjectType(fo.getEntityAlias());
		ch.setRemarks(remark);
		ch.setObjectValue(FuelOrderStatus._CANCELED_.getName());
		historyService.insert(ch);
	}

}
