package atraxo.ops.business.ext.fuelTicket.job.items;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;

import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.BatchInfo;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.DeliveryInfo;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.Density;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.FuelInfo;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.IntoPlane;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.Measurements;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.Meter;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.MeterReading;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.ObjectFactory;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.ProductInfo;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.Ticket;
import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.TicketBatch;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

public class ExportTicketItemProcessor implements ItemProcessor<List<FuelTicket>, TicketBatch> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExportTicketItemProcessor.class);

	private static final String EXTENSION = ".xml";
	private static final String SEPARATOR = "_";
	private static final String DATE_PATTERN = "yyyyMMdd";
	public static final String TICKET_IDS = "ticketIds";
	public static final String FILE_NAME = "fileName";
	public static final String TMP_FILE = "exportTicketTmpFile";

	private StepExecution stepExecution;

	@Override
	public TicketBatch process(List<FuelTicket> tickets) throws Exception {
		ObjectFactory factory = new ObjectFactory();
		TicketBatch tb = factory.createTicketBatch();
		String fileName = null;
		List<Object> ids = new ArrayList<>();
		for (FuelTicket ticket : tickets) {
			fileName = this.buildFileName(tickets);
			this.addBatchInfo(tb, ticket, tickets.size(), factory, fileName);
			this.addTicket(tb, ticket, factory);
			ids.add(ticket.getId());
		}
		if (fileName != null) {
			ExecutionContext context = this.stepExecution.getExecutionContext();
			context.putString(FILE_NAME, fileName);
			context.put(TICKET_IDS, ids);
		}
		return tb;
	}

	private String buildFileName(List<FuelTicket> tickets) {
		if (tickets.isEmpty()) {
			return "DEFAULT.xml";
		}
		FuelTicket ticket = tickets.get(0);
		StringBuilder sb = new StringBuilder();
		sb.append(ticket.getFueller().getCode());
		sb.append(SEPARATOR);
		sb.append(ticket.getDeparture().getCode());
		sb.append(SEPARATOR);
		String strDate = this.convertDate(ticket.getDeliveryDate());
		sb.append(strDate);
		sb.append(SEPARATOR);
		sb.append(String.format("%03d", this.stepExecution.getJobExecutionId()));
		sb.append(EXTENSION);
		return sb.toString();
	}

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;

	}

	private String convertDate(Date date) {
		DateFormat df1 = new SimpleDateFormat(DATE_PATTERN);
		String strDate = "";
		if (date != null) {
			strDate = df1.format(date);
		}
		return strDate;
	}

	private void addBatchInfo(TicketBatch tb, FuelTicket ticket, int ticketsNumber, ObjectFactory factory, String fileName) {
		if (tb.getBatchInfo() == null) {
			BatchInfo batchInfo = factory.createBatchInfo();
			batchInfo.setAirportCode(ticket.getDeparture().getCode());
			batchInfo.setIPACode(ticket.getFueller().getCode());
			// TODO format strings
			batchInfo.setReplyTo("TODO");
			batchInfo.setFromDate(this.convertDate(ticket.getFuelingStartDate()));
			batchInfo.setToDate(this.convertDate(ticket.getFuelingEndDate()));

			batchInfo.setTotalNumberOfTickets(ticketsNumber);
			batchInfo.setFeedName(fileName);
			tb.setBatchInfo(batchInfo);
		}

	}

	private void addTicket(TicketBatch tb, FuelTicket ticket, ObjectFactory factory) {
		Ticket fpTicket = factory.createTicket();
		fpTicket.setTicketNumber(ticket.getTicketNo());
		fpTicket.setDeliveryDate(this.convertDate(ticket.getDeliveryDate()));
		fpTicket.setAirportCode(ticket.getDeparture().getCode());
		fpTicket.setBuyer(ticket.getCustomer().getCode());
		fpTicket.setSupplier(ticket.getSupplier().getCode());
		fpTicket.setTicketAction(ticket.getTicketStatus().getCode());
		fpTicket.setProductInfo(this.createProductInfo(ticket, factory));
		fpTicket.setDeliveryInfo(this.createDeliveryInfo(ticket, factory));
		fpTicket.setMeasurements(this.createMeasurements(ticket, factory));
		fpTicket.setIPACode(ticket.getFueller().getCode());
		tb.getTicket().add(fpTicket);

	}

	private Measurements createMeasurements(FuelTicket ticket, ObjectFactory factory) {
		Measurements measurements = factory.createMeasurements();
		if (ticket.getDensity() != null && ticket.getDensityUnit() != null && ticket.getDensityVolume() != null) {
			measurements.setDensity(this.createDensity(ticket, factory));
		}
		measurements.setGrossQuantityDelivered(this.createGrossQuantity(ticket, factory));
		if (ticket.getNetUpliftQuantity() != null && !ticket.getNetUpliftQuantity().equals(BigDecimal.ZERO)) {
			measurements.setNetQuantityDelivered(this.createNetQuantity(ticket, factory));
		}
		measurements.setMeter(this.createMeter(ticket, factory));
		return measurements;
	}

	private Meter createMeter(FuelTicket ticket, ObjectFactory factory) {
		Meter meter = factory.createMeter();
		meter.setMeterReading1(this.createMeterReading(ticket, factory));
		return meter;
	}

	private MeterReading createMeterReading(FuelTicket ticket, ObjectFactory factory) {
		MeterReading reading = factory.createMeterReading();
		if (ticket.getBeforeFueling() != null) {
			reading.setMeterReadingBefore(ticket.getBeforeFueling().floatValue());
		}
		if (ticket.getAfterFueling() != null) {
			reading.setMeterReadingAfter(ticket.getAfterFueling().floatValue());
		}
		return reading;
	}

	private FuelInfo createNetQuantity(FuelTicket ticket, ObjectFactory factory) {
		FuelInfo info = factory.createFuelInfo();
		info.setFuelQuantity(ticket.getNetUpliftQuantity().floatValue());
		info.setFuelUnit(ticket.getNetUpliftUnit().getCode());
		return info;
	}

	private FuelInfo createGrossQuantity(FuelTicket ticket, ObjectFactory factory) {
		FuelInfo info = factory.createFuelInfo();
		info.setFuelQuantity(ticket.getUpliftVolume().floatValue());
		info.setFuelUnit(ticket.getUpliftUnit().getCode());
		return info;
	}

	private Density createDensity(FuelTicket ticket, ObjectFactory factory) {
		Density density = factory.createDensity();
		if (ticket.getDensity() != null) {
			density.setDensity(ticket.getDensity().floatValue());
		}
		density.setDensityUnit(ticket.getDensityUnit().getCode() + "/" + ticket.getDensityVolume().getCode());
		return density;
	}

	private DeliveryInfo createDeliveryInfo(FuelTicket ticket, ObjectFactory factory) {
		DeliveryInfo info = factory.createDeliveryInfo();
		info.setDeliveryType("IP");
		info.setFuelingOperation(ticket.getFuelingOperation().getCode());
		try {
			info.setFuelingType(ticket.getTransport().getCode());
		} catch (RuntimeException e) {
			LOGGER.warn("Got RuntimeException when setting fueling type, will do nothing ! ", e);
		}
		info.setIntoPlane(this.createIntoPlane(ticket, factory));
		return info;
	}

	private IntoPlane createIntoPlane(FuelTicket ticket, ObjectFactory factory) {
		IntoPlane intoPlane = factory.createIntoPlane();
		if (ticket.getRegistration() != null) {
			intoPlane.setAircraftRegistration(ticket.getRegistration().getRegistration());
		}
		intoPlane.setAircraftType(ticket.getAcType() == null ? "" : ticket.getAcType().getCode());
		intoPlane.setFinalDestination(ticket.getFinalDest() != null ? ticket.getFinalDest().getCode() : "");
		if (ticket.getAirlineDesignator() != null) {
			intoPlane.setFlightNumber(ticket.getAirlineDesignator() + ticket.getFlightNo());
		} else {
			intoPlane.setFlightNumber(ticket.getFlightNo());
		}

		intoPlane.setImmediateDestination(ticket.getDestination() == null ? "" : ticket.getDestination().getCode());
		return intoPlane;
	}

	private ProductInfo createProductInfo(FuelTicket ticket, ObjectFactory factory) {
		ProductInfo info = factory.createProductInfo();
		info.setProductType(ticket.getProductType().getName());
		info.setCustoms(ticket.getCustomsStatus().getCode());
		return info;
	}
}
