/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.flightEvent.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.tolerance.IToleranceService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.business.ext.tolerance.ToleranceResult;
import atraxo.fmbas.business.ext.tolerance.ToleranceVerifier;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.business.ext.exceptions.DuplicateFlightEventException;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.business.ext.fuelOrderLocation.delegate.FolFlightEventsStatus_Bd;
import atraxo.ops.business.ext.fuelOrderLocation.delegate.FuelOrderLocation_Bd;
import atraxo.ops.business.ext.fuelQuoteLocation.delegate.FuelQuoteLocation_Bd;
import atraxo.ops.business.ext.integration.FuelEventSource;
import atraxo.ops.domain.ext.DeliveryNoteMethod.DeliveryNoteMethod;
import atraxo.ops.domain.ext.flightEvent.AttachedItem;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FlightEventStatus;
import atraxo.ops.domain.impl.ops_type.FuelOrderStatus;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link FlightEvent} domain entity.
 */
public class FlightEvent_Service extends atraxo.ops.business.impl.flightEvent.FlightEvent_Service implements IFlightEventService {

	@Override
	public void updateWithoutBusiness(List<FlightEvent> list) throws BusinessException {
		for (FlightEvent e : list) {
			this.updateWithoutBusiness(e);
		}
	}

	@Override
	@Transactional
	public void updateWithoutBusiness(FlightEvent e) throws BusinessException {
		this.onUpdate(e);
	}

	@Override
	protected void preInsert(FlightEvent e) throws BusinessException {
		super.preInsert(e);
		this.checkIsUnique(e);
		this.calculateTotalQuantity(e);
		this.checkScheduledDates(e);
		if (this.isAssagnedToFuelRequest(e)) {
			this.checkFuelRequestFuelingingDate(e);
		}
	}

	private boolean isAssagnedToLocQuote(FlightEvent e) {
		return e.getLocQuote() != null;
	}

	private boolean isAssagnedToFuelRequest(FlightEvent e) {
		return e.getFuelRequest() != null;
	}

	private void checkIsUnique(FlightEvent e) throws DuplicateFlightEventException {
		if (!this.isUnique(e)) {
			throw new DuplicateFlightEventException("Duplicate business key for flight type!");
		}
	}

	private void sendMessage(FlightEvent e, DeliveryNoteMethod method) {
		if (e.getLocOrder() != null && e.getLocOrder().getFuelOrder().getStatus().equals(FuelOrderStatus._CONFIRMED_)) {
			FuelEventSource source = new FuelEventSource(e, method);
			this.sendMessage("maintainFuelEvents", source);
		}
	}

	private boolean isUnique(FlightEvent e) {
		Map<String, Object> params = new HashMap<>();
		params.put("operator", e.getOperator());
		params.put("upliftDate", e.getUpliftDate());
		params.put("registration", e.getRegistration());
		params.put("departure", e.getDeparture());
		List<FlightEvent> flightEvents = this.findEntitiesByAttributes(params);
		for (FlightEvent flightEvent : flightEvents) {
			if (e.getId() == null || (e.getId() != null && !e.getId().equals(flightEvent.getId()))) {
				if (e.getFuelRequest() == null || flightEvent.getFuelRequest() == null) {
					if (e.getLocQuote() != null && flightEvent.getLocQuote() != null
							&& e.getLocQuote().getId().equals(flightEvent.getLocQuote().getId())) {
						return false;
					}
				} else if (e.getLocQuote() == null) {
					if (flightEvent.getFuelRequest() != null && e.getFuelRequest().getId().equals(flightEvent.getFuelRequest().getId())) {
						return false;
					}
				} else if (flightEvent.getFuelRequest() != null && flightEvent.getLocQuote() != null
						&& e.getFuelRequest().getId().equals(flightEvent.getFuelRequest().getId())
						&& e.getLocQuote().getId().equals(flightEvent.getLocQuote().getId())) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void insert(List<FlightEvent> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void update(List<FlightEvent> list) throws BusinessException {
		super.update(list);
	}

	@Override
	protected void preUpdate(FlightEvent e) throws BusinessException {
		super.preUpdate(e);
		this.checkIsUnique(e);
		this.calculateTotalQuantity(e);
		this.checkScheduledDates(e);
		if (this.isAssagnedToLocQuote(e)) {
			this.checkContractPeriod(e);
		}
		if (this.isAssagnedToFuelRequest(e)) {
			this.checkFuelRequestFuelingingDate(e);
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		FlightEvent flightEvent = this.findById(ids.get(0));
		if (this.isAssagnedToLocQuote(flightEvent)) {
			FuelQuoteLocation_Bd bd = this.getBusinessDelegate(FuelQuoteLocation_Bd.class);
			bd.upadteOnFlightEventRemove(ids.toArray(new Integer[ids.size()]));
		}
		if (flightEvent.getLocOrder() != null) {
			FuelOrderLocation_Bd bd = this.getBusinessDelegate(FuelOrderLocation_Bd.class);
			bd.upadteOnFlightEventRemove(ids, flightEvent.getLocOrder());
		}
		for (Object id : ids) {
			flightEvent = this.findById(id);
			this.sendMessage(flightEvent, DeliveryNoteMethod.DELETE);
		}
	}

	@Override
	protected void postInsert(FlightEvent e) throws BusinessException {
		super.postInsert(e);
		this.checkTankCapacity(e);
		this.checkAndUpdateLocQuote(e);
		this.checkAndUpdateLocOrder(e);
	}

	@Override
	protected void postUpdate(FlightEvent e) throws BusinessException {
		super.postUpdate(e);
		this.checkTankCapacity(e);
		this.checkAndUpdateLocQuote(e);
		this.checkAndUpdateLocOrder(e);
	}

	private void checkAndUpdateLocQuote(FlightEvent e) throws BusinessException {
		if (this.isAssagnedToLocQuote(e)) {
			this.checkContractPeriod(e);
			this.updateFuelQuoteLocation(e);
		}
	}

	private void updateFuelQuoteLocation(FlightEvent e) throws BusinessException {
		FuelQuoteLocation_Bd bd = this.getBusinessDelegate(FuelQuoteLocation_Bd.class);
		bd.updateOnFlightEventChange(e.getLocQuote(), e);

	}

	private void checkAndUpdateLocOrder(FlightEvent e) throws BusinessException {
		if (e.getLocOrder() != null) {
			this.updateFuelOrderLocation(e);
		}
	}

	private void updateFuelOrderLocation(FlightEvent e) throws BusinessException {
		FuelOrderLocation_Bd bd = this.getBusinessDelegate(FuelOrderLocation_Bd.class);
		bd.updateOnFlightEventChange(e.getLocOrder(), e);

	}

	private void checkTankCapacity(FlightEvent e) throws BusinessException {
		if (e.getAircraftType() == null || e.getQuantity() == null || e.getUnit() == null) {
			return;
		}
		ISystemParameterService paramSrv = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		IUnitService unitSrv = (IUnitService) this.findEntityService(Unit.class);
		UnitConverterService unitConverterService = this.getApplicationContext().getBean(UnitConverterService.class);
		Unit sysUnit = unitSrv.findByCode(paramSrv.getSysVol());
		ToleranceVerifier verifier = new ToleranceVerifier(unitConverterService, this.getBusinessDelegate(ExchangeRate_Bd.class),
				this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity());
		IToleranceService tolService = (IToleranceService) this.findEntityService(Tolerance.class);
		Tolerance tolerance = tolService.getTankCapacityTolerance();
		ToleranceResult toleranceResult = verifier.checkHigherToleranceLimit(BigDecimal.valueOf(e.getAircraftType().getTankCapacity()),
				e.getQuantity(), sysUnit, e.getUnit(), null, null, tolerance, GregorianCalendar.getInstance().getTime());
		switch (toleranceResult.getType()) {
		case NOT_WITHIN:
			e.setToleranceMessage("Tank Capacity Overdue!");
			break;
		case WITHIN:
		case MARGIN:
		default:
			break;
		}

	}

	private void checkScheduledDates(FlightEvent e) throws BusinessException {
		this.verifyDates(e.getArrivalDate(), e.getDeparterDate(), OpsErrorCode.FLIGHT_EVENT_DATE1);
		this.verifyDates(e.getArrivalDate(), e.getUpliftDate(), OpsErrorCode.FLIGHT_EVENT_DATE2);
		this.verifyDates(e.getUpliftDate(), e.getDeparterDate(), OpsErrorCode.FLIGHT_EVENT_DATE3);
	}

	/**
	 * Verify the dates. Valid to must be greater than valid from.
	 *
	 * @param from
	 * @param to
	 * @param error
	 * @throws BusinessException
	 */
	public void verifyDates(Date from, Date to, OpsErrorCode error) throws BusinessException {
		if (from != null && to != null && from.after(to)) {
			throw new BusinessException(error, error.getErrMsg());
		}
	}

	private void checkContractPeriod(FlightEvent e) {
		FuelQuoteLocation locQuote = e.getLocQuote();
		Contract contract = locQuote.getContract();
		if (contract == null) {
			return;
		}
		if (contract.getValidFrom().compareTo(locQuote.getFuelReleaseStartsOn()) >= 0
				|| contract.getValidTo().compareTo(locQuote.getFuelReleaseEndsOn()) <= 0) {
			e.setToleranceMessage("Supplier contract validity period does not cover fuel release validity period.");
		}

	}

	private void checkFuelRequestFuelingingDate(FlightEvent e) throws BusinessException {
		FuelRequest fr = e.getFuelRequest();
		if (e.getUpliftDate().before(fr.getRequestDate())) {
			throw new BusinessException(OpsErrorCode.FUELING_DATE_FUEL_RECEIVING, OpsErrorCode.FUELING_DATE_FUEL_RECEIVING.getErrMsg());
		}
	}

	/**
	 *
	 */
	@Override
	@Transactional
	public void updateOrDelete(Integer id, AttachedItem attachedItem) throws BusinessException {
		FlightEvent e = this.findById(id);
		switch (attachedItem) {
		case FUEL_REQUEST:
			e.setFuelRequest(null);
			this.update(e);
			break;
		case FUEL_QUOTATION:
			FuelQuoteLocation_Bd bd = this.getBusinessDelegate(FuelQuoteLocation_Bd.class);
			bd.upadteOnFlightEventRemove(id);
			e.setLocQuote(null);
			this.update(e);
			break;
		}
		if (!this.isOrphan(e)) {
			this.delete(e);
		}
	}

	private boolean isOrphan(FlightEvent e) {
		return e.getLocQuote() != null || e.getFuelRequest() != null || e.getLocOrder() != null;
	}

	@Override
	public List<FlightEvent> findByFuelOrder(FuelOrder fuelOrder) throws BusinessException {
		List<FlightEvent> flightEvents = new ArrayList<>();
		if (fuelOrder == null || fuelOrder.getFuelQuote() == null || fuelOrder.getFuelQuote().getFuelQuoteLocations() == null) {
			return flightEvents;
		}
		Collection<FuelQuoteLocation> quotLocations = fuelOrder.getFuelQuote().getFuelQuoteLocations();
		for (FuelQuoteLocation fql : quotLocations) {
			flightEvents.addAll(fql.getFlightEvents());
		}
		return flightEvents;
	}

	@Override
	@Transactional
	@History(value = "Rescheduled", type = FlightEvent.class)
	public void reschedule(FlightEvent flightEvent, Date arrivalDate, Date departureDate, Date fuelingDate, Boolean timeReference,
			@Reason String remark) throws BusinessException {
		if (arrivalDate != null) {
			flightEvent.setArrivalDate(arrivalDate);
		}
		if (departureDate != null) {
			flightEvent.setDeparterDate(departureDate);
		}
		if (fuelingDate != null) {
			flightEvent.setUpliftDate(fuelingDate);
		}
		flightEvent.setTimeReference(timeReference);
		this.update(flightEvent);
		this.setLocationOrderPeriod(flightEvent);
	}

	/**
	 * Set if necessary the location order period.
	 *
	 * @param flightEvent
	 * @throws BusinessException
	 */
	private void setLocationOrderPeriod(FlightEvent flightEvent) throws BusinessException {
		FuelOrderLocation fol = flightEvent.getLocOrder();
		if (fol == null) {
			return;
		}
		Collection<FlightEvent> flightEvents = fol.getFlightEvents();
		if (this.setStartsEndsOfFuelLocation(flightEvents, fol)) {
			IFuelOrderLocationService folService = (IFuelOrderLocationService) this.findEntityService(FuelOrderLocation.class);
			folService.update(fol);
		}
	}

	/**
	 * @param {@link Collection}<{@link FlightEvent}>
	 * @param fol
	 * @return
	 */
	private boolean setStartsEndsOfFuelLocation(Collection<FlightEvent> flightEvents, FuelOrderLocation fol) {
		Date min = this.getMinDate(flightEvents, fol.getFuelReleaseStartsOn());
		Date max = this.getMaxDate(flightEvents, fol.getFuelReleaseEndsOn());
		if (min.before(fol.getFuelReleaseStartsOn())) {
			fol.setFuelReleaseStartsOn(min);
			return true;
		}
		if (max.after(fol.getFuelReleaseEndsOn())) {
			fol.setFuelReleaseEndsOn(max);
			return true;
		}
		return false;
	}

	/**
	 * Get the smallest uplift date from the list.
	 *
	 * @param - {@link Collection}<{@link FlightEvent}>
	 * @return - smallest {@link Date}.
	 */
	private Date getMinDate(Collection<FlightEvent> flightEvents, Date referenceDate) {
		Date retDate = (Date) referenceDate.clone();
		for (FlightEvent fe : flightEvents) {
			retDate = fe.getUpliftDate();
			if (retDate.after(fe.getUpliftDate())) {
				retDate = fe.getUpliftDate();
			}
		}
		return retDate;
	}

	/**
	 * Get the highest uplift date from the list.
	 *
	 * @param {@link Collection}<{@link FlightEvent}>
	 * @return - highest {@link Date}.
	 */
	private Date getMaxDate(Collection<FlightEvent> flightEvents, Date referenceDate) {
		Date retDate = (Date) referenceDate.clone();
		for (FlightEvent fe : flightEvents) {
			if (retDate.before(fe.getUpliftDate())) {
				retDate = fe.getUpliftDate();
			}
		}
		return retDate;
	}

	@Override
	@Transactional
	@History(type = FlightEvent.class)
	public void updateStatus(FlightEvent flightEvent, @Action String action, @Reason String remarks, String status) throws BusinessException {
		FlightEventStatus eventStatus = FlightEventStatus.getByName(status);
		flightEvent.setStatus(eventStatus);
		this.update(flightEvent);
		FuelOrder fuelOrder = flightEvent.getLocOrder().getFuelOrder();
		FolFlightEventsStatus_Bd folBd = this.getBusinessDelegate(FolFlightEventsStatus_Bd.class);
		if (eventStatus.equals(FlightEventStatus._CANCELED_)) {
			folBd.recalculateEvents(flightEvent, FlightEventStatus._CANCELED_);
			this.sendMessage(flightEvent, DeliveryNoteMethod.DELETE);
		}
		if (eventStatus.equals(FlightEventStatus._SCHEDULED_)) {
			folBd.recalculateEvents(flightEvent, FlightEventStatus._SCHEDULED_);
			if (fuelOrder.getStatus().equals(FuelOrderStatus._CONFIRMED_) || fuelOrder.getStatus().equals(FuelOrderStatus._RELEASED_)) {
				this.sendMessage(flightEvent, DeliveryNoteMethod.GENERATE);
			}
		}
	}

	/**
	 * Total quantity is no_events * quantity_per_event
	 *
	 * @param flightEvent
	 * @throws BusinessException
	 */
	private void calculateTotalQuantity(FlightEvent flightEvent) throws BusinessException {
		BigDecimal quantity = flightEvent.getQuantity();
		Integer events = flightEvent.getEvents();
		if (events == null || events < 1) {
			throw new BusinessException(OpsErrorCode.FLIGHT_EVENT_EVENT_NUMBER_ZERO, OpsErrorCode.FLIGHT_EVENT_EVENT_NUMBER_ZERO.getErrMsg());
		}
		if (quantity == null) {
			flightEvent.setTotalQuantity(null);
			return;
		}
		BigDecimal totalQuantity = quantity.multiply(BigDecimal.valueOf(events));
		flightEvent.setTotalQuantity(totalQuantity);
	}
}
