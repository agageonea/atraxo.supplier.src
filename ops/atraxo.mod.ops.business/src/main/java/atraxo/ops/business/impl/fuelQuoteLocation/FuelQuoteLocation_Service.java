/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelQuoteLocation;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.fuelQuoteLocation.CapturedPriceInformationResult;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelQuoteLocation} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelQuoteLocation_Service
		extends
			AbstractEntityService<FuelQuoteLocation> {

	/**
	 * Public constructor for FuelQuoteLocation_Service
	 */
	public FuelQuoteLocation_Service() {
		super();
	}

	/**
	 * Public constructor for FuelQuoteLocation_Service
	 * 
	 * @param em
	 */
	public FuelQuoteLocation_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelQuoteLocation> getEntityClass() {
		return FuelQuoteLocation.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelQuoteLocation
	 */
	public FuelQuoteLocation findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelQuoteLocation.NQ_FIND_BY_BUSINESS,
							FuelQuoteLocation.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelQuoteLocation", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelQuoteLocation", "id"), nure);
		}
	}

	/**
	 * Find by reference: quote
	 *
	 * @param quote
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByQuote(FuelQuotation quote) {
		return this.findByQuoteId(quote.getId());
	}
	/**
	 * Find by ID of reference: quote.id
	 *
	 * @param quoteId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByQuoteId(Integer quoteId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.quote.id = :quoteId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("quoteId", quoteId).getResultList();
	}
	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.location.id = :locationId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findBySupplier(Suppliers supplier) {
		return this.findBySupplierId(supplier.getId());
	}
	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findBySupplierId(Integer supplierId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.supplier.id = :supplierId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("supplierId", supplierId).getResultList();
	}
	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByContract(Contract contract) {
		return this.findByContractId(contract.getId());
	}
	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByContractId(Integer contractId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.contract.id = :contractId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractId", contractId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.unit.id = :unitId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: intoPlaneAgent
	 *
	 * @param intoPlaneAgent
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByIntoPlaneAgent(Suppliers intoPlaneAgent) {
		return this.findByIntoPlaneAgentId(intoPlaneAgent.getId());
	}
	/**
	 * Find by ID of reference: intoPlaneAgent.id
	 *
	 * @param intoPlaneAgentId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByIntoPlaneAgentId(
			Integer intoPlaneAgentId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.intoPlaneAgent.id = :intoPlaneAgentId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("intoPlaneAgentId", intoPlaneAgentId)
				.getResultList();
	}
	/**
	 * Find by reference: quotation
	 *
	 * @param quotation
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByQuotation(Quotation quotation) {
		return this.findByQuotationId(quotation.getId());
	}
	/**
	 * Find by ID of reference: quotation.id
	 *
	 * @param quotationId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByQuotationId(Integer quotationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.quotation.id = :quotationId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("quotationId", quotationId).getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.currency.id = :currencyId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByAverageMethod(
			AverageMethod averageMethod) {
		return this.findByAverageMethodId(averageMethod.getId());
	}
	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByAverageMethodId(Integer averageMethodId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.averageMethod.id = :averageMethodId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("averageMethodId", averageMethodId)
				.getResultList();
	}
	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByFinancialSource(
			FinancialSources financialSource) {
		return this.findByFinancialSourceId(financialSource.getId());
	}
	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByFinancialSourceId(
			Integer financialSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.financialSource.id = :financialSourceId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("financialSourceId", financialSourceId)
				.getResultList();
	}
	/**
	 * Find by reference: pricingPolicy
	 *
	 * @param pricingPolicy
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByPricingPolicy(PricePolicy pricingPolicy) {
		return this.findByPricingPolicyId(pricingPolicy.getId());
	}
	/**
	 * Find by ID of reference: pricingPolicy.id
	 *
	 * @param pricingPolicyId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByPricingPolicyId(Integer pricingPolicyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuoteLocation e where e.clientId = :clientId and e.pricingPolicy.id = :pricingPolicyId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("pricingPolicyId", pricingPolicyId)
				.getResultList();
	}
	/**
	 * Find by reference: flightEvents
	 *
	 * @param flightEvents
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByFlightEvents(FlightEvent flightEvents) {
		return this.findByFlightEventsId(flightEvents.getId());
	}
	/**
	 * Find by ID of reference: flightEvents.id
	 *
	 * @param flightEventsId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByFlightEventsId(Integer flightEventsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelQuoteLocation e, IN (e.flightEvents) c where e.clientId = :clientId and c.id = :flightEventsId",
						FuelQuoteLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("flightEventsId", flightEventsId).getResultList();
	}
}
