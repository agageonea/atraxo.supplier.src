package atraxo.ops.business.ext.bpm.delegate.fuelticket.cancel;

import java.util.Arrays;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.ext.fuelTicket.CancelFuelTicketResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

public class FuelTicketCancelDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(FuelTicketCancelDelegate.class);

	@Autowired
	private IFuelTicketService fuelTicketService;

	@Override
	public void execute() throws Exception {
		Object fuelTicketIdObj = this.execution.getVariable(WorkflowVariablesConstants.FUEL_TICKET_ID_VAR);
		String remark = (String) this.execution.getVariable(WorkflowVariablesConstants.TASK_NOTE_VAR);

		if (fuelTicketIdObj != null) {
			try {
				FuelTicket ticket = this.fuelTicketService.findById(fuelTicketIdObj);
				CancelFuelTicketResult result = this.fuelTicketService.cancelFuelTicket(Arrays.asList(ticket), remark);
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonString = objectMapper.writeValueAsString(result);

				this.execution.setVariable(WorkflowVariablesConstants.NOTIFICATION_METHOD_NAME, "cancelFuelTicket");
				this.execution.setVariable(WorkflowVariablesConstants.NOTIFICATION_METHOD_PARAM, jsonString);

				this.execution.setVariable(WorkflowVariablesConstants.TASK_ACCEPTANCE_VAR, true);
			} catch (Exception e) {
				LOGGER.error("Could not cancel fuel ticket", e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_RELEASE, e.getMessage(), e);
			}
		}
	}

	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}

}
