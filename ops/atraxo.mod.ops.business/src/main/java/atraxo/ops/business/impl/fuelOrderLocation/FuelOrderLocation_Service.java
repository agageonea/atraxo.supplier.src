/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelOrderLocation;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.fuelOrderLocation.FuelOrderLocationMessage;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelOrderLocation} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelOrderLocation_Service
		extends
			AbstractEntityService<FuelOrderLocation> {

	/**
	 * Public constructor for FuelOrderLocation_Service
	 */
	public FuelOrderLocation_Service() {
		super();
	}

	/**
	 * Public constructor for FuelOrderLocation_Service
	 * 
	 * @param em
	 */
	public FuelOrderLocation_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelOrderLocation> getEntityClass() {
		return FuelOrderLocation.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelOrderLocation
	 */
	public FuelOrderLocation findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelOrderLocation.NQ_FIND_BY_BUSINESS,
							FuelOrderLocation.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelOrderLocation", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelOrderLocation", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelOrder
	 *
	 * @param fuelOrder
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFuelOrder(FuelOrder fuelOrder) {
		return this.findByFuelOrderId(fuelOrder.getId());
	}
	/**
	 * Find by ID of reference: fuelOrder.id
	 *
	 * @param fuelOrderId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFuelOrderId(Integer fuelOrderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.fuelOrder.id = :fuelOrderId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelOrderId", fuelOrderId).getResultList();
	}
	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.location.id = :locationId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findBySupplier(Suppliers supplier) {
		return this.findBySupplierId(supplier.getId());
	}
	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findBySupplierId(Integer supplierId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.supplier.id = :supplierId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("supplierId", supplierId).getResultList();
	}
	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByContract(Contract contract) {
		return this.findByContractId(contract.getId());
	}
	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByContractId(Integer contractId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.contract.id = :contractId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contractId", contractId).getResultList();
	}
	/**
	 * Find by reference: iplAgent
	 *
	 * @param iplAgent
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByIplAgent(Suppliers iplAgent) {
		return this.findByIplAgentId(iplAgent.getId());
	}
	/**
	 * Find by ID of reference: iplAgent.id
	 *
	 * @param iplAgentId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByIplAgentId(Integer iplAgentId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.iplAgent.id = :iplAgentId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("iplAgentId", iplAgentId).getResultList();
	}
	/**
	 * Find by reference: paymentCurrency
	 *
	 * @param paymentCurrency
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPaymentCurrency(
			Currencies paymentCurrency) {
		return this.findByPaymentCurrencyId(paymentCurrency.getId());
	}
	/**
	 * Find by ID of reference: paymentCurrency.id
	 *
	 * @param paymentCurrencyId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPaymentCurrencyId(
			Integer paymentCurrencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.paymentCurrency.id = :paymentCurrencyId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("paymentCurrencyId", paymentCurrencyId)
				.getResultList();
	}
	/**
	 * Find by reference: paymentUnit
	 *
	 * @param paymentUnit
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPaymentUnit(Unit paymentUnit) {
		return this.findByPaymentUnitId(paymentUnit.getId());
	}
	/**
	 * Find by ID of reference: paymentUnit.id
	 *
	 * @param paymentUnitId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPaymentUnitId(Integer paymentUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.paymentUnit.id = :paymentUnitId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("paymentUnitId", paymentUnitId).getResultList();
	}
	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFinancialSource(
			FinancialSources financialSource) {
		return this.findByFinancialSourceId(financialSource.getId());
	}
	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFinancialSourceId(
			Integer financialSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.financialSource.id = :financialSourceId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("financialSourceId", financialSourceId)
				.getResultList();
	}
	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByAverageMethod(
			AverageMethod averageMethod) {
		return this.findByAverageMethodId(averageMethod.getId());
	}
	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByAverageMethodId(Integer averageMethodId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.averageMethod.id = :averageMethodId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("averageMethodId", averageMethodId)
				.getResultList();
	}
	/**
	 * Find by reference: pricePolicy
	 *
	 * @param pricePolicy
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPricePolicy(PricePolicy pricePolicy) {
		return this.findByPricePolicyId(pricePolicy.getId());
	}
	/**
	 * Find by ID of reference: pricePolicy.id
	 *
	 * @param pricePolicyId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPricePolicyId(Integer pricePolicyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrderLocation e where e.clientId = :clientId and e.pricePolicy.id = :pricePolicyId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("pricePolicyId", pricePolicyId).getResultList();
	}
	/**
	 * Find by reference: flightEvents
	 *
	 * @param flightEvents
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFlightEvents(FlightEvent flightEvents) {
		return this.findByFlightEventsId(flightEvents.getId());
	}
	/**
	 * Find by ID of reference: flightEvents.id
	 *
	 * @param flightEventsId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFlightEventsId(Integer flightEventsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelOrderLocation e, IN (e.flightEvents) c where e.clientId = :clientId and c.id = :flightEventsId",
						FuelOrderLocation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("flightEventsId", flightEventsId).getResultList();
	}
}
