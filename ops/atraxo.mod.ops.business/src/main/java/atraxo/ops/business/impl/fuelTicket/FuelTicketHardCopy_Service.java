/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelTicket;

import atraxo.ops.business.api.fuelTicket.IFuelTicketHardCopyService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.fuelTicket.FuelTicketHardCopy;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelTicketHardCopy} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelTicketHardCopy_Service
		extends
			AbstractEntityService<FuelTicketHardCopy>
		implements
			IFuelTicketHardCopyService {

	/**
	 * Public constructor for FuelTicketHardCopy_Service
	 */
	public FuelTicketHardCopy_Service() {
		super();
	}

	/**
	 * Public constructor for FuelTicketHardCopy_Service
	 * 
	 * @param em
	 */
	public FuelTicketHardCopy_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelTicketHardCopy> getEntityClass() {
		return FuelTicketHardCopy.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTicketHardCopy
	 */
	public FuelTicketHardCopy findByBk(FuelTicket fuelTicket) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelTicketHardCopy.NQ_FIND_BY_BK,
							FuelTicketHardCopy.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("fuelTicket", fuelTicket).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTicketHardCopy", "fuelTicket"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTicketHardCopy", "fuelTicket"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTicketHardCopy
	 */
	public FuelTicketHardCopy findByBk(Long fuelTicketId) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							FuelTicketHardCopy.NQ_FIND_BY_BK_PRIMITIVE,
							FuelTicketHardCopy.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("fuelTicketId", fuelTicketId)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTicketHardCopy", "fuelTicketId"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTicketHardCopy", "fuelTicketId"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTicketHardCopy
	 */
	public FuelTicketHardCopy findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelTicketHardCopy.NQ_FIND_BY_BUSINESS,
							FuelTicketHardCopy.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTicketHardCopy", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTicketHardCopy", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelTicket
	 *
	 * @param fuelTicket
	 * @return List<FuelTicketHardCopy>
	 */
	public List<FuelTicketHardCopy> findByFuelTicket(FuelTicket fuelTicket) {
		return this.findByFuelTicketId(fuelTicket.getId());
	}
	/**
	 * Find by ID of reference: fuelTicket.id
	 *
	 * @param fuelTicketId
	 * @return List<FuelTicketHardCopy>
	 */
	public List<FuelTicketHardCopy> findByFuelTicketId(Integer fuelTicketId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicketHardCopy e where e.clientId = :clientId and e.fuelTicket.id = :fuelTicketId",
						FuelTicketHardCopy.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelTicketId", fuelTicketId).getResultList();
	}
}
