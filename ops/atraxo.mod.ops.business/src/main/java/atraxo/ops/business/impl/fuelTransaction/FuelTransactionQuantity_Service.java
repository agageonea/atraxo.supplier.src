/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelTransaction;

import atraxo.ops.business.api.fuelTransaction.IFuelTransactionQuantityService;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelTransactionQuantity} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelTransactionQuantity_Service
		extends
			AbstractEntityService<FuelTransactionQuantity>
		implements
			IFuelTransactionQuantityService {

	/**
	 * Public constructor for FuelTransactionQuantity_Service
	 */
	public FuelTransactionQuantity_Service() {
		super();
	}

	/**
	 * Public constructor for FuelTransactionQuantity_Service
	 * 
	 * @param em
	 */
	public FuelTransactionQuantity_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelTransactionQuantity> getEntityClass() {
		return FuelTransactionQuantity.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionQuantity
	 */
	public FuelTransactionQuantity findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							FuelTransactionQuantity.NQ_FIND_BY_BUSINESS,
							FuelTransactionQuantity.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTransactionQuantity", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTransactionQuantity", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelTransaction
	 *
	 * @param fuelTransaction
	 * @return List<FuelTransactionQuantity>
	 */
	public List<FuelTransactionQuantity> findByFuelTransaction(
			FuelTransaction fuelTransaction) {
		return this.findByFuelTransactionId(fuelTransaction.getId());
	}
	/**
	 * Find by ID of reference: fuelTransaction.id
	 *
	 * @param fuelTransactionId
	 * @return List<FuelTransactionQuantity>
	 */
	public List<FuelTransactionQuantity> findByFuelTransactionId(
			Integer fuelTransactionId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTransactionQuantity e where e.clientId = :clientId and e.fuelTransaction.id = :fuelTransactionId",
						FuelTransactionQuantity.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelTransactionId", fuelTransactionId)
				.getResultList();
	}
}
