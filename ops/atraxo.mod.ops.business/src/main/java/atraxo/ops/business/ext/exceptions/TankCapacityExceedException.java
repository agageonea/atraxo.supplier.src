package atraxo.ops.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class TankCapacityExceedException extends BusinessException {

	public TankCapacityExceedException() {
		super(OpsErrorCode.TANK_CAPACITY_EXCEED, OpsErrorCode.TANK_CAPACITY_EXCEED.getErrMsg());
	}

}
