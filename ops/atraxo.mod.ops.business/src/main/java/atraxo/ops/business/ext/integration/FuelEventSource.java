package atraxo.ops.business.ext.integration;

import java.util.List;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.ops.domain.ext.DeliveryNoteMethod.DeliveryNoteMethod;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;

public class FuelEventSource {

	private final Object source;
	private final List<Contract> sellContracts;
	private final Contract buyContract;
	private final FuelOrderLocation fol;
	private final DeliveryNoteMethod method;

	public FuelEventSource(Object source, FuelOrderLocation fol, DeliveryNoteMethod method) {
		this.source = source;
		this.fol = fol;
		this.sellContracts = null;
		this.buyContract = null;
		this.method = method;
	}

	public FuelEventSource(Object source, List<Contract> sellContracts, Contract buyContract, DeliveryNoteMethod method) {
		this.source = source;
		this.fol = null;
		this.sellContracts = sellContracts;
		this.buyContract = buyContract;
		this.method = method;
	}

	public FuelEventSource(Object source, List<Contract> sellContracts, DeliveryNoteMethod method) {
		this(source, sellContracts, null, method);
	}

	public FuelEventSource(Object source, DeliveryNoteMethod method) {
		this(source, null, null, method);
	}

	public Object getSource() {
		return this.source;
	}

	public List<Contract> getSellContracts() {
		return this.sellContracts;
	}

	public Contract getBuyContract() {
		return this.buyContract;
	}

	public FuelOrderLocation getFol() {
		return this.fol;
	}

	public DeliveryNoteMethod getMethod() {
		return this.method;
	}

}
