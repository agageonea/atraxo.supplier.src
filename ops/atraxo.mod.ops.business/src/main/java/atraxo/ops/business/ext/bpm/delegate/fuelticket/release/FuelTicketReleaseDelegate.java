package atraxo.ops.business.ext.bpm.delegate.fuelticket.release;

import java.util.Arrays;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author abolindu
 */
public class FuelTicketReleaseDelegate extends AbstractJavaDelegate {

	@Autowired
	private IFuelTicketService fuelTicketService;

	@Override
	public void execute() throws Exception {
		Object fuelTicketIdObj = this.execution.getVariable(WorkflowVariablesConstants.FUEL_TICKET_ID_VAR);
		if (fuelTicketIdObj != null) {
			FuelTicket ticket = this.fuelTicketService.findById(fuelTicketIdObj);

			try {
				this.fuelTicketService.releaseFuelTicket(Arrays.asList(ticket));
				this.fuelTicketService.exportFuelEvents(Arrays.asList(ticket));
			} catch (Exception e) {
				if (e.getCause() instanceof PersistenceException) {
					this.fuelTicketService.insertToHistory(ticket, WorkflowMsgConstants.WKF_RESULT_APPROVE_FAILED,
							BusinessErrorCode.DUPLICATE_ENTITY_GENERAL.getErrMsg());
				} else {
					this.fuelTicketService.insertToHistory(ticket, WorkflowMsgConstants.WKF_RESULT_APPROVE_FAILED, e.getMessage());
				}

				if (Boolean.valueOf(this.execution.getVariable(WorkflowVariablesConstants.VAR_AUTOMATIC_TERMINATE_FAILED_WORKFLOWS).toString())) {
					this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_RELEASE, e.getMessage(), e);
					this.fuelTicketService.rejectFuelTicket(Arrays.asList(this.getFuelTicket(ticket.getId())),
							WorkflowMsgConstants.WKF_RESULT_APPROVE_FAILED);
				} else {
					if (e.getCause() instanceof PersistenceException) {
						throw new BusinessException(BusinessErrorCode.DUPLICATE_ENTITY_GENERAL,
								BusinessErrorCode.DUPLICATE_ENTITY_GENERAL.getErrMsg(), e);
					} else {
						throw e;
					}
				}
			}

			this.execution.setVariable(WorkflowVariablesConstants.TASK_ACCEPTANCE_VAR,
					this.fuelTicketService.getFuelTicketApprovalStatusUpdated(fuelTicketIdObj));
			this.execution.setVariable(WorkflowVariablesConstants.TASK_NOTE_VAR, ticket.getTicketNo());
		}
	}

	/**
	 * @param id
	 * @return
	 */
	private FuelTicket getFuelTicket(Integer id) {
		return this.fuelTicketService.findById(id);
	}

	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}
}
