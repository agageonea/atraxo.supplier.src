package atraxo.ops.business.ext.bpm.delegate.fuelticket.approval;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

/**
 * Approval delegate responsible with checking the RESTRICTBLOCKED sys param and extracting (if the case) approval roles for first level of approval
 *
 * @author vhojda
 */
public class FuelTicketFirstStageApprovalDelegate extends FuelTicketStageApprovalDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(FuelTicketFirstStageApprovalDelegate.class);

	@Override
	public void execute() throws Exception {
		// first, assume the next approval stage is NOT required
		this.setNextApprovalStageRequiredVariable(false);
		// extract Fuel Ticket
		FuelTicket fuelTicket = this.extractFuelTicket();
		// check if customer is blocked
		if (fuelTicket != null && fuelTicket.getCustomer().getStatus().equals(CustomerStatus._BLOCKED_)) {

			// check if there are approver roles defined and available
			WorkflowParameter param = this.workflowParameterService.findByKey(Long.valueOf(this.workflowId),
					WorkflowVariablesConstants.BLOCKED_CUSTOMER_APPROVER_ROLE);
			List<UserSupp> users = this.extractApprovers(WorkflowVariablesConstants.BLOCKED_CUSTOMER_APPROVER_ROLE, param.getMandatory());
			if (users != null) {
				// extract email data
				EmailDto emailDto = this.extractEmailDto(fuelTicket, users, true);
				if (emailDto != null) {
					this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDto);
					// next approval stage is required
					this.setNextApprovalStageRequiredVariable(true);
				}
			}

		}

		// make sure to reset the mail components
		this.resetMailComponentVariables();
	}

}
