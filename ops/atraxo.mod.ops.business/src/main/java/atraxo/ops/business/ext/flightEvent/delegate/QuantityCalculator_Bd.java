package atraxo.ops.business.ext.flightEvent.delegate;

import java.math.BigDecimal;
import java.util.Collection;

import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.ops_type.FlightEventStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class QuantityCalculator_Bd extends AbstractBusinessDelegate {

	public BigDecimal calculateQuantity(Collection<FlightEvent> flightEvents, Unit paymentUnit) throws BusinessException {
		BigDecimal retVal = BigDecimal.ZERO;
		UnitConverterService unitConverterService = this.getApplicationContext().getBean(UnitConverterService.class);
		double density = this.getSysDensity();
		for (FlightEvent fe : flightEvents) {
			if (!FlightEventStatus._CANCELED_.equals(fe.getStatus())) {
				BigDecimal fuelQty = fe.getQuantity() != null ? fe.getQuantity() : BigDecimal.ZERO;
				BigDecimal convertedQuantity = unitConverterService.convert(fe.getUnit(), paymentUnit, fuelQty, density);
				retVal = retVal.add(convertedQuantity);
			}
		}
		return retVal;
	}

	private double getSysDensity() throws BusinessException {
		ISystemParameterService service = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		return Double.parseDouble(service.getDensity());
	}

}
