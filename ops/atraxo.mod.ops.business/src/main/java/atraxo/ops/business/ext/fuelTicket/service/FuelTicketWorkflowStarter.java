package atraxo.ops.business.ext.fuelTicket.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.CollectionUtils;

import atraxo.ad.business.api.system.IClientService;
import atraxo.ad.domain.impl.system.Client;
import atraxo.fmbas.business.api.ext.bpm.IWorkflowBpmManager;
import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.ops.business.api.ext.fuelTicket.IFuelTicketWorkflowStarter;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.domain.ext.fuelTicketValidator.FuelTicketWorkflowStarterValidatorResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.service.IClientInfoProvider;
import seava.j4e.api.session.IClient;
import seava.j4e.api.session.IOrganization;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IUserSettings;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.AbstractBusinessBaseService;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;

/**
 * Spring bean responsible with starting Fuel Ticket Approval Workflow from different parts of the application.
 *
 * @author vhojda
 */
public class FuelTicketWorkflowStarter extends AbstractBusinessBaseService implements IFuelTicketWorkflowStarter {

	private static final Logger LOGGER = LoggerFactory.getLogger(FuelTicketWorkflowStarter.class);

	@Autowired
	private IWorkflowBpmManager workflowBpmManager;
	@Autowired
	private IFuelTicketService ftSrv;
	@Autowired
	private ISystemParameterService sysParamSrv;
	@Autowired
	private FuelTicketWorkflowStarterValidator fuelTicketWorkflowStarterValidator;

	/**
	 * Cancel Fuel Ticket within an workflow so the subsidiaryId to be ignored
	 *
	 * @param fuelTickets
	 * @return
	 * @throws BusinessException
	 * @throws InterruptedException
	 */
	@Override
	public WorkflowStartResult startFuelTicketCancelWorkflow(FuelTicket fuelTicket, String remark) throws BusinessException {
		LOGGER.info("Starting fuel ticket cancel workflow.");
		Map<String, Object> vars = new HashMap<>();

		vars.put(WorkflowVariablesConstants.FUEL_TICKET_ID_VAR, fuelTicket.getId());
		vars.put(WorkflowVariablesConstants.TASK_NOTE_VAR, remark);
		vars.put(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME, Session.user.get().getCode());

		vars.put(WorkflowVariablesConstants.NOTIFICATION_DETAILS, true);
		vars.put(WorkflowVariablesConstants.NOTIFICATION_DETAILS_ENTITY_CODE, fuelTicket.getTicketNo());
		vars.put(WorkflowVariablesConstants.NOTIFICATION_DETAILS_ENTITY_NAME, fuelTicket.getClass().getSimpleName());

		return this.workflowBpmManager.startWorkflow(WorkflowNames.FUEL_TICKET_CANCEL.getWorkflowName(), vars, fuelTicket.getSubsidiaryId(),
				fuelTicket);
	}

	/**
	 * Approve Fuel Ticket within an workflow so the subsidiaryId to be ignored (basically, the release operation)
	 *
	 * @param fuelTickets
	 * @param validationDone true if validation was performed already, false otherwise
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public WorkflowStartResult startFuelTicketReleaseWorkflow(List<FuelTicket> fuelTickets, boolean validationDone) throws BusinessException {
		LOGGER.info("Starting fuel ticket release workflow.");
		WorkflowStartResult result = new WorkflowStartResult();
		int submittedOk = 0;

		for (FuelTicket fuelTicket : fuelTickets) {

			// check if validation is needed
			boolean valid = true;
			FuelTicketWorkflowStarterValidatorResult validationResult = new FuelTicketWorkflowStarterValidatorResult();
			if (!validationDone) {
				// validate again the start of the workflow
				validationResult = this.fuelTicketWorkflowStarterValidator.validate(fuelTicket, WorkflowNames.FUEL_TICKET_RELEASE);
				valid = validationResult.isValid();
			}

			if (valid) {
				// if valid proceed to start the workflow

				Map<String, Object> vars = new HashMap<>();

				vars.put(WorkflowVariablesConstants.FUEL_TICKET_ID_VAR, fuelTicket.getId());
				vars.put(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME, Session.user.get().getCode());

				result = this.workflowBpmManager.startWorkflow(WorkflowNames.FUEL_TICKET_RELEASE.getWorkflowName(), vars,
						fuelTicket.getSubsidiaryId(), fuelTicket);

				if (result.isStarted()) {
					submittedOk++;
				} else {
					if (!result.getReason().contains(BusinessErrorCode.DUPLICATE_ENTITY_GENERAL.getErrMsg())) {
						// submit for approval for the entity(change status, fill in History)
						FuelTicket fuelTicketAfterStart = this.ftSrv.findById(fuelTicket.getId());
						this.ftSrv.setFuelTicketApprovalStatus(fuelTicketAfterStart, result.getReason(),
								WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_NOT_OK, FuelTicketApprovalStatus._FOR_APPROVAL_);
					}

					if (result.getReason().contains(BusinessErrorCode.THREAD_START_NO_RESOURCES.getErrMsg())) {
						// quickfix for #5642
						WorkflowInstance wkfInstance = this.workflowBpmManager.getWorkflowInstanceByEntity(WorkflowNames.FUEL_TICKET_RELEASE,
								fuelTicket.getId(), FuelTicket.class.getSimpleName());
						if (wkfInstance != null) {
							this.workflowBpmManager.terminateWorkflow(wkfInstance.getId(), result.getReason());
						}
					}
				}
			} else {
				// if not valid, write down in the history of ticket that it couldn't be released
				this.ftSrv.setFuelTicketApprovalStatus(fuelTicket, validationResult.getValidationMessage(), WorkflowMsgConstants.RELEASE_FAILED,
						FuelTicketApprovalStatus._FOR_APPROVAL_);
			}
		}

		result.setNumberOfSuccesInstances(submittedOk);
		return result;
	}

	@Async
	@Override
	public void startFuelTicketWorkflowAsync(Set<String> fuelTicketRefIds, String userCode, String clientId, List<IOrganization> organizations)
			throws BusinessException {
		LOGGER.info("Starting fuel ticket approve workflow.");
		if (!CollectionUtils.isEmpty(fuelTicketRefIds)) {
			this.createSessionUser(userCode, clientId, organizations);
			List<FuelTicket> fuelTickets = this.ftSrv.findByRefids(new ArrayList<>(fuelTicketRefIds));
			this.startFuelTicketWorkflow(fuelTickets);
		}
	}

	private void createSessionUser(String userCode, String clientId, List<IOrganization> organizations) throws BusinessException {
		try {
			LOGGER.info("Creating session user.");
			IClientService clientSrv = (IClientService) this.findEntityService(Client.class);
			Client c = clientSrv.findById(clientId);
			IClient client = new AppClient(c.getId(), c.getCode(), "");
			IUserSettings settings;
			settings = AppUserSettings.newInstance(this.getSettings());
			IUserProfile profile = new AppUserProfile(true, null, false, false, false); // create an incomplete user first
			profile.setOrganisation(organizations);
			IUser user = new AppUser(userCode, userCode, null, userCode, null, null, client, settings, profile, null, true);
			Session.user.set(user); // get the client workspace info
			IWorkspace ws = this.getApplicationContext().getBean(IClientInfoProvider.class).getClientWorkspace();
			user = new AppUser(userCode, userCode, userCode, userCode, null, null, client, settings, profile, ws, true);
			Session.user.set(user);
		} catch (InvalidConfiguration e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}

	/**
	 * @param fuelTickets
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public void startFuelTicketWorkflow(List<FuelTicket> fuelTickets) throws BusinessException {
		if (this.sysParamSrv.getFuelTicketApprovalWorkflow()) {
			String note = OpsErrorCode.FT_WKF_PROCESS_BUFFERED_REASON.getErrMsg();
			for (FuelTicket fuelTicket : fuelTickets) {
				this.startFuelTicketApprovalWorkflow(fuelTicket, note, null, false);
			}
		} else {
			this.startFuelTicketReleaseWorkflow(fuelTickets, false);
		}
	}

	/**
	 * Starts the one of the Fuel Ticket workflows depending on the given parameters
	 *
	 * @param fuelTicket
	 * @param note
	 * @param attachmentsArray the array of attachments
	 * @param validationDone true if validation was performed already, false otherwise
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public WorkflowStartResult startFuelTicketApprovalWorkflow(FuelTicket fuelTicket, String note, String attachmentsArray, boolean validationDone)
			throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("START startWorkflowFuelTicketApproval()");
		}
		LOGGER.info("Starting fuel ticket approval workflow.");
		WorkflowStartResult result = new WorkflowStartResult();

		FuelTicketWorkflowStarterValidatorResult validationResult = new FuelTicketWorkflowStarterValidatorResult();

		// check if validation is needed
		boolean valid = true;
		if (!validationDone) {
			// validate again the start of the workflow
			validationResult = this.fuelTicketWorkflowStarterValidator.validate(fuelTicket, WorkflowNames.FUEL_TICKET_APPROVAL);
			valid = validationResult.isValid();
		}

		if (valid) {
			// submit for approval for the entity(change status, fill in History)
			this.ftSrv.setFuelTicketApprovalStatus(fuelTicket, note, WorkflowMsgConstants.MSG_STARTED_FOR_APPROVAL_OK,
					FuelTicketApprovalStatus._AWAITING_APPROVAL_);

			// start workflow after adding workflow parameters
			Map<String, Object> vars = this.getWorkflowVariablesMap(note, attachmentsArray, fuelTicket);
			result = this.workflowBpmManager.startWorkflow(WorkflowNames.FUEL_TICKET_APPROVAL.getWorkflowName(), vars, fuelTicket.getSubsidiaryId(),
					fuelTicket);
		} else {
			result.setStarted(false);
			result.setReason(validationResult.getValidationMessage());
		}

		if (!result.isStarted()) {
			// submit for approval for the entity(change status, fill in History)
			FuelTicket fuelTicketAfterStart = this.ftSrv.findById(fuelTicket.getId());
			this.ftSrv.setFuelTicketApprovalStatus(fuelTicketAfterStart, result.getReason(), WorkflowMsgConstants.SUBMIT_FOR_APPROVAL_FAILED,
					FuelTicketApprovalStatus._FOR_APPROVAL_);
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("END startWorkflowFuelTicketApproval()");
		}
		return result;
	}

	/**
	 * @param note
	 * @param attachmentsArray
	 * @param fuelTicket
	 * @return
	 */
	private Map<String, Object> getWorkflowVariablesMap(String note, String attachmentsArray, FuelTicket fuelTicket) {
		LOGGER.info("Getting workflow's parameters.");
		Map<String, Object> vars = new HashMap<>();

		vars.put(WorkflowVariablesConstants.VAR_WKF_ENTITY_DESCRIPTION,
				"Fuel ticket " + fuelTicket.getTicketNo() + " at " + fuelTicket.getDeparture().getName() + " for "
						+ fuelTicket.getCustomer().getName() + " from " + fuelTicket.getSupplier().getName() + " awaiting your approval");
		vars.put(WorkflowVariablesConstants.FUEL_TICKET_ID_VAR, fuelTicket.getId());
		vars.put(WorkflowVariablesConstants.APPROVE_NOTE, note);
		vars.put(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR, this.getFullNameOfRequester());

		if (!StringUtils.isEmpty(attachmentsArray)) {
			String attachTrimmed = attachmentsArray.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").trim();
			if (!StringUtils.isEmpty(attachTrimmed)) {
				String[] attachmentIdentifiers = attachTrimmed.split(",");
				vars.put(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS, Arrays.asList(attachmentIdentifiers));
			}
		}
		return vars;
	}

	/**
	 * @return
	 */
	private String getFullNameOfRequester() {
		String fullName = "";
		IUserSuppService userService = this.getApplicationContext().getBean(IUserSuppService.class);
		try {
			UserSupp user = userService.findByLogin(Session.user.get().getLoginName());
			fullName = user.getFirstName() + " " + user.getLastName();
		} catch (Exception e) {
			LOGGER.warn("Could not retrieve current user fullname from current Session!", e);
			fullName = Session.user.get().getCode();
		}
		return fullName;
	}

	/**
	 * @param fuelTicket
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public FuelTicketWorkflowStarterValidatorResult validateWorkflowStart(FuelTicket fuelTicket) throws Exception {
		if (this.sysParamSrv.getFuelTicketApprovalWorkflow()) {
			return this.validateWorkflowStart(fuelTicket, WorkflowNames.FUEL_TICKET_APPROVAL);
		} else {
			return this.validateWorkflowStart(fuelTicket, WorkflowNames.FUEL_TICKET_RELEASE);
		}
	}

	/**
	 * @param fuelTicket
	 * @param workflowName
	 * @return
	 * @throws Exception
	 */
	@Override
	public FuelTicketWorkflowStarterValidatorResult validateWorkflowStart(FuelTicket fuelTicket, WorkflowNames workflowName) throws Exception {
		return this.fuelTicketWorkflowStarterValidator.validate(fuelTicket, workflowName);
	}

}
