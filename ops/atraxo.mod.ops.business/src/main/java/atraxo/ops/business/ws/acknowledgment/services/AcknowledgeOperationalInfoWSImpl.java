package atraxo.ops.business.ws.acknowledgment.services;

import java.io.IOException;
import java.util.concurrent.RejectedExecutionException;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.xml.sax.SAXException;

import atraxo.fmbas.domain.impl.fmbas_type.ProcessStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TransportStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import atraxo.ops.business.api.ext.fuelTicket.IFuelTicketAckProcessor;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
@WebService
public class AcknowledgeOperationalInfoWSImpl implements AcknowledgeOperationalInfoWS {

	private static final Logger LOG = LoggerFactory.getLogger(AcknowledgeOperationalInfoWSImpl.class);

	@Autowired
	private IFuelTicketAckProcessor ftAccepter;

	@Override
	public MSG acknowledgeFuelTransactionsEBits(MSG request) throws JAXBException, SAXException, IOException, BusinessException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("START acknowledgeFuelTransactionsEBits()");
		}
		try {
			this.ftAccepter.process(request, "FuelTicketAccepter", "acknoledgeFuelTicketProcessingResult");
			// acknoledgeFuelTicketProcessingResult(request);
		} catch (RejectedExecutionException e) {
			LOG.warn("Request Rejected (Acknowledge Fuel Transactions Ebits) - Bandwidth Exceeded : MessageId = "
					+ request.getHeaderCommon().getMsgID(), e);

			request.getHeaderCommon().setTransportStatus(TransportStatus._FAILURE_.getName());
			request.getHeaderCommon().setProcessStatus(ProcessStatus._FAILURE_.getName());
			request.getHeaderCommon().setErrorCode(Integer.toString(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value()));
			request.getHeaderCommon().setErrorDescription(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.getReasonPhrase());

			return request;
		}

		request.getHeaderCommon().setTransportStatus(TransportStatus._PROCESSED_.getName());
		request.getHeaderCommon().setProcessStatus(ProcessStatus._SUCCESS_.getName());

		return request;
	}

}
