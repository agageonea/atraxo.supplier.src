/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelTicket;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.fuelTicket.CancelFuelTicketResult;
import atraxo.ops.domain.ext.fuelTicket.ProvidedQuantityInforamtionResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelTicket} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelTicket_Service extends AbstractEntityService<FuelTicket> {

	/**
	 * Public constructor for FuelTicket_Service
	 */
	public FuelTicket_Service() {
		super();
	}

	/**
	 * Public constructor for FuelTicket_Service
	 * 
	 * @param em
	 */
	public FuelTicket_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelTicket> getEntityClass() {
		return FuelTicket.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTicket
	 */
	public FuelTicket findByBusinessKey(Locations departure, String ticketNo,
			Customer customer, Date deliveryDate) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelTicket.NQ_FIND_BY_BUSINESSKEY,
							FuelTicket.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("departure", departure)
					.setParameter("ticketNo", ticketNo)
					.setParameter("customer", customer)
					.setParameter("deliveryDate", deliveryDate)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTicket",
							"departure, ticketNo, customer, deliveryDate"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTicket",
							"departure, ticketNo, customer, deliveryDate"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTicket
	 */
	public FuelTicket findByBusinessKey(Long departureId, String ticketNo,
			Long customerId, Date deliveryDate) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							FuelTicket.NQ_FIND_BY_BUSINESSKEY_PRIMITIVE,
							FuelTicket.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("departureId", departureId)
					.setParameter("ticketNo", ticketNo)
					.setParameter("customerId", customerId)
					.setParameter("deliveryDate", deliveryDate)
					.getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTicket",
							"departureId, ticketNo, customerId, deliveryDate"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTicket",
							"departureId, ticketNo, customerId, deliveryDate"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTicket
	 */
	public FuelTicket findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelTicket.NQ_FIND_BY_BUSINESS,
							FuelTicket.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter(
							"subsidiaryIds",
							Session.user.get().getProfile()
									.getOrganizationIds())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTicket", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTicket", "id"), nure);
		}
	}

	/**
	 * Find by reference: departure
	 *
	 * @param departure
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDeparture(Locations departure) {
		return this.findByDepartureId(departure.getId());
	}
	/**
	 * Find by ID of reference: departure.id
	 *
	 * @param departureId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDepartureId(Integer departureId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.departure.id = :departureId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("departureId", departureId).getResultList();
	}
	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.customer.id = :customerId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: upliftUnit
	 *
	 * @param upliftUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByUpliftUnit(Unit upliftUnit) {
		return this.findByUpliftUnitId(upliftUnit.getId());
	}
	/**
	 * Find by ID of reference: upliftUnit.id
	 *
	 * @param upliftUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByUpliftUnitId(Integer upliftUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.upliftUnit.id = :upliftUnitId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("upliftUnitId", upliftUnitId).getResultList();
	}
	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findBySupplier(Suppliers supplier) {
		return this.findBySupplierId(supplier.getId());
	}
	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findBySupplierId(Integer supplierId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.supplier.id = :supplierId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("supplierId", supplierId).getResultList();
	}
	/**
	 * Find by reference: fueller
	 *
	 * @param fueller
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByFueller(Suppliers fueller) {
		return this.findByFuellerId(fueller.getId());
	}
	/**
	 * Find by ID of reference: fueller.id
	 *
	 * @param fuellerId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByFuellerId(Integer fuellerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.fueller.id = :fuellerId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuellerId", fuellerId).getResultList();
	}
	/**
	 * Find by reference: destination
	 *
	 * @param destination
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDestination(Locations destination) {
		return this.findByDestinationId(destination.getId());
	}
	/**
	 * Find by ID of reference: destination.id
	 *
	 * @param destinationId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDestinationId(Integer destinationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.destination.id = :destinationId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("destinationId", destinationId).getResultList();
	}
	/**
	 * Find by reference: registration
	 *
	 * @param registration
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByRegistration(Aircraft registration) {
		return this.findByRegistrationId(registration.getId());
	}
	/**
	 * Find by ID of reference: registration.id
	 *
	 * @param registrationId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByRegistrationId(Integer registrationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.registration.id = :registrationId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("registrationId", registrationId).getResultList();
	}
	/**
	 * Find by reference: finalDest
	 *
	 * @param finalDest
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByFinalDest(Locations finalDest) {
		return this.findByFinalDestId(finalDest.getId());
	}
	/**
	 * Find by ID of reference: finalDest.id
	 *
	 * @param finalDestId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByFinalDestId(Integer finalDestId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.finalDest.id = :finalDestId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("finalDestId", finalDestId).getResultList();
	}
	/**
	 * Find by reference: netUpliftUnit
	 *
	 * @param netUpliftUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByNetUpliftUnit(Unit netUpliftUnit) {
		return this.findByNetUpliftUnitId(netUpliftUnit.getId());
	}
	/**
	 * Find by ID of reference: netUpliftUnit.id
	 *
	 * @param netUpliftUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByNetUpliftUnitId(Integer netUpliftUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.netUpliftUnit.id = :netUpliftUnitId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("netUpliftUnitId", netUpliftUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: beforeFuelingUnit
	 *
	 * @param beforeFuelingUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByBeforeFuelingUnit(Unit beforeFuelingUnit) {
		return this.findByBeforeFuelingUnitId(beforeFuelingUnit.getId());
	}
	/**
	 * Find by ID of reference: beforeFuelingUnit.id
	 *
	 * @param beforeFuelingUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByBeforeFuelingUnitId(
			Integer beforeFuelingUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.beforeFuelingUnit.id = :beforeFuelingUnitId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("beforeFuelingUnitId", beforeFuelingUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: afterFuelingUnit
	 *
	 * @param afterFuelingUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAfterFuelingUnit(Unit afterFuelingUnit) {
		return this.findByAfterFuelingUnitId(afterFuelingUnit.getId());
	}
	/**
	 * Find by ID of reference: afterFuelingUnit.id
	 *
	 * @param afterFuelingUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAfterFuelingUnitId(Integer afterFuelingUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.afterFuelingUnit.id = :afterFuelingUnitId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("afterFuelingUnitId", afterFuelingUnitId)
				.getResultList();
	}
	/**
	 * Find by reference: densityUnit
	 *
	 * @param densityUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDensityUnit(Unit densityUnit) {
		return this.findByDensityUnitId(densityUnit.getId());
	}
	/**
	 * Find by ID of reference: densityUnit.id
	 *
	 * @param densityUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDensityUnitId(Integer densityUnitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.densityUnit.id = :densityUnitId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("densityUnitId", densityUnitId).getResultList();
	}
	/**
	 * Find by reference: densityVolume
	 *
	 * @param densityVolume
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDensityVolume(Unit densityVolume) {
		return this.findByDensityVolumeId(densityVolume.getId());
	}
	/**
	 * Find by ID of reference: densityVolume.id
	 *
	 * @param densityVolumeId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDensityVolumeId(Integer densityVolumeId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.densityVolume.id = :densityVolumeId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("densityVolumeId", densityVolumeId)
				.getResultList();
	}
	/**
	 * Find by reference: acType
	 *
	 * @param acType
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAcType(AcTypes acType) {
		return this.findByAcTypeId(acType.getId());
	}
	/**
	 * Find by ID of reference: acType.id
	 *
	 * @param acTypeId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAcTypeId(Integer acTypeId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.acType.id = :acTypeId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("acTypeId", acTypeId).getResultList();
	}
	/**
	 * Find by reference: reseller
	 *
	 * @param reseller
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByReseller(Customer reseller) {
		return this.findByResellerId(reseller.getId());
	}
	/**
	 * Find by ID of reference: reseller.id
	 *
	 * @param resellerId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByResellerId(Integer resellerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.reseller.id = :resellerId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("resellerId", resellerId).getResultList();
	}
	/**
	 * Find by reference: ammountCurr
	 *
	 * @param ammountCurr
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAmmountCurr(Currencies ammountCurr) {
		return this.findByAmmountCurrId(ammountCurr.getId());
	}
	/**
	 * Find by ID of reference: ammountCurr.id
	 *
	 * @param ammountCurrId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAmmountCurrId(Integer ammountCurrId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.ammountCurr.id = :ammountCurrId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("ammountCurrId", ammountCurrId).getResultList();
	}
	/**
	 * Find by reference: referenceTicket
	 *
	 * @param referenceTicket
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByReferenceTicket(FuelTicket referenceTicket) {
		return this.findByReferenceTicketId(referenceTicket.getId());
	}
	/**
	 * Find by ID of reference: referenceTicket.id
	 *
	 * @param referenceTicketId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByReferenceTicketId(Integer referenceTicketId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTicket e where e.clientId = :clientId and e.referenceTicket.id = :referenceTicketId",
						FuelTicket.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("referenceTicketId", referenceTicketId)
				.getResultList();
	}
}
