/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.fuelRequest.service;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.notes.INotesService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.domain.impl.notes.Notes;
import atraxo.ops.business.api.fuelQuotation.IFuelQuotationService;
import atraxo.ops.business.api.fuelRequest.IFuelRequestService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.business.ext.flightEvent.delegate.FlightEvent_Bd;
import atraxo.ops.business.ext.fuelRequest.delegate.FuelRequest_Bd;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FuelRequestStatus;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link FuelRequest} domain entity.
 */
public class FuelRequest_Service extends atraxo.ops.business.impl.fuelRequest.FuelRequest_Service implements IFuelRequestService {

	@Override
	protected void preInsert(FuelRequest e) throws BusinessException {
		super.preInsert(e);
		e.setRequestStatus(FuelRequestStatus._NEW_);
		FuelRequest_Bd frBd = this.getBusinessDelegate(FuelRequest_Bd.class);
		frBd.checkRelease(e);
		frBd.verificateDates(e.getStartDate(), e.getEndDate());
		frBd.checkCustomer(e);
		frBd.checkReceivedOn(e);
	}

	@Override
	@History(value = "New request", type = FuelRequest.class)
	@Transactional
	public void insert(List<FuelRequest> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	protected void preUpdate(FuelRequest e) throws BusinessException {
		super.preUpdate(e);
		FuelRequest_Bd frBd = this.getBusinessDelegate(FuelRequest_Bd.class);
		frBd.checkRelease(e);
		frBd.verificateDates(e.getStartDate(), e.getEndDate());
		frBd.checkCustomer(e);
		frBd.checkReceivedOn(e);
	}

	@Override
	protected void postInsert(FuelRequest e) throws BusinessException {
		super.postInsert(e);
		FuelRequest savedFr = this.findByRefid(e.getRefid());
		FuelRequest_Bd frBd = this.getBusinessDelegate(FuelRequest_Bd.class);
		frBd.calculateCode(savedFr);
		this.update(savedFr);
	}

	@Override
	@History(value = "Checked", type = FuelRequest.class)
	@Transactional
	public void check(FuelRequest fuelRequest) throws BusinessException {
		if (fuelRequest.getFlightEvent().isEmpty()) {
			throw new BusinessException(OpsErrorCode.FUEL_REQ_CHECKED_REQ, OpsErrorCode.FUEL_REQ_CHECKED_REQ.getErrMsg());
		}
		if (FuelRequestStatus._NEW_.equals(fuelRequest.getRequestStatus())) {
			fuelRequest.setRequestStatus(FuelRequestStatus._PROCESSED_);
			this.update(fuelRequest);
		}
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		FlightEvent_Bd bd = this.getBusinessDelegate(FlightEvent_Bd.class);
		bd.removeFlightTypes(ids, FuelRequest.class);
		IChangeHistoryService historyService = (IChangeHistoryService) this.findEntityService(ChangeHistory.class);
		INotesService notesService = (INotesService) this.findEntityService(Notes.class);
		historyService.deleteHistory(ids, FuelRequest.class);
		notesService.deleteNotes(ids, FuelRequest.class);

	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public FuelQuotation generateFuelQuotation(FuelRequest fuelRequest) throws BusinessException {
		IFuelQuotationService fuelQuotationService = (IFuelQuotationService) this.findEntityService(FuelQuotation.class);
		if (fuelQuotationService.findByFuelRequest(fuelRequest).isEmpty()) {
			FuelRequest_Bd frBd = this.getBusinessDelegate(FuelRequest_Bd.class);
			FuelQuotation fuelQuotation = frBd.generateFuelQuote(fuelRequest);
			if (fuelQuotation != null) {
				fuelRequest.setFuelQuotation(fuelQuotation);
				fuelRequest.setRequestStatus(FuelRequestStatus._QUOTED_);
				this.update(fuelRequest);
				return fuelQuotation;
			}
		}
		return null;
	}

	@Override
	@Transactional
	@History(type = FuelRequest.class)
	public void updateStatus(FuelRequest fuelRequest, @Action String action, @Reason String remarks) throws BusinessException {
		this.onUpdate(fuelRequest);

	}
}
