/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.fuelQuoteLocation.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.business.ext.contracts.delegate.SupplierContract_Bd;
import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.cmm.domain.ext.contracts.SupplierContract;
import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.tolerance.IToleranceService;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.business.ext.tolerance.ToleranceResult;
import atraxo.fmbas.business.ext.tolerance.ToleranceVerifier;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.json.message.SoneMessage;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.fuelQuoteLocation.IFuelQuoteLocationService;
import atraxo.ops.business.api.pricePolicy.IPricePolicyService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.business.ext.flightEvent.delegate.FlightEvent_Bd;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link FuelQuoteLocation} domain entity.
 */
public class FuelQuoteLocation_Service extends atraxo.ops.business.impl.fuelQuoteLocation.FuelQuoteLocation_Service
		implements IFuelQuoteLocationService {

	private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);

	@Autowired
	private UnitConverterService unitConverterService;
	@Autowired
	private PriceConverterService priceConverterService;

	@Override
	protected void preInsert(FuelQuoteLocation e) throws BusinessException {
		super.preInsert(e);
		this.verifyDates(e.getFuelReleaseStartsOn(), e.getFuelReleaseEndsOn());
		this.combinationIdentifier(e);
		this.setSupplierInfo(e);
	}

	private void setSupplierInfo(FuelQuoteLocation e) throws BusinessException {
		IContractService service = (IContractService) this.findEntityService(Contract.class);
		@SuppressWarnings("unchecked")
		List<SupplierContract> supplierContracts = service.getSupplierContracts(e.getLocation(), e.getFuelReleaseStartsOn(), e.getFuelReleaseEndsOn(),
				e.getCurrency(), e.getUnit(), e.getAverageMethod(), e.getFinancialSource(), e.getPeriod());
		if (supplierContracts.size() == 1) {
			SupplierContract supplierContract = supplierContracts.get(0);
			this.setSupplierInformation(e, supplierContract);
		}
	}

	private void setSupplierInformation(FuelQuoteLocation e, SupplierContract supplierContract) {
		e.setSupplier(supplierContract.getContract().getSupplier());
		e.setContract(supplierContract.getContract());
		e.setBasePrice(supplierContract.getBasePrice());
		e.setIntoplaneFee(supplierContract.getIntoPlaneFee());
		e.setOtherFees(supplierContract.getOtherFees());
		e.setTaxes(supplierContract.getTaxes());
		e.setFlightFee(supplierContract.getPerFlightFee());
		BigDecimal differential = supplierContract.getDifferential() != null ? supplierContract.getDifferential() : BigDecimal.ZERO;
		e.setFuelPrice(supplierContract.getBasePrice().add(differential));
		e.setTotalPricePerUom(e.getBasePrice().add(e.getIntoplaneFee()).add(e.getOtherFees()).add(e.getTaxes()));
		if (PricingMethod._INDEX_.equals(supplierContract.getPriceBasis())) {
			e.setTotalPricePerUom(e.getTotalPricePerUom().add(differential));
		}

		if (e.getDifferential() != null) {
			e.setTotalPricePerUom(e.getTotalPricePerUom().add(e.getDifferential()));
		}
		e.setDft(e.getIntoplaneFee().add(e.getOtherFees()).add(e.getTaxes()));
		e.setPriceBasis(supplierContract.getPriceBasis());
		e.setIndexOffset(supplierContract.getIndexOffset());
		e.setQuotation(supplierContract.getQuotation());
		e.setIntoPlaneAgent(supplierContract.getIntoPlaneAgent());
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void insert(List<FuelQuoteLocation> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void update(List<FuelQuoteLocation> list) throws BusinessException {
		super.update(list);
	}

	@Override
	protected void preUpdate(FuelQuoteLocation e) throws BusinessException {
		super.preUpdate(e);
		FuelQuoteLocation fql = this.findById(e.getId());
		this.updateQuantity(e, fql);
		this.combinationIdentifier(e);
		this.resetSupplierInfo(e);
		this.reCalculatePrice(e);
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		FlightEvent_Bd bd = this.getBusinessDelegate(FlightEvent_Bd.class);
		bd.removeFlightTypes(ids, FuelQuoteLocation.class);
	}

	private void updateQuantity(FuelQuoteLocation newFql, FuelQuoteLocation oldFql) throws BusinessException {
		if (newFql.getUnit().getId().equals(oldFql.getUnit().getId())) {
			return;
		}
		if (newFql.getQuantity() == null) {
			return;
		}
		double density = this.getSystemDensity();
		BigDecimal quantity = this.unitConverterService.convert(oldFql.getUnit(), newFql.getUnit(), newFql.getQuantity(), density);
		newFql.setQuantity(quantity);

	}

	private double getSystemDensity() throws BusinessException {
		ISystemParameterService service = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		return Double.parseDouble(service.getDensity());
	}

	private void reCalculatePrice(FuelQuoteLocation e) throws BusinessException {
		if (e.getContract() != null) {
			SupplierContract_Bd bd = this.getBusinessDelegate(SupplierContract_Bd.class);
			SupplierContract supplierContract = bd.buildSupplierContract(e.getFuelReleaseStartsOn(), e.getFuelReleaseEndsOn(), e.getContract(),
					e.getUnit(), e.getCurrency(), e.getAverageMethod(), e.getFinancialSource(), e.getPeriod());
			this.setSupplierInformation(e, supplierContract);
			this.calculateSellingPrice(e);
		}
	}

	private void calculateSellingPrice(FuelQuoteLocation e) throws BusinessException {
		if (e.getMargin() != null) {
			this.calDifferentialFromMargin(e);
		} else if (e.getPricingPolicy() != null) {
			this.convertDefaultMarkup(e);
		}
	}

	private void calDifferentialFromMargin(FuelQuoteLocation e) {
		e.setDifferential(e.getFuelPrice().multiply(e.getMargin().divide(ONE_HUNDRED, MathContext.DECIMAL64)));

	}

	private void convertDefaultMarkup(FuelQuoteLocation fql) throws BusinessException {

		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);

		BigDecimal differential = this.priceConverterService.convert(fql.getPricingPolicy().getUnit(), fql.getUnit(),
				fql.getPricingPolicy().getCurrency(), fql.getCurrency(), fql.getPricingPolicy().getDefaultMarkup(), date, fql.getFinancialSource(),
				fql.getAverageMethod(), this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity(), false, fql.getPeriod())
				.getValue();
		fql.setDifferential(differential);
	}

	private void resetSupplierInfo(FuelQuoteLocation e) {
		if (e.getContract() != null && !e.getContract().getLocation().getId().equals(e.getLocation().getId())) {
			e.setSupplier(null);
			e.setContract(null);
			e.setBasePrice(null);
			e.setFuelPrice(null);
			e.setIntoplaneFee(null);
			e.setOtherFees(null);
			e.setTaxes(null);
			e.setFlightFee(null);
			e.setTotalPricePerUom(null);
			e.setDft(null);
			e.setPaymentTerms(null);
			e.setPriceBasis(null);
			e.setIndexOffset(null);
			e.setQuotation(null);
			e.setIntoPlaneAgent(null);
		}
	}

	@Override
	protected void postUpdate(FuelQuoteLocation e) throws BusinessException {
		super.postUpdate(e);
		this.checkPricingInformation(e);
	}

	private void checkPricingInformation(FuelQuoteLocation e) throws BusinessException {
		if (e.getDifferential() == null) {
			e.setToleranceMessage(null);
			return;
		}
		if (e.getPricingPolicy() != null) {
			e.setToleranceMessage(null);
			return;
		}

		PricePolicy pricePolicy = this.getPricePolicy(e);
		if (pricePolicy == null) {
			return;
		}

		ToleranceVerifier verifier = new ToleranceVerifier(this.unitConverterService, this.getBusinessDelegate(ExchangeRate_Bd.class),
				this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity());
		IToleranceService tolService = (IToleranceService) this.findEntityService(Tolerance.class);
		Tolerance tolerance = tolService.getMarkUpTolerance();

		BigDecimal minimal = BigDecimal.ZERO;
		if (pricePolicy.getMinimalMargin() != null) {
			minimal = e.getFuelPrice().multiply(pricePolicy.getMinimalMargin().divide(ONE_HUNDRED, MathContext.DECIMAL64));
		}
		if (pricePolicy.getMinimalMarkup() != null && minimal.compareTo(pricePolicy.getMinimalMarkup()) < 0) {
			minimal = pricePolicy.getMinimalMarkup();
		}

		Currencies fromCurrency = pricePolicy.getCurrency() != null ? pricePolicy.getCurrency() : e.getCurrency();
		Unit fromUnit = pricePolicy.getUnit() != null ? pricePolicy.getUnit() : e.getUnit();
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		ToleranceResult toleranceResult = verifier.checkLowerToleranceLimit(minimal, e.getDifferential(), fromUnit, e.getUnit(), fromCurrency,
				e.getCurrency(), tolerance, date);
		switch (toleranceResult.getType()) {
		case NOT_WITHIN:
			e.setToleranceMessage(SoneMessage.FUEL_QUPTE_LOCATION_MARKUP_TOLERANCE);
			break;
		case WITHIN:
		case MARGIN:
		default:
			break;
		}

	}

	private PricePolicy getPricePolicy(FuelQuoteLocation e) throws BusinessException {
		IPricePolicyService service = (IPricePolicyService) this.findEntityService(PricePolicy.class);
		List<PricePolicy> pricePolicies = service.findByLocAndCust(e.getLocation(), e.getQuote().getCustomer());
		Collections.sort(pricePolicies, new Comparator<PricePolicy>() {
			@Override
			public int compare(PricePolicy o1, PricePolicy o2) {
				if (o1.getCustomer() != null && o2.getCustomer() != null) {
					return this.compareByLocation(o1, o2);
				} else {
					if (o1.getCustomer() == null && o2.getCustomer() == null) {
						return this.compareByLocation(o1, o2);
					} else {
						if (o1.getCustomer() != null && o2.getCustomer() == null) {
							return -1;
						} else {
							return 1;
						}
					}
				}
			}

			private int compareByLocation(PricePolicy o1, PricePolicy o2) {
				if (o1.getLocation() != null && o2.getLocation() != null) {
					return 0;
				} else {
					if (o1.getLocation() != null && o2.getLocation() == null) {
						return -1;
					} else if (o1.getLocation() == null && o2.getLocation() != null) {
						return 1;
					} else {
						return 0;
					}
				}
			}

		});
		if (pricePolicies.isEmpty()) {
			return null;
		}
		return pricePolicies.get(0);
	}

	/**
	 * Verify the dates are in the future.
	 *
	 * @param from
	 * @param to
	 * @throws BusinessException
	 */
	private void verifyDates(Date from, Date to) throws BusinessException {
		if (from != null && to != null && (from.after(to))) {
			throw new BusinessException(OpsErrorCode.FQL_FUEL_RELEASE_DATE1, OpsErrorCode.FQL_FUEL_RELEASE_DATE1.getErrMsg());
		}
		if (from != null && to != null && from.before(this.trimDate(new Date()))) {
			throw new BusinessException(OpsErrorCode.FQL_FUEL_RELEASE_DATE2, OpsErrorCode.FQL_FUEL_RELEASE_DATE2.getErrMsg());
		}
	}

	private Date trimDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * Location-EventType-OperationalType unique identifier.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void combinationIdentifier(FuelQuoteLocation e) throws BusinessException {

		String sql = "select e from " + FuelQuoteLocation.class.getSimpleName() + " e where e.clientId =:clientId ";
		if (e.getId() != null) {
			sql += "and e.id <> :fuelQuoteLocId";
		}
		Query query = this.getEntityManager().createQuery(sql, FuelQuoteLocation.class);
		query.setParameter("clientId", Session.user.get().getClientId());
		if (e.getId() != null) {
			query.setParameter("fuelQuoteLocId", e.getId());
		}
		@SuppressWarnings("unchecked")
		List<FuelQuoteLocation> quoteLoc = query.getResultList();
		if (quoteLoc != null) {
			for (FuelQuoteLocation fuelQuoteLoc : quoteLoc) {
				if (e.getQuote().getId() == fuelQuoteLoc.getQuote().getId()) {
					if (e.getQuote().getCustomer().getId() == fuelQuoteLoc.getQuote().getCustomer().getId()) {
						if (e.getLocation() != null && fuelQuoteLoc.getLocation() != null
								&& e.getLocation().getId().equals(fuelQuoteLoc.getLocation().getId()) && e.getEventType() != null
								&& fuelQuoteLoc.getEventType() != null && e.getEventType().equals(fuelQuoteLoc.getEventType())
								&& e.getOperationalType() != null && fuelQuoteLoc.getOperationalType() != null
								&& e.getOperationalType().equals(fuelQuoteLoc.getOperationalType())) {
							throw new BusinessException(OpsErrorCode.FUEL_QUOTE_LOCATION_COMBINATION,
									OpsErrorCode.FUEL_QUOTE_LOCATION_COMBINATION.getErrMsg());
						}
					}
				}
			}
		}
	}

	@Override
	public BigDecimal convertPrice(ContractPriceComponent priceComponent, Integer finSrcId, Integer avgMthdId, String currencyCode, String unitCode)
			throws BusinessException {
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		Density density = this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity();

		return this.priceConverterService.convert(priceComponent.getUnit().getCode(), unitCode, priceComponent.getCurrency().getCode(), currencyCode,
				priceComponent.getPrice(), date, finSrcId, avgMthdId, density, false, priceComponent.getContrPriceCtgry().getExchangeRateOffset())
				.getValue();
	}
}
