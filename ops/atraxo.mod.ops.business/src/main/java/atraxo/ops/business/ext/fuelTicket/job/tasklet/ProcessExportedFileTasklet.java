package atraxo.ops.business.ext.fuelTicket.job.tasklet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.business.ext.fuelTicket.job.items.ExportTicketItemProcessor;
import atraxo.ops.business.ext.fuelTicket.job.namespace.GeneralNamespaceMapper;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketTransmissionStatus;
import seava.j4e.api.exceptions.BusinessException;

public class ProcessExportedFileTasklet implements Tasklet, InitializingBean {

	@Autowired
	private IFuelTicketService fuelTicketService;

	private Resource inputFile;
	private Resource outputDir;

	private static final Logger LOG = LoggerFactory.getLogger(ProcessExportedFileTasklet.class);

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(this.inputFile, "input file must be set");
		Assert.notNull(this.outputDir, "output folder must be set");
	}

	@SuppressWarnings("unchecked")
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) {
		String fileName = (String) chunkContext.getStepContext().getJobExecutionContext().get(ExportTicketItemProcessor.FILE_NAME);
		String inputFileName = (String) chunkContext.getStepContext().getJobExecutionContext().get(ExportTicketItemProcessor.TMP_FILE);
		Object obj = chunkContext.getStepContext().getJobExecutionContext().get(ExportTicketItemProcessor.TICKET_IDS);
		List<Object> ids = (List<Object>) obj;
		FuelTicketTransmissionStatus transmissionStatus = FuelTicketTransmissionStatus._TRANSMITTED_;
		if (fileName != null) {
			try {
				this.replaceNameSpace(inputFileName, fileName);
			} catch (BusinessException e) {
				LOG.warn("Replace Name Space exception", e);
				transmissionStatus = FuelTicketTransmissionStatus._FAILED_;
			}
		}
		try {
			if (ids != null) {
				this.setFuelTicketsStatus(ids, transmissionStatus);
			}
		} catch (BusinessException e) {
			LOG.warn("Set fuel ticket status problem.", e);
		}
		return RepeatStatus.FINISHED;
	}

	private void setFuelTicketsStatus(List<Object> ids, FuelTicketTransmissionStatus transmissionStatus) throws BusinessException {
		List<FuelTicket> fuelTickets = new ArrayList<FuelTicket>();
		for (Object id : ids) {
			FuelTicket fuelTicket = this.fuelTicketService.findById(id);
			fuelTicket.setTransmissionStatus(transmissionStatus);
			fuelTickets.add(fuelTicket);
		}
		this.fuelTicketService.updateWithoutBL(fuelTickets);
	}

	private void replaceNameSpace(String inputFileName, String shortFileName) throws BusinessException {

		BufferedReader br = null;
		BufferedWriter bw = null;
		FileReader fr = null;
		FileWriter fw = null;
		File file = new File(inputFileName);
		try {
			fr = new FileReader(file);
			fw = new FileWriter(new File(this.getOutputDir().getFile(), shortFileName));
			br = new BufferedReader(fr);
			bw = new BufferedWriter(fw);
			String line;
			String ns = GeneralNamespaceMapper.PREFIX + ":";
			while ((line = br.readLine()) != null) {
				if (line.contains(ns)) {
					line = line.replace(ns, "");
					line = line.replaceAll("<root>", "");
					line = line.replaceAll("</root>", "");
				}
				bw.write(line + "\n");
			}
		} catch (FileNotFoundException e) {
			LOG.warn("File not found.", e);
		} catch (IOException e) {
			LOG.warn("File not found.", e);
		} finally {
			try {
				if (fr != null) {
					fr.close();
				}
			} catch (IOException e) {
				LOG.warn("File not found.", e);
			}
			try {
				if (fw != null) {
					fw.close();
				}
			} catch (IOException e) {
				LOG.warn("File write close problem.", e);
			}
			try {
				if (br != null) {
					br.close();
					file.delete();
				}
			} catch (IOException e) {
				LOG.warn("Buffered read close problem.", e);
			}
			try {
				if (bw != null) {
					bw.close();
				}
			} catch (IOException e) {
				LOG.warn("Buffered write close problem.", e);
			}
		}
	}

	public Resource getInputFile() {
		return this.inputFile;
	}

	public void setInputFile(Resource inputFile) {
		this.inputFile = inputFile;
	}

	public Resource getOutputDir() {
		return this.outputDir;
	}

	public void setOutputDir(Resource outputDir) {
		this.outputDir = outputDir;
	}

}
