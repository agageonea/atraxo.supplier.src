package atraxo.ops.business.ext.bpm.delegate.fuelticket.approval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.business.ext.bpm.WorkflowMsgConstants;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstanceEntity;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import seava.j4e.api.exceptions.BusinessException;

public class FuelTicketApprovalWorkflowTerminator implements IWorkflowTerminatorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FuelTicketApprovalWorkflowTerminator.class);

	@Autowired
	private IFuelTicketService fuelTicketSrv;

	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {

		List<WorkflowInstanceEntity> entities = new ArrayList<>(workflowInstance.getEntitites());

		if (!CollectionUtils.isEmpty(entities)) {
			for (WorkflowInstanceEntity entity : entities) {
				FuelTicket ticket = null;
				try {
					ticket = this.fuelTicketSrv.findByBusiness(entity.getObjectId());
				} catch (Exception e) {
					LOGGER.warn("WARNING: could not retrieve a Fuel Ticket for ID " + entity.getObjectId()
							+ " ! Will not update the entity since it doesn't exist !", e);
				}
				if (ticket != null) {
					this.fuelTicketSrv.rejectFuelTicket(Arrays.asList(ticket),
							StringUtils.isEmpty(reason) ? WorkflowMsgConstants.APPROVAL_WORKFLOW_TERMINATED_COMMENT : reason);
				}
			}
		}
	}
}
