package atraxo.ops.business.ext.bpm.delegate.fuelticket.release;

import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class FuelTicketReleaseWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) {
		// this workflow has nothing to do on terminate
	}

}
