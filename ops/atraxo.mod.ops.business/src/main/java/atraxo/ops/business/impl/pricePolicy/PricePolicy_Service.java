/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.pricePolicy;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link PricePolicy} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class PricePolicy_Service extends AbstractEntityService<PricePolicy> {

	/**
	 * Public constructor for PricePolicy_Service
	 */
	public PricePolicy_Service() {
		super();
	}

	/**
	 * Public constructor for PricePolicy_Service
	 * 
	 * @param em
	 */
	public PricePolicy_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<PricePolicy> getEntityClass() {
		return PricePolicy.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return PricePolicy
	 */
	public PricePolicy findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(PricePolicy.NQ_FIND_BY_BUSINESS,
							PricePolicy.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"PricePolicy", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"PricePolicy", "id"), nure);
		}
	}

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricePolicy e where e.clientId = :clientId and e.customer.id = :customerId",
						PricePolicy.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByLocation(Locations location) {
		return this.findByLocationId(location.getId());
	}
	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByLocationId(Integer locationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricePolicy e where e.clientId = :clientId and e.location.id = :locationId",
						PricePolicy.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locationId", locationId).getResultList();
	}
	/**
	 * Find by reference: aircraft
	 *
	 * @param aircraft
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByAircraft(AcTypes aircraft) {
		return this.findByAircraftId(aircraft.getId());
	}
	/**
	 * Find by ID of reference: aircraft.id
	 *
	 * @param aircraftId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByAircraftId(Integer aircraftId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricePolicy e where e.clientId = :clientId and e.aircraft.id = :aircraftId",
						PricePolicy.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("aircraftId", aircraftId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricePolicy e where e.clientId = :clientId and e.unit.id = :unitId",
						PricePolicy.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByCurrency(Currencies currency) {
		return this.findByCurrencyId(currency.getId());
	}
	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByCurrencyId(Integer currencyId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from PricePolicy e where e.clientId = :clientId and e.currency.id = :currencyId",
						PricePolicy.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("currencyId", currencyId).getResultList();
	}
}
