/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.flightEvent;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.flightEvent.ProvidedQuantityInforamtionResult;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FlightEvent} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FlightEvent_Service extends AbstractEntityService<FlightEvent> {

	/**
	 * Public constructor for FlightEvent_Service
	 */
	public FlightEvent_Service() {
		super();
	}

	/**
	 * Public constructor for FlightEvent_Service
	 * 
	 * @param em
	 */
	public FlightEvent_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FlightEvent> getEntityClass() {
		return FlightEvent.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FlightEvent
	 */
	public FlightEvent findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FlightEvent.NQ_FIND_BY_BUSINESS,
							FlightEvent.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FlightEvent", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FlightEvent", "id"), nure);
		}
	}

	/**
	 * Find by reference: locQuote
	 *
	 * @param locQuote
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByLocQuote(FuelQuoteLocation locQuote) {
		return this.findByLocQuoteId(locQuote.getId());
	}
	/**
	 * Find by ID of reference: locQuote.id
	 *
	 * @param locQuoteId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByLocQuoteId(Integer locQuoteId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.locQuote.id = :locQuoteId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locQuoteId", locQuoteId).getResultList();
	}
	/**
	 * Find by reference: fuelRequest
	 *
	 * @param fuelRequest
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByFuelRequest(FuelRequest fuelRequest) {
		return this.findByFuelRequestId(fuelRequest.getId());
	}
	/**
	 * Find by ID of reference: fuelRequest.id
	 *
	 * @param fuelRequestId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByFuelRequestId(Integer fuelRequestId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.fuelRequest.id = :fuelRequestId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelRequestId", fuelRequestId).getResultList();
	}
	/**
	 * Find by reference: departure
	 *
	 * @param departure
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByDeparture(Locations departure) {
		return this.findByDepartureId(departure.getId());
	}
	/**
	 * Find by ID of reference: departure.id
	 *
	 * @param departureId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByDepartureId(Integer departureId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.departure.id = :departureId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("departureId", departureId).getResultList();
	}
	/**
	 * Find by reference: destination
	 *
	 * @param destination
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByDestination(Locations destination) {
		return this.findByDestinationId(destination.getId());
	}
	/**
	 * Find by ID of reference: destination.id
	 *
	 * @param destinationId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByDestinationId(Integer destinationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.destination.id = :destinationId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("destinationId", destinationId).getResultList();
	}
	/**
	 * Find by reference: aircraftType
	 *
	 * @param aircraftType
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByAircraftType(AcTypes aircraftType) {
		return this.findByAircraftTypeId(aircraftType.getId());
	}
	/**
	 * Find by ID of reference: aircraftType.id
	 *
	 * @param aircraftTypeId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByAircraftTypeId(Integer aircraftTypeId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.aircraftType.id = :aircraftTypeId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("aircraftTypeId", aircraftTypeId).getResultList();
	}
	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByUnit(Unit unit) {
		return this.findByUnitId(unit.getId());
	}
	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByUnitId(Integer unitId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.unit.id = :unitId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("unitId", unitId).getResultList();
	}
	/**
	 * Find by reference: arrivalFrom
	 *
	 * @param arrivalFrom
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByArrivalFrom(Locations arrivalFrom) {
		return this.findByArrivalFromId(arrivalFrom.getId());
	}
	/**
	 * Find by ID of reference: arrivalFrom.id
	 *
	 * @param arrivalFromId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByArrivalFromId(Integer arrivalFromId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.arrivalFrom.id = :arrivalFromId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("arrivalFromId", arrivalFromId).getResultList();
	}
	/**
	 * Find by reference: operator
	 *
	 * @param operator
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByOperator(Customer operator) {
		return this.findByOperatorId(operator.getId());
	}
	/**
	 * Find by ID of reference: operator.id
	 *
	 * @param operatorId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByOperatorId(Integer operatorId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.operator.id = :operatorId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("operatorId", operatorId).getResultList();
	}
	/**
	 * Find by reference: handler
	 *
	 * @param handler
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByHandler(Suppliers handler) {
		return this.findByHandlerId(handler.getId());
	}
	/**
	 * Find by ID of reference: handler.id
	 *
	 * @param handlerId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByHandlerId(Integer handlerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.handler.id = :handlerId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("handlerId", handlerId).getResultList();
	}
	/**
	 * Find by reference: aircraftHomeBase
	 *
	 * @param aircraftHomeBase
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByAircraftHomeBase(Locations aircraftHomeBase) {
		return this.findByAircraftHomeBaseId(aircraftHomeBase.getId());
	}
	/**
	 * Find by ID of reference: aircraftHomeBase.id
	 *
	 * @param aircraftHomeBaseId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByAircraftHomeBaseId(Integer aircraftHomeBaseId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.aircraftHomeBase.id = :aircraftHomeBaseId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("aircraftHomeBaseId", aircraftHomeBaseId)
				.getResultList();
	}
	/**
	 * Find by reference: locOrder
	 *
	 * @param locOrder
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByLocOrder(FuelOrderLocation locOrder) {
		return this.findByLocOrderId(locOrder.getId());
	}
	/**
	 * Find by ID of reference: locOrder.id
	 *
	 * @param locOrderId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByLocOrderId(Integer locOrderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FlightEvent e where e.clientId = :clientId and e.locOrder.id = :locOrderId",
						FlightEvent.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("locOrderId", locOrderId).getResultList();
	}
}
