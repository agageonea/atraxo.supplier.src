package atraxo.ops.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class DuplicateFlightEventException extends BusinessException {

	private static final long serialVersionUID = 1949015189316544518L;

	public DuplicateFlightEventException(String errorDetails) {
		super(OpsErrorCode.DUPLICATE_EVENT_TYPE, errorDetails);
	}

}
