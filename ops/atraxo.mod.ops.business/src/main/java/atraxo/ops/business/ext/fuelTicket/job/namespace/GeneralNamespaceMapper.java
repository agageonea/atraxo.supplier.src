package atraxo.ops.business.ext.fuelTicket.job.namespace;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class GeneralNamespaceMapper extends NamespacePrefixMapper {

	public static final String PREFIX = "fuelNS"; // DEFAULT NAMESPACE

	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		return PREFIX;
	}

}
