package atraxo.ops.business.ext.fuelTransaction.dto;

import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FuelingType;
import atraxo.cmm.domain.impl.cmm_type.PaymentType;
import atraxo.cmm.domain.impl.cmm_type.ProductIATA;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionEquipment;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import atraxo.ops.domain.impl.ops_type.DeliveryType;
import atraxo.ops.domain.impl.ops_type.DensityType;
import atraxo.ops.domain.impl.ops_type.DensityUOM;
import atraxo.ops.domain.impl.ops_type.FuelQuantityType;
import atraxo.ops.domain.impl.ops_type.FuelTicketCustomsStatus;
import atraxo.ops.domain.impl.ops_type.IPTransactionCode;
import atraxo.ops.domain.impl.ops_type.InterfaceType;
import atraxo.ops.domain.impl.ops_type.InternationalFlight;
import atraxo.ops.domain.impl.ops_type.TemperatureUnit;
import atraxo.ops.domain.impl.ops_type.TicketSource;
import atraxo.ops.domain.impl.ops_type.TicketType;
import atraxo.ops.domain.impl.ops_type.ValidationState;
import seava.j4e.api.session.Session;
import seava.j4e.iata.fuelplus.iata.fuelticket.Equipment;
import seava.j4e.iata.fuelplus.iata.fuelticket.IPTransaction;
import seava.j4e.iata.fuelplus.iata.fuelticket.MeterReading;
import seava.j4e.iata.fuelplus.iata.fuelticket.ProductInformation;
import seava.j4e.iata.fuelplus.iata.fuelticket.TicketReferenceValue;
import seava.j4e.iata.fuelplus.iata.fuelticket.TotalQuantity;

public class FuelTransactionDto {

	private FuelTransactionDto() {
		// private constructor
	}

	public static FuelTransaction buildEntityObject(IPTransaction iataFt) {
		FuelTransaction ft = new FuelTransaction();
		ft.setClientId(Session.user.get().getClientId());
		/** flight information **/
		ft.setAircraftIdentification(iataFt.getFlightInformation().getAircraftIdentification().toUpperCase());
		ft.setAirlineFlightId(iataFt.getFlightInformation().getAirlineFlightID());
		for (TicketReferenceValue ref : iataFt.getFlightInformation().getTicketReferenceValue()) {
			switch (ref.getTicketReferenceValueType()) {
			case AC:
				ft.setAircraftType(ref.getValue());
				break;
			case FDT:
				ft.setFinalDestination(ref.getValue());
				break;
			case NDT:
				ft.setNextDestination(ref.getValue());
				break;
			default:
				break;
			}
		}
		ft.setUniqueTicketID(iataFt.getHeader().getUniqueTicketID());
		ft.setInternationalFlight(
				iataFt.getFlightInformation().isInternationalFlight() ? InternationalFlight._INTERNATIONAL_ : InternationalFlight._DOMESTIC_);
		if (iataFt.getFlightInformation().getFlightType() != null && iataFt.getFlightInformation().getFlightType().getFlightServiceType() != null) {
			ft.setFlightServiceType(FlightServiceType.getByCode(iataFt.getFlightInformation().getFlightType().getFlightServiceType().name()));
		} else {
			ft.setFlightServiceType(FlightServiceType._EMPTY_);
		}
		/** header **/
		ft.setAirportCode(iataFt.getHeader().getAirportCode());
		ft.setTicketNumber(iataFt.getHeader().getTicketNumber().getValue());
		ft.setIntoPlaneCode(iataFt.getHeader().getIntoPlaneCode());
		ft.setIntoPlaneName(iataFt.getHeader().getIntoPlaneName());
		ft.setTransactionDate(iataFt.getHeader().getTransactionDate().toGregorianCalendar().getTime());
		if (iataFt.getHeader().getPreviousTicketNumber() != null) {
			ft.setPreviousTicket(iataFt.getHeader().getPreviousTicketNumber().getValue());
		}
		ft.setTicketSource(TicketSource.getByCode(iataFt.getHeader().getTicketNumber().getTicketSource().name()));
		ft.setTicketType(TicketType.getByCode(iataFt.getHeader().getTicketNumber().getTicketType().name()));

		/** payment **/
		ft.setPaymentType(PaymentType.getByCode(iataFt.getPaymentInformation().getPaymentType().name()));
		if (iataFt.getPaymentInformation().getAmountReceived() != null) {
			ft.setAmountReceived(iataFt.getPaymentInformation().getAmountReceived().getValue());
			ft.setAmountReceivedCurr(iataFt.getPaymentInformation().getAmountReceived().getCurrency().name());
		}
		if (iataFt.getPaymentInformation().getCardInformation() != null) {
			ft.setCardExpiry(iataFt.getPaymentInformation().getCardInformation().getCardExpiry().toGregorianCalendar().getTime());
			ft.setCardName(iataFt.getPaymentInformation().getCardInformation().getCardName());
			ft.setCardNumber(iataFt.getPaymentInformation().getCardInformation().getCardNumber());
		}
		/** iptline **/
		ft.setIpTransactionCode(IPTransactionCode.getByCode(iataFt.getIPTLine().getIPTransactionType().getIPTransactionCode().name()));
		for (ProductInformation pInfo : iataFt.getIPTLine().getMovementInformation().getProductInformation()) {
			if (pInfo.getProductID().getProductIDQualifier() == null) {
				ft.setCustomStatus(FuelTicketCustomsStatus._EMPTY_);
				ft.setProductId(ProductIATA.getByName(pInfo.getProductID().getValue().value()));
				continue;
			}
			switch (pInfo.getProductID().getProductIDQualifier()) {
			case PRDT:
				ft.setProductId(ProductIATA.getByName(pInfo.getProductID().getValue().value()));
				if (pInfo.getProductID().getProductIDCustoms() != null) {
					ft.setCustomStatus(FuelTicketCustomsStatus.getByCode(pInfo.getProductID().getProductIDCustoms().name()));
				} else {
					ft.setCustomStatus(FuelTicketCustomsStatus._EMPTY_);
				}
				break;
			default:
				break;
			}
		}
		ft.setReceiverCode(iataFt.getIPTLine().getTransactionParties().getSale().getReceiverCode());
		ft.setReceiverName(iataFt.getIPTLine().getTransactionParties().getSale().getReceiverName());
		ft.setReceiverAccountNumber(iataFt.getIPTLine().getTransactionParties().getSale().getReceiverAccountNumber());
		ft.setSupplierCode(iataFt.getIPTLine().getTransactionParties().getSale().getSupplierOROwnerCode());
		ft.setSupplierName(iataFt.getIPTLine().getTransactionParties().getSale().getSupplierOROwnerName());
		if (iataFt.getIPTLine().getMovementInformation().getTransactionTime().getGMTDateTimeStart() != null) {
			ft.setTransactionEndDate(
					iataFt.getIPTLine().getMovementInformation().getTransactionTime().getGMTDateTimeStart().toGregorianCalendar().getTime());
		}
		if (iataFt.getIPTLine().getMovementInformation().getTransactionTime().getGMTDateTimeFinished() != null) {
			ft.setTransactionStartDate(
					iataFt.getIPTLine().getMovementInformation().getTransactionTime().getGMTDateTimeFinished().toGregorianCalendar().getTime());
		}
		/** quantity **/
		addQuantity(iataFt, ft);

		ft.setDeliveryType(DeliveryType._INTO_PLANE_);

		ft.setInterfaceType(InterfaceType._IATA_);
		ft.setValidationState(ValidationState._NOT_CHECKED_);
		/** equipment **/
		addEquipment(iataFt, ft);

		return ft;
	}

	private static void addQuantity(IPTransaction iataFt, FuelTransaction ft) {
		for (TotalQuantity totalQty : iataFt.getIPTLine().getMovementInformation().getTotalQuantity()) {
			FuelTransactionQuantity qty = new FuelTransactionQuantity();
			qty.setFuelQuantity(totalQty.getValue());
			qty.setQuantityType(FuelQuantityType.getByCode(totalQty.getTQDFlag().name()));
			if (totalQty.getTQDUOM() != null) {
				qty.setQuantityUOM(totalQty.getTQDUOM().name());
			}
			ft.addToQuantity(qty);
		}
	}

	private static void addEquipment(IPTransaction iataFt, FuelTransaction ft) {
		for (Equipment iataEq : iataFt.getIPTLine().getMovementInformation().getEquipment()) {
			FuelTransactionEquipment equipment = new FuelTransactionEquipment();
			equipment.setAvgFuelTemperature(iataEq.getAverageFuelTemperature().getValue());
			equipment.setTemperatureUOM(TemperatureUnit.getByName(iataEq.getAverageFuelTemperature().getTUOM().name()));
			if (iataEq.getDensityInformation().getDensity() != null) {
				equipment.setDensity(iataEq.getDensityInformation().getDensity().getValue());
				equipment.setDensityType(DensityType.getByCode(iataEq.getDensityInformation().getDensityType().name()));
				if (iataEq.getDensityInformation().getDensity().getDensityUOM() != null) {
					equipment.setDensityUOM(DensityUOM.getByName(iataEq.getDensityInformation().getDensity().getDensityUOM().name()));
				} else {
					equipment.setDensityUOM(DensityUOM._EMPTY_);
				}
			} else {
				equipment.setDensityUOM(DensityUOM._EMPTY_);
			}
			equipment.setFuelingEquipmentId(iataEq.getFuelingEquipmentID());

			equipment.setFuelingType(FuelingType.getByCode(iataEq.getFuelingType().name()));
			for (MeterReading meter : iataEq.getMeterReading()) {
				equipment.setMeterQuantityDelivered(meter.getMeterQuantityDelivered().getValue());
				if (meter.getMeterQuantityDelivered().getMQDUOM() != null) {
					equipment.setMeterQuantityUOM(meter.getMeterQuantityDelivered().getMQDUOM().name());
				}
				equipment.setMeterReadingEnd(meter.getMeterReadingEnd());
				equipment.setMeterReadingStart(meter.getMeterReadingStart());
			}
			ft.addToEquipment(equipment);
		}
	}

}
