package atraxo.ops.business.ext.fuelTransaction.job;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.ops.business.api.fuelTransaction.IFuelTransactionService;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.ops_type.TicketType;

public class BufferedTicketReader implements ItemReader<List<FuelTransaction>> {

	private static final Logger LOG = LoggerFactory.getLogger(BufferedTicketReader.class);

	private static final String FUEL_TRANSACTION_IDS = "FUEL_TRANSACTION_IDS";
	private static final String TYPE = "type";
	private ExecutionContext executionContext;

	@Autowired
	private IFuelTransactionService service;
	private Long offset;

	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.executionContext = stepExecution.getExecutionContext();
		this.offset = Long.parseLong(stepExecution.getJobParameters().getString("OFFSET"));
	}

	@Override
	public List<FuelTransaction> read() throws Exception {
		if (!this.executionContext.containsKey(TYPE)) {
			LOG.info("Reading original fuel transactions.");
			List<FuelTransaction> list = this.service.getFuelTranscation(this.offset);
			this.removeProcessed(list);
			this.filterListByTicketType(list, TicketType._ORIGINAL_);
			if (!CollectionUtils.isEmpty(list)) {
				FuelTransaction ft = list.get(0);
				this.addToIds(ft);
				ArrayList<FuelTransaction> retList = new ArrayList<>();
				retList.add(ft);
				return retList;
			} else {
				this.executionContext.put(TYPE, TicketType._ORIGINAL_);
			}
		}
		if (this.executionContext.containsKey(TYPE) && TicketType._ORIGINAL_.equals(this.executionContext.get(TYPE))) {
			LOG.info("Reading canceled fuel transactions.");
			List<FuelTransaction> listCancel = this.service.getFuelTranscation(this.offset);
			this.removeProcessed(listCancel);
			this.filterListByTicketType(listCancel, TicketType._CANCEL_);
			if (!CollectionUtils.isEmpty(listCancel)) {
				FuelTransaction ft = listCancel.get(0);
				this.addToIds(ft);
				ArrayList<FuelTransaction> retList = new ArrayList<>();
				retList.add(ft);
				return retList;
			} else {
				this.executionContext.put(TYPE, TicketType._CANCEL_);
			}
		}
		if (this.executionContext.containsKey(TYPE) && TicketType._CANCEL_.equals(this.executionContext.get(TYPE))) {
			LOG.info("Reading reissue fuel transactions.");
			List<FuelTransaction> reissueCancel = this.service.getFuelTranscation(this.offset);
			this.removeProcessed(reissueCancel);
			this.filterListByTicketType(reissueCancel, TicketType._REISSUE_);
			if (!CollectionUtils.isEmpty(reissueCancel)) {
				FuelTransaction ft = reissueCancel.get(0);
				this.addToIds(ft);
				ArrayList<FuelTransaction> retList = new ArrayList<>();
				retList.add(ft);
				return retList;
			} else {
				this.executionContext.put(TYPE, TicketType._REISSUE_);
			}
		}
		if (this.executionContext.containsKey(TYPE) && TicketType._REISSUE_.equals(this.executionContext.get(TYPE))) {
			LOG.info("Reading delete fuel transactions.");
			List<FuelTransaction> reissueCancel = this.service.getFuelTranscation(this.offset);
			this.removeProcessed(reissueCancel);
			this.filterListByTicketType(reissueCancel, TicketType._DELETE_);
			if (!CollectionUtils.isEmpty(reissueCancel)) {
				FuelTransaction ft = reissueCancel.get(0);
				this.addToIds(ft);
				ArrayList<FuelTransaction> retList = new ArrayList<>();
				retList.add(ft);
				return retList;
			} else {
				this.executionContext.put(TYPE, TicketType._DELETE_);
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private void addToIds(FuelTransaction ft) {
		if (this.executionContext.containsKey(FUEL_TRANSACTION_IDS)) {
			((List<Integer>) this.executionContext.get(FUEL_TRANSACTION_IDS)).add(ft.getId());
		} else {
			List<Integer> idList = new ArrayList<>();
			idList.add(ft.getId());
			this.executionContext.put(FUEL_TRANSACTION_IDS, idList);
		}
	}

	@SuppressWarnings("unchecked")
	private void removeProcessed(List<FuelTransaction> fuelTransactions) {
		LOG.info("Removing processed fuel transactions.");
		if (!this.executionContext.containsKey(FUEL_TRANSACTION_IDS)) {
			List<Integer> idsContext = new ArrayList<>();
			this.executionContext.put(FUEL_TRANSACTION_IDS, idsContext);
		}
		if (CollectionUtils.isEmpty(fuelTransactions)) {
			return;
		}
		List<Integer> ids = (List<Integer>) this.executionContext.get(FUEL_TRANSACTION_IDS);
		Iterator<FuelTransaction> iter = fuelTransactions.iterator();
		while (iter.hasNext()) {
			FuelTransaction ft = iter.next();
			boolean isInList = false;
			for (Integer id : ids) {
				if (ft.getId().intValue() == id.intValue()) {
					isInList = true;
				}
			}
			if (isInList) {
				iter.remove();
			}
		}
	}

	private void filterListByTicketType(List<FuelTransaction> list, TicketType type) {
		Iterator<FuelTransaction> iter = list.iterator();
		while (iter.hasNext()) {
			FuelTransaction ft = iter.next();
			if (!type.equals(ft.getTicketType())) {
				iter.remove();
			}
		}
	}
}
