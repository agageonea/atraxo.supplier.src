package atraxo.ops.business.ws.fueltransaction.services;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.jws.WebService;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.xml.sax.SAXException;

import atraxo.fmbas.business.api.dictionary.IDictionaryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.ops.business.api.fuelTransaction.IFuelTransactionService;
import atraxo.ops.business.api.fuelTransaction.IFuelTransactionSourceService;
import atraxo.ops.business.ext.fuelTransaction.service.FuelTransactionTransformer;
import atraxo.ops.business.ws.fueltransaction.model.ImportDocumentRequest;
import atraxo.ops.business.ws.fueltransaction.model.ImportDocumentResponse;
import atraxo.ops.business.ws.fueltransaction.model.NavisionParameter;
import atraxo.ops.domain.ext.fuelTicket.FetchResult;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSourceParameters;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.iata.fuelplus.iata.fuelticket.FuelTransaction;
import seava.j4e.iata.fuelplus.iata.fuelticket.FuelTransactionTransmission;
import seava.j4e.iata.fuelplus.iata.fuelticket.TicketStatus;

@WebService
public class ImportFuelTransactionsWsImpl implements ImportFuelTransactionsWs {

	private static final Logger LOG = LoggerFactory.getLogger(ImportFuelTransactionsWsImpl.class);

	private static final String EXT_INTERFACE_NAME = "Import IATA Fuel Transactions";

	@Autowired
	private IFuelTransactionService ftSrv;
	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	private ExternalInterfaceHistoryFacade historyFacade;
	@Autowired
	private IFuelTransactionSourceService fuelTransactionSourceService;
	@Autowired
	private IDictionaryService dictionaryService;
	@Autowired
	private IExternalInterfaceService interfaceService;

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws BusinessException
	 * @throws SAXException
	 * @throws IOException
	 */
	@Override
	public ImportDocumentResponse importFt(ImportDocumentRequest request) throws JAXBException, BusinessException, SAXException, IOException {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		System.setProperty("jdk.xml.maxOccurLimit", "200000");
		int read = 0;
		int skip = 0;
		int inserted = 0;
		int updated = 0;
		this.userSrv.createSessionUser(request.getClientCode());
		ExternalInterface inter = this.interfaceService.findByName(EXT_INTERFACE_NAME);
		try {
			StringReader reader = new StringReader(request.getFtTransmission());
			this.validateXMLSchema(reader);
			JAXBContext jaxbContext = JAXBContext.newInstance(FuelTransactionTransmission.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			FuelTransactionTransmission ftTransmission = (FuelTransactionTransmission) unmarshaller
					.unmarshal(new StringReader(request.getFtTransmission()));
			Set<atraxo.ops.domain.impl.fuelTransaction.FuelTransaction> set = new HashSet<>();
			FuelTransactionSource fts = this.getSource(request.getNavisionParameters());
			ExternalInterface externalInterface = this.interfaceService.findByName(ExtInterfaceNames.IMPORT_IATA_FUEL_TRANSACTIONS.getValue());
			for (FuelTransaction item : ftTransmission.getFuelTransaction()) {
				read++;
				if (TicketStatus.F.equals(item.getIPTransaction().getHeader().getTicketNumber().getTicketStatus())) {
					this.dictionaryService.translate(externalInterface, item.getIPTransaction());
					atraxo.ops.domain.impl.fuelTransaction.FuelTransaction ft = FuelTransactionTransformer.buildEntityObject(item.getIPTransaction());
					this.ftSrv.determineSubsidiaryAtImport(ft); // calls the determineSubsidiary method for import
					if (fts != null) {
						ft.setImportSource(fts);
					}
					if (!set.add(ft)) {
						skip++;
					}
				} else {
					skip++;
				}
			}

			// block the threads access to only one if the case (#SONE-4134)
			synchronized (this.ftSrv) {
				FetchResult fetchresult = this.ftSrv.fetch(new ArrayList<>(set));
				inserted = fetchresult.getInserted();
				updated = fetchresult.getUpdated();
			}

			// this.fuelTransactionSourceService.fetch(ftsList);
			stopWatch.stop();
		} catch (JAXBException | IOException | BusinessException | SAXException e) {
			LOG.error(e.getMessage(), e);
			this.saveHistory(inter, stopWatch, ExternalInterfacesHistoryStatus._FAILURE_, request.getRequester(), e.getLocalizedMessage());
			throw e;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(String.format(BusinessErrorCode.IATA_FT_JOB_SKIPED.getErrMsg(), skip)).append("\r\n");
		sb.append(String.format(BusinessErrorCode.IATA_FT_JOB_READ.getErrMsg(), read)).append("\r\n");
		this.saveHistory(inter, stopWatch, ExternalInterfacesHistoryStatus._SUCCESS_, request.getRequester(), sb.toString());
		ImportDocumentResponse response = new ImportDocumentResponse();
		response.setReadCount(read);
		response.setSkipCount(skip);
		response.setInserted(inserted);
		response.setUpdated(updated);
		return response;
	}

	private FuelTransactionSource getSource(NavisionParameter navParam) throws BusinessException {
		if (navParam == null) {
			return null;
		}
		try {
			return this.fuelTransactionSourceService.findByName(navParam.getUniqueID());
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			LOG.info("Could not find FuelTransactionSource for name " + navParam.getUniqueID() + " , will create a new one!", nre);
			FuelTransactionSource fts = new FuelTransactionSource();
			fts.setName(navParam.getUniqueID());
			fts.setFuelTransactions(1);
			FuelTransactionSourceParameters ftsp = new FuelTransactionSourceParameters();
			ftsp.setParamName("uniqueID");
			ftsp.setParamValue(navParam.getUniqueID());
			fts.addToParameters(ftsp);
			this.fuelTransactionSourceService.insert(fts);
			return fts;
		}
	}

	private void saveHistory(ExternalInterface externalInterface, StopWatch stopWatch, ExternalInterfacesHistoryStatus status, String requester,
			String result) throws BusinessException {
		ExternalInterfaceHistory history = new ExternalInterfaceHistory();
		history.setExternalInterface(externalInterface);
		history.setRequester(requester);
		history.setRequestReceived(new Date(stopWatch.getStartTime()));
		history.setResponseSent(new Date(stopWatch.getStartTime() + stopWatch.getTime()));
		history.setStatus(status);
		history.setResult(result);
		this.historyFacade.insert(history);

	}

	private boolean validateXMLSchema(Reader reader) throws SAXException, IOException {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = factory.newSchema(ClassPathResource.class.getResource("/xsd/iata/IATAFuelTransactionStandard.xsd"));
		Validator validator = schema.newValidator();
		// TODO create an errorhandler
		// validator.setErrorHandler(errorHandler);
		validator.validate(new StreamSource(reader));
		return true;
	}

}
