/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.fuelTransaction.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.TypedQuery;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.cmm.domain.impl.cmm_type.FlightServiceType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.FuelTicketFuelingOperation;
import atraxo.cmm.domain.impl.cmm_type.Product;
import atraxo.fmbas.business.api.ac.IAcTypesService;
import atraxo.fmbas.business.api.currencies.ICurrenciesService;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.geo.ILocationsService;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.suppliers.ISuppliersService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.util.ExtInterfaceParameters;
import atraxo.fmbas.business.ws.ExternalInterfaceWSHelper;
import atraxo.fmbas.business.ws.client.PumaRestClient;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.ext.fuelTicket.ICancelFuelTicket;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.business.api.fuelTransaction.IFuelTransactionService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.business.ext.fuelTransaction.comparator.FuelTransactionQuantityComparator;
import atraxo.ops.business.ext.integration.FuelEventSource;
import atraxo.ops.domain.ext.DeliveryNoteMethod.DeliveryNoteMethod;
import atraxo.ops.domain.ext.fuelTicket.FetchResult;
import atraxo.ops.domain.ext.fuelTicket.ProcessResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionEquipment;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSourceParameters;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketCustomsStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketSource;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketTransmissionStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import atraxo.ops.domain.impl.ops_type.SendInvoiceBy;
import atraxo.ops.domain.impl.ops_type.Suffix;
import atraxo.ops.domain.impl.ops_type.TicketType;
import atraxo.ops.domain.impl.ops_type.ValidationState;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.InvalidEnumException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link FuelTransaction} domain entity.
 */
public class FuelTransaction_Service extends atraxo.ops.business.impl.fuelTransaction.FuelTransaction_Service implements IFuelTransactionService {

	private static final String SEPARATOR = " | ";

	private static final String UNIQUE_ID = "uniqueID";

	private static final String IATA_CODE = "iataCode";

	private static final String NO_SHIP_TO_IATA_CODE = "999";

	private static final Logger LOG = LoggerFactory.getLogger(FuelTransaction_Service.class);

	@Autowired
	private ILocationsService locSrv;
	@Autowired
	private ICustomerService customerSrv;
	@Autowired
	private ISuppliersService supplierSrv;
	@Autowired
	private IAcTypesService acTypeSrv;
	@Autowired
	private IUnitService unitSrv;
	@Autowired
	private ICurrenciesService currencySrv;
	@Autowired
	private IFuelTicketService ftSrv;
	@Autowired
	private IExternalInterfaceService extInterSrv;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageFacade;
	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IChangeHistoryService historyService;
	@Autowired
	private ExternalInterfaceHistoryFacade externalInterfaceHistoryFacade;
	@Autowired
	private ThreadPoolTaskExecutor taskExecutor;

	@Override
	@Transactional
	public FetchResult fetch(List<FuelTransaction> list) throws BusinessException {
		Iterator<? extends FuelTransaction> iter = list.iterator();
		List<FuelTransaction> existingList = this.findAll();
		List<FuelTransaction> updList = new ArrayList<>();
		List<FuelTransaction> insList = new ArrayList<>();
		while (iter.hasNext()) {
			FuelTransaction ft = iter.next();
			if (ft == null) {
				iter.remove();
			} else if (existingList.contains(ft)) {
				FuelTransaction existingFt = existingList.get(existingList.indexOf(ft));
				ft.setId(existingFt.getId());
				ft.setVersion(existingFt.getVersion());
				ft.setCreatedAt(existingFt.getCreatedAt());
				ft.setCreatedBy(existingFt.getCreatedBy());
				ft.setRefid(existingFt.getRefid());
				updList.add(ft);
			} else {
				insList.add(ft);
			}
		}
		this.insert(insList);
		this.update(updList);
		return new FetchResult(updList.size(), insList.size());
	}

	@Override
	public List<FuelTransaction> getFuelTranscation(Long offset) throws BusinessException {
		Date offsetDate = DateUtils.truncate(DateUtils.addDays(GregorianCalendar.getInstance().getTime(), offset.intValue() * -1),
				Calendar.DAY_OF_MONTH);
		StringBuilder hql = new StringBuilder();
		hql.append("select e from ").append(FuelTransaction.class.getSimpleName()).append(" e where e.clientId =:clientId");
		if (offset != -1) {
			hql.append(" and e.createdAt >:created_at");
		}
		TypedQuery<FuelTransaction> query = this.getEntityManager().createQuery(hql.toString(), FuelTransaction.class);
		if (offset != -1) {
			query.setParameter("created_at", offsetDate);
		}
		return query.setParameter("clientId", Session.user.get().getClientId()).getResultList();
	}

	@Override
	public List<FuelTransaction> findAll() throws BusinessException {
		Map<String, Object> params = Collections.emptyMap();
		return this.findEntitiesByAttributes(params);
	}

	@Override
	@Transactional
	public void process(List<FuelTransaction> list, List<FuelTicket> insList) throws BusinessException {
		List<FuelTransaction> updFtList = new ArrayList<>();
		List<Object> ids = new ArrayList<>();
		list.sort((FuelTransaction o1, FuelTransaction o2) -> this.compareFuelTransaction(o1, o2));
		this.processWithoutSave(list, insList);
		for (FuelTransaction ft : list) {
			if (ValidationState._EMPTY_.equals(ft.getValidationState())) {
				ids.add(ft.getId());
			} else {
				updFtList.add(ft);
			}
		}
		this.ftSrv.insert(insList);
		this.update(updFtList);
		if (!ids.isEmpty()) {
			List<FuelTransaction> delFtList = this.findByIds(ids);
			this.delete(delFtList);
		}
	}

	@Override
	@Transactional
	public void saveProcessedResult(ProcessResult result) throws BusinessException {
		for (FuelTicket ft : result.getInsList()) {
			this.ftSrv.insert(ft);
		}
		for (FuelTicket ft : result.getUpdList()) {
			this.ftSrv.update(ft);
		}
		this.update(result.getUpdFtList());
		this.delete(result.getDelFtList());
	}

	@Override
	public void processWithoutSave(List<FuelTransaction> list, List<FuelTicket> insList) throws BusinessException {
		for (FuelTransaction ft : list) {
			FuelTicket ticket = this.internalProcess(ft);
			if (ValidationState._EMPTY_.equals(ft.getValidationState())) {
				try {
					FuelTicket existingTicket = this.ftSrv.findByBusinessKey(ticket.getDeparture(), ticket.getTicketNo(), ticket.getCustomer(),
							ticket.getDeliveryDate());
					if (ticket.getIsDeleted()) {
						StringBuilder sb = new StringBuilder(ft.getValidationResult());
						if (!BillStatus._NOT_BILLED_.equals(existingTicket.getBillStatus())
								|| !OutgoingInvoiceStatus._NOT_INVOICED_.equals(existingTicket.getInvoiceStatus())) {
							this.appendErrorMessage(new StringBuilder(ft.getValidationResult()),
									OpsErrorCode.FUEL_TICKET_ALREADY_INVOICED.getErrMsg());
							ft.setValidationResult(sb.toString());
							ft.setValidationState(ValidationState._FAILED_);
						} else if (FuelTicketApprovalStatus._OK_.equals(existingTicket.getApprovalStatus())
								&& !FuelTicketStatus._CANCELED_.equals(existingTicket.getTicketStatus())) {
							this.appendErrorMessage(sb, OpsErrorCode.FUEL_TICKET_ORIGINAL_APPROVED.getErrMsg());
							ft.setValidationResult(sb.toString());
							ft.setValidationState(ValidationState._FAILED_);
						} else {
							existingTicket.setIsDeleted(true);
							this.ftSrv.update(existingTicket);
							FuelEventSource feSrc = new FuelEventSource(existingTicket, DeliveryNoteMethod.DELETE);
							this.sendMessage("maintainFuelEvents", feSrc);
							this.saveInHistory(existingTicket);
						}
						continue;
					}
					switch (ticket.getTicketStatus()) {
					case _CANCELED_:
						this.processCancelled(existingTicket);
						break;
					case _REISSUE_:
					case _ORIGINAL_:
					case _UPDATED_:
						this.processNotCancelled(ft, existingTicket, insList);
						break;
					default:
						break;
					}
				} catch (ApplicationException nre) {
					if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
						throw nre;
					}
					if (LOG.isDebugEnabled()) {
						LOG.debug(nre.getMessage(), nre);
					}
					if (ticket.getIsDeleted()) {
						FuelTicket ftInsert = this.getFromInsertList(insList, ticket);
						if (ftInsert != null) {
							ftInsert.setIsDeleted(true);
						}
					} else {
						insList.add(ticket);
					}
				}
			}
		}
		try {
			this.sendFuelTransactionProcessResult(list);
		} catch (BusinessException e) {
			LOG.warn("Send transaction process result failed!", e);
		}
	}

	private int compareFuelTransaction(FuelTransaction o1, FuelTransaction o2) {
		switch (o1.getTicketType()) {
		case _CANCEL_:
			if (TicketType._ORIGINAL_.equals(o2.getTicketType())) {
				return 1;
			} else {
				return -1;
			}
		case _DELETE_:
			return 1;
		case _EMPTY_:
			return 0;
		case _ORIGINAL_:
			return -1;
		case _REISSUE_:
			if (TicketType._ORIGINAL_.equals(o2.getTicketType()) || TicketType._CANCEL_.equals(o2.getTicketType())) {
				return 1;
			} else {
				return -1;
			}
		default:
			break;
		}
		return 0;
	}

	private FuelTicket getFromInsertList(List<FuelTicket> insertList, FuelTicket fuelTicket) {
		for (FuelTicket ft : insertList) {
			if (ft.getDeparture().equals(fuelTicket.getDeparture()) && ft.getTicketNo().equalsIgnoreCase(fuelTicket.getTicketNo())
					&& ft.getCustomer().equals(fuelTicket.getCustomer()) && ft.getDeliveryDate().equals(fuelTicket.getDeliveryDate())) {
				return ft;
			}
		}
		return null;
	}

	private void saveInHistory(FuelTicket existingTicket) throws BusinessException {
		ChangeHistory ch = new ChangeHistory();
		ch.setObjectId(existingTicket.getId());
		ch.setObjectType(existingTicket.getClass().getSimpleName());
		ch.setObjectValue("Delete");
		this.historyService.insert(ch);
	}

	private void sendFuelTransactionProcessResult(List<FuelTransaction> list) throws BusinessException {
		ExternalInterface extInterface = this.extInterSrv.findByName(ExtInterfaceNames.FUEL_TICKET_PROCESSING_RESULT.getValue());
		ExtInterfaceParameters params = new ExtInterfaceParameters(extInterface.getParams());
		boolean isBulkMode = Boolean.parseBoolean(params.getParamValue(ExternalInterfaceWSHelper.EXTERNAL_PARAM_BULK_MODE));

		WSFuelTransactionToDTOTransformer transformer = new WSFuelTransactionToDTOTransformer();

		Map<String, List<FuelTransaction>> map = this.groupTransactions(list);
		for (Entry<String, List<FuelTransaction>> entry : map.entrySet()) {
			params.put(ExternalInterfaceWSHelper.EXTERNAL_PARAM_WS_DESTINATION_ADDRESS, entry.getKey());
			params.put(ExternalInterfaceWSHelper.CAN_MODIFY_DESTINATION_ADDRESS, "FALSE");
			if (isBulkMode) {
				PumaRestClient<FuelTransaction> client = new PumaRestClient<>(this.taskExecutor, extInterface, params, entry.getValue(),
						this.externalInterfaceHistoryFacade, transformer, this.messageFacade);

				client.start(10);
			} else {
				for (FuelTransaction ft : entry.getValue()) {
					PumaRestClient<FuelTransaction> client = new PumaRestClient<>(this.taskExecutor, extInterface, params, Arrays.asList(ft),
							this.externalInterfaceHistoryFacade, transformer, this.messageFacade);

					client.start(10);
				}
			}
		}
	}

	private HashMap<String, List<FuelTransaction>> groupTransactions(List<FuelTransaction> ftList) {
		HashMap<String, List<FuelTransaction>> map = new HashMap<>();
		for (FuelTransaction ft : ftList) {
			if (ft.getImportSource() != null && !CollectionUtils.isEmpty(ft.getImportSource().getParameters())) {
				Collection<FuelTransactionSourceParameters> parameters = ft.getImportSource().getParameters();
				for (FuelTransactionSourceParameters param = parameters.iterator().next(); parameters.iterator().hasNext();) {
					if (UNIQUE_ID.equalsIgnoreCase(param.getParamName())) {
						String uniqueId = param.getParamValue();
						StringBuilder sb = new StringBuilder();
						if (!StringUtils.isEmpty(uniqueId) && uniqueId.length() > 2
								&& ".td".equalsIgnoreCase(uniqueId.substring(uniqueId.length() - 3))) {
							sb.append(uniqueId.substring(0, uniqueId.length() - 3));
							sb.append(".ACK");
						} else {
							sb.append(uniqueId);
						}
						this.addFuelTransactionToMap(map, ft, sb.toString());
						break;
					}
				}
			} else {
				String uniqueId = "";
				this.addFuelTransactionToMap(map, ft, uniqueId);
			}
		}
		return map;
	}

	private void addFuelTransactionToMap(HashMap<String, List<FuelTransaction>> map, FuelTransaction ft, String uniqueId) {
		if (map.containsKey(uniqueId)) {
			map.get(uniqueId).add(ft);
		} else {
			List<FuelTransaction> tempList = new ArrayList<>();
			tempList.add(ft);
			map.put(uniqueId, tempList);
		}
	}

	private void processNotCancelled(FuelTransaction ft, FuelTicket existingTicket, List<FuelTicket> insList) throws BusinessException {
		if (existingTicket.getIsDeleted() != null && existingTicket.getIsDeleted()) {
			existingTicket.setIsDeleted(false);
			existingTicket.setTicketStatus(this.transformTicketTypeToTicketStatus(ft.getTicketType()));
			existingTicket.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
			existingTicket.setApprovedBy("");
			existingTicket.setRevocationRason(RevocationReason._EMPTY_);
			this.ftSrv.update(existingTicket);
			insList.add(existingTicket);
		} else {
			ft.setValidationState(ValidationState._FAILED_);
			StringBuilder sb = new StringBuilder(ft.getValidationResult());
			switch (existingTicket.getTicketStatus()) {
			case _ORIGINAL_:
				this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_DUPLICATE.getErrMsg());
				break;
			case _UPDATED_:
				this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_REISSUED.getErrMsg());
				break;
			case _CANCELED_:
				this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_CANCELED.getErrMsg());
				break;
			default:
				break;
			}
			ft.setValidationResult(sb.toString());
		}
	}

	private FuelTicketStatus transformTicketTypeToTicketStatus(TicketType ticketType) {
		switch (ticketType) {
		case _CANCEL_:
			return FuelTicketStatus._CANCELED_;
		case _ORIGINAL_:
			return FuelTicketStatus._ORIGINAL_;
		case _REISSUE_:
			return FuelTicketStatus._REISSUE_;
		default:
			return FuelTicketStatus._EMPTY_;
		}
	}

	private void processCancelled(FuelTicket existingTicket) throws BusinessException {
		switch (existingTicket.getTicketStatus()) {
		case _ORIGINAL_:
		case _UPDATED_:
			existingTicket.setTicketStatus(FuelTicketStatus._CANCELED_);
			existingTicket.setRevocationRason(RevocationReason._COMMERCIAL_REASON_);
			this.ftSrv.update(existingTicket);
			List<FuelTicket> tickets = new ArrayList<>();
			tickets.add(existingTicket);
			try {
				ICancelFuelTicket cancelFuelTicketService = this.getApplicationContext().getBean("fuelTicketCancel", ICancelFuelTicket.class);
				cancelFuelTicketService.cancelFuelTickets(tickets);
			} catch (NoSuchBeanDefinitionException ex) {
				LOG.warn("FuelTicketCancel bean not found!", ex);
			}
			break;
		default:
			break;
		}
	}

	@Override
	@Transactional
	public FuelTicket process(FuelTransaction ft) throws BusinessException {
		FuelTicket ticket = this.internalProcess(ft);
		if (ft.getValidationState().equals(ValidationState._EMPTY_)) {
			this.ftSrv.insert(ticket);
		}
		this.update(ft);
		if (ticket.getIsDeleted()) {
			FuelEventSource feSrc = new FuelEventSource(ticket, DeliveryNoteMethod.DELETE);
			this.sendMessage("maintainFuelEvents", feSrc);
		}
		return ticket;

	}

	private FuelTicket internalProcess(FuelTransaction ft) throws BusinessException {
		FuelTicket ticket = new FuelTicket();
		ticket.setIsDeleted(false);
		this.setEquipment(ft, ticket);
		this.validateFuelTicket(ft, ticket);
		ticket.setTicketNo(ft.getTicketNumber());
		ticket.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
		ticket.setBillStatus(BillStatus._NOT_BILLED_);
		ticket.setInvoiceStatus(OutgoingInvoiceStatus._NOT_INVOICED_);
		ticket.setSendInvoiceBy(SendInvoiceBy._EMPTY_);
		ticket.setSuffix(Suffix._EMPTY_);
		ticket.setRevocationRason(RevocationReason._EMPTY_);
		ticket.setTransmissionStatus(FuelTicketTransmissionStatus._NEW_);
		this.setFlightType(ft, ticket);
		this.setTicketType(ft, ticket);
		this.setTicketSource(ft, ticket);
		ticket.setDeliveryDate(ft.getTransactionDate());

		this.setFlightTypeIndicator(ft, ticket);
		this.setFinalDestination(ft, ticket);
		this.setDestination(ft, ticket);
		ticket.setPaymentType(ft.getPaymentType());
		ticket.setCardHolder(ft.getCardName());
		this.setcardNumber(ft, ticket);
		ticket.setCardExpiringDate(ft.getCardExpiry());
		ticket.setAmountReceived(ft.getAmountReceived());
		this.setAmountCurrency(ft, ticket);
		this.setIpTransactionCode(ft, ticket);
		try {
			this.setProduct(ft, ticket);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			LOG.warn("Product type not found.", nre);
		}
		this.setCustomStatus(ft, ticket);

		return ticket;
	}

	private void setAmountCurrency(FuelTransaction ft, FuelTicket ticket) {
		try {
			ticket.setAmmountCurr(this.currencySrv.findByCode(ft.getAmountReceivedCurr()));
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			LOG.warn("Amount currency not found.", nre);
		}
	}

	private void setcardNumber(FuelTransaction ft, FuelTicket ticket) {
		if (ft.getCardNumber() != null) {
			try {
				ticket.setCardNumber(ft.getCardNumber());
			} catch (NumberFormatException nfe) {
				LOG.warn("Invalid card number: " + ft.getCardNumber(), nfe);
			}
		}
	}

	private void setDestination(FuelTransaction ft, FuelTicket ticket) {
		ticket.setDestination(this.findLocation(ft.getNextDestination()));
	}

	private void setFinalDestination(FuelTransaction ft, FuelTicket ticket) {
		ticket.setFinalDest(this.findLocation(ft.getFinalDestination()));
	}

	private void setCustomStatus(FuelTransaction ft, FuelTicket ticket) {
		switch (ft.getCustomStatus()) {
		case _BONDED_:
			ticket.setCustomsStatus(FuelTicketCustomsStatus._BONDED_);
			break;
		case _DOMESTIC_:
			ticket.setCustomsStatus(FuelTicketCustomsStatus._DOMESTIC_);
			break;
		case _FTZ_:
			ticket.setCustomsStatus(FuelTicketCustomsStatus._FTZ_);
			break;
		default:
			ticket.setCustomsStatus(FuelTicketCustomsStatus._UNSPECIFIED_);
			break;
		}
	}

	private void setProduct(FuelTransaction ft, FuelTicket ticket) {
		switch (ft.getProductId()) {
		case _JETA_:
			ticket.setProductType(Product._JET_A_);
			break;
		case _JETA1_:
			ticket.setProductType(Product._JET_A1_);
			break;
		case _TS1_:
			ticket.setProductType(Product._TS_1_);
			break;
		case _JP8_:
			ticket.setProductType(Product._JP_8_);
			break;
		case _JA1A_:
			ticket.setProductType(Product._A1_BIO_);
			break;
		case _AG100_:
			ticket.setProductType(Product._AVGAS_);
			break;
		default:
			ticket.setProductType(Product._EMPTY_);
			break;
		}
	}

	private void setIpTransactionCode(FuelTransaction ft, FuelTicket ticket) {
		switch (ft.getIpTransactionCode()) {
		case _UPLIFT_:
			ticket.setFuelingOperation(FuelTicketFuelingOperation._UPLIFT_);
			break;
		case _DE_FUEL_:
			ticket.setFuelingOperation(FuelTicketFuelingOperation._DE_FUEL_);
			break;
		case _RE_FUEL_:
			ticket.setFuelingOperation(FuelTicketFuelingOperation._RE_FUEL_);
			break;
		default:
			ticket.setFuelingOperation(FuelTicketFuelingOperation._EMPTY_);
			break;
		}
	}

	private void setFlightTypeIndicator(FuelTransaction ft, FuelTicket ticket) {
		switch (ft.getInternationalFlight()) {
		case _DOMESTIC_:
			ticket.setIndicator(FlightTypeIndicator._DOMESTIC_);
			break;
		case _INTERNATIONAL_:
			ticket.setIndicator(FlightTypeIndicator._INTERNATIONAL_);
			break;
		default:
			ticket.setIndicator(FlightTypeIndicator._UNSPECIFIED_);
			break;
		}
	}

	private void setTicketSource(FuelTransaction ft, FuelTicket ticket) {
		switch (ft.getTicketSource()) {
		case _ELECTRONIC_:
			ticket.setSource(FuelTicketSource._IMPORT_);
			break;
		case _MANUAL_:
			ticket.setSource(FuelTicketSource._MANUAL_);
			break;
		default:
			ticket.setSource(FuelTicketSource._EMPTY_);
			break;
		}
	}

	private void setTicketType(FuelTransaction ft, FuelTicket ticket) {
		switch (ft.getTicketType()) {
		case _CANCEL_:
			ticket.setTicketStatus(FuelTicketStatus._CANCELED_);
			break;
		case _ORIGINAL_:
			ticket.setTicketStatus(FuelTicketStatus._ORIGINAL_);
			break;
		case _REISSUE_:
			ticket.setTicketStatus(FuelTicketStatus._REISSUE_);
			break;
		case _DELETE_:
			ticket.setIsDeleted(true);
			ticket.setTicketStatus(FuelTicketStatus._ORIGINAL_);
			break;
		default:
			ticket.setTicketStatus(FuelTicketStatus._EMPTY_);
			break;
		}
	}

	private void setFlightType(FuelTransaction ft, FuelTicket ticket) {
		switch (ft.getFlightServiceType()) {
		case _AD_HOC_:
			ticket.setFlightServiceType(FlightServiceType._AD_HOC_);
			break;
		case _SCHEDULED_:
			ticket.setFlightServiceType(FlightServiceType._SCHEDULED_);
			break;
		default:
			ticket.setFlightServiceType(FlightServiceType._EMPTY_);
			break;
		}
	}

	private void setRegistration(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) throws BusinessException {
		Customer customer = ticket.getCustomer();
		if (customer == null) {
			return;
		}
		if (StringUtils.isEmpty(ft.getAircraftIdentification())) {
			this.appendErrorMessage(sb, OpsErrorCode.FUEL_TICKET_AIRCRAFT_REGISTRATION_NUMBER.getErrMsg());
			return;
		}
		Aircraft aircraft = this.getAircraft(ft, ticket);
		if (aircraft != null) {
			ticket.setRegistration(aircraft);
		} else {
			Aircraft craft = this.buildAircraft(ft, ticket, customer);
			customer.addToAircraft(craft);
			this.customerSrv.update(customer);
			Aircraft customerAircraft = this.getAircraft(ft, ticket);
			ticket.setRegistration(customerAircraft);
		}
	}

	private Aircraft buildAircraft(FuelTransaction ft, FuelTicket ticket, Customer customer) {
		Aircraft craft = new Aircraft();
		craft.setAcTypes(ticket.getAcType());
		craft.setCustomer(customer);
		craft.setOperator(customer);
		craft.setOwner(customer);
		craft.setRegistration(ft.getAircraftIdentification());
		Date time = GregorianCalendar.getInstance().getTime();
		craft.setValidFrom(time);
		craft.setValidTo(DateUtils.addYears(time, 10));
		craft.setCreatedBy("IMPORTED");
		craft.setModifiedBy("IMPORTED");
		return craft;
	}

	private Aircraft getAircraft(FuelTransaction ft, FuelTicket ticket) {
		for (Aircraft aircraft : ticket.getCustomer().getAircraft()) {
			if (ft.getAircraftIdentification().equalsIgnoreCase(aircraft.getRegistration())) {
				return aircraft;
			}
		}
		return null;
	}

	private void validateFuelTicket(FuelTransaction ft, FuelTicket ticket) throws BusinessException {
		StringBuilder sb = new StringBuilder();
		this.setQuantity(ft, ticket, sb);
		this.setDeparture(ft, ticket, sb);
		this.setAircraftType(ft, ticket, sb);
		this.setCustomer(ft, ticket, sb);
		this.setRegistration(ft, ticket, sb);
		this.setSupplier(ft, ticket, sb);
		this.setFueller(ft, ticket, sb);
		this.setAirlineFlightID(ft, ticket, sb);
		this.setSubsididiary(ft, ticket, sb);
		switch (ft.getTicketType()) {
		case _CANCEL_:
			this.validateCanceledFuelTicket(ft, ticket, sb);
			break;
		case _REISSUE_:
			this.validateReissueFuelTicket(ft, ticket, sb);
			break;
		default:
			break;
		}

		ft.setValidationResult(sb.toString());
		if (ft.getValidationResult().isEmpty()) {
			ft.setValidationState(ValidationState._EMPTY_);
		} else {
			ft.setValidationState(ValidationState._FAILED_);
		}
	}

	private void setAirlineFlightID(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) {
		String flightId = ft.getAirlineFlightId();
		if (flightId.isEmpty()) {
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_FLIGHT_ID.getErrMsg());
		}
		if (flightId.length() < 3) {
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_INCORRECT_FLIGHT_ID.getErrMsg());
			return;
		}
		try {
			if (ft.getAirlineFlightId().length() >= 6) {
				ticket.setAirlineDesignator(ft.getAirlineFlightId().substring(0, 2));
				ticket.setFlightNo(ft.getAirlineFlightId().substring(2, 6));
				if (ft.getAirlineFlightId().length() > 6) {
					ticket.setSuffix(Suffix.getByCode(ft.getAirlineFlightId().substring(6)));
				}
			} else if (ft.getAirlineFlightId().length() >= 3) {
				ticket.setAirlineDesignator(ft.getAirlineFlightId().substring(0, 2));
				ticket.setFlightNo(ft.getAirlineFlightId().substring(2, ft.getAirlineFlightId().length()));
			}
		} catch (InvalidEnumException ex) {
			if (LOG.isDebugEnabled()) {
				LOG.debug(ex.getMessage(), ex);
			}
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_INCORRECT_FLIGHT_ID.getErrMsg());
		}

	}

	private void setSubsididiary(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) throws BusinessException {
		boolean flag = false;
		int minNumberOfLocations = Integer.MAX_VALUE;

		if (ft.getTicketIssuerCode() != null && !ft.getTicketIssuerCode().isEmpty()) {
			ticket.setSubsidiaryCode(ft.getTicketIssuerCode());
			ticket.setSubsidiaryId(this.customerService.findByCode(ft.getTicketIssuerCode()).getRefid());
		} else {
			List<Customer> possibleSubsidiaries = this.getPossibleSubsidiaries(ft);

			if (possibleSubsidiaries.isEmpty()) {
				this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_LOCATION_IS_NOT_ASSIGNED_TO_ANY_SUBSIDIARY_AREA.getErrMsg());
				return;
			}

			Customer subsidiary = possibleSubsidiaries.get(0);
			for (Customer entry : possibleSubsidiaries) {
				if (entry.getAssignedArea().getLocations().size() < minNumberOfLocations) {
					minNumberOfLocations = entry.getAssignedArea().getLocations().size();
					subsidiary = entry;
					flag = false;
				} else if (entry.getAssignedArea().getLocations().size() == minNumberOfLocations) {
					flag = true;
				}
			}

			// the error is logged if there are found many subsidiaries with same number of locations
			if (flag) {
				this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_SUBSIDIARY_COULD_NOT_BE_DETERMINED.getErrMsg());
			} else {
				ticket.setSubsidiaryCode(subsidiary.getCode());
				ticket.setSubsidiaryId(subsidiary.getRefid());
			}
		}
	}

	private void validateReissueFuelTicket(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) {
		if (!StringUtils.hasLength(ft.getPreviousTicket())) {
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_REFERENCE.getErrMsg());
			return;
		}
		try {
			FuelTicket refFuelTicket = this.ftSrv.findByBusinessKey(ticket.getDeparture(), ft.getPreviousTicket(), ticket.getCustomer(),
					ft.getTransactionDate());
			if (FuelTicketStatus._CANCELED_.equals(refFuelTicket.getTicketStatus()) && !refFuelTicket.getIsDeleted()) {
				List<FuelTicket> refTickets = this.ftSrv.findByReferenceTicketId(refFuelTicket.getId());
				if (refTickets != null) {
					Iterator<FuelTicket> iter = refTickets.iterator();
					while (iter.hasNext()) {
						FuelTicket tkt = iter.next();
						if (tkt.getIsDeleted()) {
							iter.remove();
						}
					}
					if (!refTickets.isEmpty()) {
						this.appendErrorMessage(sb, OpsErrorCode.FUEL_TICKET_CAN_NOT_BE_REISSUED.getErrMsg());
					} else {
						ticket.setReferenceTicket(refFuelTicket);
					}
				}
			} else {
				this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_REFERENCE.getErrMsg());
			}
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug(nre.getMessage(), nre);
			}
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_REFERENCE.getErrMsg());
		}
	}

	private void validateCanceledFuelTicket(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) {
		try {
			FuelTicket originalFuelTicket = this.ftSrv.findByBusinessKey(ticket.getDeparture(), ft.getTicketNumber(), ticket.getCustomer(),
					ft.getTransactionDate());
			if (FuelTicketStatus._CANCELED_.equals(originalFuelTicket.getTicketStatus())) {
				this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_ALREADY_CANCELED.getErrMsg());
			}
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug(nre.getMessage(), nre);
			}
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_ORIGINAL.getErrMsg());
		}
	}

	private void setCustomer(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		if (ft.getReceiverCode().equals(NO_SHIP_TO_IATA_CODE)) {
			params.clear();
			params.put("accountNumber", ft.getReceiverAccountNumber());
			List<Customer> list = this.customerSrv.findEntitiesByAttributes(params);
			if (list.size() == 1) {
				Customer cust = list.get(0);
				ticket.setCustomer(cust);
			} else {
				this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_CUSTOMER.getErrMsg());
			}
			return;
		}
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("accountNumber", ft.getReceiverAccountNumber());
		map.put("iataCode", ft.getReceiverCode());
		map.put("icaoCode", ft.getReceiverCode());
		map.put("code", ft.getReceiverCode());
		for (Entry<String, Object> entry : map.entrySet()) {
			params.clear();
			params.put(entry.getKey(), entry.getValue());
			List<Customer> list = this.customerSrv.findEntitiesByAttributes(params);
			if (list.size() == 1) {
				Customer cust = list.get(0);
				ticket.setCustomer(cust);
				return;
			}
			LOG.info("No/Multiple customer(s) found with " + entry.getKey() + " !");
		}
		this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_CUSTOMER.getErrMsg());
	}

	private void setDeparture(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) {
		String airportCode = ft.getAirportCode();
		Locations departureLocation = this.findLocation(airportCode);
		if (departureLocation == null) {
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_AIRPORT.getErrMsg());
		} else {
			ticket.setDeparture(departureLocation);
		}
	}

	private void setFueller(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) {
		try {
			ticket.setFueller(this.supplierSrv.findByCode(ft.getIntoPlaneCode()));
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug(nre.getMessage(), nre);
			}
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_IPL_AGENT.getErrMsg());
		}
	}

	private void setSupplier(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) {
		try {
			ticket.setSupplier(this.supplierSrv.findByCode(ft.getSupplierCode()));
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug(nre.getMessage(), nre);
			}
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_SUPPLER.getErrMsg());
		}
	}

	private void setAircraftType(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) throws BusinessException {
		try {
			AcTypes acType = this.acTypeSrv.findByCode(ft.getAircraftType());
			if (acType.getTankCapacity() != null && acType.getUnit() != null) {
				if (ticket.getUpliftVolume() != null) {
					BigDecimal uplift = this.unitSrv.convert(ticket.getUpliftUnit(), acType.getUnit(), ticket.getUpliftVolume(),
							ticket.getDensity().doubleValue(), ticket.getDensityUnit(), ticket.getDensityVolume());
					if (acType.getTankCapacity().compareTo(uplift.intValue()) < 0) {
						this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_UPLIFT_OVERDUE.getErrMsg());
					}
				} else {
					this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_UPLIFT.getErrMsg());
				}
			}
			ticket.setAcType(acType);
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			LOG.info("Could not finf AcTypes, will append 'No Aircraft Type' to the error message", nre);
			this.appendErrorMessage(sb, OpsErrorCode.FT_VALIDATION_NO_AICRAFT_TYPE.getErrMsg());
		}
	}

	private void setEquipment(FuelTransaction ft, FuelTicket ticket) {
		if (ft.getEquipment() != null) {
			FuelTransactionEquipment equipment = ft.getEquipment().iterator().next();
			ticket.setTransport(equipment.getFuelingType());
			ticket.setTemperature(equipment.getAvgFuelTemperature().intValue());
			ticket.setTemperatureUnit(equipment.getTemperatureUOM());
			ticket.setMeterStartIndex(equipment.getMeterReadingStart().intValue());
			ticket.setMeterEndIndex(equipment.getMeterReadingEnd().intValue());
			Map<String, Object> params = Collections.emptyMap();
			List<Unit> list = this.unitSrv.findEntitiesByAttributes(params);
			ticket.setDensity(equipment.getDensity());
			switch (equipment.getDensityUOM()) {
			case _KGL_:
				this.setDensityUnit(ticket, list, "KG", "LT");
				break;
			case _KGM_:
				this.setDensityUnit(ticket, list, "KG", "M3");
				break;
			case _LGH_:
				this.setDensityUnit(ticket, list, "LB", "UG");
				break;
			default:
				break;
			}
		}
	}

	private void setDensityUnit(FuelTicket ticket, List<Unit> list, String weightCode, String volumeCode) {
		for (Unit unit : list) {
			if (unit.getCode().equalsIgnoreCase(weightCode)) {
				ticket.setDensityUnit(unit);
			} else if (unit.getCode().equalsIgnoreCase(volumeCode)) {
				ticket.setDensityVolume(unit);
			}
		}
	}

	private void setQuantity(FuelTransaction ft, FuelTicket ticket, StringBuilder sb) throws BusinessException {
		List<FuelTransactionQuantity> quantities = new ArrayList<>(ft.getQuantity());
		try {
			Collections.sort(quantities, new FuelTransactionQuantityComparator((IUnitService) this.findEntityService(Unit.class)));
			for (FuelTransactionQuantity qty : quantities) {

				Map<String, Object> params = new HashMap<>();
				params.put(IATA_CODE, qty.getQuantityUOM());
				Unit unit = this.unitSrv.findEntityByAttributes(params);

				switch (qty.getQuantityType()) {
				case _NET_:
					ticket.setNetUpliftQuantity(qty.getFuelQuantity());
					ticket.setNetUpliftUnit(unit);
					break;
				case _GROSS_:
					if (ticket.getUpliftVolume() == null) {
						ticket.setUpliftVolume(qty.getFuelQuantity());
						ticket.setUpliftUnit(unit);
					}
					break;
				default:
					break;
				}
			}
		} catch (ApplicationException nre) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
				throw nre;
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug(nre.getMessage(), nre);
			}
			this.appendErrorMessage(sb, BusinessErrorCode.INVALID_UNIT_OF_MEASURE.getErrMsg());
		}

	}

	/*
	 * Determines the right subsidiary from a list of possible subsidiaries based on the smallest size Area that contains the fuelTransaction
	 * departure location
	 */
	@Override
	public void determineSubsidiaryAtImport(FuelTransaction ft) throws BusinessException {
		List<Customer> possibleSubsidiaries = this.getPossibleSubsidiaries(ft);
		if (possibleSubsidiaries.isEmpty()) {
			return;
		}

		int minNumberOfLocations = Integer.MAX_VALUE;
		Boolean sameNumberOfLocations = false;
		Customer subsidiary = possibleSubsidiaries.get(0);
		for (Customer entry : possibleSubsidiaries) {
			if (entry.getAssignedArea().getLocations().size() < minNumberOfLocations) {
				minNumberOfLocations = entry.getAssignedArea().getLocations().size();
				subsidiary = entry;
				sameNumberOfLocations = false;
			} else if (entry.getAssignedArea().getLocations().size() == minNumberOfLocations) {
				sameNumberOfLocations = true;
			}
		}

		if (sameNumberOfLocations) {
			return;
		} else {
			ft.setTicketIssuerName(subsidiary.getName());
			ft.setTicketIssuerCode(subsidiary.getCode());
		}
	}

	/**
	 * Gets all subsidiaries that have the airport code defined in their areas
	 *
	 * @param ft
	 * @return
	 * @throws BusinessException
	 */
	private List<Customer> getPossibleSubsidiaries(FuelTransaction ft) throws BusinessException {
		List<Customer> subsidiaries = this.customerService.findSubsidiaries();
		List<Customer> possibleSubsidiaries = new ArrayList<>();

		for (Customer subsidiary : subsidiaries) {
			if (subsidiary.getAssignedArea() != null) {
				for (Locations location : subsidiary.getAssignedArea().getLocations()) {
					if (this.locationEqualToAirportLocation(ft, location)) {
						possibleSubsidiaries.add(subsidiary);
						break;
					}
				}
			}
		}
		return possibleSubsidiaries;
	}

	private boolean locationEqualToAirportLocation(FuelTransaction ft, Locations location) {
		return location.getCode().equalsIgnoreCase(ft.getAirportCode()) || location.getIataCode().equalsIgnoreCase(ft.getAirportCode())
				|| location.getIcaoCode().equalsIgnoreCase(ft.getAirportCode());
	}

	private void appendErrorMessage(StringBuilder sb, String message) {
		String str = sb.toString();
		if (str.isEmpty()) {
			sb.append(message);
		} else {
			sb.append(SEPARATOR).append(message);
		}
	}

	private Locations findLocation(String locationCode) {
		Locations location = null;
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("iataCode", locationCode);
		map.put("icaoCode", locationCode);
		map.put("code", locationCode);
		Map<String, Object> params = new HashMap<>();
		for (Entry<String, Object> entry : map.entrySet()) {
			params.clear();
			params.put(entry.getKey(), entry.getValue());
			List<Locations> list = this.locSrv.findEntitiesByAttributes(params);
			if (list.size() == 1) {
				location = list.get(0);
				break;
			}
			LOG.info("No/Multiple location(s) found with " + entry.getKey() + " !");
		}
		return location;
	}

	@Override
	public Integer count() throws BusinessException {
		List<FuelTransaction> list = this.findAll();
		return list == null ? 0 : list.size();
	}
}
