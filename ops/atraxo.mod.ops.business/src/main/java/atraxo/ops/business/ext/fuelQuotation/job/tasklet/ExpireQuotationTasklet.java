package atraxo.ops.business.ext.fuelQuotation.job.tasklet;

import org.junit.Assert;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ops.business.api.fuelQuotation.IFuelQuotationService;

public class ExpireQuotationTasklet implements Tasklet, InitializingBean {

	@Autowired
	private IFuelQuotationService service;

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.assertNotNull(this.service);

	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		this.service.expire();
		return RepeatStatus.FINISHED;
	}

}
