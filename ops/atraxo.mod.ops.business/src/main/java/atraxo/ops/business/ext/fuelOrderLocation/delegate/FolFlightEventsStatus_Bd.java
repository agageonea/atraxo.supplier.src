package atraxo.ops.business.ext.fuelOrderLocation.delegate;

import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.ops_type.FlightEventStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

/**
 * Recalculate Fuel order location quantity on flight event status change.
 */
public class FolFlightEventsStatus_Bd extends AbstractBusinessDelegate {

	/**
	 * Recalculate Fuel order location quantity on flight event status change.
	 *
	 * @param flightEvent
	 * @param fes
	 *            - {@link FlightEventStatus} - CANCELD or SCHEDULED.
	 * @throws BusinessException
	 */
	public void recalculateEvents(FlightEvent flightEvent, FlightEventStatus fes) throws BusinessException {
		if (flightEvent.getLocOrder() == null) {
			return;
		}
		IFuelOrderLocationService service = (IFuelOrderLocationService) this.findEntityService(FuelOrderLocation.class);
		FuelOrderLocation fol = service.findById(flightEvent.getLocOrder().getId());
		Integer events = flightEvent.getEvents();
		Integer totalEvents = fol.getEvents();
		if (totalEvents == null || events == null || !(fes.equals(FlightEventStatus._CANCELED_) || fes.equals(FlightEventStatus._SCHEDULED_))) {
			return;
		}
		fol.setEvents(totalEvents + (fes.equals(FlightEventStatus._CANCELED_) ? events * (-1) : events));
		service.update(fol);
	}

}
