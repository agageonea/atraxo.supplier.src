package atraxo.ops.business.ext.fuelTransaction.comparator;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import atraxo.ops.domain.impl.ops_type.FuelQuantityType;

/**
 * @author apetho
 */
public class FuelTransactionQuantityComparator implements Comparator<FuelTransactionQuantity> {

	private static final String IATA_CODE = "iataCode";

	private IUnitService unitService;

	/**
	 * @param unitService
	 */
	public FuelTransactionQuantityComparator(IUnitService unitService) {
		this.unitService = unitService;
	}

	@Override
	public int compare(FuelTransactionQuantity o1, FuelTransactionQuantity o2) {
		if (FuelQuantityType._GROSS_.equals(o1.getQuantityType()) && FuelQuantityType._NET_.equals(o2.getQuantityType())) {
			return -1;
		} else if (FuelQuantityType._NET_.equals(o1.getQuantityType()) && FuelQuantityType._GROSS_.equals(o2.getQuantityType())) {
			return 1;
		} else {
			Map<String, Object> params = new HashMap<>();
			params.put(IATA_CODE, o1.getQuantityUOM());
			Unit unit1 = this.unitService.findEntityByAttributes(params);
			params.put(IATA_CODE, o2.getQuantityUOM());
			Unit unit2 = this.unitService.findEntityByAttributes(params);
			if (UnitType._VOLUME_.equals(unit1.getUnittypeInd()) && UnitType._MASS_.equals(unit2.getUnittypeInd())) {
				return -1;
			} else if (UnitType._MASS_.equals(unit1.getUnittypeInd()) && UnitType._MASS_.equals(unit2.getUnittypeInd())) {
				return 1;
			}
		}
		return 0;
	}

}
