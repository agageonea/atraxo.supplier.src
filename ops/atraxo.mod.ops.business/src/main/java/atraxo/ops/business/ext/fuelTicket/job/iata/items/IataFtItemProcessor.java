package atraxo.ops.business.ext.fuelTicket.job.iata.items;

import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ResourceAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import atraxo.fmbas.business.api.dictionary.IDictionaryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.ops.business.api.fuelTransaction.IFuelTransactionSourceService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.business.ext.fuelTicket.job.iata.listener.IataFtListener;
import atraxo.ops.business.ext.fuelTransaction.service.FuelTransactionTransformer;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSourceParameters;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.iata.fuelplus.iata.fuelticket.FuelTransaction;
import seava.j4e.iata.fuelplus.iata.fuelticket.TicketStatus;

public class IataFtItemProcessor implements ItemProcessor<FuelTransaction, atraxo.ops.domain.impl.fuelTransaction.FuelTransaction>, ResourceAware {

	private static final Logger logger = LoggerFactory.getLogger(IataFtItemProcessor.class);

	private static final String READ_COUNT = "readCount";
	private static final String SKIP_COUNT = "skipCount";
	private Resource resource;
	private StepExecution stepExecution;

	@Autowired
	private IFuelTransactionSourceService srv;

	@Autowired
	private IDictionaryService dictionaryService;

	@Autowired
	private IExternalInterfaceService interfaceService;

	/**
	 * @param stepExecution
	 */
	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
	 */
	@Override
	public atraxo.ops.domain.impl.fuelTransaction.FuelTransaction process(FuelTransaction item) throws Exception {
		FuelTransactionSource ftFile = this.getImportFile();
		ExecutionContext executionContext = this.stepExecution.getJobExecution().getExecutionContext();
		try {
			this.setReadCount(executionContext);
			if (TicketStatus.F.equals(item.getIPTransaction().getHeader().getTicketNumber().getTicketStatus())) {
				atraxo.ops.domain.impl.fuelTransaction.FuelTransaction ft = FuelTransactionTransformer.buildEntityObject(item.getIPTransaction());
				ft.setImportSource(ftFile);
				ExternalInterface externalInterface = this.interfaceService.findByName(ExtInterfaceNames.IMPORT_IATA_FUEL_TRANSACTIONS.getValue());
				this.dictionaryService.translate(externalInterface, ft);
				return ft;
			}
			this.setSkipCount(executionContext);
			return null;
		} catch (Exception e) {
			this.stepExecution.addFailureException(e);
			StringBuilder sb = new StringBuilder();
			if (executionContext.containsKey(IataFtListener.FAILURE_MSG)) {
				sb.append(executionContext.getString(IataFtListener.FAILURE_MSG));
				sb.append("\r\n");
				sb.append(String.format(OpsErrorCode.FT_PROCESS_FAILURE.getErrMsg(), item.getIPTransaction().getHeader().getTicketNumber().getValue(),
						ftFile.getName()));
			} else {
				sb.append(String.format(OpsErrorCode.FT_PROCESS_FAILURE.getErrMsg(), item.getIPTransaction().getHeader().getTicketNumber().getValue(),
						ftFile.getName()));
			}
			executionContext.put(IataFtListener.FAILURE_MSG, sb.toString());
			return null;
		}
	}

	private void setReadCount(ExecutionContext executionContext) {
		if (!executionContext.containsKey(READ_COUNT)) {
			executionContext.putInt(READ_COUNT, 1);
		} else {
			executionContext.putInt(READ_COUNT, executionContext.getInt(READ_COUNT) + 1);
		}
	}

	private void setSkipCount(ExecutionContext executionContext) {
		if (!executionContext.containsKey(SKIP_COUNT)) {
			executionContext.putInt(SKIP_COUNT, 1);
		} else {
			executionContext.putInt(SKIP_COUNT, executionContext.getInt(SKIP_COUNT) + 1);
		}
	}

	private FuelTransactionSource getImportFile() throws BusinessException {
		FuelTransactionSource ftFile = new FuelTransactionSource();
		ftFile.setProcessingDate(GregorianCalendar.getInstance().getTime());
		try {
			ftFile = this.srv.findByName(this.resource.getFilename());
			ftFile.setFuelTransactions(ftFile.getFuelTransactions() + 1);
			this.srv.update(ftFile);
		} catch (ApplicationException e) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(e.getErrorCode())) {
				throw e;
			}
			logger.info("Could not find a FuelTransactionSource, will create a new one !", e);
			ftFile.setName(this.resource.getFilename());
			ftFile.setFuelTransactions(1);
			FuelTransactionSourceParameters ftsp = new FuelTransactionSourceParameters();
			ftsp.setParamName("uniqueID");
			ftsp.setParamValue(ftFile.getName());
			ftFile.addToParameters(ftsp);
			this.srv.insert(ftFile);
		}
		return ftFile;
	}

	@Override
	public void setResource(Resource resource) {
		this.resource = resource;
	}

}
