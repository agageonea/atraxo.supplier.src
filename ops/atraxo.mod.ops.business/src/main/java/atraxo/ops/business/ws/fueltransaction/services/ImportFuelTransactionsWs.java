package atraxo.ops.business.ws.fueltransaction.services;

import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import atraxo.ops.business.ws.fueltransaction.model.ImportDocumentRequest;
import atraxo.ops.business.ws.fueltransaction.model.ImportDocumentResponse;
import seava.j4e.api.exceptions.BusinessException;

@WebService(targetNamespace = "http://services.fueltransaction.ws.business.ops.atraxo/", endpointInterface = "ImportFtEndpoint")
public interface ImportFuelTransactionsWs {

	@WebMethod(operationName = "importFt", action = "importFt")
	@WebResult(targetNamespace = "http://services.fueltransaction.ws.business.ops.atraxo/")
	ImportDocumentResponse importFt(
			@WebParam(name = "request", targetNamespace = "http://services.fueltransaction.ws.business.ops.atraxo/") ImportDocumentRequest request)
					throws JAXBException, SAXException, IOException, BusinessException;
}
