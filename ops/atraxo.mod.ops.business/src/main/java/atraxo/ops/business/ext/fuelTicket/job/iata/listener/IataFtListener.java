package atraxo.ops.business.ext.fuelTicket.job.iata.listener;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

import atraxo.ad.domain.impl.ad.Category;
import atraxo.ad.domain.impl.ad.NotificationAction;
import atraxo.ad.domain.impl.ad.Priority;
import atraxo.fmbas.business.api.notification.INotificationService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.domain.impl.notification.Notification;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.model.AppNotification;
import seava.j4e.api.service.notification.IAppNotificationService;
import seava.j4e.api.session.Session;
import seava.j4e.scheduler.batch.listener.DefaultActionListener;

public class IataFtListener extends DefaultActionListener {

	private static final Logger LOG = LoggerFactory.getLogger(IataFtListener.class);

	public static final String RESOURCES = "res";
	public static final String INV_RESOURCES = "inv_res";
	public static final String FAILURE_MSG = "failureMsg";

	@Autowired
	private INotificationService srv;

	@Autowired
	private IAppNotificationService appNotifsrv;

	@Override
	public void afterJob(JobExecution jobExecution) {
		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder();
		sb.append(status.getExitDescription());
		ExecutionContext executionContext = jobExecution.getExecutionContext();
		if (executionContext.containsKey("skipCount")) {
			sb.append(String.format(BusinessErrorCode.IATA_FT_JOB_SKIPED.getErrMsg(), executionContext.getInt("skipCount"))).append("\r\n");
		}
		if (executionContext.containsKey("readCount")) {
			sb.append(String.format(BusinessErrorCode.IATA_FT_JOB_READ.getErrMsg(), executionContext.getInt("readCount"))).append("\r\n");
		}
		if (executionContext.containsKey(FAILURE_MSG)) {
			sb.append(executionContext.getString(FAILURE_MSG)).append("\r\n");
			status = status.and(ExitStatus.FAILED);
		}
		if (!executionContext.containsKey("skipCount") && !executionContext.containsKey("readCount") && !executionContext.containsKey(FAILURE_MSG)) {
			sb.append("The job finished succesfully but no files were present to be processed");
		}
		this.archiveFiles(RESOURCES, executionContext, jobExecution.getJobParameters().getString("ARCHIVEDIR"));
		this.archiveFiles(INV_RESOURCES, executionContext, jobExecution.getJobParameters().getString("INVFILES"));
		try {
			this.sendNotification(status);
		} catch (BusinessException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
		status = new ExitStatus(status.getExitCode(), sb.toString());
		jobExecution.setExitStatus(status);

	}

	private void sendNotification(ExitStatus status) throws BusinessException {
		Notification e = new Notification();
		e.setTitle(OpsErrorCode.FT_IATA_NT_MSG_TITLE.getErrMsg());
		e.setAction(NotificationAction._GOTO_);
		e.setLink("link");
		e.setEventDate(GregorianCalendar.getInstance().getTime());
		e.setLifeTime(GregorianCalendar.getInstance().getTime());
		if (ExitStatus.COMPLETED.equals(status)) {
			e.setCategory(Category._JOB_);
			e.setDescripion(OpsErrorCode.FT_IATA_NT_MSG_SUCCESS.getErrMsg());
			e.setPriority(Priority._DEFAULT_);
		} else {
			e.setCategory(Category._ERROR_);
			e.setDescripion(OpsErrorCode.FT_IATA_NT_MSG_FAILURE.getErrMsg());
			e.setPriority(Priority._MAX_);
		}
		this.srv.insert(e);
		this.sendNotificationToClient(e);
	}

	private void sendNotificationToClient(Notification e) {
		AppNotification appNotification = new AppNotification(e.getId(), e.getTitle(), e.getDescripion(), Session.user.get().getClientId());
		appNotification.setAction(e.getAction().getName());
		appNotification.setCategory(e.getCategory().getName());
		appNotification.setEventDate(e.getEventDate());
		appNotification.setLifeTime(e.getLifeTime());
		appNotification.setLink(e.getLink());
		appNotification.setPriority(e.getPriority().getName());
		this.appNotifsrv.add(appNotification);
	}

	private void archiveFiles(String key, ExecutionContext executionContext, String archiveDir) {
		if (executionContext.containsKey(key)) {
			@SuppressWarnings("unchecked")
			Map<String, Resource> resources = (Map<String, Resource>) executionContext.get(key);
			for (Entry<String, Resource> entry : resources.entrySet()) {
				try {
					Files.move(entry.getValue().getFile().toPath(), new File(archiveDir + File.separator + entry.getValue().getFilename()).toPath(),
							StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					LOG.error(e.getLocalizedMessage(), e);
				}
			}
		}
	}
}
