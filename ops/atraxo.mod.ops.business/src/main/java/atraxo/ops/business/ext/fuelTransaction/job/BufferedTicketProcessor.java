package atraxo.ops.business.ext.fuelTransaction.job;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ops.business.api.fuelTransaction.IFuelTransactionService;
import atraxo.ops.domain.ext.fuelTicket.ProcessResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.ops_type.ValidationState;

public class BufferedTicketProcessor implements ItemProcessor<List<FuelTransaction>, ProcessResult> {

	private static final String FAILED = "failed";
	private static final String NOT_FAILED = "not failed";

	@Autowired
	private IFuelTransactionService service;
	private ExecutionContext executionContext;

	@BeforeStep
	public void storeExecutionContext(StepExecution stepExecution) {
		this.executionContext = stepExecution.getExecutionContext();
	}

	@Override
	public ProcessResult process(List<FuelTransaction> list) throws Exception {
		ProcessResult pr;
		synchronized (this.service) {
			int failed = 0;
			int notFailed = 0;

			List<FuelTicket> insList = new ArrayList<>();
			List<FuelTicket> updList = new ArrayList<>();
			List<FuelTransaction> delFtList = new ArrayList<>();
			List<FuelTransaction> updFtList = new ArrayList<>();
			this.service.processWithoutSave(list, insList);
			for (FuelTransaction ft : list) {
				if (ValidationState._EMPTY_.equals(ft.getValidationState())) {
					delFtList.add(ft);
					notFailed++;
				} else {
					updFtList.add(ft);
					failed++;
				}
			}
			if (this.executionContext.containsKey(FAILED)) {
				int n = (Integer)this.executionContext.get(FAILED);
				failed = n + failed;
			}
			if (this.executionContext.containsKey(NOT_FAILED)) {
				int n = (Integer)this.executionContext.get(NOT_FAILED);
				notFailed = n + notFailed;
			}
			this.executionContext.put(FAILED, failed);
			this.executionContext.put(NOT_FAILED, notFailed);

			pr = new ProcessResult(insList, updList, delFtList, updFtList);
		}
		return pr;
	}

}
