/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelTransaction;

import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSourceParameters;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelTransactionSource} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelTransactionSource_Service
		extends
			AbstractEntityService<FuelTransactionSource> {

	/**
	 * Public constructor for FuelTransactionSource_Service
	 */
	public FuelTransactionSource_Service() {
		super();
	}

	/**
	 * Public constructor for FuelTransactionSource_Service
	 * 
	 * @param em
	 */
	public FuelTransactionSource_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelTransactionSource> getEntityClass() {
		return FuelTransactionSource.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSource
	 */
	public FuelTransactionSource findByName(String name) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelTransactionSource.NQ_FIND_BY_NAME,
							FuelTransactionSource.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("name", name).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTransactionSource", "name"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTransactionSource", "name"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSource
	 */
	public FuelTransactionSource findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							FuelTransactionSource.NQ_FIND_BY_BUSINESS,
							FuelTransactionSource.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTransactionSource", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTransactionSource", "id"), nure);
		}
	}

	/**
	 * Find by reference: parameters
	 *
	 * @param parameters
	 * @return List<FuelTransactionSource>
	 */
	public List<FuelTransactionSource> findByParameters(
			FuelTransactionSourceParameters parameters) {
		return this.findByParametersId(parameters.getId());
	}
	/**
	 * Find by ID of reference: parameters.id
	 *
	 * @param parametersId
	 * @return List<FuelTransactionSource>
	 */
	public List<FuelTransactionSource> findByParametersId(Integer parametersId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelTransactionSource e, IN (e.parameters) c where e.clientId = :clientId and c.id = :parametersId",
						FuelTransactionSource.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("parametersId", parametersId).getResultList();
	}
	/**
	 * Find by reference: fueltransaction
	 *
	 * @param fueltransaction
	 * @return List<FuelTransactionSource>
	 */
	public List<FuelTransactionSource> findByFueltransaction(
			FuelTransaction fueltransaction) {
		return this.findByFueltransactionId(fueltransaction.getId());
	}
	/**
	 * Find by ID of reference: fueltransaction.id
	 *
	 * @param fueltransactionId
	 * @return List<FuelTransactionSource>
	 */
	public List<FuelTransactionSource> findByFueltransactionId(
			Integer fueltransactionId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelTransactionSource e, IN (e.fueltransaction) c where e.clientId = :clientId and c.id = :fueltransactionId",
						FuelTransactionSource.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fueltransactionId", fueltransactionId)
				.getResultList();
	}
}
