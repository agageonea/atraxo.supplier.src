package atraxo.ops.business.ext.fuelQuotation.job.items;

import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ops.business.api.fuelQuotation.IFuelQuotationService;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;

public class ExpireQuotationItemReader implements ItemReader<List<FuelQuotation>> {

	@Autowired
	IFuelQuotationService service;

	@Override
	public List<FuelQuotation> read() throws Exception {
		return this.service.findForExpire();
	}
}
