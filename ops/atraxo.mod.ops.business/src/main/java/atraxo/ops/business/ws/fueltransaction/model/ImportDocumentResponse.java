package atraxo.ops.business.ws.fueltransaction.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "response")
public class ImportDocumentResponse implements Serializable {

	private static final long serialVersionUID = -2574716307445546499L;

	private int readCount;
	private int skipCount;
	private int inserted;
	private int updated;

	@XmlElement(name = "readCount", required = true)
	public int getReadCount() {
		return this.readCount;
	}

	public void setReadCount(int readCount) {
		this.readCount = readCount;
	}

	@XmlElement(name = "skipCount", required = true)
	public int getSkipCount() {
		return this.skipCount;
	}

	public void setSkipCount(int skipCount) {
		this.skipCount = skipCount;
	}

	@XmlElement(name = "inserted", required = true)
	public int getInserted() {
		return this.inserted;
	}

	public void setInserted(int inserted) {
		this.inserted = inserted;
	}

	@XmlElement(name = "updated", required = true)
	public int getUpdated() {
		return this.updated;
	}

	public void setUpdated(int updated) {
		this.updated = updated;
	}

}
