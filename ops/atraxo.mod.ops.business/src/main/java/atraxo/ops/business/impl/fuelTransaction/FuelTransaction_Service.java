/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelTransaction;

import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionEquipment;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import atraxo.ops.domain.impl.ops_type.TicketType;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelTransaction} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelTransaction_Service
		extends
			AbstractEntityService<FuelTransaction> {

	/**
	 * Public constructor for FuelTransaction_Service
	 */
	public FuelTransaction_Service() {
		super();
	}

	/**
	 * Public constructor for FuelTransaction_Service
	 * 
	 * @param em
	 */
	public FuelTransaction_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelTransaction> getEntityClass() {
		return FuelTransaction.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTransaction
	 */
	public FuelTransaction findByBusinessKey(String airportCode,
			String supplierCode, Date transactionDate, String ticketNumber,
			TicketType ticketType) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelTransaction.NQ_FIND_BY_BUSINESSKEY,
							FuelTransaction.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("airportCode", airportCode)
					.setParameter("supplierCode", supplierCode)
					.setParameter("transactionDate", transactionDate)
					.setParameter("ticketNumber", ticketNumber)
					.setParameter("ticketType", ticketType).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(
					J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTransaction",
							"airportCode, supplierCode, transactionDate, ticketNumber, ticketType"),
					nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(
					J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTransaction",
							"airportCode, supplierCode, transactionDate, ticketNumber, ticketType"),
					nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTransaction
	 */
	public FuelTransaction findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelTransaction.NQ_FIND_BY_BUSINESS,
							FuelTransaction.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTransaction", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTransaction", "id"), nure);
		}
	}

	/**
	 * Find by reference: importSource
	 *
	 * @param importSource
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByImportSource(
			FuelTransactionSource importSource) {
		return this.findByImportSourceId(importSource.getId());
	}
	/**
	 * Find by ID of reference: importSource.id
	 *
	 * @param importSourceId
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByImportSourceId(Integer importSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTransaction e where e.clientId = :clientId and e.importSource.id = :importSourceId",
						FuelTransaction.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("importSourceId", importSourceId).getResultList();
	}
	/**
	 * Find by reference: equipment
	 *
	 * @param equipment
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByEquipment(
			FuelTransactionEquipment equipment) {
		return this.findByEquipmentId(equipment.getId());
	}
	/**
	 * Find by ID of reference: equipment.id
	 *
	 * @param equipmentId
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByEquipmentId(Integer equipmentId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelTransaction e, IN (e.equipment) c where e.clientId = :clientId and c.id = :equipmentId",
						FuelTransaction.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("equipmentId", equipmentId).getResultList();
	}
	/**
	 * Find by reference: quantity
	 *
	 * @param quantity
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByQuantity(FuelTransactionQuantity quantity) {
		return this.findByQuantityId(quantity.getId());
	}
	/**
	 * Find by ID of reference: quantity.id
	 *
	 * @param quantityId
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByQuantityId(Integer quantityId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelTransaction e, IN (e.quantity) c where e.clientId = :clientId and c.id = :quantityId",
						FuelTransaction.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("quantityId", quantityId).getResultList();
	}
}
