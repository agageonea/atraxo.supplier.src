package atraxo.ops.business.ws.acknowledgment.services;

import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author zspeter
 */
@WebService(targetNamespace = "http://services.acknowledgment.ws.business.ops.atraxo/", endpointInterface = "AcknowledgeOperationalInfo")
public interface AcknowledgeOperationalInfoWS {

	/**
	 * @param request
	 * @return
	 * @throws JAXBException
	 * @throws SAXException
	 * @throws IOException
	 * @throws BusinessException
	 */
	@WebMethod(operationName = "acknowledgeFuelTransactionsEBits", action = "acknowledgeFuelTransactionsEBits")
	@WebResult(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse")
	MSG acknowledgeFuelTransactionsEBits(
			@WebParam(name = "MSG", targetNamespace = "http://Pumaenergy.Common.eAviation.Schemas.Message_RequestResponse") MSG request)
			throws JAXBException, SAXException, IOException, BusinessException;

}
