package atraxo.ops.business.ext.fuelTicket.delegate;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;

import atraxo.ops.business.api.fuelTicket.IFuelTicketHardCopyService;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.fuelTicket.FuelTicketHardCopy;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class FuelTicketAttachement_Bd extends AbstractBusinessDelegate {

	private static final Logger logger = LoggerFactory.getLogger(FuelTicketAttachement_Bd.class);

	public byte[] string2byteArray(String imageStr) {
		if (imageStr == null || imageStr.length() < 1) {
			return new byte[0];
		}
		return Base64.decode(imageStr.replace(' ', '+').getBytes());
	}

	public void saveAttachment(FuelTicket fuelTicket, String pictureStr) throws BusinessException, UnsupportedEncodingException {
		if (fuelTicket == null || pictureStr == null) {
			return;
		}
		byte[] picture = this.string2byteArray(pictureStr);
		if (picture == null) {
			return;
		}
		IFuelTicketHardCopyService fthcService = (IFuelTicketHardCopyService) this.findEntityService(FuelTicketHardCopy.class);
		IFuelTicketService fuelTicketService = (IFuelTicketService) this.findEntityService(FuelTicket.class);
		fuelTicket = fuelTicketService.findByBusinessKey(fuelTicket.getDeparture(), fuelTicket.getTicketNo(), fuelTicket.getCustomer(),
				fuelTicket.getDeliveryDate());
		FuelTicketHardCopy ftch = null;
		try {
			ftch = fthcService.findByBk(fuelTicket);
			ftch.setFile(picture);
			fthcService.update(ftch);
		} catch (ApplicationException ex) {
			if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(ex.getErrorCode())) {
				throw ex;
			}
			logger.info("Could not find a FuelTicketHardCopy, will create a new one !", ex);
			ftch = new FuelTicketHardCopy();
			ftch.setFuelTicket(fuelTicket);
			ftch.setFile(picture);
			fthcService.insert(ftch);
		}
	}
}
