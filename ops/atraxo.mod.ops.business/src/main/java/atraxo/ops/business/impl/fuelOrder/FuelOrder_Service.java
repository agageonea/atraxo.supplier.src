/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelOrder;

import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelOrder} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelOrder_Service extends AbstractEntityService<FuelOrder> {

	/**
	 * Public constructor for FuelOrder_Service
	 */
	public FuelOrder_Service() {
		super();
	}

	/**
	 * Public constructor for FuelOrder_Service
	 * 
	 * @param em
	 */
	public FuelOrder_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelOrder> getEntityClass() {
		return FuelOrder.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelOrder
	 */
	public FuelOrder findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelOrder.NQ_FIND_BY_CODE,
							FuelOrder.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelOrder", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelOrder", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelOrder
	 */
	public FuelOrder findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelOrder.NQ_FIND_BY_BUSINESS,
							FuelOrder.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelOrder", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelOrder", "id"), nure);
		}
	}

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrder e where e.clientId = :clientId and e.customer.id = :customerId",
						FuelOrder.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: fuelQuote
	 *
	 * @param fuelQuote
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByFuelQuote(FuelQuotation fuelQuote) {
		return this.findByFuelQuoteId(fuelQuote.getId());
	}
	/**
	 * Find by ID of reference: fuelQuote.id
	 *
	 * @param fuelQuoteId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByFuelQuoteId(Integer fuelQuoteId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrder e where e.clientId = :clientId and e.fuelQuote.id = :fuelQuoteId",
						FuelOrder.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelQuoteId", fuelQuoteId).getResultList();
	}
	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByContact(Contacts contact) {
		return this.findByContactId(contact.getId());
	}
	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByContactId(Integer contactId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrder e where e.clientId = :clientId and e.contact.id = :contactId",
						FuelOrder.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contactId", contactId).getResultList();
	}
	/**
	 * Find by reference: registeredBy
	 *
	 * @param registeredBy
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByRegisteredBy(UserSupp registeredBy) {
		return this.findByRegisteredById(registeredBy.getId());
	}
	/**
	 * Find by ID of reference: registeredBy.id
	 *
	 * @param registeredById
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByRegisteredById(String registeredById) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrder e where e.clientId = :clientId and e.registeredBy.id = :registeredById",
						FuelOrder.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("registeredById", registeredById).getResultList();
	}
	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findBySubsidiary(Customer subsidiary) {
		return this.findBySubsidiaryId(subsidiary.getId());
	}
	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findBySubsidiaryId(Integer subsidiaryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelOrder e where e.clientId = :clientId and e.subsidiary.id = :subsidiaryId",
						FuelOrder.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiaryId", subsidiaryId).getResultList();
	}
	/**
	 * Find by reference: fuelOrderLocations
	 *
	 * @param fuelOrderLocations
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByFuelOrderLocations(
			FuelOrderLocation fuelOrderLocations) {
		return this.findByFuelOrderLocationsId(fuelOrderLocations.getId());
	}
	/**
	 * Find by ID of reference: fuelOrderLocations.id
	 *
	 * @param fuelOrderLocationsId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByFuelOrderLocationsId(
			Integer fuelOrderLocationsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelOrder e, IN (e.fuelOrderLocations) c where e.clientId = :clientId and c.id = :fuelOrderLocationsId",
						FuelOrder.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelOrderLocationsId", fuelOrderLocationsId)
				.getResultList();
	}
}
