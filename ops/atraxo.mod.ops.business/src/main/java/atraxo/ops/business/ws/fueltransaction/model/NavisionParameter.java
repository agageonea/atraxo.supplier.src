package atraxo.ops.business.ws.fueltransaction.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author apetho
 */
public class NavisionParameter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 739783585800632047L;
	private String uniqueID;

	@XmlElement(name = "uniqueID", required = false)
	public String getUniqueID() {
		return this.uniqueID;
	}

	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

}
