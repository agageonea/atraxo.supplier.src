package atraxo.ops.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class FuelTicketUpdateException extends BusinessException {

	public FuelTicketUpdateException() {
		super(OpsErrorCode.FUEL_TICKET_UPDATE, OpsErrorCode.FUEL_TICKET_UPDATE.getErrMsg());
	}
}
