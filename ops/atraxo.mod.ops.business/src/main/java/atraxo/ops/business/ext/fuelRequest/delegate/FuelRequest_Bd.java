package atraxo.ops.business.ext.fuelRequest.delegate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.UnitType;
import atraxo.fmbas.domain.impl.masteragreements.MasterAgreement;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.business.api.fuelQuotation.IFuelQuotationService;
import atraxo.ops.business.api.fuelQuoteLocation.IFuelQuoteLocationService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FuelQuoteCycle;
import atraxo.ops.domain.impl.ops_type.FuelQuoteScope;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatus;
import atraxo.ops.domain.impl.ops_type.FuelQuoteType;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class FuelRequest_Bd extends AbstractBusinessDelegate {

	public void checkRelease(FuelRequest fr) throws BusinessException {
		if (fr.getIsOpenRelease() && (fr.getStartDate() == null || fr.getEndDate() == null)) {
			throw new BusinessException(OpsErrorCode.FUEL_REQ_RELEASE_PER, OpsErrorCode.FUEL_REQ_RELEASE_PER.getErrMsg());
		} else if (!fr.getIsOpenRelease()) {
			fr.setStartDate(null);
			fr.setEndDate(null);
		}
		if (fr.getStartDate() != null && fr.getStartDate().before(fr.getRequestDate())) {
			throw new BusinessException(OpsErrorCode.FUEL_REQ_OPEN_RELEASE, OpsErrorCode.FUEL_REQ_OPEN_RELEASE.getErrMsg());
		}
	}

	public void checkCustomer(FuelRequest fr) throws BusinessException {
		if (fr.getCustomer() == null) {
			throw new BusinessException(OpsErrorCode.FUEL_REQ_CUST_EMPTY, OpsErrorCode.FUEL_REQ_CUST_EMPTY.getErrMsg());
		}
	}

	public void checkReceivedOn(FuelRequest fr) throws BusinessException {
		if (fr.getRequestDate() == null) {
			throw new BusinessException(OpsErrorCode.FUEL_REQ_RECEIV_ON_EMPTY, OpsErrorCode.FUEL_REQ_RECEIV_ON_EMPTY.getErrMsg());
		}
	}

	public void calculateCode(FuelRequest fr) {

		int n = String.valueOf(fr.getId()).length();
		String code = "FR";
		for (int i = 1; i <= 5 - n; ++i) {
			code += "0";
		}
		fr.setRequestCode(code + fr.getId());
	}

	/**
	 * Verify the dates. Valid to must be greater than valid from.
	 *
	 * @param from
	 * @param to
	 * @throws BusinessException
	 */
	public void verificateDates(Date from, Date to) throws BusinessException {
		if (from != null && to != null && from.after(to)) {
			throw new BusinessException(OpsErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					OpsErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
	}

	/**
	 * @param fq
	 * @param fr
	 * @throws BusinessException
	 */
	private void generateLocationQuotes(FuelQuotation fq, FuelRequest fr) throws BusinessException {
		IFlightEventService service = (IFlightEventService) this.findEntityService(FlightEvent.class);
		IFuelQuoteLocationService fquoteService = (IFuelQuoteLocationService) this.findEntityService(FuelQuoteLocation.class);
		List<FlightEvent> flightEvents = service.findByFuelRequest(fr);

		for (FlightEvent flightEvent : flightEvents) {
			if (flightEvent.getDeparture() != null) {
				List<FuelQuoteLocation> qLocations = fquoteService.findByQuote(fq);
				FuelQuoteLocation fuelQuotation = this.verifyIfExistsQuotationLocation(qLocations, flightEvent);
				if (fuelQuotation == null) {
					FuelQuoteLocation fqlDepa = this.generateFuelQuoteLocation(fq, flightEvent);
					fquoteService.insert(fqlDepa);
					fuelQuotation = fquoteService.findByRefid(fqlDepa.getRefid());
				}
				flightEvent.setLocQuote(fuelQuotation);
				fuelQuotation.addToFlightEvents(flightEvent);
				service.update(flightEvent);
			}

		}
	}

	private FuelQuoteLocation verifyIfExistsQuotationLocation(List<FuelQuoteLocation> qLocations, FlightEvent ft) {
		for (FuelQuoteLocation fql : qLocations) {
			if (fql.getLocation().getId().equals(ft.getDeparture().getId()) && fql.getEventType().equals(ft.getEventType())
					&& fql.getOperationalType().equals(ft.getOperationType())) {
				return fql;
			}
		}
		return null;
	}

	/**
	 * @param fq
	 * @param location
	 * @param eventType
	 * @param operationalType
	 * @return
	 * @throws BusinessException
	 */
	private FuelQuoteLocation generateFuelQuoteLocation(FuelQuotation fq, FlightEvent ft) throws BusinessException {
		IUnitService unitService = (IUnitService) this.findEntityService(Unit.class);
		FuelQuoteLocation fql = new FuelQuoteLocation();
		fql.setLocation(ft.getDeparture());
		fql.setEventType(ft.getEventType());
		fql.setOperationalType(ft.getOperationType());
		fql.setProduct(ft.getProduct());
		fql.setDeliveryMethod(ContractSubType._NA_);
		fql.setPriceBasis(PricingMethod._EMPTY_);
		fql.setQuote(fq);
		fql.setUnit(unitService.getBaseUnit(UnitType._VOLUME_, BigDecimal.ONE));
		Date date = GregorianCalendar.getInstance().getTime();
		if (fq.getPostedOn() != null) {
			date = fq.getPostedOn();
		}
		ICustomerService service = (ICustomerService) this.findEntityService(Customer.class);
		MasterAgreement agreement = service.getValidMasterAgreement(fq.getCustomer(), date);
		this.setFuelQuotationLocationFromAgreement(fql, agreement);
		return fql;
	}

	private void setFuelQuotationLocationFromAgreement(FuelQuoteLocation pq, MasterAgreement agreement) {
		pq.setPaymentTerms(agreement.getPaymentTerms());
		pq.setInvoiceFreq(agreement.getInvoiceFrequency());
		pq.setCurrency(agreement.getCurrency());
		pq.setPeriod(agreement.getPeriod());
		pq.setFinancialSource(agreement.getFinancialsource());
		pq.setAverageMethod(agreement.getAverageMethod());
		pq.setPaymentRefDate(agreement.getReferenceTo());
	}

	/**
	 * @param fr
	 * @return
	 * @throws BusinessException
	 */
	@Transactional
	public FuelQuotation generateFuelQuote(FuelRequest fr) throws BusinessException {
		IFuelQuotationService fuelQuotationService = (IFuelQuotationService) this.findEntityService(FuelQuotation.class);
		FuelQuotation fq = new FuelQuotation();
		fq.setCustomer(fr.getCustomer());
		fq.setOpenRelease(fr.getIsOpenRelease());
		fq.setFuelRequest(fr);
		fq.setOwner(fr.getProcessedBy());
		fq.setPostedOn(new Date());
		fq.setValidUntil(DateUtils.addDays(new Date(), 30));
		fq.setStatus(FuelQuoteStatus._NEW_);
		fq.setScope(FuelQuoteScope._INTO_PLANE_);
		fq.setType(FuelQuoteType._AD_HOC_);
		fq.setPricingCycle(FuelQuoteCycle._NOTICE_);
		fq.setHolder(fr.getSubsidiary());
		fuelQuotationService.insert(fq);
		List<FuelQuotation> fuelQuotations = fuelQuotationService.findByFuelRequest(fr);
		if (fuelQuotations != null && !fuelQuotations.isEmpty()) {
			FuelQuotation fqSaved = fuelQuotations.get(0);
			this.generateLocationQuotes(fqSaved, fr);
			return fqSaved;
		} else {
			return null;
		}
	}
}
