/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.fuelOrderLocation.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.business.ext.contracts.delegate.SupplierContract_Bd;
import atraxo.cmm.domain.ext.contracts.SupplierContract;
import atraxo.cmm.domain.impl.cmm_type.ContractSubType;
import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.cmm_type.PricingMethod;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.domain.ext.json.message.SoneMessage;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.business.ext.fuelOrderLocation.delegate.DeleteFuelOrderLocation_Bd;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.ops_type.FuelOrderStatus;
import atraxo.ops.domain.impl.ops_type.InvoiceStatus;
import atraxo.ops.domain.impl.ops_type.PricePolicyType;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Business extensions specific for {@link FuelOrderLocation} domain entity.
 */
public class FuelOrderLocation_Service extends atraxo.ops.business.impl.fuelOrderLocation.FuelOrderLocation_Service
		implements IFuelOrderLocationService {

	@Override
	protected void preDelete(FuelOrderLocation fol) throws BusinessException {
		super.preDelete(fol);
		DeleteFuelOrderLocation_Bd bd = this.getBusinessDelegate(DeleteFuelOrderLocation_Bd.class);
		bd.deleteFlightEvents(fol);

	}

	@Override
	@History(type = FuelOrderLocation.class, value = "Supplier change")
	@Transactional
	public void changeSupplier(FuelOrderLocation fol, Integer contractId, @Reason String remarks) throws BusinessException {
		Contract contract = this.findById(contractId, Contract.class);
		SupplierContract_Bd bd = this.getBusinessDelegate(SupplierContract_Bd.class);
		MasterAgreementsPeriod indexOffset = !Arrays.asList(MasterAgreementsPeriod.values()).contains(fol.getIndexOffset())
				? MasterAgreementsPeriod._CURRENT_ : MasterAgreementsPeriod.getByName(fol.getIndexOffset());
		SupplierContract supplierContract = bd.buildSupplierContract(fol.getFuelReleaseStartsOn(), fol.getFuelReleaseEndsOn(), contract,
				fol.getPaymentUnit(), fol.getPaymentCurrency(), fol.getAverageMethod(), fol.getFinancialSource(), indexOffset);

		fol.setContract(contract);
		fol.setSupplier(contract.getSupplier());
		fol.setBasePrice(supplierContract.getBasePrice());

		BigDecimal fuelPrice = supplierContract.getDifferential() != null ? supplierContract.getBasePrice().add(supplierContract.getDifferential())
				: supplierContract.getBasePrice();
		if (fol.getFuelPrice().compareTo(fuelPrice.setScale(6, RoundingMode.HALF_UP)) < 0) {
			fol.setWarningMessage(SoneMessage.SUPPLIER_CHANGE_FUEL_PRICE_IS_HIGHER);
		}
		fol.setFuelPrice(fuelPrice);
		if (fol.getSellingFuelPrice() != null && fol.getSellingFuelPrice().compareTo(fuelPrice) < 0) {
			fol.setWarningMessage(SoneMessage.SUPPLIER_CHANGE_SELLING_PRICE_IS_LOWER);
		}
		fol.setIntoPlaneFees(supplierContract.getIntoPlaneFee());
		fol.setOtherFees(supplierContract.getOtherFees());
		fol.setTaxes(supplierContract.getTaxes());
		fol.setPerFlightFee(supplierContract.getPerFlightFee());
		fol.setTotalPricePerUom(fol.getFuelPrice().add(fol.getIntoPlaneFees()).add(fol.getOtherFees()).add(fol.getTaxes()));
		if (fol.getDifferential() != null) {
			fol.setTotalPricePerUom(fol.getTotalPricePerUom().add(fol.getDifferential()));
		}

		fol.setPriceBasis(supplierContract.getPriceBasis());
		fol.setDeliveryMethod(ContractSubType.getByName(supplierContract.getDelivery()));
		fol.setExchangeRateOffset(supplierContract.getExchangeRateOffset());
		fol.setIndexOffset(supplierContract.getIndexOffset());
		fol.setIplAgent(supplierContract.getIntoPlaneAgent());
		fol.setDft(fol.getIntoPlaneFees().add(fol.getOtherFees()).add(fol.getTaxes()));

		this.update(fol);
	}

	@Override
	public void generate(List<FuelQuoteLocation> locationQuotes, FuelOrder fuelOrder) throws BusinessException {
		for (FuelQuoteLocation fql : locationQuotes) {
			this.generate(fql, fuelOrder);
		}
	}

	@Override
	public void generate(FuelQuoteLocation locationQuote, FuelOrder fuelOrder) throws BusinessException {

		FuelOrderLocation orderLocation = new FuelOrderLocation();
		orderLocation.setAverageMethod(locationQuote.getAverageMethod());
		orderLocation.setBasePrice(locationQuote.getBasePrice());
		orderLocation.setBuyInvoiceStatus(InvoiceStatus._NO_ACCRUALS_);
		orderLocation.setContract(locationQuote.getContract());
		orderLocation.setDeliveryMethod(locationQuote.getDeliveryMethod());
		orderLocation.setDft(locationQuote.getDft());
		orderLocation.setDifferential(locationQuote.getDifferential());
		orderLocation.setEvents(locationQuote.getEvents() != null ? locationQuote.getEvents() : 0);
		orderLocation.setExchangeRateOffset(locationQuote.getPeriod().getName());
		orderLocation.setFinancialSource(locationQuote.getFinancialSource());
		orderLocation.setFlightType(locationQuote.getEventType());
		orderLocation.setFuelOrder(fuelOrder);
		BigDecimal differential = orderLocation.getDifferential() != null ? orderLocation.getDifferential() : BigDecimal.ZERO;
		if (PricingMethod._INDEX_.equals(locationQuote.getPriceBasis())) {
			BigDecimal suppDiff = locationQuote.getFuelPrice().subtract(locationQuote.getBasePrice());
			differential = differential.add(suppDiff);
		}
		orderLocation.setFuelPrice(locationQuote.getFuelPrice());
		orderLocation.setSellingFuelPrice(orderLocation.getBasePrice().add(differential));
		orderLocation.setFuelReleaseEndsOn(locationQuote.getFuelReleaseEndsOn());
		orderLocation.setFuelReleaseStartsOn(locationQuote.getFuelReleaseStartsOn());
		orderLocation.setIndexOffset(locationQuote.getIndexOffset());
		orderLocation.setInvoiceFrequency(locationQuote.getInvoiceFreq());
		orderLocation.setIplAgent(locationQuote.getIntoPlaneAgent());
		orderLocation.setIntoPlaneFees(locationQuote.getIntoplaneFee());
		orderLocation.setLocation(locationQuote.getLocation());
		orderLocation.setMargin(locationQuote.getMargin());
		orderLocation.setOperationalType(locationQuote.getOperationalType());
		orderLocation.setOtherFees(locationQuote.getOtherFees());
		orderLocation.setPaymentCurrency(locationQuote.getCurrency());
		orderLocation.setPaymentRefDay(locationQuote.getPaymentRefDate());
		orderLocation.setPaymentTerms(locationQuote.getPaymentTerms());
		orderLocation.setPaymentUnit(locationQuote.getUnit());
		orderLocation.setPerFlightFee(locationQuote.getFlightFee());
		orderLocation.setPriceBasis(locationQuote.getPriceBasis());
		orderLocation.setPricePolicy(locationQuote.getPricingPolicy());
		orderLocation.setPricePolicyType(locationQuote.getMargin() != null ? PricePolicyType._MARGIN_ : PricePolicyType._MARKUP_);
		orderLocation.setProduct(locationQuote.getProduct());
		orderLocation.setQuantity(locationQuote.getQuantity());
		orderLocation.setSalesInvoiceStatus(InvoiceStatus._NO_ACCRUALS_);
		orderLocation.setSupplier(locationQuote.getSupplier());
		orderLocation.setTaxes(locationQuote.getTaxes());
		orderLocation.setTotalPricePerUom(locationQuote.getTotalPricePerUom());
		orderLocation.setFlightEvents(locationQuote.getFlightEvents());
		this.insert(orderLocation);
		this.createRelationToFlightEvents(orderLocation);

	}

	private void createRelationToFlightEvents(FuelOrderLocation orderLocation) throws BusinessException {
		IFlightEventService service = (IFlightEventService) this.findEntityService(FlightEvent.class);
		for (FlightEvent flightEvent : orderLocation.getFlightEvents()) {
			flightEvent.setLocOrder(orderLocation);
			service.updateWithoutBusiness(flightEvent);
		}
	}

	@Override
	public List<FuelOrderLocation> findDirectFuelOrder(Suppliers supplier, Customer customer, FlightTypeIndicator flightType, Locations location,
			Date fuelingDate) throws BusinessException {

		Date truncatedFuelingdate = DateUtils.truncate(fuelingDate, Calendar.DAY_OF_MONTH);

		Map<String, Object> params = new HashMap<>();
		params.put("supplier", supplier);
		params.put("location", location);
		params.put("flightType", flightType);

		List<FuelOrderLocation> list = this.findEntitiesByAttributes(params);
		List<FuelOrderLocation> filteredList = new ArrayList<>();
		for (FuelOrderLocation e : list) {
			FuelOrder fo = e.getFuelOrder();
			if (fo.getCustomer().equals(customer) && fo.getStatus().equals(FuelOrderStatus._RELEASED_)) {
				for (FlightEvent flightEvent : e.getFlightEvents()) {
					Date truncatedUplift = DateUtils.truncate(flightEvent.getUpliftDate(), Calendar.DAY_OF_MONTH);
					if (truncatedFuelingdate.equals(truncatedUplift)) {
						filteredList.add(e);
						break;
					}
				}

			}
		}
		return filteredList;
	}

}
