/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelTransaction;

import atraxo.ops.business.api.fuelTransaction.IFuelTransactionEquipmentService;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionEquipment;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelTransactionEquipment} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelTransactionEquipment_Service
		extends
			AbstractEntityService<FuelTransactionEquipment>
		implements
			IFuelTransactionEquipmentService {

	/**
	 * Public constructor for FuelTransactionEquipment_Service
	 */
	public FuelTransactionEquipment_Service() {
		super();
	}

	/**
	 * Public constructor for FuelTransactionEquipment_Service
	 * 
	 * @param em
	 */
	public FuelTransactionEquipment_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelTransactionEquipment> getEntityClass() {
		return FuelTransactionEquipment.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionEquipment
	 */
	public FuelTransactionEquipment findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							FuelTransactionEquipment.NQ_FIND_BY_BUSINESS,
							FuelTransactionEquipment.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTransactionEquipment", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTransactionEquipment", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelTransaction
	 *
	 * @param fuelTransaction
	 * @return List<FuelTransactionEquipment>
	 */
	public List<FuelTransactionEquipment> findByFuelTransaction(
			FuelTransaction fuelTransaction) {
		return this.findByFuelTransactionId(fuelTransaction.getId());
	}
	/**
	 * Find by ID of reference: fuelTransaction.id
	 *
	 * @param fuelTransactionId
	 * @return List<FuelTransactionEquipment>
	 */
	public List<FuelTransactionEquipment> findByFuelTransactionId(
			Integer fuelTransactionId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTransactionEquipment e where e.clientId = :clientId and e.fuelTransaction.id = :fuelTransactionId",
						FuelTransactionEquipment.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelTransactionId", fuelTransactionId)
				.getResultList();
	}
}
