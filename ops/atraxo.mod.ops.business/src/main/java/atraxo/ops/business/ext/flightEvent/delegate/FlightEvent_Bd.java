package atraxo.ops.business.ext.flightEvent.delegate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class FlightEvent_Bd extends AbstractBusinessDelegate {

	@Transactional
	public void removeFlightTypes(List<? super Integer> ids, Class<?> clazz) throws BusinessException {
		IFlightEventService service = (IFlightEventService) this.findEntityService(FlightEvent.class);
		List<FlightEvent> markForUpdate = new ArrayList<FlightEvent>();
		if (clazz.isAssignableFrom(FuelRequest.class)) {
			for (Object id : ids) {
				List<FlightEvent> flightTypes = service.findByFuelRequestId((Integer) id);
				for (FlightEvent flightType : flightTypes) {
					flightType.setFuelRequest(null);
				}
				markForUpdate.addAll(flightTypes);
			}
			service.update(markForUpdate);
		}
		if (clazz.isAssignableFrom(FuelQuoteLocation.class)) {
			for (Object id : ids) {
				List<FlightEvent> flightTypes = service.findByLocQuoteId((Integer) id);
				for (FlightEvent flightType : flightTypes) {
					flightType.setLocQuote(null);
				}
				markForUpdate.addAll(flightTypes);
			}
			service.update(markForUpdate);
		}
		if (clazz.isAssignableFrom(FuelOrderLocation.class)) {
			for (Object id : ids) {
				List<FlightEvent> flightTypes = service.findByLocOrderId((Integer) id);
				for (FlightEvent flightType : flightTypes) {
					flightType.setLocOrder(null);
				}
				markForUpdate.addAll(flightTypes);
			}
			service.updateWithoutBusiness(markForUpdate);
		}
		for (FlightEvent flightEvent : markForUpdate) {
			if (this.isOrfan(flightEvent)) {
				service.deleteById(flightEvent.getId());
			}
		}

	}

	private boolean isOrfan(FlightEvent flightEvent) {
		return flightEvent.getFuelRequest() == null && flightEvent.getLocQuote() == null && flightEvent.getLocOrder() == null;
	}
}
