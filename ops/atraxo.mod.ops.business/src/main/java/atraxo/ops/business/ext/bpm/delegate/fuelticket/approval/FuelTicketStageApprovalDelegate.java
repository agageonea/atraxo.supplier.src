package atraxo.ops.business.ext.bpm.delegate.fuelticket.approval;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.ApprovalDelegate;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailAddressesDto;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.ext.mailmerge.dto.FuelTicketDto;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

/**
 * @author abolindu
 * @author vhojda
 */
public abstract class FuelTicketStageApprovalDelegate extends ApprovalDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(FuelTicketStageApprovalDelegate.class);

	@Autowired
	protected IFuelTicketService fuelTicketService;

	@Autowired
	protected IWorkflowParameterService workflowParameterService;

	@Autowired
	protected ISystemParameterService systemParameterService;

	/**
	 * @param fuelTicket
	 * @param users
	 * @param checkBlocked
	 *            true if the blocked user(s) should be checked, false otherwise
	 * @return
	 */
	protected EmailDto extractEmailDto(FuelTicket fuelTicket, List<UserSupp> users, boolean checkBlocked) {
		EmailDto emailDTO = new EmailDto();

		Date deliveryDate = null;
		String ticketNumber = null;
		String departure = null;
		String customerName = null;
		String supplierCode = null;
		String unitOfMeasure = null;
		BigDecimal quantity = null;
		boolean blocked = false;

		try {
			deliveryDate = fuelTicket.getDeliveryDate();
			ticketNumber = fuelTicket.getTicketNo();
			departure = fuelTicket.getDeparture().getName();
			customerName = fuelTicket.getCustomer().getName();
			supplierCode = fuelTicket.getSupplier().getCode();
			unitOfMeasure = fuelTicket.getUpliftUnit().getCode();
			quantity = fuelTicket.getUpliftVolume();
			blocked = checkBlocked ? fuelTicket.getCustomer().getStatus().equals(CustomerStatus._BLOCKED_) : false;
		} catch (Exception e) {
			LOGGER.error("ERROR:could not extract data from the Fuel Ticket " + fuelTicket.getId() + " in order to send the mail ! ", e);
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					"Could not extract data from the Fuel Ticket " + fuelTicket.getId() + " in order to send the mail !");
			emailDTO = null;
		}

		if (emailDTO != null) {

			String subject = new StringBuilder("Fuel Ticket ").append(fuelTicket.getTicketNo()).append(" at ")
					.append(fuelTicket.getDeparture().getName()).append(" for ").append(blocked ? "blocked customer " : "")
					.append(fuelTicket.getCustomer().getName()).append(" from ").append(fuelTicket.getSupplier().getName())
					.append(" awaiting your approval").toString();
			emailDTO.setSubject(subject);

			Object emailAttachments = this.execution.getVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_ATTACHMENTS);
			if (emailAttachments != null && emailAttachments instanceof List) {
				emailDTO.setAttachments((List<String>) emailAttachments);
			}

			List<EmailAddressesDto> emailAddressesDtoList = new ArrayList<>();
			for (UserSupp user : users) {
				FuelTicketDto dto = new FuelTicketDto();
				dto.setApproveNote(this.getVariableFromWorkflow(WorkflowVariablesConstants.APPROVE_NOTE));
				dto.setFullnameOfRequester(this.getVariableFromWorkflow(WorkflowVariablesConstants.FULLNAME_REQUESTER_VAR));
				dto.setFullName(user.getFirstName() + " " + user.getLastName());
				dto.setTitle(user.getTitle().getName());
				dto.setDeliveryDate(deliveryDate);
				dto.setTicketNumber(ticketNumber);
				dto.setDeparture(departure);
				dto.setCustomerName(customerName);
				dto.setSupplierCode(supplierCode);
				dto.setUnitOfMeasure(unitOfMeasure);
				dto.setQuantity(quantity);

				EmailAddressesDto emailAddressesDto = new EmailAddressesDto();
				emailAddressesDto.setAddress(user.getEmail());
				emailAddressesDto.setData(dto);
				emailAddressesDtoList.add(emailAddressesDto);
			}
			emailDTO.setTo(emailAddressesDtoList);
		}
		return emailDTO;
	}

	/**
	 * @return
	 */
	protected FuelTicket extractFuelTicket() {
		FuelTicket fuelTicket = null;
		Object fuelTicketId = this.execution.getVariable(WorkflowVariablesConstants.FUEL_TICKET_ID_VAR);
		if (fuelTicketId != null) {
			try {
				fuelTicket = this.fuelTicketService.findById(fuelTicketId);
			} catch (Exception e) {
				LOGGER.error("ERROR:could not find an Fuel Ticket for ID " + fuelTicketId, e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						"Could not find an Fuel Ticket for ID " + fuelTicketId);
			}
		} else {
			this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
					"The variable for the id of the Fuel Ticket could not be found !");
		}
		return fuelTicket;
	}

	/**
	 * @param required
	 */
	protected void setNextApprovalStageRequiredVariable(boolean required) {
		this.execution.setVariable(WorkflowVariablesConstants.NEXT_APPROVAL_STAGE_REQUIRED, required);
	}

	/**
	 * @param required
	 */
	protected void setSecondApprovalStageRequiredVariable(boolean required) {
		this.execution.setVariable(WorkflowVariablesConstants.SECOND_APPROVAL_STAGE_REQUIRED, required);
	}

	@Override
	public String getSessionUserCode() {
		return null;
	}

}
