package atraxo.ops.business.ext.fuelTicket.job.tasklet;

import java.util.Set;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import atraxo.ops.business.api.ext.fuelTicket.IFuelTicketWorkflowStarter;
import atraxo.ops.business.ext.fuelTransaction.job.BufferedTicketWriter;
import seava.j4e.api.session.Session;

/**
 * @author zspeter
 */
public class ApproveFuelTicketTasklet implements Tasklet {

	@Autowired
	private IFuelTicketWorkflowStarter fuelTicketWkfStarter;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		Object obj = chunkContext.getStepContext().getJobExecutionContext().get(BufferedTicketWriter.TICKETS);
		@SuppressWarnings("unchecked")
		Set<String> ids = (Set<String>) obj;
		if (!CollectionUtils.isEmpty(ids)) {
			this.fuelTicketWkfStarter.startFuelTicketWorkflowAsync(ids, Session.user.get().getCode(), Session.user.get().getClientId(),
					Session.user.get().getProfile().getOrganizations());
		}
		return RepeatStatus.FINISHED;
	}

}
