package atraxo.ops.business.ext.fuelTicket.job.items;

import java.io.File;
import java.util.UUID;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.xml.StaxEventItemWriter;
import org.springframework.core.io.FileSystemResource;

import atraxo.ops.domain.ext.fuelplus.jetA.fuelticket.TicketBatch;

public class ExportTicketItemWriter extends StaxEventItemWriter<TicketBatch> {

	private String tmpDir;

	public void setTmpDir(String tmpDir) {
		this.tmpDir = tmpDir.substring(tmpDir.indexOf(':') + 1);
	}

	@BeforeStep
	public void changeResource(StepExecution stepExecution) {
		String tmpFile = this.tmpDir + File.separator + UUID.randomUUID().toString();
		stepExecution.getJobExecution().getExecutionContext().putString(ExportTicketItemProcessor.TMP_FILE, tmpFile);
		this.setResource(new FileSystemResource(tmpFile));
	}
}
