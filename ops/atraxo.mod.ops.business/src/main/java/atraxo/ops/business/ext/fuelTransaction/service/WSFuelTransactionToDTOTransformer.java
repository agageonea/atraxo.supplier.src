package atraxo.ops.business.ext.fuelTransaction.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.springframework.util.CollectionUtils;

import atraxo.fmbas.business.api.ws.WSModelToDTOTransformer;
import atraxo.fmbas.domain.ws.dto.WSPayloadDataDTO;
import atraxo.fmbas.domain.ws.dto.WSTransportDataType;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.ops_type.ValidationState;
import atraxo.ops.domain.ws.FuelTicketTransmissionAcknowledgement;
import atraxo.ops.domain.ws.FuelTicketTransmissionAcknowledgement.FuelTicketAcknowledgement;
import seava.j4e.business.utils.DateUtils;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author zspeter
 */
public class WSFuelTransactionToDTOTransformer implements WSModelToDTOTransformer<FuelTransaction> {

	/*
	 * (non-Javadoc)
	 * @see atraxo.fmbas.business.api.ws.WSModelToDTOTransformer#transformModelToDTO(java.util.Collection, java.util.Map)
	 */
	@Override
	public WSPayloadDataDTO transformModelToDTO(Collection<FuelTransaction> modelEntities, Map<String, String> paramMap) throws JAXBException {
		WSPayloadDataDTO dto = new WSPayloadDataDTO();
		FuelTicketTransmissionAcknowledgement ftta = this.buildAcknowledgement(modelEntities);
		dto.setPayload(XMLUtils.toXML(ftta, ftta.getClass()));
		return dto;
	}

	private FuelTicketTransmissionAcknowledgement buildAcknowledgement(Collection<FuelTransaction> transactions) {
		FuelTicketTransmissionAcknowledgement ftta = new FuelTicketTransmissionAcknowledgement();
		if (!CollectionUtils.isEmpty(transactions)) {
			for (FuelTransaction ft : transactions) {
				FuelTicketAcknowledgement fta = new FuelTicketAcknowledgement();
				if (ValidationState._EMPTY_.equals(ft.getValidationState())) {
					fta.setProcessed(Boolean.TRUE.toString().toUpperCase());
					fta.setComment("Fuel transaction processed successfully");
				} else {
					fta.setProcessed(Boolean.FALSE.toString().toUpperCase());
					fta.setComment(ft.getValidationResult());
				}
				fta.setResend(Boolean.FALSE.toString().toUpperCase());
				fta.setDateTimeStamp(DateUtils.toXMLGregorianCalendar(Calendar.getInstance().getTime()));
				fta.setUniqueID(ft.getUniqueTicketID());
				ftta.getFuelTicketAcknowledgement().add(fta);
			}
		}
		return ftta;
	}

	@Override
	public WSTransportDataType getTransportDataType() {
		return WSTransportDataType.FUEL_TRANSACTIONS;
	}

}
