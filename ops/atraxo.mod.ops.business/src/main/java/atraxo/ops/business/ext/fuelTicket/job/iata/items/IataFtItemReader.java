package atraxo.ops.business.ext.fuelTicket.job.iata.items;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ResourceAware;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.xml.sax.SAXException;

import atraxo.ops.business.ext.fuelTicket.job.iata.listener.IataFtListener;

public class IataFtItemReader<T> extends MultiResourceItemReader<T> {

	private ResourceAware delegate1;
	private StepExecution stepExecution;
	private Validator validator;

	@BeforeStep
	public void beforeStep(StepExecution stepExecution) throws SAXException {
		this.stepExecution = stepExecution;
		System.setProperty("jdk.xml.maxOccurLimit", "200000");
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = factory.newSchema(ClassPathResource.class.getResource("/xsd/iata/IataFuelTransactionStandard.xsd"));
		this.validator = schema.newValidator();
	}

	/**
	 * @param delegate reads items from single {@link Resource}.
	 */
	public void setDelegate1(ResourceAware delegate1) {
		this.delegate1 = delegate1;
	}

	@Override
	public T read() throws Exception {

		T read = super.read();
		if (read != null) {
			while (!this.validateXMLSchema(this.getCurrentResource())) {
				this.addResourcesToContext(IataFtListener.INV_RESOURCES, this.getCurrentResource().getFilename(),
						this.stepExecution.getJobExecution().getExecutionContext());
				read = super.read();
				if (read == null) {
					break;
				}
			}
		}
		if (this.getCurrentResource() != null) {
			this.addResourcesToContext(IataFtListener.RESOURCES, this.getCurrentResource().getFilename(),
					this.stepExecution.getJobExecution().getExecutionContext());
			this.delegate1.setResource(this.getCurrentResource());
		}
		return read;
	}

	private boolean validateXMLSchema(Resource resource) throws IOException {
		InputStream inputStream = resource.getInputStream();
		try {
			this.validator.validate(new StreamSource(inputStream));
		} catch (SAXException e) {
			this.stepExecution.addFailureException(e);
			if (this.stepExecution.getJobExecution().getExecutionContext().containsKey(IataFtListener.FAILURE_MSG)) {
				this.stepExecution.getJobExecution().getExecutionContext().put(IataFtListener.FAILURE_MSG,
						new StringBuilder(this.stepExecution.getJobExecution().getExecutionContext().getString(IataFtListener.FAILURE_MSG))
								.append("\r\n").append(resource.getFilename()).append(" ").append(e.getLocalizedMessage()).toString());
			} else {
				this.stepExecution.getJobExecution().getExecutionContext().put(IataFtListener.FAILURE_MSG,
						new StringBuilder(resource.getFilename()).append(" ").append(e.getLocalizedMessage()).toString());
			}
			return false;
		} finally {
			inputStream.close();
		}
		return true;
	}

	private void addResourcesToContext(String key, String fileName, ExecutionContext executionContext) {
		if (!executionContext.containsKey(key)) {
			executionContext.put(key, new HashMap<String, Resource>());
		}
		@SuppressWarnings("unchecked")
		Map<String, Resource> resources = (Map<String, Resource>) executionContext.get(key);
		if (!resources.containsKey(fileName)) {
			resources.put(fileName, this.getCurrentResource());
		}
	}

}
