package atraxo.ops.business.ext.fuelOrder.delete;

import java.util.ArrayList;
import java.util.List;

import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.business.api.fuelQuotation.IFuelQuotationService;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class DeleteFuelOrder_Bd extends AbstractBusinessDelegate {

	public void removeQuotationReference(List<Object> ids) throws BusinessException {
		IFuelQuotationService service = (IFuelQuotationService) this.findEntityService(FuelQuotation.class);
		for (Object id : ids) {
			List<FuelQuotation> fuelQuotations = service.findByFuelOrderId((Integer) id);
			for (FuelQuotation fq : fuelQuotations) {
				fq.setFuelOrder(null);
				fq.setStatus(FuelQuoteStatus._SUBMITTED_);
			}
			service.update(fuelQuotations);
		}
	}

	public void deleteFuelOrderLocations(List<Object> fuelOrderIdList) throws BusinessException {
		IFuelOrderLocationService service = (IFuelOrderLocationService) this.findEntityService(FuelOrderLocation.class);
		List<FuelOrderLocation> fuelOrderLocations = new ArrayList<>();
		for (Object fuelOrderId : fuelOrderIdList) {
			fuelOrderLocations.addAll(service.findByFuelOrderId((Integer) fuelOrderId));
		}
		service.delete(fuelOrderLocations);
	}

}
