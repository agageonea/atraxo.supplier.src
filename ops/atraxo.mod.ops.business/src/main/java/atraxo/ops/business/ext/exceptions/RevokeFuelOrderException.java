package atraxo.ops.business.ext.exceptions;

import seava.j4e.api.exceptions.BusinessException;

public class RevokeFuelOrderException extends BusinessException {

	public RevokeFuelOrderException(String invoiceCode) {
		super(OpsErrorCode.FUEL_ORDER_INVOICE, String.format(OpsErrorCode.FUEL_ORDER_INVOICE.getErrMsg(), invoiceCode));
	}
}
