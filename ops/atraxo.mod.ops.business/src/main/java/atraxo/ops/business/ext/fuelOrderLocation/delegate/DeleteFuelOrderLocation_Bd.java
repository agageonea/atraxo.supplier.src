package atraxo.ops.business.ext.fuelOrderLocation.delegate;

import java.util.Arrays;
import java.util.List;

import atraxo.ops.business.ext.flightEvent.delegate.FlightEvent_Bd;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class DeleteFuelOrderLocation_Bd extends AbstractBusinessDelegate {

	public void deleteFlightEvents(List<FuelOrderLocation> fuelOrderLocations) throws BusinessException {
		FlightEvent_Bd bd = this.getBusinessDelegate(FlightEvent_Bd.class);
		for (FuelOrderLocation fol : fuelOrderLocations) {
			this.deleteFlightEvents(fol, bd);
		}
	}

	public void deleteFlightEvents(FuelOrderLocation fol) throws BusinessException {
		FlightEvent_Bd bd = this.getBusinessDelegate(FlightEvent_Bd.class);
		this.deleteFlightEvents(fol, bd);
	}

	private void deleteFlightEvents(FuelOrderLocation fol, FlightEvent_Bd bd) throws BusinessException {
		bd.removeFlightTypes(Arrays.asList(fol.getId()), FuelOrderLocation.class);

	}
}
