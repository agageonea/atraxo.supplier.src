/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelQuotation;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelQuotation} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelQuotation_Service extends AbstractEntityService<FuelQuotation> {

	/**
	 * Public constructor for FuelQuotation_Service
	 */
	public FuelQuotation_Service() {
		super();
	}

	/**
	 * Public constructor for FuelQuotation_Service
	 * 
	 * @param em
	 */
	public FuelQuotation_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelQuotation> getEntityClass() {
		return FuelQuotation.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelQuotation
	 */
	public FuelQuotation findByCode(String code) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelQuotation.NQ_FIND_BY_CODE,
							FuelQuotation.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("code", code).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelQuotation", "code"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelQuotation", "code"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelQuotation
	 */
	public FuelQuotation findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelQuotation.NQ_FIND_BY_BUSINESS,
							FuelQuotation.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelQuotation", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelQuotation", "id"), nure);
		}
	}

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuotation e where e.clientId = :clientId and e.customer.id = :customerId",
						FuelQuotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: fuelRequest
	 *
	 * @param fuelRequest
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelRequest(FuelRequest fuelRequest) {
		return this.findByFuelRequestId(fuelRequest.getId());
	}
	/**
	 * Find by ID of reference: fuelRequest.id
	 *
	 * @param fuelRequestId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelRequestId(Integer fuelRequestId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuotation e where e.clientId = :clientId and e.fuelRequest.id = :fuelRequestId",
						FuelQuotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelRequestId", fuelRequestId).getResultList();
	}
	/**
	 * Find by reference: fuelOrder
	 *
	 * @param fuelOrder
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelOrder(FuelOrder fuelOrder) {
		return this.findByFuelOrderId(fuelOrder.getId());
	}
	/**
	 * Find by ID of reference: fuelOrder.id
	 *
	 * @param fuelOrderId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelOrderId(Integer fuelOrderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuotation e where e.clientId = :clientId and e.fuelOrder.id = :fuelOrderId",
						FuelQuotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelOrderId", fuelOrderId).getResultList();
	}
	/**
	 * Find by reference: holder
	 *
	 * @param holder
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByHolder(Customer holder) {
		return this.findByHolderId(holder.getId());
	}
	/**
	 * Find by ID of reference: holder.id
	 *
	 * @param holderId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByHolderId(Integer holderId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelQuotation e where e.clientId = :clientId and e.holder.id = :holderId",
						FuelQuotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("holderId", holderId).getResultList();
	}
	/**
	 * Find by reference: fuelQuoteLocations
	 *
	 * @param fuelQuoteLocations
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelQuoteLocations(
			FuelQuoteLocation fuelQuoteLocations) {
		return this.findByFuelQuoteLocationsId(fuelQuoteLocations.getId());
	}
	/**
	 * Find by ID of reference: fuelQuoteLocations.id
	 *
	 * @param fuelQuoteLocationsId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelQuoteLocationsId(
			Integer fuelQuoteLocationsId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelQuotation e, IN (e.fuelQuoteLocations) c where e.clientId = :clientId and c.id = :fuelQuoteLocationsId",
						FuelQuotation.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelQuoteLocationsId", fuelQuoteLocationsId)
				.getResultList();
	}
}
