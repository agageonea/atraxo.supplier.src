package atraxo.ops.business.ext.fuelTicket.service;

import java.util.Date;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceHistoryService;
import atraxo.fmbas.business.api.externalInterfaces.IExternalInterfaceService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.exceptions.BusinessErrorCode;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceHistoryFacade;
import atraxo.fmbas.business.ext.externalInterfaces.service.ExternalInterfaceMessageHistoryFacade;
import atraxo.fmbas.business.ws.GUIDMessageGenerator;
import atraxo.fmbas.domain.ext.externalInterface.ExtInterfaceNames;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterface;
import atraxo.fmbas.domain.impl.externalInterfaces.ExternalInterfaceHistory;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfaceMessageHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ExternalInterfacesHistoryStatus;
import atraxo.fmbas.domain.impl.fmbas_type.ProcessStatus;
import atraxo.fmbas.domain.impl.fmbas_type.TransportStatus;
import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import atraxo.fmbas.domain.ws.messgeXSD.MSG.HeaderCommon;
import atraxo.ops.business.api.ext.fuelTicket.IFuelTicketAccepter;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.commons.utils.XMLUtils;

/**
 * @author zspeter
 */
public class FuelTicketAccepterImpl implements IFuelTicketAccepter {

	private static final Logger LOG = LoggerFactory.getLogger(FuelTicketAccepterImpl.class);

	@Autowired
	private IExternalInterfaceService externalInterfaceService;
	@Autowired
	private ExternalInterfaceHistoryFacade externalInterfaceHistoryFacade;
	@Autowired
	private IUserSuppService userSrv;
	@Autowired
	private ExternalInterfaceMessageHistoryFacade messageHistoryFacade;
	@Autowired
	private IExternalInterfaceHistoryService historyService;

	@Override
	public MSG acknoledgeFuelTicketProcessingResult(MSG request) throws BusinessException {
		boolean success = false;
		HeaderCommon headerCommon = request.getHeaderCommon();
		try {
			String clientIdentifier = GUIDMessageGenerator.extractClientIdentifier(headerCommon.getCorrelationID());
			this.userSrv.createSessionUser(clientIdentifier);
			ExternalInterface externalInterface = this.externalInterfaceService.findByName(ExtInterfaceNames.ACK_FUEL_TICKET_INTERFACE.getValue());
			StopWatch stopWatch = new StopWatch();
			stopWatch.start();
			String historyDetails = "";
			try {
				this.verifyInterfaceEnabbled(externalInterface);
				historyDetails = XMLUtils.toString(request, MSG.class, true, true);
				if (TransportStatus.getByName(headerCommon.getTransportStatus()).equals(TransportStatus._SENT_)) {
					success = true;
					headerCommon.setErrorCode("Fuel Ticket processing acknowledgement processed success!");
					LOG.info("Fuel Ticket processing acknowledgement processed success!");
				} else {
					headerCommon.setErrorCode("Fuel Ticket processing acknowledgement processed success with transposrt status different from Sent!");
					LOG.info("Fuel Ticket processing acknowledgement processed success with transposrt status different from Sent!");
				}
				stopWatch.stop();
				this.saveToHistory(externalInterface, stopWatch, success, historyDetails);
				this.historyService.updateHistoryOutboundFromACK(success, request.getHeaderCommon().getCorrelationID(), true);
			} catch (BusinessException be) {
				headerCommon.setErrorCode(be.getMessage());
				LOG.warn("Interface is disabled!", be);
			} catch (Exception e) {
				headerCommon.setErrorCode(e.getMessage());
				LOG.warn("Fuel Ticket processing acknowledgement processed failure!", e);
				stopWatch.stop();
				this.saveToHistory(externalInterface, stopWatch, success, historyDetails);
				this.historyService.updateHistoryOutboundFromACK(success, request.getHeaderCommon().getCorrelationID(), false);
			}
			this.updateMessageHistory(request, success);
		} catch (Exception e) {
			LOG.warn("Fuel Ticket processing acknowledgement processed failure!", e);
			headerCommon.setErrorCode(e.getMessage());
		}

		headerCommon.setErrorDescription(headerCommon.getErrorCode());

		request.getHeaderCommon().setTransportStatus(TransportStatus._PROCESSED_.getName());
		request.getHeaderCommon().setProcessStatus(success ? ProcessStatus._SUCCESS_.getName() : ProcessStatus._FAILURE_.getName());

		return request;
	}

	private void updateMessageHistory(MSG message, boolean success) throws BusinessException {
		ExternalInterfaceMessageHistoryStatus status = success ? ExternalInterfaceMessageHistoryStatus._ACKNOWLEDGED_
				: ExternalInterfaceMessageHistoryStatus._FAILED_;
		this.messageHistoryFacade.updateMessageHistory(message, status);
	}

	private void saveToHistory(ExternalInterface externalInterface, StopWatch stopWatch, boolean success, String message) throws BusinessException {
		ExternalInterfaceHistory history = new ExternalInterfaceHistory();
		history.setExternalInterface(externalInterface);
		history.setRequester(Session.user.get().getClientCode());
		history.setRequestReceived(new Date(stopWatch.getStartTime()));
		history.setResponseSent(new Date(stopWatch.getStartTime() + stopWatch.getTime()));
		history.setStatus(success ? ExternalInterfacesHistoryStatus._SUCCESS_ : ExternalInterfacesHistoryStatus._FAILURE_);
		history.setResult(message);
		this.externalInterfaceHistoryFacade.insert(history);
	}

	protected void verifyInterfaceEnabbled(ExternalInterface externalInterface) throws BusinessException {
		if (!externalInterface.getEnabled()) {
			throw new BusinessException(BusinessErrorCode.INTERFACE_DISABLED, BusinessErrorCode.INTERFACE_DISABLED.getErrMsg());
		}
	}
}
