/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.fuelTicket.service;

import atraxo.ops.business.api.fuelTicket.IFuelTicketHardCopyService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicketHardCopy;

/**
 * Business extensions specific for {@link FuelTicketHardCopy} domain entity.
 * 
 */
public class FuelTicketHardCopy_Service extends atraxo.ops.business.impl.fuelTicket.FuelTicketHardCopy_Service 
	implements IFuelTicketHardCopyService{

	//Implement me

}
