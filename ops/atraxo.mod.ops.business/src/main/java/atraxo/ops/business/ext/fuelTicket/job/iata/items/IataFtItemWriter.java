package atraxo.ops.business.ext.fuelTicket.job.iata.items;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ops.business.api.fuelTransaction.IFuelTransactionService;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;

public class IataFtItemWriter implements ItemWriter<FuelTransaction> {

	@Autowired
	private IFuelTransactionService srv;

	@SuppressWarnings("unchecked")
	@Override
	public void write(List<? extends FuelTransaction> items) throws Exception {
		this.srv.fetch((List<FuelTransaction>) items);
	}

}
