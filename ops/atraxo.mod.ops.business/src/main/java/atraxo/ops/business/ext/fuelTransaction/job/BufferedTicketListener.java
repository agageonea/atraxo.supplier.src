package atraxo.ops.business.ext.fuelTransaction.job;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;

import atraxo.fmbas.domain.impl.fmbas_type.JobRunResult;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import seava.j4e.scheduler.batch.listener.DefaultActionListener;

public class BufferedTicketListener extends DefaultActionListener {

	private static final String FAILED = "failed";
	private static final String NOT_FAILED = "not failed";

	@Override
	public void afterJob(JobExecution jobExecution) {
		super.afterJob(jobExecution);
		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder(status.getExitDescription());
		List<StepExecution> stepExecution = (List<StepExecution>) jobExecution.getStepExecutions();
		int failed = 0;
		int notfailed = 0;

		for (StepExecution se : stepExecution) {
			ExecutionContext executionContext = se.getExecutionContext();
			if (executionContext.containsKey(FAILED)) {
				failed = (Integer) executionContext.get(FAILED);
				sb.append(OpsErrorCode.FT_FAILED_PROCESSED.getErrMsg());
				sb.append(failed);
			}
			if (executionContext.containsKey(NOT_FAILED)) {
				sb.append("\n");
				notfailed = (Integer) executionContext.get(NOT_FAILED);
				sb.append(OpsErrorCode.FT_SUCCESFULL_PROCESSED.getErrMsg());
				sb.append(notfailed);
			}
		}
		if (sb.toString().equals("|")) {
			sb.append(OpsErrorCode.FT_SUCCESFULL_PROCESSED.getErrMsg() + ": 0");
			sb.append("\n");
			sb.append(OpsErrorCode.FT_FAILED_PROCESSED.getErrMsg() + ": 0");
			sb.deleteCharAt(0);
		}

		if (!status.getExitCode().equals(ExitStatus.FAILED.getExitCode())) {
			if (failed != 0) {
				status = new ExitStatus(JobRunResult._COMPLETED___FAILED_.toString(), sb.toString());
			} else {
				status = new ExitStatus(JobRunResult._COMPLETED___SUCCESSFUL_.toString(), sb.toString());
			}
		}

		jobExecution.setExitStatus(status);

	}

}
