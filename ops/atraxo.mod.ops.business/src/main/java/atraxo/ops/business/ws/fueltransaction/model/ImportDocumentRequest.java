package atraxo.ops.business.ws.fueltransaction.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlCDATA;

@XmlRootElement(name = "request")
public class ImportDocumentRequest implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -1306582212960480872L;

	private String clientCode;
	private String requester;
	private String ftTransmission;
	private NavisionParameter navisionParameters;

	// @XmlElement(name = "FuelTransaction", required = true, namespace = "http://www.IATA.com/IATAFuelCodeDirectory")
	// public FuelTransactionTransmission getFtTransmission() {
	// return this.ftTransmission;
	// }
	//
	// public void setFtTransmission(FuelTransactionTransmission ftTransmission) {
	// this.ftTransmission = ftTransmission;
	// }

	@XmlElement(name = "client", required = true)
	public String getClientCode() {
		return this.clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	@XmlCDATA
	public String getFtTransmission() {
		return this.ftTransmission;
	}

	public void setFtTransmission(String ftTransmission) {
		this.ftTransmission = ftTransmission;
	}

	@XmlElement(name = "requester", required = true)
	public String getRequester() {
		return this.requester;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

	@XmlElement(name = "navisionParameters", required = false)
	public NavisionParameter getNavisionParameters() {
		return this.navisionParameters;
	}

	public void setNavisionParameters(NavisionParameter navisionParameters) {
		this.navisionParameters = navisionParameters;
	}

}
