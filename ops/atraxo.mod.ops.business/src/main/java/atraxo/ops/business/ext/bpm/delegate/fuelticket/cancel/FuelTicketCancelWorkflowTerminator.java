package atraxo.ops.business.ext.bpm.delegate.fuelticket.cancel;

import atraxo.fmbas.business.api.custom.workflow.IWorkflowTerminatorService;
import atraxo.fmbas.domain.impl.workflow.WorkflowInstance;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class FuelTicketCancelWorkflowTerminator extends AbstractBusinessBaseService implements IWorkflowTerminatorService {

	@Override
	public void terminateWorkflow(WorkflowInstance workflowInstance, String reason) throws BusinessException {
		// this workflow has nothing to do on terminate
	}

}
