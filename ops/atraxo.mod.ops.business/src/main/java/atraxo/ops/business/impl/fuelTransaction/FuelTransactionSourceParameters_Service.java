/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelTransaction;

import atraxo.ops.business.api.fuelTransaction.IFuelTransactionSourceParametersService;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSourceParameters;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelTransactionSourceParameters} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelTransactionSourceParameters_Service
		extends
			AbstractEntityService<FuelTransactionSourceParameters>
		implements
			IFuelTransactionSourceParametersService {

	/**
	 * Public constructor for FuelTransactionSourceParameters_Service
	 */
	public FuelTransactionSourceParameters_Service() {
		super();
	}

	/**
	 * Public constructor for FuelTransactionSourceParameters_Service
	 * 
	 * @param em
	 */
	public FuelTransactionSourceParameters_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelTransactionSourceParameters> getEntityClass() {
		return FuelTransactionSourceParameters.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSourceParameters
	 */
	public FuelTransactionSourceParameters findByCode(
			FuelTransactionSource fuelTransactionSource, String paramName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							FuelTransactionSourceParameters.NQ_FIND_BY_CODE,
							FuelTransactionSourceParameters.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("fuelTransactionSource",
							fuelTransactionSource)
					.setParameter("paramName", paramName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTransactionSourceParameters",
							"fuelTransactionSource, paramName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTransactionSourceParameters",
							"fuelTransactionSource, paramName"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSourceParameters
	 */
	public FuelTransactionSourceParameters findByCode(
			Long fuelTransactionSourceId, String paramName) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							FuelTransactionSourceParameters.NQ_FIND_BY_CODE_PRIMITIVE,
							FuelTransactionSourceParameters.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("fuelTransactionSourceId",
							fuelTransactionSourceId)
					.setParameter("paramName", paramName).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTransactionSourceParameters",
							"fuelTransactionSourceId, paramName"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTransactionSourceParameters",
							"fuelTransactionSourceId, paramName"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSourceParameters
	 */
	public FuelTransactionSourceParameters findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(
							FuelTransactionSourceParameters.NQ_FIND_BY_BUSINESS,
							FuelTransactionSourceParameters.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelTransactionSourceParameters", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelTransactionSourceParameters", "id"), nure);
		}
	}

	/**
	 * Find by reference: fuelTransactionSource
	 *
	 * @param fuelTransactionSource
	 * @return List<FuelTransactionSourceParameters>
	 */
	public List<FuelTransactionSourceParameters> findByFuelTransactionSource(
			FuelTransactionSource fuelTransactionSource) {
		return this
				.findByFuelTransactionSourceId(fuelTransactionSource.getId());
	}
	/**
	 * Find by ID of reference: fuelTransactionSource.id
	 *
	 * @param fuelTransactionSourceId
	 * @return List<FuelTransactionSourceParameters>
	 */
	public List<FuelTransactionSourceParameters> findByFuelTransactionSourceId(
			Integer fuelTransactionSourceId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelTransactionSourceParameters e where e.clientId = :clientId and e.fuelTransactionSource.id = :fuelTransactionSourceId",
						FuelTransactionSourceParameters.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelTransactionSourceId",
						fuelTransactionSourceId).getResultList();
	}
}
