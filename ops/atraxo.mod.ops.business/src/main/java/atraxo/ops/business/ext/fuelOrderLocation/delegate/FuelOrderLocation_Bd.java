package atraxo.ops.business.ext.fuelOrderLocation.delegate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.business.ext.flightEvent.delegate.QuantityCalculator_Bd;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class FuelOrderLocation_Bd extends AbstractBusinessDelegate {

	private void updateFuelingDates(FuelOrderLocation fol, FlightEvent flightEvent) throws BusinessException {
		if (fol.getFuelReleaseEndsOn() == null) {
			fol.setFuelReleaseEndsOn(flightEvent.getUpliftDate());
		} else if (flightEvent.getUpliftDate().after(fol.getFuelReleaseEndsOn())) {
			fol.setFuelReleaseEndsOn(flightEvent.getUpliftDate());
		}
		if (fol.getFuelReleaseStartsOn() == null) {
			fol.setFuelReleaseStartsOn(flightEvent.getUpliftDate());
		} else if (flightEvent.getUpliftDate().before(fol.getFuelReleaseStartsOn())) {
			fol.setFuelReleaseStartsOn(flightEvent.getUpliftDate());
		}
	}

	public void updateOnFlightEventChange(FuelOrderLocation fol, FlightEvent flightEvent) throws BusinessException {
		this.calculateQuantity(fol, fol.getFlightEvents());
		this.updateFuelingDates(fol, flightEvent);
		this.mergeEntity(fol);
	}

	public void upadteOnFlightEventRemove(List<Object> ids, FuelOrderLocation fol) throws BusinessException {
		Collection<FlightEvent> flightEvents = new ArrayList<>();
		for (FlightEvent fe : fol.getFlightEvents()) {
			if (!ids.contains(fe.getId())) {
				flightEvents.add(fe);
			}
		}
		this.calculateQuantity(fol, flightEvents);
		this.mergeEntity(fol);
	}

	private void mergeEntity(FuelOrderLocation fol) throws BusinessException {
		IFuelOrderLocationService fols = (IFuelOrderLocationService) this.findEntityService(FuelOrderLocation.class);
		fols.update(fol);
	}

	private void calculateQuantity(FuelOrderLocation fol, Collection<FlightEvent> flightEvents) throws BusinessException {
		QuantityCalculator_Bd quantityCalculator = this.getBusinessDelegate(QuantityCalculator_Bd.class);
		BigDecimal quantity = quantityCalculator.calculateQuantity(flightEvents, fol.getPaymentUnit());
		fol.setQuantity(quantity);
	}

}
