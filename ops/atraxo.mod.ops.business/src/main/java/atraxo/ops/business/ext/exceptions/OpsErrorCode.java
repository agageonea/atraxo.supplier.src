package atraxo.ops.business.ext.exceptions;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.j4e.api.exceptions.IErrorCode;
import seava.j4e.api.session.Session;

/**
 * Error code used in operations module. Error codes should be between 5000 - 5999.
 *
 * @author zspeter
 */
public enum OpsErrorCode implements IErrorCode {

	// **************Errors*********//
	/* Flight events */
	DUPLICATE_EVENT_TYPE(5000, "Duplicate business key for flight types.", "Error"),

	FLIGHT_EVENT_DATE1(5001, "Arrival date cannot be after departure date.", "Error"),

	FLIGHT_EVENT_DATE2(5002, "Arrival date cannot be after fueling date.", "Error"),

	FLIGHT_EVENT_DATE3(5003, "Fueling date cannot be after departure date.", "Error"),

	FUELING_DATE_FUEL_RECEIVING(5004, "Fueling date must be greater than the fuel request receiving date.", "Error"),

	FLIGHT_EVENT_EVENT_NUMBER_ZERO(5005, "The number of events must be positive integer different than zero.", "Error"),

	PRICE_POLICY_ACTIVATE(5010, "You can activate or deactivate the price policy only during the validity period.", "Error"),

	PRICE_POLICY_COMBINATION(5011, "You already have a price policy with the same Customer-Location-Aircraft type combination.", "Error"),

	PRICE_POLICY_MARGIN_MARKUP(5012, "You need to define a default margin or markup.", "Error"),

	PRICE_POLICY_MINIMAL_MARGIN(5012, "You need to define a minimal margin.", "Error"),

	PRICE_POLICY_MINIMAL_MARKUP(5013, "You need to define a minimal markup.", "Error"),

	/* Fuel order */

	CAN_NOT_CANCEL_FUEL_ORDER(
			5020,
			"The selected purchase order cannot be cancelled because the status of the assigned flights is different than Scheduled.",
			"Error"),
	FUEL_ORDER_INVOICE(5021, "The selected purchase order cannot be cancelled because is used in the invoice %s.", "Error"),

	/* Fuel Quote */

	FUEL_QUOTE_LOCATION_NOT_ASSIGNED_CONTRACT(5030, "The location(s): %s must have a supply contract assigned.", "Error"),

	FUEL_QUOTE_NOT_ASSIGNED_LOCATION(5031, "The fuel quote must have a location assigned.", "Error"),

	FQ_CUSTOMER_APPROVED(5032, "Customer status must be approved.", "Error"),

	FQ_CUSTOMER_VALID(5033, "The customer entered is not valid.", "Error"),

	FQ_VALID_UNTIL_EMPTY(5034, "Valid until can't be empty.", "Error"),

	FQ_VALID_UNTIL_PAST(5035, "Valid until can't be in the past.", "Error"),

	FQL_FUEL_RELEASE_DATE1(5036, "Fuel release end date can not be before fuel release start date.", "Error"),

	FQL_FUEL_RELEASE_DATE2(5037, "Fuel release start date and end date shall be in the future.", "Error"),

	FUEL_QUOTE_LOCATION_COMBINATION(
			5038,
			"You already have a fuel quote location with the same location - flight type - operational type combination.",
			"Error"),

	FUEL_REQ_RELEASE_PER(5040, "The open release period must be indicated.", "Error"),

	FUEL_REQ_OPEN_RELEASE(5041, "The open release start date must be grater than the request receiving date.", "Error"),

	FUEL_REQ_CUST_EMPTY(5042, "The customer can not be empty.", "Error"),

	FUEL_REQ_RECEIV_ON_EMPTY(5043, "The received on can not be empty.", "Error"),

	FUEL_REQ_CHECKED_REQ(5044, "You can't set as checked requests without at least one flight event.", "Error"),

	VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO(5050, "Valid from can not be greater than valid to.", "Error"),

	FUEL_TICKET_REGISTRATION(5060, "The registration is not valid for this customer.", "Error"),
	FUEL_TICKET_UPDATE(5061, "Fuel ticket have been modified by a background process. Please refresh the page an try again.", "Error"),
	TANK_CAPACITY_EXCEED(5062, "Tank Capacity Exceeded!", "Error"),
	FUEL_TICKET_RPC_ACTION_ERROR(
			5063,
			"Action cannot be executed on the following Fuel Tickets because of the following reasons: %s The records have been reloaded. <br>Please try again.",
			"Validation"),

	FUEL_TICKET_CANCEL_REASON_IS_MISSING(5064, "Fuel ticket cancel reason is missing", "Error"),
	FUEL_TICKET_AIRCRAFT_REGISTRATION_NUMBER(
			5065,
			"The fuel ticket cannot be processed as it does not have an aircraft registration number specified.",
			"Error"),

	IATA_LOCATION_NOT_FOUND(5070, "Review your IATA location code in User Management.", "Error"),
	IATA_IMPORT_ERROR(5071, "Error occurred during IATA fuel ticket import of file %s.", "Error"),

	FT_VALIDATION_LOCATION_IS_NOT_ASSIGNED_TO_ANY_SUBSIDIARY_AREA(5074, "Location is not assigned to any subsidiary area.", "Error"),
	FT_VALIDATION_SUBSIDIARY_COULD_NOT_BE_DETERMINED(5075, "Subsidiary could not be determined.", "Error"),
	FT_VALIDATION_NO_FLIGHT_ID(5076, "Unknown flight ID", "Error"),
	FT_VALIDATION_INCORRECT_FLIGHT_ID(5077, "Invalid flight ID (AAXXXXY, AA - airline designator, XXXX - flight no, Y - suffix)", "Error"),
	FT_VALIDATION_ALREADY_CANCELED(5078, "Ticket has been already canceled.", "Error"),
	FT_VALIDATION_NO_ORIGINAL(5079, "Original ticket does not exists.", "Error"),
	FT_VALIDATION_NO_AIRPORT(5080, "Unknown airport.", "Error"),
	FT_VALIDATION_NO_CUSTOMER(5081, "Unknow ship-to.", "Error"),
	FT_VALIDATION_NO_SUPPLER(5082, "Unknown supplier.", "Error"),
	FT_VALIDATION_NO_IPL_AGENT(5083, "Unknown fueler.", "Error"),
	FT_VALIDATION_NO_AICRAFT_TYPE(5084, "Unknown aircraft type.", "Error"),
	FT_VALIDATION_UPLIFT_OVERDUE(5085, "Over uplift.", "Error"),
	FT_VALIDATION_NO_REFERENCE(5086, "Missing reference ticket.", "Error"),
	FT_VALIDATION_DUPLICATE(5087, "Not permited. Duplicate.", "Error"),
	FT_VALIDATION_REISSUED(5088, "Reissued ticket exists.", "Error"),
	FT_VALIDATION_CANCELED(5089, "Canceled ticket exists.", "Error"),

	FUEL_TICKET_STATUS_MUST_BE_CANCELLED(5090, "The fuel ticket status must be cancelled.", "Error"),
	FUEL_TICKET_CAN_NOT_BE_REISSUED(5091, "The fuel ticket can be reissued only once.", "Error"),
	FUEL_TICKET_CAN_NOT_BE_DELETED(5092, "The fuel ticket can not be deleted because it is referenced by a reissued ticket.", "Error"),

	FUEL_TICKET_CAN_NOT_BE_UNDONE(5093, "The fuel ticket can not be undone because it is of type reissued.", "Error"),
	FT_VALIDATION_NO_UPLIFT(5094, "Missing gross volume", "Error"),

	FUEL_TICKET_CAN_NOT_DELETED_IS_INVOICED(5095, "The fuel ticket can not delete because is invoiced or billed!", "Error"),
	FUEL_TICKET_ALREADY_INVOICED(5096, "Fuel ticket already invoiced!", "Error"),
	FUEL_TICKET_ORIGINAL_APPROVED(5097, "Fuel ticket with status Original is already approved!", "Error"),
	FUEL_TICKET_ALREADY_DELETED(5098, "Fuel ticket is already deleted!", "Error"),
	FUEL_TICKET_NO_SUBSIDIARY(5099, "The current user doesn't have any subsidiaries assigned!", "Error"),

	FLIGHT_EVENT_CAN_NOT_RESCHEDULE(5100, "The flight event is used in a released invoice.", "Error"),
	FUEL_TICKET_NOT_VALID(5101, "Invalid fuel ticket processing prerequisites", "Validation"),

	FUEL_TICKET_WORKFLOW_ENTITY_NOT_EXISTING(5130, "Could not retrieve a fuel ticket for ID %s", "Error"),
	FUEL_TICKET_NOT_FOUND(5131, "The selected fuel ticket does not exist in database. Please refresh your page.", "Error"),
	FUEL_TICKET_WORKFLOW_NO_SYS_PARAM(
			5132,
			"Can not submit for approval, please check the 'Use fuel ticket approval workflow' system parameter.",
			"Error"),
	FUEL_TICKET_WORKFLOW_CONCURRENCY_NOT_START_ERROR(
			5133,
			"Approval process could not be started, the fuel ticket update was modified in the meantime, please refresh and try again.",
			"Error"),
	FUEL_TICKET_WORKFLOW_NOT_START_REASON(5134, "Approval process could not be started. Reason : ", "Error"),

	FT_WKF_PROCESS_BUFFERED_REASON(5135, "Submitted as result of buffered fuel tickets processing", "Message"),
	FUEL_TICKET_RELEASE_NO_SYS_PARAM(5136, "Can not release, please check the 'Use fuel ticket approval workflow' system parameter.", "Error"),
	FUEL_TICKET_WORKFLOW_APPROVED(
			5137,
			"The selected Fuel Ticket has been changed in the meantime and can no longer be approved/rejected. Please reload the data and try again.",
			"Error"),
	FUEL_TICKET_WORKFLOW_IDENTIFICATION_ERROR(5138, "Workflow instance identification error. Check Fuel Ticket history for details.", "Error"),
	NET_QUANTITY_NOT_DEFINED_ERROR(5139, "No Net Quantity has been found for Fuel Ticket", "Error"),
	NO_WORKFLOW_DEFINED(5141, "No %s workflow defined for the following subsidiary: %s", "Error"),
	NOT_SYNCRONIZED_WITH_DATABASE(5142, "The following fuel ticket is not syncronized with database.", "Error"),
	NO_WORKFLOW_PAREMETER_DEFINED(5144, "No %s workflow parameter defined for the workflow %s", "Error"),
	FUEL_TICKET_CANCEL_WORKFLOW_CONCURRENCY_ERROR(
			5145,
			"Cancel process could not be started, a workflow instance is already processing this request.",
			"Error"),
	FUEL_TICKET_CANCEL_WORKFLOW_NOT_START(5146, "Cancel process could not be started for the following fuel tickets. %s", "Error"),
	// ************** Messages *********//

	FT_REJECT_NO_RESALE_EVENT(10001, " | No Resale event. ( No buy & resale contract found )", "Message"),
	FT_REJECT_TOO_MANY_RESALE_EVENTS(10002, " | Too many resale events", "Message"),
	FT_REJECT_TOO_MANY_SALE_CONTRACTS(10003, " | Too many sale contracts", "Message"),
	FT_REJECT_NO_EXT_SALE_CONTRACTS(10004, " | External sale contract not found ", "Message"),
	FT_SUCCESFULL_PROCESSED(10005, "Succesfully processed tickets: ", "Message"),
	FT_FAILED_PROCESSED(10006, "Failed processed tickets: ", "Message"),
	FT_PROCESS_FAILURE(10007, "Process Failure: Ticket: %s, File: %s.", "Message"),
	FT_PROCESS_NT_MSG_TITLE(10008, "Processed buffered fuel tickets", "Message"),
	FT_PROCESS_NT_MSG_DESC(10009, "Processed %s fuel tickets successfully. %s fuel tickets failed.", "Message"),
	FT_IATA_NT_MSG_TITLE(10010, "Import IATA Fuel Tickets", "Message"),
	FT_IATA_NT_MSG_SUCCESS(10011, "Import of fuel transactions was completed successfully.", "Message"),
	FT_IATA_NT_MSG_FAILURE(10012, "Import of fuel transactions failed. Check job result for details.", "Message"),
	FT_REJECT_NO_BUY_CONTRACT_FOUND(10013, " | Buy contract not found", "Message"),
	FT_DIRECT_FUEL_ORDER_NOT_FOUND(10014, "Direct fuel order not found ", "Message"),
	FT_DIRECT_SALE_CONTRACT_NOT_FOUND(10015, " | Direct sale contract not found", "Message"),
	FT_INDIRECT_SALE_CONTRACT_NOT_FOUND(10016, " | Indirect sale contract not found", "Message");

	private static final Logger LOG = LoggerFactory.getLogger(OpsErrorCode.class);

	private ResourceBundle boundle;
	private final int errNo;
	private final String errMsg;
	private final String errGroup;

	private OpsErrorCode(int errNo, String errMsg, String errGroup) {
		this.errNo = errNo;
		this.errMsg = errMsg;
		this.errGroup = errGroup;
	}

	@Override
	public String getErrGroup() {
		return this.errGroup;
	}

	@Override
	public int getErrNo() {
		return this.errNo;
	}

	@Override
	public String getErrMsg() {
		try {
			String language = Session.user.get().getSettings().getLanguage();
			if (language.equals("sys")) {
				this.boundle = ResourceBundle.getBundle("locale.sone.ops.error.messages");
			} else {
				this.boundle = ResourceBundle.getBundle("locale.sone.ops.error.messages", new Locale(language));
			}
		} catch (NullPointerException e) {
			LOG.trace("No session while testing", e);
			this.boundle = ResourceBundle.getBundle("locale.sone.ops.error.messages");
		}
		return this.boundle.containsKey(Integer.toString(this.errNo)) ? this.boundle.getString(Integer.toString(this.errNo)) : this.errMsg;
	}
}
