/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.fuelTransaction.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import atraxo.ops.business.api.fuelTransaction.IFuelTransactionService;
import atraxo.ops.business.api.fuelTransaction.IFuelTransactionSourceService;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link FuelTransactionSource} domain entity.
 */
public class FuelTransactionSource_Service extends atraxo.ops.business.impl.fuelTransaction.FuelTransactionSource_Service
		implements IFuelTransactionSourceService {

	@Autowired
	private IFuelTransactionService ftSrv;

	@Override
	@Transactional
	public void fetch(List<FuelTransactionSource> list) throws BusinessException {
		List<FuelTransactionSource> existingList = this.findAll();
		List<FuelTransactionSource> updList = new ArrayList<>();
		List<FuelTransactionSource> insList = new ArrayList<>();
		Iterator<? extends FuelTransactionSource> iter = list.iterator();
		while (iter.hasNext()) {
			FuelTransactionSource fts = iter.next();
			if (fts == null) {
				iter.remove();
			} else if (existingList.contains(fts)) {
				FuelTransactionSource existingFtSrc = existingList.get(existingList.indexOf(fts));
				fts.setId(existingFtSrc.getId());
				fts.setVersion(existingFtSrc.getVersion());
				fts.setCreatedAt(existingFtSrc.getCreatedAt());
				fts.setCreatedBy(existingFtSrc.getCreatedBy());
				fts.setRefid(existingFtSrc.getRefid());
				List<FuelTransaction> existingFuelTickets = this.ftSrv.findAll();
				for (FuelTransaction ft : fts.getFueltransaction()) {
					ft.setClientId(Session.user.get().getClientId());
					if (existingFuelTickets.contains(ft)) {
						FuelTransaction existingFt = existingFuelTickets.get(existingFuelTickets.indexOf(ft));
						ft.setId(existingFt.getId());
						ft.setVersion(existingFt.getVersion());
						ft.setCreatedAt(existingFt.getCreatedAt());
						ft.setCreatedBy(existingFt.getCreatedBy());
						ft.setRefid(existingFt.getRefid());
					}
				}
				updList.add(fts);
			} else {
				insList.add(fts);
			}
		}

		this.insert(insList);
		this.update(updList);
	}

	@Override
	public List<FuelTransactionSource> findAll() throws BusinessException {
		Map<String, Object> params = Collections.emptyMap();
		return this.findEntitiesByAttributes(params);
	}

}
