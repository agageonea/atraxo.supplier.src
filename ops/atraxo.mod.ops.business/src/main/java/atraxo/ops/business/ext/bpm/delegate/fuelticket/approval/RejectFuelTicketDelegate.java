package atraxo.ops.business.ext.bpm.delegate.fuelticket.approval;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.business.ext.bpm.delegate.AbstractJavaDelegate;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

/**
 * @author abolindu
 */
public class RejectFuelTicketDelegate extends AbstractJavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(RejectFuelTicketDelegate.class);

	@Autowired
	private IFuelTicketService fuelTicketService;

	@Override
	public void execute() throws Exception {
		Object fuelTicketId = this.execution.getVariable(WorkflowVariablesConstants.FUEL_TICKET_ID_VAR);
		if (fuelTicketId != null) {
			FuelTicket fuelTicket = null;
			try {
				fuelTicket = this.fuelTicketService.findById(fuelTicketId);
			} catch (Exception e) {
				LOGGER.error("ERROR:could not find an Fuel Ticket for ID " + fuelTicketId, e);
				this.throwBpmErrorForWorkflow(WorkflowVariablesConstants.ERROR_DEFAULT_CODE_APPROVE,
						"Could not find an Fuel Ticket for ID " + fuelTicketId);
			}

			if (fuelTicket != null) {
				this.fuelTicketService.rejectFuelTicket(Arrays.asList(fuelTicket), this.getNote());
			}
		}
	}

	@Override
	public String getSessionUserCode() {
		return this.getVariableFromWorkflow(WorkflowVariablesConstants.TASK_NOTE_APPROVER_USERNAME);
	}

}
