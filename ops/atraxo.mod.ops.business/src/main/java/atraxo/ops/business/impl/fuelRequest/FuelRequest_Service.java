/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.impl.fuelRequest;

import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.entity.AbstractEntityService;

/**
 * Repository functionality for {@link FuelRequest} domain entity. It contains
 * finder methods based on unique keys as well as reference fields.
 * 
 */
public class FuelRequest_Service extends AbstractEntityService<FuelRequest> {

	/**
	 * Public constructor for FuelRequest_Service
	 */
	public FuelRequest_Service() {
		super();
	}

	/**
	 * Public constructor for FuelRequest_Service
	 * 
	 * @param em
	 */
	public FuelRequest_Service(EntityManager em) {
		super();
		this.setEntityManager(em);
	}

	@Override
	public Class<FuelRequest> getEntityClass() {
		return FuelRequest.class;
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelRequest
	 */
	public FuelRequest findByCode(String requestCode) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelRequest.NQ_FIND_BY_CODE,
							FuelRequest.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("requestCode", requestCode).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelRequest", "requestCode"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelRequest", "requestCode"), nure);
		}
	}
	/**
	 * Find by unique key
	 *
	 * @return FuelRequest
	 */
	public FuelRequest findByBusiness(Integer id) {
		try {
			return this
					.getEntityManager()
					.createNamedQuery(FuelRequest.NQ_FIND_BY_BUSINESS,
							FuelRequest.class)
					.setParameter("clientId",
							Session.user.get().getClient().getId())
					.setParameter("id", id).getSingleResult();
		} catch (NoResultException nre) {
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(),
							"FuelRequest", "id"), nre);
		} catch (NonUniqueResultException nure) {
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(
							J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(),
							"FuelRequest", "id"), nure);
		}
	}

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByCustomer(Customer customer) {
		return this.findByCustomerId(customer.getId());
	}
	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByCustomerId(Integer customerId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelRequest e where e.clientId = :clientId and e.customer.id = :customerId",
						FuelRequest.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("customerId", customerId).getResultList();
	}
	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByContact(Contacts contact) {
		return this.findByContactId(contact.getId());
	}
	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByContactId(Integer contactId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelRequest e where e.clientId = :clientId and e.contact.id = :contactId",
						FuelRequest.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("contactId", contactId).getResultList();
	}
	/**
	 * Find by reference: fuelQuotation
	 *
	 * @param fuelQuotation
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByFuelQuotation(FuelQuotation fuelQuotation) {
		return this.findByFuelQuotationId(fuelQuotation.getId());
	}
	/**
	 * Find by ID of reference: fuelQuotation.id
	 *
	 * @param fuelQuotationId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByFuelQuotationId(Integer fuelQuotationId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelRequest e where e.clientId = :clientId and e.fuelQuotation.id = :fuelQuotationId",
						FuelRequest.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("fuelQuotationId", fuelQuotationId)
				.getResultList();
	}
	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findBySubsidiary(Customer subsidiary) {
		return this.findBySubsidiaryId(subsidiary.getId());
	}
	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findBySubsidiaryId(Integer subsidiaryId) {
		return this
				.getEntityManager()
				.createQuery(
						"select e from FuelRequest e where e.clientId = :clientId and e.subsidiary.id = :subsidiaryId",
						FuelRequest.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("subsidiaryId", subsidiaryId).getResultList();
	}
	/**
	 * Find by reference: flightEvent
	 *
	 * @param flightEvent
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByFlightEvent(FlightEvent flightEvent) {
		return this.findByFlightEventId(flightEvent.getId());
	}
	/**
	 * Find by ID of reference: flightEvent.id
	 *
	 * @param flightEventId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByFlightEventId(Integer flightEventId) {
		return this
				.getEntityManager()
				.createQuery(
						"select distinct e from FuelRequest e, IN (e.flightEvent) c where e.clientId = :clientId and c.id = :flightEventId",
						FuelRequest.class)
				.setParameter("clientId",
						Session.user.get().getClient().getId())
				.setParameter("flightEventId", flightEventId).getResultList();
	}
}
