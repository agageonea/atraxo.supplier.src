/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.fuelTicket.service;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import atraxo.cmm.domain.impl.cmm_type.BillStatus;
import atraxo.fmbas.business.api.customer.ICustomerService;
import atraxo.fmbas.business.api.geo.ILocationsService;
import atraxo.fmbas.business.api.history.IChangeHistoryService;
import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.api.tolerance.IToleranceService;
import atraxo.fmbas.business.api.uom.IUnitService;
import atraxo.fmbas.business.api.user.IUserSuppService;
import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.business.ext.exceptions.DuplicateEntityException;
import atraxo.fmbas.business.ext.exchangerate.delegate.ExchangeRate_Bd;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.business.ext.tolerance.ToleranceResult;
import atraxo.fmbas.business.ext.tolerance.ToleranceVerifier;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.history.ChangeHistory;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.tolerance.Tolerance;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.ops.business.api.custom.fuelTicket.IApproveFuelTicketService;
import atraxo.ops.business.api.ext.fuelTicket.ICancelFuelTicket;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.business.ext.exceptions.TankCapacityExceedException;
import atraxo.ops.business.ext.fuelTicket.delegate.FuelTicketAttachement_Bd;
import atraxo.ops.business.ext.integration.FuelEventSource;
import atraxo.ops.domain.ext.DeliveryNoteMethod.DeliveryNoteMethod;
import atraxo.ops.domain.ext.fuelTicket.ApprovalStatus;
import atraxo.ops.domain.ext.fuelTicket.CancelFuelTicketResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketSource;
import atraxo.ops.domain.impl.ops_type.FuelTicketStatus;
import atraxo.ops.domain.impl.ops_type.FuelTicketTransmissionStatus;
import atraxo.ops.domain.impl.ops_type.OutgoingInvoiceStatus;
import atraxo.ops.domain.impl.ops_type.RevocationReason;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link FuelTicket} domain entity.
 */
public class FuelTicket_Service extends atraxo.ops.business.impl.fuelTicket.FuelTicket_Service implements IFuelTicketService {

	private static final String UPDATED = "Updated";

	private static final Logger LOG = LoggerFactory.getLogger(FuelTicket_Service.class);

	@Autowired
	private IApproveFuelTicketService approveSrv;
	@Autowired
	private ICustomerService customerService;
	@Autowired
	private IChangeHistoryService historyService;
	@Autowired
	private ISystemParameterService parameterService;
	@Autowired
	private IUnitService unitService;
	@Autowired
	private IToleranceService toleranceService;
	@Autowired
	private ILocationsService locationService;
	@Autowired
	private IUserSuppService userSupplierService;
	@Autowired
	private UnitConverterService unitConverterService;
	@Autowired
	private ICancelFuelTicket cancelFuelTicketService;

	private static final String FUEL_EVENT_EXPORT_NOTIF_CHANNEL = "exportNotifyFuelEvent";

	@Override
	protected void preInsert(FuelTicket e) throws BusinessException {
		this.checkBusinessKey(e);
		this.checkTankCapacityTolerance(e);
	}

	/**
	 * @param e
	 * @param method
	 */
	private void sendMessage(FuelTicket e, DeliveryNoteMethod method) {
		FuelEventSource feSrc = new FuelEventSource(e, method);
		this.sendMessage("maintainFuelEvents", feSrc);
	}

	@Override
	protected void preUpdate(FuelTicket e) throws BusinessException {
		this.checkBusinessKey(e);
		this.checkTankCapacityTolerance(e);
		this.setStatus(e);
	}

	@Override
	protected void postDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.postDeleteByIds(ids, context);
		this.historyService.deleteHistory(ids, FuelTicket.class);
	}

	/**
	 * When I save my changes, in case the ticket was already transmitted, the ticket status is changed to Updated Transmission status is changed to:
	 * New Approval state is changed to: Not checked On save I have to indicate the reason for the change and this is saved in ticket history
	 *
	 * @param ticket
	 * @throws BusinessException
	 */
	private void setStatus(FuelTicket ticket) throws BusinessException {
		if (FuelTicketTransmissionStatus._TRANSMITTED_.equals(ticket.getTransmissionStatus())) {
			ticket.setTicketStatus(FuelTicketStatus._UPDATED_);
			ticket.setTransmissionStatus(FuelTicketTransmissionStatus._NEW_);
			ticket.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
			this.insertToHistory(ticket, UPDATED, "");
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void insertToHistory(FuelTicket e, String objectValue, String remarks) throws BusinessException {
		ChangeHistory changeHistory = new ChangeHistory();
		changeHistory.setObjectId(e.getId());
		changeHistory.setObjectType(e.getClass().getSimpleName());
		changeHistory.setObjectValue(objectValue);
		changeHistory.setRemarks(remarks);
		this.historyService.insert(changeHistory);
	}

	private void checkBusinessKey(FuelTicket e) throws DuplicateEntityException {
		Map<String, Object> params = new HashMap<>();
		params.put("departure", e.getDeparture());
		params.put("ticketNo", e.getTicketNo());
		params.put("customer", e.getCustomer());
		params.put("deliveryDate", e.getDeliveryDate());
		List<FuelTicket> ftList = this.findEntitiesByAttributes(params);
		if ((e.getId() != null && ftList.size() > 1) || (e.getId() == null && !ftList.isEmpty())) {
			throw new DuplicateEntityException(FuelTicket.class, "Ticket # - Customer - Delivery date - Departure");
		}
	}

	private void checkTankCapacityTolerance(FuelTicket e) throws BusinessException {
		if (e.getAcType() == null) {
			return;
		}
		Unit sysUnit = this.unitService.findByCode(this.parameterService.getSysVol());
		ToleranceVerifier verifier = new ToleranceVerifier(this.unitConverterService, this.getBusinessDelegate(ExchangeRate_Bd.class),
				this.getDensity(e));
		Tolerance tolerance = this.toleranceService.getTankCapacityTolerance();
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		ToleranceResult toleranceResult = verifier.checkHigherToleranceLimit(BigDecimal.valueOf(e.getAcType().getTankCapacity()), e.getUpliftVolume(),
				e.getAcType().getUnit() != null ? e.getAcType().getUnit() : sysUnit, e.getUpliftUnit(), null, null, tolerance, date);
		switch (toleranceResult.getType()) {
		case NOT_WITHIN:
			throw new TankCapacityExceedException();
		case WITHIN:
		case MARGIN:
		default:
			break;
		}
	}

	private Density getDensity(FuelTicket ft) throws BusinessException {
		Density density = this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity();
		if (ft.getDensity() != null) {
			density = new Density(ft.getDensityUnit(), ft.getDensityVolume(), ft.getDensity().doubleValue());
		}
		return density;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void releaseFuelTicket(List<FuelTicket> fuelTickets) throws BusinessException {
		for (FuelTicket fuelTicket : fuelTickets) {
			FuelTicket ticket = this.findByBusiness(fuelTicket.getId());
			if (ticket.getTicketStatus().equals(FuelTicketStatus._CANCELED_)
					&& ticket.getTransmissionStatus().equals(FuelTicketTransmissionStatus._NEW_)) {
				continue;
			}
			this.approveSrv.releaseFuelTicket(ticket);
		}
	}

	@Override
	public void exportFuelEvents(List<FuelTicket> fuelTickets) {
		this.sendMessage(FUEL_EVENT_EXPORT_NOTIF_CHANNEL, fuelTickets);
	}

	@Override
	@Transactional
	@History(value = "Rejected", type = FuelTicket.class)
	public void rejectFuelTicket(List<FuelTicket> fuelTickets, @Reason String reason) throws BusinessException {
		for (FuelTicket fuelTicket : fuelTickets) {
			fuelTicket.setApprovalStatus(FuelTicketApprovalStatus._REJECTED_);
		}
		this.update(fuelTickets);
	}

	@Override
	@Transactional
	public List<FuelTicket> getFuelTicketsForTransmission() throws BusinessException {
		Map<String, Object> params = new HashMap<>();
		params.put("transmissionStatus", FuelTicketTransmissionStatus._SUBMITTED_);
		List<FuelTicket> rs = this.findEntitiesByAttributes(params);
		for (FuelTicket ft : rs) {
			ft.setTransmissionStatus(FuelTicketTransmissionStatus._EXPORTED_);
			this.onUpdate(ft);
		}
		return !rs.isEmpty() ? rs : null;
	}

	@Override
	@History(value = "Cancel", type = FuelTicket.class)
	@Transactional(rollbackFor = BusinessException.class)
	public CancelFuelTicketResult cancelFuelTicket(List<FuelTicket> fuelTickets, @Reason String reason) throws BusinessException {
		for (FuelTicket fuelTicket : fuelTickets) {
			fuelTicket.setTicketStatus(FuelTicketStatus._CANCELED_);
			fuelTicket.setTransmissionStatus(FuelTicketTransmissionStatus._NEW_);
			fuelTicket.setRevocationRason(RevocationReason.getByName(reason));
			this.onUpdate(fuelTicket);
		}

		return this.cancelFuelTicketService.cancelFuelTickets(fuelTickets);
	}

	@Override
	@Transactional
	@History(value = "Submit", type = FuelTicket.class)
	public void submitFuelTicket(List<FuelTicket> fuelTickets) throws BusinessException {
		for (FuelTicket fuelTicket : fuelTickets) {
			fuelTicket.setTransmissionStatus(FuelTicketTransmissionStatus._SUBMITTED_);
		}
		this.update(fuelTickets);
	}

	@Override
	@Transactional
	@History(value = "Reset", type = FuelTicket.class)
	public void resetFuelTicket(List<FuelTicket> fuelTickets, @Reason String reason) throws BusinessException {
		List<Object> ids = new ArrayList<>();
		for (FuelTicket fuelTicket : fuelTickets) {
			fuelTicket.setTicketStatus(FuelTicketStatus._ORIGINAL_);
			fuelTicket.setTransmissionStatus(FuelTicketTransmissionStatus._NEW_);
			fuelTicket.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
			ids.add(fuelTicket.getId());
		}
		this.update(fuelTickets);
		this.sendMessage(fuelTickets, DeliveryNoteMethod.DELETE);
	}

	@Override
	public Locations getUserLocation(String loginName) throws BusinessException {
		try {
			UserSupp user = this.userSupplierService.findByLogin(loginName);
			return this.locationService.findByCodeAndClientId(user.getLocation().getCode(), user.getClientId());
		} catch (ApplicationException | NullPointerException be) {
			LOG.warn("Location can not be found for user:" + loginName, be);
			return null;
		}
	}

	@Override
	public Customer verifyCustomer(String customer) throws BusinessException {
		try {
			return this.customerService.findByCode(customer);
		} catch (ApplicationException be) {
			LOG.warn("Customer can not be found:" + customer, be);
			return null;
		}
	}

	@Override
	@Transactional
	public void updateWithoutBL(List<FuelTicket> fuelTickets) throws BusinessException {
		for (FuelTicket fuelTicket : fuelTickets) {
			this.onUpdate(fuelTicket);
		}
	}

	@Override
	@Transactional
	public List<FuelTicket> synchronize(List<FuelTicket> fuelTickets) throws BusinessException {
		List<FuelTicket> retList = new ArrayList<>();
		for (FuelTicket ft : fuelTickets) {
			this.verifyAndSetUnits(ft);
			ft.setCapturedBy(Session.user.get().getName());
			try {
				FuelTicket ftEntity = this.getByBusinessKey(ft);
				this.synchronize(ft, ftEntity);
				if (ftEntity.getSource().equals(FuelTicketSource._MANUAL_)) {
					ftEntity.setSource(FuelTicketSource._IMPORT_);
				}
				this.update(ftEntity);
				retList.add(ftEntity);
			} catch (ApplicationException nre) {
				if (J4eErrorCode.DB_DUPLICATE_KEY_FOUND.equals(nre.getErrorCode())) {
					throw nre;
				}
				LOG.warn("Fuel ticket can not be found", nre);
				ft.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
				ft.setTransmissionStatus(FuelTicketTransmissionStatus._NEW_);
				ft.setTicketStatus(FuelTicketStatus._ORIGINAL_);
				if (ft.getSource().equals(FuelTicketSource._MANUAL_)) {
					ft.setSource(FuelTicketSource._IMPORT_);
				}
				this.insert(ft);
				retList.add(ft);
			}
		}
		return retList;
	}

	private void synchronize(FuelTicket ft, FuelTicket ftEntity) {
		FuelTicketStatus action = ft.getTicketStatus();
		switch (action) {
		case _ORIGINAL_:
		case _UPDATED_:
			ft.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
			ft.setTransmissionStatus(FuelTicketTransmissionStatus._NEW_);
			ft.setTicketStatus(FuelTicketStatus._UPDATED_);

			if (FuelTicketStatus._ORIGINAL_.equals(ftEntity.getTicketStatus())) {
				ft.setTicketStatus(FuelTicketStatus._ORIGINAL_);
			}
			if (FuelTicketTransmissionStatus._TRANSMITTED_.equals(ftEntity.getTransmissionStatus())
					&& FuelTicketApprovalStatus._OK_.equals(ft.getApprovalStatus())) {
				if (FuelTicketStatus._ORIGINAL_.equals(ft.getTicketStatus())) {
					ft.setTicketStatus(FuelTicketStatus._UPDATED_);
				} else if (FuelTicketStatus._UPDATED_.equals(ft.getTicketStatus())) {
					ft.setTicketStatus(FuelTicketStatus._ORIGINAL_);
				}
			}
			break;
		case _CANCELED_:
			this.synchronizeStatuses(ft, ftEntity);
			break;
		default:
			break;
		}
		this.copyFields(ft, ftEntity);
	}

	private void synchronizeStatuses(FuelTicket ft, FuelTicket ftEntity) {
		FuelTicketStatus status = ftEntity.getTicketStatus();
		switch (status) {
		case _ORIGINAL_:
			ft.setApprovalStatus(FuelTicketApprovalStatus._OK_);
			ft.setTransmissionStatus(FuelTicketTransmissionStatus._TRANSMITTED_);
			ft.setTicketStatus(FuelTicketStatus._CANCELED_);
			if (FuelTicketApprovalStatus._OK_.equals(ftEntity.getApprovalStatus())
					&& FuelTicketTransmissionStatus._TRANSMITTED_.equals(ftEntity.getTransmissionStatus())) {
				ft.setTransmissionStatus(FuelTicketTransmissionStatus._NEW_);
				ft.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
			}
			break;
		case _UPDATED_:
			ft.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
			ft.setTransmissionStatus(FuelTicketTransmissionStatus._NEW_);
			ft.setTicketStatus(FuelTicketStatus._CANCELED_);
			break;
		default:
			break;
		}
	}

	private void verifyAndSetUnits(FuelTicket ft) throws BusinessException {
		Unit defaultSystemVolUnit = this.unitService.findByCode(this.parameterService.getSysVol());
		Unit defaultSystemMassUnit = this.unitService.findByCode(this.parameterService.getSysWeight());
		if (ft.getNetUpliftUnit() == null) {
			ft.setNetUpliftUnit(defaultSystemVolUnit);
		}
		if (ft.getBeforeFuelingUnit() == null) {
			ft.setBeforeFuelingUnit(defaultSystemVolUnit);
		}
		if (ft.getAfterFuelingUnit() == null) {
			ft.setAfterFuelingUnit(defaultSystemVolUnit);
		}
		if (ft.getDensityUnit() == null) {
			ft.setDensityUnit(defaultSystemMassUnit);
		}
		if (ft.getDensityVolume() == null) {
			ft.setDensityVolume(defaultSystemVolUnit);
		}
	}

	/**
	 * Copy values from the source to the destination entity.
	 *
	 * @param ftSource - source entity.
	 * @param frDestination - destination entity.
	 * @throws BusinessException
	 */
	private void copyFields(FuelTicket ftSource, FuelTicket frDestination) {
		Method[] supMethods = frDestination.getClass().getSuperclass().getMethods();
		for (Method method : frDestination.getClass().getMethods()) {
			method.setAccessible(true);
			String setMethodName = method.getName();
			if (!this.isMethodInList(supMethods, method) && setMethodName.length() >= 4 && "set".equalsIgnoreCase(setMethodName.substring(0, 3))) {
				String paramName = setMethodName.substring(3);
				String getMethodName = "get" + paramName;
				try {
					Method targetMethod = ftSource.getClass().getMethod(getMethodName);
					targetMethod.setAccessible(true);
					Object obj = targetMethod.invoke(ftSource);
					method.invoke(frDestination, obj);
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					// should not happen
					LOG.warn("Copy fuel ticket exeption", e);
				}
			}
		}
	}

	private boolean isMethodInList(Method[] methods, Method method) {
		for (Method m : methods) {
			m.setAccessible(true);
			if (m.getName().equalsIgnoreCase(method.getName())) {
				return true;
			}
		}
		return false;
	}

	private FuelTicket getByBusinessKey(FuelTicket fuelTicket) {
		Map<String, Object> params = new HashMap<>();
		params.put("departure", fuelTicket.getDeparture());
		params.put("ticketNo", fuelTicket.getTicketNo());
		params.put("customer", fuelTicket.getCustomer());
		params.put("deliveryDate", fuelTicket.getDeliveryDate());
		return this.findByUk(FuelTicket.NQ_FIND_BY_BUSINESSKEY, params);
	}

	@Override
	public List<FuelTicket> findByDateCustomerSupplierLocationFlightType(Date validFrom, Date validTo, Suppliers supplier, Locations location,
			String type) throws BusinessException {

		String hql = "select e from " + FuelTicket.class.getSimpleName()
				+ " e where e.clientId=:clientId and e.approvalStatus =:approvalStatus and e.supplier=:supplier and e.departure=:location and e.indicator =:type and e.deliveryDate BETWEEN :validFrom and :validTo";
		return this.getEntityManager().createQuery(hql, FuelTicket.class).setParameter("clientId", supplier.getClientId())
				.setParameter("supplier", supplier).setParameter("location", location).setParameter("type", type).setParameter("validFrom", validFrom)
				.setParameter("validTo", validTo).setParameter("approvalStatus", ApprovalStatus.OK.getName()).getResultList();
	}

	@Override
	public void saveFuelTicketHardCopy(FuelTicket fuelTicket, String ticketHardCopy) throws BusinessException {
		FuelTicketAttachement_Bd fthcBd = this.getBusinessDelegate(FuelTicketAttachement_Bd.class);
		try {
			fthcBd.saveAttachment(fuelTicket, ticketHardCopy);
		} catch (UnsupportedEncodingException e) {
			LOG.warn("Save fuel ticket hard copy exeption.", e);
		}
	}

	@Override
	@Transactional
	public void setFlagDelete(List<FuelTicket> fuelTickets, Boolean isDeleted) throws BusinessException {
		for (FuelTicket ft : fuelTickets) {
			if (!BillStatus._NOT_BILLED_.equals(ft.getBillStatus()) || !OutgoingInvoiceStatus._NOT_INVOICED_.equals(ft.getInvoiceStatus())) {
				throw new BusinessException(OpsErrorCode.FUEL_TICKET_ALREADY_INVOICED, OpsErrorCode.FUEL_TICKET_ALREADY_INVOICED.getErrMsg());
			}
			if (FuelTicketApprovalStatus._OK_.equals(ft.getApprovalStatus()) && !FuelTicketStatus._CANCELED_.equals(ft.getTicketStatus())) {
				throw new BusinessException(OpsErrorCode.FUEL_TICKET_ORIGINAL_APPROVED, OpsErrorCode.FUEL_TICKET_ORIGINAL_APPROVED.getErrMsg());
			}
			ft.setIsDeleted(isDeleted);
			ft.setReferenceTicket(null);
			if (!isDeleted) {
				ft.setApprovalStatus(FuelTicketApprovalStatus._FOR_APPROVAL_);
			}
		}
		this.update(fuelTickets);
		this.sendMessage(fuelTickets, DeliveryNoteMethod.DELETE);
		this.writeInHistoryDelete(fuelTickets, isDeleted);
	}

	private void writeInHistoryDelete(List<FuelTicket> fuelTickets, Boolean isDeleted) throws BusinessException {
		for (FuelTicket fuelTicket : fuelTickets) {
			this.historyService.saveChangeHistory(fuelTicket, isDeleted ? "Delete" : "Undo delete", null);
		}
	}

	private void sendMessage(List<FuelTicket> fuelTickets, DeliveryNoteMethod method) {
		for (FuelTicket fuelTicket : fuelTickets) {
			this.sendMessage(fuelTicket, method);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Boolean getFuelTicketApprovalStatusUpdated(Object id) throws BusinessException {
		FuelTicket fuelTicket = this.findById(id);
		return fuelTicket.getApprovalStatus().equals(FuelTicketApprovalStatus._OK_);
	}

	@Override
	@Transactional
	@History(type = FuelTicket.class)
	public void setFuelTicketApprovalStatus(FuelTicket e, @Reason String reason, @Action String action, FuelTicketApprovalStatus status)
			throws BusinessException {
		e.setApprovalStatus(status);
		this.onUpdate(e);
	}

}
