package atraxo.ops.business.ext.fuelTicket.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.workflow.IWorkflowParameterService;
import atraxo.fmbas.business.api.workflow.IWorkflowService;
import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.impl.workflow.Workflow;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.domain.ext.fuelTicketValidator.FuelTicketWorkflowStarterValidatorResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

/**
 * Validator class that has the role of validating a Fuel Ticket prior to starting the workflow for approval.
 *
 * @author vhojda
 */
public class FuelTicketWorkflowStarterValidator {

	private static final Logger LOGGER = LoggerFactory.getLogger(FuelTicketWorkflowStarterValidator.class);

	@Autowired
	private IWorkflowService workflowService;
	@Autowired
	private IWorkflowParameterService workflowParameterService;

	/**
	 * @param fuelTicket
	 * @param workflowName
	 */
	public FuelTicketWorkflowStarterValidatorResult validate(FuelTicket fuelTicket, WorkflowNames workflowName) {
		// reset validation

		FuelTicketWorkflowStarterValidatorResult result = new FuelTicketWorkflowStarterValidatorResult();

		// get the required workflow (validate against the existence of the workflow itself)
		Workflow wkf = null;
		try {
			wkf = this.workflowService.findByNameAndSubsidiary(workflowName.getWorkflowName(), fuelTicket.getSubsidiaryCode());
		} catch (Exception e) {
			LOGGER.warn("WARNING! No workflow named " + workflowName.getWorkflowName() + " was found for the subsidiary "
					+ fuelTicket.getSubsidiaryCode() + "/n" + e.getMessage(), e);
			result.setValidationMessage(
					String.format(OpsErrorCode.NO_WORKFLOW_DEFINED.getErrMsg(), workflowName.getWorkflowName(), fuelTicket.getSubsidiaryCode()));
		}

		// get the net quantity parameter for the workflow (if it exists)
		if (wkf != null) {
			WorkflowParameter netQuantityParam = null;
			try {
				netQuantityParam = this.workflowParameterService.findByKey(wkf, WorkflowVariablesConstants.CHECK_NET_QUANTITY);
			} catch (Exception e) {
				LOGGER.warn("WARNING! No workflow parameter named " + WorkflowVariablesConstants.CHECK_NET_QUANTITY + " was found for the workflow "
						+ workflowName.getWorkflowName() + "/n" + e.getMessage(), e);
				result.setValidationMessage(String.format(OpsErrorCode.NO_WORKFLOW_PAREMETER_DEFINED.getErrMsg(),
						WorkflowVariablesConstants.CHECK_NET_QUANTITY, workflowName.getWorkflowName()));
			}

			// check net quantity
			if (netQuantityParam != null) {
				Boolean netQuantityParamValue = Boolean.parseBoolean(netQuantityParam.getParamValue());
				// checks the conditions for netQuantity
				if (netQuantityParamValue) {
					if (fuelTicket.getNetUpliftQuantity() == null) {
						result.setValidationMessage(OpsErrorCode.NET_QUANTITY_NOT_DEFINED_ERROR.getErrMsg());
					}
				}
			}
		}

		// set valid value
		result.setValid(StringUtils.isEmpty(result.getValidationMessage()));
		return result;
	}

}