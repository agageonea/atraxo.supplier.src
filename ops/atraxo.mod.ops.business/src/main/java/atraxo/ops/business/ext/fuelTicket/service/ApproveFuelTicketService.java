package atraxo.ops.business.ext.fuelTicket.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.api.contracts.IContractService;
import atraxo.cmm.domain.impl.cmm_type.ContractType;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.shipTo.ShipTo;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.ops.business.api.custom.fuelTicket.IApproveFuelTicketService;
import atraxo.ops.business.api.fuelOrderLocation.IFuelOrderLocationService;
import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.business.ext.integration.FuelEventSource;
import atraxo.ops.domain.ext.DeliveryNoteMethod.DeliveryNoteMethod;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * @author zspeter
 */
public class ApproveFuelTicketService extends AbstractBusinessBaseService implements IApproveFuelTicketService {

	private static final String MAINTAIN_FUEL_EVENTS = "maintainFuelEvents";

	private static final String RELEASED = "Released";

	@Autowired
	private IFuelTicketService fuelTicketSrv;

	@Autowired
	private IContractService contractSrv;

	@Autowired
	private IFuelOrderLocationService orderLocSrv;

	/**
	 * @param e
	 * @throws BusinessException
	 */
	@Override
	public void releaseFuelTicket(FuelTicket e) throws BusinessException {
		StringBuilder historyMessage = new StringBuilder();
		List<FuelOrderLocation> orderLocations = this.orderLocSrv.findDirectFuelOrder(e.getSupplier(), e.getCustomer(), e.getIndicator(),
				e.getDeparture(), e.getDeliveryDate());
		if (orderLocations.size() == 1) {
			FuelEventSource feSrc = new FuelEventSource(e, orderLocations.get(0), DeliveryNoteMethod.GENERATE);
			this.sendMessage(MAINTAIN_FUEL_EVENTS, feSrc);
			e.setApprovedBy(Session.user.get().getName());
			this.fuelTicketSrv.setFuelTicketApprovalStatus(e, "", RELEASED, FuelTicketApprovalStatus._OK_);
			// generate sale
		} else {
			historyMessage.append(OpsErrorCode.FT_DIRECT_FUEL_ORDER_NOT_FOUND.getErrMsg());
			List<Contract> saleContracts = this.contractSrv.findSaleContracts(e.getSupplier().getId(), e.getCustomer(), e.getIndicator(),
					ContractType._PRODUCT_, e.getDeparture(), e.getDeliveryDate(), e.getProductType());
			List<Contract> directSaleContracts = this.getDirectSaleContract(saleContracts, e.getCustomer());
			if (directSaleContracts.size() > 1) {
				historyMessage.append(OpsErrorCode.FT_REJECT_TOO_MANY_SALE_CONTRACTS.getErrMsg());

				this.fuelTicketSrv.setFuelTicketApprovalStatus(e,
						historyMessage.append(OpsErrorCode.FT_REJECT_TOO_MANY_SALE_CONTRACTS.getErrMsg()).toString(),
						FuelTicketApprovalStatus._RECHECK_.getName(), FuelTicketApprovalStatus._RECHECK_);

			} else if (directSaleContracts.size() == 1) {
				FuelEventSource feSrc = new FuelEventSource(e, directSaleContracts, DeliveryNoteMethod.GENERATE);
				this.sendMessage(MAINTAIN_FUEL_EVENTS, feSrc);
				e.setApprovedBy(Session.user.get().getName());

				this.fuelTicketSrv.setFuelTicketApprovalStatus(e, "", RELEASED, FuelTicketApprovalStatus._OK_);
				// generate sale
			} else {
				historyMessage.append(OpsErrorCode.FT_DIRECT_SALE_CONTRACT_NOT_FOUND.getErrMsg()).toString();
				List<Contract> inDirectSaleContracts = this.getIndirectSaleContract(saleContracts, e.getCustomer());
				if (inDirectSaleContracts.isEmpty()) {
					historyMessage.append(OpsErrorCode.FT_INDIRECT_SALE_CONTRACT_NOT_FOUND.getErrMsg()).toString();
					Map<Contract, Contract> reSaleContracts = this.findResaleContracts(e);
					this.manageResaleEvent(e, reSaleContracts, historyMessage);
				} else if (inDirectSaleContracts.size() == 1) {
					this.manageIndirectSaleContract(e, inDirectSaleContracts, historyMessage);
				} else {
					// more than one indirect resale contract
					this.fuelTicketSrv.setFuelTicketApprovalStatus(e,
							historyMessage.append(OpsErrorCode.FT_REJECT_TOO_MANY_SALE_CONTRACTS.getErrMsg()).toString(),
							FuelTicketApprovalStatus._RECHECK_.getName(), FuelTicketApprovalStatus._RECHECK_);
				}
			}
		}
	}

	private void manageIndirectSaleContract(FuelTicket e, List<Contract> inDirectSaleContracts, StringBuilder errorMessage) throws BusinessException {
		Contract saleContract = inDirectSaleContracts.get(0);
		if (this.isInternalSale(saleContract)) {
			Map<Contract, Contract> reSaleContracts = this.findResaleContracts(e);
			this.manageResaleEvent(e, reSaleContracts, errorMessage);
			// generate resale
		} else {
			FuelEventSource feSrc = new FuelEventSource(e, Arrays.asList(saleContract), DeliveryNoteMethod.GENERATE);
			this.sendMessage(MAINTAIN_FUEL_EVENTS, feSrc);
			e.setApprovalStatus(FuelTicketApprovalStatus._OK_);
			e.setApprovedBy(Session.user.get().getName());
			// generate sale
		}
	}

	private void manageResaleEvent(FuelTicket e, Map<Contract, Contract> reSaleContracts, StringBuilder errorMessage) throws BusinessException {
		if (reSaleContracts.isEmpty()) {
			this.fuelTicketSrv.setFuelTicketApprovalStatus(e, errorMessage.append(OpsErrorCode.FT_REJECT_NO_RESALE_EVENT.getErrMsg()).toString(),
					FuelTicketApprovalStatus._RECHECK_.getName(), FuelTicketApprovalStatus._RECHECK_);
			// works only for internal resale
		} else if (reSaleContracts.size() == 2) {
			List<Contract> saleContractList = new ArrayList<>(reSaleContracts.keySet());
			Contract buyContract = reSaleContracts.get(saleContractList.get(0));
			FuelEventSource feSrc = new FuelEventSource(e, saleContractList, buyContract, DeliveryNoteMethod.GENERATE);
			this.sendMessage(MAINTAIN_FUEL_EVENTS, feSrc);
			e.setApprovalStatus(FuelTicketApprovalStatus._OK_);
			e.setApprovedBy(Session.user.get().getName());

			// write in history
			this.fuelTicketSrv.setFuelTicketApprovalStatus(e, "", RELEASED, FuelTicketApprovalStatus._OK_);

			// generate resale event
		} else {
			this.fuelTicketSrv.setFuelTicketApprovalStatus(e,
					errorMessage.append(OpsErrorCode.FT_REJECT_TOO_MANY_RESALE_EVENTS.getErrMsg()).toString(),
					FuelTicketApprovalStatus._RECHECK_.getName(), FuelTicketApprovalStatus._RECHECK_);
		}
	}

	private Map<Contract, Contract> findResaleContracts(FuelTicket e) throws BusinessException {
		List<Contract> purchaseContracts = this.contractSrv.findPurchaseContracts(e.getSupplier(), e.getIndicator(), e.getDeparture(),
				e.getDeliveryDate(), e.getProductType());
		List<Contract> locSaleContracts = this.contractSrv.findSaleContracts(e.getIndicator(), ContractType._PRODUCT_, e.getDeparture(),
				e.getDeliveryDate(), e.getProductType());
		Collections.sort(locSaleContracts, new Comparator<Contract>() {
			@Override
			public int compare(Contract o1, Contract o2) {
				return o1.getHolder().getId().compareTo(o2.getHolder().getId());
			}
		});
		Map<Contract, Contract> reSaleContracts = new HashMap<>();
		boolean hasFinalSale = false;
		for (Contract purchaseContract : purchaseContracts) {
			for (Contract saleContract : locSaleContracts) {
				if (purchaseContract.equals(saleContract.getResaleRef())
						&& (saleContract.getCustomer().equals(e.getCustomer()) || this.isInShipTo(e.getCustomer(), saleContract))) {
					reSaleContracts.put(saleContract, purchaseContract);
					hasFinalSale = true;
				}
			}
		}
		if (!hasFinalSale) {
			reSaleContracts = Collections.emptyMap();
		}
		return reSaleContracts;
	}

	private boolean isInShipTo(Customer e, Contract saleContract) {
		if (saleContract.getShipTo() != null) {
			for (Iterator<ShipTo> iter = saleContract.getShipTo().iterator(); iter.hasNext();) {
				ShipTo shipTo = iter.next();
				if (shipTo.getCustomer().equals(e)) {
					return true;
				}
			}
		}
		return false;
	}

	private List<Contract> getIndirectSaleContract(List<Contract> list, Customer customer) {
		List<Contract> inDirectList = new ArrayList<>();
		for (Contract c : list) {
			for (ShipTo shipTo : c.getShipTo()) {
				if (customer.equals(shipTo.getCustomer())) {
					inDirectList.add(c);
				}
			}
		}
		return inDirectList;
	}

	private List<Contract> getDirectSaleContract(List<Contract> list, Customer customer) {
		List<Contract> directSaleContracts = new ArrayList<>();
		for (Contract c : list) {
			if (c.getCustomer().equals(customer)) {
				directSaleContracts.add(c);
			}
		}
		return directSaleContracts;
	}

	private boolean isInternalSale(Contract saleContract) {
		return saleContract.getCustomer().getIsSubsidiary();
	}
}
