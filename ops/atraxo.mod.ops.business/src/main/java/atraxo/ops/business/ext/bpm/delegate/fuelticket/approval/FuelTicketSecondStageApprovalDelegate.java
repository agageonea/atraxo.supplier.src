package atraxo.ops.business.ext.bpm.delegate.fuelticket.approval;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import atraxo.fmbas.business.ext.bpm.WorkflowVariablesConstants;
import atraxo.fmbas.domain.ext.mailmerge.dto.EmailDto;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.fmbas.domain.impl.workflow.WorkflowParameter;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

/**
 * @author abolindu
 */
public class FuelTicketSecondStageApprovalDelegate extends FuelTicketStageApprovalDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(FuelTicketSecondStageApprovalDelegate.class);

	@Override
	public void execute() throws Exception {
		WorkflowParameter param = this.workflowParameterService.findByKey(Long.valueOf(this.workflowId), WorkflowVariablesConstants.APPROVER_ROLE);
		if (!StringUtils.isEmpty(param.getParamValue())) {
			List<UserSupp> users = this.extractApprovers(WorkflowVariablesConstants.APPROVER_ROLE);
			if (users != null) {
				FuelTicket fuelTicket = this.extractFuelTicket();
				if (fuelTicket != null) {
					EmailDto emailDto = this.extractEmailDto(fuelTicket, users, false);
					if (emailDto != null) {
						this.execution.setVariable(WorkflowVariablesConstants.VAR_WKF_EMAIL_DTO, emailDto);
					}
				}
			}
			// make sure to reset the mail components
			this.resetMailComponentVariables();
			this.setSecondApprovalStageRequiredVariable(true);
		} else {
			this.setSecondApprovalStageRequiredVariable(false);
		}

	}

}
