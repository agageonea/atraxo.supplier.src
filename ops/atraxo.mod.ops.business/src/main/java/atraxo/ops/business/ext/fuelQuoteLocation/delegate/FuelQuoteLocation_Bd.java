package atraxo.ops.business.ext.fuelQuoteLocation.delegate;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atraxo.fmbas.business.api.sys.ISystemParameterService;
import atraxo.fmbas.business.ext.uom.service.UnitConverterService;
import atraxo.fmbas.domain.impl.sys.SystemParameter;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.business.api.flightEvent.IFlightEventService;
import atraxo.ops.business.api.fuelQuoteLocation.IFuelQuoteLocationService;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessDelegate;

public class FuelQuoteLocation_Bd extends AbstractBusinessDelegate {

	/**
	 * Calculates quantity for fuel quote location from flight types.
	 *
	 * @param e Fuel quote location.
	 * @param qty Initial quantity.
	 * @param flighEventIds Flight type ids that will be ignored from calculation.
	 * @param unit
	 * @throws BusinessException
	 */
	private void calculateQuantity(FuelQuoteLocation e, BigDecimal qty, Unit unit, Integer... flighEventIds) throws BusinessException {
		UnitConverterService unitConverterService = this.getApplicationContext().getBean(UnitConverterService.class);
		double density = this.getSysDensity();
		e.setQuantity(unitConverterService.convert(unit, e.getUnit(), qty, density));
		List<Integer> ids = Arrays.asList(flighEventIds);
		for (Iterator<FlightEvent> iter = e.getFlightEvents().iterator(); iter.hasNext();) {
			FlightEvent flightEvent = iter.next();
			if (!ids.contains(flightEvent.getId())) {
				BigDecimal fuelQty = flightEvent.getTotalQuantity() != null ? flightEvent.getTotalQuantity() : BigDecimal.ZERO;
				BigDecimal convertedQuantity = unitConverterService.convert(flightEvent.getUnit(), e.getUnit(), fuelQty, density);
				e.setQuantity(e.getQuantity().add(convertedQuantity));
			}
		}
	}

	private void updateFuelingDates(FuelQuoteLocation fql, FlightEvent flightEvent) throws BusinessException {
		if (fql.getFuelReleaseEndsOn() == null) {
			fql.setFuelReleaseEndsOn(flightEvent.getUpliftDate());
		} else if (flightEvent.getUpliftDate().after(fql.getFuelReleaseEndsOn())) {
			fql.setFuelReleaseEndsOn(flightEvent.getUpliftDate());
		}
		if (fql.getFuelReleaseStartsOn() == null) {
			fql.setFuelReleaseStartsOn(flightEvent.getUpliftDate());
		} else if (flightEvent.getUpliftDate().before(fql.getFuelReleaseStartsOn())) {
			fql.setFuelReleaseStartsOn(flightEvent.getUpliftDate());
		}

	}

	public void updateOnFlightEventChange(FuelQuoteLocation fql, FlightEvent flightEvent) throws BusinessException {
		BigDecimal qty = flightEvent.getTotalQuantity() != null ? flightEvent.getTotalQuantity() : BigDecimal.ZERO;
		this.calculateQuantity(fql, qty, flightEvent.getUnit(), flightEvent.getId());
		this.updateFuelingDates(fql, flightEvent);
		this.calculateEvents(fql, flightEvent);
		this.mergeEntity(fql);

	}

	public void upadteOnFlightEventRemove(Integer... flightEventIds) throws BusinessException {
		IFlightEventService service = (IFlightEventService) this.findEntityService(FlightEvent.class);
		Map<Integer, FuelQuoteLocation> fqlMap = new HashMap<>();
		for (Integer id : flightEventIds) {
			FlightEvent flightEvent = service.findById(id);
			FuelQuoteLocation fql = flightEvent.getLocQuote();
			if (fqlMap.containsKey(fql.getId())) {
				fql = fqlMap.get(fql.getId());
			}
			BigDecimal qty = flightEvent.getTotalQuantity() != null ? flightEvent.getTotalQuantity() : BigDecimal.ZERO;
			this.calculateQuantity(fql, qty.multiply(BigDecimal.valueOf(-1), MathContext.DECIMAL64), flightEvent.getUnit());
			fql.setEvents(fql.getEvents() - flightEvent.getEvents());
			fqlMap.put(fql.getId(), fql);

		}
		for (FuelQuoteLocation fql : fqlMap.values()) {
			this.mergeEntity(fql);
		}

	}

	private void mergeEntity(FuelQuoteLocation fql) throws BusinessException {
		IFuelQuoteLocationService fqls = (IFuelQuoteLocationService) this.findEntityService(FuelQuoteLocation.class);
		fqls.update(fql);
	}

	private double getSysDensity() throws BusinessException {
		ISystemParameterService service = (ISystemParameterService) this.findEntityService(SystemParameter.class);
		return Double.parseDouble(service.getDensity());
	}

	private void calculateEvents(FuelQuoteLocation fql, FlightEvent newFlightEvent) {
		Collection<FlightEvent> flightEvents = fql.getFlightEvents();
		Integer events = newFlightEvent.getEvents();
		for (FlightEvent fe : flightEvents) {
			if (!fe.getId().equals(newFlightEvent.getId())) {
				events = events + (fe.getEvents() != null ? fe.getEvents() : 0);
			}
		}
		fql.setEvents(events);
	}

}
