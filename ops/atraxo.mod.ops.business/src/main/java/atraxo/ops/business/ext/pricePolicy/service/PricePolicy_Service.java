/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.pricePolicy.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.cmm.business.ext.prices.service.PriceConverterService;
import atraxo.fmbas.business.ext.sys.delegate.SystemParameterDelegate;
import atraxo.fmbas.domain.ext.exchangerate.ConversionResult;
import atraxo.fmbas.domain.ext.uom.Density;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.fmbas_type.MasterAgreementsPeriod;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.ops.business.api.pricePolicy.IPricePolicyService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link PricePolicy} domain entity.
 */
public class PricePolicy_Service extends atraxo.ops.business.impl.pricePolicy.PricePolicy_Service implements IPricePolicyService {

	@Autowired
	private PriceConverterService priceConverterService;

	@Override
	protected void preInsert(PricePolicy e) throws BusinessException {
		super.preInsert(e);
		this.verificateDates(e.getValidFrom(), e.getValidTo());
		this.combinationIdentifier(e);
		this.checkActiveValidity(e);
		this.defaultMarginMarkup(e);
	}

	@Override
	protected void preUpdate(PricePolicy e) throws BusinessException {
		super.preUpdate(e);
		this.verificateDates(e.getValidFrom(), e.getValidTo());
		this.combinationIdentifier(e);
		this.checkActiveValidity(e);
		this.defaultMarginMarkup(e);
	}

	@Override
	public List<PricePolicy> findByLocAndCust(Locations location, Customer customer) throws BusinessException {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT e FROM ").append(PricePolicy.class.getSimpleName());
		sb.append(" e WHERE ");
		sb.append(" e.clientId =:clientId and ( e.location = :location or e.location is null) and ");
		sb.append("( e.customer = :customer or e.customer is null)");
		sb.append(" and e.active = true and e.validFrom <= :date and e.validTo >= :date");
		return this.getEntityManager().createQuery(sb.toString(), PricePolicy.class).setParameter("location", location)
				.setParameter("customer", customer).setParameter("date", GregorianCalendar.getInstance().getTime())
				.setParameter("clientId", Session.user.get().getClientId()).getResultList();
	}

	/**
	 * Verify the dates. Valid to must be greater than valid from.
	 *
	 * @param from
	 * @param to
	 * @throws BusinessException
	 */
	public void verificateDates(Date from, Date to) throws BusinessException {
		if (from != null && to != null && from.after(to)) {
			throw new BusinessException(OpsErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					OpsErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
	}

	/**
	 * Policy can be activated or deactivated only between the validity period.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void checkActiveValidity(PricePolicy e) throws BusinessException {
		Date date = new Date();
		if ((e.getValidFrom().compareTo(date) > 0 || e.getValidTo().compareTo(date) < 0) && e.getActive()) {
			throw new BusinessException(OpsErrorCode.PRICE_POLICY_ACTIVATE, OpsErrorCode.PRICE_POLICY_ACTIVATE.getErrMsg());
		}
	}

	/**
	 * Customer-Location-AircraftType unique identifier.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void combinationIdentifier(PricePolicy e) throws BusinessException {

		String sql = "select e from " + PricePolicy.class.getSimpleName() + " e where e.clientId =:clientId ";
		if (e.getId() != null) {
			sql += "and e.id <> :pricePoliciId";
		}
		Query query = this.getEntityManager().createQuery(sql, PricePolicy.class);
		query.setParameter("clientId", Session.user.get().getClientId());
		if (e.getId() != null) {
			query.setParameter("pricePoliciId", e.getId());
		}
		@SuppressWarnings("unchecked")
		List<PricePolicy> policies = query.getResultList();
		if (policies != null) {
			for (PricePolicy pricePolicy : policies) {
				pricePolicy.getCustomer();
				if (this.isBetweenDates(e.getValidFrom(), e.getValidTo(), pricePolicy.getValidFrom())
						|| this.isBetweenDates(e.getValidFrom(), e.getValidTo(), pricePolicy.getValidTo())) {
					if (e.getCustomer() == pricePolicy.getCustomer() || (e.getCustomer() != null && pricePolicy.getCustomer() != null
							&& e.getCustomer().getId().equals(pricePolicy.getCustomer().getId()))) {
						if (e.getLocation() == pricePolicy.getLocation() || (e.getLocation() != null && pricePolicy.getLocation() != null
								&& e.getLocation().getId().equals(pricePolicy.getLocation().getId()))) {
							if (e.getAircraft() == pricePolicy.getAircraft() || (e.getAircraft() != null && pricePolicy.getAircraft() != null
									&& e.getAircraft().getId().equals(pricePolicy.getAircraft().getId()))) {
								throw new BusinessException(OpsErrorCode.PRICE_POLICY_COMBINATION, OpsErrorCode.PRICE_POLICY_COMBINATION.getErrMsg());
							}
						}
					}
				}
			}

		}
	}

	/**
	 * Check if the date is between the given dates.
	 *
	 * @param from - start of the period.
	 * @param to - period's end.
	 * @param date - the date which must be checked.
	 * @return True if date is between from and to. The period margins are not included.
	 */
	public boolean isBetweenDates(Date from, Date to, Date date) {
		return date.compareTo(from) > -1 && date.compareTo(to) < 1;
	}

	/**
	 * Verifies the values on margin and markup.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void defaultMarginMarkup(PricePolicy e) throws BusinessException {

		if (e.getDefaultMargin() == null && e.getDefaultMarkup() == null) {
			throw new BusinessException(OpsErrorCode.PRICE_POLICY_MARGIN_MARKUP, OpsErrorCode.PRICE_POLICY_MARGIN_MARKUP.getErrMsg());
		} else {
			if (e.getDefaultMargin() != null && e.getMinimalMargin() == null) {
				throw new BusinessException(OpsErrorCode.PRICE_POLICY_MINIMAL_MARGIN, OpsErrorCode.PRICE_POLICY_MINIMAL_MARGIN.getErrMsg());
			} else {
				if (e.getDefaultMarkup() != null && e.getMinimalMarkup() == null) {
					throw new BusinessException(OpsErrorCode.PRICE_POLICY_MINIMAL_MARKUP, OpsErrorCode.PRICE_POLICY_MINIMAL_MARKUP.getErrMsg());
				}
			}
		}
	}

	@Override
	public BigDecimal convert(Integer fromUnitId, Integer toUnitId, Integer fromCurrencyId, Integer toCurrencyId, BigDecimal value, Integer finSrcId,
			Integer avgMthdId, String offset) throws BusinessException {
		Density density = this.getBusinessDelegate(SystemParameterDelegate.class).getSystemDensity();
		Date date = DateUtils.truncate(GregorianCalendar.getInstance().getTime(), Calendar.DAY_OF_MONTH);
		ConversionResult result = this.priceConverterService.convert(fromUnitId, toUnitId, fromCurrencyId, toCurrencyId, value, date, finSrcId,
				avgMthdId, density, false, MasterAgreementsPeriod.getByName(offset));
		return result.getValue();
	}

}
