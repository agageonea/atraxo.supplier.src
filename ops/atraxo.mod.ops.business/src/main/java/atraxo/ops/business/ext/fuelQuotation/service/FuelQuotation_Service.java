/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.ext.fuelQuotation.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import atraxo.fmbas.business.ext.aop.history.Action;
import atraxo.fmbas.business.ext.aop.history.History;
import atraxo.fmbas.domain.impl.fmbas_type.CustomerStatus;
import atraxo.ops.business.api.fuelQuotation.IFuelQuotationService;
import atraxo.ops.business.api.fuelQuoteLocation.IFuelQuoteLocationService;
import atraxo.ops.business.api.fuelRequest.IFuelRequestService;
import atraxo.ops.business.ext.exceptions.OpsErrorCode;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import atraxo.ops.domain.impl.ops_type.FuelQuoteStatus;
import atraxo.ops.domain.impl.ops_type.FuelRequestStatus;
import seava.j4e.api.annotation.Reason;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.Session;

/**
 * Business extensions specific for {@link FuelQuotation} domain entity.
 */
public class FuelQuotation_Service extends atraxo.ops.business.impl.fuelQuotation.FuelQuotation_Service implements IFuelQuotationService {

	@Override
	@Transactional
	@History(type = FuelQuotation.class)
	public void updateStatus(FuelQuotation e, @Action String action, @Reason String reason) throws BusinessException {
		FuelQuoteStatus status = FuelQuoteStatus.getByName(action);
		e.setStatus(status);
		if (FuelQuoteStatus._SUBMITTED_.equals(status)) {
			this.verifyLocationsContracts(e);
			e.setPostedOn(GregorianCalendar.getInstance().getTime());
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.add(GregorianCalendar.DATE, 30);
			if (e.getValidUntil().before(calendar.getTime())) {
				e.setValidUntil(calendar.getTime());
			}
		}
		this.onUpdate(e);
	}

	@Override
	protected void preInsert(FuelQuotation e) throws BusinessException {
		super.preInsert(e);
		this.verifyDates(e.getPostedOn(), e.getValidUntil());
		this.checkValidUntil(e);
		this.isCustomer(e);
		this.checkCustomer(e);
	}

	@Override
	protected void preUpdate(FuelQuotation e) throws BusinessException {
		super.preUpdate(e);
		this.verifyDates(e.getPostedOn(), e.getValidUntil());
		this.checkValidUntil(e);
		this.isCustomer(e);
		this.checkCustomer(e);
	}

	@Override
	protected void postInsert(FuelQuotation e) throws BusinessException {
		super.postInsert(e);
		FuelQuotation savedFq = this.findByRefid(e.getRefid());
		savedFq.setCode(this.calculateCode(savedFq.getId()));
		this.update(savedFq);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void insert(List<FuelQuotation> list) throws BusinessException {
		super.insert(list);
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void update(List<FuelQuotation> list) throws BusinessException {
		super.update(list);
	}

	/**
	 * Verify the dates. Valid to must be greater than valid from.
	 *
	 * @param from
	 * @param to
	 * @throws BusinessException
	 */
	private void verifyDates(Date from, Date to) throws BusinessException {
		if (from != null && to != null && from.after(to)) {
			throw new BusinessException(OpsErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO,
					OpsErrorCode.VALID_FROM_CAN_NOT_BE_GREATER_THAN_VALID_TO.getErrMsg());
		}
	}

	/**
	 * Verify if customer code is approved.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void checkCustomer(FuelQuotation e) throws BusinessException {
		if (!CustomerStatus._ACTIVE_.equals(e.getCustomer().getStatus()) && this.isCustomerChanged(e)) {
			throw new BusinessException(OpsErrorCode.FQ_CUSTOMER_APPROVED, OpsErrorCode.FQ_CUSTOMER_APPROVED.getErrMsg());
		}
	}

	private boolean isCustomerChanged(FuelQuotation e) {
		if (e.getId() == null) {
			return false;
		}
		FuelQuotation fq = this.findById(e.getId());
		return !e.getCustomer().getId().equals(fq.getCustomer().getId());
	}

	/**
	 * Verify if the isCustomer is true.
	 *
	 * @param e
	 * @throws BusinessException
	 */
	private void isCustomer(FuelQuotation e) throws BusinessException {
		if (e.getCustomer() != null && (e.getCustomer().getIsCustomer() == null || !e.getCustomer().getIsCustomer())) {
			throw new BusinessException(OpsErrorCode.FQ_CUSTOMER_VALID, OpsErrorCode.FQ_CUSTOMER_VALID.getErrMsg());
		}
	}

	/**
	 * Check valid until to be at least today.
	 *
	 * @param e - FuelQuotation
	 * @throws BusinessException
	 */
	private void checkValidUntil(FuelQuotation e) throws BusinessException {
		Date date = new Date();
		if (e.getValidUntil() == null) {
			throw new BusinessException(OpsErrorCode.FQ_VALID_UNTIL_EMPTY, OpsErrorCode.FQ_VALID_UNTIL_EMPTY.getErrMsg());
		}

		// Solved valid until can't be in the past error on Fuel Order creation

		if (e.getValidUntil().before(date) && !FuelQuoteStatus._ORDERED_.equals(e.getStatus())) {
			throw new BusinessException(OpsErrorCode.FQ_VALID_UNTIL_PAST, OpsErrorCode.FQ_VALID_UNTIL_PAST.getErrMsg());
		}
	}

	@Override
	@Transactional(rollbackFor = BusinessException.class)
	public void delete(FuelQuotation e) throws BusinessException {
		super.delete(e);
	}

	@Override
	@Transactional
	public void expire() throws BusinessException {
		IFuelQuotationService service = (IFuelQuotationService) this.findEntityService(FuelQuotation.class);
		List<FuelQuotation> fuelQuotations = service.findForExpire();
		for (FuelQuotation fuelQuotation : fuelQuotations) {
			fuelQuotation.setStatus(FuelQuoteStatus._EXPIRED_);
			service.updateStatus(fuelQuotation, FuelQuoteStatus._EXPIRED_.getName(), "Job expire.");
		}
	}

	@Override
	public List<FuelQuotation> findForExpire() throws BusinessException {
		Date date = GregorianCalendar.getInstance().getTime();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT e FROM ");
		sb.append(FuelQuotation.class.getSimpleName());
		sb.append(" e WHERE ");
		sb.append(" e.clientId =:clientId and");
		sb.append(" e.status = :status");
		sb.append(" and e.validUntil < :date");
		return this.getEntityManager().createQuery(sb.toString(), FuelQuotation.class).setParameter("status", FuelQuoteStatus._SUBMITTED_)
				.setParameter("date", date).setParameter("clientId", Session.user.get().getClientId()).getResultList();
	}

	@Override
	protected void preDeleteByIds(List<Object> ids, Map<String, Object> context) throws BusinessException {
		super.preDeleteByIds(ids, context);
		this.removeRequestReference(ids);
		IFuelQuoteLocationService locQuaoteService = (IFuelQuoteLocationService) this.findEntityService(FuelQuoteLocation.class);
		List<Object> locIds = new ArrayList<>();
		for (Object id : ids) {
			FuelQuotation fq = this.findById(id);
			for (FuelQuoteLocation fql : fq.getFuelQuoteLocations()) {
				locIds.add(fql.getId());
			}
		}
		locQuaoteService.deleteByIds(locIds);
	}

	private void removeRequestReference(List<Object> ids) throws BusinessException {
		IFuelRequestService service = (IFuelRequestService) this.findEntityService(FuelRequest.class);
		for (Object id : ids) {
			List<FuelRequest> fuelRequest = service.findByFuelQuotationId((Integer) id);
			for (FuelRequest fr : fuelRequest) {
				fr.setFuelQuotation(null);
				fr.setRequestStatus(FuelRequestStatus._PROCESSED_);
			}
			service.update(fuelRequest);
		}
	}

	private String calculateCode(int id) {
		StringBuilder sb = new StringBuilder("FQ");
		sb.append(String.format("%05d", id));
		return sb.toString();
	}

	private void verifyLocationsContracts(FuelQuotation fq) throws BusinessException {
		Collection<FuelQuoteLocation> locations = fq.getFuelQuoteLocations();
		if (locations == null || locations.isEmpty()) {
			throw new BusinessException(OpsErrorCode.FUEL_QUOTE_NOT_ASSIGNED_LOCATION,
					String.format(OpsErrorCode.FUEL_QUOTE_NOT_ASSIGNED_LOCATION.getErrMsg()));
		}
		String locationsString = "";
		for (FuelQuoteLocation fql : locations) {
			if (fql.getContract() == null) {
				locationsString += locationsString.matches("") ? fql.getLocation().getCode() : ", " + fql.getLocation().getCode();
			}
		}
		if (!locationsString.matches("")) {
			throw new BusinessException(OpsErrorCode.FUEL_QUOTE_LOCATION_NOT_ASSIGNED_CONTRACT,
					String.format(OpsErrorCode.FUEL_QUOTE_LOCATION_NOT_ASSIGNED_CONTRACT.getErrMsg(), locationsString));
		}
	}
}
