package atraxo.ops.business.ext.integration;

import atraxo.ops.domain.ext.DeliveryNoteMethod.DeliveryNoteMethod;

public class MessageObject {

	private Integer objectId;
	private String objectType;
	private DeliveryNoteMethod method;

	public MessageObject(Integer objectId, String objectType, DeliveryNoteMethod method) {
		super();
		this.objectId = objectId;
		this.objectType = objectType;
		this.method = method;
	}

	public MessageObject() {
		super();
	}

	public Integer getObjectId() {
		return this.objectId;
	}

	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public DeliveryNoteMethod getMethod() {
		return this.method;
	}

	public void setMethod(DeliveryNoteMethod method) {
		this.method = method;
	}

}
