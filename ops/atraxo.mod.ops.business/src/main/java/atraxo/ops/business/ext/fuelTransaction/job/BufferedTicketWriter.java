package atraxo.ops.business.ext.fuelTransaction.job;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ops.business.api.fuelTransaction.IFuelTransactionService;
import atraxo.ops.domain.ext.fuelTicket.ProcessResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

public class BufferedTicketWriter implements ItemWriter<ProcessResult> {

	private static final Logger LOG = LoggerFactory.getLogger(BufferedTicketWriter.class);

	public static final String TICKETS = "tickets";

	@Autowired
	private IFuelTransactionService srv;

	private StepExecution stepExecution;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;

	}

	@Override
	public void write(List<? extends ProcessResult> items) throws Exception {
		LOG.info("Writing fuel transaction.");
		synchronized (this.srv) {
			Set<Object> refIds = new HashSet<>();
			for (ProcessResult result : items) {
				this.srv.saveProcessedResult(result);
				for (FuelTicket ticket : result.getInsList()) {
					refIds.add(ticket.getRefid());
				}
				for (FuelTicket ticket : result.getUpdList()) {
					refIds.add(ticket.getRefid());
				}
			}
			ExecutionContext context = this.stepExecution.getExecutionContext();
			if (context.containsKey(TICKETS)) {
				@SuppressWarnings("unchecked")
				Set<Object> set = (Set<Object>) context.get(TICKETS);
				refIds.addAll(set);
			}
			context.put(TICKETS, refIds);
		}
	}
}
