package atraxo.ops.business.ext.fuelTicket.job.items;

import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import atraxo.ops.business.api.fuelTicket.IFuelTicketService;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;

public class ExportTicketItemReader implements ItemReader<List<FuelTicket>> {

	@Autowired
	private IFuelTicketService service;

	@Override
	public List<FuelTicket> read() throws Exception {
		return this.service.getFuelTicketsForTransmission();
	}
}
