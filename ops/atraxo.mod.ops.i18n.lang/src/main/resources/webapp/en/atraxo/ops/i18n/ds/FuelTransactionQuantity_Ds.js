Ext.override(atraxo.ops.i18n.ds.FuelTransactionQuantity_Ds, {
	fuelQuantity__lbl: "Quantity",
	fuelQuantity__tlp: "Fueling quantity",
	fuelTransId__lbl: "Fuel Transaction (ID)",
	quantityType__lbl: "Quantity type",
	quantityType__tlp: "Quantity type: gross or net",
	quantityUOM__lbl: "Unit",
	quantityUOM__tlp: "Unit of measurement used for quantity"
});
