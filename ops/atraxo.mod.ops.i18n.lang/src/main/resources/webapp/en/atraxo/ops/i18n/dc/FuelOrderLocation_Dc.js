
Ext.override(atraxo.ops.i18n.dc.FuelOrderLocation_Dc$Details, {
	btn__lbl: "...",
	btn__tlp: "...",
	flightType__lbl: "Event type",
	flightType__tlp: "Event type",
	fuelReleaseEndsOn__lbl: "ends on",
	fuelReleaseEndsOn__tlp: "ends on",
	fuelReleaseStartsOn__lbl: "Fuel release validity period",
	fuelReleaseStartsOn__tlp: "Fuel release validity period",
	markup__lbl: "Markup",
	markup__tlp: "Markup",
	sellingPrice__lbl: "Selling fuel price",
	sellingPrice__tlp: "Selling fuel price"
});

Ext.override(atraxo.ops.i18n.dc.FuelOrderLocation_Dc$Filter, {
	currency__lbl: "Currency",
	currency__tlp: "Currency",
	unitCode__lbl: "Unit",
	unitCode__tlp: "Unit"
});

Ext.override(atraxo.ops.i18n.dc.FuelOrderLocation_Dc$List, {
	currency__lbl: "Currency",
	currency__tlp: "Currency",
	unitCode__lbl: "Unit",
	unitCode__tlp: "Unit"
});

Ext.override(atraxo.ops.i18n.dc.FuelOrderLocation_Dc$SupplySelect, {
	curr__lbl: "Payment currency and unit",
	curr__tlp: "Payment currency and unit",
	location__lbl: "Location",
	location__tlp: "Location"
});
