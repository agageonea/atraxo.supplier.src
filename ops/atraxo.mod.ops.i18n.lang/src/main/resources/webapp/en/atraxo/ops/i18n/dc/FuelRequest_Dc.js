
Ext.override(atraxo.ops.i18n.dc.FuelRequest_Dc$EditableList, {
	customerCode__tlp: "Customer code",
	openRelease__tlp: "Indicate if the fuel request is an open release",
	processedBy__tlp: "Name of the person who registered the fuel request",
	requestCode__tlp: "The fuel request's unique registration number",
	requestOn__tlp: "Date on which the fuel request was registered",
	status__tlp: "The status of the Fuel Request",
	subsidiaryCode__lbl: "Receiver",
	subsidiaryCode__tlp: "Receiver"
});

Ext.override(atraxo.ops.i18n.dc.FuelRequest_Dc$Filter, {
	subsidiaryCode__lbl: "Receiver",
	subsidiaryCode__tlp: "Receiver"
});

Ext.override(atraxo.ops.i18n.dc.FuelRequest_Dc$FormView, {
	contactLabel__lbl: "Contact person",
	contact__tlp: "Name of contact",
	customerLabel__lbl: "Customer",
	customerReferenceLabel__lbl: "Reference number",
	customerReference__tlp: "Customer reference number",
	customer__tlp: "Customer code",
	fromLabel__lbl: "Valid from",
	openReleaseLabel__lbl: "Open release",
	openRelease__tlp: "Indicate if the fuel request is an open release",
	processedByLabel__lbl: "Captured by",
	processedBy__tlp: "Name of the person who registered the fuel request",
	quoteNoLabel__lbl: "Fuel quotation",
	quoteNo__tlp: "Fuel quote code",
	receiverLabel__lbl: "Fuel request receiver",
	requestCodeLabel__lbl: "Request #",
	requestCode__tlp: "The fuel request's unique registration number",
	requestOnLabel__lbl: "Received on",
	requestOn__tlp: "Date on which the fuel request was registered",
	statusLabel__lbl: "Status",
	status__tlp: "The status of the Fuel Request",
	subsidiaryCode__tlp: "Receiver",
	toLabel__lbl: "Valid to"
});
