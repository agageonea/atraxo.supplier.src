
Ext.override(atraxo.ops.i18n.dc.ArchivedFuelTicket_Dc$ArchivedList, {
	afterUnit__lbl: "Unit",
	beforeUnit__lbl: "Unit",
	customer__lbl: "Ship-to",
	date__lbl: "Fueling date",
	densityMass__lbl: "Mass",
	densityVol__lbl: "Volume",
	flightID__lbl: "Flight #",
	netQuantUnit__lbl: "Unit",
	upliftUnit__lbl: "Unit"
});

Ext.override(atraxo.ops.i18n.dc.ArchivedFuelTicket_Dc$ArchivedTab, {
	fuelingEndLabel__lbl: "Fueling end date",
	fuelingStartLabel__lbl: "Fueling start date",
	meterEndIndexLabel__lbl: "Meter end index",
	meterStartIndexLabel__lbl: "Meter start index",
	refuelerArrivalLabel__lbl: "Refueler arrival",
	row3Label__lbl: "Net quantity",
	row4Label__lbl: "Before fueling",
	row5Label__lbl: "After fueling",
	row6Label__lbl: "Density",
	row8Label__lbl: "Temperature",
	transportLabel__lbl: "Transport by"
});

Ext.override(atraxo.ops.i18n.dc.ArchivedFuelTicket_Dc$ArchivedTabPayment, {
	cardExpLabel__lbl: "Card expiring date",
	cardHolderLabel__lbl: "Name on card",
	cardNumberLabel__lbl: "Card #",
	paymentTypeLabel__lbl: "Payment type",
	row1Label__lbl: "Amount received",
	row2Label__lbl: "Send invoice by"
});

Ext.override(atraxo.ops.i18n.dc.ArchivedFuelTicket_Dc$ArchivedView, {
	acTypeLabel__lbl: "Aircraft type",
	airlineDesLabel__lbl: "Airline designator",
	customerLabel__lbl: "Ship-to",
	customsStatusLabel__lbl: "Customs status",
	deliveryDateLabel__lbl: "Delivery date",
	departureLabel__lbl: "Departure",
	destinationLabel__lbl: "Destination",
	finalDestLabel__lbl: "Final destination",
	fuelerLabel__lbl: "Fueler",
	fuelingOperLabel__lbl: "Fueling operation",
	indicatorLabel__lbl: "Flight type",
	productLabel__lbl: "Product",
	registrationLabel__lbl: "Aircraft",
	registration__lbl: "Aircraft registration",
	resellerLabel__lbl: "Reseller",
	row1Label__lbl: "Quantity",
	row2Label__lbl: "Flight #",
	supplierLabel__lbl: "Supplier",
	ticketNoLabel__lbl: "Fuel ticket #"
});

Ext.override(atraxo.ops.i18n.dc.ArchivedFuelTicket_Dc$Filter, {
	approvedBy__tlp: "Name of the person who approved the Fuel Ticket",
	capturedBy__tlp: "Name of the person who registered the Fuel Ticket"
});
