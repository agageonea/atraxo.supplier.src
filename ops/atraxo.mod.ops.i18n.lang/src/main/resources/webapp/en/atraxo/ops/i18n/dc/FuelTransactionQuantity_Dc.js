
Ext.override(atraxo.ops.i18n.dc.FuelTransactionQuantity_Dc$Filter, {
	quantityType__lbl: "Type",
	quantity__lbl: "Fueling quantity",
	unit__lbl: "Unit"
});

Ext.override(atraxo.ops.i18n.dc.FuelTransactionQuantity_Dc$List, {
	quantityType__lbl: "Type",
	quantity__lbl: "Fueling quantity",
	unit__lbl: "Unit"
});
