
Ext.override(atraxo.ops.i18n.dc.FuelQuoteLocation_Dc$Edit, {
	btnSelectMarkup__lbl: "...",
	btnSelectMarkup__tlp: "...",
	btn__lbl: "...",
	btn__tlp: "...",
	location__lbl: "Location",
	location__tlp: "Location"
});

Ext.override(atraxo.ops.i18n.dc.FuelQuoteLocation_Dc$Filter, {
	avgMethodName__lbl: "Averaging method",
	currency__lbl: "Currency",
	currency__tlp: "Currency",
	financialSourceCode__lbl: "Financial source",
	unitCode__lbl: "Unit",
	unitCode__tlp: "Unit"
});

Ext.override(atraxo.ops.i18n.dc.FuelQuoteLocation_Dc$List, {
	averageMethodCode__lbl: "Averaging method",
	currency__lbl: "Currency",
	currency__tlp: "Currency",
	financialSourceCode__lbl: "Financial source",
	unitCode__lbl: "Unit",
	unitCode__tlp: "Unit"
});

Ext.override(atraxo.ops.i18n.dc.FuelQuoteLocation_Dc$PaymentTerms, {
	avgMethodName__lbl: "Averaging method",
	financialSourceCode__lbl: "Financial source"
});

Ext.override(atraxo.ops.i18n.dc.FuelQuoteLocation_Dc$PopUp, {
	avgMethodName__lbl: "Averaging method",
	financialSourceCode__lbl: "Financial source",
	location__lbl: "Location",
	location__tlp: "Location"
});

Ext.override(atraxo.ops.i18n.dc.FuelQuoteLocation_Dc$PricingSelect, {
	curr__lbl: "Payment currency and unit",
	curr__tlp: "Payment currency and unit",
	location__lbl: "Location",
	location__tlp: "Location"
});

Ext.override(atraxo.ops.i18n.dc.FuelQuoteLocation_Dc$SupplySelect, {
	curr__lbl: "Payment currency and unit",
	curr__tlp: "Payment currency and unit",
	location__lbl: "Location",
	location__tlp: "Location"
});
