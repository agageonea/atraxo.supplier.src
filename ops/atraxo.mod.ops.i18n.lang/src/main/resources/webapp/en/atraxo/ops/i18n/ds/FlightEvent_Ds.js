Ext.override(atraxo.ops.i18n.ds.FlightEvent_Ds, {
	acTypeCode__lbl: "Aircraft type",
	acTypeCode__tlp: "Aircraft type",
	acTypeId__lbl: "Aircraft Type (ID)",
	action__lbl: "Action",
	action__tlp: "Action",
	aircraftCallSign__lbl: "Aircraft call sign",
	aircraftCallSign__tlp: "Aircraft call sign",
	aircraftHBCode__lbl: "Aircraft home base",
	aircraftHBCode__tlp: "Code",
	aircraftHBId__lbl: "Aircraft Home Base (ID)",
	arrivalDate__lbl: "Estimated arrival time",
	arrivalDate__tlp: "Estimated arrival time",
	arrivalFromCode__lbl: "Arrival from",
	arrivalFromCode__tlp: "Code",
	arrivalFromId__lbl: "Arrival From (ID)",
	billStatus__lbl: "Purchase invoice status",
	billStatus__tlp: "Indicates if the ticket was invoiced by the supplier",
	captain__lbl: "Name of captain",
	captain__tlp: "Name of captain",
	departerDate__lbl: "Estimated departure time",
	departerDate__tlp: "Estimated departure time",
	destCode__lbl: "Destination",
	destCode__tlp: "Destination",
	destId__lbl: "Destination (ID)",
	eventTp__lbl: "Event type",
	eventTp__tlp: "Event type",
	eventType__lbl: "Event type",
	eventType__tlp: "Event type",
	events__lbl: "Events",
	events__tlp: "Events",
	flightDetails__lbl: "<span style='font-weight:bold !important'>Flight details</span>",
	flightNo__lbl: "Flight #",
	flightNo__tlp: "Flight #",
	fuelReqId__lbl: "Fuel Request (ID)",
	fuelRequestCode__lbl: "Request #",
	fuelRequestCode__tlp: "Request #",
	fuelRequestStatus__lbl: "Status",
	fuelRequestStatus__tlp: "Status",
	fuelingDetails__lbl: "<span style='font-weight:bold !important'>Fueling details</span>",
	handlerCode__lbl: "Handler",
	handlerCode__tlp: "Handler",
	handlerId__lbl: "Handler (ID)",
	invoiceStatus__lbl: "Invoice status",
	invoiceStatus__tlp: "Indicates if the ticket was invoiced to the customer",
	isEnabled__lbl: "",
	locCode__lbl: "Departure",
	locCode__tlp: "Departure",
	locId__lbl: "Departure (ID)",
	locQuoteId__lbl: "Loc Quote (ID)",
	locationOrderId__lbl: "Loc Order (ID)",
	newArrivalDate__lbl: "Arrival date",
	newDepartureDate__lbl: "Departer date",
	newSchedule__lbl: "<span style='font-weight:bold !important'>New schedule</span>",
	newTimeReference__lbl: "",
	newUpliftDate__lbl: "Fueling date",
	oldSchedule__lbl: "<span style='font-weight:bold !important'>Old schedule</span>",
	operationTp__lbl: "Operational type",
	operationTp__tlp: "Operational type",
	operationType__lbl: "Operation type",
	operationType__tlp: "Operation type",
	operatorCode__lbl: "Operator",
	operatorCode__tlp: "Operator",
	operatorId__lbl: "Operator (ID)",
	paramStatus__lbl: "",
	perCaptain__lbl: "Quantity as per captain's discretion",
	perCaptain__tlp: "Quantity as per captain's discretion",
	product__lbl: "Product",
	product__tlp: "Product",
	quantity__lbl: "Fueling quantity",
	quantity__tlp: "Fueling quantity",
	registration__lbl: "Aircraft",
	registration__tlp: "Aircraft registration number",
	remarks__lbl: "",
	scheduleDetails__lbl: "<span style='font-weight:bold !important'>Schedule details</span>",
	separator__lbl: "<hr style='height:1px; border:none; color:#D0D0D0; background-color:#D0D0D0;'>",
	src__lbl: "Action source",
	src__tlp: "Action source",
	status__lbl: "Status",
	status__tlp: "Status",
	suffix__lbl: "Flight suffix",
	suffix__tlp: "Flight suffix",
	tankCap__lbl: "Tank capacity",
	tankCap__tlp: "Tank capacity",
	tankCapacity__lbl: "Tank capacity",
	tankCapacity__tlp: "Tank capacity",
	timeReference__lbl: "Time reference GMT/UTC",
	timeReference__tlp: "Time reference GMT/UTC",
	toleranceMessage__lbl: "Tolerance message",
	toleranceMessage__tlp: "Tolerance message",
	totalQuantity__lbl: "Total quantity",
	totalQuantity__tlp: "The fuel quantity required for all flight occurrences",
	unitCode__lbl: "Unit",
	unitCode__tlp: "Unit",
	unitId__lbl: "Unit (ID)",
	upliftDate__lbl: "Fueling date and time",
	upliftDate__tlp: "Fueling date and time",
	warningMessage__lbl: ""
});
