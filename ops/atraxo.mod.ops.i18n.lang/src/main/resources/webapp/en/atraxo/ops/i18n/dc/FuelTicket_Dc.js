
Ext.override(atraxo.ops.i18n.dc.FuelTicket_Dc$ApprovalNote, {
	approvalNote__lbl: "Add a note to be sent with your request to the approvers"
});

Ext.override(atraxo.ops.i18n.dc.FuelTicket_Dc$Edit, {
	acTypeLabel__lbl: "Aircraft type",
	addRegistration__lbl: "<i class='fa fa-plus'></i>",
	airlineDesLabel__lbl: "Airline designator",
	customerLabel__lbl: "Ship-to",
	customsStatusLabel__lbl: "Customs status",
	deliveryDateLabel__lbl: "Delivery date",
	departureLabel__lbl: "Departure",
	destinationLabel__lbl: "Destination",
	finalDestLabel__lbl: "Final destination",
	fuelerLabel__lbl: "Fueler",
	fuelingOperLabel__lbl: "Fueling operation",
	indicatorLabel__lbl: "Flight type",
	productLabel__lbl: "Product",
	refTicketNo__lbl: "Reference fuel ticket #",
	registrationLabel__lbl: "Aircraft",
	registration__lbl: "Aircraft registration",
	resellerLabel__lbl: "Reseller",
	row1Label__lbl: "Quantity",
	row2Label__lbl: "Flight #",
	supplierLabel__lbl: "Supplier",
	ticketNoLabel__lbl: "Fuel ticket #"
});

Ext.override(atraxo.ops.i18n.dc.FuelTicket_Dc$Filter, {
	approvedBy__tlp: "Name of the person who approved the fuel ticket",
	capturedBy__tlp: "Name of the person who registered the fuel ticket"
});

Ext.override(atraxo.ops.i18n.dc.FuelTicket_Dc$List, {
	afterUnit__lbl: "Unit",
	beforeUnit__lbl: "Unit",
	btnHardCopy__lbl: "Hard copy",
	btnHardCopy__tlp: "Fuel ticket hard copy",
	customerName__lbl: "Ship-to name",
	customer__lbl: "Ship-to",
	date__lbl: "Fueling date",
	densityMass__lbl: "Mass",
	densityVol__lbl: "Volume",
	flightID__lbl: "Flight #",
	netQuantUnit__lbl: "Unit",
	upliftUnit__lbl: "Unit"
});

Ext.override(atraxo.ops.i18n.dc.FuelTicket_Dc$NewFuelTicketNumber, {
	newFuelTicketNumber__lbl: "New fuel ticket #"
});

Ext.override(atraxo.ops.i18n.dc.FuelTicket_Dc$PopUp, {
	acTypeLabel__lbl: "Aircraft type",
	addRegistration__lbl: "<i class='fa fa-plus'></i>",
	airlineDesLabel__lbl: "Airline designator",
	customerLabel__lbl: "Ship-to",
	customsStatusLabel__lbl: "Customs status",
	deliveryDateLabel__lbl: "Delivery date",
	departureLabel__lbl: "Departure",
	destinationLabel__lbl: "Destination",
	fuelReleaseLabel__lbl: "Reseller",
	fuelerLabel__lbl: "Fueler",
	fuelingOperLabel__lbl: "Fueling operation",
	indicatorLabel__lbl: "Flight type",
	productLabel__lbl: "Product",
	registrationLabel__lbl: "Aircraft",
	row1Label__lbl: "Quantity",
	row2Label__lbl: "Flight #",
	row3Label__lbl: "Density",
	row4Label__lbl: "Temperature",
	row5Label__lbl: "Net quantity",
	supplierLabel__lbl: "Supplier",
	ticketNoLabel__lbl: "Fuel ticket #"
});

Ext.override(atraxo.ops.i18n.dc.FuelTicket_Dc$RevokeRemark, {
	cancelWindowText__lbl: "You are about to revoke the selected fuel tickets. If the tickets have been invoiced and the invoices have been released, credit memos will be generated automatically.",
	emptyLine__lbl: " ",
	revocationReason__lbl: "Please indicate the reason of the revocation"
});

Ext.override(atraxo.ops.i18n.dc.FuelTicket_Dc$Tab, {
	fuelingEndLabel__lbl: "Fueling end date",
	fuelingStartLabel__lbl: "Fueling start date",
	meterEndIndexLabel__lbl: "Meter end index",
	meterStartIndexLabel__lbl: "Meter start index",
	refuelerArrivalLabel__lbl: "Refueler arrival",
	row3Label__lbl: "Net quantity",
	row4Label__lbl: "Before fueling",
	row5Label__lbl: "After fueling",
	row6Label__lbl: "Density",
	row8Label__lbl: "Temperature",
	transportLabel__lbl: "Transport by"
});

Ext.override(atraxo.ops.i18n.dc.FuelTicket_Dc$TabPayment, {
	cardExpLabel__lbl: "Card expiring date",
	cardHolderLabel__lbl: "Name on card",
	cardNumberLabel__lbl: "Card #",
	paymentTypeLabel__lbl: "Payment type",
	row1Label__lbl: "Amount received",
	row2Label__lbl: "Send invoice by"
});
