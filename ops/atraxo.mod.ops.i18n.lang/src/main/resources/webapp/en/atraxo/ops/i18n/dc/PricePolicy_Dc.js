
Ext.override(atraxo.ops.i18n.dc.PricePolicy_Dc$Edit, {
	minimalMargin__lbl: "Minimal"
});

Ext.override(atraxo.ops.i18n.dc.PricePolicy_Dc$Filter, {
	defaultMargin__lbl: "Default margin",
	defaultMarkup__lbl: "Default markup",
	validTo__lbl: "Valid to"
});

Ext.override(atraxo.ops.i18n.dc.PricePolicy_Dc$List, {
	curency__lbl: "Currency",
	defaultMargin__lbl: "Default margin",
	defaultMarkup__lbl: "Default markup",
	minimalMargin__lbl: "Minimal margin",
	minimalMarkup__lbl: "Minimal markup",
	unit__lbl: "Unit",
	validTo__lbl: "Valid to"
});
