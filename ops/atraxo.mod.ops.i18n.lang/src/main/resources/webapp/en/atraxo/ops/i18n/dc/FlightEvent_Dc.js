
Ext.override(atraxo.ops.i18n.dc.FlightEvent_Dc$ListRequest, {
	airport__lbl: "Location"
});

Ext.override(atraxo.ops.i18n.dc.FlightEvent_Dc$RescheduleFlightevent, {
	newArrivalTime__lbl: "Estimated arrival time",
	newDepartureDate__lbl: "Estimated departure time",
	newTimeReference__lbl: "Time reference GMT/UTC",
	newUpliftDate__lbl: "Fueling date and time",
	oldArrivalTime__lbl: "Estimated arrival time",
	oldDepartureDate__lbl: "Estimated departure time",
	oldTimeReference__lbl: "Time reference GMT/UTC",
	oldUpliftDate__lbl: "Fueling date and time"
});
