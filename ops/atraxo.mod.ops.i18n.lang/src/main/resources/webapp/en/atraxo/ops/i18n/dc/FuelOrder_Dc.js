
Ext.override(atraxo.ops.i18n.dc.FuelOrder_Dc$Edit, {
	fuelQuoteCode__lbl: "Quote #"
});

Ext.override(atraxo.ops.i18n.dc.FuelOrder_Dc$Filter, {
	subsidiaryCode__lbl: "Receiver",
	subsidiaryCode__tlp: "Receiver"
});

Ext.override(atraxo.ops.i18n.dc.FuelOrder_Dc$List, {
	subsidiaryCode__lbl: "Receiver",
	subsidiaryCode__tlp: "Receiver"
});
