
Ext.override(atraxo.ops.i18n.dc.FuelTransactionEquipment_Dc$Edit, {
	densityTypeLabel__lbl: "Density source",
	equipmentIdLabel__lbl: "Equipment ID",
	fuelingTypeLabel__lbl: "Fueling type",
	meterEndLabel__lbl: "Meter end",
	meterStartLabel__lbl: "Meter start",
	row1Label__lbl: "Average temperature",
	row2Label__lbl: "Density",
	row3Label__lbl: "Meter quantity"
});

Ext.override(atraxo.ops.i18n.dc.FuelTransactionEquipment_Dc$Filter, {
	densityType__lbl: "Density type",
	densityUOM__lbl: "Unit",
	density__lbl: "Density",
	equipmentId__lbl: "Equipment ID",
	fuelingType__lbl: "Fueling type",
	meterQuantityDelivered__lbl: "Meter quantity",
	meterReadingEnd__lbl: "Meter reading stop",
	meterReadingStart__lbl: "Meter reading start",
	temperatureUnit__lbl: "Unit",
	temperature__lbl: "Fueling temperature",
	unit__lbl: "Unit"
});

Ext.override(atraxo.ops.i18n.dc.FuelTransactionEquipment_Dc$List, {
	densityType__lbl: "Density type",
	densityUOM__lbl: "Unit",
	density__lbl: "Density",
	equipmentId__lbl: "Equipment ID",
	fuelingType__lbl: "Fueling type",
	meterQuantityDelivered__lbl: "Meter quantity",
	meterQuantityUOM__lbl: "Unit",
	meterReadingEnd__lbl: "Meter reading stop",
	meterReadingStart__lbl: "Meter reading start",
	temperatureUnit__lbl: "Unit",
	temperature__lbl: "Fueling temperature"
});
