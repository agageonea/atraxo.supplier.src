/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
__OPS_TYPE__ = {
	FuelQuoteType : {
		_SCHEDULED_ : "Scheduled" , 
		_AD_HOC_ : "Ad-hoc" 
	},
	FuelQuoteScope : {
		_INTO_PLANE_ : "Into plane" , 
		_EX_HYDRANT_ : "Ex-hydrant" , 
		_INTO_STORAGE_ : "Into storage" 
	},
	FuelQuoteStatus : {
		_NEW_ : "New" , 
		_SUBMITTED_ : "Submitted" , 
		_NEGOTIATING_ : "Negotiating" , 
		_ORDERED_ : "Ordered" , 
		_CLOSED_ : "Closed" , 
		_CANCELED_ : "Canceled" , 
		_EXPIRED_ : "Expired" 
	},
	FuelQuoteCycle : {
		_NOTICE_ : "Notice" , 
		_WEEKLY_ : "Weekly" , 
		_BI_WEEKLY_ : "Bi-weekly" , 
		_MONTHLY_ : "Monthly" 
	},
	FuelRequestStatus : {
		_NEW_ : "New" , 
		_QUOTED_ : "Quoted" , 
		_PROCESSED_ : "Processed" , 
		_CANCELED_ : "Canceled" , 
		_CLOSED_ : "Closed" 
	},
	OperationType : {
		_COMMERCIAL_ : "Commercial" , 
		_BUSINESS_ : "Business" , 
		_PRIVATE_ : "Private" 
	},
	FuelTicketCustomsStatus : {
		_BONDED_ : "Bonded" , 
		_DOMESTIC_ : "Domestic" , 
		_FTZ_ : "FTZ" , 
		_UNSPECIFIED_ : "Unspecified" , 
		_DUTY_PAID_ : "Duty Paid" , 
		_DUTY_FREE_ : "Duty Free" , 
		_NOT_APPLICABLE_ : "Not Applicable" 
	},
	Suffix : {
		_D_DIVERTED_ : "D-Diverted" , 
		_F_FERRY_ : "F-Ferry" , 
		_G_GROUND_RETURN_ : "G-Ground return" 
	},
	FuelTicketApprovalStatus : {
		_FOR_APPROVAL_ : "For approval" , 
		_AWAITING_APPROVAL_ : "Awaiting approval" , 
		_OK_ : "OK" , 
		_REJECTED_ : "Rejected" , 
		_RECHECK_ : "Recheck" 
	},
	FuelTicketStatus : {
		_ORIGINAL_ : "Original" , 
		_UPDATED_ : "Updated" , 
		_CANCELED_ : "Canceled" , 
		_REISSUE_ : "Reissue" 
	},
	FuelTicketTransmissionStatus : {
		_NEW_ : "New" , 
		_SUBMITTED_ : "Submitted" , 
		_FAILED_ : "Failed" , 
		_EXPORTED_ : "Exported" , 
		_TRANSMITTED_ : "Transmitted" 
	},
	FuelTicketSource : {
		_MANUAL_ : "Manual" , 
		_IMPORT_ : "Import" , 
		_SCAN_ : "Scan" 
	},
	FuelOrderStatus : {
		_NEW_ : "New" , 
		_REJECTED_ : "Rejected" , 
		_CONFIRMED_ : "Confirmed" , 
		_CANCELED_ : "Canceled" , 
		_RELEASED_ : "Released" 
	},
	InvoiceStatus : {
		_NO_ACCRUALS_ : "No Accruals" , 
		_PARTIALLY_ : "Partially" , 
		_OK_ : "OK" , 
		_OPEN_ISSUES_ : "Open Issues" 
	},
	PricePolicyType : {
		_MARGIN_ : "Margin" , 
		_MARKUP_ : "Markup" 
	},
	FlightEventStatus : {
		_SCHEDULED_ : "Scheduled" , 
		_FULFILLED_ : "Fulfilled" , 
		_CANCELED_ : "Canceled" , 
		_INVOICED_ : "Invoiced" 
	},
	OutgoingInvoiceStatus : {
		_NOT_INVOICED_ : "Not invoiced" , 
		_AWAITING_APPROVAL_ : "Awaiting approval" , 
		_AWAITING_PAYMENT_ : "Awaiting payment" , 
		_PAID_ : "Paid" , 
		_CREDITED_ : "Credited" 
	},
	TemperatureUnit : {
		_C_ : "C" , 
		_F_ : "F" 
	},
	SendInvoiceBy : {
		_E_MAIL_ : "E-mail" , 
		_FAX_ : "Fax" 
	},
	InterfaceType : {
		_IATA_ : "IATA" , 
		_JETA_COM_ : "JETA.COM" 
	},
	DeliveryType : {
		_INTO_PLANE_ : "Into Plane" , 
		_BULK_MOVEMENT_ : "Bulk Movement" 
	},
	TicketType : {
		_ORIGINAL_ : "Original" , 
		_REISSUE_ : "Reissue" , 
		_CANCEL_ : "Cancel" , 
		_DELETE_ : "Delete" 
	},
	TicketSource : {
		_MANUAL_ : "Manual" , 
		_ELECTRONIC_ : "Electronic" 
	},
	InternationalFlight : {
		_INTERNATIONAL_ : "International" , 
		_DOMESTIC_ : "Domestic" 
	},
	IPTransactionCode : {
		_UPLIFT_ : "Uplift" , 
		_DE_FUEL_ : "De-fuel" , 
		_RE_FUEL_ : "Re-fuel" 
	},
	ValidationState : {
		_NOT_CHECKED_ : "Not checked" , 
		_FAILED_ : "Failed" 
	},
	DensityType : {
		_MEASURED_ : "Measured" , 
		_STANDARD_ : "Standard" 
	},
	DensityUOM : {
		_KGM_ : "KGM" , 
		_KGL_ : "KGL" , 
		_LGH_ : "LGH" 
	},
	FuelQuantityType : {
		_GROSS_ : "Gross" , 
		_NET_ : "Net" 
	},
	RevocationReason : {
		_INCORRECT_CUSTOMER_INFORMATION_ : "Incorrect Customer Information" , 
		_INCORRECT_FLIGHT_AIRCRAFT_INFORMATION_ : "Incorrect Flight/Aircraft Information" , 
		_INCORRECT_VOLUME_ : "Incorrect Volume" , 
		_INCORRECT_PRICE___DISCOUNT___TAX___FREIGHT_ : "Incorrect Price / Discount / Tax / Freight" , 
		_COMMERCIAL_REASON_ : "Commercial Reason" 
	}
}
