/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ops.ui.extjs.ds.FuelQuoteLocation_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FuelQuoteLocation_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"location", dataIndex:"locCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25}})
			.addCombo({ xtype:"combo", name:"flightType", dataIndex:"flightType", store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_]})
			.addCombo({ xtype:"combo", name:"operType", dataIndex:"operationalType", store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_]})
			.addDateField({name:"startOn", dataIndex:"fuelReleaseStartsOn"})
			.addDateField({name:"endsOn", dataIndex:"fuelReleaseEndsOn"})
			.addLov({name:"supplier", dataIndex:"suppCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					filterFieldMapping: [{lovField:"isSupplier", value: "true"} ]}})
			.addLov({name:"iplAgent", dataIndex:"iplAgentCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					filterFieldMapping: [{lovField:"iplAgent", value: "true"} ]}})
			.addLov({name:"unitCode", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2}})
			.addLov({name:"currency", dataIndex:"currCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3}})
			.addLov({name:"financialSourceCode", dataIndex:"financialSourceCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CodeLov_Lov", selectOnFocus:true, maxLength:25}})
			.addLov({name:"avgMethodName", dataIndex:"averageMethodName", maxLength:50,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AvgMethLov_Lov", selectOnFocus:true, maxLength:50}})
			.addNumberField({name:"fuelPrice", dataIndex:"basePrice", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"intoplaneFee", dataIndex:"intoplaneFee", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"otherFees", dataIndex:"otherFees", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"taxes", dataIndex:"taxes", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"totalPricePerUom", dataIndex:"totalPricePerUom", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"flightFee", dataIndex:"flightFee", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"events", dataIndex:"events", width:100, maxLength:11})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelQuoteLocation_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"locCode", dataIndex:"locCode", width:80})
		.addTextColumn({ name:"flightType", dataIndex:"flightType", width:100})
		.addTextColumn({ name:"operType", dataIndex:"operationalType", width:120})
		.addDateColumn({ name:"fuelReleaseStartsOn", dataIndex:"fuelReleaseStartsOn", _mask_: Masks.DATE})
		.addDateColumn({ name:"fuelReleaseEndsOn", dataIndex:"fuelReleaseEndsOn", _mask_: Masks.DATE})
		.addTextColumn({ name:"suppCode", dataIndex:"suppCode", width:100})
		.addTextColumn({ name:"iplAgent", dataIndex:"iplAgentCode", width:90})
		.addNumberColumn({ name:"quantity", dataIndex:"quantity", width:80, sysDec:"dec_unit",  renderer:function(value) {return Ext.util.Format.number(Math.round(value), Main.getNumberFormat(2))}})
		.addTextColumn({ name:"unitCode", dataIndex:"unitCode", width:50})
		.addTextColumn({ name:"currency", dataIndex:"currCode", width:70})
		.addNumberColumn({ name:"differential", dataIndex:"resellerDiff", width:90, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"fuelPrice", dataIndex:"basePrice", sysDec:"dec_crncy"})
		.addNumberColumn({ name:"intoplaneFee", dataIndex:"intoplaneFee", sysDec:"dec_crncy"})
		.addNumberColumn({ name:"otherFees", dataIndex:"otherFees", sysDec:"dec_crncy"})
		.addNumberColumn({ name:"taxes", dataIndex:"taxes", width:80, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"totalPricePerUom", dataIndex:"totalPricePerUom", sysDec:"dec_crncy"})
		.addNumberColumn({ name:"flightFee", dataIndex:"flightFee", sysDec:"dec_crncy"})
		.addTextColumn({ name:"quoteCode", dataIndex:"quoteCode", hidden:true, width:100})
		.addTextColumn({ name:"operationalType", dataIndex:"operationalType", hidden:true, width:100})
		.addTextColumn({ name:"product", dataIndex:"product", hidden:true, width:100})
		.addTextColumn({ name:"contractCode", dataIndex:"contractCode", hidden:true, width:100})
		.addTextColumn({ name:"deliveryMethod", dataIndex:"deliveryMethod", hidden:true, width:100})
		.addNumberColumn({ name:"events", dataIndex:"events", width:100})
		.addTextColumn({ name:"pricingBases", dataIndex:"priceBasis", hidden:true, width:100})
		.addTextColumn({ name:"quotName", dataIndex:"quotationName", hidden:true, width:100})
		.addTextColumn({ name:"quotCode", dataIndex:"quoteCode", hidden:true, width:100})
		.addNumberColumn({ name:"basePrice", dataIndex:"basePrice", hidden:true, width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"margin", dataIndex:"capturedMargin", hidden:true, width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"paymentTerms", dataIndex:"paymentTerms", hidden:true, width:100})
		.addTextColumn({ name:"paymentRefDate", dataIndex:"paymentRefDate", hidden:true, width:100})
		.addTextColumn({ name:"invoiceFreq", dataIndex:"invoiceFreq", hidden:true, width:100})
		.addTextColumn({ name:"averageMethodCode", dataIndex:"averageMethodCode", hidden:true, width:100})
		.addTextColumn({ name:"financialSourceCode", dataIndex:"financialSourceCode", hidden:true, width:100})
		.addTextColumn({ name:"indexOffset", dataIndex:"indexOffset", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: PopUp ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc$PopUp", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuoteLocation_Dc$PopUp",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", noEdit:true , width:240, maxLength:32, labelWidth:110})
		.addLov({name:"location", bind:"{d.locCode}", dataIndex:"locCode", allowBlank:false, width:265, xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:130,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ]})
		.addCombo({ xtype:"combo", name:"flightType", bind:"{d.flightType}", dataIndex:"flightType", allowBlank:false, width:255, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:120})
		.addCombo({ xtype:"combo", name:"operationalType", bind:"{d.operationalType}", dataIndex:"operationalType", allowBlank:false, width:265, store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_], labelWidth:135})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", width:190, sysDec:"dec_unit", maxLength:19, labelWidth:110})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", allowBlank:false, width:50, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addNumberField({name:"events", bind:"{d.events}", dataIndex:"events", width:265, maxLength:11, labelWidth:130})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.product}", dataIndex:"product", width:255, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_], labelWidth:120})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", width:180, maxLength:4, labelWidth:110})
		.addCombo({ xtype:"combo", name:"paymentRefDate", bind:"{d.paymentRefDate}", dataIndex:"paymentRefDate", width:290, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_], labelWidth:155})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFreq}", dataIndex:"invoiceFreq", width:255, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], labelWidth:120,listeners:{
			expand:{scope:this, fn:this._filterInvoiceFreq_}
		}})
		.addLov({name:"curr", bind:"{d.currCode}", dataIndex:"currCode", width:260, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:135,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			fpchange:{scope:this, fn:this._enableFs_}
		}})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", allowBlank:false, width:240, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25, labelWidth:110,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "currId"} ],listeners:{
			fpchange:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_}
		}})
		.addLov({name:"avgMethodName", bind:"{d.averageMethodName}", dataIndex:"averageMethodName", _enableFn_: this._enableIfFinancialSource_, allowBlank:false, width:266, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50, labelWidth:132,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "averageMethodId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsField: "currId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvg_}
		}})
		.addCombo({ xtype:"combo", name:"period", bind:"{d.period}", dataIndex:"period", allowBlank:false, width:255, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelWidth:120})
		.addDateField({name:"fuelReleaseStartsOn", bind:"{d.fuelReleaseStartsOn}", dataIndex:"fuelReleaseStartsOn", allowBlank:false, width:240, labelWidth:110})
		.addDateField({name:"fuelReleaseEndsOn", bind:"{d.fuelReleaseEndsOn}", dataIndex:"fuelReleaseEndsOn", allowBlank:false, width:265, labelWidth:132})
		.addDisplayFieldText({ name:"separator", bind:"{d.separator}", dataIndex:"separator", maxLength:16, labelWidth:910, labelAlign:"center"})
		.addDisplayFieldText({ name:"separator1", bind:"{d.separator}", dataIndex:"separator", maxLength:16, labelWidth:910, labelAlign:"center"})
		.addDisplayFieldText({ name:"days", bind:"{d.days}", dataIndex:"days", maxLength:32, labelWidth:30})
		.addDisplayFieldText({ name:"text", bind:"{d.fuelReleaseVP}", dataIndex:"fuelReleaseVP", maxLength:64, labelWidth:170})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("customer"),this._getConfig_("location"),this._getConfig_("flightType"),this._getConfig_("operationalType")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantity"),this._getConfig_("unit"),this._getConfig_("events"),this._getConfig_("product")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("paymentTerms"),this._getConfig_("days"),this._getConfig_("paymentRefDate"),this._getConfig_("invoiceFreq"),this._getConfig_("curr")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("financialSourceCode"),this._getConfig_("avgMethodName"),this._getConfig_("period")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("fuelReleaseStartsOn"),this._getConfig_("fuelReleaseEndsOn")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:860})
		.addPanel({ name:"col1", width:1030, layout:"anchor"})
		.addPanel({ name:"col2", width:1030, layout:"anchor"})
		.addPanel({ name:"col3", width:1030, layout:"anchor"})
		.addPanel({ name:"col4", width:1030, layout:"anchor"})
		.addPanel({ name:"col5", width:1030, layout:"anchor"})
		.addPanel({ name:"col6", width:1030, layout:"anchor"})
		.addPanel({ name:"col7", width:1030, layout:"anchor"})
		.addPanel({ name:"col8", width:1030, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col3", ["separator"])
		.addChildrenTo("col4", ["row3"])
		.addChildrenTo("col5", ["row4"])
		.addChildrenTo("col6", ["separator1"])
		.addChildrenTo("col7", ["text"])
		.addChildrenTo("col8", ["row5"]);
	},
	/* ==================== Business functions ==================== */
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });
						}, this);
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_enableFs_: function(el,newVal,oldVal) {
		
		
						var financialSourceCode = this._get_("financialSourceCode");
						var elVal = el.getValue(); 
						if (!Ext.isEmpty(elVal)) {
							financialSourceCode.setReadOnly(false);
						}
						else {
							financialSourceCode.setReadOnly(true);
						}
						financialSourceCode.setValue("");
						
	},
	
	_enableAvgMethods_: function(el,newVal,oldVal) {
		
		
						var avgMethodName = this._get_("avgMethodName");
						var elVal = el.getValue(); 
						if (!Ext.isEmpty(elVal)) {
							avgMethodName.setReadOnly(false);
						}
						else {
							avgMethodName.setReadOnly(true);
						}
						avgMethodName.setValue("");
	},
	
	_enableIfFinancialSource_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var financialSourceCode = record.get("financialSourceCode");
							if (!Ext.isEmpty(financialSourceCode)) {
								result = true;
							}
						}
		
						return result;
	},
	
	_filterInvoiceFreq_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_ && value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_ 
								&& value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_ && value !== __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_) {
								return value;
							}
						});
	}
});

/* ================= EDIT FORM: PaymentTerms ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc$PaymentTerms", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuoteLocation_Dc$PaymentTerms",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", width:170, maxLength:4, labelWidth:120})
		.addCombo({ xtype:"combo", name:"paymentRefDate", bind:"{d.paymentRefDate}", dataIndex:"paymentRefDate", width:350, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_], labelWidth:195})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFreq}", dataIndex:"invoiceFreq", width:280, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], labelWidth:130,listeners:{
			expand:{scope:this, fn:this._filterInvoiceFreq_}
		}})
		.addLov({name:"curr", bind:"{d.currCode}", dataIndex:"currCode", width:205, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:130,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			change:{scope:this, fn:this._enableFs_}
		}})
		.addLov({name:"financialSourceCode", bind:"{d.financialSourceCode}", dataIndex:"financialSourceCode", _enableFn_: function(dc, rec) { return dc.record.data.currId != null; ; } , allowBlank:false, width:270, xtype:"fmbas_ExchangeRateToFinancialSourceAndAvgFilteredLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"} ],
			filterFieldMapping: [{lovField:"currencyId", dsField: "currId"} ],listeners:{
			change:{scope:this, fn:this._enableAvgMethods_},
			expand:{scope:this, fn:this._filterDistinct_}
		}})
		.addLov({name:"avgMethodName", bind:"{d.averageMethodName}", dataIndex:"averageMethodName", _enableFn_: this._enableIfFinancialSource_, allowBlank:false, width:286, xtype:"fmbas_ExchangeRateToAvgMthdFilteredLov_Lov", maxLength:50, labelWidth:130,
			retFieldMapping: [{lovField:"avgMthdId", dsField: "averageMethodId"} ],
			filterFieldMapping: [{lovField:"financialSourceId", dsField: "financialSourceId"}, {lovField:"currencyId", dsField: "currId"} ],listeners:{
			expand:{scope:this, fn:this._filterDistinctAvg_}
		}})
		.addCombo({ xtype:"combo", name:"period", bind:"{d.period}", dataIndex:"period", allowBlank:false, width:280, store:[ __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT_, __FMBAS_TYPE__.MasterAgreementsPeriod._PREVIOUS_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__2_, __FMBAS_TYPE__.MasterAgreementsPeriod._CURRENT__1_], labelWidth:130})
		.addDisplayFieldText({ name:"days", bind:"{d.days}", dataIndex:"days", maxLength:32, labelWidth:30})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("paymentTerms"),this._getConfig_("days"),this._getConfig_("paymentRefDate"),this._getConfig_("invoiceFreq"),this._getConfig_("curr")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("financialSourceCode"),this._getConfig_("avgMethodName"),this._getConfig_("period")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:1100, layout:"anchor"})
		.addPanel({ name:"col2", width:1100, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"]);
	},
	/* ==================== Business functions ==================== */
	
	_filterDistinct_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("financialSourceName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });
						}, this);
	},
	
	_filterDistinctAvg_: function(el) {
		
						var store = el.store;
						store.on("load", function(store) {
							store.clearFilter();
				            var hits = {};
				            store.filterBy(function(record) {
				                var name = record.get("avgMthdName");
				                if (hits[name]) {
				                    return false;
				                } else {
				                    hits[name] = true;
				                    return true;
				                }
				            });	
						}, this);
	},
	
	_enableFs_: function(el,newVal,oldVal) {
		
		
						if (el._fpRawValue_ != el._onFocusVal_) {
						var financialSourceCode = this._get_("financialSourceCode");
						var elVal = el.getValue(); 
						if (!Ext.isEmpty(elVal)) {
							financialSourceCode.setReadOnly(false);
						}
						else {
							financialSourceCode.setReadOnly(true);
						}
						financialSourceCode.setValue("");
						}
						
	},
	
	_enableAvgMethods_: function(el,newVal,oldVal) {
		
		
						if (el._fpRawValue_ != el._onFocusVal_) {
						var avgMethodName = this._get_("avgMethodName");
						var elVal = el.getValue(); 
						if (!Ext.isEmpty(elVal)) {
							avgMethodName.setReadOnly(false);
						}
						else {
							avgMethodName.setReadOnly(true);
						}
						avgMethodName.setValue("");
						}
	},
	
	_enableIfFinancialSource_: function() {
		
						
						var dc = this._controller_;
						var record = dc.getRecord();
						var result = false;
		
						if (record) {
							var financialSourceCode = record.get("financialSourceCode");
							if (!Ext.isEmpty(financialSourceCode)) {
								result = true;
							}
						}
		
						return result;
	},
	
	_filterInvoiceFreq_: function(el) {
		
						var s = el.store;
						s.filter(function(r) {
							var value = r.get("field1");
							if ( value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_ && value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_ 
								&& value !== __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_ && value !== __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_) {
								return value;
							}
						});
	}
});

/* ================= GRID: Temp ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelQuoteLocation_Dc$Temp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuoteLocation_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btn", scope: this, handler: this._wdwShow, text: "...", noInsert:true})
		.addButton({name:"btnSelectMarkup", scope: this, handler: this._wdwShow1, text: "...", noInsert:true, _enableFn_:this._hasSupplier_})
		.addTextField({ name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", noEdit:true , width:200, maxLength:32, labelWidth:80})
		.addLov({name:"location", bind:"{d.locCode}", dataIndex:"locCode", allowBlank:false, width:180, xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:80,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ],listeners:{
			change:{scope:this, fn:this.changeLocation},
			select:{scope:this, fn:this.changeLocation},
			blur:{scope:this, fn:this.locationBlur}
		}})
		.addCombo({ xtype:"combo", name:"flightType", bind:"{d.flightType}", dataIndex:"flightType", allowBlank:false, width:200, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:80})
		.addCombo({ xtype:"combo", name:"operationalType", bind:"{d.operationalType}", dataIndex:"operationalType", allowBlank:false, width:240, store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_], labelWidth:120})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", _enableFn_: this._countFlightEvents_, width:130, sysDec:"dec_unit", maxLength:19, labelWidth:60, decimals:2})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addNumberField({name:"events", bind:"{d.events}", dataIndex:"events", _enableFn_: this._countFlightEvents_, width:120, maxLength:11, labelWidth:50})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.product}", dataIndex:"product", width:160, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_], labelWidth:60})
		.addDateField({name:"fuelReleaseStartsOn", bind:"{d.fuelReleaseStartsOn}", dataIndex:"fuelReleaseStartsOn", allowBlank:false, width:200, labelWidth:80})
		.addDateField({name:"fuelReleaseEndsOn", bind:"{d.fuelReleaseEndsOn}", dataIndex:"fuelReleaseEndsOn", allowBlank:false, width:180, labelWidth:70})
		.addTextField({ name:"supplier", bind:"{d.suppCode}", dataIndex:"suppCode", width:200, maxLength:32, labelWidth:80})
		.addTextField({ name:"contract", bind:"{d.contractCode}", dataIndex:"contractCode", noEdit:true , width:190, maxLength:32, labelWidth:90})
		.addTextField({ name:"priceBasis", bind:"{d.priceBasis}", dataIndex:"priceBasis", noEdit:true , width:180, maxLength:32, labelWidth:80})
		.addTextField({ name:"quotation", bind:"{d.quotationName}", dataIndex:"quotationName", noEdit:true , width:200, maxLength:100, labelWidth:80})
		.addTextField({ name:"contractIndexOffset", bind:"{d.indexOffset}", dataIndex:"indexOffset", noEdit:true , width:160, maxLength:32, labelWidth:60})
		.addNumberField({name:"contractFuelPrice", bind:"{d.fuelPrice}", dataIndex:"fuelPrice", noEdit:true , width:170, sysDec:"dec_crncy", maxLength:19, labelWidth:60})
		.addTextField({ name:"settlementCurrencyCode", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:50, maxLength:3, hideLabel:"true"})
		.addTextField({ name:"settlementUnitCode", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:50, maxLength:2, hideLabel:"true"})
		.addTextField({ name:"iplAgent", bind:"{d.iplAgentCode}", dataIndex:"iplAgentCode", noEdit:true , width:200, maxLength:32, labelWidth:80})
		.addTextField({ name:"deliveryMethod", bind:"{d.deliveryMethod}", dataIndex:"deliveryMethod", noEdit:true , width:228, maxLength:32, labelWidth:128})
		.addDisplayFieldText({ name:"text", bind:"{d.fuelReleaseVP}", dataIndex:"fuelReleaseVP", maxLength:64, labelWidth:170})
		.addDisplayFieldText({ name:"text1", bind:"{d.supplyInfo}", dataIndex:"supplyInfo", maxLength:64, labelWidth:120})
		.addDisplayFieldText({ name:"text2", bind:"{d.pricingInfo}", dataIndex:"pricingInfo", maxLength:64, labelWidth:120})
		.addBooleanField({ name:"selectPricePolicy", bind:"{d.selectPricePolicy}", dataIndex:"selectPricePolicy", fieldCls:"x-form-radio", hideLabel:"true"})
		.addBooleanField({ name:"capturePricePolicy", bind:"{d.capturePricePolicy}", dataIndex:"capturePricePolicy", fieldCls:"x-form-radio", hideLabel:"true"})
		.addDisplayFieldText({ name:"selectedPP", bind:"{d.selectedPP}", dataIndex:"selectedPP", width:395, maxLength:64, labelWidth:130})
		.addDisplayFieldText({ name:"capturedPP", bind:"{d.capturedPP}", dataIndex:"capturedPP", width:300, maxLength:64, labelWidth:220})
		.addDisplayFieldText({ name:"teext", bind:"{d.teext}", dataIndex:"teext", width:22, maxLength:16, hideLabel:"true"})
		.addNumberField({name:"sellingPrice", bind:"{d.sellingPrice}", dataIndex:"sellingPrice", noEdit:true , width:300, sysDec:"dec_crncy", maxLength:21, labelWidth:200})
		.addTextField({ name:"sellingUnit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:40, maxLength:2, hideLabel:"true"})
		.addTextField({ name:"sellingCurrency", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:40, maxLength:3, hideLabel:"true"})
		.addNumberField({name:"selectedMarkup", bind:"{d.selectedMarkup}", dataIndex:"selectedMarkup", noEdit:true , width:170, sysDec:"dec_crncy", maxLength:21, labelWidth:78})
		.addTextField({ name:"selectedMarkupCurrency", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:40, maxLength:3, hideLabel:"true"})
		.addTextField({ name:"selectedMarkupUnit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:40, maxLength:2, hideLabel:"true"})
		.addNumberField({name:"capturedMarkup", bind:"{d.capturedMarkup}", dataIndex:"capturedMarkup", _enableFn_: this._hasSupplier_, width:140, sysDec:"dec_crncy", maxLength:21, labelWidth:50})
		.addTextField({ name:"capturedMarkupUnit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:40, maxLength:2, hideLabel:"true"})
		.addTextField({ name:"capturedMarkupCurrency", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:40, maxLength:3, hideLabel:"true"})
		.addDisplayFieldText({ name:"pricePolicy", bind:"{d.pricePolicy}", dataIndex:"pricePolicy", maxLength:64, labelWidth:270})
		.addNumberField({name:"capturedMargin", bind:"{d.capturedMargin}", dataIndex:"capturedMargin", _enableFn_: this._hasSupplier_, width:140, sysDec:"dec_crncy", maxLength:19, labelWidth:50})
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", maxLength:1, labelWidth:20})
		.addBooleanField({ name:"selectMarkup", bind:"{d.selectMarkup}", dataIndex:"selectMarkup", noLabel: true, fieldCls:"x-form-radio", style:"margin-left: 163px",listeners:{
			change:{scope:this, fn:this._reloadFuelLocRecod_}
		}})
		.addBooleanField({ name:"selectMargin", bind:"{d.selectMargin}", dataIndex:"selectMargin", noLabel: true, fieldCls:"x-form-radio", style:"margin-left: 175px"})
		.addDisplayFieldText({ name:"sep", bind:"{d.separator}", dataIndex:"separator", noEdit:true , maxLength:16, labelWidth:1300})
		.addDisplayFieldText({ name:"sep1", bind:"{d.separator}", dataIndex:"separator", noEdit:true , maxLength:16, labelWidth:1300})
		.addDisplayFieldText({ name:"sep2", bind:"{d.separator}", dataIndex:"separator", noEdit:true , maxLength:16, labelWidth:1300})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("customer"),this._getConfig_("location"),this._getConfig_("flightType"),this._getConfig_("operationalType"),this._getConfig_("product"),this._getConfig_("quantity"),this._getConfig_("unit"),this._getConfig_("events")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("fuelReleaseStartsOn"),this._getConfig_("fuelReleaseEndsOn")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("supplier"),this._getConfig_("btn"),this._getConfig_("contract"),this._getConfig_("priceBasis"),this._getConfig_("quotation"),this._getConfig_("contractIndexOffset"),this._getConfig_("contractFuelPrice"),this._getConfig_("settlementCurrencyCode"),this._getConfig_("settlementUnitCode")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("iplAgent"),this._getConfig_("deliveryMethod")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("teext"),this._getConfig_("selectPricePolicy"),this._getConfig_("selectedPP"),this._getConfig_("capturePricePolicy"),this._getConfig_("capturedPP"),this._getConfig_("sellingPrice"),this._getConfig_("sellingCurrency"),this._getConfig_("sellingUnit")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("selectedMarkup"),this._getConfig_("selectedMarkupCurrency"),this._getConfig_("selectedMarkupUnit"),this._getConfig_("btnSelectMarkup"),this._getConfig_("selectMarkup"),this._getConfig_("capturedMarkup"),this._getConfig_("capturedMarkupCurrency"),this._getConfig_("capturedMarkupUnit")]})
		.add({name:"row7", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("pricePolicy"),this._getConfig_("selectMargin"),this._getConfig_("capturedMargin"),this._getConfig_("percent")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:1360, layout:"anchor"})
		.addPanel({ name:"col2", width:1300, layout:"anchor"})
		.addPanel({ name:"col3", width:1360, layout:"anchor"})
		.addPanel({ name:"col4", width:1360, layout:"anchor"})
		.addPanel({ name:"col5", width:1300, layout:"anchor"})
		.addPanel({ name:"col6", width:1360, layout:"anchor"})
		.addPanel({ name:"col7", width:1360, layout:"anchor"})
		.addPanel({ name:"col8", width:1360, layout:"anchor"})
		.addPanel({ name:"col9", width:1300, layout:"anchor"})
		.addPanel({ name:"col10", width:1360, layout:"anchor"})
		.addPanel({ name:"col11", width:1360, layout:"anchor"})
		.addPanel({ name:"col12", width:1360, layout:"anchor"})
		.addPanel({ name:"col13", width:1360, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8", "col9", "col10", "col11", "col12", "col13"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["sep"])
		.addChildrenTo("col3", ["text"])
		.addChildrenTo("col4", ["row2"])
		.addChildrenTo("col5", ["sep1"])
		.addChildrenTo("col6", ["text1"])
		.addChildrenTo("col7", ["row3"])
		.addChildrenTo("col8", ["row4"])
		.addChildrenTo("col9", ["sep2"])
		.addChildrenTo("col10", ["text2"])
		.addChildrenTo("col11", ["row5"])
		.addChildrenTo("col12", ["row6"])
		.addChildrenTo("col13", ["row7"]);
	},
	/* ==================== Business functions ==================== */
	
	_isInArray_: function(value,array) {
		
					    return array.indexOf(value) > -1;
	},
	
	_countFlightEvents_: function() {
		
						var dc = this._controller_;
						var record = dc.getRecord();
						var isEnabled = record.data.isFlightEvent;
						return !isEnabled;
	},
	
	_hasSupplier_: function() {
		
						var supplier = this._get_("supplier");
						var supplierValue = supplier.getValue();
						var result = "";
						if (supplierValue == false) {
							result = false;
						}
						else {
							result = true;
						}
						return result;
	},
	
	_disableSelectFields_: function() {
		
		
						var fields = ["btnSelectMarkup"];
						
						Ext.each(fields, function(field) {
						        Ext.suspendLayouts();
						        var fieldCmp = this._get_(field);
						        if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
						            fieldCmp.setDisabled(true);
						        } else {
						            fieldCmp.setReadOnly(true);
						        }
						        Ext.resumeLayouts(true);
						}, this);
	},
	
	_disableCaptureFields_: function() {
		
		
						var fields = ["selectMarkup","selectMargin","capturedMarkup","capturedMargin"];
						
						Ext.each(fields, function(field) {
						        Ext.suspendLayouts();
						        var fieldCmp = this._get_(field);
						        if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
						            fieldCmp.setDisabled(true);
						        } else {
						            fieldCmp.setReadOnly(true);
						        }
						        Ext.resumeLayouts(true);
						}, this);
	},
	
	_enableSelectFields_: function() {
		
		
						var fields = ["btnSelectMarkup"];
		
							Ext.each(fields, function(field) {
								var fieldCmp = this._get_(field);
								fieldCmp.suspendLayout = true;
								if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
									fieldCmp.setDisabled(false);
								}
								else {
									fieldCmp.setReadOnly(false);
								}
								fieldCmp.suspendLayout = false;
									
							},this);				
	},
	
	_enableMargin_: function() {
		
		
						var fields = ["capturedMargin"];
		
							Ext.each(fields, function(field) {
								var fieldCmp = this._get_(field);
								fieldCmp.suspendLayout = true;
								if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
									fieldCmp.setDisabled(false);
								}
								else {
									fieldCmp.setReadOnly(false);
								}
								fieldCmp.suspendLayout = false;
									
							},this);				
	},
	
	_disableMargin_: function() {
		
		
						var fields = ["capturedMargin"];
		
							Ext.each(fields, function(field) {
								var fieldCmp = this._get_(field);
								fieldCmp.suspendLayout = true;
								if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
									fieldCmp.setDisabled(true);
								}
								else {
									fieldCmp.setReadOnly(true);
								}
								fieldCmp.suspendLayout = false;
									
							},this);				
	},
	
	_enableMarkup_: function() {
		
		
						var field = "capturedMarkup";
						var fieldCmp = this._get_(field);
								if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
									fieldCmp.setDisabled(false);
								}
								else {
									fieldCmp.setReadOnly(false);
								}				
	},
	
	_disableMarkup_: function() {
		
		
						var field = "capturedMarkup";
						var fieldCmp = this._get_(field);
								if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
									fieldCmp.setDisabled(true);
								}
								else {
									fieldCmp.setReadOnly(true);
								}
		
										
	},
	
	_enableCaptureFields_: function() {
		
		
							var fields = ["selectMarkup","selectMargin","capturedMarkup","capturedMargin"];
		
							var selectMarkupValue = this._get_("selectMarkup").checked;
							var selectMarginValue = this._get_("selectMargin").checked;
		
							Ext.each(fields, function(field) {
								var fieldCmp = this._get_(field);
								fieldCmp.suspendLayout = true;
								if (fieldCmp.name == "capturedMarkup" && selectMarkupValue == true) {
									if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
										fieldCmp.setDisabled(false);
									}
									else {
										fieldCmp.setReadOnly(false);
									}
								}
								else if (fieldCmp.name == "capturedMargin" && selectMarginValue == true) {
									if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
										fieldCmp.setDisabled(false);
									}
									else {
										fieldCmp.setReadOnly(false);
									}
								}
								else {
									if (fieldCmp.xtype == "checkbox" || fieldCmp.xtype == "button") {
										fieldCmp.setDisabled(false);
									}
									else {
										fieldCmp.setReadOnly(true);
									}
								}
								fieldCmp.suspendLayout = false;
							},this);				
	},
	
	_reloadFuelLocRecod_: function() {
		
		
						var dc = this._controller_;
						var selectPricePolicy = this._get_("selectPricePolicy");
						var capturePricePolicy = this._get_("capturePricePolicy");
						var selectMarkup = this._get_("selectMarkup");
						var selectMargin = this._get_("selectMargin");
						var supplier = this._get_("supplier");
		
						if (supplier.getValue() != "") {
							capturePricePolicy.setDisabled(false);
							selectPricePolicy.setDisabled(false);
							
							
						}
						
						if (capturePricePolicy.checked == true) {
							if (supplier.getValue() != "") {
								selectMarkup.setDisabled(true);
								selectMargin.setDisabled(true);
								this._disableSelectFields_();
								if (dc.readOnly == false) {
									this._enableCaptureFields_();
								}
							}
							else {
								this._disableSelectFields_();
								this._disableCaptureFields_();
							}
						}
		
						if (selectPricePolicy.checked == true) {
							
							if (supplier.getValue() != "") {
		
								this._disableCaptureFields_();
								if (dc.readOnly == false) {
									this._enableSelectFields_();
								}
							}
							else {
								this._disableSelectFields_();
								this._disableCaptureFields_();
							}
							
						}
						
			
	},
	
	_afterApplyStates_: function() {
		
		
						var dc = this._controller_;
						var selectPricePolicy = this._get_("selectPricePolicy");
						var capturePricePolicy = this._get_("capturePricePolicy");
						var selectMarkup = this._get_("selectMarkup");
						var selectMargin = this._get_("selectMargin");
						var supplier = this._get_("supplier");
		
		
		
						if (dc.readOnly == true) {
							this._get_("btnSelectMarkup").setDisabled(true);
							this._get_("btn").setDisabled(true);
						}
		
						if (supplier.getValue() == "") {
							capturePricePolicy.setDisabled(true);
							selectPricePolicy.setDisabled(true);
							selectMarkup.setDisabled(true);
							selectMargin.setDisabled(true);
						}
						else {
							capturePricePolicy.setDisabled(false);
							selectPricePolicy.setDisabled(false);
							selectMarkup.setDisabled(false);
							selectMargin.setDisabled(false);
						}
		
		
		
						if (capturePricePolicy.checked == true) {
							if (supplier.getValue() != "") {
								this._disableSelectFields_();
								if (dc.readOnly == false) {
									this._enableCaptureFields_();
								}
							}
							else {
								this._disableSelectFields_();
								this._disableCaptureFields_();
							}
						}
		
						if (selectPricePolicy.checked == true) {
							if (supplier.getValue() != "") {
								this._disableCaptureFields_();
								if (dc.readOnly == false) {
									this._enableSelectFields_();
								}
							}
							else {
								this._disableSelectFields_();
								this._disableCaptureFields_();
							}
							
						}
		//
		//				// Radio change events
		//
						selectPricePolicy.on("change", function() {
						
							if (selectPricePolicy.checked == false) {
								if (dc.readOnly == false) {
									this._enableCaptureFields_();
								}
								capturePricePolicy.setValue(true);
							}
							else {
								this._disableCaptureFields_();
								capturePricePolicy.setValue(false);
							}
						},this);
		
						capturePricePolicy.on("change", function() {
		
							if (capturePricePolicy.checked == false) {
								if (dc.readOnly == false) {
									this._enableSelectFields_();
								}
								selectPricePolicy.setValue(true);
							}
							else {
								this._disableSelectFields_();
								selectPricePolicy.setValue(false);
							}
						},this);
		
						selectMarkup.on("change", function() {
		
							if (selectMarkup.checked == false) {
								if (dc.readOnly == false) {
									this._enableMargin_();
								}
								selectMargin.setValue(true);
							}
							else {
								this._disableMargin_();
								selectMargin.setValue(false);
							}
						},this);
		
						selectMargin.on("change", function() {
		
							if (selectMargin.checked == false) {
								if (dc.readOnly == false) {
									this._enableMarkup_();
								}
								selectMarkup.setValue(true);
							}
							else {
								this._disableMarkup_();
								selectMarkup.setValue(false);
							}
						},this);
		
						
		
	},
	
	_wdwShow: function() {
		
					this._controller_.fireEvent("showWindow", this);
	},
	
	_wdwShow1: function() {
		
					this._controller_.fireEvent("showWindow1", this);
	},
	
	changeLocation: function(field,oldValue,newValue) {
		
						this._locationChanged_ = false;
		
						if (this._controller_.dcState.isDirty == true) {
							this._locationChanged_  = true;
						}
					    
						
	},
	
	locationBlur: function(field) {
		
		
						if (this._locationChanged_ === true ) {
							var supplier = this._get_("supplier");
							var supplierValue = supplier.getValue();
							var result = "";
							if (supplierValue == false) {
								result = false;
							}
							else {
								this._controller_.fireEvent("locationChange", this);
								Main.warning("Please review supply info.");
							}
						}
						this._locationChanged_ = false;
	}
});

/* ================= EDIT FORM: SupplySelect ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc$SupplySelect", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuoteLocation_Dc$SupplySelect",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", noEdit:true , width:170, maxLength:32})
		.addLov({name:"location", bind:"{d.locCode}", dataIndex:"locCode", noEdit:true , width:200, xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ]})
		.addCombo({ xtype:"combo", name:"flightType", bind:"{d.flightType}", dataIndex:"flightType", noEdit:true , width:220, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:120})
		.addCombo({ xtype:"combo", name:"operationalType", bind:"{d.operationalType}", dataIndex:"operationalType", noEdit:true , width:220, store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_], labelWidth:110})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", noEdit:true , width:170, maxLength:4})
		.addCombo({ xtype:"combo", name:"paymentRefDate", bind:"{d.paymentRefDate}", dataIndex:"paymentRefDate", noEdit:true , width:200, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_], labelWidth:100})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFreq}", dataIndex:"invoiceFreq", noEdit:true , width:220, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], labelWidth:120})
		.addLov({name:"curr", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:230, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:180,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:50, noLabel: true, xtype:"fmbas_UnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addDateField({name:"fuelReleaseStartsOn", bind:"{d.fuelReleaseStartsOn}", dataIndex:"fuelReleaseStartsOn", noEdit:true , width:200})
		.addDateField({name:"fuelReleaseEndsOn", bind:"{d.fuelReleaseEndsOn}", dataIndex:"fuelReleaseEndsOn", noEdit:true , width:170, labelWidth:70})
		.addDisplayFieldText({ name:"text", bind:"{d.fuelReleaseVP}", dataIndex:"fuelReleaseVP", maxLength:64, labelAlign:"left", labelWidth:200})
		.addDisplayFieldText({ name:"text1", bind:"{d.supplyOpt}", dataIndex:"supplyOpt", maxLength:64, labelAlign:"left", labelWidth:200})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("customer"),this._getConfig_("location"),this._getConfig_("flightType"),this._getConfig_("operationalType"),this._getConfig_("curr"),this._getConfig_("unit")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("paymentTerms"),this._getConfig_("paymentRefDate"),this._getConfig_("invoiceFreq")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("fuelReleaseStartsOn"),this._getConfig_("fuelReleaseEndsOn")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:1200, layout:"anchor"})
		.addPanel({ name:"col2", width:1200, layout:"anchor"})
		.addPanel({ name:"col3", width:400, layout:"anchor"})
		.addPanel({ name:"col4", width:1200, layout:"anchor"})
		.addPanel({ name:"col5", width:400, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col3", ["text"])
		.addChildrenTo("col4", ["row3"])
		.addChildrenTo("col5", ["text1"]);
	}
});

/* ================= EDIT FORM: PricingSelect ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc$PricingSelect", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuoteLocation_Dc$PricingSelect",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", noEdit:true , width:170, maxLength:32})
		.addLov({name:"location", bind:"{d.locCode}", dataIndex:"locCode", noEdit:true , width:200, xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ]})
		.addCombo({ xtype:"combo", name:"flightType", bind:"{d.flightType}", dataIndex:"flightType", noEdit:true , width:220, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:120})
		.addCombo({ xtype:"combo", name:"operationalType", bind:"{d.operationalType}", dataIndex:"operationalType", noEdit:true , width:220, store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_], labelWidth:110})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", noEdit:true , width:170, maxLength:4})
		.addCombo({ xtype:"combo", name:"paymentRefDate", bind:"{d.paymentRefDate}", dataIndex:"paymentRefDate", noEdit:true , width:200, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_], labelWidth:100})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFreq}", dataIndex:"invoiceFreq", noEdit:true , width:220, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], labelWidth:120})
		.addLov({name:"curr", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:230, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:180,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:50, noLabel: true, xtype:"fmbas_UnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addDateField({name:"fuelReleaseStartsOn", bind:"{d.fuelReleaseStartsOn}", dataIndex:"fuelReleaseStartsOn", noEdit:true , width:200})
		.addDateField({name:"fuelReleaseEndsOn", bind:"{d.fuelReleaseEndsOn}", dataIndex:"fuelReleaseEndsOn", noEdit:true , width:170, labelWidth:70})
		.addTextField({ name:"supplier", bind:"{d.suppCode}", dataIndex:"suppCode", noEdit:true , width:200, maxLength:32})
		.addTextField({ name:"contract", bind:"{d.contractCode}", dataIndex:"contractCode", noEdit:true , width:200, maxLength:32, labelWidth:70})
		.addTextField({ name:"priceBasis", bind:"{d.priceBasis}", dataIndex:"priceBasis", noEdit:true , width:220, maxLength:32, labelWidth:90})
		.addTextField({ name:"quotation", bind:"{d.quotationName}", dataIndex:"quotationName", noEdit:true , width:200, maxLength:100})
		.addTextField({ name:"contractIndexOffset", bind:"{d.indexOffset}", dataIndex:"indexOffset", noEdit:true , width:170, maxLength:32, labelWidth:80})
		.addNumberField({name:"contractFuelPrice", bind:"{d.fuelPrice}", dataIndex:"fuelPrice", noEdit:true , width:150, sysDec:"dec_crncy", maxLength:19})
		.addTextField({ name:"settlementCurrencyCode", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:40, maxLength:3, hideLabel:"true"})
		.addTextField({ name:"settlementUnitCode", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:40, maxLength:2, hideLabel:"true"})
		.addDisplayFieldText({ name:"text", bind:"{d.fuelReleaseVP}", dataIndex:"fuelReleaseVP", maxLength:64, labelAlign:"left", labelWidth:200})
		.addDisplayFieldText({ name:"text1", bind:"{d.supplyInfo}", dataIndex:"supplyInfo", maxLength:64, labelAlign:"left", labelWidth:200})
		.addDisplayFieldText({ name:"text2", bind:"{d.pricingOpt}", dataIndex:"pricingOpt", maxLength:64, labelAlign:"left", labelWidth:200})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("customer"),this._getConfig_("location"),this._getConfig_("flightType"),this._getConfig_("operationalType"),this._getConfig_("curr"),this._getConfig_("unit")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("paymentTerms"),this._getConfig_("paymentRefDate"),this._getConfig_("invoiceFreq")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("fuelReleaseStartsOn"),this._getConfig_("fuelReleaseEndsOn")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("supplier"),this._getConfig_("contract"),this._getConfig_("priceBasis"),this._getConfig_("quotation"),this._getConfig_("contractIndexOffset")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("contractFuelPrice"),this._getConfig_("settlementCurrencyCode"),this._getConfig_("settlementUnitCode")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:1200, layout:"anchor"})
		.addPanel({ name:"col2", width:1200, layout:"anchor"})
		.addPanel({ name:"col3", width:1200, layout:"anchor"})
		.addPanel({ name:"col4", width:1200, layout:"anchor"})
		.addPanel({ name:"col5", width:1200, layout:"anchor"})
		.addPanel({ name:"col6", width:1200, layout:"anchor"})
		.addPanel({ name:"col7", width:1200, layout:"anchor"})
		.addPanel({ name:"col8", width:1200, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col3", ["text"])
		.addChildrenTo("col4", ["row3"])
		.addChildrenTo("col5", ["text1"])
		.addChildrenTo("col6", ["row4"])
		.addChildrenTo("col7", ["row5"])
		.addChildrenTo("col8", ["text2"]);
	}
});

/* ================= EDIT FORM: Disclaimer ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc$Disclaimer", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuoteLocation_Dc$Disclaimer",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addHtmlEditor({ name:"disclaimer", bind:{value:"{d.disclaimer}"}, dataIndex:"disclaimer", hideLabel:"true"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["disclaimer"]);
	}
});
