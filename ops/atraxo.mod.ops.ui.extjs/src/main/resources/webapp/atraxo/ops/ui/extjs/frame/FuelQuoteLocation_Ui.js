/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.frame.FuelQuoteLocation_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.FuelQuoteLocation_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("fuelQuoteLoc", Ext.create(atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc,{}))
		.addDc("contract", Ext.create(atraxo.ops.ui.extjs.dc.CustomerLocationContracts_Dc,{}))
		.addDc("pricePolicies", Ext.create(atraxo.ops.ui.extjs.dc.PricePolicies_Dc,{}))
		.addDc("flightEvent", Ext.create(atraxo.ops.ui.extjs.dc.FlightEvent_Dc,{}))
		.addDc("priceCat", Ext.create(atraxo.ops.ui.extjs.dc.PriceCategory_Dc,{}))
		.addDc("priceRest", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceRestriction_Dc,{multiEdit: true,  readOnly:true}))
		.addDc("contractPriceCmpn", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponent_Dc,{ readOnly:true}))
		.addDc("contractPriceCmpnConv", Ext.create(atraxo.cmm.ui.extjs.dc.ContractPriceComponentConv_Dc,{ readOnly:true}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.linkDc("contract", "fuelQuoteLoc",{fields:[
					{childField:"locId", parentField:"locId"}, {childParam:"startsOn", parentField:"fuelReleaseStartsOn"}, {childParam:"endsOn", parentField:"fuelReleaseEndsOn"}, {childParam:"fqlUnit", parentField:"unitId"}, {childParam:"fqlCurrency", parentField:"currId"}, {childParam:"avgMthId", parentField:"averageMethodId"}, {childParam:"finSrcId", parentField:"financialSourceId"}, {childParam:"period", parentField:"period"}]})
				.linkDc("pricePolicies", "fuelQuoteLoc",{fields:[
					{childField:"active", value:"true"}, {childParam:"dft", parentField:"dft"}, {childParam:"productPrice", parentField:"basePrice"}, {childParam:"fuelPrice", parentField:"fuelPrice"}, {childParam:"fqLocationId", parentField:"locId"}, {childParam:"fqCustomerId", parentField:"customerId"}, {childParam:"unitCode", parentField:"unitCode"}, {childParam:"unitId", parentField:"unitId"}, {childParam:"currencyCode", parentField:"currCode"}, {childParam:"currecnyId", parentField:"currId"}, {childParam:"finSrcId", parentField:"financialSourceId"}, {childParam:"avgMthdId", parentField:"averageMethodId"}, {childParam:"period", parentField:"period"}]})
				.linkDc("flightEvent", "fuelQuoteLoc",{fetchMode:"auto",fields:[
					{childField:"locQuoteId", parentField:"id"}, {childField:"eventTp", parentField:"flightType"}, {childField:"operationTp", parentField:"operationalType"}]})
				.linkDc("priceCat", "fuelQuoteLoc",{fetchMode:"auto",reloadChildrenOnParentUpdate:true,fields:[
					{childField:"contractId", parentField:"contractId"}, {childParam:"settlementCurrencyCode", parentField:"currCode"}, {childParam:"settlementUnitCode", parentField:"unitCode"}]})
				.linkDc("priceRest", "priceCat",{fetchMode:"auto",fields:[
					{childField:"contractPriceCtgry", parentField:"id"}]})
				.linkDc("contractPriceCmpn", "priceCat",{fetchMode:"auto",fields:[
					{childField:"contractPriceCat", parentField:"id"}, {childField:"priceCtgryName", parentField:"priceCtgryName", noFilter:true}]})
				.linkDc("contractPriceCmpnConv", "priceCat",{fetchMode:"auto",fields:[
					{childField:"contractPriceCtgry", parentField:"id"}]})
				.linkDc("history", "fuelQuoteLoc",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "fuelQuoteLoc",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"btnSelect",glyph:fp_asc.list_glyph.glyph,iconCls: fp_asc.list_glyph.css, disabled:true, handler: this.onBtnSelect,stateManager:[{ name:"selected_one", dc:"contract"}], scope:this})
		.addButton({name:"btnSelectPolicy",glyph:fp_asc.list_glyph.glyph,iconCls: fp_asc.list_glyph.css, disabled:true, handler: this.onBtnSelectPolicy,stateManager:[{ name:"selected_one", dc:"pricePolicies"}], scope:this})
		.addButton({name:"btnSuppCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnSuppCancel, scope:this})
		.addButton({name:"btnPolicyCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnPolicyCancel, scope:this})
		.addButton({name:"btnFlightEventCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnFlightEventCancel, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNew",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNew, scope:this})
		.addButton({name:"priceCatHelp",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onPriceCatHelp, scope:this})
		.addButton({name:"btnUpdOrDel",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnUpdOrDel,stateManager:[{ name:"selected_not_zero", dc:"flightEvent", and: function() {return (this._getDc_("flightEvent").params.get("isEnabled") == true);} }], scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addDcFormView("fuelQuoteLoc", {name:"Edit", xtype:"ops_FuelQuoteLocation_Dc$Edit"})
		.addDcGridView("flightEvent", {name:"FlightEvents", _hasTitle_:true, xtype:"ops_FlightEvent_Dc$List"})
		.addDcFormView("flightEvent", {name:"FlightEventsPopUp", xtype:"ops_FlightEvent_Dc$QuotationEdit"})
		.addDcFormView("flightEvent", {name:"FlightEventsEdit", xtype:"ops_FlightEvent_Dc$QuotationEdit"})
		.addDcFormView("fuelQuoteLoc", {name:"PaymentTerms", _hasTitle_:true, xtype:"ops_FuelQuoteLocation_Dc$PaymentTerms"})
		.addDcFormView("fuelQuoteLoc", {name:"Disclaimer", _hasTitle_:true, xtype:"ops_FuelQuoteLocation_Dc$Disclaimer",  bodyPadding:"0"})
		.addDcGridView("fuelQuoteLoc", {name:"Documents", _hasTitle_:true, xtype:"ops_FuelQuoteLocation_Dc$Temp",  disabled:true})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"NotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("fuelQuoteLoc", {name:"locEdit", xtype:"ops_FuelQuoteLocation_Dc$SupplySelect"})
		.addDcGridView("contract", {name:"suppList", xtype:"ops_CustomerLocationContracts_Dc$SupplierContracts"})
		.addDcFormView("fuelQuoteLoc", {name:"priceLocEdit", xtype:"ops_FuelQuoteLocation_Dc$PricingSelect"})
		.addDcGridView("pricePolicies", {name:"pricePolicyList", xtype:"ops_PricePolicies_Dc$PricePolicies"})
		.addDcGridView("priceCat", {name:"PriceCat", xtype:"ops_PriceCategory_Dc$PriceCategoryList"})
		.addDcFilterFormView("priceCat", {name:"PriceCatFilter", xtype:"ops_PriceCategory_Dc$Filter"})
		.addDcFormView("priceCat", {name:"priceCategoryDetails", xtype:"ops_PriceCategory_Dc$PriceCategoryDetails"})
		.addDcEditGridView("priceRest", {name:"priceRest", _hasTitle_:true, height:"30%", xtype:"cmm_ContractPriceRestriction_Dc$List", frame:true,  collapsible:true})
		.addDcGridView("contractPriceCmpn", {name:"priceCompList", _hasTitle_:true, xtype:"cmm_ContractPriceComponent_Dc$SimpleList"})
		.addDcFilterFormView("contractPriceCmpn", {name:"priceCompFilter", xtype:"cmm_ContractPriceComponent_Dc$Filter"})
		.addDcGridView("contractPriceCmpnConv", {name:"priceComponentConv", _hasTitle_:true, xtype:"cmm_ContractPriceComponentConv_Dc$List"})
		.addDcFilterFormView("contractPriceCmpnConv", {name:"priceComponentConvFilter", xtype:"cmm_ContractPriceComponentConv_Dc$Filter"})
		.addDcFormView("priceCat", {name:"totals", xtype:"ops_PriceCategory_Dc$Totals",  split:false, cls:"sone-total-panel", listeners:{afterrender: {scope: this, fn: function(view) {var grid = this._get_('PriceCat'); grid._totalPanelId_ = view.getId(); }}}})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"supplier", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"pricing", layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"priceComponentTab", height:"40%", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"prices", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwSupplier", _hasTitle_:true, width:1170, height:500, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("supplier")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSelect"), this._elems_.get("btnSuppCancel")]}]})
		.addWindow({name:"wdwFlightEvent", _hasTitle_:true, width:920, height:440, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("FlightEventsPopUp")],  listeners:{ close:{fn:this.onWdwflightEventClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveNew"), this._elems_.get("btnFlightEventCancel")]}]})
		.addWindow({name:"wdwPricing", _hasTitle_:true, width:1240, height:540, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("pricing")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSelectPolicy"), this._elems_.get("btnPolicyCancel")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NotesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas1", dcName:"fuelQuoteLoc"})
		
		.addTabPanelForUserFields({tabPanelName:"priceComponentTab", containerPanelName:"canvas3", dcName:"priceCat"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["Edit", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas2", ["FlightEventsEdit"], ["center"])
		.addChildrenTo("canvas3", ["priceCategoryDetails", "priceRest", "priceComponentTab"], ["north", "center", "south"])
		.addChildrenTo("supplier", ["locEdit", "suppList"], ["north", "center"])
		.addChildrenTo("pricing", ["priceLocEdit", "pricePolicyList"], ["north", "center"])
		.addChildrenTo("detailsTab", ["FlightEvents"])
		.addChildrenTo("priceComponentTab", ["priceCompList"])
		.addChildrenTo("prices", ["PriceCat", "totals"], ["center", "south"])
		.addToolbarTo("Edit", "tlbEdit")
		.addToolbarTo("FlightEvents", "tlbFlightEvent")
		.addToolbarTo("FlightEventsEdit", "tlbFlightEventEdit")
		.addToolbarTo("PriceCat", "tlbPriceCat")
		.addToolbarTo("priceCategoryDetails", "tlbPriceCatDetails")
		.addToolbarTo("NotesList", "tlbNoteList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3"])
		.addChildrenTo2("detailsTab", ["prices", "PaymentTerms", "Disclaimer", "NotesList", "History", "Documents"])
		.addChildrenTo2("priceComponentTab", ["priceComponentConv"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbEdit", {dc: "fuelQuoteLoc"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbFlightEvent", {dc: "flightEvent"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnUpdOrDel")])
			.addReports()
		.end()
		.beginToolbar("tlbFlightEventEdit", {dc: "flightEvent"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbPriceCat", {dc: "priceCat"})
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"})
			.addReports()
		.end()
		.beginToolbar("tlbPriceCatDetails", {dc: "priceCat"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("priceCatHelp")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"canvas1",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnSelect
	 */
	,onBtnSelect: function() {
		this.selectSupplier();
		this._getWindow_("wdwSupplier").close();
	}
	
	/**
	 * On-Click handler for button btnSelectPolicy
	 */
	,onBtnSelectPolicy: function() {
		this.selectPolicy();
		this._getWindow_("wdwPricing").close();
	}
	
	/**
	 * On-Click handler for button btnSuppCancel
	 */
	,onBtnSuppCancel: function() {
		this._getWindow_("wdwSupplier").close();
	}
	
	/**
	 * On-Click handler for button btnPolicyCancel
	 */
	,onBtnPolicyCancel: function() {
		this._getWindow_("wdwPricing").close();
	}
	
	/**
	 * On-Click handler for button btnFlightEventCancel
	 */
	,onBtnFlightEventCancel: function() {
		this.onWdwflightEventClose();
		this._getWindow_("wdwFlightEvent").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveNew
	 */
	,onBtnSaveNew: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button priceCatHelp
	 */
	,onPriceCatHelp: function() {
		
						var url = Main.urlHelp+"/FuelPriceQuotes.html";
						window.open( url, "SONE_Help");
	}
	
	/**
	 * On-Click handler for button btnUpdOrDel
	 */
	,onBtnUpdOrDel: function() {
		this.updateOrDelete();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	,updateOrDeleteRpcCall: function() {	
		var successFn = function() {
			this._getDc_("flightEvent").doQuery();
			this._getDc_("fuelQuoteLoc").doReloadRecord();
		};
		var o={
			name:"updateOrDelete",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("flightEvent").doRpcDataList(o);
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,onShowUi: function() {	
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelQuotation_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("fuelQuoteLoc").getRecord().get("id")
			},
			callback: function (params) {
				this.reloadFuelQuoteLoc(params);
			}
		});
	}
	
	,updateOrDelete: function() {
		
						var dc = this._getDc_("flightEvent");
						dc.setParamValue("src",__TYPES__.flightEventEditSrc.src.fuelQuote);
						this.updateOrDeleteRpcCall();
	}
	
	,_isInArray_: function(value,array) {
		
					    return array.indexOf(value) > -1;
	}
	
	,_when_called_for_details_: function(params) {
			
						var flightEvent = this._getDc_("flightEvent");
		
						var dc = this._getDc_("fuelQuoteLoc");
						var record = dc.getRecord();
						var noExecStatus = [
							__TYPES__.fuelQuoteLocation.status.closed, 
							__TYPES__.fuelQuoteLocation.status.canceled, 
							__TYPES__.fuelQuoteLocation.status.submitted, 
							__TYPES__.fuelQuoteLocation.status.ordered, 
							__TYPES__.fuelQuoteLocation.status.expired
						];
		
						var disclaimer = this._get_("Disclaimer")._get_("disclaimer");
						disclaimer.disabledCls = "x-form-readonly";
						
						dc.setFilterValue("id", params.fuelQuoteLocId);
						dc.doQuery();
						if (this._isInArray_(params.quoteStatus,noExecStatus) == true) {
							dc.setReadOnly(true);
							flightEvent.setReadOnly(true);
							flightEvent.setParamValue("isEnabled",false);					
						}
						else {
							dc.setReadOnly(false);
							flightEvent.setReadOnly(false);
							flightEvent.setParamValue("isEnabled",true);					
						}
		
						if (params.quoteStatus == __TYPES__.fuelQuoteLocation.status.ordered || params.quoteStatus == __TYPES__.fuelQuoteLocation.status.submitted) {
							disclaimer.setReadOnly(true);
						}
						else {
							disclaimer.setReadOnly(false);
						}
	}
	
	,_reloadFuelLocRecod_: function() {
		
							this._get_("Edit")._reloadFuelLocRecod_();
	}
	
	,_onShowSupplierWindow_: function() {
		
						var wdwSupplier = this._getWindow_("wdwSupplier");
						var btnSelect = Ext.ComponentQuery.query("[name=btnSelect]")[0];
						var supplierGrid = this._get_("suppList").getView();
						var store = this._getDc_("contract").store;
		
						store.on("load", function(ds) {
							if (store.getCount() == 0) {
								btnSelect.setDisabled(true);
							}
							else {
								btnSelect.setDisabled(false);
							}
						});
		
						supplierGrid.getSelectionModel().on("selectionchange", function(sm, selectedRows, opts) {
						    if (selectedRows.length == 1) {
								btnSelect.setDisabled(false);
							}
							else {
								btnSelect.setDisabled(true);
							}
						}, this);
		
	}
	
	,_afterDefineDcs_: function() {
		
		
					var fuelQuoteLoc = this._getDc_("fuelQuoteLoc");
					var contract = this._getDc_("contract");
					var flightEvent = this._getDc_("flightEvent");
									
					var note = this._getDc_("note");
		
					note.on("afterDoNew", function (dc) {
						dc.setEditMode();
					    this.showNoteWdw();
					}, this);
		
					note.on("OnEditIn", function(dc) {
						this._getWindow_("noteWdw").show();
					}, this);
					
					note.on("afterDoSaveSuccess", function(dc, ajaxResult) {
					    if (ajaxResult.options.options.doNewAfterSave === true) {
					        dc.doNew();
					    } else if (ajaxResult.options.options.doCloseNoteWindowAfterSave === true) {
					        this._getWindow_("noteWdw").close();
					    }
					}, this);
		
					fuelQuoteLoc.on("afterDoCancel", function (dc) { 
						dc.doQuery();
					} , this );
		
		
					fuelQuoteLoc.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						this._get_("Edit")._reloadFuelLocRecod_();
						var warningMsg = dc.getRecord().get("toleranceMessage");
						if (Ext.isEmpty(warningMsg)) {
							return;
						}
						var arrMsg = warningMsg.split("||");
						if (arrMsg.length == 2) {
							Main.warning(arrMsg[1]);
						}
					} , this );
		
		
					flightEvent.on("afterDoSaveSuccess", function (dc, ajaxResult) {
						var warningMsg = dc.getRecord().get("toleranceMessage");
						if (!Ext.isEmpty(warningMsg)) {
							var arrMsg = warningMsg.split("||");
							if (arrMsg.length == 2) {
								Main.warning(arrMsg[1]);
							}
						}
						
						if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwFlightEvent").close();
		
						} else if (ajaxResult.options.options.doNewAfterSave === true) {
							flightEvent.doNew();
						}
					} , this );
		
					flightEvent.on("afterDoCommitSuccess", function (dc, ajaxResult) {
						fuelQuoteLoc.doReloadRecord();
						this._get_("Edit")._applyStates_(fuelQuoteLoc.getRecord());
					} , this );
		
					fuelQuoteLoc.on("locationChange", function(dc) {
						this._getDc_("contract").dcContext._updateCtxData_("locationChange");
					} , this );
		
					fuelQuoteLoc.on("showWindow", function(dc) {
						this._getDc_("contract").doQuery();
						this._getWindow_("wdwSupplier").show(undefined, function(){
						 	this._onShowSupplierWindow_();
						},this);	
					} , this );
		
					fuelQuoteLoc.on("cancelEdit", function(dc) {
						dc._controller_.doCancel();
					} , this );
		
					fuelQuoteLoc.on("showWindow1", function(dc) {
						this._getDc_("pricePolicies").doQuery();
						this._getWindow_("wdwPricing").show();		
					} , this );
		
					flightEvent.on("afterDoNew", function(dc) {
						dc.setEditMode();
						this._getWindow_("wdwFlightEvent").show();
						var dst = this._getDc_("flightEvent").getRecord();
						var src = this._getDc_("fuelQuoteLoc").getRecord();
		
						dst.beginEdit();
		 
						dst.set("locCode", src.get("locCode"));
						dst.set("locId", src.get("locId"));				
						dst.set("operatorCode", src.get("customerCode"));
						dst.set("operatorId", src.get("customerId"));
						dst.set("eventType", src.get("flightType"));
						dst.set("operationType", src.get("operationalType"));
		
						dst.endEdit();
					} , this );
		
					fuelQuoteLoc.on("onEditOut", function (dc) {
					    this.onShowUi();
					}, this);			
		
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/FuelPriceQuotes.html";
					window.open( url, "SONE_Help");
	}
	
	,selectSupplier: function() {
		 
						var fqlRec = this._getDc_("fuelQuoteLoc").getRecord();
						var contractRec = this._getDc_("contract").getRecord();
		
						fqlRec.set("contractId", contractRec.get("id"));
		
						fqlRec.set("suppCode", contractRec.get("counterPartyCode"));
						fqlRec.set("suppId", contractRec.get("counterPartyId"));
		
						fqlRec.set("contractCode", contractRec.get("code"));
						fqlRec.set("basePrice", contractRec.get("basePrice"));
						fqlRec.set("fuelPrice", contractRec.get("fuelPrice"));
						fqlRec.set("intoplaneFee", contractRec.get("intoPlaneFee"));
						fqlRec.set("otherFees", contractRec.get("otherFees"));
						fqlRec.set("taxes", contractRec.get("taxes"));
						fqlRec.set("flightFee", contractRec.get("perFlightFee"));
						fqlRec.set("totalPricePerUom", contractRec.get("totalPrice"));
		
						fqlRec.set("priceBasis", contractRec.get("pricingBases"));
						fqlRec.set("pbId", contractRec.get("pricingBaseId"));
						fqlRec.set("deliveryMethod", contractRec.get("delivery"));
						fqlRec.set("contractIndexOffset", contractRec.get("exchangeRateOffset"));
						fqlRec.set("iplAgentCode", contractRec.get("iplAgentCode"));
						fqlRec.set("iplId", contractRec.get("iplAgentId"));
						fqlRec.set("quotationName", contractRec.get("quotationName"));
						fqlRec.set("quotationId", contractRec.get("quotationId"));
						fqlRec.set("dft", contractRec.get("dft"));
						this._getDc_("fuelQuoteLoc").doSave();
		
	}
	
	,selectPolicy: function() {
		
						var fqlRec = this._getDc_("fuelQuoteLoc").getRecord();
						var policyRec = this._getDc_("pricePolicies").getRecord();
						
						fqlRec.set("pricingPolicyId", policyRec.get("pricePolicyId"));
						fqlRec.set("differential", policyRec.get("markup"));
						fqlRec.set("selectedMarkup", policyRec.get("markup"));
						if ("Default margin" == policyRec.get("policyType")) {
							fqlRec.set("capturedMargin", policyRec.get("value"));
						} else {
							fqlRec.set("capturedMargin", null);
						}
						fqlRec.set("sellingPrice", policyRec.get("markup") + fqlRec.get("fuelPrice"));
						this._getDc_("fuelQuoteLoc").doSave();
		
						this._get_("Edit")._get_("pricePolicy").labelEl.update("<span> Price policy: "+policyRec.get("policyName")+", "+policyRec.get("policyType"));
						
		
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,onNoteWdwClose: function() {
		
						var dc = this._getDc_("note");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,saveClose: function() {
		
					var flightEvent = this._getDc_("flightEvent");
					flightEvent.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveEdit: function() {
		
					var flightEvent = this._getDc_("flightEvent");
					flightEvent.doSave({doNewAfterSave: true });
	}
	
	,onWdwflightEventClose: function() {
		
						var dc = this._getDc_("flightEvent");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
});
