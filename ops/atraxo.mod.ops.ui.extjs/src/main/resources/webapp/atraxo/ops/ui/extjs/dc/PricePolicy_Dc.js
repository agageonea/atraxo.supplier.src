/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.PricePolicy_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ops.ui.extjs.ds.PricePolicy_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.PricePolicy_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_PricePolicy_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"name", dataIndex:"name", maxLength:64,
				editor:{xtype:"atraxo.ops.ui.extjs.lov.PricePolicyName_Lov", selectOnFocus:true, maxLength:64}})
			.addLov({name:"customerCode", dataIndex:"customerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addLov({name:"locationCode", dataIndex:"locationCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "locationId"} ]}})
			.addLov({name:"acTypeCode", dataIndex:"acTypeCode", maxLength:4,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AcTypesLov_Lov", selectOnFocus:true, maxLength:4,
					retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ]}})
			.addNumberField({name:"defaultMargin", dataIndex:"defaultMargin", sysDec:"dec_prc", maxLength:19})
			.addNumberField({name:"defaultMarkup", dataIndex:"defaultMarkup", sysDec:"dec_unit", maxLength:19})
			.addLov({name:"currecies", dataIndex:"currCode", width:60, maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"id", dsField: "currId"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addLov({name:"unit", dataIndex:"unitCode", width:60, maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"id", dsField: "unitId"} ],
					filterFieldMapping: [{lovField:"unitTypeInd", value: "Volume"} ]}})
			.addDateField({name:"validFrom", dataIndex:"validFrom"})
			.addDateField({name:"validTo", dataIndex:"validTo"})
			.addBooleanField({ name:"active", dataIndex:"active"})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.PricePolicy_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_PricePolicy_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:100})
		.addTextColumn({ name:"customer", dataIndex:"customerCode", width:100})
		.addTextColumn({ name:"location", dataIndex:"locationCode", width:100})
		.addTextColumn({ name:"acType", dataIndex:"acTypeCode", width:100})
		.addNumberColumn({ name:"defaultMargin", dataIndex:"defaultMargin", width:120, sysDec:"dec_prc"})
		.addNumberColumn({ name:"defaultMarkup", dataIndex:"defaultMarkup", width:120, sysDec:"dec_unit"})
		.addTextColumn({ name:"curency", dataIndex:"currCode", width:80})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:50})
		.addDateColumn({ name:"validFrom", dataIndex:"validFrom", _mask_: Masks.DATE})
		.addDateColumn({ name:"validTo", dataIndex:"validTo", _mask_: Masks.DATE})
		.addTextColumn({ name:"yesNo", dataIndex:"yesNo", width:70})
		.addTextColumn({ name:"comments", dataIndex:"comments", width:120})
		.addNumberColumn({ name:"minimalMargin", dataIndex:"minimalMargin", hidden:true, width:120, sysDec:"dec_prc"})
		.addNumberColumn({ name:"minimalMarkup", dataIndex:"minimalMarkup", hidden:true, width:120, sysDec:"dec_unit"})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_afterDefineColumns_: function() {
		
						this._getBuilder_().merge("defaultMargin", { renderer: function(v, md, rec, ri, ci, store) {if (v){return v + " %"} else {return " "}}});
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.PricePolicy_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_PricePolicy_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "customerId"} ]})
		.addLov({name:"location", bind:"{d.locationCode}", dataIndex:"locationCode", xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "locationId"} ]})
		.addLov({name:"acType", bind:"{d.acTypeCode}", dataIndex:"acTypeCode", xtype:"fmbas_AcTypesLov_Lov", maxLength:4,
			retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", width:60, xtype:"fmbas_UnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ],
			filterFieldMapping: [{lovField:"unitTypeInd", value: "Volume"} ]})
		.addLov({name:"currecies", bind:"{d.currCode}", dataIndex:"currCode", width:60, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "currId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", allowBlank:false, maxLength:64, labelWidth:90})
		.addNumberField({name:"defaultMargin", bind:"{d.defaultMargin}", dataIndex:"defaultMargin", width:170, sysDec:"dec_prc", maxLength:19})
		.addNumberField({name:"minimalMargin", bind:"{d.minimalMargin}", dataIndex:"minimalMargin", width:150, sysDec:"dec_prc", maxLength:19, labelWidth:80})
		.addNumberField({name:"defaultMarkup", bind:"{d.defaultMarkup}", dataIndex:"defaultMarkup", width:170, sysDec:"dec_unit", maxLength:19})
		.addNumberField({name:"minimalMarkup", bind:"{d.minimalMarkup}", dataIndex:"minimalMarkup", width:170, sysDec:"dec_unit", maxLength:19})
		.addCombo({ xtype:"combo", name:"yesNo", bind:"{d.yesNo}", dataIndex:"yesNo", store:[ __CMM_TYPE__.YesNo._True_, __CMM_TYPE__.YesNo._False_], labelWidth:90})
		.addTextArea({ name:"comments", bind:"{d.comments}", dataIndex:"comments", width:400})
		.addDateField({name:"validFrom", bind:"{d.validFrom}", dataIndex:"validFrom", allowBlank:false, labelWidth:90})
		.addDateField({name:"validto", bind:"{d.validTo}", dataIndex:"validTo", allowBlank:false, labelWidth:70})
		.addDisplayFieldText({ name:"margin", bind:"{d.margin}", dataIndex:"margin", maxLength:4000, labelWidth:55})
		.addDisplayFieldText({ name:"markup", bind:"{d.markup}", dataIndex:"markup", maxLength:4000, labelWidth:60})
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", noUpdate:true, width:20, maxLength:1, labelWidth:20, readOnly:true})
		.addDisplayFieldText({ name:"percent2", bind:"{d.percent}", dataIndex:"percent", noUpdate:true, width:20, maxLength:1, labelWidth:20, readOnly:true})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("customer"),this._getConfig_("name")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("location"),this._getConfig_("yesNo")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("acType"),this._getConfig_("validFrom"),this._getConfig_("validto")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("defaultMargin"),this._getConfig_("percent"),this._getConfig_("minimalMargin"),this._getConfig_("percent2")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("defaultMarkup"),this._getConfig_("currecies"),this._getConfig_("unit")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:600, layout:"anchor"})
		.addPanel({ name:"col2", width:600, layout:"anchor"})
		.addPanel({ name:"col3", width:800, layout:"anchor"})
		.addPanel({ name:"col4", width:300, layout:"anchor"})
		.addPanel({ name:"col5", width:600, layout:"anchor"})
		.addPanel({ name:"col6", width:300, layout:"anchor"})
		.addPanel({ name:"col7", width:600, layout:"anchor"})
		.addPanel({ name:"col8", width:300, layout:"anchor"})
		.addPanel({ name:"col9", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8", "col9"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col3", ["row3"])
		.addChildrenTo("col4", ["margin"])
		.addChildrenTo("col5", ["row4"])
		.addChildrenTo("col6", ["markup"])
		.addChildrenTo("col7", ["row5"])
		.addChildrenTo("col8", ["minimalMarkup"])
		.addChildrenTo("col9", ["comments"]);
	}
});
