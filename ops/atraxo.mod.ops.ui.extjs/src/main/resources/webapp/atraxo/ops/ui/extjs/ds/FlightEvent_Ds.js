/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.FlightEvent_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_FlightEvent_Ds"
	},
	
	
	initRecord: function() {
		this.set("product", "Jet-A1");
		this.set("unitCode", _SYSTEMPARAMETERS_.sysvol);
		this.set("status", "Scheduled");
		this.set("events", 1);
		this.set("billStatus", "Not billed");
		this.set("invoiceStatus", "Not invoiced");
	},
	
	fields: [
		{name:"locQuoteId", type:"int", allowNull:true},
		{name:"eventTp", type:"string"},
		{name:"operationTp", type:"string"},
		{name:"locationOrderId", type:"int", allowNull:true},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string"},
		{name:"fuelReqId", type:"int", allowNull:true},
		{name:"fuelRequestCode", type:"string"},
		{name:"fuelRequestStatus", type:"string"},
		{name:"destId", type:"int", allowNull:true},
		{name:"destCode", type:"string"},
		{name:"acTypeId", type:"int", allowNull:true},
		{name:"acTypeCode", type:"string"},
		{name:"tankCap", type:"int", allowNull:true},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"arrivalFromId", type:"int", allowNull:true},
		{name:"arrivalFromCode", type:"string"},
		{name:"operatorId", type:"int", allowNull:true},
		{name:"operatorCode", type:"string"},
		{name:"handlerId", type:"int", allowNull:true},
		{name:"handlerCode", type:"string"},
		{name:"aircraftHBId", type:"int", allowNull:true},
		{name:"aircraftHBCode", type:"string"},
		{name:"tankCapacity", type:"int", allowNull:true},
		{name:"flightNo", type:"string"},
		{name:"registration", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"product", type:"string"},
		{name:"perCaptain", type:"boolean"},
		{name:"upliftDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"arrivalDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"departerDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"captain", type:"string"},
		{name:"eventType", type:"string"},
		{name:"operationType", type:"string"},
		{name:"aircraftCallSign", type:"string"},
		{name:"timeReference", type:"boolean"},
		{name:"toleranceMessage", type:"string"},
		{name:"status", type:"string"},
		{name:"events", type:"int", allowNull:true},
		{name:"totalQuantity", type:"float", allowNull:true},
		{name:"suffix", type:"string"},
		{name:"billStatus", type:"string"},
		{name:"invoiceStatus", type:"string"},
		{name:"newArrivalDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"newDepartureDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"newUpliftDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"newTimeReference", type:"boolean", noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"flightDetails", type:"string", noFilter:true, noSort:true},
		{name:"scheduleDetails", type:"string", noFilter:true, noSort:true},
		{name:"fuelingDetails", type:"string", noFilter:true, noSort:true},
		{name:"newSchedule", type:"string", noFilter:true, noSort:true},
		{name:"oldSchedule", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.FlightEvent_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"locQuoteId", type:"int", allowNull:true},
		{name:"eventTp", type:"string"},
		{name:"operationTp", type:"string"},
		{name:"locationOrderId", type:"int", allowNull:true},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string"},
		{name:"fuelReqId", type:"int", allowNull:true},
		{name:"fuelRequestCode", type:"string"},
		{name:"fuelRequestStatus", type:"string"},
		{name:"destId", type:"int", allowNull:true},
		{name:"destCode", type:"string"},
		{name:"acTypeId", type:"int", allowNull:true},
		{name:"acTypeCode", type:"string"},
		{name:"tankCap", type:"int", allowNull:true},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"arrivalFromId", type:"int", allowNull:true},
		{name:"arrivalFromCode", type:"string"},
		{name:"operatorId", type:"int", allowNull:true},
		{name:"operatorCode", type:"string"},
		{name:"handlerId", type:"int", allowNull:true},
		{name:"handlerCode", type:"string"},
		{name:"aircraftHBId", type:"int", allowNull:true},
		{name:"aircraftHBCode", type:"string"},
		{name:"tankCapacity", type:"int", allowNull:true},
		{name:"flightNo", type:"string"},
		{name:"registration", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"product", type:"string"},
		{name:"perCaptain", type:"boolean", allowNull:true},
		{name:"upliftDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"arrivalDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"departerDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"captain", type:"string"},
		{name:"eventType", type:"string"},
		{name:"operationType", type:"string"},
		{name:"aircraftCallSign", type:"string"},
		{name:"timeReference", type:"boolean", allowNull:true},
		{name:"toleranceMessage", type:"string"},
		{name:"status", type:"string"},
		{name:"events", type:"int", allowNull:true},
		{name:"totalQuantity", type:"float", allowNull:true},
		{name:"suffix", type:"string"},
		{name:"billStatus", type:"string"},
		{name:"invoiceStatus", type:"string"},
		{name:"newArrivalDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"newDepartureDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"newUpliftDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"newTimeReference", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"flightDetails", type:"string", noFilter:true, noSort:true},
		{name:"scheduleDetails", type:"string", noFilter:true, noSort:true},
		{name:"fuelingDetails", type:"string", noFilter:true, noSort:true},
		{name:"newSchedule", type:"string", noFilter:true, noSort:true},
		{name:"oldSchedule", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.ops.ui.extjs.ds.FlightEvent_DsParam", {
	extend: 'Ext.data.Model',



	initParam: function() {
		this.set("isEnabled", true);
	},

	fields: [
		{name:"action", type:"string"},
		{name:"isEnabled", type:"boolean"},
		{name:"paramStatus", type:"string"},
		{name:"remarks", type:"string"},
		{name:"src", type:"string"},
		{name:"warningMessage", type:"string"}
	]
});
