/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelRequest_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ops.ui.extjs.ds.FuelRequest_DsParam,
	recordModel: atraxo.ops.ui.extjs.ds.FuelRequest_Ds,
		
			/* ================ Business functions ================ */
	
	canDoDelete: function() {
		
					var selRecords = this.selectedRecords;
		
					for (var i = 0; i < selRecords.length; i++) {
		               if (selRecords[i].data.requestStatus == __TYPES__.fuelRequests.status.quoted 
							&& !Ext.isEmpty(selRecords[i].data.fuelQuotationCode)) {
						  return false;
					   }
					}
					return  true;
		
	}

});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FuelRequest_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FuelRequest_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"customerCode", dataIndex:"customerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addDateField({name:"requestOn", dataIndex:"requestDate"})
			.addTextField({ name:"processedBy", dataIndex:"processedBy", maxLength:64})
			.addBooleanField({ name:"openRelease", dataIndex:"isOpenRelease"})
			.addDateField({name:"from", dataIndex:"startDate"})
			.addDateField({name:"to", dataIndex:"endDate"})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"requestStatus", store:[ __OPS_TYPE__.FuelRequestStatus._NEW_, __OPS_TYPE__.FuelRequestStatus._QUOTED_, __OPS_TYPE__.FuelRequestStatus._PROCESSED_, __OPS_TYPE__.FuelRequestStatus._CANCELED_, __OPS_TYPE__.FuelRequestStatus._CLOSED_]})
			.addLov({name:"subsidiaryCode", dataIndex:"subsidiaryCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserSubsidiaryLov_Lov", selectOnFocus:true, maxLength:32}})
		;
	}

});

/* ================= EDIT-GRID: EditableList ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelRequest_Dc$EditableList", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ops_FuelRequest_Dc$EditableList",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addLov({name:"customerCode", dataIndex:"customerCode", width:100, allowBlank: false, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_CustomerCodeLov_Lov", allowBlank:false, maxLength:32,
				retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
		.addDateColumn({name:"requestOn", dataIndex:"requestDate", width:100, allowBlank: false, _mask_: Masks.DATE })
		.addLov({name:"processedBy", dataIndex:"processedBy", width:100, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_UserListLov_Lov", maxLength:64,
				filterFieldMapping: [{lovField:"active", value: "true"} ]}})
		.addBooleanColumn({name:"openRelease", dataIndex:"isOpenRelease", width:100})
		.addDateColumn({name:"from", dataIndex:"startDate", width:100, _enableFn_: function(dc, rec, col, fld) { return rec.data.isOpenRelease == true; } , _mask_: Masks.DATE })
		.addDateColumn({name:"to", dataIndex:"endDate", width:100, _enableFn_: function(dc, rec, col, fld) { return rec.data.isOpenRelease == true; } , _mask_: Masks.DATE })
		.addTextColumn({name:"status", dataIndex:"requestStatus", width:100, noEdit: true, maxLength:32})
		.addTextColumn({name:"requestCode", dataIndex:"requestCode", hidden:true, width:100, noEdit: true, maxLength:32})
		.addTextColumn({name:"quoteCode", dataIndex:"quotNo", hidden:true, width:50, noEdit: true, maxLength:100})
		.addTextColumn({name:"contactName", dataIndex:"contactName", hidden:true, width:120, noEdit: true, maxLength:255})
		.addTextColumn({name:"customerReference", dataIndex:"customerReferenceNo", hidden:true, width:120, noEdit: true, maxLength:32})
		.addTextColumn({name:"subsidiaryCode", dataIndex:"subsidiaryCode", hidden:true, width:120, noEdit: true, maxLength:32})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	beforeEdit: function() {
		
						var data = arguments[0].record.data;
						return Ext.isEmpty(data.requestStatus) || data.requestStatus == __TYPES__.fuelRequests.status.nevv || data.requestStatus == __TYPES__.fuelRequests.status.processed ; 
	}
});

/* ================= EDIT FORM: FormView ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelRequest_Dc$FormView", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelRequest_Dc$FormView",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", allowBlank:false, noLabel: true, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "customerId"} ]})
		.addDateField({name:"requestOn", bind:"{d.requestDate}", dataIndex:"requestDate", noLabel: true, labelWidth:115})
		.addTextField({ name:"status", bind:"{d.requestStatus}", dataIndex:"requestStatus", noEdit:true , noLabel: true, maxLength:32})
		.addTextField({ name:"processedBy", bind:"{d.processedBy}", dataIndex:"processedBy", noEdit:true , noLabel: true, maxLength:64})
		.addTextField({ name:"requestCode", bind:"{d.requestCode}", dataIndex:"requestCode", noEdit:true , noLabel: true, maxLength:32})
		.addLov({name:"contact", bind:"{d.contactName}", dataIndex:"contactName", noLabel: true, xtype:"fmbas_ContactsLov_Lov", maxLength:255,
			retFieldMapping: [{lovField:"id", dsField: "contactId"} ],
			filterFieldMapping: [{lovField:"objectId", dsField: "customerId"}, {lovField:"objectType", dsField: "customerEntityAlias"} ]})
		.addTextField({ name:"customerReference", bind:"{d.customerReferenceNo}", dataIndex:"customerReferenceNo", noLabel: true, maxLength:32, labelWidth:115})
		.addBooleanField({ name:"openRelease", bind:"{d.isOpenRelease}", dataIndex:"isOpenRelease", noLabel: true,listeners:{
			change:{scope:this, fn:function() {this._applyStates_(this._controller_.record);}}
		}})
		.addDateField({name:"from", bind:"{d.startDate}", dataIndex:"startDate", _visibleFn_: function(dc, rec) { return this._get_("openRelease").getValue() == true; } , noLabel: true, labelWidth:115})
		.addDateField({name:"to", bind:"{d.endDate}", dataIndex:"endDate", _visibleFn_: function(dc, rec) { return this._get_("openRelease").getValue() == true; } , noLabel: true})
		.addTextField({ name:"quoteNo", bind:"{d.fuelQuotationCode}", dataIndex:"fuelQuotationCode", noEdit:true , noLabel: true, maxLength:32})
		.addTextField({ name:"subsidiaryCode", bind:"{d.subsidiaryCode}", dataIndex:"subsidiaryCode", noEdit:true , noLabel: true, maxLength:32})
		.addDisplayFieldText({ name:"customerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"contactLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"openReleaseLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"requestOnLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"customerReferenceLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fromLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return this._get_("openRelease").getValue() == true; } , maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"statusLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"processedByLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"toLabel", bind:"{d.labelField}", dataIndex:"labelField", _visibleFn_: function(dc, rec) { return this._get_("openRelease").getValue() == true; } , maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"requestCodeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"quoteNoLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"receiverLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left:50px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1", "table2", "table3", "table4"])
		.addChildrenTo("table1", ["customerLabel", "customer", "contactLabel", "contact", "openReleaseLabel", "openRelease"])
		.addChildrenTo("table2", ["requestOnLabel", "requestOn", "customerReferenceLabel", "customerReference", "fromLabel", "from"])
		.addChildrenTo("table3", ["statusLabel", "status", "processedByLabel", "processedBy", "toLabel", "to"])
		.addChildrenTo("table4", ["requestCodeLabel", "requestCode", "quoteNoLabel", "quoteNo", "receiverLabel", "subsidiaryCode"]);
	}
});

/* ================= GRID: Temp ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelRequest_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelRequest_Dc$Temp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: ChangeHistory ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelRequest_Dc$ChangeHistory", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelRequest_Dc$ChangeHistory",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remarks}", paramIndex:"remarks", labelAlign:"top"})
		.addHiddenField({ name:"action", bind:"{p.action}", paramIndex:"action"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"remarkCol1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["action", "remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						return true;
	}
});
