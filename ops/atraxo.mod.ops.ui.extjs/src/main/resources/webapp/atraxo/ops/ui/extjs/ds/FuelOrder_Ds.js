/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelOrder_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_FuelOrder_Ds"
	},
	
	
	validators: {
		customerCode: [{type: 'presence'}],
		fuelQuoteCode: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("status", "New");
		this.set("postedOn", new Date());
		this.set("userCode", getApplication().session.user.code);
	},
	
	fields: [
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"creditTerms", type:"string", noFilter:true, noSort:true},
		{name:"creditLimit", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"creditCurrency", type:"string", noFilter:true, noSort:true},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"creditCard", type:"boolean", noFilter:true, noSort:true},
		{name:"exposure", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"customerStatus", type:"string"},
		{name:"fuelQuoteId", type:"int", allowNull:true},
		{name:"fuelQuoteCode", type:"string"},
		{name:"fuelQuoteIssueDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fuelQuoteValidUntil", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"contactObjectId", type:"int", allowNull:true, noFilter:true},
		{name:"code", type:"string"},
		{name:"customerReferenceNo", type:"string"},
		{name:"postedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"userId", type:"string"},
		{name:"userCode", type:"string"},
		{name:"userName", type:"string"},
		{name:"status", type:"string"},
		{name:"openRelease", type:"boolean"},
		{name:"contractType", type:"string"},
		{name:"scope", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"subsidiaryName", type:"string"},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"alertLimit", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelOrder_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"creditTerms", type:"string", noFilter:true, noSort:true},
		{name:"creditLimit", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"creditCurrency", type:"string", noFilter:true, noSort:true},
		{name:"dummyField", type:"string", noFilter:true, noSort:true},
		{name:"creditCard", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"exposure", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"customerStatus", type:"string"},
		{name:"fuelQuoteId", type:"int", allowNull:true},
		{name:"fuelQuoteCode", type:"string"},
		{name:"fuelQuoteIssueDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fuelQuoteValidUntil", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"contactObjectId", type:"int", allowNull:true, noFilter:true},
		{name:"code", type:"string"},
		{name:"customerReferenceNo", type:"string"},
		{name:"postedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"userId", type:"string"},
		{name:"userCode", type:"string"},
		{name:"userName", type:"string"},
		{name:"status", type:"string"},
		{name:"openRelease", type:"boolean", allowNull:true},
		{name:"contractType", type:"string"},
		{name:"scope", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"subsidiaryName", type:"string"},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"alertLimit", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.ops.ui.extjs.ds.FuelOrder_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"action", type:"string"},
		{name:"dummyField", type:"string"},
		{name:"paramStatus", type:"string"},
		{name:"remarks", type:"string"}
	]
});
