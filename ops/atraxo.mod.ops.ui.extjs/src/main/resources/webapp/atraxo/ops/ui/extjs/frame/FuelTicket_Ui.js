/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.frame.FuelTicket_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.FuelTicket_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("fuelTicket", Ext.create(atraxo.ops.ui.extjs.dc.FuelTicket_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("archivedFuelTicket", Ext.create(atraxo.ops.ui.extjs.dc.ArchivedFuelTicket_Dc,{}))
		.addDc("archivedAttachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("fuelTicketHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("archivedFuelTicketHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("archivedNote", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("aircraft", Ext.create(atraxo.fmbas.ui.extjs.dc.Aircraft_Dc,{}))
		.addDc("fuelTransaction", Ext.create(atraxo.ops.ui.extjs.dc.FuelTransaction_Dc,{}))
		.addDc("fuelTransQuantity", Ext.create(atraxo.ops.ui.extjs.dc.FuelTransactionQuantity_Dc,{multiEdit: true}))
		.addDc("fuelTransEquipment", Ext.create(atraxo.ops.ui.extjs.dc.FuelTransactionEquipment_Dc,{}))
		.addDc("bufferedHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("bufferedNote", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("bufferedAttachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("attachmentPop", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.linkDc("attachment", "fuelTicket",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("archivedAttachment", "archivedFuelTicket",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("fuelTicketHistory", "fuelTicket",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("archivedFuelTicketHistory", "archivedFuelTicket",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "fuelTicket",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("archivedNote", "archivedFuelTicket",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("fuelTransQuantity", "fuelTransaction",{fetchMode:"auto",fields:[
					{childField:"fuelTransId", parentField:"id"}]})
				.linkDc("fuelTransEquipment", "fuelTransaction",{fetchMode:"auto",fields:[
					{childField:"fuelTransId", parentField:"id"}]})
				.linkDc("bufferedHistory", "fuelTransaction",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("bufferedNote", "fuelTransaction",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("bufferedAttachment", "fuelTransaction",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}]})
				.linkDc("attachmentPop", "fuelTicket",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"categType", value:"upload"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"btnSaveEditWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditWdw, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnCloseNoteWdwTrans",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdwTrans, scope:this})
		.addButton({name:"btnSaveCloseNoteWdwTrans",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdwTrans, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addButton({name:"helpArchived",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpArchived, scope:this})
		.addButton({name:"helpWdwTrans",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwTrans, scope:this})
		.addButton({name:"helpWdwTrans1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwTrans1, scope:this})
		.addButton({name:"btnReleaseList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnReleaseList,stateManager:[{ name:"selected_not_zero", dc:"fuelTicket", and: function(dc) {return (this.canRelease(dc));} }], scope:this, visible: !this.useWorkflowForApproval() })
		.addButton({name:"btnReleaseEditForm",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnReleaseEditForm,stateManager:[{ name:"record_is_clean", dc:"fuelTicket", and: function(dc) {return (this.canRelease(dc));} }], scope:this, visible: !this.useWorkflowForApproval() })
		.addButton({name:"btnApprovalList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApprovalList,stateManager:[{ name:"selected_one", dc:"fuelTicket", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnApprovalEditForm",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnApprovalEditForm,stateManager:[{ name:"selected_one_clean", dc:"fuelTicket", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnRejectList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnRejectList,stateManager:[{ name:"selected_one", dc:"fuelTicket", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnRejectEditForm",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnRejectEditForm,stateManager:[{ name:"selected_one_clean", dc:"fuelTicket", and: function(dc) {return (this.canApproveReject(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSaveCloseApproveWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApproveWdw, scope:this})
		.addButton({name:"btnSaveCloseRejectWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectWdw, scope:this})
		.addButton({name:"btnCancelApproveWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApproveWdw, scope:this})
		.addButton({name:"btnCancelRejectWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRejectWdw, scope:this})
		.addButton({name:"btnSaveCloseRejectHistoryWdwList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectHistoryWdwList, scope:this})
		.addButton({name:"btnSaveCloseRejectHistoryWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRejectHistoryWdw, scope:this})
		.addButton({name:"btnRevokeCancelList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnRevokeCancelList, scope:this})
		.addButton({name:"btnRevokeCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnRevokeCancel, scope:this})
		.addButton({name:"btnCancelList",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnCancelList,stateManager:[{ name:"selected_not_zero", dc:"fuelTicket", and: function(dc) {return (this.canRevoke(dc));} }], scope:this})
		.addButton({name:"btnCancelEditForm",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:false, handler: this.onBtnCancelEditForm,stateManager:[{ name:"record_is_clean", dc:"fuelTicket", and: function(dc) {return (this.canRevoke(dc));} }], scope:this})
		.addButton({name:"btnSaveCloseCancelHistoryWdwList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveCloseCancelHistoryWdwList,stateManager:[{ name:"list_not_empty", dc:"fuelTicket", and: function(dc) {return (this.canCancel(dc));} }], scope:this})
		.addButton({name:"btnSaveCloseCancelHistoryWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSaveCloseCancelHistoryWdw,stateManager:[{ name:"list_not_empty", dc:"fuelTicket", and: function(dc) {return (this.canCancel(dc));} }], scope:this})
		.addButton({name:"btnRevokeCancelCancelList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnRevokeCancelCancelList, scope:this})
		.addButton({name:"btnRevokeCancelCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnRevokeCancelCancel, scope:this})
		.addButton({name:"btnSubmitForApproval",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApproval,stateManager:[{ name:"selected_one_clean", dc:"fuelTicket", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSubmitForApprovalEdit",glyph:fp_asc.wkf_glyph.glyph,iconCls: fp_asc.wkf_glyph.css, disabled:true, handler: this.onBtnSubmitForApprovalEdit,stateManager:[{ name:"selected_one_clean", dc:"fuelTicket", and: function(dc) {return (this.canSubmitForApproval(dc));} }], scope:this, visible: this.useWorkflowForApproval() })
		.addButton({name:"btnSaveCloseApprovalWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseApprovalWdw, scope:this})
		.addButton({name:"btnCancelApprovalWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelApprovalWdw, scope:this})
		.addButton({name:"btnSaveCloseAircraftRegistrationWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseAircraftRegistrationWdw, scope:this})
		.addButton({name:"btnCloseAircraftRegistrationWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseAircraftRegistrationWdw, scope:this})
		.addButton({name:"btnProcess",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnProcess,stateManager:[{ name:"record_is_clean", dc:"fuelTransaction"}], scope:this})
		.addButton({name:"btnProcessEdit",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnProcessEdit,stateManager:[{ name:"record_is_clean", dc:"fuelTransaction"}], scope:this})
		.addButton({name:"btnCancelEquipmentEdit",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelEquipmentEdit, scope:this})
		.addButton({name:"btnSaveEquipment",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveEquipment, scope:this})
		.addButton({name:"btnViewArchived",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnViewArchived,stateManager:[{ name:"selected_one_clean", dc:"archivedFuelTicket"}], scope:this})
		.addButton({name:"btnPurgeArchived",glyph:fp_asc.thrash_glyph.glyph, disabled:true, handler: this.onBtnPurgeArchived,stateManager:[{ name:"selected_not_zero", dc:"archivedFuelTicket"}], scope:this})
		.addButton({name:"btnEmptyArchived",glyph:fp_asc.recycle_glyph.glyph, disabled:true, handler: this.onBtnEmptyArchived,stateManager:[{ name:"list_not_empty", dc:"archivedFuelTicket"}], scope:this})
		.addButton({name:"btnUndoArchived",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnUndoArchived,stateManager:[{ name:"selected_not_zero", dc:"archivedFuelTicket"}], scope:this})
		.addButton({name:"btnPurgeArchivedDetails",glyph:fp_asc.thrash_glyph.glyph, disabled:false, handler: this.onBtnPurgeArchivedDetails, scope:this})
		.addButton({name:"btnUndoArchivedDetails",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnUndoArchivedDetails,stateManager:[{ name:"selected_not_zero", dc:"archivedFuelTicket"}], scope:this})
		.addButton({name:"btnBackArchived",glyph:fp_asc.back_glyph.glyph,iconCls: fp_asc.back_glyph.css, disabled:false, handler: this.onBtnBackArchived, scope:this})
		.addButton({name:"btnDeleteTicket",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnDeleteTicket,stateManager:[{ name:"selected_not_zero", dc:"fuelTicket", and: function(dc) {return (this.canDelete(dc));} }], scope:this})
		.addButton({name:"btnReissueEdit",glyph:fp_asc.refresh_glyph.glyph, disabled:true, handler: this.onBtnReissueEdit,stateManager:[{ name:"selected_one_clean", dc:"fuelTicket", and: function(dc) {return (this.canReissue(dc));} }], scope:this})
		.addButton({name:"btnReissueList",glyph:fp_asc.refresh_glyph.glyph, disabled:true, handler: this.onBtnReissueList,stateManager:[{ name:"selected_one_clean", dc:"fuelTicket", and: function(dc) {return (this.canReissue(dc));} }], scope:this})
		.addButton({name:"btnCancelReissue",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelReissue, scope:this})
		.addButton({name:"btnSaveReissue",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveReissue, scope:this})
		.addButton({name:"btnCancelReissueList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelReissueList, scope:this})
		.addButton({name:"btnSaveReissueList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveReissueList, scope:this})
		.addDcFilterFormView("fuelTicket", {name:"Filter", xtype:"ops_FuelTicket_Dc$Filter"})
		.addDcGridView("fuelTicket", {name:"List", xtype:"ops_FuelTicket_Dc$List"})
		.addDcFormView("fuelTicket", {name:"PopUp", xtype:"ops_FuelTicket_Dc$PopUp",  cls:"form-large-space", _firstFocusItemName_:"customer", _acquireFocusUpdate_: false})
		.addDcFormView("fuelTicket", {name:"Edit", xtype:"ops_FuelTicket_Dc$Edit",  cls:"form-large-space", _firstFocusItemName_:"customer", _acquireFocusInsert_: false})
		.addDcFormView("fuelTicket", {name:"TabEquipment", _hasTitle_:true, xtype:"ops_FuelTicket_Dc$Tab"})
		.addDcFormView("archivedFuelTicket", {name:"TabEquipmentArchived", _hasTitle_:true, xtype:"ops_ArchivedFuelTicket_Dc$ArchivedTab"})
		.addDcFormView("fuelTicket", {name:"TabPayment", _hasTitle_:true, xtype:"ops_FuelTicket_Dc$TabPayment"})
		.addDcFormView("archivedFuelTicket", {name:"TabPaymentArchived", _hasTitle_:true, xtype:"ops_ArchivedFuelTicket_Dc$ArchivedTabPayment"})
		.addDcGridView("fuelTicketHistory", {name:"fuelTicketHistoryList", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcGridView("archivedFuelTicketHistory", {name:"fuelTicketHistoryListArchived", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcGridView("note", {name:"notesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcGridView("archivedNote", {name:"notesListArchived", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"notesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcFormView("fuelTicket", {name:"fuelTicketRemarkEdit", xtype:"ops_FuelTicket_Dc$EditRemark", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("fuelTicket", {name:"fuelTicketRemarkEditList", xtype:"ops_FuelTicket_Dc$EditRemark", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("fuelTicket", {name:"fuelTicketRemarkCancel", xtype:"ops_FuelTicket_Dc$RevokeRemark", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("fuelTicket", {name:"fuelTicketRemarkCancelList", xtype:"ops_FuelTicket_Dc$RevokeRemark", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("fuelTicket", {name:"fuelTicketRemarkReset", xtype:"ops_FuelTicket_Dc$EditRemark", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcFormView("fuelTicket", {name:"fuelTicketRemarkResetList", xtype:"ops_FuelTicket_Dc$EditRemark", _acquireFocusInsert_: false, _acquireFocusUpdate_: false})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcGridView("archivedAttachment", {name:"attachmentListArchived", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addDcFormView("aircraft", {name:"newAircraftRegistration", xtype:"fmbas_Aircraft_Dc$New"})
		.addDcGridView("archivedFuelTicket", {name:"ArchivedFuelTickets", xtype:"ops_ArchivedFuelTicket_Dc$ArchivedList"})
		.addDcFilterFormView("archivedFuelTicket", {name:"ArchivedFuelFilter", xtype:"ops_ArchivedFuelTicket_Dc$Filter"})
		.addDcFormView("archivedFuelTicket", {name:"ArchivedFuelTicketsView", xtype:"ops_ArchivedFuelTicket_Dc$ArchivedView"})
		.addDcFilterFormView("fuelTransaction", {name:"FuelTransactionFilter", xtype:"ops_FuelTransaction_Dc$Filter"})
		.addDcGridView("fuelTransaction", {name:"FuelTransactionList", xtype:"ops_FuelTransaction_Dc$List"})
		.addDcFormView("fuelTransaction", {name:"FuelTransactionEdit", xtype:"ops_FuelTransaction_Dc$Edit"})
		.addDcFormView("fuelTransaction", {name:"FuelTransactionPayment", _hasTitle_:true, xtype:"ops_FuelTransaction_Dc$TabPayment"})
		.addDcGridView("bufferedHistory", {name:"FuelTransHistory", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcGridView("bufferedNote", {name:"FuelTransNotes", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("bufferedNote", {name:"FuelTransNotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("bufferedAttachment", {name:"FuelTransAttachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("bufferedAttachment", {name:"FuelTransAttachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addDcFilterFormView("fuelTransQuantity", {name:"FuelTransactionQuantityFilter", xtype:"ops_FuelTransactionQuantity_Dc$Filter"})
		.addDcEditGridView("fuelTransQuantity", {name:"FuelTransactionQuantity", _hasTitle_:true, xtype:"ops_FuelTransactionQuantity_Dc$List", frame:true})
		.addDcFilterFormView("fuelTransEquipment", {name:"FuelTransactionEquipmentFilter", xtype:"ops_FuelTransactionEquipment_Dc$Filter"})
		.addDcGridView("fuelTransEquipment", {name:"FuelTransactionEquipment", _hasTitle_:true, xtype:"ops_FuelTransactionEquipment_Dc$List"})
		.addDcFormView("fuelTransEquipment", {name:"FuelTransactionEquipmentEdit", xtype:"ops_FuelTransactionEquipment_Dc$Edit"})
		.addDcFormView("fuelTicket", {name:"NewFruelTicketNumber", xtype:"ops_FuelTicket_Dc$NewFuelTicketNumber"})
		.addDcFormView("fuelTicket", {name:"NewFruelTicketNumberList", xtype:"ops_FuelTicket_Dc$NewFuelTicketNumber"})
		.addDcFormView("fuelTicket", {name:"approveNote", xtype:"ops_FuelTicket_Dc$ApprovalNote"})
		.addDcFormView("fuelTicket", {name:"rejectNote", xtype:"ops_FuelTicket_Dc$ApprovalNote"})
		.addDcFormView("fuelTicket", {name:"approvalNote", xtype:"ops_FuelTicket_Dc$ApprovalNote",  split:false})
		.addDcGridView("attachmentPop", {name:"documentsAssignList", _hasTitle_:true, height:300, xtype:"ad_Attachment_Dc$attachmentAssignList",  cls:"sone-custom-panel-title"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"TabbedPanel", xtype:"tabpanel", activeTab:0, plain:false,  cls:"sone-tab-two-levels", deferredRender:true})
		.addPanel({name:"Buffered", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"ArchiveStackedPanel", layout:"card", activeItem:0})
		.addPanel({name:"Archived", _hasTitle_:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"BufferedPanel", layout:"card", activeItem:0})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"TabBuffered", xtype:"tabpanel", activeTab:0, plain:false,  deferredRender:true})
		.addPanel({name:"Proccesed", _hasTitle_:true, layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas4", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas5", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"Tabs", xtype:"tabpanel", activeTab:0, plain:false,  deferredRender:true})
		.addPanel({name:"TabsArchived", xtype:"tabpanel", activeTab:0, plain:false,  deferredRender:true})
		.addPanel({name:"panelAssignAttachments", layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwApprovalNote", _hasTitle_:true, width:600, height:530, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("panelAssignAttachments")],  closable:true, listeners:{ show:{fn:this.onApprovalWdwShow, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApprovalWdw"), this._elems_.get("btnCancelApprovalWdw")]}]})
		.addWindow({name:"wdwEquipment", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("FuelTransactionEquipmentEdit")],  listeners:{ close:{fn:this.onWdwEquipmentClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveEquipment"), this._elems_.get("btnCancelEquipmentEdit")]}]})
		.addWindow({name:"wdwFuelTicket", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PopUp")],  listeners:{ close:{fn:this.onWdwFuelTicketClose, scope:this}, show: {scope: this, fn: function() {this._onWdwdFuelTicketShow_()}} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveEditWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addWindow({name:"wdwRejectReasonList", _hasTitle_:true, width:600, height:430, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("fuelTicketRemarkEditList")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectHistoryWdwList"), this._elems_.get("btnRevokeCancelList")]}]})
		.addWindow({name:"wdwRejectReason", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("fuelTicketRemarkEdit")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectHistoryWdw"), this._elems_.get("btnRevokeCancel")]}]})
		.addWindow({name:"wdwCancelReasonList", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("fuelTicketRemarkCancelList")],  listeners:{ close:{fn:this.cancelwdwCancelReasonList, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseCancelHistoryWdwList"), this._elems_.get("btnRevokeCancelCancelList")]}]})
		.addWindow({name:"wdwCancelReason", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("fuelTicketRemarkCancel")],  listeners:{ close:{fn:this.cancelwdwCancelReason, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseCancelHistoryWdw"), this._elems_.get("btnRevokeCancelCancel")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("notesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addWindow({name:"noteWdwTrans", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("FuelTransNotesEdit")],  listeners:{ close:{fn:this.onNoteWdwTransClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdwTrans"), this._elems_.get("btnCloseNoteWdwTrans")]}]})
		.addWindow({name:"wdwNewAircraftRegistration", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("newAircraftRegistration")],  listeners:{ close:{fn:this.onAircraftWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseAircraftRegistrationWdw"), this._elems_.get("btnCloseAircraftRegistrationWdw")]}]})
		.addWindow({name:"wdwNewFuelTicketNumber", _hasTitle_:true, height:170, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NewFruelTicketNumber")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveReissue"), this._elems_.get("btnCancelReissue")]}]})
		.addWindow({name:"wdwApproveNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("approveNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseApproveWdw"), this._elems_.get("btnCancelApproveWdw")]}]})
		.addWindow({name:"wdwRejectNote", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("rejectNote")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRejectWdw"), this._elems_.get("btnCancelRejectWdw")]}]})
		.addWindow({name:"wdwNewFuelTicketNumberList", _hasTitle_:true, height:170, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NewFruelTicketNumberList")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveReissueList"), this._elems_.get("btnCancelReissueList")]}]})
		.addTabPanelForUserFields({tabPanelName:"TabBuffered", containerPanelName:"canvas3", dcName:"fuelTransaction"})
		
		.addTabPanelForUserFields({tabPanelName:"Tabs", containerPanelName:"canvas2", dcName:"fuelTicket"})
		
		.addTabPanelForUserFields({tabPanelName:"TabsArchived", containerPanelName:"canvas5", dcName:"archivedFuelTicket"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["TabbedPanel"])
		.addChildrenTo("TabbedPanel", ["Proccesed"])
		.addChildrenTo("Buffered", ["BufferedPanel"], ["center"])
		.addChildrenTo("ArchiveStackedPanel", ["canvas4", "canvas5"])
		.addChildrenTo("Archived", ["ArchiveStackedPanel"], ["center"])
		.addChildrenTo("BufferedPanel", ["FuelTransactionList", "canvas3"])
		.addChildrenTo("canvas3", ["FuelTransactionEdit", "TabBuffered"], ["north", "center"])
		.addChildrenTo("TabBuffered", ["FuelTransactionQuantity"])
		.addChildrenTo("Proccesed", ["canvas1", "canvas2"])
		.addChildrenTo("canvas1", ["List"], ["center"])
		.addChildrenTo("canvas2", ["Edit", "Tabs"], ["north", "center"])
		.addChildrenTo("canvas4", ["ArchivedFuelTickets"], ["center"])
		.addChildrenTo("canvas5", ["ArchivedFuelTicketsView", "TabsArchived"], ["north", "center"])
		.addChildrenTo("Tabs", ["TabEquipment"])
		.addChildrenTo("TabsArchived", ["TabEquipmentArchived"])
		.addChildrenTo("panelAssignAttachments", ["approvalNote", "documentsAssignList"], ["north", "center"])
		.addToolbarTo("List", "tlbList")
		.addToolbarTo("ArchivedFuelTickets", "tlbArchive")
		.addToolbarTo("Edit", "tlbEdit")
		.addToolbarTo("notesList", "tlbNoteList")
		.addToolbarTo("attachmentList", "tlbAttachments")
		.addToolbarTo("FuelTransactionList", "tlbBuffList")
		.addToolbarTo("FuelTransactionEdit", "tlbBuffEdit")
		.addToolbarTo("FuelTransactionQuantity", "tlbQuantity")
		.addToolbarTo("FuelTransactionEquipment", "tlbEquipment")
		.addToolbarTo("FuelTransNotes", "tlbNoteListBuffered")
		.addToolbarTo("FuelTransAttachmentList", "tlbAttachmentsBuffered")
		.addToolbarTo("ArchivedFuelTicketsView", "tlbArchivedDetails");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("TabbedPanel", ["Buffered", "Archived"])
		.addChildrenTo2("TabBuffered", ["FuelTransactionEquipment", "FuelTransactionPayment", "FuelTransNotes", "FuelTransAttachmentList", "FuelTransHistory"])
		.addChildrenTo2("Tabs", ["TabPayment", "notesList", "attachmentList", "fuelTicketHistoryList"])
		.addChildrenTo2("TabsArchived", ["TabPaymentArchived", "notesListArchived", "attachmentListArchived", "fuelTicketHistoryListArchived"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "fuelTicket"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnDeleteTicket"),this._elems_.get("btnReleaseList"),this._elems_.get("btnSubmitForApproval"),this._elems_.get("btnApprovalList"),this._elems_.get("btnRejectList"),this._elems_.get("btnCancelList"),this._elems_.get("btnReissueList"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbArchive", {dc: "archivedFuelTicket"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnViewArchived"),this._elems_.get("btnPurgeArchived"),this._elems_.get("btnEmptyArchived"),this._elems_.get("btnUndoArchived"),this._elems_.get("helpArchived")])
			.addReports()
			.addDefaultDblClickBtnAction("btnViewArchived")
		.end()
		.beginToolbar("tlbEdit", {dc: "fuelTicket"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnReleaseEditForm"),this._elems_.get("btnSubmitForApprovalEdit"),this._elems_.get("btnApprovalEditForm"),this._elems_.get("btnRejectEditForm"),this._elems_.get("btnCancelEditForm"),this._elems_.get("btnReissueEdit"),this._elems_.get("helpWdw1")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbBuffList", {dc: "fuelTransaction"})
			.addButtons([])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnProcess"),this._elems_.get("helpWdwTrans")])
			.addReports()
		.end()
		.beginToolbar("tlbBuffEdit", {dc: "fuelTransaction"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnProcessEdit"),this._elems_.get("helpWdwTrans1")])
			.addReports()
		.end()
		.beginToolbar("tlbQuantity", {dc: "fuelTransQuantity"})
			.addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbEquipment", {dc: "fuelTransEquipment"})
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main"})
			.addReports()
		.end()
		.beginToolbar("tlbNoteListBuffered", {dc: "bufferedNote"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAttachmentsBuffered", {dc: "bufferedAttachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbArchivedDetails", {dc: "archivedFuelTicket"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnBackArchived"),this._elems_.get("btnPurgeArchivedDetails"),this._elems_.get("btnUndoArchivedDetails")])
			.addReports()
		.end();
	}
	
	,_applyTabLoadRestriction_: function(){
		this._getBuilder_()
		.addTabLoadRestriction("fuelTransQuantity","TabBuffered",["FuelTransactionQuantity"])
		.addTabLoadRestriction("fuelTransEquipment","TabBuffered",["FuelTransactionEquipment"])
		.addTabLoadRestriction("bufferedNote","TabBuffered",["FuelTransNotes"])
		.addTabLoadRestriction("bufferedAttachment","TabBuffered",["FuelTransAttachmentList"])
		.addTabLoadRestriction("bufferedHistory","TabBuffered",["FuelTransHistory"])
		.addTabLoadRestriction("note","Tabs",["notesList"])
		.addTabLoadRestriction("fuelTicketHistory","Tabs",["fuelTicketHistoryList"])
		.addTabLoadRestriction("attachment","Tabs",["attachmentList"])
		.addTabLoadRestriction("archivedNote","TabsArchived",["notesListArchived"])
		.addTabLoadRestriction("archivedFuelTicketHistory","TabsArchived",["fuelTicketHistoryListArchived"])
		.addTabLoadRestriction("archivedAttachment","TabsArchived",["attachmentListArchived"])
		;
	}

	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onWdwFuelTicketClose();
		this._getWindow_("wdwFuelTicket").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button btnSaveEditWdw
	 */
	,onBtnSaveEditWdw: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdwTrans
	 */
	,onBtnCloseNoteWdwTrans: function() {
		this._getDc_("bufferedNote").doCancel();
		this._getWindow_("noteWdwTrans").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdwTrans
	 */
	,onBtnSaveCloseNoteWdwTrans: function() {
		this._getDc_("bufferedNote").doSave();
		this._getWindow_("noteWdwTrans").close();
		this._getDc_("bufferedNote").doReloadPage();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpArchived
	 */
	,onHelpArchived: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwTrans
	 */
	,onHelpWdwTrans: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwTrans1
	 */
	,onHelpWdwTrans1: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnReleaseList
	 */
	,onBtnReleaseList: function() {
		var successFn = function(dc,response) {
			dc.setParamValue("remark","")
			this._getDc_("fuelTicketHistory").doReloadPage();
			this._applyStateAllButtons_();
			this.showError(response)
			this._getDc_("fuelTicket").doReloadPage();
		};
		var o={
			name:"releaseList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnReleaseEditForm
	 */
	,onBtnReleaseEditForm: function() {
		var successFn = function(dc,response) {
			dc.setParamValue("remark","")
			this._getDc_("fuelTicketHistory").doReloadPage();
			this._applyStateAllButtons_();
			this._getDc_("fuelTicket").doEditOut();
			this.showError(response)
			this._getDc_("fuelTicket").doReloadPage();
		};
		var o={
			name:"release",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnApprovalList
	 */
	,onBtnApprovalList: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnApprovalEditForm
	 */
	,onBtnApprovalEditForm: function() {
		this.openApproveWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectList
	 */
	,onBtnRejectList: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnRejectEditForm
	 */
	,onBtnRejectEditForm: function() {
		this.openRejectWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApproveWdw
	 */
	,onBtnSaveCloseApproveWdw: function() {
		this.approveRpc()
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectWdw
	 */
	,onBtnSaveCloseRejectWdw: function() {
		this.rejectRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApproveWdw
	 */
	,onBtnCancelApproveWdw: function() {
		this._getWindow_("wdwApproveNote").close();
		this._getDc_("fuelTicket").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnCancelRejectWdw
	 */
	,onBtnCancelRejectWdw: function() {
		this._getWindow_("wdwRejectNote").close();
		this._getDc_("fuelTicket").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectHistoryWdwList
	 */
	,onBtnSaveCloseRejectHistoryWdwList: function() {
		var successFn = function(dc) {
			this._getWindow_("wdwRejectReasonList").close();
			dc.setParamValue("remark","")
			this._getDc_("fuelTicketHistory").doReloadPage();
			this._applyStateAllButtons_();
			this._getDc_("fuelTicket").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"rejectList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRejectHistoryWdw
	 */
	,onBtnSaveCloseRejectHistoryWdw: function() {
		var successFn = function(dc) {
			this._getWindow_("wdwRejectReason").close();
			dc.setParamValue("remark","")
			this._getDc_("fuelTicketHistory").doReloadPage();
			this._applyStateAllButtons_();
			this._getDc_("fuelTicket").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"reject",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnRevokeCancelList
	 */
	,onBtnRevokeCancelList: function() {
		this._getWindow_("wdwRejectReasonList").close();
	}
	
	/**
	 * On-Click handler for button btnRevokeCancel
	 */
	,onBtnRevokeCancel: function() {
		this._getWindow_("wdwRejectReason").close();
	}
	
	/**
	 * On-Click handler for button btnCancelList
	 */
	,onBtnCancelList: function() {
		this._getWindow_("wdwCancelReasonList").show();
	}
	
	/**
	 * On-Click handler for button btnCancelEditForm
	 */
	,onBtnCancelEditForm: function() {
		this._getWindow_("wdwCancelReasonList").show();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseCancelHistoryWdwList
	 */
	,onBtnSaveCloseCancelHistoryWdwList: function() {
		var successFn = function() {
			this._getWindow_("wdwCancelReasonList").close();
			this._applyStateAllButtons_();
			this._getDc_("fuelTicket").doReloadPage();
		};
		var failureFn = function(dc,response) {
			this._getWindow_("wdwCancelReasonList").close();
			Main.rpcFailure(response);
		};
		var o={
			name:"cancelList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnSaveCloseCancelHistoryWdw
	 */
	,onBtnSaveCloseCancelHistoryWdw: function() {
		var successFn = function() {
			this._getDc_("fuelTicket").setParamValue("remark","");
			this._getWindow_("wdwCancelReason").close();
			this._applyStateAllButtons_();
			this._getDc_("fuelTicket").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"cancel",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnRevokeCancelCancelList
	 */
	,onBtnRevokeCancelCancelList: function() {
		this.cancelFuelTicketCancelWindowList();
	}
	
	/**
	 * On-Click handler for button btnRevokeCancelCancel
	 */
	,onBtnRevokeCancelCancel: function() {
		this.cancelFuelTicketCancelWindowEdit();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApproval
	 */
	,onBtnSubmitForApproval: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSubmitForApprovalEdit
	 */
	,onBtnSubmitForApprovalEdit: function() {
		this.openSubmitForApprovalWindow();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseApprovalWdw
	 */
	,onBtnSaveCloseApprovalWdw: function() {
		this.submitForApprovalRpc()
	}
	
	/**
	 * On-Click handler for button btnCancelApprovalWdw
	 */
	,onBtnCancelApprovalWdw: function() {
		this._getWindow_("wdwApprovalNote").close();
		this._getDc_("fuelTicket").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseAircraftRegistrationWdw
	 */
	,onBtnSaveCloseAircraftRegistrationWdw: function() {
		this._saveAndCloseNewRegistrationWdw_();
	}
	
	/**
	 * On-Click handler for button btnCloseAircraftRegistrationWdw
	 */
	,onBtnCloseAircraftRegistrationWdw: function() {
		this._getDc_("aircraft").doCancel();
		this._getWindow_("wdwNewAircraftRegistration").close();
	}
	
	/**
	 * On-Click handler for button btnProcess
	 */
	,onBtnProcess: function() {
		this._process_();
	}
	
	/**
	 * On-Click handler for button btnProcessEdit
	 */
	,onBtnProcessEdit: function() {
		this._processEdit_();
	}
	
	/**
	 * On-Click handler for button btnCancelEquipmentEdit
	 */
	,onBtnCancelEquipmentEdit: function() {
		this.onWdwEquipmentClose();
		this._getWindow_("wdwEquipment").close();
	}
	
	/**
	 * On-Click handler for button btnSaveEquipment
	 */
	,onBtnSaveEquipment: function() {
		this._getDc_("fuelTransEquipment").doSave();
		this._getWindow_("wdwEquipment").close();
	}
	
	/**
	 * On-Click handler for button btnViewArchived
	 */
	,onBtnViewArchived: function() {
		this._showArchivedTicketDetails_();
	}
	
	/**
	 * On-Click handler for button btnPurgeArchived
	 */
	,onBtnPurgeArchived: function() {
		this._getDc_("archivedFuelTicket").doDelete();
	}
	
	/**
	 * On-Click handler for button btnEmptyArchived
	 */
	,onBtnEmptyArchived: function() {
		this._purgeAllArchivedTickets_();
	}
	
	/**
	 * On-Click handler for button btnUndoArchived
	 */
	,onBtnUndoArchived: function() {
		var successFn = function() {
			this._getDc_("archivedFuelTicket").doReloadPage();
			this._getDc_("fuelTicket").doReloadPage();
		};
		var o={
			name:"undo",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("archivedFuelTicket").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnPurgeArchivedDetails
	 */
	,onBtnPurgeArchivedDetails: function() {
		this._purgeArchivedAndReturnToList_();
	}
	
	/**
	 * On-Click handler for button btnUndoArchivedDetails
	 */
	,onBtnUndoArchivedDetails: function() {
		var successFn = function() {
			this._backToArchivedList_();
			this._getDc_("archivedFuelTicket").doReloadPage();
			this._getDc_("fuelTicket").doReloadPage();
		};
		var o={
			name:"undo",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("archivedFuelTicket").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnBackArchived
	 */
	,onBtnBackArchived: function() {
		this._backToArchivedList_();
	}
	
	/**
	 * On-Click handler for button btnDeleteTicket
	 */
	,onBtnDeleteTicket: function() {
		this._deleteFuelTicket_();
	}
	
	/**
	 * On-Click handler for button btnReissueEdit
	 */
	,onBtnReissueEdit: function() {
		this._getWindow_("wdwNewFuelTicketNumber").show();
	}
	
	/**
	 * On-Click handler for button btnReissueList
	 */
	,onBtnReissueList: function() {
		this._getWindow_("wdwNewFuelTicketNumberList").show();
	}
	
	/**
	 * On-Click handler for button btnCancelReissue
	 */
	,onBtnCancelReissue: function() {
		this._getWindow_("wdwNewFuelTicketNumber").close();
		this._getDc_("fuelTicket").doCancel();
		this._getDc_("fuelTicket").params.set("newFuelTicketNumber","")
	}
	
	/**
	 * On-Click handler for button btnSaveReissue
	 */
	,onBtnSaveReissue: function() {
		this._getWindow_("wdwNewFuelTicketNumber").close();
		var successFn = function() {
			this._getDc_("fuelTicket").params.set("newFuelTicketNumber","")
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"reissue",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelReissueList
	 */
	,onBtnCancelReissueList: function() {
		this._getWindow_("wdwNewFuelTicketNumberList").close();
		this._getDc_("fuelTicket").doCancel();
		this._getDc_("fuelTicket").params.set("newFuelTicketNumber","")
	}
	
	/**
	 * On-Click handler for button btnSaveReissueList
	 */
	,onBtnSaveReissueList: function() {
		this._getWindow_("wdwNewFuelTicketNumberList").close();
		var successFn = function() {
			this._getDc_("fuelTicket").params.set("newFuelTicketNumber","")
			this._getDc_("fuelTicket").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"reissue",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcData(o);
	}
	
	,cancelFuelTicketCancelWindowList: function() {	
		this._getDc_("fuelTicket").doCancel();;
		this._getWindow_("wdwCancelReasonList").close();
	}
	
	,cancelFuelTicketCancelWindowEdit: function() {	
		this._getDc_("fuelTicket").doCancel();;
		this._getWindow_("wdwCancelReason").close();
	}
	
	,cancelList: function() {	;
		var successFn = function(dc,response) {
			this._showResult_(response); this._getDc_("fuelTicket").setParamValue("remark",""); this._setupRevokation_(response);
			this._getWindow_("wdwCancelReasonList").close();
			this._getDc_("fuelTicketHistory").doReloadPage();
			this._applyStateAllButtons_();
			this._getDc_("fuelTicket").doReloadPage();
		};
		var failureFn = function(dc,response) {
			Main.rpcFailure(response);
		};
		var o={
			name:"cancelList",
			callbacks:{
				successFn: successFn,
				successScope: this,
				failureFn: failureFn,
				failureScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcDataList(o);
	}
	
	,onNoteWdwClose: function() {	
		this._getDc_("note").doCancel();
	}
	
	,onNoteWdwTransClose: function() {	
		this._getDc_("bufferedNote").doCancel();
	}
	
	,onAircraftWdwClose: function() {	
		this._getDc_("aircraft").doCancel();
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,_hardCopy_: function() {	
		var successFn = function() {
			this.showHardCopy();
		};
		var o={
			name:"getHardCopy",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fuelTicket").doRpcData(o);
	}
	
	,cancelwdwCancelReasonList: function() {
		
						var dc = this._getDc_("fuelTicket");
						dc.doCancel();
						dc.setParamValue("remark",null);
	}
	
	,cancelwdwCancelReason: function() {
		
						var dc = this._getDc_("fuelTicket");
						dc.doCancel();
						dc.setParamValue("remark",null);
	}
	
	,_setupRevokation_: function(response) {
		
						var data = Ext.getResponseDataInJSON(response).params;
						var cancelResult = data.cancelResult;
						var generatedCreditMemos = JSON.parse(cancelResult).generatedCreditMemos;
						var removedFromInvoices = JSON.parse(cancelResult).removedFromInvoices;
						if (Ext.isEmpty(removedFromInvoices) && Ext.isEmpty(generatedCreditMemos)) {
							this._showEmptyRevocationWdw_();
						}
						else {
							this._showRevocationWdw_(cancelResult);
						}
	}
	
	,_setupRevokationFromNotification_: function(response) {
		
						var generatedCreditMemos = response.generatedCreditMemos;
						var removedFromInvoices = response.removedFromInvoices;
						
						if (Ext.isEmpty(removedFromInvoices) && Ext.isEmpty(generatedCreditMemos)) {
							this._showEmptyRevocationWdw_();
						}
						else {
							this._showRevocationWdw_(JSON.stringify(response));
						}
	}
	
	,_showEmptyRevocationWdw_: function() {
		
						var winId = Ext.id();
						var ctx = this;
						var window = Ext.create("Ext.window.Window", {
						    title: Main.translate("applicationMsg","revokeWindowTitle__lbl"),
							id: winId,
						    height: 160,
						    width: 500,
							modal: true,
							bodyPadding: 10,
							buttonAlign : "center",
						    items: [{
								html: Main.translate("applicationMsg","revokeWindowFirstLabel__lbl"),
								style: "font-weight:bold; margin-bottom:5px"
							},{
								html: Main.translate("applicationMsg","revokeWindowSecondLabel__lbl"),
								style: "font-weight:bold; margin-bottom:5px"
							}],
							buttons: [{
					            text : "OK",
								glyph: "xf00c@FontAwesome",
								iconCls: "glyph-green",
					            handler  : function() {
					            	var w = Ext.getCmp(winId);
									var dc = ctx._getDc_("fuelTicket");
									dc.doReloadPage();
									w.hide();
					            }
				            }]
						}).show();
						var dc = this._getDc_("fuelTicket");
						dc.doReloadPage();
						return window;
	}
	
	,_showRevocationWdw_: function(data) {
		
						data = JSON.parse(data);
						var winId = Ext.id();
						var ctx = this;
						var generatedCreditMemos = new Ext.data.JsonStore({
					    	root: "generatedCreditMemos",
					    	fields: [ "ticketNumber", "invoiceNumber" ],
					    	data: data,
							autoLoad: true,
							proxy:{
							    type: "memory",
							    reader:{
						        	rootProperty: "generatedCreditMemos"
						        }
							}
					    });
		
						var removedFromInvoices = new Ext.data.JsonStore({
					    	root: "removedFromInvoices",
					    	fields: [ "ticketNumber", "invoiceNumber" ],
					    	autoLoad: true,
					    	data: data,					
							proxy:{
							    type: "memory",
							    reader:{
						        	rootProperty: "removedFromInvoices"
						        }
							}
					    });
						
						var window = Ext.create("Ext.window.Window", {
						    title: Main.translate("applicationMsg","revokeWindowTitle__lbl"),
							id: winId,
						    height: 370,
						    width: 500,
							modal: true,
							bodyPadding: 10,
							buttonAlign : "center",
						    items: [{
								html: Main.translate("applicationMsg","revokeWindowThirdLabel__lbl"),
								style: "font-weight:bold; margin-bottom:5px"
							},{
								xtype: "gridpanel",
								layout: "fit",
								height: 100,
								columns: [
						            {text: "Ticket #", dataIndex: "ticketNumber", flex: 1},
						            {text: "Credit memo #", dataIndex: "invoiceNumber", flex: 1},
					            ],
								store: generatedCreditMemos,
							},{
								html: Main.translate("applicationMsg","revokeWindowFourthLabel__lbl"),
								style: "font-weight:bold; margin-bottom:5px; margin-top:15px"
							},{
								xtype: "gridpanel",
								layout: "fit",
								height: 100,
								columns: [
						            {text: "Ticket #", dataIndex: "ticketNumber", flex: 1},
						            {text: "Sales invoice #", dataIndex: "invoiceNumber", flex: 1},
					            ],
								store: removedFromInvoices,
							}],
							buttons: [{
					            text : "OK",
								glyph: "xf00c@FontAwesome",
								iconCls: "glyph-green",
					            handler  : function() {
					            	var w = Ext.getCmp(winId);
									var dc = ctx._getDc_("fuelTicket");
									dc.doReloadPage();
									w.hide();
					            }
				            },{
					            text : "Show invoices",
								glyph: "xf14c@FontAwesome",
								iconCls: "glyph-blue",
								//disabled: Ext.isEmpty(data.removedFromInvoices) ? true : false,
					            handler  : function() {
									var bundle = "FuelEvents_Ui.ui";
									var frame = "atraxo.acc.ui.extjs.frame.OutgoingInvoices_Ui";
									var w = Ext.getCmp(winId);
					            	getApplication().showFrame(frame,{
										url:Main.buildUiPath(bundle, frame, false),
										params: {
											invoices: data.removedFromInvoices,
											creditMemos: data.generatedCreditMemos
										},
										callback: function (params) {
											this._show_removed_(params);
										}
									});
									w.hide();
					            }
				            }]
						}).show();
						var dc = this._getDc_("fuelTicket");
						dc.doReloadPage();
						return window;
	}
	
	,_when_called_from_dshboard_: function(id) {
		
						var dc = this._getDc_("fuelTicket");
						dc.doCancel();
						dc.doClearAllFilters();
						dc.setFilterValue("id", id);				
						dc.doQuery({showEdit: true}); 
	}
	
	,_deleteFuelTicket_: function() {
		
						var f = this._getDc_("fuelTicket");
						var a = this._getDc_("archivedFuelTicket");
						Ext.Msg.show({
						    title: "Warning",
						    msg: "Do you really want to delete the selected record?",
						    buttons: Ext.MessageBox.YES + Ext.MessageBox.NO,
						    fn: function(btnId) {
						        if (btnId == "yes") {
						            var successFn = function(dc,response) {								
										f.doReloadPage();
										a.doReloadPage();
									};
									var failureFn = function(dc,response) {
										Main.rpcFailure(response);
									}; 
									var o={
										name:"delete",
										callbacks:{
											successFn: successFn,
											successScope: this,
											failureFn: failureFn,
											failureScope: this
										},
										modal:true
									};
									f.doRpcDataList(o);
						        }
						    },
							buttonText: {
				                yes: "Ok", no: "Cancel"
				            },
						    scope: this
						});
	}
	
	,openApproveWindow: function() {
		
						var label = "Please provide an approval reason.";
						var title = "Approve";
						this.setupNoteWindow("wdwApproveNote","approveNote","btnSaveCloseApproveWdw","btnCancelApproveWdw" , label , title);		
	}
	
	,openRejectWindow: function() {
		
						var label = "Please provide a reason for rejection.";
						var title = "Reject";
						this.setupNoteWindow("wdwRejectNote","rejectNote","btnSaveCloseRejectWdw","btnCancelRejectWdw" , label , title);	
	}
	
	,setupNoteWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var approvalNoteField = this._get_(theForm)._get_("approvalNote");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(true);
						
						approvalNoteField.fieldLabel = label;
						approvalNoteField.setValue("");
						window.title = title;
						
						approvalNoteField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
								cancelButton.setDisabled(true);
						    }
						});
						
						if ("wdwApprovalNote" == theWindow) {
							var view = this._get_("documentsAssignList");
							view.store.load();
						}
						window.show();
	}
	
	,approveRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwApproveNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("fuelTicket");
		                var t = {
					        name: "approve",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,rejectRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								//close window
								this._getWindow_("wdwRejectNote").close();
								
								//reload page
								dc.doReloadPage();
							}
						};
		
		                var dc = this._getDc_("fuelTicket");
		                var t = {
					        name: "reject",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,_onShow_from_externalInterfaceMessageHistory_: function(params) {
			
						var dc = this._getDc_("fuelTransaction");
						dc.setFilterValue("id", params.objectId);
						dc.doQuery({queryFromExternalInterface: true});
	}
	
	,_showResult_: function(response) {
		
						var params = Ext.getResponseDataInJSON(response).params;
						if (!Ext.isEmpty(params.generatorMessage)) {
							Main.info(params.generatorMessage);
						}
	}
	
	,_onShow_refFuleTicket_: function(params) {
		
						var dc = this._getDc_("fuelTicket");
						dc.doClearAllFilters();
						dc.setFilterValue("ticketNo", params.refFuelTicket);				
						dc.doQuery({showFilteredEdit: true});
						Main.working();
	}
	
	,_purgeAllArchivedTickets_: function() {
		
						Ext.MessageBox.show({
					        msg: Main.translate("applicationMsg","purgeAllArchivedTickets__lbl"),
					        buttons: Ext.MessageBox.OKCANCEL,
							scope: this,
					        fn: function(btn){
					            if(btn == "ok"){
					            	this._doDeleteAllArchivedTickets_();
					            } else {
					                return;
					            }
					        }
					    }, this);
	}
	
	,_doDeleteAllArchivedTickets_: function() {
		
		
						var dc = this._getDc_("archivedFuelTicket");
						var ids = [];
			
						Ext.each(dc.store.getRange(), function(n) {
							var o = {
								id : n.data.id,
								isDeleted: true
							};
							ids.push(o);
						}, this);
			
						Ext.Ajax.request({
				            url: Main.dsAPI(dc.dsName, "json").destroy,
				            method: "POST",
				            scope: this,
				            params: {
				              data: JSON.stringify(ids)
				            },
				            success: function(response, opts) {
				            	dc.doReloadPage();
				            }
						});
	}
	
	,_purgeArchivedAndReturnToList_: function() {
		
						var dc = this._getDc_("archivedFuelTicket");
						dc.doDelete({returnToList: true});
	}
	
	,_showArchivedTicketDetails_: function() {
		
						this._showStackedViewElement_("ArchiveStackedPanel", "canvas5");
	}
	
	,_backToArchivedList_: function() {
		
						this._showStackedViewElement_("ArchiveStackedPanel", "canvas4");
	}
	
	,_process_: function() {
		
						var o={
							name:"processList",
							showWorking: false
						};
						this._getDc_("fuelTransaction").doRpcDataList(o);
	}
	
	,_processEdit_: function() {
		
						var dc = this._getDc_("fuelTransaction");
						var o={
							name:"processList",
							showWorking: false
						};
						dc.doRpcDataList(o);
						if(dc.store.totalCount == 1){
							dc.doEditOut();
						}
	}
	
	,_undoArchived_: function() {
		
						var dc = this._getDc_("archivedFuelTicket");
						var r = dc.getRecord();
			
						if (r) {
							var isDeleted = r.get("isDeleted");
							if (isDeleted == true) {
								r.set("isDeleted", false);
								dc.doSave({returnToList: true});
							}
						}
	}
	
	,_setDefaultLovValue_: function(viewName,fieldToGet,valueToSet,fieldToReturn,fielToReturnValueIn,opt) {
		
		 
		                // -------------- Function config --------------
		 
		                var dcView = this._get_(viewName);
		 
		                // -------------- End function config --------------
		 
		                var field = dcView._get_(fieldToGet);
		                var valueField = field.valueField;
		                var record = dcView._controller_.getRecord();
		 
		                var store = field.store;
		
		                store.load({
		                    callback: function(rec) {
		
		                        var i = 0, l = rec.length;
		
								if (fieldToReturn instanceof Array) {
									var valueToReturn = [];
								}
								else {
									var valueToReturn = "";
								}
		                        
		                        for (i ; i < l; i++) {
		                            var data = rec[i].data;
		                            for (var key in data) {
		                                if (data[valueField] == valueToSet) {
		
											if (fieldToReturn instanceof Array) {
												for (var x = 0; x<fieldToReturn.length; x++) {
													valueToReturn.push(data[fieldToReturn[x]]);
												}
											}
											else {
												valueToReturn = data[fieldToReturn];
											}
		                                    break;
		                                }
		                            }
		                        }
		
		                        if (record) {
		                            record.beginEdit();
		 
		                            record.set(field.dataIndex, valueToSet);
									if (fielToReturnValueIn instanceof Array) {
										for (var z = 0; z<fielToReturnValueIn.length; z++) {
											record.set(fielToReturnValueIn[z], valueToReturn[z]);
										}
									}
									else {
										record.set(fielToReturnValueIn, valueToReturn);
									}
		                            
		                            if (opt) {
		                                for (var key in opt) {
		                                    record.set(key, opt[key]);
		                                }
		                            }
		                            record.endEdit();
		                        }
		                    }
		                },this);
	}
	
	,_saveAndCloseNewRegistrationWdw_: function() {
		
						// wdwNewAircraftRegistration
						var dc = this._getDc_("aircraft");
						dc.doSave({closeNewAircraftWdw: true});
	}
	
	,setupHistoryWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(false);
						remarksField.setReadOnly(false);
						
						remarksField.fieldLabel = label;
						window.title = title;
						
						window.show();
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
	}
	
	,_deleteTicket_: function() {
		
						var dc = this._getDc_("fuelTicket");
						var r = dc.getRecord();
			
						if (r) {
							var isDeleted = r.get("isDeleted");
							if (isDeleted == false) {
								r.set("isDeleted", true);
								dc.doSave({returnToList: true});
							}
						}
	}
	
	,rejectList: function() {
		
						// Setup the reset window
						var label = "You are about to reject the fuel ticket.<br/> Please provide the reason for the rejection.";
						var title = "Reject Fuel Ticket";
						this.setupHistoryWindow("wdwRejectReasonList","fuelTicketRemarkEditList","btnSaveCloseRejectHistoryWdwList","btnRevokeCancelList" , label , title);				
	}
	
	,reject: function() {
		
						// Setup the reset window
						var label = "You are about to reject the fuel ticket.<br/> Please provide the reason for the rejection.";
						var title = "Reject Fuel Ticket";
						this.setupHistoryWindow("wdwRejectReason","fuelTicketRemarkEdit","btnSaveCloseRejectHistoryWdw","btnRevokeCancel" , label , title);				
	}
	
	,canRelease: function(dc) {
		 
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if((record.data.ticketStatus === __OPS_TYPE__.FuelTicketStatus._CANCELED_) || record.data.approvalStatus === __OPS_TYPE__.FuelTicketApprovalStatus._OK_ ){
								return false;			
							}
						}
						return true;
	}
	
	,canApproveReject: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var data = selectedRecords[i].data;
							if(data.approvalStatus === __OPS_TYPE__.FuelTicketApprovalStatus._AWAITING_APPROVAL_ && data.canBeCompleted == true) {
								return true;
							}
						}
						return false;
	}
	
	,canDelete: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var data = selectedRecords[i].data;
							if((data.approvalStatus === __OPS_TYPE__.FuelTicketApprovalStatus._OK_  && data.ticketStatus != __OPS_TYPE__.FuelTicketStatus._CANCELED_ )
								||data.invoiceStatus !=__OPS_TYPE__.OutgoingInvoiceStatus._NOT_INVOICED_ 
								|| data.billStatus !=__CMM_TYPE__.BillStatus._NOT_BILLED_ 
								|| ( this.useWorkflowForApproval() && data.approvalStatus === __OPS_TYPE__.FuelTicketApprovalStatus._AWAITING_APPROVAL_) ){
								return false;
							}
						}
						return true;
	}
	
	,canRevoke: function(dc) {
		
						var selectedRecords = dc.selectedRecords;		
						var canRevoke = true;		
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.ticketStatus === __OPS_TYPE__.FuelTicketStatus._CANCELED_ || record.data.approvalStatus !== __OPS_TYPE__.FuelTicketApprovalStatus._OK_ ) {
								canRevoke = false;			
							}
						}
						return canRevoke;
	}
	
	,canCancel: function(dc) {
		
						return !Ext.isEmpty(dc.getParamValue("remark"));
	}
	
	,canSubmit: function(dc) {
		
						var selectedRecords = dc.selectedRecords;
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.approvalStatus !== __TYPES__.fuelTicket.approvalStatus.ok || !(record.data.ticketStatus===__TYPES__.fuelTicket.ticketStatus.original || record.data.ticketStatus===__TYPES__.fuelTicket.ticketStatus.updated || record.data.ticketStatus===__TYPES__.fuelTicket.ticketStatus.canceled) || !(record.data.transmissionStatus === __TYPES__.fuelTicket.transmissionStatus.nevv || record.data.transmissionStatus === __TYPES__.fuelTicket.transmissionStatus.failed) ){
								return false;			
							}
						}
						return true;
	}
	
	,canReset: function(dc) {
		
						var selectedRecords = dc.selectedRecords;				
						for(var i=0 ; i<selectedRecords.length ; ++i){
							var record = selectedRecords[i];
							if(record.data.ticketStatus !== __TYPES__.fuelTicket.ticketStatus.canceled || record.data.invoiceStatus !== __TYPES__.fuelTicket.invoiceStatus.notInvoiced || record.data.billStatus !== __TYPES__.fuelTicket.billStatus.notBilled){
								return false;			
							}
						}
						return true;
	}
	
	,canReissue: function(dc) {
		
						var canceled = ( dc.record.data.ticketStatus == __TYPES__.fuelTicket.ticketStatus.canceled );
						var wkfCheckOk = true;
						if ( this.useWorkflowForApproval() ) {
							wkfCheckOk = !(dc.record.data.approvalStatus === __OPS_TYPE__.FuelTicketApprovalStatus._AWAITING_APPROVAL_);
						}
						return canceled && wkfCheckOk;
	}
	
	,_when_called_from_appMenu_: function() {
		
						var dc = this._getDc_("fuelTicket");
						dc.doEditOut({doNewAfterEditOut:true});
	}
	
	,_when_called_from_delivery_notes_: function(params) {
		
		
						var dc = this._getDc_("fuelTicket");
		
						dc.doEmptyCtxFilter();
						dc.doClearAllFilters();
		
						this._showStackedViewElement_("main", "canvas2");
						dc.setFilterValue("id", params.id);
						dc.doQuery();
	}
	
	,_afterLinkElementsPhase2_: function() {
		
						this._linkedDone_ = true;
						this._recordChange_();
	}
	
	,_afterDefineDcs_: function() {
		
		
						var fuelTicket = this._getDc_("fuelTicket");	
						var aircraft = this._getDc_("aircraft");
						var archivedFuelTicket = this._getDc_("archivedFuelTicket");
						var note = this._getDc_("note");
						var bufferedNote = this._getDc_("bufferedNote");
						var fuelTransEquipment = this._getDc_("fuelTransEquipment");
						var fuelTransaction = this._getDc_("fuelTransaction");
						this.validationStatusTooltipId = Ext.id();
						this._linkedDone_ = null;
						
						fuelTicket.on("recordReload", function(dc){
							this._applyStateAllButtons_();
						}, this);
		
						fuelTicket.on("refreshButtonState", function(dc){
							this._applyStateAllButtons_();
						}, this);
			
						fuelTicket.on("afterDoQuerySuccess", function(dc, ajaxResult) {
							Main.workingEnd();
							if (ajaxResult.options.showEdit == true) {
								this._showStackedViewElement_("main", "canvas2");
							}
						}, this);
			
						fuelTransaction.on("afterDoQuerySuccess", function(dc, ajaxResult) {
							if(ajaxResult.options.queryFromExternalInterface === true) {
								if (ajaxResult.records.length != 0) {
									this._showStackedViewElement_("main", "FuelTransactionList");
								} else {
									Main.info(Main.translate("applicationMsg", "externalInterfaceViewNoObj__lbl"));
									return;
								}
							}
						}, this);
			
						archivedFuelTicket.on("afterDoDeleteSuccess", function(dc, ajaxResult){
							if (ajaxResult.options.options.returnToList == true) {
								this._backToArchivedList_();
							}
						}, this);
			
						archivedFuelTicket.on("afterDoSaveSuccess", function(dc, ajaxResult){
							if (ajaxResult.options.options.returnToList == true) {
								this._backToArchivedList_();
								dc.doReloadPage();
							}
						}, this);
			
						aircraft.on("afterDoSaveSuccess", function(dc, ajaxResult){
							if (ajaxResult.options.options.closeNewAircraftWdw == true) {
								var w = this._getWindow_("wdwNewAircraftRegistration");
			
								var callbackFn = function() {
								}
								
								var data = Ext.getResponseDataInJSON(ajaxResult.response).data; 
								var registration = data[0].registration;
								this._setDefaultLovValue_("Edit", "registration", registration, ["id","acTypeId","acTypeCode"], ["aircraftId","acTypeId","acTypeCode"]);
								w.close();
							}
						}, this);	
			
						fuelTransaction.on("recordChange", function() {
							if(this._linkedDone_ === true){
								Ext.defer(this._recordChange_, 100, this);
							}
						}, this);
			
						aircraft.on("afterDoNew", function(dc, ajaxResult){
							if (!Ext.isEmpty(ajaxResult.setValidFromValidTo) && ajaxResult.setValidFromValidTo == true) {
								var r = dc.getRecord();
								if (r) {
			
									r.set("validFrom", new Date(2000,0,1));
									r.set("validTo", new Date(2020,0,1));
			
									var fuelTicketDc = this._getDc_("fuelTicket");
									var fuelTicketRec = fuelTicketDc.getRecord();
			
									if (fuelTicketRec) {
										var customerCode = fuelTicketRec.get("custCode");
										var custId = fuelTicketRec.get("custId");
										r.set("custCode",customerCode);
										r.set("custId",custId);
										r.set("operId",custId);
										r.set("ownerId",custId);
									}	
									
									
								}
							}
						}, this);		
			
						fuelTicket.on("afterDoServiceSuccess", function(dc, response, name, options) {
							this._applyStateAllButtons_();
						}, this);
			
						fuelTransaction.on("afterDoServiceSuccess", function(dc, response, name, options) {
							dc.doReloadPage();
						}, this);
						
						// ########################################################################################
						// La click pe Back, dupa ce s-a facut "New", daca nu se reincarca DC-ul parinte, in lista
						// va aparea record-ul nou creat care nu ia in considerare filtrul aplicat
						// ########################################################################################
						
						fuelTicket.on("onEditOut", function(dc, ajaxResult) {
								dc.doQuery();
						        // Dan: wait until the store is loaded and after call the doNew() method
								dc.store.on("load", function() {
									if (ajaxResult.doNewAfterEditOut === true) {
										dc.doNew();
										ajaxResult.doNewAfterEditOut = false;
									}
								}, this);
			
						}, this);
			
						fuelTicket.on("openHardCopy", function(){
							this._hardCopy_();
						},this);
						
						
						//***************************************************** Set the deafault values *******************************//
						fuelTicket.on("afterDoNew", function(dc) {			
						
						    var t = {
						        name: "getDefaultValues",
								modal: true,
						        callbacks: {
						            successFn: this.onShowWdw,
						            successScope: this,
						            failureFn: this.onShowWdw,
						            failureScope: this
						        }
						    };
						    dc.doRpcData(t);
						}, this);
				
						//******************************************* Save fuel ticket ***********************************************//
			
						fuelTicket.on("afterDoSaveSuccess", function(dc, ajaxResult) {
						    if (ajaxResult.options.options.doNewAfterSave === true) {
						        dc.doNew();
						    } else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
						        this._getWindow_("wdwFuelTicket").close();					
						    } else if (ajaxResult.options.options.doCloseEditAfterSave === true) {
						        this._getWindow_("wdwFuelTicket").close();
						        fuelTicket.doEditIn();
						    }
							this._applyStateAllButtons_();
						    var message = dc.getRecord().get("toleranceMessage");
						    if (!Ext.isEmpty(message)) {
						        
						        Main.warning(message);
						    }
						}, this);
			
						//*************************************** Enable dissable delete **********************************************//
						fuelTicket.on("recordchange" , function(dc){
							var rec = fuelTicket.record ;
							if(rec){
								var invoiceStatus = fuelTicket.record.data.invoiceStatus;
								var billStatus = fuelTicket.record.data.billStatus;
		
								if(invoiceStatus == __TYPES__.fuelTicket.invoiceStatus.notInvoiced && billStatus == __TYPES__.fuelTicket.billStatus.notBilled){
									fuelTicket.canDoDelete = true;						
									fuelTicket.updateDcState();	
								}else{
									fuelTicket.canDoDelete = false;						
									fuelTicket.updateDcState();	
								}
							}
						},this); 
		
						fuelTicket.on("recordChange", function (obj) {
							this._onFuelTicketRecordChange_(obj.dc);
			            }, this);
						
						// Daca exista actiune new si actiune edit, doNew-ul invoka automat si doEditIn
						// Deschiderea ferestrei e asociata mai jos cu evenimentul de afterDoEditIn.
						// Nu e necesar si pe  afterDoNew daca nu exista alte opertaii in plus de facut
						// ##################################################################################
						// Edit Andras: doEditIn nu funtioneaza daca avem n canvasuri, asadar mai bine impart pe 2 actiuni separate: afterDoNew si onEditIn
						
						note.on("afterDoNew", function() {
						    this._getWindow_("noteWdw").show();
						}, this);
			
						note.on("onEditIn", function() {
						    this._getWindow_("noteWdw").show();
						}, this);
						
						note.on("afterDoSaveSuccess", function(dc, ajaxResult) {
						    if (ajaxResult.options.options.doNewAfterSave === true) {
						        dc.doNew();
						    } else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
						        this._getWindow_("noteWdw").close();
						    }
						}, this);
			
						fuelTransEquipment.on("onEditIn", function(){
							 this._getWindow_("wdwEquipment").show();
						}, this);
			
						bufferedNote.on("afterDoNew", function() {
						    this._getWindow_("noteWdwTrans").show();
						}, this);
			
						bufferedNote.on("onEditIn", function() {
						    this._getWindow_("noteWdwTrans").show();
						}, this);
		
						
					
	}
	
	,_recordChange_: function() {
		
						var fuelTransaction = this._getDc_("fuelTransaction");
						var r = fuelTransaction.getRecord();
						var view = this._get_("FuelTransactionEdit");
						var counterElId = view._validationStatusCounterId_;
						var counterEl = document.getElementById(counterElId);
						var validationStatus = view._get_("validationStatus");		
						var toolTip = Ext.getCmp(this.validationStatusTooltipId);
						var ctx = this;
					
						if (toolTip) {
							toolTip.destroy();
						}
						
						if (r) {
		
							var status = r.get("validationState");
							var validationResult = r.get("validationResult");
							var resultArray = validationResult.split(" | ");
		
							if (status == __OPS_TYPE__.FuelTicketTransmissionStatus._FAILED_ ) {
		
								counterEl.style.display = "block";
								counterEl.innerHTML = resultArray.length;
		
								Ext.create("Ext.tip.ToolTip", {
									id: ctx.validationStatusTooltipId,
							        target: validationStatus.getEl(),
							        html: resultArray.join("<br>")
							    });
							}
							else {
								counterEl.style.display = "none";
								counterEl.innerHTML = "";
								if (toolTip) {
									toolTip.destroy();
								}
							}
						}
	}
	
	,saveNew: function() {
		
					var fuelTicket = this._getDc_("fuelTicket");
					fuelTicket.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var fuelTicket = this._getDc_("fuelTicket");
					fuelTicket.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveEdit: function() {
		
					var fuelTicket = this._getDc_("fuelTicket");
					fuelTicket.doSave({doCloseEditAfterSave:true});
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/FuelTickets.html";
					window.open( url, "SONE_Help");
	}
	
	,onShowWdw: function() {
		
						var window = this._getWindow_("wdwFuelTicket");
						window.show();
	}
	
	,onWdwFuelTicketClose: function() {
		
						var dc = this._getDc_("fuelTicket");
						dc.doCancel();
	}
	
	,_onWdwdFuelTicketShow_: function() {
		
						var dc = this._getDc_("fuelTicket");
						var r = dc.getRecord();
						if (r) {
							r.set("customer",null);
						}
	}
	
	,onWdwEquipmentClose: function() {
		
						var dc = this._getDc_("fuelTransEquipment");
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,saveCloseNote: function() {
		
		
					var note = this._getDc_("note");
					note.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,showError: function(response) {
		
		        		var res = Ext.getResponseDataInJSON(response).params;
						var responseResult = res.approveResult;
		
						if(responseResult == false) {
							Main.error(res.approveErrorDescription);
						}
	}
	
	,_isEditable_: function(data) {
		
						return ( data.ticketStatus!=__TYPES__.fuelTicket.ticketStatus.canceled ) ;
	}
	
	,showHardCopy: function() {
		
		                var dc = this._getDc_("fuelTicket");
		                var fn = dc.params.get("hardCopy");
		                var r = window.open(Main.urlDownload + "/" + fn,"HardCopy");
		                r.focus();                              
	}
	
	,openSubmitForApprovalWindow: function() {
		
						var label = "Add your note to be sent to your request to the approvers";
						var title = "Submit for approval";
						this.setupPopUpWindow("wdwApprovalNote","approvalNote","btnSaveCloseApprovalWdw","btnCancelApprovalWdw" , label , title);				
	}
	
	,canSubmitForApproval: function(dc) {
		
						var record = dc.getRecord();
						if(record) {
							var status = record.data.approvalStatus;
							if (status === __OPS_TYPE__.FuelTicketApprovalStatus._RECHECK_ 
									|| status === __OPS_TYPE__.FuelTicketApprovalStatus._REJECTED_
									|| status === __OPS_TYPE__.FuelTicketApprovalStatus._FOR_APPROVAL_) {
									return true;
								}
						}
						return false;
	}
	
	,useWorkflowForApproval: function() {
		
						return _SYSTEMPARAMETERS_.sysfuelticketapproval === "true" ;
	}
	
	,onApprovalWdwShow: function() {
		
						var dc = this._getDc_("attachment");
						dc.doQuery();
	}
	
	,submitForApprovalRpc: function(param) {
		
						var callbackFn = function(dc,response,serviceName,specs) {
							var responseData = Ext.getResponseDataInJSON(response);
							if (responseData && responseData.params) {
								var responseDescription = responseData.params.submitForApprovalDescription;							
								var responseResult = responseData.params.submitForApprovalResult;
								
								//close window
								this._getWindow_("wdwApprovalNote").close();
								
								//display results (only if error)
								if (responseResult == false) {
									Main.error(responseDescription);
								}
								else {
									//reload page
									dc.doReloadPage();
								}
							}
						};
		
						this._sendAttachments_();
		                var dc = this._getDc_("fuelTicket");
		                var t = {
					        name: "submitForApproval",
							modal: true,
					        callbacks: {
					            successFn: callbackFn,
					            successScope: this,
					            failureFn: callbackFn,
					            failureScope: this
					        }
					    };
					    dc.doRpcData(t);
	}
	
	,setupPopUpWindow: function(theWindow,theForm,saveBtn,cancelBtn,label,title) {
		
						var window = this._getWindow_(theWindow);
						var remarksField = this._get_(theForm)._get_("approvalNote");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						var ctx = this;
						
						saveButton.setDisabled(true);
						
						remarksField.fieldLabel = label;
		
						remarksField.setValue("");
						window.title = title;
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
								cancelButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
						var fuelTicketDc = this._getDc_("fuelTicket");
		
						//set the default remark as the reason
						var view = this._get_("documentsAssignList");
						view.store.load();
						var firstSelectedPrice = fuelTicketDc.selectedRecords[0];
						if( firstSelectedPrice) {
							remarksField.setValue(firstSelectedPrice.data.reason);
						}
						window.show();
	}
	
	,_sendAttachments_: function() {
		
						var list = this._get_("documentsAssignList");
						var view = list.getView();
						var selectedAttachments = [];
						var dc = this._getDc_("fuelTicket");
						if (view.getSelectionModel().hasSelection()) {
						   var rows = view.getSelectionModel().getSelection();
						   Ext.each(rows, function(row) {
								selectedAttachments.push(row.data.id);
						   }, this);
						}
						dc.setParamValue("selectedAttachments",JSON.stringify(selectedAttachments));
	}
	
	,_onFuelTicketRecordChange_: function(fuelTicket) {
		
						if (this.useWorkflowForApproval()) {
							this._makeFuelTicketReadOnlyOnAwaitingApproval_ (fuelTicket);
						}
	}
	
	,_makeFuelTicketReadOnlyOnAwaitingApproval_: function(fuelTicket) {
		
						var record = fuelTicket.getRecord();
						if(record) {
							//attachments section should be editable
							var setChildrenState = function(state) {
								var childrenDcs = fuelTicket.children;
								for (var i = 0; i<childrenDcs.length; i++) {
									if (childrenDcs[i]._instanceKey_ != "attachment") {
										childrenDcs[i].readOnly = state;
									}							
								}
							}
		
							var approvalStatus = record.get("approvalStatus");
		
							if (approvalStatus == __OPS_TYPE__.FuelTicketApprovalStatus._AWAITING_APPROVAL_) {
								fuelTicket._maskEditable_ = true;
								setChildrenState(true);
							}
							else { 	
								fuelTicket._maskEditable_ = false;					
								setChildrenState(false);
							}
		
							//note section should be editable
							var noteDchildDc = this._getDc_("note");
							noteDchildDc.readOnly=false;
						}
	}
	
	,_afterDefineElements_: function() {
		
						var popUp = this._getElementConfig_("PopUp"); 
						if( popUp ){
							popUp._shouldDisableIfDcIsMasked_ = false;
						}
		
						var approveNote = this._getElementConfig_("approveNote");
		                var rejectNote = this._getElementConfig_("rejectNote");
		
		                if( approveNote ){
		                    approveNote._shouldDisableIfDcIsMasked_ = false;
		                }
		
		                if( rejectNote ){
		                    rejectNote._shouldDisableIfDcIsMasked_ = false;
		                }
	}
});
