/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.PriceCategory_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ops.ui.extjs.ds.PriceCategories_DsParam,
	recordModel: atraxo.ops.ui.extjs.ds.PriceCategories_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.PriceCategory_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_PriceCategory_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"name", dataIndex:"name", maxLength:100})
			.addLov({name:"priceCategory", dataIndex:"priceCtgryName", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.PriceCategoriesLov_Lov", selectOnFocus:true, maxLength:32}})
			.addLov({name:"pcType", dataIndex:"pcType", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.PriceCategoriesTypeLov_Lov", selectOnFocus:true, maxLength:32}})
			.addBooleanField({ name:"restrictionsParameter", dataIndex:"restriction"})
			.addLov({name:"currency", dataIndex:"currencyCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3}})
			.addLov({name:"unit", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2}})
		;
	}

});

/* ================= GRID: PriceCategoryList ================= */

Ext.define("atraxo.ops.ui.extjs.dc.PriceCategory_Dc$PriceCategoryList", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_PriceCategory_Dc$PriceCategoryList",
	_noPaginator_: true,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"name", dataIndex:"name", width:200})
		.addTextColumn({ name:"priceCategory", dataIndex:"priceCtgryName", width:200})
		.addTextColumn({ name:"pcType", dataIndex:"pcType", width:120})
		.addBooleanColumn({ name:"restrictionsParameter", dataIndex:"restriction", width:200})
		.addNumberColumn({ name:"price", dataIndex:"price", width:120, decimals:6,  renderer:function(val, meta, record, rowIndex) { return this._markUtilization_(val, meta, record, rowIndex); }})
		.addTextColumn({ name:"currency", dataIndex:"currencyCode", width:80})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:80})
		.addNumberColumn({ name:"convertedPrice", dataIndex:"convertedPrice", width:120, sysDec:"dec_prc"})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._currencyParamName_ = "settlementCurrencyCode";
		this._unitParamName_ = "settlementUnitCode";
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_markUtilization_: function(value,meta,record,rowIndex) {
		
		
						var used = record.get("used");
		
						if (!Ext.isEmpty(value)) {
							if(used == true) {
					        	meta.style = "font-weight:bold !important";
						    } 
							return Ext.util.Format.number(value, Main.getNumberFormat(2));
						}
	}
});

/* ================= EDIT FORM: Totals ================= */

Ext.define("atraxo.ops.ui.extjs.dc.PriceCategory_Dc$Totals", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_PriceCategory_Dc$Totals",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDisplayFieldNumber({ name:"total", bind:"{p.total}", paramIndex:"total", maxLength:19, labelWidth:150, labelAlign:"left", decimals:_SYSTEMPARAMETERS_.dec_crncy, labelStyle:"font-weight:bold; text-transform:uppercase; white-space:nowrap" })
		.addDisplayFieldText({ name:"curr", bind:"{p.settlementCurrencyCode}", paramIndex:"settlementCurrencyCode", maxLength:3, hideLabel:"true", fieldStyle:"padding-left:5px"})
		.addDisplayFieldText({ name:"unit", bind:"{p.settlementUnitCode}", paramIndex:"settlementUnitCode", maxLength:2, hideLabel:"true", fieldStyle:"padding-left:5px"})
		.addDisplayFieldText({ name:"slash", bind:"{d.slash}", dataIndex:"slash", width:10, maxLength:1, labelWidth:10})
		.add({name:"cf", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("total"),this._getConfig_("curr"),this._getConfig_("slash"),this._getConfig_("unit")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"row", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"col", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["row"])
		.addChildrenTo("row", ["col"])
		.addChildrenTo("col", ["cf"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterApplyStates_: function() {
		
		
						this._controller_.on("afterDoQuerySuccess", function(dc) {
		
							var currency = dc.getParamValue("settlementCurrencyCode");
							var unit = dc.getParamValue("settlementUnitCode");
							var record = dc.getRecord();
							
							if (record) {
								var contractUnit = record.get("unitCode");
								var contractCurrency = record.get("currencyCode");
								var curr = this._get_("curr");
								var unit = this._get_("unit");
		
								if (Ext.isEmpty(curr.getValue())) {
									curr.setValue(contractCurrency);
								}
								if (Ext.isEmpty(unit.getValue())) {
									unit.setValue(contractUnit);
								}
							}
						},this);
		
	}
});

/* ================= EDIT FORM: PriceCategoryDetails ================= */

Ext.define("atraxo.ops.ui.extjs.dc.PriceCategory_Dc$PriceCategoryDetails", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_PriceCategory_Dc$PriceCategoryDetails",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"name", bind:"{d.name}", dataIndex:"name", noEdit:true , maxLength:100})
		.addTextField({ name:"priceCategory", bind:"{d.priceCtgryName}", dataIndex:"priceCtgryName", noEdit:true , maxLength:32})
		.addBooleanField({ name:"continous", bind:"{d.continous}", dataIndex:"continous", noEdit:true })
		.addTextField({ name:"quantityType", bind:"{d.quantityType}", dataIndex:"quantityType", noEdit:true , maxLength:32})
		.addTextField({ name:"vat", bind:"{d.vat}", dataIndex:"vat", noEdit:true , maxLength:32, labelWidth:50})
		.addBooleanField({ name:"includeAveragePrice", bind:"{d.includeInAverage}", dataIndex:"includeInAverage", noEdit:true , width:300, labelWidth:200})
		.addDisplayFieldText({ name:"conversion", bind:"{d.conversion}", dataIndex:"conversion", noEdit:true , maxLength:100, labelWidth:250, labelAlign:"left"})
		.addBooleanField({ name:"default", bind:"{d.defaultPriceCtgy}", dataIndex:"defaultPriceCtgy", noEdit:true , width:200})
		.addTextField({ name:"forex", bind:"{d.forex}", dataIndex:"forex", noEdit:true , maxLength:255})
		.addTextField({ name:"period", bind:"{d.exchangeRateOffset}", dataIndex:"exchangeRateOffset", noEdit:true , maxLength:32})
		.addTextArea({ name:"comments", bind:"{d.comments}", dataIndex:"comments", noEdit:true })
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("name"),this._getConfig_("priceCategory"),this._getConfig_("vat"),this._getConfig_("quantityType")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("continous"),this._getConfig_("includeAveragePrice"),this._getConfig_("default")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("conversion")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("forex"),this._getConfig_("period")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:1200, layout:"anchor"})
		.addPanel({ name:"col2", width:900, layout:"anchor"})
		.addPanel({ name:"col5", width:620, layout:"anchor"})
		.addPanel({ name:"col6", width:600, layout:"anchor"})
		.addPanel({ name:"col7", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col5", "col6", "col7"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col5", ["row5"])
		.addChildrenTo("col6", ["row6"])
		.addChildrenTo("col7", ["comments"]);
	}
});
