/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelTransactionEquipment_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_FuelTransactionEquipment_Ds"
	},
	
	
	fields: [
		{name:"fuelTransId", type:"int", allowNull:true},
		{name:"fuelingEquipmentId", type:"string"},
		{name:"fuelingType", type:"string"},
		{name:"avgFuelTemperature", type:"float", allowNull:true},
		{name:"temperatureUOM", type:"string"},
		{name:"densityType", type:"string"},
		{name:"density", type:"float", allowNull:true},
		{name:"densityUOM", type:"string"},
		{name:"meterReadingStart", type:"float", allowNull:true},
		{name:"meterReadingEnd", type:"float", allowNull:true},
		{name:"meterQuantityDelivered", type:"float", allowNull:true},
		{name:"meterQuantityUOM", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelTransactionEquipment_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"fuelTransId", type:"int", allowNull:true},
		{name:"fuelingEquipmentId", type:"string"},
		{name:"fuelingType", type:"string"},
		{name:"avgFuelTemperature", type:"float", allowNull:true},
		{name:"temperatureUOM", type:"string"},
		{name:"densityType", type:"string"},
		{name:"density", type:"float", allowNull:true},
		{name:"densityUOM", type:"string"},
		{name:"meterReadingStart", type:"float", allowNull:true},
		{name:"meterReadingEnd", type:"float", allowNull:true},
		{name:"meterQuantityDelivered", type:"float", allowNull:true},
		{name:"meterQuantityUOM", type:"string"},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
