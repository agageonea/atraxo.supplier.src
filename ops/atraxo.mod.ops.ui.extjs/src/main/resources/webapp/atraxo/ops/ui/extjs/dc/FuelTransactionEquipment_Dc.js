/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelTransactionEquipment_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ops.ui.extjs.ds.FuelTransactionEquipment_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FuelTransactionEquipment_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FuelTransactionEquipment_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addTextField({ name:"equipmentId", dataIndex:"fuelingEquipmentId", maxLength:15})
			.addCombo({ xtype:"combo", name:"fuelingType", dataIndex:"fuelingType", store:[ __CMM_TYPE__.FuelingType._RE_FUELER_, __CMM_TYPE__.FuelingType._HYDRANT_]})
			.addNumberField({name:"temperature", dataIndex:"avgFuelTemperature", sysDec:"dec_unit", maxLength:19})
			.addCombo({ xtype:"combo", name:"temperatureUnit", dataIndex:"temperatureUOM", store:[ __OPS_TYPE__.TemperatureUnit._C_, __OPS_TYPE__.TemperatureUnit._F_]})
			.addCombo({ xtype:"combo", name:"densityType", dataIndex:"densityType", store:[ __OPS_TYPE__.DensityType._MEASURED_, __OPS_TYPE__.DensityType._STANDARD_]})
			.addNumberField({name:"density", dataIndex:"density", sysDec:"dec_unit", maxLength:19})
			.addCombo({ xtype:"combo", name:"densityUOM", dataIndex:"densityUOM", store:[ __OPS_TYPE__.DensityUOM._KGM_, __OPS_TYPE__.DensityUOM._KGL_, __OPS_TYPE__.DensityUOM._LGH_]})
			.addNumberField({name:"meterReadingStart", dataIndex:"meterReadingStart", sysDec:"dec_unit", maxLength:19})
			.addNumberField({name:"meterReadingEnd", dataIndex:"meterReadingEnd", sysDec:"dec_unit", maxLength:19})
			.addNumberField({name:"meterQuantityDelivered", dataIndex:"meterQuantityDelivered", sysDec:"dec_unit", maxLength:19})
			.addLov({name:"unit", dataIndex:"meterQuantityUOM", width:50, maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"code", dsField: "meterQuantityUOM"} ]}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTransactionEquipment_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelTransactionEquipment_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"equipmentId", dataIndex:"fuelingEquipmentId", width:100})
		.addTextColumn({ name:"fuelingType", dataIndex:"fuelingType", width:100})
		.addNumberColumn({ name:"temperature", dataIndex:"avgFuelTemperature", width:130, sysDec:"dec_unit"})
		.addTextColumn({ name:"temperatureUnit", dataIndex:"temperatureUOM", width:50})
		.addTextColumn({ name:"densityType", dataIndex:"densityType", width:100})
		.addNumberColumn({ name:"density", dataIndex:"density", width:80, sysDec:"dec_unit"})
		.addTextColumn({ name:"densityUOM", dataIndex:"densityUOM", width:50})
		.addNumberColumn({ name:"meterReadingStart", dataIndex:"meterReadingStart", width:130, sysDec:"dec_unit"})
		.addNumberColumn({ name:"meterReadingEnd", dataIndex:"meterReadingEnd", width:130, sysDec:"dec_unit"})
		.addNumberColumn({ name:"meterQuantityDelivered", dataIndex:"meterQuantityDelivered", width:100, sysDec:"dec_unit"})
		.addTextColumn({ name:"meterQuantityUOM", dataIndex:"meterQuantityUOM", width:50})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTransactionEquipment_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTransactionEquipment_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"equipmentId", bind:"{d.fuelingEquipmentId}", dataIndex:"fuelingEquipmentId", width:170, maxLength:15, hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"fuelingType", bind:"{d.fuelingType}", dataIndex:"fuelingType", width:170, store:[ __CMM_TYPE__.FuelingType._RE_FUELER_, __CMM_TYPE__.FuelingType._HYDRANT_], hideLabel:"true"})
		.addNumberField({name:"temperature", bind:"{d.avgFuelTemperature}", dataIndex:"avgFuelTemperature", width:100, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"temperatureUnit", bind:"{d.temperatureUOM}", dataIndex:"temperatureUOM", width:70, store:[ __OPS_TYPE__.TemperatureUnit._C_, __OPS_TYPE__.TemperatureUnit._F_], hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"densityType", bind:"{d.densityType}", dataIndex:"densityType", width:170, store:[ __OPS_TYPE__.DensityType._MEASURED_, __OPS_TYPE__.DensityType._STANDARD_], hideLabel:"true"})
		.addNumberField({name:"density", bind:"{d.density}", dataIndex:"density", width:100, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"densityUOM", bind:"{d.densityUOM}", dataIndex:"densityUOM", width:70, store:[ __OPS_TYPE__.DensityUOM._KGM_, __OPS_TYPE__.DensityUOM._KGL_, __OPS_TYPE__.DensityUOM._LGH_], hideLabel:"true"})
		.addNumberField({name:"meterStart", bind:"{d.meterReadingStart}", dataIndex:"meterReadingStart", width:170, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addNumberField({name:"meterEnd", bind:"{d.meterReadingEnd}", dataIndex:"meterReadingEnd", width:170, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addNumberField({name:"meterQuantity", bind:"{d.meterQuantityDelivered}", dataIndex:"meterQuantityDelivered", width:100, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addLov({name:"unit", bind:"{d.meterQuantityUOM}", dataIndex:"meterQuantityUOM", width:70, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:3, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "meterQuantityUOM"} ]})
		.addDisplayFieldText({ name:"equipmentIdLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelingTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row1Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row2Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"densityTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"meterStartLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"meterEndLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row3Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("temperature"),this._getConfig_("temperatureUnit")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("density"),this._getConfig_("densityUOM")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("meterQuantity"),this._getConfig_("unit")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"mainTable", layout: {type:"table"}})
		.addPanel({ name:"table1",  style:"margin-right:50px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["mainTable"])
		.addChildrenTo("mainTable", ["table1"])
		.addChildrenTo("table1", ["equipmentIdLabel", "equipmentId", "fuelingTypeLabel", "fuelingType", "row1Label", "row1", "row2Label", "row2", "densityTypeLabel", "densityType", "meterStartLabel", "meterStart", "meterEndLabel", "meterEnd", "row3Label", "row3"]);
	}
});
