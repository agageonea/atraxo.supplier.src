/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.frame.FuelRequests_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.FuelRequests_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("fuelRequests", Ext.create(atraxo.ops.ui.extjs.dc.FuelRequest_Dc,{multiEdit: true}))
		.addDc("fuelRequestsEdit", Ext.create(atraxo.ops.ui.extjs.dc.FuelRequest_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("flightEvent", Ext.create(atraxo.ops.ui.extjs.dc.FlightEvent_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.linkDc("fuelRequestsEdit", "fuelRequests",{fetchMode:"auto",fields:[
					{childField:"id", parentField:"id"}]})
				.linkDc("attachment", "fuelRequests",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"targetType", value:"N/A"}]})
				.linkDc("flightEvent", "fuelRequestsEdit",{fetchMode:"auto",fields:[
					{childField:"fuelReqId", parentField:"id"}]})
				.linkDc("history", "fuelRequests",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "fuelRequests",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnCancelWithMenu",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, isBtnContainer:true,stateManager:[{ name:"selected_not_zero", dc:"fuelRequests", and: function(dc) {return (this.fnCancelEnabled(dc));} }], scope:this})
		.addButton({name:"btnCancelSelected",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", handler: this.onBtnCancelSelected, scope:this})
		.addButton({name:"btnCancelAll",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:false,  _btnContainerName_:"btnCancelWithMenu", _isDefaultHandler_:true, handler: this.onBtnCancelAll, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addButton({name:"helpWdwDetailsView",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwDetailsView, scope:this})
		.addButton({name:"detailsButton",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onDetailsButton,stateManager:[{ name:"selected_one", dc:"fuelRequests"}], scope:this})
		.addButton({name:"checkedButton",glyph:fp_asc.gear_glyph.glyph, disabled:true, handler: this.onCheckedButton,stateManager:[{ name:"selected_one", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.nevv );} }], scope:this})
		.addButton({name:"checkedButton1",glyph:fp_asc.gear_glyph.glyph, disabled:true, handler: this.onCheckedButton1,stateManager:[{ name:"selected_one", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.nevv );} }], scope:this})
		.addButton({name:"revokeButton",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onRevokeButton,stateManager:[{ name:"selected_not_zero", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.nevv || dc.record.data.requestStatus==__TYPES__.fuelRequests.status.processed);} }], scope:this})
		.addButton({name:"revokeButton1",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onRevokeButton1,stateManager:[{ name:"selected_not_zero", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.nevv || dc.record.data.requestStatus==__TYPES__.fuelRequests.status.processed);} }], scope:this})
		.addButton({name:"closeButton",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onCloseButton,stateManager:[{ name:"selected_not_zero", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.nevv || dc.record.data.requestStatus==__TYPES__.fuelRequests.status.processed);} }], scope:this})
		.addButton({name:"closeButton1",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onCloseButton1,stateManager:[{ name:"selected_not_zero", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.nevv || dc.record.data.requestStatus==__TYPES__.fuelRequests.status.processed);} }], scope:this})
		.addButton({name:"resetButton",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onResetButton,stateManager:[{ name:"selected_not_zero", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.closed || dc.record.data.requestStatus==__TYPES__.fuelRequests.status.canceled);} }], scope:this})
		.addButton({name:"resetButton1",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onResetButton1,stateManager:[{ name:"selected_not_zero", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.closed || dc.record.data.requestStatus==__TYPES__.fuelRequests.status.canceled);} }], scope:this})
		.addButton({name:"generateQuoteButton",glyph:fp_asc.gear_glyph.glyph, disabled:true, handler: this.onGenerateQuoteButton,stateManager:[{ name:"selected_one", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.processed);} }], scope:this})
		.addButton({name:"generateQuoteButton1",glyph:fp_asc.gear_glyph.glyph, disabled:true, handler: this.onGenerateQuoteButton1,stateManager:[{ name:"selected_one", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.processed);} }], scope:this})
		.addButton({name:"openQuoteButton",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onOpenQuoteButton,stateManager:[{ name:"selected_one", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.quoted);} }], scope:this})
		.addButton({name:"openQuoteButton1",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onOpenQuoteButton1,stateManager:[{ name:"selected_one", dc:"fuelRequests", and: function(dc) {return (dc.record.data.requestStatus==__TYPES__.fuelRequests.status.quoted);} }], scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNew",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNew, scope:this})
		.addButton({name:"btnFlightEventCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnFlightEventCancel, scope:this})
		.addButton({name:"btnResetCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnResetCancel, scope:this})
		.addButton({name:"btnResetSave",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnResetSave, scope:this})
		.addButton({name:"btnRevokeSave",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnRevokeSave, scope:this})
		.addButton({name:"btnRevokeCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnRevokeCancel, scope:this})
		.addButton({name:"btnCloseSave",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnCloseSave, scope:this})
		.addButton({name:"btnCloseCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseCancel, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addDcFilterFormView("fuelRequests", {name:"FuelRequestFilter", xtype:"ops_FuelRequest_Dc$Filter"})
		.addDcEditGridView("fuelRequests", {name:"FuelRequestEditableList", xtype:"ops_FuelRequest_Dc$EditableList", frame:true})
		.addDcFormView("fuelRequestsEdit", {name:"FuelRequestFormView", xtype:"ops_FuelRequest_Dc$FormView"})
		.addDcGridView("flightEvent", {name:"FlightEventsList", _hasTitle_:true, xtype:"ops_FlightEvent_Dc$ListRequest"})
		.addDcFormView("flightEvent", {name:"FlightEventsPopUp", xtype:"ops_FlightEvent_Dc$RequestEdit"})
		.addDcFormView("flightEvent", {name:"FlightEventsEdit", xtype:"ops_FlightEvent_Dc$RequestEdit"})
		.addDcGridView("fuelRequests", {name:"Activities", _hasTitle_:true, xtype:"ops_FuelRequest_Dc$Temp",  disabled:true})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"NotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("fuelRequests", {name:"historyEdit", xtype:"ops_FuelRequest_Dc$ChangeHistory"})
		.addDcFormView("fuelRequests", {name:"historyEdit2", xtype:"ops_FuelRequest_Dc$ChangeHistory"})
		.addDcFormView("fuelRequests", {name:"historyEdit3", xtype:"ops_FuelRequest_Dc$ChangeHistory"})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addWindow({name:"wdwFlightEvent", _hasTitle_:true, width:920, height:440, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("FlightEventsPopUp")],  listeners:{ close:{fn:this.onWdwFlightTypeClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveNew"), this._elems_.get("btnFlightEventCancel")]}]})
		.addWindow({name:"wdwReset", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("historyEdit")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnResetSave"), this._elems_.get("btnResetCancel")]}]})
		.addWindow({name:"wdwRevoke", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("historyEdit2")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnRevokeSave"), this._elems_.get("btnRevokeCancel")]}]})
		.addWindow({name:"wdwClose", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("historyEdit3")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnCloseSave"), this._elems_.get("btnCloseCancel")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NotesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"fuelRequestsEdit"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["FuelRequestEditableList"], ["center"])
		.addChildrenTo("canvas2", ["FuelRequestFormView", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["FlightEventsEdit"], ["center"])
		.addChildrenTo("detailsTab", ["FlightEventsList"])
		.addToolbarTo("FuelRequestEditableList", "tlbFuelRequestEditableList")
		.addToolbarTo("FuelRequestFormView", "tlbFuelRequestFormView")
		.addToolbarTo("FlightEventsList", "tlbFlightEventList")
		.addToolbarTo("FlightEventsEdit", "tlbFlightEventEdit")
		.addToolbarTo("NotesList", "tlbNoteList")
		.addToolbarTo("attachmentList", "tlbAttachments");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2", "canvas3"])
		.addChildrenTo2("detailsTab", ["NotesList", "History", "Activities", "attachmentList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbFuelRequestEditableList", {dc: "fuelRequests"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelWithMenu"),this._elems_.get("btnCancelSelected"),this._elems_.get("btnCancelAll"),this._elems_.get("checkedButton"),this._elems_.get("revokeButton"),this._elems_.get("closeButton"),this._elems_.get("resetButton"),this._elems_.get("generateQuoteButton"),this._elems_.get("openQuoteButton"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbFuelRequestFormView", {dc: "fuelRequestsEdit"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("checkedButton1"),this._elems_.get("revokeButton1"),this._elems_.get("closeButton1"),this._elems_.get("resetButton1"),this._elems_.get("generateQuoteButton1"),this._elems_.get("openQuoteButton1"),this._elems_.get("helpWdw1")])
			.addReports()
		.end()
		.beginToolbar("tlbFlightEventList", {dc: "flightEvent"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addCopy({iconCls:fp_asc.copy_glyph.css,glyph:fp_asc.copy_glyph.glyph,autoEdit:"false"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbFlightEventEdit", {dc: "flightEvent"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph,showView:"canvas2"}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnCancelSelected
	 */
	,onBtnCancelSelected: function() {
		this.fnCancelSelected();
	}
	
	/**
	 * On-Click handler for button btnCancelAll
	 */
	,onBtnCancelAll: function() {
		this.fnCancelAll();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwDetailsView
	 */
	,onHelpWdwDetailsView: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button detailsButton
	 */
	,onDetailsButton: function() {
		this._showStackedViewElement_("main", "canvas2"); 
	}
	
	/**
	 * On-Click handler for button checkedButton
	 */
	,onCheckedButton: function() {
		var o={
			name:"check",
			modal:true
		};
		this._getDc_("fuelRequests").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button checkedButton1
	 */
	,onCheckedButton1: function() {
		var o={
			name:"check",
			modal:true
		};
		this._getDc_("fuelRequests").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button revokeButton
	 */
	,onRevokeButton: function() {
		this.showRevokeWdw();
	}
	
	/**
	 * On-Click handler for button revokeButton1
	 */
	,onRevokeButton1: function() {
		this.showRevokeWdw();
	}
	
	/**
	 * On-Click handler for button closeButton
	 */
	,onCloseButton: function() {
		this.showCloseWdw();
	}
	
	/**
	 * On-Click handler for button closeButton1
	 */
	,onCloseButton1: function() {
		this.showCloseWdw();
	}
	
	/**
	 * On-Click handler for button resetButton
	 */
	,onResetButton: function() {
		this.showResetWdw();
	}
	
	/**
	 * On-Click handler for button resetButton1
	 */
	,onResetButton1: function() {
		this.showResetWdw();
	}
	
	/**
	 * On-Click handler for button generateQuoteButton
	 */
	,onGenerateQuoteButton: function() {
		var successFn = function() {
			var bundle = "atraxo.mod.ops";
			var frame = "atraxo.ops.ui.extjs.frame.FuelQuotation_Ui";
			getApplication().showFrame(frame,{
				url:Main.buildUiPath(bundle, frame, false),
				params: {
					fuelQuotationCode: this._getDc_("fuelRequests").getRecord().get("fuelQuotationCode")
				},
				callback: function (params) {
					this._when_called_to_edit(params);
				}
			});
		};
		var o={
			name:"generateFuelQuotation",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fuelRequests").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button generateQuoteButton1
	 */
	,onGenerateQuoteButton1: function() {
		var successFn = function() {
			var bundle = "atraxo.mod.ops";
			var frame = "atraxo.ops.ui.extjs.frame.FuelQuotation_Ui";
			getApplication().showFrame(frame,{
				url:Main.buildUiPath(bundle, frame, false),
				params: {
					fuelQuotationCode: this._getDc_("fuelRequests").getRecord().get("fuelQuotationCode")
				},
				callback: function (params) {
					this._when_called_to_edit(params);
				}
			});
		};
		var o={
			name:"generateFuelQuotation",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fuelRequests").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button openQuoteButton
	 */
	,onOpenQuoteButton: function() {
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelQuotation_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				fuelQuotationCode: this._getDc_("fuelRequests").getRecord().get("fuelQuotationCode")
			},
			callback: function (params) {
				this._when_called_to_edit(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button openQuoteButton1
	 */
	,onOpenQuoteButton1: function() {
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelQuotation_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				fuelQuotationCode: this._getDc_("fuelRequests").getRecord().get("fuelQuotationCode")
			},
			callback: function (params) {
				this._when_called_to_edit(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveNew
	 */
	,onBtnSaveNew: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button btnFlightEventCancel
	 */
	,onBtnFlightEventCancel: function() {
		this.onWdwFlightTypeClose();
		this._getWindow_("wdwFlightEvent").close();
	}
	
	/**
	 * On-Click handler for button btnResetCancel
	 */
	,onBtnResetCancel: function() {
		this._getWindow_("wdwReset").close();
	}
	
	/**
	 * On-Click handler for button btnResetSave
	 */
	,onBtnResetSave: function() {
		var o={
			name:"updateStatus",
			modal:true
		};
		this._getDc_("fuelRequests").doRpcDataList(o);
		this._getWindow_("wdwReset").close();
	}
	
	/**
	 * On-Click handler for button btnRevokeSave
	 */
	,onBtnRevokeSave: function() {
		var o={
			name:"updateStatus",
			modal:true
		};
		this._getDc_("fuelRequests").doRpcDataList(o);
		this._getWindow_("wdwRevoke").close();
	}
	
	/**
	 * On-Click handler for button btnRevokeCancel
	 */
	,onBtnRevokeCancel: function() {
		this._getWindow_("wdwRevoke").close();
	}
	
	/**
	 * On-Click handler for button btnCloseSave
	 */
	,onBtnCloseSave: function() {
		var o={
			name:"updateStatus",
			modal:true
		};
		this._getDc_("fuelRequests").doRpcDataList(o);
		this._getWindow_("wdwClose").close();
	}
	
	/**
	 * On-Click handler for button btnCloseCancel
	 */
	,onBtnCloseCancel: function() {
		this._getWindow_("wdwClose").close();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,onNoteWdwClose: function() {	
		this._getDc_("note").doCancel();
	}
	
	,setupHistoryWindow: function(theWindow,theForm,saveBtn,cancelBtn) {
		
						var window = this._getWindow_(theWindow);
						window.show();
		
						var remarksField = this._get_(theForm)._get_("remarks");
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(false);
						remarksField.setReadOnly(false);
		
						remarksField.on("change", function(field) {
							if (field.value.length > 0) {
								saveButton.setDisabled(false);
								cancelButton.setDisabled(true);
							}	
							else {
								saveButton.setDisabled(true);
								cancelButton.setDisabled(false);
							}	
						});
	}
	
	,_reload_the_page_: function() {
		
						var dc = this._getDc_("fuelRequests");
						dc.doReloadPage();
	}
	
	,_when_called_from_appMenu_: function() {
		
						var dc = this._getDc_("fuelRequests");
						dc.doEditOut({doNewAfterEditOut:true});
	}
	
	,setAction: function(status) {
		 
						var dc = this._getDc_("fuelRequests");
						dc.setParamValue("action", status);
	}
	
	,showResetWdw: function() {
		
		
						// Setup the reset window
						this.setupHistoryWindow("wdwReset","historyEdit","btnResetSave","btnResetCancel");
		
						// Set the status for the current window
		
						this.setAction(__TYPES__.fuelRequests.status.nevv);
	}
	
	,showRevokeWdw: function() {
		
		
						// Setup the reset window
						this.setupHistoryWindow("wdwRevoke","historyEdit2","btnRevokeSave","btnRevokeCancel");
		
						// Set the status for the current window
		
						this.setAction(__TYPES__.fuelRequests.status.canceled);
	}
	
	,showCloseWdw: function() {
		
		
						// Setup the reset window
						this.setupHistoryWindow("wdwClose","historyEdit3","btnCloseSave","btnCloseCancel");
		
						// Set the status for the current window
		
						this.setAction(__TYPES__.fuelRequests.status.closed);
	}
	
	,resetDcState: function(parent,master,detail) {
		
						var record = parent.getRecord(); 
						if (record) {
							if ( this._isEditable_(record.data)) {
								master.setReadOnly(false);
								detail.setReadOnly(false);
							} else {
								master.setReadOnly(true);
								detail.setReadOnly(true);
							}
						}				
	}
	
	,_isEditable_: function(data) {
		
						return ( data.requestStatus == __TYPES__.fuelRequests.status.nevv || data.requestStatus == __TYPES__.fuelRequests.status.processed ) ;
	}
	
	,_setDefaultLovValue_: function(viewName,fieldToGet,valueToSet,fieldToReturn,fielToReturnValueIn,opt) {
		
		 
		                // -------------- Function config --------------
		 
		                var dcView = this._get_(viewName);				
		 
		                // -------------- End function config --------------
		 
		                var field = dcView._get_(fieldToGet);
		                var valueField = field.valueField;
		                var record = dcView._controller_.getRecord();
		 
		                var store = field.store;
		
						
		
		                store.load({
		                    callback: function(rec) {
		
		                        var i = 0, l = rec.length;
		
								if (fieldToReturn instanceof Array) {
									var valueToReturn = [];
								}
								else {
									var valueToReturn = "";
								}
		                        
		 
		                        for (i ; i < l; i++) {
		                            var data = rec[i].data;
		                            for (var key in data) {
		                                if (data[valueField] == valueToSet) {
		
											if (fieldToReturn instanceof Array) {
												for (var x = 0; x<fieldToReturn.length; x++) {
													valueToReturn.push(data[fieldToReturn[x]]);
												}
											}
											else {
												valueToReturn = data[fieldToReturn];
											}
		                                    break;
											
		                                }
		                            }
		                        }
		
		                        if (record) {
		                            record.beginEdit();
		 
		                            record.set(field.dataIndex, valueToSet);
									if (fielToReturnValueIn instanceof Array) {
										for (var z = 0; z<fielToReturnValueIn.length; z++) {
											record.set(fielToReturnValueIn[z], valueToReturn[z]);
										}
									}
									else {
										record.set(fielToReturnValueIn, valueToReturn);
									}
		                            
		                            if (opt) {
		                                for (var key in opt) {
		                                    record.set(key, opt[key]);
		 
		                                }
		                            }
		                            record.endEdit();
		                        }
		                        
		 
		                    }
		                },this);
	}
	
	,_afterDefineDcs_: function() {
		
		
					var fuelRequestsDc = this._getDc_("fuelRequests");
					var fuelRequestsEdit = this._getDc_("fuelRequestsEdit");
					var flightEvent = this._getDc_("flightEvent");
		
					fuelRequestsDc.on("afterDoNew", function (dc) {
						var r = dc.getRecord();
						if (r) {
							r.set("subsidiaryCode", parent._ACTIVESUBSIDIARY_.code);
						}
					}, this);
		
					//*******************************//	
		
					fuelRequestsEdit.on("afterDoSaveSuccess", function (dc) {
						this._applyStateAllButtons_();
						fuelRequestsDc.doReloadRecord();
					}, this);
		
					//*******************************//	
		
					fuelRequestsEdit.on("afterDoServiceSuccess", function(dc){
						this._applyStateAllButtons_();
						this.resetDcState(fuelRequestsDc,fuelRequestsEdit,flightEvent);
						dc.setParamValue("remarks","");
					}, this);
		
					//*******************************//	
		
					fuelRequestsDc.on("afterDoSaveSuccess", function (dc) {
						this._applyStateAllButtons_();
						fuelRequestsEdit.doReloadRecord();
					}, this);
		
					//*******************************//	
		
					fuelRequestsDc.on("afterDoServiceSuccess", function(dc){
						this._applyStateAllButtons_();
						this.resetDcState(fuelRequestsDc,fuelRequestsEdit,flightEvent);
						fuelRequestsEdit.doReloadRecord();
						dc.setParamValue("remarks","");
					}, this);
		
					//*******************************//	
		
					fuelRequestsDc.on("OnEditIn", function (dc) {
						this.resetDcState(fuelRequestsDc,fuelRequestsEdit,flightEvent);
					}, this);
		
					//*******************************//	
		
					fuelRequestsDc.on("OnEditOut", function (dc, ajaxResult) {
						if (ajaxResult.doNewAfterEditOut == true) {
							// Dan: wait until the store is loaded and after call the doNew() method
							dc.store.on("load", function() {
								dc.doNew();
							}, this);
						}
					}, this);
		
					//*******************************//	
		
					fuelRequestsEdit.on("afterdoquerysuccess", function (dc) {
						this.resetDcState(fuelRequestsDc,dc,flightEvent);
					}, this);
		
					
					//*******************************//	
		
					flightEvent.on("afterDoNew", function(dc) {
						this._getWindow_("wdwFlightEvent").show();
						dc.setEditMode();
						var dst = this._getDc_("flightEvent").getRecord();
						var src = this._getDc_("fuelRequests").getRecord();
		
						dst.beginEdit();
		 
						dst.set("operatorCode", src.get("customerCode"));
						dst.set("operatorId", src.get("customerId"));
						
						dst.endEdit();
					} , this );
		
					//*******************************//	
		
					flightEvent.on("afterDoSaveSuccess", function (dc, ajaxResult) {
						var message = dc.getRecord().get("toleranceMessage");
						if(message != ""){
							Main.warning(message);
						}
						if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwFlightEvent").close();
							flightEvent.doReloadPage();
						} else if (ajaxResult.options.options.doNewAfterSave === true) {
							flightEvent.doNew();
						}
					} , this );
		
					//********* Note **********//
		
					var note = this._getDc_("note");
		
					note.on("afterDoNew", function (dc) {
					    this.showNoteWdw();
					}, this);
		
					note.on("OnEditIn", function(dc) {
					   this._getWindow_("noteWdw").show();
					}, this);
					
					note.on("afterDoSaveSuccess", function(dc, ajaxResult) {
					    if (ajaxResult.options.options.doNewAfterSave === true) {
					        dc.doNew();
					    } else if (ajaxResult.options.options.doCloseNoteWindowAfterSave === true) {
					        this._getWindow_("noteWdw").close();
					    }
					}, this);
		
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,openHelp: function() {
		
					var url = Main.urlHost+"/supplierONE/supplierONE/FuelRequests.html";
					window.open( url, "SONE_Help");
	}
	
	,saveClose: function() {
		
		
					var flightEvent = this._getDc_("flightEvent");
					flightEvent.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveEdit: function() {
		
					var flightEvent = this._getDc_("flightEvent");
					flightEvent.doSave({doNewAfterSave: true });
	}
	
	,onWdwFlightTypeClose: function() {
		
						var dc = this._getDc_("flightEvent");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,fnCancelSelected: function() {
		
						var dc = this._getDc_("fuelRequests");
						dc.doCancelSelected();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelAll: function() {
		
						var dc = this._getDc_("fuelRequests");
						dc.doCancel();
						this._applyStateButton_("btnCancelWithMenu");
	}
	
	,fnCancelEnabled: function(dc) {
		
						return e4e.dc.DcActionsStateManager.isCancelEnabled(dc);
	}
});
