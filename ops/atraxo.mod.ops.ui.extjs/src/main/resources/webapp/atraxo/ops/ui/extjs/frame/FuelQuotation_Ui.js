/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.frame.FuelQuotation_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.FuelQuotation_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("priceQuotation", Ext.create(atraxo.ops.ui.extjs.dc.FuelQuotation_Dc,{}))
		.addDc("fuelQuoteLoc", Ext.create(atraxo.ops.ui.extjs.dc.FuelQuoteLocation_Dc,{}))
		.addDc("attachment", Ext.create(atraxo.ad.ui.extjs.dc.Attachment_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.linkDc("fuelQuoteLoc", "priceQuotation",{fetchMode:"auto",fields:[
					{childField:"quoteId", parentField:"id"}]})
				.linkDc("attachment", "priceQuotation",{fetchMode:"auto",fields:[
					{childField:"targetRefid", parentField:"id"}, {childField:"targetAlias", parentField:"entityAlias"}, {childField:"targetType", value:"N/A"}]})
				.linkDc("history", "priceQuotation",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "priceQuotation",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnHelpList",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpList, scope:this})
		.addButton({name:"btnHelpEditForm",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onBtnHelpEditForm, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveEditWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditWdw, scope:this})
		.addButton({name:"btnCancelNewPriceQuoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelNewPriceQuoteWdw, scope:this})
		.addButton({name:"btnCancelLoc",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelLoc, scope:this})
		.addButton({name:"btnCancelQuoteList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelQuoteList,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.negotiation || dc.record.data.status==__TYPES__.fuelQuotation.status.nevv || dc.record.data.status==__TYPES__.fuelQuotation.status.submitted);} }], scope:this})
		.addButton({name:"btnCancelQuoteEditForm",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnCancelQuoteEditForm,stateManager:[{ name:"record_status_is_edit", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.negotiation || dc.record.data.status==__TYPES__.fuelQuotation.status.nevv || dc.record.data.status==__TYPES__.fuelQuotation.status.submitted);} }], scope:this})
		.addButton({name:"btnCloseQuoteList",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnCloseQuoteList,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status=='Negotiation' || dc.record.data.status==__TYPES__.fuelQuotation.status.nevv || dc.record.data.status==__TYPES__.fuelQuotation.status.submitted);} }], scope:this})
		.addButton({name:"btnCloseQuotetEditForm",glyph:fp_asc.delete_glyph.glyph,iconCls: fp_asc.delete_glyph.css, disabled:true, handler: this.onBtnCloseQuotetEditForm,stateManager:[{ name:"record_status_is_edit", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.negotiation || dc.record.data.status==__TYPES__.fuelQuotation.status.nevv || dc.record.data.status==__TYPES__.fuelQuotation.status.submitted);} }], scope:this})
		.addButton({name:"btnResetQuoteList",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnResetQuoteList,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return ((dc.record.data.status==__TYPES__.fuelQuotation.status.closed || dc.record.data.status==__TYPES__.fuelQuotation.status.canceled));} }], scope:this})
		.addButton({name:"btnResetQuoteEditForm",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnResetQuoteEditForm,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return ((dc.record.data.status==__TYPES__.fuelQuotation.status.closed || dc.record.data.status==__TYPES__.fuelQuotation.status.canceled));} }], scope:this})
		.addButton({name:"btnSubmitQuoteList",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSubmitQuoteList,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.nevv || dc.record.data.status==__TYPES__.fuelQuotation.status.negotiation);} }], scope:this})
		.addButton({name:"btnSubmitQuoteEditForm",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:true, handler: this.onBtnSubmitQuoteEditForm,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.nevv || dc.record.data.status==__TYPES__.fuelQuotation.status.negotiation);} }], scope:this})
		.addButton({name:"btnNegociateQuoteList",glyph:fp_asc.negociate_glyph.glyph, disabled:true, handler: this.onBtnNegociateQuoteList,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.submitted);} }], scope:this})
		.addButton({name:"btnNegociateQuoteEditForm",glyph:fp_asc.negociate_glyph.glyph, disabled:true, handler: this.onBtnNegociateQuoteEditForm,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.submitted);} }], scope:this})
		.addButton({name:"btnGeneratOrderOpenList",glyph:fp_asc.gear_glyph.glyph, disabled:true, handler: this.onBtnGeneratOrderOpenList,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.submitted);} }], scope:this})
		.addButton({name:"btnGeneratOrderOpenEdit",glyph:fp_asc.gear_glyph.glyph, disabled:true, handler: this.onBtnGeneratOrderOpenEdit,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.submitted);} }], scope:this})
		.addButton({name:"btnGenerateOrder",glyph:fp_asc.gear_glyph.glyph, disabled:false, handler: this.onBtnGenerateOrder, scope:this})
		.addButton({name:"btnGenerateOrderCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnGenerateOrderCancel, scope:this})
		.addButton({name:"openOrderButtonList",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onOpenOrderButtonList,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.ordered);} }], scope:this})
		.addButton({name:"openOrderButtonEdit",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onOpenOrderButtonEdit,stateManager:[{ name:"selected_one", dc:"priceQuotation", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelQuotation.status.ordered);} }], scope:this})
		.addButton({name:"btnContinue",glyph:fp_asc.rollforward_glyph.glyph,iconCls: fp_asc.rollforward_glyph.css, disabled:false, handler: this.onBtnContinue, scope:this})
		.addButton({name:"btnNewFromRequest",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnNewFromRequest, scope:this})
		.addButton({name:"btnChangeOwner", disabled:false, handler: this.onBtnChangeOwner, scope:this})
		.addButton({name:"btnSaveCloseRemarkWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRemarkWdw, scope:this})
		.addButton({name:"btnCancelRemarkWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRemarkWdw, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addDcFilterFormView("priceQuotation", {name:"Filter", xtype:"ops_FuelQuotation_Dc$Filter"})
		.addDcGridView("priceQuotation", {name:"List", xtype:"ops_FuelQuotation_Dc$List"})
		.addDcFormView("priceQuotation", {name:"PopUp", xtype:"ops_FuelQuotation_Dc$New"})
		.addDcFormView("priceQuotation", {name:"Edit", xtype:"ops_FuelQuotation_Dc$Edit"})
		.addDcFilterFormView("fuelQuoteLoc", {name:"Location", xtype:"ops_FuelQuoteLocation_Dc$Filter"})
		.addDcGridView("fuelQuoteLoc", {name:"Locations", _hasTitle_:true, xtype:"ops_FuelQuoteLocation_Dc$List"})
		.addDcFormView("fuelQuoteLoc", {name:"LocPopUp", xtype:"ops_FuelQuoteLocation_Dc$PopUp"})
		.addDcFormView("priceQuotation", {name:"Disclaimer", _hasTitle_:true, xtype:"ops_FuelQuotation_Dc$Disclaimer",  bodyPadding:"0"})
		.addDcGridView("priceQuotation", {name:"Activities", _hasTitle_:true, xtype:"ops_FuelQuotation_Dc$Temp",  disabled:true})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"NotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("priceQuotation", {name:"generateOrderPopUp", xtype:"ops_FuelQuotation_Dc$GenerateQuote"})
		.addDcFormView("priceQuotation", {name:"remark", xtype:"ops_FuelQuotation_Dc$Remark"})
		.addDcGridView("attachment", {name:"attachmentList", _hasTitle_:true, xtype:"ad_Attachment_Dc$List"})
		.addDcFormView("attachment", {name:"attachmentEdit", xtype:"ad_Attachment_Dc$New"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addWindow({name:"wdwPriceQuotation", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PopUp")],  listeners:{ close:{fn:this.onWdwPriceQuotClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveEditWdw"), this._elems_.get("btnCancelNewPriceQuoteWdw")]}]})
		.addWindow({name:"wdwFuelQuoteLoc", _hasTitle_:true, width:1070, height:360, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("LocPopUp")],  listeners:{ close:{fn:this.onWdwfuelQuoteLocClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnContinue"), this._elems_.get("btnCancelLoc")]}]})
		.addWindow({name:"wdwGenerateOrder", _hasTitle_:true, width:360, height:220, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("generateOrderPopUp")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnGenerateOrder"), this._elems_.get("btnGenerateOrderCancel")]}]})
		.addWindow({name:"wdwRemarks", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("remark")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRemarkWdw"), this._elems_.get("btnCancelRemarkWdw")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NotesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"priceQuotation"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["List"], ["center"])
		.addChildrenTo("canvas2", ["Edit", "detailsTab"], ["north", "center"])
		.addChildrenTo("detailsTab", ["Locations"])
		.addToolbarTo("List", "tlbList")
		.addToolbarTo("Edit", "tlbEdit")
		.addToolbarTo("Locations", "tlbListLoc")
		.addToolbarTo("NotesList", "tlbNoteList")
		.addToolbarTo("attachmentList", "tlbAttachments");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"])
		.addChildrenTo2("detailsTab", ["Disclaimer", "NotesList", "History", "Activities", "attachmentList"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "priceQuotation"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelQuoteList"),this._elems_.get("btnCloseQuoteList"),this._elems_.get("btnResetQuoteList"),this._elems_.get("btnSubmitQuoteList"),this._elems_.get("btnGeneratOrderOpenList"),this._elems_.get("btnNegociateQuoteList"),this._elems_.get("btnNewFromRequest"),this._elems_.get("openOrderButtonList"),this._elems_.get("btnHelpList")])
			.addReports()
		.end()
		.beginToolbar("tlbEdit", {dc: "priceQuotation"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnCancelQuoteEditForm"),this._elems_.get("btnCloseQuotetEditForm"),this._elems_.get("btnResetQuoteEditForm"),this._elems_.get("btnSubmitQuoteEditForm"),this._elems_.get("btnGeneratOrderOpenEdit"),this._elems_.get("btnNegociateQuoteEditForm"),this._elems_.get("openOrderButtonEdit"),this._elems_.get("btnHelpEditForm")])
			.addReports()
		.end()
		.beginToolbar("tlbListLoc", {dc: "fuelQuoteLoc"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbAttachments", {dc: "attachment"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnHelpList
	 */
	,onBtnHelpList: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnHelpEditForm
	 */
	,onBtnHelpEditForm: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveEditWdw
	 */
	,onBtnSaveEditWdw: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button btnCancelNewPriceQuoteWdw
	 */
	,onBtnCancelNewPriceQuoteWdw: function() {
		this.onWdwPriceQuotClose();
		this._getWindow_("wdwPriceQuotation").close();
	}
	
	/**
	 * On-Click handler for button btnCancelLoc
	 */
	,onBtnCancelLoc: function() {
		this.onWdwfuelQuoteLocClose();
		this._getWindow_("wdwFuelQuoteLoc").close();
	}
	
	/**
	 * On-Click handler for button btnCancelQuoteList
	 */
	,onBtnCancelQuoteList: function() {
		this.cancelQuote();
	}
	
	/**
	 * On-Click handler for button btnCancelQuoteEditForm
	 */
	,onBtnCancelQuoteEditForm: function() {
		this.cancelQuote();
	}
	
	/**
	 * On-Click handler for button btnCloseQuoteList
	 */
	,onBtnCloseQuoteList: function() {
		this.closeQuote();
	}
	
	/**
	 * On-Click handler for button btnCloseQuotetEditForm
	 */
	,onBtnCloseQuotetEditForm: function() {
		this.closeQuote();
	}
	
	/**
	 * On-Click handler for button btnResetQuoteList
	 */
	,onBtnResetQuoteList: function() {
		this.resetQuote();
	}
	
	/**
	 * On-Click handler for button btnResetQuoteEditForm
	 */
	,onBtnResetQuoteEditForm: function() {
		this.resetQuote();
	}
	
	/**
	 * On-Click handler for button btnSubmitQuoteList
	 */
	,onBtnSubmitQuoteList: function() {
		this.submitQuote();
		var successFn = function(dc) {
			this.submitCallback(dc)
		};
		var o={
			name:"updateSubmitStatusList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("priceQuotation").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnSubmitQuoteEditForm
	 */
	,onBtnSubmitQuoteEditForm: function() {
		this.submitQuote();
		var successFn = function(dc) {
			this.submitCallback(dc)
		};
		var o={
			name:"updateSubmitStatus",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("priceQuotation").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnNegociateQuoteList
	 */
	,onBtnNegociateQuoteList: function() {
		this.negotiateQuote();
		var successFn = function(dc) {
			this._getDc_("priceQuotation").setParamValue("remark","");
								this._getDc_("fuelQuoteLoc").dcContext._updateCtxData_();
								this.setQuoteLocationNewDeleteButtonAvailability(this._getDc_("fuelQuoteLoc"));
								this._applyStateAllButtons_();
		};
		var o={
			name:"updateStatusList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("priceQuotation").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnNegociateQuoteEditForm
	 */
	,onBtnNegociateQuoteEditForm: function() {
		this.negotiateQuote();
		var successFn = function(dc) {
			this._getDc_("priceQuotation").setParamValue("remark","");
								this._getDc_("fuelQuoteLoc").dcContext._updateCtxData_();
								this.setQuoteLocationNewDeleteButtonAvailability(this._getDc_("fuelQuoteLoc"));
								this._applyStateAllButtons_();
		};
		var o={
			name:"updateStatus",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("priceQuotation").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnGeneratOrderOpenList
	 */
	,onBtnGeneratOrderOpenList: function() {
		this.generateOrderWdwShow();
	}
	
	/**
	 * On-Click handler for button btnGeneratOrderOpenEdit
	 */
	,onBtnGeneratOrderOpenEdit: function() {
		this.generateOrderWdwShow();
	}
	
	/**
	 * On-Click handler for button btnGenerateOrder
	 */
	,onBtnGenerateOrder: function() {
		var successFn = function() {
			var bundle = "atraxo.mod.ops";
			var frame = "atraxo.ops.ui.extjs.frame.FuelOrder_Ui";
			getApplication().showFrame(frame,{
				url:Main.buildUiPath(bundle, frame, false),
				params: {
					fuelOrderId: this._getDc_("priceQuotation").getRecord().get("fuelOrderId")
				},
				callback: function (params) {
					this._when_called_to_edit(params);
				}
			});
		};
		var o={
			name:"generateOrder",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("priceQuotation").doRpcData(o);
		this._getWindow_("wdwGenerateOrder").close();
	}
	
	/**
	 * On-Click handler for button btnGenerateOrderCancel
	 */
	,onBtnGenerateOrderCancel: function() {
		this._getWindow_("wdwGenerateOrder").close();
	}
	
	/**
	 * On-Click handler for button openOrderButtonList
	 */
	,onOpenOrderButtonList: function() {
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelOrder_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				fuelOrderId: this._getDc_("priceQuotation").getRecord().get("fuelOrderId")
			},
			callback: function (params) {
				this._when_called_to_edit(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button openOrderButtonEdit
	 */
	,onOpenOrderButtonEdit: function() {
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelOrder_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				fuelOrderId: this._getDc_("priceQuotation").getRecord().get("fuelOrderId")
			},
			callback: function (params) {
				this._when_called_to_edit(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnContinue
	 */
	,onBtnContinue: function() {
		this._getDc_("fuelQuoteLoc").doSave();
	}
	
	/**
	 * On-Click handler for button btnNewFromRequest
	 */
	,onBtnNewFromRequest: function() {
	}
	
	/**
	 * On-Click handler for button btnChangeOwner
	 */
	,onBtnChangeOwner: function() {
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRemarkWdw
	 */
	,onBtnSaveCloseRemarkWdw: function() {
		var successFn = function() {
			this._getWindow_("wdwRemarks").close();
			this._getDc_("priceQuotation").setParamValue("remark","")
		};
		var o={
			name:"updateStatus",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("priceQuotation").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelRemarkWdw
	 */
	,onBtnCancelRemarkWdw: function() {
		this._getWindow_("wdwRemarks").close();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	,generateReport: function() {	
		var o={
			name:"generateReport",
			modal:true
		};
		this._getDc_("priceQuotation").doRpcData(o);
	}
	
	,onShowWdw: function() {	
		this._getWindow_("wdwPriceQuotation").show();
	}
	
	,reloadFuelQuoteLoc: function() {	
		this._getDc_("fuelQuoteLoc").doReloadPage();
	}
	
	,onShowWindow: function() {	
		this._getWindow_("wdwFuelQuoteLoc").show();
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,onNoteWdwClose: function() {	
		this._getDc_("note").doCancel();
	}
	
	,closePopUp: function() {	
		this._getWindow_("wdwFuelQuoteLoc").close();
	}
	
	,onShowUi: function() {	
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelQuoteLocation_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				fuelQuoteLocId: this._getDc_("fuelQuoteLoc").getRecord().get("id"),
				quoteStatus: this._getDc_("priceQuotation").getRecord().get("status")
			},
			callback: function (params) {
				this._when_called_for_details_(params);
			}
		});
	}
	
	,submitCallback: function(dc) {
			
						
						this.generateReport();
						
						// ===========================================================
						// End Dan: SONE-1625 Create JSON request for dialog reports
						// ===========================================================
		
						this._getDc_("fuelQuoteLoc").dcContext._updateCtxData_();
						this.setQuoteLocationNewDeleteButtonAvailability(this._getDc_("fuelQuoteLoc"));
						this._applyStateAllButtons_();
	}
	
	,_when_called_from_appMenu_: function() {
		
						var dc = this._getDc_("priceQuotation");
						dc.doEditOut({doNewAfterEditOut:true});
	}
	
	,_afterDefineDcs_: function() {
		
		
					var priceQuotation = this._getDc_("priceQuotation");
					var fuelQuoteLoc = this._getDc_("fuelQuoteLoc");
					var history = this._getDc_("history");
					var note = this._getDc_("note");
		
					note.on("afterDoNew", function (dc) {
					    this.showNoteWdw();
					}, this);
		
					note.on("OnEditIn", function(dc) {
					    this._getWindow_("noteWdw").show();
					}, this);
					
					note.on("afterDoSaveSuccess", function(dc, ajaxResult) {
					    if (ajaxResult.options.options.doNewAfterSave === true) {
					        dc.doNew();
					    } else if (ajaxResult.options.options.doCloseNoteWindowAfterSave === true) {
					        this._getWindow_("noteWdw").close();
					    }
					}, this);
		
					priceQuotation.on("recordChange", function (dc) {
						this.setQuoteLocationNewDeleteButtonAvailability(this._getDc_("fuelQuoteLoc"));
					}, this);
					
					priceQuotation.on("afterDoQuerySuccess", function (dc) {
						// Enable or disable the Fuel Quote location "new" and "delete" buttons
						this.setQuoteLocationNewDeleteButtonAvailability(this._getDc_("fuelQuoteLoc"));
					}, this);
		
					priceQuotation.on("afterDoNew", function (dc) {
						dc.setEditMode();
					    this.onShowWdw();
						var r = dc.getRecord();
						if (r) {
							r.set("subsidiaryCode", parent._ACTIVESUBSIDIARY_.code);
						}
		
						// Set the owner value as the current logged in user name
		
						var user = getApplication().getSession().getUser().name
						this._getDc_("priceQuotation").getRecord().set("owner", user);
					}, this);
		
					priceQuotation.on("afterDoSaveSuccess", function (dc, ajaxResult) {
						priceQuotation.doReloadRecord();
						if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwPriceQuotation").close();
						} else if (ajaxResult.options.options.doCloseEditAfterSave === true) {
							this._getWindow_("wdwPriceQuotation").close();
							dc.doEditIn();
						}
					}, this);	
		
					fuelQuoteLoc.on("afterDoNew", function (dc) {
		
						var dst = this._getDc_("fuelQuoteLoc").getRecord();
						var src = this._getDc_("priceQuotation").getRecord();
		
						dst.beginEdit();
		 
						dst.set("customerCode", src.get("customerCode"));
						dst.set("customerId", src.get("customerId"));				
						dst.set("paymentTerms", src.get("paymentTerm"));
						dst.set("paymentRefDate", src.get("referenceTo"));
						dst.set("invoiceFreq", src.get("invFreq"));
						dst.set("currCode", src.get("payCurr"));
						dst.set("currId", src.get("payCurrId"));
						dst.set("forex", src.get("finSrcCode") + "/" +  src.get("avgMthdCode"));
						dst.set("financialSourceId", src.get("finSrcId"));
						dst.set("averageMethodId", src.get("avgMthdId"));
						dst.set("period", src.get("period"));
		
						dst.endEdit();
		
						dc.setEditMode();
						this.onShowWindow();
		
						
					}, this);
		
					fuelQuoteLoc.on("afterDoSaveSuccess", function (dc) {
						//TODO should be done in meta at button definition
						this.closePopUp();
						//TODO should be done in meta at button definition
						Ext.defer(this.onShowUi, 250, this);
					}, this);
		
					fuelQuoteLoc.on("onEditIn", function (dc) {
						//TODO should be done in meta at button definition
					    this.onShowUi();
					}, this);
					
					priceQuotation.on("selectionchange", function() {
					   this._syncReadOnlyStates_();
					}, this);
		
					priceQuotation.on("afterDoServiceSuccess",  function(){
						this._syncReadOnlyStates_();
						this._applyStateAllButtons_();
					}, this);
		
	}
	
	,_syncReadOnlyStates_: function() {
		
						var rec = this._getDc_("priceQuotation").getRecord();
						if (!rec) { return; }
						var fuelQuoteLoc = this._getDc_("fuelQuoteLoc");
						var status = rec.get("status");
						if (status==__TYPES__.fuelQuotation.status.submitted || status==__TYPES__.fuelQuotation.status.closed || status==__TYPES__.fuelQuotation.status.canceled || status==__TYPES__.fuelQuotation.status.ordered || status==__TYPES__.fuelQuotation.status.approved || status==__TYPES__.fuelQuotation.status.expired) {
							fuelQuoteLoc.setReadOnly(true);	
							fuelQuoteLoc.updateDcState();
						}
						else {
							fuelQuoteLoc.setReadOnly(false);
							fuelQuoteLoc.updateDcState();
						}
		
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/FuelPriceQuotes.html";
					window.open( url, "SONE_Help");
	}
	
	,_when_called_to_edit: function(params) {
		
					var dc = this._getDc_("priceQuotation");
					dc.doClearQuery();
					dc.setFilterValue("code", params.fuelQuotationCode);
					dc.doQuery();
					this._showStackedViewElement_("main", "canvas2");
	}
	
	,cancelQuote: function() {
		
						var label = "You are about to revoke the fuel quotation.<br/> Please provide a reason.";
						var title = "Revoke Fuel Quotation";
						var action = __TYPES__.fuelQuotation.status.canceled;
						this.setupHistoryWindow("wdwRemarks", "remark", "btnSaveCloseRemarkWdw", "btnCancelRemarkWdw", action, label , title);				
	}
	
	,closeQuote: function() {
		
						var label = "You are about to close the fuel quotation.<br/> Please provide a reason.";
						var title = "Close Fuel Quote";
						var action = __TYPES__.fuelQuotation.status.closed;
						this.setupHistoryWindow("wdwRemarks", "remark", "btnSaveCloseRemarkWdw", "btnCancelRemarkWdw", action, label , title);				
	}
	
	,resetQuote: function() {
		
						var label = "You are about to reset the fuel quotation.<br/> Please provide a reason.";
						var title = "Reset Fuel Quote";
						var action = __TYPES__.fuelQuotation.status.nevv;
						this.setupHistoryWindow("wdwRemarks", "remark", "btnSaveCloseRemarkWdw", "btnCancelRemarkWdw", action, label , title);				
	}
	
	,submitQuote: function() {
		
						var dc = this._getDc_("priceQuotation");
						dc.setParamValue("action", __TYPES__.fuelQuotation.status.submitted);
						dc.setParamValue("remark", __TYPES__.fuelQuotation.status.submitted);
	}
	
	,negotiateQuote: function() {
		
						var dc = this._getDc_("priceQuotation");
						dc.setParamValue("action", __TYPES__.fuelQuotation.status.negotiation);
						dc.setParamValue("remark", __TYPES__.fuelQuotation.status.negotiation);
	}
	
	,openReport: function() {
		
						var dc = this._getDc_("priceQuotation");
						var r = dc.getRecord();
						var id = r.get("id");
						dc.setParamValue("remark",""); 
						var report = this.getReport("FUELQUOTE");
						if(report != null){
							this.runReport(report);
						}
	}
	
	,saveClose: function() {
		
		
					var priceQuotation = this._getDc_("priceQuotation");
					priceQuotation.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,saveEdit: function() {
		
					var priceQuotation = this._getDc_("priceQuotation");
					priceQuotation.doSave({doCloseEditAfterSave: true });
		
	}
	
	,saveCloseNote: function() {
		
		
					var note = this._getDc_("note");
					note.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,generateOrderWdwShow: function() {
		
					var window = this._getWindow_("wdwGenerateOrder");
					window.show(undefined, function(){
						var priceQuotation = this._getDc_("priceQuotation");
						priceQuotation.setParamValue("orderPostedOn", new Date());
						priceQuotation.setParamValue("orderSubmittedBy", priceQuotation.getRecord().get("primaryClientId"));
						priceQuotation.setParamValue("orderSubmittedName", priceQuotation.getRecord().get("primaryClientName"));
				    }, this);		
	}
	
	,setupHistoryWindow: function(theWindow,theForm,saveBtn,cancelBtn,action,label,title) {
		
						var window = this._getWindow_(theWindow);
					 
						var remarksField = this._get_(theForm)._get_("remarks");
						var dc = this._getDc_("priceQuotation");
		
						var saveButton = this._get_(saveBtn);
						var cancelButton = this._get_(cancelBtn);
						
						saveButton.setDisabled(true);
						cancelButton.setDisabled(false);
						dc.setParamValue("action", action);
						dc.setParamValue("remarks", "");
						
						remarksField.setFieldLabel(label);
						window.setTitle(title);
		
						remarksField.on("change", function(field) {
						    if (field.value.length > 0) {
						        saveButton.setDisabled(false);
						    } else {
						        saveButton.setDisabled(true);
						    }
						});
		
						//TODO: formul  apare disabled  o singura data. Hai sa-l fortam in enable, si ramine de vazut de la ce ... 
						var f = this._get_(theForm);
						if (f.disabled) {
							f.enable();
						}
		
						window.show();
	}
	
	,_afterLinkElementsPhase2_: function() {
		
						this.setQuoteLocationNewDeleteButtonAvailability(this._getDc_("fuelQuoteLoc"));
	}
	
	,setQuoteLocationNewDeleteButtonAvailability: function(dc) {
		
						
						var list = this._get_("List");
						if(this._get_("Disclaimer")){
							var disclaimer = this._get_("Disclaimer")._get_("disclaimer");
						    var view = list.getView();
			
							disclaimer.disabledCls = "x-form-readonly";
							
							if (view.getSelectionModel().hasSelection()) {
							   var row = view.getSelectionModel().getSelection()[0];
							   var status = row.get("status");
			
							   if (status == __TYPES__.fuelQuotation.status.ordered || status == __TYPES__.fuelQuotation.status.submitted) {
									//dc.canDoDelete = false;						
									dc.setReadOnly(true);
									dc.updateDcState();
									disclaimer.setReadOnly(true);
								}
								else {
									//dc.canDoDelete = true;						
									dc.setReadOnly(false);
									dc.updateDcState();
									disclaimer.setReadOnly(false);
								}
							}
						}
	}
	
	,onWdwPriceQuotClose: function() {
		
						var dc = this._getDc_("priceQuotation");
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,onWdwfuelQuoteLocClose: function() {
		
						var dc = this._getDc_("fuelQuoteLoc");
						dc.clearEditMode();
						if(dc.isDirty()){
							dc.doCancel();
						}
	}
});
