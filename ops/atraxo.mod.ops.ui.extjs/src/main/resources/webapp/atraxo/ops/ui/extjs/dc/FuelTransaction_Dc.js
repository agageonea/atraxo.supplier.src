/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelTransaction_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ops.ui.extjs.ds.FuelTransaction_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FuelTransaction_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FuelTransaction_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateField({name:"date", dataIndex:"processingDate"})
			.addLov({name:"customer", dataIndex:"intoPlaneCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"code", dsField: "intoPlaneCode"} ,{lovField:"name", dsField: "intoPlaneName"} ]}})
			.addLov({name:"registration", dataIndex:"aircraftIdentification", maxLength:10,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AircraftLov_Lov", selectOnFocus:true, maxLength:10,
					retFieldMapping: [{lovField:"registration", dsField: "aircraftIdentification"} ]}})
			.addLov({name:"acType", dataIndex:"aircraftType", maxLength:4,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AcTypesLov_Lov", selectOnFocus:true, maxLength:4,
					retFieldMapping: [{lovField:"code", dsField: "aircraftType"} ],
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addTextField({ name:"airlineFlightId", dataIndex:"airlineFlightId", maxLength:10})
			.addLov({name:"departure", dataIndex:"airportCode", maxLength:5,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:5,
					retFieldMapping: [{lovField:"code", dsField: "airportCode"} ],
					filterFieldMapping: [{lovField:"isAirport", value: "true"} ]}})
			.addLov({name:"destination", dataIndex:"nextDestination", maxLength:5,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:5,
					retFieldMapping: [{lovField:"code", dsField: "nextDestination"} ],
					filterFieldMapping: [{lovField:"isAirport", value: "true"} ]}})
			.addLov({name:"finalDest", dataIndex:"finalDestination", maxLength:5,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:5,
					retFieldMapping: [{lovField:"code", dsField: "finalDestination"} ],
					filterFieldMapping: [{lovField:"isAirport", value: "true"} ]}})
			.addTextField({ name:"ticketNo", dataIndex:"ticketNumber", maxLength:20})
			.addLov({name:"supplier", dataIndex:"supplierCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"code", dsField: "supplierCode"} ,{lovField:"name", dsField: "supplierName"} ],
					filterFieldMapping: [{lovField:"isSupplier", value: "true"}, {lovField:"fuelSupplier", value: "true"} ]}})
			.addCombo({ xtype:"combo", name:"ticketType", dataIndex:"ticketType", store:[ __OPS_TYPE__.TicketType._ORIGINAL_, __OPS_TYPE__.TicketType._REISSUE_, __OPS_TYPE__.TicketType._CANCEL_, __OPS_TYPE__.TicketType._DELETE_]})
			.addCombo({ xtype:"combo", name:"validationState", dataIndex:"validationState", store:[ __OPS_TYPE__.ValidationState._NOT_CHECKED_, __OPS_TYPE__.ValidationState._FAILED_]})
			.addTextField({ name:"validationResult", dataIndex:"validationResult", maxLength:2500})
			.addDateField({name:"processingDate", dataIndex:"createdAt"})
			.addTextField({ name:"importFileName", dataIndex:"importFileName", maxLength:255})
			.addTextField({ name:"receiverAccountNumber", dataIndex:"receiverAccountNumber", maxLength:50})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTransaction_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelTransaction_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"date", dataIndex:"transactionDate", width:100, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"customer", dataIndex:"receiverCode", width:70})
		.addTextColumn({ name:"registration", dataIndex:"aircraftIdentification", width:85})
		.addTextColumn({ name:"acType", dataIndex:"aircraftType", hidden:true, width:100})
		.addTextColumn({ name:"flightId", dataIndex:"airlineFlightId", width:80})
		.addTextColumn({ name:"departure", dataIndex:"airportCode", width:80})
		.addTextColumn({ name:"destination", dataIndex:"nextDestination", hidden:true, width:80})
		.addTextColumn({ name:"finalDest", dataIndex:"finalDestination", hidden:true, width:100})
		.addTextColumn({ name:"ticketNo", dataIndex:"ticketNumber", width:70})
		.addTextColumn({ name:"supplier", dataIndex:"supplierCode", width:70})
		.addTextColumn({ name:"ticketType", dataIndex:"ticketType", width:100})
		.addTextColumn({ name:"validationState", dataIndex:"validationState", width:120})
		.addTextColumn({ name:"validationResult", dataIndex:"validationResult", width:120})
		.addDateColumn({ name:"processingDate", dataIndex:"createdAt", width:120, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"importFileName", dataIndex:"importFileName", width:100})
		.addTextColumn({ name:"receiverAccountNumber", dataIndex:"receiverAccountNumber", hidden:true, width:120})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: Temp ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTransaction_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelTransaction_Dc$Temp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTransaction_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTransaction_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateTimeField({name:"deliveryDate", bind:"{d.transactionDate}", dataIndex:"transactionDate", width:160, hideLabel:"true"})
		.addDateTimeField({name:"fuelingStart", bind:"{d.transactionStartDate}", dataIndex:"transactionStartDate", width:160, hideLabel:"true"})
		.addDateTimeField({name:"fuelingEnd", bind:"{d.transactionEndDate}", dataIndex:"transactionEndDate", width:160, hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"fuelingOper", bind:"{d.ipTransactionCode}", dataIndex:"ipTransactionCode", width:160, store:[ __OPS_TYPE__.IPTransactionCode._UPLIFT_, __OPS_TYPE__.IPTransactionCode._DE_FUEL_, __OPS_TYPE__.IPTransactionCode._RE_FUEL_], hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.productId}", dataIndex:"productId", width:160, store:[ __CMM_TYPE__.ProductIATA._ADT_, __CMM_TYPE__.ProductIATA._ADTV_, __CMM_TYPE__.ProductIATA._AG100_, __CMM_TYPE__.ProductIATA._AG1HL_, __CMM_TYPE__.ProductIATA._AG80_, __CMM_TYPE__.ProductIATA._AHF_, __CMM_TYPE__.ProductIATA._AIF_, __CMM_TYPE__.ProductIATA._APT_, __CMM_TYPE__.ProductIATA._ATT_, __CMM_TYPE__.ProductIATA._CCF_, __CMM_TYPE__.ProductIATA._CHN_, __CMM_TYPE__.ProductIATA._CNF_, __CMM_TYPE__.ProductIATA._CNT_, __CMM_TYPE__.ProductIATA._COM_, __CMM_TYPE__.ProductIATA._CON_, __CMM_TYPE__.ProductIATA._CSF_, __CMM_TYPE__.ProductIATA._CUF_, __CMM_TYPE__.ProductIATA._DEF_, __CMM_TYPE__.ProductIATA._DEH_, __CMM_TYPE__.ProductIATA._DIF_, __CMM_TYPE__.ProductIATA._DIS_, __CMM_TYPE__.ProductIATA._DMF_, __CMM_TYPE__.ProductIATA._ENV_, __CMM_TYPE__.ProductIATA._EWF_, __CMM_TYPE__.ProductIATA._FLF_, __CMM_TYPE__.ProductIATA._FMJ_, __CMM_TYPE__.ProductIATA._FRF_, __CMM_TYPE__.ProductIATA._GAF_, __CMM_TYPE__.ProductIATA._GSF_, __CMM_TYPE__.ProductIATA._HNG_, __CMM_TYPE__.ProductIATA._HUF_, __CMM_TYPE__.ProductIATA._HYD_, __CMM_TYPE__.ProductIATA._INF_, __CMM_TYPE__.ProductIATA._INS_, __CMM_TYPE__.ProductIATA._ITP_, __CMM_TYPE__.ProductIATA._JA1A_, __CMM_TYPE__.ProductIATA._JAA_, __CMM_TYPE__.ProductIATA._JETA_, __CMM_TYPE__.ProductIATA._JETA1_, __CMM_TYPE__.ProductIATA._JP4_, __CMM_TYPE__.ProductIATA._JP5_, __CMM_TYPE__.ProductIATA._JP8_, __CMM_TYPE__.ProductIATA._JSF_, __CMM_TYPE__.ProductIATA._LVF_, __CMM_TYPE__.ProductIATA._MVF_, __CMM_TYPE__.ProductIATA._NFF_, __CMM_TYPE__.ProductIATA._NON_, __CMM_TYPE__.ProductIATA._OFF_, __CMM_TYPE__.ProductIATA._OTH_, __CMM_TYPE__.ProductIATA._OVT_, __CMM_TYPE__.ProductIATA._OWF_, __CMM_TYPE__.ProductIATA._PLF_, __CMM_TYPE__.ProductIATA._REB_, __CMM_TYPE__.ProductIATA._STR_, __CMM_TYPE__.ProductIATA._SYS_, __CMM_TYPE__.ProductIATA._THP_, __CMM_TYPE__.ProductIATA._TPF_, __CMM_TYPE__.ProductIATA._TRF_, __CMM_TYPE__.ProductIATA._TS1_], hideLabel:"true"})
		.addLov({name:"registration", bind:"{d.aircraftIdentification}", dataIndex:"aircraftIdentification", width:160, xtype:"fmbas_AircraftLov_Lov", maxLength:10, hideLabel:"true",
			retFieldMapping: [{lovField:"registration", dsField: "aircraftIdentification"} ,{lovField:"acTypeCode", dsField: "aircraftType"} ],
			filterFieldMapping: [{lovField:"custCode", dsField: "receiverCode"} ],listeners:{
			fpchange:{scope:this, fn:this._disableAcType_}
		}})
		.addLov({name:"acType", bind:"{d.aircraftType}", dataIndex:"aircraftType", _enableFn_: function(dc, rec) { return this._canEditAcType_(rec); } , width:160, xtype:"fmbas_AcTypesLov_Lov", maxLength:4, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "aircraftType"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addTextField({ name:"flightId", bind:"{d.airlineFlightId}", dataIndex:"airlineFlightId", width:160, maxLength:10, hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"flightType", bind:"{d.internationalFlight}", dataIndex:"internationalFlight", width:160, store:[ __OPS_TYPE__.InternationalFlight._INTERNATIONAL_, __OPS_TYPE__.InternationalFlight._DOMESTIC_], hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"customsStatus", bind:"{d.customStatus}", dataIndex:"customStatus", width:160, store:[ __OPS_TYPE__.FuelTicketCustomsStatus._BONDED_, __OPS_TYPE__.FuelTicketCustomsStatus._DOMESTIC_, __OPS_TYPE__.FuelTicketCustomsStatus._FTZ_, __OPS_TYPE__.FuelTicketCustomsStatus._UNSPECIFIED_, __OPS_TYPE__.FuelTicketCustomsStatus._DUTY_PAID_, __OPS_TYPE__.FuelTicketCustomsStatus._DUTY_FREE_, __OPS_TYPE__.FuelTicketCustomsStatus._NOT_APPLICABLE_], hideLabel:"true"})
		.addLov({name:"departure", bind:"{d.airportCode}", dataIndex:"airportCode", width:160, xtype:"fmbas_LocationsLov_Lov", maxLength:5, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "airportCode"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ]})
		.addLov({name:"destination", bind:"{d.nextDestination}", dataIndex:"nextDestination", width:160, xtype:"fmbas_LocationsLov_Lov", maxLength:5, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "nextDestination"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ]})
		.addLov({name:"finalDest", bind:"{d.finalDestination}", dataIndex:"finalDestination", width:160, xtype:"fmbas_LocationsLov_Lov", maxLength:5, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "finalDestination"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ]})
		.addLov({name:"customer", bind:"{d.receiverCode}", dataIndex:"receiverCode", width:80, xtype:"fmbas_CustomerBlockedActiveLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "receiverCode"} ,{lovField:"name", dsField: "receiverName"} ]})
		.addLov({name:"supplier", bind:"{d.supplierCode}", dataIndex:"supplierCode", width:80, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "supplierCode"} ,{lovField:"name", dsField: "supplierName"} ],
			filterFieldMapping: [{lovField:"isSupplier", value: "true"}, {lovField:"fuelSupplier", value: "true"} ]})
		.addLov({name:"fueler", bind:"{d.intoPlaneCode}", dataIndex:"intoPlaneCode", width:80, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "intoPlaneCode"} ,{lovField:"name", dsField: "intoPlaneName"} ],
			filterFieldMapping: [{lovField:"isSupplier", value: "true"}, {lovField:"iplAgent", value: "true"} ]})
		.addLov({name:"ticketIssuer", bind:"{d.ticketIssuerCode}", dataIndex:"ticketIssuerCode", width:80, xtype:"fmbas_SubsidiaryLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "ticketIssuerCode"} ,{lovField:"name", dsField: "ticketIssuerName"} ],
			filterFieldMapping: [{lovField:"isSubsidiary", value: "true"} ]})
		.addTextField({ name:"customerName", bind:"{d.receiverName}", dataIndex:"receiverName", noEdit:true , width:160, maxLength:50, hideLabel:"true"})
		.addTextField({ name:"supplierName", bind:"{d.supplierName}", dataIndex:"supplierName", noEdit:true , width:160, maxLength:50, hideLabel:"true"})
		.addTextField({ name:"fuelerName", bind:"{d.intoPlaneName}", dataIndex:"intoPlaneName", noEdit:true , width:160, maxLength:50, hideLabel:"true"})
		.addTextField({ name:"ticketIssuerName", bind:"{d.ticketIssuerName}", dataIndex:"ticketIssuerName", noEdit:true , width:160, maxLength:50, hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"ticketType", bind:"{d.ticketType}", dataIndex:"ticketType", store:[ __OPS_TYPE__.TicketType._ORIGINAL_, __OPS_TYPE__.TicketType._REISSUE_, __OPS_TYPE__.TicketType._CANCEL_, __OPS_TYPE__.TicketType._DELETE_], cls:"sone-flat-kpi sone-flat-combo"})
		.addCombo({ xtype:"combo", name:"serviceType", bind:"{d.flightServiceType}", dataIndex:"flightServiceType", store:[ __CMM_TYPE__.FlightServiceType._AD_HOC_, __CMM_TYPE__.FlightServiceType._SCHEDULED_, __CMM_TYPE__.FlightServiceType._GENERAL_AVIATION_], cls:"sone-flat-kpi sone-flat-combo"})
		.addTextField({ name:"referenceTicket", bind:"{d.previousTicket}", dataIndex:"previousTicket", maxLength:16, cls:"sone-flat-kpi"})
		.addDisplayFieldText({ name:"validationStatus", bind:"{d.validationState}", dataIndex:"validationState", maxLength:16})
		.addDisplayFieldText({ name:"validationResult", bind:"{d.validationResult}", dataIndex:"validationResult", maxLength:2500})
		.addTextField({ name:"ticketNumber", bind:"{d.ticketNumber}", dataIndex:"ticketNumber", width:250, maxLength:20, cls:"sone-flat-kpi"})
		.addDisplayFieldText({ name:"deliveryDateLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelingStartLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelingEndLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"productLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelingOperLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"registrationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"acTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"flightIdLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"flightTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"customsStatusLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"departureLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"destinationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"finalDestLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"customerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"supplierLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"ticketIssuerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.add({name:"row", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("ticketNumber")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"mainTable", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table5", layout: {type:"table", columns:1}})
		.addPanel({ name:"p3",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "mainTable"])
		.addChildrenTo("mainTable", ["table1", "table2", "table3", "table4", "table5"])
		.addChildrenTo("table1", ["deliveryDateLabel", "deliveryDate", "fuelingStartLabel", "fuelingStart", "fuelingEndLabel", "fuelingEnd", "fuelingOperLabel", "fuelingOper", "productLabel", "product"])
		.addChildrenTo("table2", ["registrationLabel", "registration", "acTypeLabel", "acType", "flightIdLabel", "flightId", "flightTypeLabel", "flightType", "customsStatusLabel", "customsStatus"])
		.addChildrenTo("table3", ["departureLabel", "departure", "destinationLabel", "destination", "finalDestLabel", "finalDest"])
		.addChildrenTo("table4", ["customerLabel", "customer", "fuelerLabel", "fueler", "supplierLabel", "supplier", "ticketIssuerLabel", "ticketIssuer"])
		.addChildrenTo("table5", ["customerName", "fuelerName", "supplierName", "ticketIssuerName"])
		.addChildrenTo("p3", ["c1", "c2", "c3", "c4"])
		.addChildrenTo("titleAndKpi", ["title", "p3"])
		.addChildrenTo("title", ["row"])
		.addChildrenTo("c1", ["ticketType"])
		.addChildrenTo("c2", ["serviceType"])
		.addChildrenTo("c3", ["referenceTicket"])
		.addChildrenTo("c4", ["validationStatus"]);
	},
	/* ==================== Business functions ==================== */
	
	_afterDefineElements_: function() {
		
		
						this._validationStatusCounterId_ = Ext.id();
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
		
						// Dan: Buffered fuel tickets notifications counter
		
						this._getBuilder_().change("validationStatus",{
							listeners: {
								afterrender: {
									scope: this,
									fn: function(el) {
										var el = Ext.get(el.getId()),children = el.dom.childNodes, classSelector = "x-form-item-body", node;
		
										Ext.each(children, function(child){
										    if(child.className.indexOf(classSelector) !== -1) {
										        node = child;
										    }
										});
						
										if (node) {
											var counterEl = document.createElement("div");
											counterEl.className = "sone-kpi-counter";
											counterEl.id = this._validationStatusCounterId_;
											counterEl.style.display = "block";
											node.appendChild(counterEl);
										}
									}
								}
							}
						});
						
						this._controller_.on("afterDoServiceSuccess", function() {					
							this._applyStates_(this._controller_.getRecord());
						},this);
		
						
		
	},
	
	_canEditAcType_: function(record) {
		
						var reg = this._controller_.getRecord().get("aircraftIdentification");
						if(Ext.isEmpty(reg)){
							return record!= null
						} else { 
							return false 
						}
	},
	
	_disableAcType_: function(el,newVal,oldVal) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							var acType = this._get_("acType");
							if (!Ext.isEmpty(el.getValue())) {
								acType._disable_();
							}
							else {
								acType._enable_();
							}
						}
	}
});

/* ================= EDIT FORM: TabPayment ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTransaction_Dc$TabPayment", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTransaction_Dc$TabPayment",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"paymentType", bind:"{d.paymentType}", dataIndex:"paymentType", width:150, store:[ __CMM_TYPE__.PaymentType._CONTRACT_, __CMM_TYPE__.PaymentType._FUEL_CARD_, __CMM_TYPE__.PaymentType._CREDIT_CARD_, __CMM_TYPE__.PaymentType._CASH_], hideLabel:"true"})
		.addTextField({ name:"cardNumber", bind:"{d.cardNumber}", dataIndex:"cardNumber", width:150, hideLabel:"true", maxLength:20})
		.addDateField({name:"cardExp", bind:"{d.cardExpiry}", dataIndex:"cardExpiry", width:160, hideLabel:"true"})
		.addTextField({ name:"cardHolder", bind:"{d.cardName}", dataIndex:"cardName", width:160, maxLength:35, hideLabel:"true"})
		.addNumberField({name:"amountReceived", bind:"{d.amountReceived}", dataIndex:"amountReceived", width:120, sysDec:"dec_crncy", maxLength:19, hideLabel:"true"})
		.addLov({name:"amountCurr", bind:"{d.amountReceivedCurr}", dataIndex:"amountReceivedCurr", width:60, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, hideLabel:"true",
			retFieldMapping: [{lovField:"code", dsField: "amountReceivedCurr"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addDisplayFieldText({ name:"paymentTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"cardNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"cardExpLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"cardHolderLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row1Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("amountReceived"),this._getConfig_("amountCurr")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"mainTable", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:50px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["mainTable"])
		.addChildrenTo("mainTable", ["table1", "table2", "table3"])
		.addChildrenTo("table1", ["paymentTypeLabel", "paymentType", "cardNumberLabel", "cardNumber"])
		.addChildrenTo("table2", ["cardExpLabel", "cardExp", "cardHolderLabel", "cardHolder"])
		.addChildrenTo("table3", ["row1Label", "row1"]);
	}
});
