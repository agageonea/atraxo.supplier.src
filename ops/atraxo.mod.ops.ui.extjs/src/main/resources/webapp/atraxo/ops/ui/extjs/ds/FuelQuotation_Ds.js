/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelQuotation_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_FuelQuotation_Ds"
	},
	
	
	validators: {
		customerCode: [{type: 'presence'}],
		type: [{type: 'presence'}],
		postedOn: [{type: 'presence'}],
		validUntil: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("status", "New");
		this.set("type", "Ad-hoc");
		this.set("scope", "Into plane");
		this.set("pricingCycle", "Notice");
		this.set("postedOn", new Date());
		this.set("validUntil", Ext.Date.add(new Date(), Ext.Date.DAY, +30));
	},
	
	fields: [
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"creditTerms", type:"string", noFilter:true, noSort:true},
		{name:"creditLimit", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"creditCurrency", type:"string", noFilter:true, noSort:true},
		{name:"creditCard", type:"boolean", noFilter:true, noSort:true},
		{name:"exposure", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"customerEntityAlias", type:"string", noFilter:true},
		{name:"customerRefId", type:"string", noFilter:true},
		{name:"fuelRequestId", type:"int", allowNull:true},
		{name:"fuelRequestCode", type:"string"},
		{name:"code", type:"string"},
		{name:"type", type:"string"},
		{name:"scope", type:"string"},
		{name:"status", type:"string"},
		{name:"postedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validUntil", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"pricingCycle", type:"string"},
		{name:"openRelease", type:"boolean"},
		{name:"owner", type:"string"},
		{name:"disclaimer", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true, noFilter:true},
		{name:"avgMthdCode", type:"string"},
		{name:"fuelOrderId", type:"int", allowNull:true, noFilter:true},
		{name:"fuelOrderCode", type:"string"},
		{name:"finSrcId", type:"int", allowNull:true, noFilter:true},
		{name:"finSrcCode", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"subsidiaryName", type:"string"},
		{name:"paymentTerm", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"referenceTo", type:"string", noFilter:true, noSort:true},
		{name:"invFreq", type:"string", noFilter:true, noSort:true},
		{name:"payCurr", type:"string", noFilter:true, noSort:true},
		{name:"payCurrId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"period", type:"string", noFilter:true, noSort:true},
		{name:"primaryClientId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"primaryClientName", type:"string", noFilter:true, noSort:true},
		{name:"alertLimit", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelQuotation_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"creditTerms", type:"string", noFilter:true, noSort:true},
		{name:"creditLimit", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"creditCurrency", type:"string", noFilter:true, noSort:true},
		{name:"creditCard", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"exposure", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"customerEntityAlias", type:"string", noFilter:true},
		{name:"customerRefId", type:"string", noFilter:true},
		{name:"fuelRequestId", type:"int", allowNull:true},
		{name:"fuelRequestCode", type:"string"},
		{name:"code", type:"string"},
		{name:"type", type:"string"},
		{name:"scope", type:"string"},
		{name:"status", type:"string"},
		{name:"postedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validUntil", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"pricingCycle", type:"string"},
		{name:"openRelease", type:"boolean", allowNull:true},
		{name:"owner", type:"string"},
		{name:"disclaimer", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true, noFilter:true},
		{name:"avgMthdCode", type:"string"},
		{name:"fuelOrderId", type:"int", allowNull:true, noFilter:true},
		{name:"fuelOrderCode", type:"string"},
		{name:"finSrcId", type:"int", allowNull:true, noFilter:true},
		{name:"finSrcCode", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"subsidiaryName", type:"string"},
		{name:"paymentTerm", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"referenceTo", type:"string", noFilter:true, noSort:true},
		{name:"invFreq", type:"string", noFilter:true, noSort:true},
		{name:"payCurr", type:"string", noFilter:true, noSort:true},
		{name:"payCurrId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"period", type:"string", noFilter:true, noSort:true},
		{name:"primaryClientId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"primaryClientName", type:"string", noFilter:true, noSort:true},
		{name:"alertLimit", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.ops.ui.extjs.ds.FuelQuotation_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"action", type:"string"},
		{name:"orderCustomerRef", type:"string"},
		{name:"orderPostedOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"orderSubmittedBy", type:"int", allowNull:true},
		{name:"orderSubmittedName", type:"string"},
		{name:"remarks", type:"string"}
	]
});
