/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.CustomerLocationContracts_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_CustomerLocationContracts_Ds"
	},
	
	
	fields: [
		{name:"locId", type:"int", allowNull:true},
		{name:"locName", type:"string"},
		{name:"counterPartyId", type:"int", allowNull:true},
		{name:"counterPartyCode", type:"string"},
		{name:"code", type:"string"},
		{name:"type", type:"string"},
		{name:"scope", type:"string"},
		{name:"delivery", type:"string"},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"creditTerms", type:"string"},
		{name:"limitedTo", type:"string"},
		{name:"iplAgentCode", type:"string"},
		{name:"iplAgentId", type:"int", allowNull:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"pricingBases", type:"string", noFilter:true, noSort:true},
		{name:"pricingBaseId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"dft", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"totalPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"unit", type:"string", noFilter:true, noSort:true},
		{name:"exposure", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"quotationName", type:"string", noFilter:true, noSort:true},
		{name:"quotationId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"exchangeRateOffset", type:"string", noFilter:true, noSort:true},
		{name:"basePrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"fuelPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"differential", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"intoPlaneFee", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"otherFees", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"taxes", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"perFlightFee", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.CustomerLocationContracts_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"locId", type:"int", allowNull:true},
		{name:"locName", type:"string"},
		{name:"counterPartyId", type:"int", allowNull:true},
		{name:"counterPartyCode", type:"string"},
		{name:"code", type:"string"},
		{name:"type", type:"string"},
		{name:"scope", type:"string"},
		{name:"delivery", type:"string"},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"creditTerms", type:"string"},
		{name:"limitedTo", type:"string"},
		{name:"iplAgentCode", type:"string"},
		{name:"iplAgentId", type:"int", allowNull:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"pricingBases", type:"string", noFilter:true, noSort:true},
		{name:"pricingBaseId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"dft", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"totalPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"unit", type:"string", noFilter:true, noSort:true},
		{name:"exposure", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"quotationName", type:"string", noFilter:true, noSort:true},
		{name:"quotationId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"exchangeRateOffset", type:"string", noFilter:true, noSort:true},
		{name:"basePrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"fuelPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"differential", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"intoPlaneFee", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"otherFees", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"taxes", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"perFlightFee", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.ops.ui.extjs.ds.CustomerLocationContracts_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"avgMthId", type:"int", allowNull:true},
		{name:"endsOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"finSrcId", type:"int", allowNull:true},
		{name:"fqlCurrency", type:"int", allowNull:true},
		{name:"fqlUnit", type:"int", allowNull:true},
		{name:"period", type:"string"},
		{name:"startsOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT}
	]
});
