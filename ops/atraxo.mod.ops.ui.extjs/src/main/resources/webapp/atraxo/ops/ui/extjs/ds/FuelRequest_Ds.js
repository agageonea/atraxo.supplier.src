/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelRequest_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_FuelRequest_Ds"
	},
	
	
	initRecord: function() {
		this.set("processedBy", getApplication().session.user.code);
	},
	
	fields: [
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"customerRefId", type:"string", noFilter:true},
		{name:"customerEntityAlias", type:"string", noFilter:true},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"requestCode", type:"string"},
		{name:"requestDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"customerReferenceNo", type:"string", noFilter:true},
		{name:"requestedService", type:"string"},
		{name:"isOpenRelease", type:"boolean"},
		{name:"startDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"requestStatus", type:"string"},
		{name:"processedBy", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"subsidiaryName", type:"string"},
		{name:"fuelQuotationId", type:"int", allowNull:true, noFilter:true},
		{name:"fuelQuotationCode", type:"string"},
		{name:"quotNo", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelRequest_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"customerRefId", type:"string", noFilter:true},
		{name:"customerEntityAlias", type:"string", noFilter:true},
		{name:"contactId", type:"int", allowNull:true},
		{name:"contactName", type:"string"},
		{name:"requestCode", type:"string"},
		{name:"requestDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"customerReferenceNo", type:"string", noFilter:true},
		{name:"requestedService", type:"string"},
		{name:"isOpenRelease", type:"boolean", allowNull:true},
		{name:"startDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"requestStatus", type:"string"},
		{name:"processedBy", type:"string"},
		{name:"subsidiaryId", type:"int", allowNull:true},
		{name:"subsidiaryCode", type:"string"},
		{name:"subsidiaryName", type:"string"},
		{name:"fuelQuotationId", type:"int", allowNull:true, noFilter:true},
		{name:"fuelQuotationCode", type:"string"},
		{name:"quotNo", type:"string", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.ops.ui.extjs.ds.FuelRequest_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"action", type:"string"},
		{name:"remarks", type:"string"},
		{name:"tmpField", type:"string"}
	]
});
