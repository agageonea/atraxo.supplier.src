/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.frame.FuelOrder_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.FuelOrder_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("fuelOrder", Ext.create(atraxo.ops.ui.extjs.dc.FuelOrder_Dc,{}))
		.addDc("fuelOrderLoc", Ext.create(atraxo.ops.ui.extjs.dc.FuelOrderLocation_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.linkDc("fuelOrderLoc", "fuelOrder",{fetchMode:"auto",fields:[
					{childField:"fuelOrderId", parentField:"id"}]})
				.linkDc("history", "fuelOrder",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("note", "fuelOrder",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveEditWdw",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:false, handler: this.onBtnSaveEditWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"btnCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancel, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdw1",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw1, scope:this})
		.addButton({name:"helpWdw2",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw2, scope:this})
		.addButton({name:"btnRejectList",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnRejectList,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelOrder.status.nevv);} }], scope:this})
		.addButton({name:"btnRejectForm",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onBtnRejectForm,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelOrder.status.nevv);} }], scope:this})
		.addButton({name:"btnRevokeList",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnRevokeList,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.status!=__TYPES__.fuelOrder.status.rejected && dc.record.data.status!=__TYPES__.fuelOrder.status.canceled);} }], scope:this})
		.addButton({name:"btnRevokeForm",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onBtnRevokeForm,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.status!=__TYPES__.fuelOrder.status.rejected && dc.record.data.status!=__TYPES__.fuelOrder.status.canceled);} }], scope:this})
		.addButton({name:"btnReopenList",glyph:fp_asc.rollforward_glyph.glyph,iconCls: fp_asc.rollforward_glyph.css, disabled:true, handler: this.onBtnReopenList,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelOrder.status.rejected || dc.record.data.status==__TYPES__.fuelOrder.status.canceled);} }], scope:this})
		.addButton({name:"btnReopenForm",glyph:fp_asc.rollforward_glyph.glyph,iconCls: fp_asc.rollforward_glyph.css, disabled:true, handler: this.onBtnReopenForm,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelOrder.status.rejected || dc.record.data.status==__TYPES__.fuelOrder.status.canceled);} }], scope:this})
		.addButton({name:"btnConfirmList",glyph:fp_asc.commit_glyph.glyph,iconCls: fp_asc.commit_glyph.css, disabled:true, handler: this.onBtnConfirmList,stateManager:[{ name:"selected_not_zero", dc:"fuelOrder", and: function(dc) {return (this.canConfirm(dc));} }], scope:this})
		.addButton({name:"btnConfirmForm",glyph:fp_asc.commit_glyph.glyph,iconCls: fp_asc.commit_glyph.css, disabled:true, handler: this.onBtnConfirmForm,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelOrder.status.nevv);} }], scope:this})
		.addButton({name:"btnReleaseList",glyph:fp_asc.release_glyph.glyph,iconCls: fp_asc.release_glyph.css, disabled:true, handler: this.onBtnReleaseList,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelOrder.status.confirmed);} }], scope:this})
		.addButton({name:"btnReleaseForm",glyph:fp_asc.release_glyph.glyph,iconCls: fp_asc.release_glyph.css, disabled:true, handler: this.onBtnReleaseForm,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.status==__TYPES__.fuelOrder.status.confirmed);} }], scope:this})
		.addButton({name:"btnOpenList",glyph:fp_asc.list_glyph.glyph,iconCls: fp_asc.list_glyph.css, disabled:true, handler: this.onBtnOpenList,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.fuelQuoteId != null );} }], scope:this})
		.addButton({name:"btnOpenForm",glyph:fp_asc.list_glyph.glyph,iconCls: fp_asc.list_glyph.css, disabled:true, handler: this.onBtnOpenForm,stateManager:[{ name:"selected_one", dc:"fuelOrder", and: function(dc) {return (dc.record.data.fuelQuoteId != null);} }], scope:this})
		.addButton({name:"btnSaveCloseRemarkWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRemarkWdw, scope:this})
		.addButton({name:"btnCancelRemarkWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRemarkWdw, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"btnLocDetails",glyph:fp_asc.edit_glyph.glyph,iconCls: fp_asc.edit_glyph.css, disabled:true, handler: this.onBtnLocDetails,stateManager:[{ name:"selected_one", dc:"fuelOrderLoc"}], scope:this})
		.addDcFilterFormView("fuelOrder", {name:"Filter", xtype:"ops_FuelOrder_Dc$Filter"})
		.addDcGridView("fuelOrder", {name:"List", xtype:"ops_FuelOrder_Dc$List"})
		.addDcFormView("fuelOrder", {name:"New", xtype:"ops_FuelOrder_Dc$New"})
		.addDcFormView("fuelOrder", {name:"Edit", xtype:"ops_FuelOrder_Dc$Edit"})
		.addDcFilterFormView("fuelOrderLoc", {name:"Location", xtype:"ops_FuelOrderLocation_Dc$Filter"})
		.addDcGridView("fuelOrderLoc", {name:"Locations", _hasTitle_:true, xtype:"ops_FuelOrderLocation_Dc$List"})
		.addDcGridView("fuelOrder", {name:"Activities", _hasTitle_:true, xtype:"ops_FuelOrder_Dc$Temp",  disabled:true})
		.addDcGridView("fuelOrder", {name:"Documents", _hasTitle_:true, xtype:"ops_FuelOrder_Dc$Temp",  disabled:true})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("fuelOrder", {name:"remark", xtype:"ops_FuelOrder_Dc$EditRemark"})
		.addDcGridView("note", {name:"notesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"notesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addWindow({name:"wdwNewFuelOrder", _hasTitle_:true, width:460, height:280, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("New")],  listeners:{ close:{fn:this.onWdwFuelOrderClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnSaveEditWdw"), this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnCancel")]}]})
		.addWindow({name:"wdwRemarks", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("remark")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRemarkWdw"), this._elems_.get("btnCancelRemarkWdw")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("notesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas2", dcName:"fuelOrder"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["List"], ["center"])
		.addChildrenTo("canvas2", ["Edit", "detailsTab"], ["north", "center"])
		.addChildrenTo("detailsTab", ["Locations"])
		.addToolbarTo("List", "tlbList")
		.addToolbarTo("Edit", "tlbEdit")
		.addToolbarTo("notesList", "tlbNoteList")
		.addToolbarTo("Locations", "tlbLocationsList");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"])
		.addChildrenTo2("detailsTab", ["notesList", "History", "Activities", "Documents"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "fuelOrder"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnRejectList"),this._elems_.get("btnRevokeList"),this._elems_.get("btnReopenList"),this._elems_.get("btnConfirmList"),this._elems_.get("btnReleaseList"),this._elems_.get("btnOpenList"),this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbEdit", {dc: "fuelOrder"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnRejectForm"),this._elems_.get("btnRevokeForm"),this._elems_.get("btnReopenForm"),this._elems_.get("btnConfirmForm"),this._elems_.get("btnReleaseForm"),this._elems_.get("btnOpenForm"),this._elems_.get("helpWdw1")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbLocationsList", {dc: "fuelOrderLoc"})
			.addButtons([])
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("btnLocDetails"),this._elems_.get("helpWdw2")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
	}
	
	/**
	 * On-Click handler for button btnSaveEditWdw
	 */
	,onBtnSaveEditWdw: function() {
		this.saveEdit();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button btnCancel
	 */
	,onBtnCancel: function() {
		this.onWdwFuelOrderClose();
		this._getWindow_("wdwNewFuelOrder").close();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw1
	 */
	,onHelpWdw1: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdw2
	 */
	,onHelpWdw2: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button btnRejectList
	 */
	,onBtnRejectList: function() {
		this.rejectPurchaseOrder();
	}
	
	/**
	 * On-Click handler for button btnRejectForm
	 */
	,onBtnRejectForm: function() {
		this.rejectPurchaseOrder();
	}
	
	/**
	 * On-Click handler for button btnRevokeList
	 */
	,onBtnRevokeList: function() {
		this.cancelPurchaseOrder();
	}
	
	/**
	 * On-Click handler for button btnRevokeForm
	 */
	,onBtnRevokeForm: function() {
		this.cancelPurchaseOrder();
	}
	
	/**
	 * On-Click handler for button btnReopenList
	 */
	,onBtnReopenList: function() {
		this.reopenPurchaseOrder();
	}
	
	/**
	 * On-Click handler for button btnReopenForm
	 */
	,onBtnReopenForm: function() {
		this.reopenPurchaseOrder();
	}
	
	/**
	 * On-Click handler for button btnConfirmList
	 */
	,onBtnConfirmList: function() {
		this.confirmPurchaseOrder();
		var successFn = function() {
			this._getDc_("fuelOrder").doReloadPage();
		};
		var o={
			name:"updateStatusList",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fuelOrder").doRpcDataList(o);
	}
	
	/**
	 * On-Click handler for button btnConfirmForm
	 */
	,onBtnConfirmForm: function() {
		this.confirmPurchaseOrder();
		var successFn = function() {
			this._getDc_("fuelOrder").doReloadRecord();
			this._getDc_("history").doReloadPage();
		};
		var o={
			name:"updateStatus",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fuelOrder").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnReleaseList
	 */
	,onBtnReleaseList: function() {
		this.releasePurchaseOrder();
	}
	
	/**
	 * On-Click handler for button btnReleaseForm
	 */
	,onBtnReleaseForm: function() {
		this.releasePurchaseOrder();
	}
	
	/**
	 * On-Click handler for button btnOpenList
	 */
	,onBtnOpenList: function() {
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelQuotation_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				fuelQuotationCode: this._getDc_("fuelOrder").getRecord().get("fuelQuoteCode")
			},
			callback: function (params) {
				this._when_called_to_edit(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnOpenForm
	 */
	,onBtnOpenForm: function() {
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelQuotation_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				fuelQuotationCode: this._getDc_("fuelOrder").getRecord().get("fuelQuoteCode")
			},
			callback: function (params) {
				this._when_called_to_edit(params);
			}
		});
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRemarkWdw
	 */
	,onBtnSaveCloseRemarkWdw: function() {
		var successFn = function() {
			this._getWindow_("wdwRemarks").close();
			this._getDc_("fuelOrder").doReloadPage();
			this._getDc_("fuelOrder").setParamValue("remarks","")
		};
		var o={
			name:"updateStatus",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fuelOrder").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelRemarkWdw
	 */
	,onBtnCancelRemarkWdw: function() {
		this._getWindow_("wdwRemarks").close();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnLocDetails
	 */
	,onBtnLocDetails: function() {
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelOrderLocation_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				fuelOrderLocationId: this._getDc_("fuelOrderLoc").getRecord().get("id"),
				fuelOrderStatus: this._getDc_("fuelOrder").getRecord().get("status")
			},
			callback: function (params) {
				this._when_called_to_edit(params);
			}
		});
	}
	
	,showNewWdw: function() {	
		this._getWindow_("wdwNewFuelOrder").show();
	}
	
	,generateReport: function() {	
		var o={
			name:"generateReport",
			modal:true
		};
		this._getDc_("fuelOrder").doRpcData(o);
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,onNoteWdwClose: function() {	
		this._getDc_("note").doCancel();
	}
	
	,_afterDefineDcs_: function() {
		
					var fuelOrder = this._getDc_("fuelOrder");
		
					fuelOrder.on("afterDoNew", function(dc) {
						dc.setEditMode();
						this.showNewWdw();
					},this);
		
					fuelOrder.on("afterDoSaveSuccess", function (dc, ajaxResult) { 
		
						if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwNewFuelOrder").close();	
						} 
						else if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						}
						else if (ajaxResult.options.options.doCloseEditAfterSave === true) {
							this._getWindow_("wdwNewFuelOrder").close();	
							dc.doEditIn();
						}
					}, this);
		
					var note = this._getDc_("note");
		
					note.on("afterDoNew", function (dc) {
					    this.showNoteWdw();
					}, this);
		
					note.on("OnEditIn", function(dc) {
					   this._getWindow_("noteWdw").show();
					}, this);
					
					note.on("afterDoSaveSuccess", function(dc, ajaxResult) {
					    if (ajaxResult.options.options.doNewAfterSave === true) {
					        dc.doNew();
					    } else if (ajaxResult.options.options.doCloseNoteWindowAfterSave === true) {
					        this._getWindow_("noteWdw").close();
					    }
					}, this);
	}
	
	,_when_called_to_edit: function(params) {
		
					var dc = this._getDc_("fuelOrder");
					dc.doClearQuery();
					dc.setFilterValue("id", params.fuelOrderId);
					dc.doQuery();
					this._showStackedViewElement_("main", "canvas2");			
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/FuelOrders.html";
					window.open( url, "SONE_Help");
	}
	
	,setActionAndRemarks: function(status,remarks) {
		 
					var dc = this._getDc_("fuelOrder");
					dc.setParamValue("action", status);
					dc.setParamValue("remarks", remarks);
	}
	
	,saveEdit: function() {
		
					var fuelOrder = this._getDc_("fuelOrder");
					fuelOrder.doSave({doCloseEditAfterSave: true });
	}
	
	,saveNew: function() {
		
					var fuelOrder = this._getDc_("fuelOrder");
					fuelOrder.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var fuelOrder = this._getDc_("fuelOrder");
					fuelOrder.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,onWdwFuelOrderClose: function() {
		
					var dc = this._getDc_("fuelOrder");
					dc.clearEditMode();
					if(dc.isDirty()){
							dc.doCancel();
						}
	}
	
	,setupHistoryWindow: function(theWindow,theForm,saveBtn,cancelBtn,action,label,title,status) {
		
					var window = this._getWindow_(theWindow);
					 
					var remarksField = this._get_(theForm)._get_("remarks");
					var dc = this._getDc_("fuelOrder");
		
					var saveButton = this._get_(saveBtn);
					var cancelButton = this._get_(cancelBtn);
						
					saveButton.setDisabled(true);
					cancelButton.setDisabled(false);
					dc.setParamValue("action", action);
					dc.setParamValue("paramStatus", status);
					dc.setParamValue("remark", "");
						
					remarksField.setFieldLabel(label);
					window.setTitle(title);
		
					remarksField.on("change", function(field) {
						if (field.value.length > 0) {
							saveButton.setDisabled(false);
						} else {
						    saveButton.setDisabled(true);
						}
					});
		
					//TODO: formul  apare disabled  o singura data. Hai sa-l fortam in enable, si ramine de vazut de la ce ... 
					var f = this._get_(theForm);
					if (f.disabled) {
						f.enable();
					}
		
					window.show();
	}
	
	,rejectPurchaseOrder: function() {
		
					var label = "You are about to reject the fuel order.<br/> Please provide a reason.";
					var title = "Reject Fuel Order";
					var action = __TYPES__.fuelOrder.status.rejected;
					var status = __TYPES__.fuelOrder.status.rejected;
					this.setupHistoryWindow("wdwRemarks", "remark", "btnSaveCloseRemarkWdw", "btnCancelRemarkWdw", action, label , title , status);	
	}
	
	,reopenPurchaseOrder: function() {
		
					var label = "You are about to reopen the fuel order.<br/> Please provide a reason.";
					var title = "Reopen Fuel Order";
					var action = "Reopen";
					var status = __TYPES__.fuelOrder.status.nevv;
					this.setupHistoryWindow("wdwRemarks", "remark", "btnSaveCloseRemarkWdw", "btnCancelRemarkWdw", action, label , title, status);	
	}
	
	,confirmPurchaseOrder: function() {
		
					var dc = this._getDc_("fuelOrder");
					dc.setParamValue("action", __TYPES__.fuelOrder.status.confirmed);
					dc.setParamValue("remark", __TYPES__.fuelOrder.status.confirmed);
					dc.setParamValue("paramStatus", __TYPES__.fuelOrder.status.confirmed);
	}
	
	,releasePurchaseOrder: function() {
		
					var dc = this._getDc_("fuelOrder");
					dc.setParamValue("action", __TYPES__.fuelOrder.status.released);
					dc.setParamValue("remark", __TYPES__.fuelOrder.status.released);
					dc.setParamValue("paramStatus", __TYPES__.fuelOrder.status.released);
		
					var successFn = function(dc,response,serviceName,specs) {
						this._getDc_("fuelOrder").doReloadRecord();
						this._applyStateAllButtons_();
					};
					var o={
						name:"updateStatusToRelease",
						callbacks:{
							successFn: successFn,
							successScope: this
						},
						modal:true
					};
		
					dc.doRpcData(o);			
					this._getDc_("history").doReloadPage();
		
					this.generateReport();
			
	}
	
	,cancelPurchaseOrder: function() {
		
					var label = "You are about to cancel the fuel order.<br/> Please provide a reason.";
					var title = "Cancel Fuel Order";
					var action = __TYPES__.fuelOrder.status.canceled;
					var status = __TYPES__.fuelOrder.status.canceled;
					this.setupHistoryWindow("wdwRemarks", "remark", "btnSaveCloseRemarkWdw", "btnCancelRemarkWdw", action, label , title , status);	
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,reloadFuelOrderLoc: function() {
		
						var fuelOrderLocDc = this._getDc_("fuelOrderLoc");
						var fuelOrderDc = this._getDc_("fuelOrder");
						fuelOrderLocDc.doReloadPage();
						fuelOrderDc.doReloadRecord();
	}
	
	,canConfirm: function(dc) {
		
					var selectedRecords = dc.selectedRecords;
					for(var i=0 ; i<selectedRecords.length ; ++i){
						var record = selectedRecords[i];
						if(record.data.status != __TYPES__.fuelOrder.status.nevv ){
							return false;
						}
					}
					return true;
	}
});
