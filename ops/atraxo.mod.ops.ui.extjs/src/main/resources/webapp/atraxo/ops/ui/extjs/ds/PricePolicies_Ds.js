/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.PricePolicies_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_PricePolicies_Ds"
	},
	
	
	fields: [
		{name:"uniId", type:"int", allowNull:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"policyName", type:"string"},
		{name:"active", type:"boolean"},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"defaultMargin", type:"float", allowNull:true},
		{name:"defaultMarkup", type:"float", allowNull:true},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"aircraftTypeId", type:"int", allowNull:true},
		{name:"aircraftType", type:"string"},
		{name:"policyType", type:"string", noFilter:true, noSort:true},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"policyUnit", type:"string", noFilter:true, noSort:true},
		{name:"markup", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"sellingFuelPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"differential", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"totalSellingPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"gridProductPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"gridDft", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"sellingUnit", type:"string", noFilter:true, noSort:true},
		{name:"pricePolicyId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.PricePolicies_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"uniId", type:"int", allowNull:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"policyName", type:"string"},
		{name:"active", type:"boolean", allowNull:true},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"defaultMargin", type:"float", allowNull:true},
		{name:"defaultMarkup", type:"float", allowNull:true},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"aircraftTypeId", type:"int", allowNull:true},
		{name:"aircraftType", type:"string"},
		{name:"policyType", type:"string", noFilter:true, noSort:true},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"policyUnit", type:"string", noFilter:true, noSort:true},
		{name:"markup", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"sellingFuelPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"differential", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"totalSellingPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"gridProductPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"gridDft", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"sellingUnit", type:"string", noFilter:true, noSort:true},
		{name:"pricePolicyId", type:"int", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.ops.ui.extjs.ds.PricePolicies_DsParam", {
	extend: 'Ext.data.Model',



	fields: [
		{name:"avgMthdId", type:"int", allowNull:true},
		{name:"currecnyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"dft", type:"float", allowNull:true},
		{name:"finSrcId", type:"int", allowNull:true},
		{name:"fqCustomerId", type:"int", allowNull:true},
		{name:"fqLocationId", type:"int", allowNull:true},
		{name:"fuelPrice", type:"float", allowNull:true},
		{name:"period", type:"string"},
		{name:"productPrice", type:"float", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true}
	]
});
