/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.ArchivedFuelTicket_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_ArchivedFuelTicket_Ds"
	},
	
	
	validators: {
		ticketNo: [{type: 'presence'}],
		upliftVolume: [{type: 'presence'}],
		flightNo: [{type: 'presence'}],
		deliveryDate: [{type: 'presence'}],
		depCode: [{type: 'presence'}],
		custCode: [{type: 'presence'}],
		suppCode: [{type: 'presence'}],
		fuelingOperation: [{type: 'presence'}],
		productType: [{type: 'presence'}],
		customsStatus: [{type: 'presence'}],
		fuellerCode: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("upliftUnitCode", _SYSTEMPARAMETERS_.sysvol);
		this.set("deliveryDate", new Date());
		this.set("suppCode", getApplication().session.client.code);
		this.set("fuellerCode", getApplication().session.client.code);
		this.set("customsStatus", "Unspecified");
		this.set("indicator", "Unspecified");
		this.set("productType", "Jet-A");
		this.set("fuelingOperation", "Uplift");
		this.set("netUpliftUnitCode", _SYSTEMPARAMETERS_.sysvol);
		this.set("beforeFuelingUnitCode", _SYSTEMPARAMETERS_.sysvol);
		this.set("afterFuelingUnitCode", _SYSTEMPARAMETERS_.sysvol);
		this.set("deliveryType", "IP");
		this.set("source", "Manual");
		this.set("ticketStatus", "Original");
		this.set("transmissionStatus", "New");
		this.set("approvalStatus", "For approval");
		this.set("capturedBy", getApplication().session.user.name);
		this.set("densityUnitCode", _SYSTEMPARAMETERS_.sysweight);
		this.set("densityVolumeCode", _SYSTEMPARAMETERS_.sysvol);
		this.set("fuelingStartDate", Ext.Date.add(new Date(), Ext.Date.DAY, -1));
		this.set("fuelingEndDate", Ext.Date.add(new Date(), Ext.Date.SECOND, 24*60*60*(-1)+60));
		this.set("billStatus", "Not billed");
		this.set("invoiceStatus", "Not invoiced");
		this.set("temperatureUnit", "C");
		this.set("sendInvoiceBy", "E-mail");
		this.set("paymentType", "Contract");
	},
	
	fields: [
		{name:"depId", type:"int", allowNull:true},
		{name:"depCode", type:"string"},
		{name:"custId", type:"int", allowNull:true},
		{name:"custCode", type:"string"},
		{name:"upliftUnitId", type:"int", allowNull:true},
		{name:"upliftUnitCode", type:"string"},
		{name:"suppId", type:"int", allowNull:true},
		{name:"suppCode", type:"string"},
		{name:"fuellerId", type:"int", allowNull:true},
		{name:"fuellerCode", type:"string"},
		{name:"destId", type:"int", allowNull:true},
		{name:"destCode", type:"string"},
		{name:"finalDestId", type:"int", allowNull:true},
		{name:"finalDestCode", type:"string"},
		{name:"netUpliftUnitId", type:"int", allowNull:true},
		{name:"netUpliftUnitCode", type:"string"},
		{name:"beforeFuelingUnitId", type:"int", allowNull:true},
		{name:"beforeFuelingUnitCode", type:"string"},
		{name:"afterFuelingUnitId", type:"int", allowNull:true},
		{name:"afterFuelingUnitCode", type:"string"},
		{name:"densityUnitId", type:"int", allowNull:true},
		{name:"densityUnitCode", type:"string"},
		{name:"densityVolumeId", type:"int", allowNull:true},
		{name:"densityVolumeCode", type:"string"},
		{name:"acTypeId", type:"int", allowNull:true},
		{name:"acTypeCode", type:"string"},
		{name:"aircraftId", type:"int", allowNull:true},
		{name:"aircraftReg", type:"string"},
		{name:"resellerId", type:"int", allowNull:true},
		{name:"resellerCode", type:"string"},
		{name:"amountCurrId", type:"int", allowNull:true},
		{name:"amountCurrCode", type:"string"},
		{name:"refTicketId", type:"int", allowNull:true},
		{name:"refTicketNo", type:"string"},
		{name:"upliftVolume", type:"float", allowNull:true},
		{name:"ticketNo", type:"string"},
		{name:"deliveryDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"airlineDesignator", type:"string"},
		{name:"flightNo", type:"string"},
		{name:"suffix", type:"string"},
		{name:"customsStatus", type:"string"},
		{name:"productType", type:"string"},
		{name:"fuelingOperation", type:"string"},
		{name:"indicator", type:"string"},
		{name:"netUpliftQuantity", type:"float", allowNull:true},
		{name:"beforeFueling", type:"float", allowNull:true},
		{name:"afterFueling", type:"float", allowNull:true},
		{name:"density", type:"float", allowNull:true},
		{name:"fuelingStartDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fuelingEndDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliveryType", type:"string"},
		{name:"source", type:"string"},
		{name:"transport", type:"string"},
		{name:"ticketStatus", type:"string"},
		{name:"transmissionStatus", type:"string"},
		{name:"transmissionDetails", type:"string"},
		{name:"approvalStatus", type:"string"},
		{name:"transmissionDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"capturedBy", type:"string"},
		{name:"approvedBy", type:"string"},
		{name:"toleranceMessage", type:"string"},
		{name:"billStatus", type:"string"},
		{name:"invoiceStatus", type:"string"},
		{name:"meterStartIndex", type:"int", allowNull:true},
		{name:"meterEndIndex", type:"int", allowNull:true},
		{name:"temperature", type:"int", allowNull:true},
		{name:"temperatureUnit", type:"string"},
		{name:"refuelerArrivalTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"paymentType", type:"string"},
		{name:"cardNumber", type:"string"},
		{name:"cardExpiringDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"cardHolder", type:"string"},
		{name:"amountReceived", type:"float", allowNull:true},
		{name:"sendInvoiceBy", type:"string"},
		{name:"sendInvoiceTo", type:"string"},
		{name:"isDeleted", type:"boolean"},
		{name:"flightServiceType", type:"string"},
		{name:"tmp", type:"string", noFilter:true, noSort:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"flightID", type:"string", noFilter:true, noSort:true},
		{name:"isHardCopy", type:"boolean", noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.ArchivedFuelTicket_DsFilter", {
	extend: 'Ext.data.Model',
	
	getFilterAttributes : function(){
		return ["deliveryDate_cutTime"];
	},
	
	fields: [
		{name:"depId", type:"int", allowNull:true},
		{name:"depCode", type:"string"},
		{name:"custId", type:"int", allowNull:true},
		{name:"custCode", type:"string"},
		{name:"upliftUnitId", type:"int", allowNull:true},
		{name:"upliftUnitCode", type:"string"},
		{name:"suppId", type:"int", allowNull:true},
		{name:"suppCode", type:"string"},
		{name:"fuellerId", type:"int", allowNull:true},
		{name:"fuellerCode", type:"string"},
		{name:"destId", type:"int", allowNull:true},
		{name:"destCode", type:"string"},
		{name:"finalDestId", type:"int", allowNull:true},
		{name:"finalDestCode", type:"string"},
		{name:"netUpliftUnitId", type:"int", allowNull:true},
		{name:"netUpliftUnitCode", type:"string"},
		{name:"beforeFuelingUnitId", type:"int", allowNull:true},
		{name:"beforeFuelingUnitCode", type:"string"},
		{name:"afterFuelingUnitId", type:"int", allowNull:true},
		{name:"afterFuelingUnitCode", type:"string"},
		{name:"densityUnitId", type:"int", allowNull:true},
		{name:"densityUnitCode", type:"string"},
		{name:"densityVolumeId", type:"int", allowNull:true},
		{name:"densityVolumeCode", type:"string"},
		{name:"acTypeId", type:"int", allowNull:true},
		{name:"acTypeCode", type:"string"},
		{name:"aircraftId", type:"int", allowNull:true},
		{name:"aircraftReg", type:"string"},
		{name:"resellerId", type:"int", allowNull:true},
		{name:"resellerCode", type:"string"},
		{name:"amountCurrId", type:"int", allowNull:true},
		{name:"amountCurrCode", type:"string"},
		{name:"refTicketId", type:"int", allowNull:true},
		{name:"refTicketNo", type:"string"},
		{name:"upliftVolume", type:"float", allowNull:true},
		{name:"ticketNo", type:"string"},
		{name:"deliveryDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"airlineDesignator", type:"string"},
		{name:"flightNo", type:"string"},
		{name:"suffix", type:"string"},
		{name:"customsStatus", type:"string"},
		{name:"productType", type:"string"},
		{name:"fuelingOperation", type:"string"},
		{name:"indicator", type:"string"},
		{name:"netUpliftQuantity", type:"float", allowNull:true},
		{name:"beforeFueling", type:"float", allowNull:true},
		{name:"afterFueling", type:"float", allowNull:true},
		{name:"density", type:"float", allowNull:true},
		{name:"fuelingStartDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fuelingEndDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"deliveryType", type:"string"},
		{name:"source", type:"string"},
		{name:"transport", type:"string"},
		{name:"ticketStatus", type:"string"},
		{name:"transmissionStatus", type:"string"},
		{name:"transmissionDetails", type:"string"},
		{name:"approvalStatus", type:"string"},
		{name:"transmissionDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"capturedBy", type:"string"},
		{name:"approvedBy", type:"string"},
		{name:"toleranceMessage", type:"string"},
		{name:"billStatus", type:"string"},
		{name:"invoiceStatus", type:"string"},
		{name:"meterStartIndex", type:"int", allowNull:true},
		{name:"meterEndIndex", type:"int", allowNull:true},
		{name:"temperature", type:"int", allowNull:true},
		{name:"temperatureUnit", type:"string"},
		{name:"refuelerArrivalTime", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"paymentType", type:"string"},
		{name:"cardNumber", type:"string"},
		{name:"cardExpiringDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"cardHolder", type:"string"},
		{name:"amountReceived", type:"float", allowNull:true},
		{name:"sendInvoiceBy", type:"string"},
		{name:"sendInvoiceTo", type:"string"},
		{name:"isDeleted", type:"boolean", allowNull:true},
		{name:"flightServiceType", type:"string"},
		{name:"tmp", type:"string", noFilter:true, noSort:true},
		{name:"formTitle", type:"string", noFilter:true, noSort:true},
		{name:"flightID", type:"string", noFilter:true, noSort:true},
		{name:"isHardCopy", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"labelField", type:"string", noFilter:true, noSort:true},
		{name:"subsidiaryId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.ops.ui.extjs.ds.ArchivedFuelTicket_DsParam", {
	extend: 'Ext.data.Model',



	initParam: function() {
		this.set("customer", "FFT");
	},

	fields: [
		{name:"customer", type:"string"},
		{name:"hardCopy", type:"string"},
		{name:"locationCode", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"newFuelTicketId", type:"int", allowNull:true},
		{name:"newFuelTicketNumber", type:"string"},
		{name:"remark", type:"string"},
		{name:"ticketIdToHC", type:"int", allowNull:true}
	]
});
