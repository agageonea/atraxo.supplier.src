/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ops.ui.extjs.ds.FuelTicket_DsParam,
	recordModel: atraxo.ops.ui.extjs.ds.FuelTicket_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FuelTicket_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateTimeField({name:"date", dataIndex:"deliveryDate"})
			.addLov({name:"customer", dataIndex:"custCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "custId"} ]}})
			.addLov({name:"customerName", dataIndex:"custName", maxLength:100,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerLov_Lov", selectOnFocus:true, maxLength:100,
					retFieldMapping: [{lovField:"id", dsField: "custId"} ]}})
			.addLov({name:"registration", dataIndex:"aircraftReg", maxLength:10,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.AircraftLov_Lov", selectOnFocus:true, maxLength:10,
					retFieldMapping: [{lovField:"id", dsField: "aircraftId"} ,{lovField:"acTypeId", dsField: "acTypeId"} ,{lovField:"acTypeCode", dsField: "acTypeCode"} ]}})
			.addTextField({ name:"flightNo", dataIndex:"flightNo", maxLength:4, caseRestriction:"uppercase"})
			.addTextField({ name:"airlineDesignator", dataIndex:"airlineDesignator", maxLength:3, caseRestriction:"uppercase"})
			.addLov({name:"departure", dataIndex:"depCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "depId"} ],
					filterFieldMapping: [{lovField:"isAirport", value: "true"} ]}})
			.addLov({name:"destination", dataIndex:"destCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25,
					retFieldMapping: [{lovField:"id", dsField: "destId"} ],
					filterFieldMapping: [{lovField:"isAirport", value: "true"} ]}})
			.addTextField({ name:"ticketNo", dataIndex:"ticketNo", maxLength:32})
			.addLov({name:"fueler", dataIndex:"fuellerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "fuellerId"} ],
					filterFieldMapping: [{lovField:"isSupplier", value: "true"}, {lovField:"iplAgent", value: "true"} ]}})
			.addLov({name:"supplier", dataIndex:"suppCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "suppId"} ],
					filterFieldMapping: [{lovField:"isSupplier", value: "true"}, {lovField:"fuelSupplier", value: "true"} ]}})
			.addNumberField({name:"quantity", dataIndex:"upliftVolume", sysDec:"dec_unit", maxLength:19})
			.addLov({name:"upliftUnit", dataIndex:"upliftUnitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2,
					retFieldMapping: [{lovField:"id", dsField: "upliftUnitId"} ]}})
			.addCombo({ xtype:"combo", name:"approval", dataIndex:"approvalStatus", store:[ __OPS_TYPE__.FuelTicketApprovalStatus._FOR_APPROVAL_, __OPS_TYPE__.FuelTicketApprovalStatus._AWAITING_APPROVAL_, __OPS_TYPE__.FuelTicketApprovalStatus._OK_, __OPS_TYPE__.FuelTicketApprovalStatus._REJECTED_, __OPS_TYPE__.FuelTicketApprovalStatus._RECHECK_]})
			.addCombo({ xtype:"combo", name:"ticketStatus", dataIndex:"ticketStatus", store:[ __OPS_TYPE__.FuelTicketStatus._ORIGINAL_, __OPS_TYPE__.FuelTicketStatus._UPDATED_, __OPS_TYPE__.FuelTicketStatus._CANCELED_, __OPS_TYPE__.FuelTicketStatus._REISSUE_]})
			.addCombo({ xtype:"combo", name:"transmissionStatus", dataIndex:"transmissionStatus", store:[ __OPS_TYPE__.FuelTicketTransmissionStatus._NEW_, __OPS_TYPE__.FuelTicketTransmissionStatus._SUBMITTED_, __OPS_TYPE__.FuelTicketTransmissionStatus._FAILED_, __OPS_TYPE__.FuelTicketTransmissionStatus._EXPORTED_, __OPS_TYPE__.FuelTicketTransmissionStatus._TRANSMITTED_]})
			.addLov({name:"capturedBy", dataIndex:"capturedBy", width:100, maxLength:255,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserListLov_Lov", selectOnFocus:true, maxLength:255,
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addLov({name:"approvedBy", dataIndex:"approvedBy", width:100, maxLength:255,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserListLov_Lov", selectOnFocus:true, maxLength:255,
					filterFieldMapping: [{lovField:"active", value: "true"} ]}})
			.addCombo({ xtype:"combo", name:"source", dataIndex:"source", store:[ __OPS_TYPE__.FuelTicketSource._MANUAL_, __OPS_TYPE__.FuelTicketSource._IMPORT_, __OPS_TYPE__.FuelTicketSource._SCAN_]})
			.addNumberField({name:"meterStartIndex", dataIndex:"meterStartIndex", maxLength:11})
			.addNumberField({name:"meterEndIndex", dataIndex:"meterEndIndex", maxLength:11})
			.addNumberField({name:"temperature", dataIndex:"temperature", maxLength:11})
			.addTextField({ name:"temperatureUnit", dataIndex:"temperatureUnit", maxLength:32})
			.addDateTimeField({name:"refuelerArrivalTime", dataIndex:"refuelerArrivalTime"})
			.addCombo({ xtype:"combo", name:"paymentType", dataIndex:"paymentType", store:[ __CMM_TYPE__.PaymentType._CONTRACT_, __CMM_TYPE__.PaymentType._FUEL_CARD_, __CMM_TYPE__.PaymentType._CREDIT_CARD_, __CMM_TYPE__.PaymentType._CASH_]})
			.addLov({name:"resseler", dataIndex:"resellerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "resellerId"} ],
					filterFieldMapping: [{lovField:"natureOfBusiness", value: "Reseller"} ]}})
			.addTextField({ name:"sendInvoiceTo", dataIndex:"sendInvoiceTo", maxLength:64})
			.addTextField({ name:"refTicketNo", dataIndex:"refTicketNo", maxLength:32})
			.addLov({name:"subsidiary", dataIndex:"subsidiaryCode", maxLength:64, caseRestriction:"uppercase",
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SubsidiaryLov_Lov", selectOnFocus:true, maxLength:64, caseRestriction:"uppercase",
					retFieldMapping: [{lovField:"refid", dsField: "subsidiaryId"} ],
					filterFieldMapping: [{lovField:"isSubsidiary", value: "true"} ]}})
			.addCombo({ xtype:"combo", name:"revocationRason", dataIndex:"revocationReason", store:[ __OPS_TYPE__.RevocationReason._INCORRECT_CUSTOMER_INFORMATION_, __OPS_TYPE__.RevocationReason._INCORRECT_FLIGHT_AIRCRAFT_INFORMATION_, __OPS_TYPE__.RevocationReason._INCORRECT_VOLUME_, __OPS_TYPE__.RevocationReason._INCORRECT_PRICE___DISCOUNT___TAX___FREIGHT_, __OPS_TYPE__.RevocationReason._COMMERCIAL_REASON_]})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelTicket_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"ticketStatus", dataIndex:"ticketStatus", width:160,  draggable:false, hideable:false, renderer:function(value, metaData) {return this._badgifyColumn_(value); }})
		.addDateColumn({ name:"date", dataIndex:"deliveryDate", width:120, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"customer", dataIndex:"custCode", width:70})
		.addTextColumn({ name:"customerName", dataIndex:"custName", width:150})
		.addTextColumn({ name:"registration", dataIndex:"aircraftReg", width:85})
		.addTextColumn({ name:"acType", dataIndex:"acTypeCode", hidden:true, width:85})
		.addTextColumn({ name:"flightID", dataIndex:"flightID", width:100})
		.addTextColumn({ name:"suffix", dataIndex:"suffix", hidden:true, width:100})
		.addTextColumn({ name:"airlineDesignator", dataIndex:"airlineDesignator", hidden:true, width:110})
		.addTextColumn({ name:"departure", dataIndex:"depCode", width:75})
		.addTextColumn({ name:"destination", dataIndex:"destCode", width:80})
		.addTextColumn({ name:"ticketNo", dataIndex:"ticketNo", width:100})
		.addTextColumn({ name:"supplier", dataIndex:"suppCode", width:70})
		.addTextColumn({ name:"fueller", dataIndex:"fuellerCode", hidden:true, width:60})
		.addTextColumn({ name:"reseller", dataIndex:"resellerCode", hidden:true, width:60})
		.addNumberColumn({ name:"quantity", dataIndex:"upliftVolume", width:80, sysDec:"dec_unit"})
		.addTextColumn({ name:"upliftUnit", dataIndex:"upliftUnitCode", width:40})
		.addTextColumn({ name:"fuelingOp", dataIndex:"fuelingOperation", hidden:true, width:110})
		.addTextColumn({ name:"productType", dataIndex:"productType", hidden:true, width:100})
		.addTextColumn({ name:"customs", dataIndex:"customsStatus", hidden:true, width:100})
		.addTextColumn({ name:"flightType", dataIndex:"indicator", hidden:true, width:90})
		.addTextColumn({ name:"approval", dataIndex:"approvalStatus", width:110})
		.addTextColumn({ name:"transmissionStatus", dataIndex:"transmissionStatus", width:130})
		.addTextColumn({ name:"capturedBy", dataIndex:"capturedBy", width:100})
		.addTextColumn({ name:"approvedBy", dataIndex:"approvedBy", width:100})
		.addTextColumn({ name:"finalDest", dataIndex:"finalDestCode", hidden:true, width:110})
		.addTextColumn({ name:"transport", dataIndex:"transport", hidden:true, width:80})
		.addNumberColumn({ name:"netQuant", dataIndex:"netUpliftQuantity", hidden:true, width:90, sysDec:"dec_unit",  renderer:function(value) {return Ext.util.Format.number(Math.round(value), Main.getNumberFormat(2)) }})
		.addTextColumn({ name:"netQuantUnit", dataIndex:"netUpliftUnitCode", hidden:true, width:40})
		.addNumberColumn({ name:"beforeFuel", dataIndex:"beforeFueling", hidden:true, width:90, sysDec:"dec_unit",  renderer:function(value) {return Ext.util.Format.number(Math.round(value), Main.getNumberFormat(2)) }})
		.addTextColumn({ name:"beforeUnit", dataIndex:"beforeFuelingUnitCode", hidden:true, width:40})
		.addNumberColumn({ name:"afterFuel", dataIndex:"afterFueling", hidden:true, width:90, sysDec:"dec_unit",  renderer:function(value) {return Ext.util.Format.number(Math.round(value), Main.getNumberFormat(2)) }})
		.addTextColumn({ name:"afterUnit", dataIndex:"afterFuelingUnitCode", hidden:true, width:50})
		.addNumberColumn({ name:"meterStartIndex", dataIndex:"meterStartIndex", hidden:true})
		.addNumberColumn({ name:"meterEndIndex", dataIndex:"meterEndIndex", hidden:true})
		.addNumberColumn({ name:"density", dataIndex:"density", hidden:true, width:80, sysDec:"dec_prc"})
		.addTextColumn({ name:"densityMass", dataIndex:"densityUnitCode", hidden:true, width:50})
		.addTextColumn({ name:"densityVol", dataIndex:"densityVolumeCode", hidden:true, width:60})
		.addNumberColumn({ name:"temperature", dataIndex:"temperature", hidden:true, width:110})
		.addTextColumn({ name:"temperatureUnit", dataIndex:"temperatureUnit", hidden:true, width:110})
		.addDateColumn({ name:"refuelerArrivalTime", dataIndex:"refuelerArrivalTime", hidden:true, width:110, _mask_: Masks.DATETIME})
		.addDateColumn({ name:"fuelStart", dataIndex:"fuelingStartDate", hidden:true, width:110, _mask_: Masks.DATETIME})
		.addDateColumn({ name:"fuelEnd", dataIndex:"fuelingEndDate", hidden:true, width:110, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"paymentType", dataIndex:"paymentType", hidden:true, width:110})
		.addTextColumn({ name:"cardNumber", dataIndex:"cardNumber", hidden:true, width:60})
		.addDateColumn({ name:"cardExp", dataIndex:"cardExpiringDate", hidden:true, width:120, _mask_: Masks.DATE})
		.addTextColumn({ name:"cardHolder", dataIndex:"cardHolder", hidden:true, width:100})
		.addNumberColumn({ name:"amountReceived", dataIndex:"amountReceived", hidden:true, width:120, sysDec:"dec_crncy"})
		.addTextColumn({ name:"amountCurr", dataIndex:"amountCurrCode", hidden:true, width:70})
		.addTextColumn({ name:"sendInvoiceBy", dataIndex:"sendInvoiceBy", hidden:true, width:110})
		.addTextColumn({ name:"sendInvoiceTo", dataIndex:"sendInvoiceTo", hidden:true, width:110})
		.addTextColumn({ name:"flightServiceType", dataIndex:"flightServiceType", hidden:true, width:110})
		.addTextColumn({ name:"subsidiaryCode", dataIndex:"subsidiaryCode", hidden:true, width:110})
		.addTextColumn({ name:"source", dataIndex:"source", hidden:true, width:80})
		.addTextColumn({ name:"billStatus", dataIndex:"billStatus", hidden:true, width:100})
		.addTextColumn({ name:"invoiceStatus", dataIndex:"invoiceStatus", hidden:true, width:100})
		.addTextColumn({ name:"referenceFuelTicket", dataIndex:"refTicketNo", hidden:true, width:100})
		.addTextColumn({ name:"revocationRason", dataIndex:"revocationReason", hidden:true, width:100})
		.addButtonColumn({ name:"btnHardCopy", hidden:true, width:100,  glyph:"xf0c6@FontAwesome", handler:this.__btnHandler__, _enableFn_:this.__enableFn_xxx__})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	__btnHandler__: function(btn) {
		
						var dc = btn._getController_();		
						var rec = btn.getWidgetRecord();	
						dc.setParamValue("ticketIdToHC",rec.data.id);	
						dc.fireEvent("openHardCopy");
	},
	
	__enableFn_xxx__: function(record) {
		
						return record.data.isHardCopy;
	},
	
	_badgifyColumn_: function(value) {
		
						var badgeCls = "sone-badge-default";
						if (value === __OPS_TYPE__.FuelTicketStatus._ORIGINAL_) {
							badgeCls = "sone-badge-default";
						}
						else if (value === __OPS_TYPE__.FuelTicketStatus._UPDATED_) {
							badgeCls = "sone-badge-green";
						}
						else if (value === __OPS_TYPE__.FuelTicketStatus._CANCELED_) {
							badgeCls = "sone-badge-red";
						}				
						else if (value === __OPS_TYPE__.FuelTicketStatus._REISSUE_) {
							badgeCls = "sone-badge-yellow";
						}
						var badge = "<div class='sone-badge "+badgeCls+"'>"+value+"</div>";
						return badge;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTicket_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"addRegistration", scope: this, handler: this._openAddRegistrationWdw_, text: "<i class='fa fa-plus'></i>", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } })
		.addTextField({ name:"ticketNo", bind:"{d.ticketNo}", dataIndex:"ticketNo", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:180, maxLength:32, hideLabel:"true"})
		.addNumberField({name:"upliftQuantity", bind:"{d.upliftVolume}", dataIndex:"upliftVolume", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:120, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addLov({name:"upliftUnit", bind:"{d.upliftUnitCode}", dataIndex:"upliftUnitCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "upliftUnitId"} ]})
		.addTextField({ name:"flightNo", bind:"{d.flightNo}", dataIndex:"flightNo", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:100, maxLength:4, caseRestriction:"uppercase", hideLabel:"true", minLength:0, listeners:{keydown: { scope: this, fn: function(field, e) { this._restrictNumeric_(field, e) }}}, enableKeyEvents:true})
		.addTextField({ name:"airlineDes", bind:"{d.airlineDesignator}", dataIndex:"airlineDesignator", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:180, maxLength:3, caseRestriction:"uppercase", hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"suffix", bind:"{d.suffix}", dataIndex:"suffix", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:80, store:[ __OPS_TYPE__.Suffix._D_DIVERTED_, __OPS_TYPE__.Suffix._F_FERRY_, __OPS_TYPE__.Suffix._G_GROUND_RETURN_], hideLabel:"true"})
		.addDateTimeField({name:"deliveryDate", bind:"{d.deliveryDate}", dataIndex:"deliveryDate", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:180, hideLabel:"true"})
		.addLov({name:"registration", bind:"{d.aircraftReg}", dataIndex:"aircraftReg", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:142, xtype:"fmbas_AircraftLov_Lov", maxLength:10, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "aircraftId"} ,{lovField:"acTypeId", dsField: "acTypeId"} ,{lovField:"acTypeCode", dsField: "acTypeCode"} ],
			filterFieldMapping: [{lovField:"custId", dsField: "custId"} ],listeners:{
			fpchange:{scope:this, fn:this._disableAcType_}
		}})
		.addLov({name:"acType", bind:"{d.acTypeCode}", dataIndex:"acTypeCode", _enableFn_: function(dc, rec) { return this._canEditAcType_(rec); } , width:180, xtype:"fmbas_AcTypesLov_Lov", maxLength:4, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"departure", bind:"{d.depCode}", dataIndex:"depCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:180, xtype:"fmbas_LocationsLov_Lov", maxLength:25, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "depId"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ]})
		.addLov({name:"destination", bind:"{d.destCode}", dataIndex:"destCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:180, xtype:"fmbas_LocationsLov_Lov", maxLength:25, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "destId"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ]})
		.addLov({name:"customer", bind:"{d.custCode}", dataIndex:"custCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:180, xtype:"fmbas_CustomerBlockedActiveLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "custId"} ,{lovField:"iataCode", dsField: "airlineDesignator"} ]})
		.addLov({name:"supplier", bind:"{d.suppCode}", dataIndex:"suppCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:180, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "suppId"} ],
			filterFieldMapping: [{lovField:"isSupplier", value: "true"}, {lovField:"fuelSupplier", value: "true"} ]})
		.addLov({name:"fueler", bind:"{d.fuellerCode}", dataIndex:"fuellerCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:180, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "fuellerId"} ],
			filterFieldMapping: [{lovField:"isSupplier", value: "true"}, {lovField:"iplAgent", value: "true"} ]})
		.addCombo({ xtype:"combo", name:"fuelingOper", bind:"{d.fuelingOperation}", dataIndex:"fuelingOperation", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:180, store:[ __CMM_TYPE__.FuelTicketFuelingOperation._UPLIFT_, __CMM_TYPE__.FuelTicketFuelingOperation._PRE_FUEL_, __CMM_TYPE__.FuelTicketFuelingOperation._RE_FUEL_, __CMM_TYPE__.FuelTicketFuelingOperation._DE_FUEL_, __CMM_TYPE__.FuelTicketFuelingOperation._FUEL_DUMP_, __CMM_TYPE__.FuelTicketFuelingOperation._DE_FUEL_TO_DRUM_, __CMM_TYPE__.FuelTicketFuelingOperation._FUEL_INTO_DRUM_, __CMM_TYPE__.FuelTicketFuelingOperation._FUEL_INTO_TRUCK_], hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.productType}", dataIndex:"productType", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:180, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_], hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"customsStatus", bind:"{d.customsStatus}", dataIndex:"customsStatus", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , allowBlank:false, width:180, store:[ __OPS_TYPE__.FuelTicketCustomsStatus._BONDED_, __OPS_TYPE__.FuelTicketCustomsStatus._DOMESTIC_, __OPS_TYPE__.FuelTicketCustomsStatus._FTZ_, __OPS_TYPE__.FuelTicketCustomsStatus._UNSPECIFIED_, __OPS_TYPE__.FuelTicketCustomsStatus._DUTY_PAID_, __OPS_TYPE__.FuelTicketCustomsStatus._DUTY_FREE_, __OPS_TYPE__.FuelTicketCustomsStatus._NOT_APPLICABLE_], hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"indicator", bind:"{d.indicator}", dataIndex:"indicator", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:180, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], hideLabel:"true"})
		.addLov({name:"finalDest", bind:"{d.finalDestCode}", dataIndex:"finalDestCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:180, xtype:"fmbas_LocationsLov_Lov", maxLength:25, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "finalDestId"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ]})
		.addDisplayFieldText({ name:"ticketStatus", bind:"{d.ticketStatus}", dataIndex:"ticketStatus", maxLength:16})
		.addDisplayFieldText({ name:"approvalStatus", bind:"{d.approvalStatus}", dataIndex:"approvalStatus", maxLength:32})
		.addDisplayFieldText({ name:"transmissionStatus", bind:"{d.transmissionStatus}", dataIndex:"transmissionStatus", maxLength:16})
		.addDisplayFieldText({ name:"subsidiaryCode", bind:"{d.subsidiaryCode}", dataIndex:"subsidiaryCode", maxLength:64, caseRestriction:"uppercase"})
		.addDisplayFieldText({ name:"formTitle", bind:"{d.formTitle}", dataIndex:"formTitle", noEdit:true , maxLength:32})
		.addLov({name:"reseller", bind:"{d.resellerCode}", dataIndex:"resellerCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:180, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "resellerId"} ],
			filterFieldMapping: [{lovField:"natureOfBusiness", value: "Reseller"} ]})
		.addDisplayFieldText({ name:"ticketNoLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"deliveryDateLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row1Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"productLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelingOperLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"registrationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"acTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row2Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"airlineDesLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"indicatorLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"departureLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"destinationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"finalDestLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"customsStatusLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"customerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"supplierLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"resellerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"refTicketNo", bind:"{d.refTicketNo}", dataIndex:"refTicketNo", _visibleFn_: function(dc, rec) { return !Ext.isEmpty(dc.getRecord().get('refTicketNo')); } , maxLength:32, style:"cursor: pointer", listeners:{afterrender: {scope: this, fn: function(el) {this._openRefTicket_(el)}}}})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("upliftQuantity"),this._getConfig_("upliftUnit")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("flightNo"),this._getConfig_("suffix")]})
		.add({name:"row7", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("formTitle")]})
		.add({name:"registrationAndBtn", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("registration"),this._getConfig_("addRegistration")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"mainTable", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"p3",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c5", width:300, defaults: { labelAlign:"top", cls:"sone-highlight-kpi"}, layout:"anchor"})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "mainTable"])
		.addChildrenTo("mainTable", ["table1", "table2", "table3", "table4"])
		.addChildrenTo("table1", ["ticketNoLabel", "ticketNo", "deliveryDateLabel", "deliveryDate", "row1Label", "row1", "productLabel", "product", "fuelingOperLabel", "fuelingOper"])
		.addChildrenTo("table2", ["registrationLabel", "registrationAndBtn", "acTypeLabel", "acType", "row2Label", "row2", "airlineDesLabel", "airlineDes", "indicatorLabel", "indicator"])
		.addChildrenTo("table3", ["departureLabel", "departure", "destinationLabel", "destination", "finalDestLabel", "finalDest", "customsStatusLabel", "customsStatus"])
		.addChildrenTo("table4", ["customerLabel", "customer", "fuelerLabel", "fueler", "supplierLabel", "supplier", "resellerLabel", "reseller"])
		.addChildrenTo("p3", ["c1", "c2", "c3", "c4", "c5"])
		.addChildrenTo("titleAndKpi", ["title", "p3"])
		.addChildrenTo("c1", ["ticketStatus"])
		.addChildrenTo("c2", ["approvalStatus"])
		.addChildrenTo("c3", ["transmissionStatus"])
		.addChildrenTo("c4", ["subsidiaryCode"])
		.addChildrenTo("c5", ["refTicketNo"])
		.addChildrenTo("title", ["row7"]);
	},
	/* ==================== Business functions ==================== */
	
	_restrictNumeric_: function(field,e) {
		
						var code = e.browserEvent.keyCode;
						var keyCodes = [8,9,39,37,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105];
				        if (keyCodes.indexOf(code) < 0) {
				            e.stopEvent();
				        }
	},
	
	_openRefTicket_: function(el) {
		
		
						var bundle = "atraxo.mod.ops";
						var frame = "atraxo.ops.ui.extjs.frame.FuelTicket_Ui";
		
						el.getEl().on("click", function() { 
		
							getApplication().showFrame(frame,{
								url:Main.buildUiPath(bundle, frame, false),
								params: {
									refFuelTicket: el.getValue()
								},
								callback: function (params) {
									this._onShow_refFuleTicket_(params)
								}
							});
		
					    });
	},
	
	_disableAcType_: function(el) {
		
						if (el._fpRawValue_ !== el._onFocusVal_) {
							var acType = this._get_("acType");
							if (!Ext.isEmpty(el.getValue())) {
								acType._disable_();
							}
							else {
								acType._enable_();
							}
						}
	},
	
	_openAddRegistrationWdw_: function() {
		
						// wdwNewAircraftRegistration
						var dc = this._controller_;
						var frame = dc.getFrame();
						var aircraftDc = frame._getDc_("aircraft");
						var w = frame._getWindow_("wdwNewAircraftRegistration");
						w.show(undefined, function() {
							aircraftDc.doNew({setValidFromValidTo: true});
						}, this);
	},
	
	_afterDefineElements_: function() {
		
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
						
						this._controller_.on("afterDoServiceSuccess", function() {					
							this._applyStates_(this._controller_.getRecord());
						},this);
	},
	
	_afterApplyStates_: function() {
		 
						var ticketNoValue = "";
						var r = this._controller_.getRecord();
						if (r) {
							ticketNoValue = r.get("ticketNo");
						}
						var formTitle = this._get_("formTitle");				
						var originalTitleLabel = formTitle.fieldLabel;
						formTitle.labelEl.update(originalTitleLabel+" #"+ticketNoValue);
						
	},
	
	_canEdit_: function(record) {
		
						return record!== null && (record.data.approvalStatus!==__TYPES__.fuelTicket.approvalStatus.ok)
						&& !(record.data.ticketStatus===__TYPES__.fuelTicket.ticketStatus.canceled || record.data.transmissionStatus===__TYPES__.fuelTicket.transmissionStatus.exported)
						&& record.data.invoiceStatus !== __TYPES__.fuelTicket.invoiceStatus.awaitingPayment && record.data.invoiceStatus !== __TYPES__.fuelTicket.invoiceStatus.paid && record.data.billStatus !== __TYPES__.fuelTicket.invoiceStatus.paid && record.data.billStatus !== __TYPES__.fuelTicket.invoiceStatus.awaitingPayment
						&& record.data.approvalStatus !== __OPS_TYPE__.FuelTicketApprovalStatus._AWAITING_APPROVAL_;
	},
	
	_canEditAcType_: function(record) {
		
						var reg = this._controller_.getRecord().get("aircraftReg");
						if(Ext.isEmpty(reg)){
							return record!== null && (record.data.approvalStatus!==__TYPES__.fuelTicket.approvalStatus.ok)
							&& !(record.data.ticketStatus===__TYPES__.fuelTicket.ticketStatus.canceled || record.data.transmissionStatus===__TYPES__.fuelTicket.transmissionStatus.exported)
							&& record.data.invoiceStatus !== __TYPES__.fuelTicket.invoiceStatus.awaitingPayment && record.data.invoiceStatus !== __TYPES__.fuelTicket.invoiceStatus.paid && record.data.billStatus !== __TYPES__.fuelTicket.invoiceStatus.paid && record.data.billStatus !== __TYPES__.fuelTicket.invoiceStatus.awaitingPayment;
						} else { 
							return false 
						}
	}
});

/* ================= EDIT FORM: Tab ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$Tab", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTicket_Dc$Tab",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"transport", bind:"{d.transport}", dataIndex:"transport", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:180, store:[ __CMM_TYPE__.FuelingType._RE_FUELER_, __CMM_TYPE__.FuelingType._HYDRANT_], hideLabel:"true"})
		.addNumberField({name:"netQuantity", bind:"{d.netUpliftQuantity}", dataIndex:"netUpliftQuantity", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:120, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addLov({name:"netQuantityUnit", bind:"{d.netUpliftUnitCode}", dataIndex:"netUpliftUnitCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "netUpliftUnitId"} ]})
		.addNumberField({name:"beforeFueling", bind:"{d.beforeFueling}", dataIndex:"beforeFueling", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:120, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addLov({name:"beforeFuelingUnit", bind:"{d.beforeFuelingUnitCode}", dataIndex:"beforeFuelingUnitCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "beforeFuelingUnitId"} ]})
		.addNumberField({name:"afterFueling", bind:"{d.afterFueling}", dataIndex:"afterFueling", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:120, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addLov({name:"afterFuelingUnit", bind:"{d.afterFuelingUnitCode}", dataIndex:"afterFuelingUnitCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "afterFuelingUnitId"} ]})
		.addNumberField({name:"density", bind:"{d.density}", dataIndex:"density", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:60, sysDec:"dec_prc", maxLength:19, hideLabel:"true"})
		.addLov({name:"densityUnit", bind:"{d.densityUnitCode}", dataIndex:"densityUnitCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:60, xtype:"fmbas_UnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "densityUnitId"} ],
			filterFieldMapping: [{lovField:"unitTypeInd", value: "Mass"} ]})
		.addLov({name:"densityVolume", bind:"{d.densityVolumeCode}", dataIndex:"densityVolumeCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:60, xtype:"fmbas_UnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "densityVolumeId"} ],
			filterFieldMapping: [{lovField:"unitTypeInd", value: "Volume"} ]})
		.addDateTimeField({name:"fuelingStart", bind:"{d.fuelingStartDate}", dataIndex:"fuelingStartDate", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:180, hideLabel:"true"})
		.addDateTimeField({name:"fuelingEnd", bind:"{d.fuelingEndDate}", dataIndex:"fuelingEndDate", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:180, hideLabel:"true"})
		.addNumberField({name:"meterStartIndex", bind:"{d.meterStartIndex}", dataIndex:"meterStartIndex", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:120, maxLength:11, hideLabel:"true"})
		.addNumberField({name:"meterEndIndex", bind:"{d.meterEndIndex}", dataIndex:"meterEndIndex", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:120, maxLength:11, hideLabel:"true"})
		.addNumberField({name:"temperature", bind:"{d.temperature}", dataIndex:"temperature", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:120, maxLength:11, hideLabel:"true",listeners:{
			fpchange:{scope:this, fn:this._validateTemperature_}
		}})
		.addCombo({ xtype:"combo", name:"temperatureUnit", bind:"{d.temperatureUnit}", dataIndex:"temperatureUnit", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:60, store:[ __OPS_TYPE__.TemperatureUnit._C_, __OPS_TYPE__.TemperatureUnit._F_], hideLabel:"true",listeners:{
			fpchange:{scope:this, fn:this._resetTemperature_}
		}})
		.addDateTimeField({name:"refuelerArrivalTime", bind:"{d.refuelerArrivalTime}", dataIndex:"refuelerArrivalTime", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:180, hideLabel:"true"})
		.addDisplayFieldText({ name:"transportLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"refuelerArrivalLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelingStartLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelingEndLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row3Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row4Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row5Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"meterStartIndexLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"meterEndIndexLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row6Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row8Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("netQuantity"),this._getConfig_("netQuantityUnit")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("beforeFueling"),this._getConfig_("beforeFuelingUnit")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("afterFueling"),this._getConfig_("afterFuelingUnit")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("density"),this._getConfig_("densityUnit"),this._getConfig_("densityVolume")]})
		.add({name:"row8", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("temperature"),this._getConfig_("temperatureUnit")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"mainTable", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table4",  style:"margin-left:50px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["mainTable"])
		.addChildrenTo("mainTable", ["table1", "table2", "table3", "table4"])
		.addChildrenTo("table1", ["transportLabel", "transport", "refuelerArrivalLabel", "refuelerArrivalTime", "fuelingStartLabel", "fuelingStart", "fuelingEndLabel", "fuelingEnd"])
		.addChildrenTo("table2", ["row3Label", "row3", "row4Label", "row4", "row5Label", "row5"])
		.addChildrenTo("table3", ["meterStartIndexLabel", "meterStartIndex", "meterEndIndexLabel", "meterEndIndex"])
		.addChildrenTo("table4", ["row6Label", "row6", "row8Label", "row8"]);
	},
	/* ==================== Business functions ==================== */
	
	_resetTemperature_: function() {
		
						var el = this._get_("temperature");
						el.setValue("");
	},
	
	_validateTemperature_: function() {
		
						var el = this._get_("temperature");
						var newVal = el.getValue();
						var tempUnitVal = this._get_("temperatureUnit").getValue();
						if (tempUnitVal == "C") {
							if (newVal < -47 || newVal > 50) {
								Main.warning("Value is out of range! The value must be within -47 and 50");
							    el.setValue("");
							}
						}
						else if (tempUnitVal == "F") {
							if (newVal < -53 || newVal > 122) {
								Main.warning("Value is out of range! The value must be within -53 and 122");
							    el.setValue("");
							}
						}
	},
	
	_canEdit_: function(record) {
		
						return record!= null && (record.data.approvalStatus!=__TYPES__.fuelTicket.approvalStatus.ok)
						&& !(record.data.ticketStatus==__TYPES__.fuelTicket.ticketStatus.canceled || record.data.transmissionStatus==__TYPES__.fuelTicket.transmissionStatus.exported)
						&& record.data.invoiceStatus != __TYPES__.fuelTicket.invoiceStatus.awaitingPayment && record.data.invoiceStatus != __TYPES__.fuelTicket.invoiceStatus.paid && record.data.billStatus != __TYPES__.fuelTicket.invoiceStatus.paid && record.data.billStatus != __TYPES__.fuelTicket.invoiceStatus.awaitingPayment;
	}
});

/* ================= EDIT FORM: TabPayment ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$TabPayment", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTicket_Dc$TabPayment",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addCombo({ xtype:"combo", name:"paymentType", bind:"{d.paymentType}", dataIndex:"paymentType", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:150, store:[ __CMM_TYPE__.PaymentType._CONTRACT_, __CMM_TYPE__.PaymentType._FUEL_CARD_, __CMM_TYPE__.PaymentType._CREDIT_CARD_, __CMM_TYPE__.PaymentType._CASH_], hideLabel:"true"})
		.addTextField({ name:"cardNumber", bind:"{d.cardNumber}", dataIndex:"cardNumber", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:150, hideLabel:"true", maxLength:20})
		.addDateField({name:"cardExp", bind:"{d.cardExpiringDate}", dataIndex:"cardExpiringDate", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:160, hideLabel:"true"})
		.addTextField({ name:"cardHolder", bind:"{d.cardHolder}", dataIndex:"cardHolder", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:160, maxLength:64, hideLabel:"true"})
		.addNumberField({name:"amountReceived", bind:"{d.amountReceived}", dataIndex:"amountReceived", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:120, sysDec:"dec_crncy", maxLength:19, hideLabel:"true"})
		.addLov({name:"amountCurr", bind:"{d.amountCurrCode}", dataIndex:"amountCurrCode", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:60, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "amountCurrId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addCombo({ xtype:"combo", name:"sendInvoiceBy", bind:"{d.sendInvoiceBy}", dataIndex:"sendInvoiceBy", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:80, store:[ __OPS_TYPE__.SendInvoiceBy._E_MAIL_, __OPS_TYPE__.SendInvoiceBy._FAX_], hideLabel:"true"})
		.addTextField({ name:"sendInvoiceTo", bind:"{d.sendInvoiceTo}", dataIndex:"sendInvoiceTo", _enableFn_: function(dc, rec) { return this._canEdit_(rec); } , width:100, maxLength:64, hideLabel:"true"})
		.addDisplayFieldText({ name:"paymentTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"cardNumberLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"cardExpLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"cardHolderLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row1Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row2Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("amountReceived"),this._getConfig_("amountCurr")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("sendInvoiceBy"),this._getConfig_("sendInvoiceTo")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"mainTable", layout: {type:"table"}})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:50px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["mainTable"])
		.addChildrenTo("mainTable", ["table1", "table2", "table3"])
		.addChildrenTo("table1", ["paymentTypeLabel", "paymentType", "cardNumberLabel", "cardNumber"])
		.addChildrenTo("table2", ["cardExpLabel", "cardExp", "cardHolderLabel", "cardHolder"])
		.addChildrenTo("table3", ["row1Label", "row1", "row2Label", "row2"]);
	},
	/* ==================== Business functions ==================== */
	
	_canEdit_: function(record) {
		
						return record!= null && (record.data.approvalStatus!=__TYPES__.fuelTicket.approvalStatus.ok)
						&& !(record.data.ticketStatus==__TYPES__.fuelTicket.ticketStatus.canceled || record.data.transmissionStatus==__TYPES__.fuelTicket.transmissionStatus.exported)
						&& record.data.invoiceStatus != __TYPES__.fuelTicket.invoiceStatus.awaitingPayment && record.data.invoiceStatus != __TYPES__.fuelTicket.invoiceStatus.paid && record.data.billStatus != __TYPES__.fuelTicket.invoiceStatus.paid && record.data.billStatus != __TYPES__.fuelTicket.invoiceStatus.awaitingPayment;
	}
});

/* ================= EDIT FORM: PopUp ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$PopUp", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTicket_Dc$PopUp",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"addRegistration", scope: this, handler: this._openAddRegistrationWdw_, text: "<i class='fa fa-plus'></i>", noEdit:true })
		.addTextField({ name:"ticketNo", bind:"{d.ticketNo}", dataIndex:"ticketNo", allowBlank:false, width:180, maxLength:32, hideLabel:"true"})
		.addNumberField({name:"upliftQuantity", bind:"{d.upliftVolume}", dataIndex:"upliftVolume", allowBlank:false, width:100, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addLov({name:"upliftUnit", bind:"{d.upliftUnitCode}", dataIndex:"upliftUnitCode", allowBlank:false, width:80, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "upliftUnitId"} ]})
		.addNumberField({name:"netUpliftQuantity", bind:"{d.netUpliftQuantity}", dataIndex:"netUpliftQuantity", width:100, sysDec:"dec_unit", maxLength:19, hideLabel:"true"})
		.addLov({name:"netUpliftUnit", bind:"{d.netUpliftUnitCode}", dataIndex:"netUpliftUnitCode", width:80, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "netUpliftUnitId"} ]})
		.addTextField({ name:"flightNo", bind:"{d.flightNo}", dataIndex:"flightNo", allowBlank:false, width:100, caseRestriction:"uppercase", hideLabel:"true", minLength:0, maxLength:4, listeners:{keydown: { scope: this, fn: function(field, e) { this._restrictNumeric_(field, e) }}}, enableKeyEvents:true})
		.addTextField({ name:"airlineDes", bind:"{d.airlineDesignator}", dataIndex:"airlineDesignator", width:180, maxLength:3, caseRestriction:"uppercase", hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"suffix", bind:"{d.suffix}", dataIndex:"suffix", width:80, store:[ __OPS_TYPE__.Suffix._D_DIVERTED_, __OPS_TYPE__.Suffix._F_FERRY_, __OPS_TYPE__.Suffix._G_GROUND_RETURN_], hideLabel:"true"})
		.addDateTimeField({name:"deliveryDate", bind:"{d.deliveryDate}", dataIndex:"deliveryDate", allowBlank:false, width:180, hideLabel:"true"})
		.addLov({name:"registration", bind:"{d.aircraftReg}", dataIndex:"aircraftReg", noEdit:true , allowBlank:false, width:142, xtype:"fmbas_AircraftLov_Lov", maxLength:10, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "aircraftId"} ,{lovField:"acTypeId", dsField: "acTypeId"} ,{lovField:"acTypeCode", dsField: "acTypeCode"} ],
			filterFieldMapping: [{lovField:"custCode", dsField: "custCode"} ],listeners:{
			fpchange:{scope:this, fn:this._disableAcType_}
		}})
		.addLov({name:"acType", bind:"{d.acTypeCode}", dataIndex:"acTypeCode", width:180, xtype:"fmbas_AcTypesLov_Lov", maxLength:4, hideLabel:"true", readOnly:true,
			retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addLov({name:"departure", bind:"{d.depCode}", dataIndex:"depCode", allowBlank:false, width:180, xtype:"fmbas_LocationsLov_Lov", maxLength:25, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "depId"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ]})
		.addLov({name:"destination", bind:"{d.destCode}", dataIndex:"destCode", width:180, xtype:"fmbas_LocationsLov_Lov", maxLength:25, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "destId"} ],
			filterFieldMapping: [{lovField:"isAirport", value: "true"} ]})
		.addLov({name:"customer", bind:"{d.custCode}", dataIndex:"custCode", allowBlank:false, width:180, xtype:"fmbas_CustomerBlockedActiveLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "custId"} ,{lovField:"iataCode", dsField: "airlineDesignator"} ,{lovField:"name", dsField: "custName"} ],listeners:{
			fpchange:{scope:this, fn:this._resetRegistration_}
		}})
		.addLov({name:"supplier", bind:"{d.suppCode}", dataIndex:"suppCode", allowBlank:false, width:180, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "suppId"} ],
			filterFieldMapping: [{lovField:"isSupplier", value: "true"}, {lovField:"fuelSupplier", value: "true"} ]})
		.addLov({name:"fueler", bind:"{d.fuellerCode}", dataIndex:"fuellerCode", allowBlank:false, width:180, xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "fuellerId"} ],
			filterFieldMapping: [{lovField:"isSupplier", value: "true"}, {lovField:"iplAgent", value: "true"} ]})
		.addCombo({ xtype:"combo", name:"fuelingOper", bind:"{d.fuelingOperation}", dataIndex:"fuelingOperation", allowBlank:false, width:180, store:[ __CMM_TYPE__.FuelTicketFuelingOperation._UPLIFT_, __CMM_TYPE__.FuelTicketFuelingOperation._PRE_FUEL_, __CMM_TYPE__.FuelTicketFuelingOperation._RE_FUEL_, __CMM_TYPE__.FuelTicketFuelingOperation._DE_FUEL_, __CMM_TYPE__.FuelTicketFuelingOperation._FUEL_DUMP_, __CMM_TYPE__.FuelTicketFuelingOperation._DE_FUEL_TO_DRUM_, __CMM_TYPE__.FuelTicketFuelingOperation._FUEL_INTO_DRUM_, __CMM_TYPE__.FuelTicketFuelingOperation._FUEL_INTO_TRUCK_], hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.productType}", dataIndex:"productType", allowBlank:false, width:180, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_], hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"customsStatus", bind:"{d.customsStatus}", dataIndex:"customsStatus", allowBlank:false, width:180, store:[ __OPS_TYPE__.FuelTicketCustomsStatus._BONDED_, __OPS_TYPE__.FuelTicketCustomsStatus._DOMESTIC_, __OPS_TYPE__.FuelTicketCustomsStatus._FTZ_, __OPS_TYPE__.FuelTicketCustomsStatus._UNSPECIFIED_, __OPS_TYPE__.FuelTicketCustomsStatus._DUTY_PAID_, __OPS_TYPE__.FuelTicketCustomsStatus._DUTY_FREE_, __OPS_TYPE__.FuelTicketCustomsStatus._NOT_APPLICABLE_], hideLabel:"true"})
		.addCombo({ xtype:"combo", name:"indicator", bind:"{d.indicator}", dataIndex:"indicator", width:180, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], hideLabel:"true"})
		.addLov({name:"reseller", bind:"{d.resellerCode}", dataIndex:"resellerCode", width:180, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "resellerId"} ],
			filterFieldMapping: [{lovField:"natureOfBusiness", value: "Reseller"} ]})
		.addNumberField({name:"density", bind:"{d.density}", dataIndex:"density", width:60, sysDec:"dec_prc", maxLength:19, hideLabel:"true"})
		.addLov({name:"densityUnit", bind:"{d.densityUnitCode}", dataIndex:"densityUnitCode", width:60, xtype:"fmbas_UnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "densityUnitId"} ],
			filterFieldMapping: [{lovField:"unitTypeInd", value: "Mass"} ]})
		.addLov({name:"densityVolume", bind:"{d.densityVolumeCode}", dataIndex:"densityVolumeCode", width:60, xtype:"fmbas_UnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "densityVolumeId"} ],
			filterFieldMapping: [{lovField:"unitTypeInd", value: "Volume"} ]})
		.addNumberField({name:"temperature", bind:"{d.temperature}", dataIndex:"temperature", width:120, maxLength:11, hideLabel:"true",listeners:{
			fpchange:{scope:this, fn:this._validateTemperature_}
		}})
		.addCombo({ xtype:"combo", name:"temperatureUnit", bind:"{d.temperatureUnit}", dataIndex:"temperatureUnit", width:60, store:[ __OPS_TYPE__.TemperatureUnit._C_, __OPS_TYPE__.TemperatureUnit._F_], hideLabel:"true",listeners:{
			fpchange:{scope:this, fn:this._resetTemperature_}
		}})
		.addDisplayFieldText({ name:"customerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"ticketNoLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"deliveryDateLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row1Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"productLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelingOperLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"departureLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"destinationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"registrationLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"acTypeLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row2Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"airlineDesLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelerLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"supplierLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"fuelReleaseLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"indicatorLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"customsStatusLabel", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row3Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row4Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.addDisplayFieldText({ name:"row5Label", bind:"{d.labelField}", dataIndex:"labelField", maxLength:32, style:"float:right"})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("upliftQuantity"),this._getConfig_("upliftUnit")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("flightNo"),this._getConfig_("suffix")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("density"),this._getConfig_("densityUnit"),this._getConfig_("densityVolume")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("temperature"),this._getConfig_("temperatureUnit")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("netUpliftQuantity"),this._getConfig_("netUpliftUnit")]})
		.add({name:"registrationAndBtn", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("registration"),this._getConfig_("addRegistration")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"table"}, padding:"0 30 5 0"})
		.addPanel({ name:"table1", layout: {type:"table", columns:2}})
		.addPanel({ name:"table2",  style:"margin-left:50px", layout: {type:"table", columns:2}})
		.addPanel({ name:"table3",  style:"margin-left:50px", layout: {type:"table", columns:2}});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["table1", "table2", "table3"])
		.addChildrenTo("table1", ["customerLabel", "customer", "ticketNoLabel", "ticketNo", "deliveryDateLabel", "deliveryDate", "row1Label", "row1", "row5Label", "row5", "productLabel", "product", "fuelingOperLabel", "fuelingOper"])
		.addChildrenTo("table2", ["departureLabel", "departure", "destinationLabel", "destination", "registrationLabel", "registrationAndBtn", "acTypeLabel", "acType", "row2Label", "row2", "airlineDesLabel", "airlineDes"])
		.addChildrenTo("table3", ["fuelerLabel", "fueler", "supplierLabel", "supplier", "fuelReleaseLabel", "reseller", "indicatorLabel", "indicator", "customsStatusLabel", "customsStatus", "row3Label", "row3", "row4Label", "row4"]);
	},
	/* ==================== Business functions ==================== */
	
	_restrictNumeric_: function(field,e) {
		
						var code = e.browserEvent.keyCode;
						var keyCodes = [8,9,39,37,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105];
				        if (keyCodes.indexOf(code) < 0) {
				            e.stopEvent();
				        }
	},
	
	_resetTemperature_: function() {
		
						var el = this._get_("temperature");
						el.setValue("");
	},
	
	_validateTemperature_: function() {
		
						var el = this._get_("temperature");
						var newVal = el.getValue();
						var tempUnitVal = this._get_("temperatureUnit").getValue();
						if (tempUnitVal === "C") {
							if (newVal < -47 || newVal > 50) {
								Main.warning("Value is out of range! The value must be within -47 and 50");
							    el.setValue("");
							}
						}
						else if (tempUnitVal === "F") {
							if (newVal < -53 || newVal > 122) {
								Main.warning("Value is out of range! The value must be within -53 and 122");
							    el.setValue("");
							}
						}
	},
	
	_resetRegistration_: function(el,newVal,oldVal) {
		
						var btn = this._get_("addRegistration");
						var registration = this._get_("registration");
		
						registration.setValue("");
		
						if (!Ext.isEmpty(el.getValue())) {
							btn._enable_();
							registration._enable_();
						}
						else {
							btn._disable_();
							registration._disable_();
						}
	},
	
	_disableAcType_: function(el,newVal,oldVal) {
		
						if (el._fpRawValue_ != el._onFocusVal_) {
							var acType = this._get_("acType");
							if (!Ext.isEmpty(el.getValue())) {
								acType._disable_();
							}
							else {
								acType._enable_();
							}
						}
	},
	
	_openAddRegistrationWdw_: function() {
		
						// wdwNewAircraftRegistration
						var dc = this._controller_;
						var frame = dc.getFrame();
						var aircraftDc = frame._getDc_("aircraft");
						var w = frame._getWindow_("wdwNewAircraftRegistration");
						w.show(undefined, function() {
							aircraftDc.doNew({setValidFromValidTo: true});
						}, this);
	}
});

/* ================= EDIT FORM: EditRemark ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$EditRemark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTicket_Dc$EditRemark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remark}", paramIndex:"remark", width:400, labelAlign:"top", fieldStyle:"margin-top: 5px"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:560})
		.addPanel({ name:"col1", width:560, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						//remarks.allowBlank=true;		
	}
});

/* ================= EDIT FORM: RevokeRemark ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$RevokeRemark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTicket_Dc$RevokeRemark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLabel({ name:"cancelWindowText", width:400})
		.addLabel({ name:"emptyLine", width:200})
		.addCombo({ xtype:"combo", name:"revocationReason", bind:"{d.revocationReason}", dataIndex:"revocationReason", allowBlank:false, width:400, store:[ __OPS_TYPE__.RevocationReason._INCORRECT_CUSTOMER_INFORMATION_, __OPS_TYPE__.RevocationReason._INCORRECT_FLIGHT_AIRCRAFT_INFORMATION_, __OPS_TYPE__.RevocationReason._INCORRECT_VOLUME_, __OPS_TYPE__.RevocationReason._INCORRECT_PRICE___DISCOUNT___TAX___FREIGHT_, __OPS_TYPE__.RevocationReason._COMMERCIAL_REASON_], labelAlign:"top", fieldStyle:"margin-top: 5px",listeners:{
			fpchange:{scope:this, fn:this._setReason_}
		}})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:560})
		.addPanel({ name:"col1", width:560, layout:"anchor"})
		.addPanel({ name:"col2", width:560, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["cancelWindowText"])
		.addChildrenTo("col2", ["emptyLine", "revocationReason"]);
	},
	/* ==================== Business functions ==================== */
	
	_setReason_: function(el) {
		
						var v = el.getValue();
						var ctrl = this._controller_;
						ctrl.setParamValue("remark",v);
						ctrl.fireEvent("refreshButtonState");
	},
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("revocationReason");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						//remarks.allowBlank=true;		
	}
});

/* ================= EDIT FORM: NewFuelTicketNumber ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$NewFuelTicketNumber", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTicket_Dc$NewFuelTicketNumber",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"newFuelTicketNumber", bind:"{p.newFuelTicketNumber}", paramIndex:"newFuelTicketNumber", width:230, maxLength:255, labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:235})
		.addPanel({ name:"col1", width:235, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["newFuelTicketNumber"]);
	}
});

/* ================= EDIT FORM: ApprovalNote ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTicket_Dc$ApprovalNote", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelTicket_Dc$ApprovalNote",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"approvalNote", bind:"{p.approvalNote}", paramIndex:"approvalNote", width:580, labelAlign:"top", labelStyle:"font-weight:bold", fieldStyle:"margin-top: 5px;"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:580})
		.addPanel({ name:"col1", width:580, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["approvalNote"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var approvalNote = this._get_("approvalNote");
						approvalNote.setRawValue("");
	}
});
