/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelOrder_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ops.ui.extjs.ds.FuelOrder_DsParam,
	recordModel: atraxo.ops.ui.extjs.ds.FuelOrder_Ds,
		
			/* ================ Business functions ================ */
	
	canDoDelete: function() {
		
					var selRecords = this.selectedRecords;
		
					for (var i = 0; i < selRecords.length; i++) {
		               if (selRecords[i].data.status == __TYPES__.fuelOrder.status.canceled 
							|| selRecords[i].data.status == __TYPES__.fuelOrder.status.rejected) {
						  return true;
					   }
					}
					return  false;
		
	},
	
	isEnable: function() {
		
					var selRecords = this._controller_.selectedRecords;
		
					for (var i = 0; i < selRecords.length; i++) {
		               if (selRecords[i].data.status == __TYPES__.fuelOrder.status.canceled 
							|| selRecords[i].data.status == __TYPES__.fuelOrder.status.confirmed
							|| selRecords[i].data.status == __TYPES__.fuelOrder.status.released) {
						  return false;
					   }
					}
					return true;
	}

});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FuelOrder_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FuelOrder_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"customerCode", dataIndex:"customerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addDateField({name:"postedOn", dataIndex:"postedOn", labelWidth:120})
			.addTextField({ name:"customerReferenceNo", dataIndex:"customerReferenceNo", maxLength:32, labelWidth:120})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __OPS_TYPE__.FuelOrderStatus._NEW_, __OPS_TYPE__.FuelOrderStatus._REJECTED_, __OPS_TYPE__.FuelOrderStatus._CONFIRMED_, __OPS_TYPE__.FuelOrderStatus._CANCELED_, __OPS_TYPE__.FuelOrderStatus._RELEASED_]})
			.addLov({name:"registeredBy", dataIndex:"userName", maxLength:255,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserNameLov_Lov", selectOnFocus:true, maxLength:255,
					retFieldMapping: [{lovField:"id", dsField: "userId"} ]}})
			.addLov({name:"subsidiaryCode", dataIndex:"subsidiaryCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserSubsidiaryLov_Lov", selectOnFocus:true, maxLength:32}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrder_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelOrder_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"customer", dataIndex:"customerCode", width:100})
		.addDateColumn({ name:"postedOn", dataIndex:"postedOn", width:100, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"fuelQuoteCode", dataIndex:"fuelQuoteCode", width:100})
		.addTextColumn({ name:"customerReferenceNo", dataIndex:"customerReferenceNo", width:155})
		.addTextColumn({ name:"status", dataIndex:"status", width:100})
		.addTextColumn({ name:"registeredBy", dataIndex:"userName", width:130})
		.addTextColumn({ name:"postedBy", dataIndex:"contactName", hidden:true, width:100})
		.addBooleanColumn({ name:"openRelease", dataIndex:"openRelease", hidden:true, width:100})
		.addDateColumn({ name:"fuelQuoteIssueDate", dataIndex:"fuelQuoteIssueDate", hidden:true, width:100, _mask_: Masks.DATETIME})
		.addDateColumn({ name:"validUntil", dataIndex:"fuelQuoteValidUntil", hidden:true, width:100, _mask_: Masks.DATE})
		.addTextColumn({ name:"creditTerms", dataIndex:"creditTerms", hidden:true, width:100})
		.addNumberColumn({ name:"creditLimit", dataIndex:"creditLimit", hidden:true, width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"creditCurrency", dataIndex:"creditCurrency", hidden:true, width:100})
		.addBooleanColumn({ name:"creditCard", dataIndex:"creditCard", hidden:true, width:100})
		.addNumberColumn({ name:"exposure", dataIndex:"exposure", sysDec:"dec_prc",  renderer:function(val, meta, record, rowIndex) { return this._markUtilization_(val, meta, record, rowIndex); }})
		.addTextColumn({ name:"fuelOrderCode", dataIndex:"code", hidden:true, width:100})
		.addTextColumn({ name:"contractType", dataIndex:"contractType", hidden:true, width:100})
		.addTextColumn({ name:"creditTerm", dataIndex:"creditTerms", hidden:true, width:100})
		.addNumberColumn({ name:"alertLimit", dataIndex:"alertLimit", hidden:true, width:80})
		.addTextColumn({ name:"subsidiaryCode", dataIndex:"subsidiaryCode", hidden:true, width:120})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_markUtilization_: function(value,meta,record,rowIndex) {
		
		
						var globalStyles = "border:0px !important";
						var redStyles = "background: #F94F52; color: #FFFFFF;"+globalStyles;
						var yellowStyles = "background: #F9B344; color: #FFFFFF;"+globalStyles;
						var greenStyles = "background: #52AA39; color: #FFFFFF;"+globalStyles;
		
						var alertLimit = record.get("alertLimit");
		
						if (!Ext.isEmpty(value)) {
							if(value <= alertLimit) {
					        	meta.style = greenStyles;
						    } else if (value > alertLimit && value <= 100) {
						        meta.style = yellowStyles;
						    } else if (value > alertLimit && value >= 100) {
						        meta.style = redStyles;
						    }
							return value +" %"
						}
						
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrder_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelOrder_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", allowBlank:false, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32, labelWidth:135,
			retFieldMapping: [{lovField:"id", dsField: "customerId"} ,{lovField:"id", dsField: "contactObjectId"} ,{lovField:"status", dsField: "customerStatus"} ,{lovField:"primaryContactId", dsField: "contactId"} ,{lovField:"primaryContactName", dsField: "contactName"} ],listeners:{
			fpchange:{scope:this, fn:this._setQuoteLovState_},
			select:{scope:this, fn:this._clearQuote_}
		}})
		.addLov({name:"fuelQuoteCode", bind:"{d.fuelQuoteCode}", dataIndex:"fuelQuoteCode", allowBlank:false, xtype:"ops_FuelQuoteLov_Lov", maxLength:32, labelWidth:135,
			retFieldMapping: [{lovField:"id", dsField: "fuelQuoteId"} ,{lovField:"subsidiaryId", dsField: "subsidiaryId"} ],
			filterFieldMapping: [{lovField:"customerCode", dsField: "customerCode"}, {lovField:"status", value: "Submitted"} ],listeners:{
			blur:{scope:this, fn:this._checkValidQuote_},
			focus:{scope:this, fn:this._checkValidQuote_}
		}})
		.addDateField({name:"postedOn", bind:"{d.postedOn}", dataIndex:"postedOn", labelWidth:135})
		.addLov({name:"contactName", bind:"{d.contactName}", dataIndex:"contactName", xtype:"fmbas_ContactsLov_Lov", maxLength:255, labelWidth:135,
			retFieldMapping: [{lovField:"id", dsField: "contactId"} ],
			filterFieldMapping: [{lovField:"objectId", dsField: "contactId"}, {lovField:"objectType", value: "Customers"} ]})
		.addTextField({ name:"customerReferenceNo", bind:"{d.customerReferenceNo}", dataIndex:"customerReferenceNo", maxLength:32, labelWidth:135})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["customer", "fuelQuoteCode", "postedOn", "contactName", "customerReferenceNo"]);
	},
	/* ==================== Business functions ==================== */
	
	_showErrorAndResetField_: function(field,msg) {
		
						Main.error(msg);
						field.setValue("");
						return false;
	},
	
	_clearQuote_: function() {
		
						var fuelQuoteCode = this._get_("fuelQuoteCode");
						fuelQuoteCode.setValue("");
	},
	
	_checkValidQuote_: function() {
		
		
						var fuelQuoteCode = this._get_("fuelQuoteCode");
		
						if (!Ext.isEmpty(fuelQuoteCode.getValue())) {
							var items = fuelQuoteCode.store.data.items;
							
							if (items.length < 1) {
								this._showErrorAndResetField_(fuelQuoteCode, "The selected fuel quote is not valid!");
							}
						}
		
	},
	
	_setQuoteLovState_: function() {
		
		
						var customerCode = this._get_("customer");
						var quoteLov = this._get_("fuelQuoteCode");
						var contactName = this._get_("contactName");
		
						Ext.suspendLayouts();
		
		
						if (Ext.isEmpty(customerCode.getValue())) {
							quoteLov.setReadOnly(true);
							contactName.setReadOnly(true);
						}
						else {
							quoteLov.setReadOnly(false);
							contactName.setReadOnly(false);
						}
						Ext.resumeLayouts(true);
		
	}
});

/* ================= GRID: Temp ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrder_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelOrder_Dc$Temp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrder_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelOrder_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", noEdit:true , allowBlank:false, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32, labelWidth:100,
			retFieldMapping: [{lovField:"id", dsField: "customerId"} ,{lovField:"id", dsField: "contactObjectId"} ]})
		.addDateField({name:"postedOn", bind:"{d.postedOn}", dataIndex:"postedOn", _enableFn_: this._controller_.isEnable, allowBlank:false, labelWidth:100})
		.addLov({name:"contactName", bind:"{d.contactName}", dataIndex:"contactName", _enableFn_: this._controller_.isEnable, xtype:"fmbas_ContactsLov_Lov", maxLength:255, labelWidth:155,
			retFieldMapping: [{lovField:"id", dsField: "contactId"} ],
			filterFieldMapping: [{lovField:"objectId", dsField: "contactId"}, {lovField:"objectType", value: "Customers"} ]})
		.addTextField({ name:"customerReferenceNo", bind:"{d.customerReferenceNo}", dataIndex:"customerReferenceNo", _enableFn_: this._controller_.isEnable, maxLength:32, labelWidth:155})
		.addLov({name:"registeredBy", bind:"{d.userName}", dataIndex:"userName", _enableFn_: this._controller_.isEnable, xtype:"fmbas_UserNameLov_Lov", maxLength:255, labelWidth:115,
			retFieldMapping: [{lovField:"id", dsField: "userId"} ,{lovField:"userCode", dsField: "userCode"} ]})
		.addHiddenField({ name:"code", bind:"{d.code}", dataIndex:"code", labelWidth:90})
		.addDisplayFieldText({ name:"separator", bind:"{d.separator}", dataIndex:"separator", maxLength:16, labelWidth:885, labelAlign:"center"})
		.addTextField({ name:"creditTerms", bind:"{d.creditTerms}", dataIndex:"creditTerms", noEdit:true , maxLength:64, labelWidth:100, readOnly:true})
		.addNumberField({name:"creditLimit", bind:"{d.creditLimit}", dataIndex:"creditLimit", noEdit:true , width:282, sysDec:"dec_crncy", maxLength:21, labelWidth:150, readOnly:true})
		.addTextField({ name:"currency", bind:"{d.creditCurrency}", dataIndex:"creditCurrency", noEdit:true , width:40, maxLength:32, hideLabel:"true", readOnly:true, style:"margin-left:5px"})
		.addBooleanField({ name:"creditCard", bind:"{d.creditCard}", dataIndex:"creditCard", noEdit:true , labelWidth:110, readOnly:true})
		.addNumberField({name:"exposure", bind:"{d.exposure}", dataIndex:"exposure", noEdit:true , width:155, sysDec:"dec_prc", maxLength:19, labelWidth:90, readOnly:true, listeners:{change: {scope: this, fn:function(el) {this._setExposureColor_()}}}})
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", width:70, maxLength:1, labelWidth:20, readOnly:true})
		.addBooleanField({ name:"openRelease", bind:"{d.openRelease}", dataIndex:"openRelease", _enableFn_: this._controller_.isEnable, labelWidth:115})
		.addHiddenField({ name:"alertLimit", bind:"{d.alertLimit}", dataIndex:"alertLimit", width:160, listeners:{change: {scope: this, fn:function(el) {this._setExposureColor_()}}}})
		.addDisplayFieldText({ name:"status", bind:"{d.status}", dataIndex:"status", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"fuelQuoteCode", bind:"{d.fuelQuoteCode}", dataIndex:"fuelQuoteCode", noEdit:true , maxLength:32})
		.addDisplayFieldDate({ name:"fuelQuoteIssueDate", bind:"{d.fuelQuoteIssueDate}", dataIndex:"fuelQuoteIssueDate", noEdit:true  })
		.addDisplayFieldDate({ name:"fuelQuoteValidUntil", bind:"{d.fuelQuoteValidUntil}", dataIndex:"fuelQuoteValidUntil", noEdit:true  })
		.addDisplayFieldText({ name:"receiver", bind:"{d.subsidiaryCode}", dataIndex:"subsidiaryCode", noEdit:true , maxLength:32})
		.addDisplayFieldText({ name:"formTitle", bind:"{d.formTitle}", dataIndex:"formTitle", noEdit:true , maxLength:32})
		.add({name:"credit", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("creditTerms"),this._getConfig_("creditLimit"),this._getConfig_("currency"),this._getConfig_("creditCard"),this._getConfig_("exposure"),this._getConfig_("percent"),this._getConfig_("alertLimit")]})
		.add({name:"fuelQuoteOpenRelease", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("fuelQuoteValidUntil"),this._getConfig_("openRelease")]})
		.add({name:"row7", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("formTitle")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:1280})
		.addPanel({ name:"row1", width:1280, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"p2",  cls:"sone-kpi-panel", layout: {type:"table"}})
		.addPanel({ name:"titleAndKpi",  style:"margin-bottom:10px", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"row2", width:885})
		.addPanel({ name:"row3", width:1280})
		.addPanel({ name:"c0", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c1", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c2", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c3", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"c4", width:300, defaults: { labelAlign:"top"}, layout:"anchor"})
		.addPanel({ name:"title", width:300,  cls:"sone-title-panel", layout:"anchor"})
		.addPanel({ name:"rowSeparator", width:300, layout:"anchor"})
		.addPanel({ name:"col1", width:270, layout:"anchor"})
		.addPanel({ name:"col2", width:325, layout:"anchor"})
		.addPanel({ name:"col3", width:270, layout:"anchor"})
		.addPanel({ name:"col4", width:1270, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["titleAndKpi", "row1", "row2", "row3"])
		.addChildrenTo("row1", ["col1", "col2", "col3"])
		.addChildrenTo("p2", ["c0", "c1", "c2", "c3", "c4"])
		.addChildrenTo("titleAndKpi", ["title", "p2"])
		.addChildrenTo("row2", ["rowSeparator"])
		.addChildrenTo("row3", ["col4"])
		.addChildrenTo("c0", ["fuelQuoteCode"])
		.addChildrenTo("c1", ["status"])
		.addChildrenTo("c2", ["fuelQuoteIssueDate"])
		.addChildrenTo("c3", ["fuelQuoteValidUntil"])
		.addChildrenTo("c4", ["receiver"])
		.addChildrenTo("title", ["row7"])
		.addChildrenTo("rowSeparator", ["separator"])
		.addChildrenTo("col1", ["customer", "postedOn"])
		.addChildrenTo("col2", ["contactName", "customerReferenceNo"])
		.addChildrenTo("col3", ["registeredBy", "openRelease", "code"])
		.addChildrenTo("col4", ["credit"]);
	},
	/* ==================== Business functions ==================== */
	
	_setExposureColor_: function() {
		
		
						var record = this._controller_.getRecord();
						var redStyles = {background: "#F94F52", color: "#FFFFFF", opacity: 1};
						var yellowStyles = {background: "#F9B344", color: "#FFFFFF", opacity: 1};
						var greenStyles = {background: "#52AA39", color: "#FFFFFF", opacity: 1};
						var neutralStyles = {background: "#DDDDDD", color: "#333333", opacity: 0.7};
		
						if (record) {
							var alertLimit = record.get("alertLimit");
							var exposureValue = record.get("exposure");
							var exposure = this._get_("exposure");
		
							if (!Ext.isEmpty(exposureValue)) {
								if(exposureValue <= alertLimit) {
		           					exposure.inputEl.setStyle(greenStyles);
							    } else if (exposureValue > alertLimit && exposureValue <= 100) {
		            				exposure.inputEl.setStyle(yellowStyles);
							    } else if (exposureValue > alertLimit && exposureValue >= 100) {
		            				exposure.inputEl.setStyle(redStyles);
							    }
								exposure.addCls("sone-field-no-border");
							}
							else {
								exposure.setRawValue(0);
								exposure.inputEl.setStyle(neutralStyles);
								exposure.removeCls("sone-field-no-border");
							}
						}
							
						
						
	},
	
	_afterDefineElements_: function() {
		
		
						this._getBuilder_().change("titleAndKpi",{
							layout: {
						        type: "hbox",
						        align: "stretch"
						    },
						    defaults: {
						        flex: 1,
								height: 60
						    }
						});
						
	},
	
	_afterApplyStates_: function() {
		
						if(this._controller_.getRecord()){
							var code = this._controller_.getRecord().get("code");
							var formTitle = this._get_("formTitle");
							var originalTitleLabel = formTitle.fieldLabel;
							formTitle.labelEl.update(originalTitleLabel+" #"+code);
						}
	}
});

/* ================= EDIT FORM: EditRemark ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrder_Dc$EditRemark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelOrder_Dc$EditRemark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remarks}", paramIndex:"remarks", labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"remarkCol1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						return true;
	}
});
