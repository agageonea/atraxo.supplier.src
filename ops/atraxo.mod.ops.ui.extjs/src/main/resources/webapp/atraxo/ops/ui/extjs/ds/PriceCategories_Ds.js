/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.PriceCategories_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_PriceCategories_Ds"
	},
	
	
	fields: [
		{name:"name", type:"string"},
		{name:"continous", type:"boolean"},
		{name:"includeInAverage", type:"boolean"},
		{name:"quantityType", type:"string"},
		{name:"exchangeRateOffset", type:"string"},
		{name:"restriction", type:"boolean"},
		{name:"vat", type:"string"},
		{name:"defaultPriceCtgy", type:"boolean"},
		{name:"comments", type:"string"},
		{name:"forex", type:"string"},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractValidFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"contractValidTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"priceCategory", type:"int", allowNull:true},
		{name:"priceCtgryName", type:"string"},
		{name:"pcType", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceName", type:"string"},
		{name:"avgMethodIndicatorDefMeth", type:"boolean"},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"convertedPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"conversion", type:"string", noFilter:true, noSort:true},
		{name:"used", type:"boolean", noFilter:true, noSort:true},
		{name:"slash", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.PriceCategories_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"name", type:"string"},
		{name:"continous", type:"boolean", allowNull:true},
		{name:"includeInAverage", type:"boolean", allowNull:true},
		{name:"quantityType", type:"string"},
		{name:"exchangeRateOffset", type:"string"},
		{name:"restriction", type:"boolean", allowNull:true},
		{name:"vat", type:"string"},
		{name:"defaultPriceCtgy", type:"boolean", allowNull:true},
		{name:"comments", type:"string"},
		{name:"forex", type:"string"},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractValidFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"contractValidTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noFilter:true, noSort:true},
		{name:"value", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"currencyId", type:"int", allowNull:true},
		{name:"currencyCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"priceCategory", type:"int", allowNull:true},
		{name:"priceCtgryName", type:"string"},
		{name:"pcType", type:"string"},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceName", type:"string"},
		{name:"avgMethodIndicatorDefMeth", type:"boolean", allowNull:true},
		{name:"avgMethodIndicatorName", type:"string"},
		{name:"avgMthdId", type:"int", allowNull:true},
		{name:"price", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"convertedPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"conversion", type:"string", noFilter:true, noSort:true},
		{name:"used", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"slash", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});

Ext.define("atraxo.ops.ui.extjs.ds.PriceCategories_DsParam", {
	extend: 'Ext.data.Model',


	validators: {
		settlementCurrencyCode: [{type: 'presence'}],
		settlementUnitCode: [{type: 'presence'}]
	},


	initParam: function() {
		this.set("settlementCurrencyCode", _SYSTEMPARAMETERS_.syscrncy);
		this.set("settlementUnitCode", _SYSTEMPARAMETERS_.sysvol);
	},

	fields: [
		{name:"settlementCurrencyCode", type:"string"},
		{name:"settlementUnitCode", type:"string"},
		{name:"total", type:"float", allowNull:true}
	]
});
