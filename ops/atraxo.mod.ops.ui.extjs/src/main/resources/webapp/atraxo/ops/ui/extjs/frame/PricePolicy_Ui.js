/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.frame.PricePolicy_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.PricePolicy_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("pricePolicy", Ext.create(atraxo.ops.ui.extjs.dc.PricePolicy_Dc,{trackEditMode: true}))
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnOnEditWdwClose",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnOnEditWdwClose, scope:this})
		.addButton({name:"btnSaveCloseWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseWdw, scope:this})
		.addButton({name:"btnSaveNewWdw",glyph:fp_asc.new_glyph.glyph,iconCls: fp_asc.new_glyph.css, disabled:false, handler: this.onBtnSaveNewWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"helpWdwEdit",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdwEdit, scope:this})
		.addDcFilterFormView("pricePolicy", {name:"Filter", xtype:"ops_PricePolicy_Dc$Filter"})
		.addDcGridView("pricePolicy", {name:"List", xtype:"ops_PricePolicy_Dc$List"})
		.addDcFormView("pricePolicy", {name:"PopUp", xtype:"ops_PricePolicy_Dc$Edit"})
		.addDcFormView("pricePolicy", {name:"Edit", xtype:"ops_PricePolicy_Dc$Edit"})
		.addWindow({name:"wdwPricePolicy", _hasTitle_:true, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("PopUp")],  listeners:{ close:{fn:this.onWdwPricePolClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveNewWdw"), this._elems_.get("btnSaveCloseWdw"), this._elems_.get("btnOnEditWdwClose")]}]})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas2", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}}); 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["List"], ["center"])
		.addChildrenTo("canvas2", ["Edit"], ["center"])
		.addToolbarTo("List", "tlbList")
		.addToolbarTo("Edit", "tlbEdit");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas2"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbList", {dc: "pricePolicy"})
			.addButtons([])
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbEdit", {dc: "pricePolicy"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addSave({iconCls:fp_asc.save_glyph.css,glyph:fp_asc.save_glyph.glyph}).addCancel({iconCls:fp_asc.rollback_glyph.css,glyph:fp_asc.rollback_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdwEdit")])
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnOnEditWdwClose
	 */
	,onBtnOnEditWdwClose: function() {
		this.onWdwPricePolClose();
		this._getWindow_("wdwPricePolicy").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseWdw
	 */
	,onBtnSaveCloseWdw: function() {
		this.saveClose();
		this._getDc_("pricePolicy").doReloadPage();
	}
	
	/**
	 * On-Click handler for button btnSaveNewWdw
	 */
	,onBtnSaveNewWdw: function() {
		this.saveNew();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button helpWdwEdit
	 */
	,onHelpWdwEdit: function() {
		this.openHelp();
	}
	
	,onShowWdw: function() {	
		this._getWindow_("wdwPricePolicy").show();
	}
	
	,_afterDefineDcs_: function() {
		
		
					var pricePolicy = this._getDc_("pricePolicy");
		
					pricePolicy.on("afterDoNew", function (dc) {
						dc.setEditMode();
					    this.onShowWdw();
					}, this);
		
					this._getDc_("pricePolicy").on("afterDoSaveSuccess", function (dc, ajaxResult) { 
						if (ajaxResult.options.options.doNewAfterSave === true) {
							dc.doNew(); 
						} else if (ajaxResult.options.options.doCloseNewWindowAfterSave === true) {
							this._getWindow_("wdwPricePolicy").close();
						}
					}, this);
		
	}
	
	,saveNew: function() {
		
					var pricePol = this._getDc_("pricePolicy");
					pricePol.doSave({doNewAfterSave:true});
	}
	
	,saveClose: function() {
		
					var pricePol = this._getDc_("pricePolicy");
					pricePol.doSave({doCloseNewWindowAfterSave: true });
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/PricingPolicies.html";
					window.open( url, "SONE_Help");
	}
	
	,onWdwPricePolClose: function() {
		
						var dc = this._getDc_("pricePolicy");
						dc.clearEditMode();
						dc.doCancel(); 
	}
});
