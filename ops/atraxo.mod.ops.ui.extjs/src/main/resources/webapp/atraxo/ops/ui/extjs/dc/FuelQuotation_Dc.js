/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelQuotation_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ops.ui.extjs.ds.FuelQuotation_DsParam,
	recordModel: atraxo.ops.ui.extjs.ds.FuelQuotation_Ds,
		
			/* ================ Business functions ================ */
	
	canDoDelete: function() {
		
					var selRecords = this.selectedRecords;
		
					for (var i = 0; i < selRecords.length; i++) {
		               if ((selRecords[i].data.status == __TYPES__.fuelQuotation.status.ordered || selRecords[i].data.status == __TYPES__.fuelQuotation.status.submitted)
							&& !Ext.isEmpty(selRecords[i].data.code)) {
						  return false;
					   }
					}
					return  true;
		
	}

});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FuelQuotation_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FuelQuotation_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"customerCode", dataIndex:"customerCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CustomerCodeLov_Lov", selectOnFocus:true, maxLength:32,
					retFieldMapping: [{lovField:"id", dsField: "customerId"} ]}})
			.addTextField({ name:"code", dataIndex:"code", maxLength:32})
			.addDateField({name:"postedOn", dataIndex:"postedOn"})
			.addDateField({name:"validUntil", dataIndex:"validUntil"})
			.addCombo({ xtype:"combo", name:"type", dataIndex:"type", store:[ __OPS_TYPE__.FuelQuoteType._SCHEDULED_, __OPS_TYPE__.FuelQuoteType._AD_HOC_]})
			.addCombo({ xtype:"combo", name:"scope", dataIndex:"scope", store:[ __OPS_TYPE__.FuelQuoteScope._INTO_PLANE_, __OPS_TYPE__.FuelQuoteScope._EX_HYDRANT_, __OPS_TYPE__.FuelQuoteScope._INTO_STORAGE_]})
			.addCombo({ xtype:"combo", name:"pricingCycle", dataIndex:"pricingCycle", store:[ __OPS_TYPE__.FuelQuoteCycle._NOTICE_, __OPS_TYPE__.FuelQuoteCycle._WEEKLY_, __OPS_TYPE__.FuelQuoteCycle._BI_WEEKLY_, __OPS_TYPE__.FuelQuoteCycle._MONTHLY_]})
			.addLov({name:"fuelRequestCode", dataIndex:"fuelRequestCode", maxLength:32,
				editor:{xtype:"atraxo.ops.ui.extjs.lov.FuelRequestLov_Lov", selectOnFocus:true, maxLength:32}})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __OPS_TYPE__.FuelQuoteStatus._NEW_, __OPS_TYPE__.FuelQuoteStatus._SUBMITTED_, __OPS_TYPE__.FuelQuoteStatus._NEGOTIATING_, __OPS_TYPE__.FuelQuoteStatus._ORDERED_, __OPS_TYPE__.FuelQuoteStatus._CLOSED_, __OPS_TYPE__.FuelQuoteStatus._CANCELED_, __OPS_TYPE__.FuelQuoteStatus._EXPIRED_]})
			.addBooleanField({ name:"openRelease", dataIndex:"openRelease"})
			.addTextField({ name:"owner", dataIndex:"owner", maxLength:64})
			.addLov({name:"holder", dataIndex:"subsidiaryCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.UserSubsidiaryLov_Lov", selectOnFocus:true, maxLength:32}})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuotation_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelQuotation_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"customer", dataIndex:"customerCode", width:100})
		.addDateColumn({ name:"postedOn", dataIndex:"postedOn", _mask_: Masks.DATE})
		.addDateColumn({ name:"validUntil", dataIndex:"validUntil", _mask_: Masks.DATE})
		.addTextColumn({ name:"type", dataIndex:"type", width:70})
		.addTextColumn({ name:"scope", dataIndex:"scope", width:100})
		.addTextColumn({ name:"cycle", dataIndex:"pricingCycle", width:100})
		.addTextColumn({ name:"fuelRequestCode", dataIndex:"fuelRequestCode", width:100})
		.addTextColumn({ name:"status", dataIndex:"status", width:100})
		.addBooleanColumn({ name:"openRelease", dataIndex:"openRelease", width:100})
		.addTextColumn({ name:"owner", dataIndex:"owner", width:100})
		.addNumberColumn({ name:"exposure", dataIndex:"exposure", width:110, sysDec:"dec_prc",  renderer:function(val, meta, record, rowIndex) { return this._markUtilization_(val, meta, record, rowIndex); }})
		.addTextColumn({ name:"code", dataIndex:"code", hidden:true, width:80})
		.addTextColumn({ name:"holder", dataIndex:"subsidiaryCode", hidden:true, width:100})
		.addNumberColumn({ name:"creditLimit", dataIndex:"creditLimit", hidden:true, width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"creditTerm", dataIndex:"creditTerms", hidden:true, width:100})
		.addTextColumn({ name:"creditCurrency", dataIndex:"creditCurrency", hidden:true, width:120})
		.addBooleanColumn({ name:"creditCard", dataIndex:"creditCard", hidden:true, width:120})
		.addNumberColumn({ name:"alertLimit", dataIndex:"alertLimit", hidden:true, width:80})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
	,
	/* ==================== Business functions ==================== */
	
	_markUtilization_: function(value,meta,record,rowIndex) {
		
		
						var globalStyles = "border:0px !important";
						var redStyles = "background: #F94F52; color: #FFFFFF;"+globalStyles;
						var yellowStyles = "background: #F9B344; color: #FFFFFF;"+globalStyles;
						var greenStyles = "background: #52AA39; color: #FFFFFF;"+globalStyles;
		
						var alertLimit = record.get("alertLimit");
		
						if (!Ext.isEmpty(value)) {
							if(value <= alertLimit) {
					        	meta.style = greenStyles;
						    } else if (value > alertLimit && value <= 100) {
						        meta.style = yellowStyles;
						    } else if (value > alertLimit && value >= 100) {
						        meta.style = redStyles;
						    }
							return value +" %"
						}
						
	}
});

/* ================= EDIT FORM: New ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuotation_Dc$New", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuotation_Dc$New",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", allowBlank:false, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "customerId"} ]})
		.addCombo({ xtype:"combo", name:"type", bind:"{d.type}", dataIndex:"type", allowBlank:false, store:[ __OPS_TYPE__.FuelQuoteType._SCHEDULED_, __OPS_TYPE__.FuelQuoteType._AD_HOC_]})
		.addBooleanField({ name:"openRelease", bind:"{d.openRelease}", dataIndex:"openRelease"})
		.addDateField({name:"postedOn", bind:"{d.postedOn}", dataIndex:"postedOn", allowBlank:false})
		.addCombo({ xtype:"combo", name:"scope", bind:"{d.scope}", dataIndex:"scope", store:[ __OPS_TYPE__.FuelQuoteScope._INTO_PLANE_, __OPS_TYPE__.FuelQuoteScope._EX_HYDRANT_, __OPS_TYPE__.FuelQuoteScope._INTO_STORAGE_]})
		.addDateField({name:"validUntil", bind:"{d.validUntil}", dataIndex:"validUntil", allowBlank:false})
		.addCombo({ xtype:"combo", name:"cycle", bind:"{d.pricingCycle}", dataIndex:"pricingCycle", store:[ __OPS_TYPE__.FuelQuoteCycle._NOTICE_, __OPS_TYPE__.FuelQuoteCycle._WEEKLY_, __OPS_TYPE__.FuelQuoteCycle._BI_WEEKLY_, __OPS_TYPE__.FuelQuoteCycle._MONTHLY_]})
		.addHiddenField({ name:"owner", bind:"{d.owner}", dataIndex:"owner"})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("customer"),this._getConfig_("postedOn"),this._getConfig_("validUntil")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("type"),this._getConfig_("scope"),this._getConfig_("cycle")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:840, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["row1", "row2", "openRelease"]);
	}
});

/* ================= GRID: Temp ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuotation_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelQuotation_Dc$Temp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Edit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuotation_Dc$Edit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuotation_Dc$Edit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", _enableFn_: this._isEnabled_, allowBlank:false, width:200, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "customerId"} ]})
		.addCombo({ xtype:"combo", name:"type", bind:"{d.type}", dataIndex:"type", _enableFn_: this._isEnabled_, allowBlank:false, width:200, store:[ __OPS_TYPE__.FuelQuoteType._SCHEDULED_, __OPS_TYPE__.FuelQuoteType._AD_HOC_]})
		.addBooleanField({ name:"openRelease", bind:"{d.openRelease}", dataIndex:"openRelease", _enableFn_: this._isEnabled_, width:250})
		.addDateField({name:"postedOn", bind:"{d.postedOn}", dataIndex:"postedOn", _enableFn_: this._isEnabledDates_, allowBlank:false, width:250, labelWidth:80})
		.addCombo({ xtype:"combo", name:"scope", bind:"{d.scope}", dataIndex:"scope", _enableFn_: this._isEnabled_, width:250, store:[ __OPS_TYPE__.FuelQuoteScope._INTO_PLANE_, __OPS_TYPE__.FuelQuoteScope._EX_HYDRANT_, __OPS_TYPE__.FuelQuoteScope._INTO_STORAGE_], labelWidth:80})
		.addDateField({name:"validUntil", bind:"{d.validUntil}", dataIndex:"validUntil", _enableFn_: this._isEnabledDates_, allowBlank:false, width:250})
		.addCombo({ xtype:"combo", name:"cycle", bind:"{d.pricingCycle}", dataIndex:"pricingCycle", _enableFn_: this._isEnabled_, width:250, store:[ __OPS_TYPE__.FuelQuoteCycle._NOTICE_, __OPS_TYPE__.FuelQuoteCycle._WEEKLY_, __OPS_TYPE__.FuelQuoteCycle._BI_WEEKLY_, __OPS_TYPE__.FuelQuoteCycle._MONTHLY_]})
		.addTextField({ name:"owner", bind:"{d.owner}", dataIndex:"owner", noEdit:true , width:250, maxLength:64})
		.addTextField({ name:"fuelRequestCode", bind:"{d.fuelRequestCode}", dataIndex:"fuelRequestCode", noEdit:true , width:200, maxLength:32})
		.addTextField({ name:"status", bind:"{d.status}", dataIndex:"status", noEdit:true , width:200, maxLength:32})
		.addTextField({ name:"code", bind:"{d.code}", dataIndex:"code", noEdit:true , width:160, maxLength:32, labelWidth:60})
		.addHiddenField({ name:"alertLimit", bind:"{d.alertLimit}", dataIndex:"alertLimit", width:160, listeners:{change: {scope: this, fn:function(el) {this._setExposureColor_()}}}})
		.addDisplayFieldText({ name:"separator", bind:"{d.separator}", dataIndex:"separator", noEdit:true , maxLength:16, labelWidth:1300})
		.addTextField({ name:"creditTerms", bind:"{d.creditTerms}", dataIndex:"creditTerms", noEdit:true , width:200, maxLength:64})
		.addNumberField({name:"creditLimit", bind:"{d.creditLimit}", dataIndex:"creditLimit", noEdit:true , width:160, sysDec:"dec_crncy", maxLength:21, labelWidth:80})
		.addTextField({ name:"currency", bind:"{d.creditCurrency}", dataIndex:"creditCurrency", noEdit:true , width:40, maxLength:32, labelWidth:0, hideLabel:"true"})
		.addBooleanField({ name:"creditCard", bind:"{d.creditCard}", dataIndex:"creditCard", noEdit:true , labelWidth:80})
		.addNumberField({name:"exposure", bind:"{d.exposure}", dataIndex:"exposure", noEdit:true , width:130, sysDec:"dec_prc", maxLength:19, labelWidth:80, listeners:{change: {scope: this, fn:function(el) {this._setExposureColor_()}}}})
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", noEdit:true , width:70, maxLength:1, labelWidth:20})
		.addLov({name:"holder", bind:"{d.subsidiaryCode}", dataIndex:"subsidiaryCode", noEdit:true , width:160, xtype:"fmbas_UserSubsidiaryLov_Lov", maxLength:32, labelWidth:60})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("customer"),this._getConfig_("postedOn"),this._getConfig_("validUntil"),this._getConfig_("openRelease"),this._getConfig_("fuelRequestCode"),this._getConfig_("code")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("type"),this._getConfig_("scope"),this._getConfig_("cycle"),this._getConfig_("owner"),this._getConfig_("status"),this._getConfig_("holder")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("creditTerms"),this._getConfig_("creditLimit"),this._getConfig_("currency"),this._getConfig_("creditCard"),this._getConfig_("exposure"),this._getConfig_("percent"),this._getConfig_("alertLimit")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:1360, layout:"anchor"})
		.addPanel({ name:"col2", width:1360, layout:"anchor"})
		.addPanel({ name:"col3", width:1300, layout:"anchor"})
		.addPanel({ name:"col4", width:1360, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col3", ["separator"])
		.addChildrenTo("col4", ["row3"]);
	},
	/* ==================== Business functions ==================== */
	
	_setExposureColor_: function() {
		
		
						var record = this._controller_.getRecord();
						var redStyles = {background: "#F94F52", color: "#FFFFFF", opacity: 1};
						var yellowStyles = {background: "#F9B344", color: "#FFFFFF", opacity: 1};
						var greenStyles = {background: "#52AA39", color: "#FFFFFF", opacity: 1};
						var neutralStyles = {background: "#DDDDDD", color: "#333333", opacity: 0.7};
		
						if (record) {
							var alertLimit = record.get("alertLimit");
							var exposureValue = record.get("exposure");
						}
			
						var exposure = this._get_("exposure");
			
						if (!Ext.isEmpty(exposureValue)) {
							if(exposureValue <= alertLimit) {
			   					exposure.inputEl.setStyle(greenStyles);
						    } else if (exposureValue > alertLimit && exposureValue <= 100) {
			    				exposure.inputEl.setStyle(yellowStyles);
						    } else if (exposureValue > alertLimit && exposureValue >= 100) {
			    				exposure.inputEl.setStyle(redStyles);
						    }
							exposure.addCls("sone-field-no-border");
						}
						else {
							exposure.setRawValue(0);
							exposure.inputEl.setStyle(neutralStyles);
							exposure.removeCls("sone-field-no-border");
						}
						
						
	},
	
	_afterDefineElements_: function() {
		
						this._controller_.on("afterDoServiceSuccess", function() {
							this._applyStates_(this._controller_.getRecord());
						},this);
	},
	
	_beforeApplyStates_: function() {
		
					this._get_("percent").labelEl.update("%");
	},
	
	_isEnabled_: function() {
		
					var dc = this._controller_;
					var record = dc.getRecord();
					var status = record.get("status");
					//TODO use Types.js
					if (status=="Submitted" || status=="Closed" || status=="Canceled" || status=="Ordered" || status=="Active" || status=="Expired") {
						return false;
					}
					else {
						return true;
					}
	},
	
	_isEnabledDates_: function() {
		
					var dc = this._controller_;
					var record = dc.getRecord();
					var status = record.get("status");
		
					if (status=="Submitted" || status=="Closed" || status=="Canceled" || status=="Ordered" || status=="Active") {
						return false;
					}
					else {
						return true;
					}
	}
});

/* ================= EDIT FORM: Disclaimer ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuotation_Dc$Disclaimer", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuotation_Dc$Disclaimer",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addHtmlEditor({ name:"disclaimer", bind:{value:"{d.disclaimer}"}, dataIndex:"disclaimer", hideLabel:"true"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["disclaimer"]);
	}
});

/* ================= EDIT FORM: Remark ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuotation_Dc$Remark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuotation_Dc$Remark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remarks}", paramIndex:"remarks", labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"remarkCol1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						return true;
	}
});

/* ================= EDIT FORM: GenerateQuote ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelQuotation_Dc$GenerateQuote", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelQuotation_Dc$GenerateQuote",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateField({name:"postedOn", bind:"{p.orderPostedOn}", paramIndex:"orderPostedOn", labelWidth:130})
		.addLov({name:"submittedBy", bind:"{p.orderSubmittedName}", paramIndex:"orderSubmittedName", xtype:"fmbas_ContactsLov_Lov", maxLength:100, labelWidth:130,
			retFieldMapping: [{lovField:"id", dsParam: "orderSubmittedBy"} ],
			filterFieldMapping: [{lovField:"objectId", dsField: "customerId"}, {lovField:"objectType", dsField: "customerEntityAlias"} ]})
		.addTextField({ name:"customerRef", bind:"{p.orderCustomerRef}", paramIndex:"orderCustomerRef", maxLength:16, labelWidth:130})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:300, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["postedOn", "submittedBy", "customerRef"]);
	}
});
