/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelTicketHardCopy_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ops.ui.extjs.ds.FuelTicketHardCopy_Ds
});
