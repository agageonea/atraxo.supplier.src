/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.PricePolicies_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ops.ui.extjs.ds.PricePolicies_DsParam,
	recordModel: atraxo.ops.ui.extjs.ds.PricePolicies_Ds
});

/* ================= GRID: PricePolicies ================= */

Ext.define("atraxo.ops.ui.extjs.dc.PricePolicies_Dc$PricePolicies", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_PricePolicies_Dc$PricePolicies",
	selType: null,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"policyName", dataIndex:"policyName", width:100})
		.addTextColumn({ name:"location", dataIndex:"locationCode", width:70})
		.addTextColumn({ name:"customer", dataIndex:"customerCode", width:70})
		.addTextColumn({ name:"aircraft", dataIndex:"aircraftType", width:100})
		.addTextColumn({ name:"policyType", dataIndex:"policyType", width:100})
		.addNumberColumn({ name:"value", dataIndex:"value", width:70, sysDec:"dec_crncy"})
		.addTextColumn({ name:"unit", dataIndex:"policyUnit", width:60})
		.addNumberColumn({ name:"markup", dataIndex:"markup", width:80, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"sellingFuelPrice", dataIndex:"sellingFuelPrice", width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"productPrice", dataIndex:"gridProductPrice", width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"differenctial", dataIndex:"differential", width:100, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"dft", dataIndex:"gridDft", width:70, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"totalSellingPrice", dataIndex:"totalSellingPrice", width:100, sysDec:"dec_crncy"})
		.addTextColumn({ name:"sellingUnit", dataIndex:"sellingUnit", width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
