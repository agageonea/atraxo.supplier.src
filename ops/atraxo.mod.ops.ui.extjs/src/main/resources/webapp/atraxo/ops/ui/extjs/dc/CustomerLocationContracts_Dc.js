/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.CustomerLocationContracts_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ops.ui.extjs.ds.CustomerLocationContracts_DsParam,
	recordModel: atraxo.ops.ui.extjs.ds.CustomerLocationContracts_Ds
});

/* ================= GRID: SupplierContracts ================= */

Ext.define("atraxo.ops.ui.extjs.dc.CustomerLocationContracts_Dc$SupplierContracts", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_CustomerLocationContracts_Dc$SupplierContracts",
	selType: null,


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"supplier", dataIndex:"counterPartyCode", width:80})
		.addTextColumn({ name:"contract", dataIndex:"code", width:80})
		.addTextColumn({ name:"domInt", dataIndex:"limitedTo", width:80})
		.addTextColumn({ name:"type", dataIndex:"type", width:80})
		.addTextColumn({ name:"delivery", dataIndex:"delivery", width:80})
		.addTextColumn({ name:"scope", dataIndex:"scope", width:80})
		.addTextColumn({ name:"pricingBase", dataIndex:"pricingBases", width:80})
		.addNumberColumn({ name:"fuelPrice", dataIndex:"fuelPrice", width:80, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"dft", dataIndex:"dft", width:80, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"totalPrice", dataIndex:"totalPrice", width:80, sysDec:"dec_crncy"})
		.addTextColumn({ name:"unit", dataIndex:"unit", width:70})
		.addNumberColumn({ name:"paymentTerms", dataIndex:"paymentTerms", width:100})
		.addTextColumn({ name:"credtiTerms", dataIndex:"creditTerms", width:100})
		.addNumberColumn({ name:"exposure", dataIndex:"exposure", width:80})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
