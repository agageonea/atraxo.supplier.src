/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelRequestLov_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_FuelRequestLov_Ds"
	},
	
	
	fields: [
		{name:"requestCode", type:"string"},
		{name:"requestDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"customerReferenceNo", type:"string"},
		{name:"requestedService", type:"string"},
		{name:"isOpenRelease", type:"boolean"},
		{name:"startDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"requestStatus", type:"string"},
		{name:"processedBy", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelRequestLov_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"requestCode", type:"string"},
		{name:"requestDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"customerReferenceNo", type:"string"},
		{name:"requestedService", type:"string"},
		{name:"isOpenRelease", type:"boolean", allowNull:true},
		{name:"startDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"endDate", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"requestStatus", type:"string"},
		{name:"processedBy", type:"string"},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
