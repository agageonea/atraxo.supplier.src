/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelQuoteLocation_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_FuelQuoteLocation_Ds"
	},
	
	
	validators: {
		locCode: [{type: 'presence'}],
		flightType: [{type: 'presence'}],
		operationalType: [{type: 'presence'}],
		forex: [{type: 'presence'}],
		period: [{type: 'presence'}],
		fuelReleaseStartsOn: [{type: 'presence'}],
		fuelReleaseEndsOn: [{type: 'presence'}]
	},
	
	initRecord: function() {
		this.set("flightType", "Unspecified");
		this.set("operationalType", "Business");
		this.set("product", "Jet-A1");
		this.set("deliveryMethod", "Unspecified");
		this.set("unitCode", _SYSTEMPARAMETERS_.sysvol);
	},
	
	fields: [
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"days", type:"string", noFilter:true, noSort:true},
		{name:"fuelReleaseVP", type:"string", noFilter:true, noSort:true},
		{name:"supplyInfo", type:"string", noFilter:true, noSort:true},
		{name:"pricingInfo", type:"string", noFilter:true, noSort:true},
		{name:"pricingOpt", type:"string", noFilter:true, noSort:true},
		{name:"supplyOpt", type:"string", noFilter:true, noSort:true},
		{name:"paymentUnit", type:"string", noFilter:true, noSort:true},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"averageMethodId", type:"int", allowNull:true},
		{name:"averageMethodCode", type:"string"},
		{name:"averageMethodName", type:"string"},
		{name:"quoteId", type:"int", allowNull:true},
		{name:"quoteCode", type:"string"},
		{name:"quoteStatus", type:"string"},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string"},
		{name:"suppId", type:"int", allowNull:true},
		{name:"suppCode", type:"string"},
		{name:"pricingPolicyId", type:"int", allowNull:true},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"iplId", type:"int", allowNull:true},
		{name:"iplAgentCode", type:"string"},
		{name:"quotationId", type:"int", allowNull:true},
		{name:"quotationName", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"priceBasis", type:"string"},
		{name:"flightType", type:"string"},
		{name:"operationalType", type:"string"},
		{name:"product", type:"string"},
		{name:"quoteVersion", type:"string"},
		{name:"deliveryMethod", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"events", type:"int", allowNull:true},
		{name:"indexOffset", type:"string"},
		{name:"basePrice", type:"float", allowNull:true},
		{name:"differential", type:"float", allowNull:true},
		{name:"fuelPrice", type:"float", allowNull:true},
		{name:"intoplaneFee", type:"float", allowNull:true},
		{name:"otherFees", type:"float", allowNull:true},
		{name:"taxes", type:"float", allowNull:true},
		{name:"flightFee", type:"float", allowNull:true},
		{name:"totalPricePerUom", type:"float", allowNull:true},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"disclaimer", type:"string"},
		{name:"paymentRefDate", type:"string"},
		{name:"invoiceFreq", type:"string"},
		{name:"period", type:"string"},
		{name:"fuelReleaseStartsOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fuelReleaseEndsOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"settlementCurrencyCode", type:"string"},
		{name:"settlementUnitCode", type:"string"},
		{name:"forex", type:"string"},
		{name:"dft", type:"float", allowNull:true},
		{name:"toleranceMessage", type:"string"},
		{name:"capturedMargin", type:"float", allowNull:true},
		{name:"quotation", type:"string", noFilter:true, noSort:true},
		{name:"supplierUnitCode", type:"string", noFilter:true, noSort:true},
		{name:"resellerDiff", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"selectPricePolicy", type:"boolean", noFilter:true, noSort:true},
		{name:"capturePricePolicy", type:"boolean", noFilter:true, noSort:true},
		{name:"sellingPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"selectedMarkup", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"capturedMarkup", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"selectedPP", type:"string", noFilter:true, noSort:true},
		{name:"capturedPP", type:"string", noFilter:true, noSort:true},
		{name:"teext", type:"string", noFilter:true, noSort:true},
		{name:"pricePolicy", type:"string", noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"selectMarkup", type:"boolean", noFilter:true, noSort:true},
		{name:"selectMargin", type:"boolean", noFilter:true, noSort:true},
		{name:"isFlightEvent", type:"boolean", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.FuelQuoteLocation_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"separator", type:"string", noFilter:true, noSort:true},
		{name:"days", type:"string", noFilter:true, noSort:true},
		{name:"fuelReleaseVP", type:"string", noFilter:true, noSort:true},
		{name:"supplyInfo", type:"string", noFilter:true, noSort:true},
		{name:"pricingInfo", type:"string", noFilter:true, noSort:true},
		{name:"pricingOpt", type:"string", noFilter:true, noSort:true},
		{name:"supplyOpt", type:"string", noFilter:true, noSort:true},
		{name:"paymentUnit", type:"string", noFilter:true, noSort:true},
		{name:"financialSourceId", type:"int", allowNull:true},
		{name:"financialSourceCode", type:"string"},
		{name:"averageMethodId", type:"int", allowNull:true},
		{name:"averageMethodCode", type:"string"},
		{name:"averageMethodName", type:"string"},
		{name:"quoteId", type:"int", allowNull:true},
		{name:"quoteCode", type:"string"},
		{name:"quoteStatus", type:"string"},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"contractId", type:"int", allowNull:true},
		{name:"contractCode", type:"string"},
		{name:"locId", type:"int", allowNull:true},
		{name:"locCode", type:"string"},
		{name:"suppId", type:"int", allowNull:true},
		{name:"suppCode", type:"string"},
		{name:"pricingPolicyId", type:"int", allowNull:true},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"iplId", type:"int", allowNull:true},
		{name:"iplAgentCode", type:"string"},
		{name:"quotationId", type:"int", allowNull:true},
		{name:"quotationName", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"priceBasis", type:"string"},
		{name:"flightType", type:"string"},
		{name:"operationalType", type:"string"},
		{name:"product", type:"string"},
		{name:"quoteVersion", type:"string"},
		{name:"deliveryMethod", type:"string"},
		{name:"quantity", type:"float", allowNull:true},
		{name:"events", type:"int", allowNull:true},
		{name:"indexOffset", type:"string"},
		{name:"basePrice", type:"float", allowNull:true},
		{name:"differential", type:"float", allowNull:true},
		{name:"fuelPrice", type:"float", allowNull:true},
		{name:"intoplaneFee", type:"float", allowNull:true},
		{name:"otherFees", type:"float", allowNull:true},
		{name:"taxes", type:"float", allowNull:true},
		{name:"flightFee", type:"float", allowNull:true},
		{name:"totalPricePerUom", type:"float", allowNull:true},
		{name:"paymentTerms", type:"int", allowNull:true},
		{name:"disclaimer", type:"string"},
		{name:"paymentRefDate", type:"string"},
		{name:"invoiceFreq", type:"string"},
		{name:"period", type:"string"},
		{name:"fuelReleaseStartsOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"fuelReleaseEndsOn", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"settlementCurrencyCode", type:"string"},
		{name:"settlementUnitCode", type:"string"},
		{name:"forex", type:"string"},
		{name:"dft", type:"float", allowNull:true},
		{name:"toleranceMessage", type:"string"},
		{name:"capturedMargin", type:"float", allowNull:true},
		{name:"quotation", type:"string", noFilter:true, noSort:true},
		{name:"supplierUnitCode", type:"string", noFilter:true, noSort:true},
		{name:"resellerDiff", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"selectPricePolicy", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"capturePricePolicy", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"sellingPrice", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"selectedMarkup", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"capturedMarkup", type:"float", allowNull:true, noFilter:true, noSort:true},
		{name:"selectedPP", type:"string", noFilter:true, noSort:true},
		{name:"capturedPP", type:"string", noFilter:true, noSort:true},
		{name:"teext", type:"string", noFilter:true, noSort:true},
		{name:"pricePolicy", type:"string", noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"selectMarkup", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"selectMargin", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"isFlightEvent", type:"boolean", allowNull:true, noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
