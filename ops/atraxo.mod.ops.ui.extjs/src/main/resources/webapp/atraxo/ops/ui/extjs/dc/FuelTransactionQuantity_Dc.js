/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelTransactionQuantity_Dc", {
	extend: "e4e.dc.AbstractDc",
	recordModel: atraxo.ops.ui.extjs.ds.FuelTransactionQuantity_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FuelTransactionQuantity_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FuelTransactionQuantity_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addNumberField({name:"quantity", dataIndex:"fuelQuantity", sysDec:"dec_unit", maxLength:19})
			.addLov({name:"unit", dataIndex:"quantityUOM", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:3,
					retFieldMapping: [{lovField:"code", dsField: "quantityUOM"} ]}})
			.addCombo({ xtype:"combo", name:"quantityType", dataIndex:"quantityType", store:[ __OPS_TYPE__.FuelQuantityType._GROSS_, __OPS_TYPE__.FuelQuantityType._NET_]})
		;
	}

});

/* ================= EDIT-GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelTransactionQuantity_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvEditableGrid",
	alias: "widget.ops_FuelTransactionQuantity_Dc$List",

	
	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()	
		.addNumberColumn({name:"quantity", dataIndex:"fuelQuantity", width:100, sysDec:"dec_unit", maxLength:19, align:"right" })
		.addLov({name:"unit", dataIndex:"quantityUOM", width:100, xtype:"gridcolumn", 
			editor:{xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:3,
				retFieldMapping: [{lovField:"code", dsField: "quantityUOM"} ]}})
		.addComboColumn({name:"quantityType", dataIndex:"quantityType", width:100, 
			editor:{xtype:"combo", mode: 'local', triggerAction:'all', forceSelection:true, store:[ __OPS_TYPE__.FuelQuantityType._GROSS_, __OPS_TYPE__.FuelQuantityType._NET_]}})
		.addDefaults();
	},

	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
