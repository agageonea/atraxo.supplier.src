/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.frame.FuelOrderLocation_Ui", {
	extend: "e4e.ui.AbstractUi",
	alias: "widget.FuelOrderLocation_Ui",
	
	/**
	 * Data-controls definition
	 */
	_defineDcs_: function() {
		this._getBuilder_().addDc("fuelOrderLocation", Ext.create(atraxo.ops.ui.extjs.dc.FuelOrderLocation_Dc,{}))
		.addDc("flightEvent", Ext.create(atraxo.ops.ui.extjs.dc.FlightEvent_Dc,{}))
		.addDc("note", Ext.create(atraxo.fmbas.ui.extjs.dc.Note_Dc,{}))
		.addDc("history", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("flightEventHistory", Ext.create(atraxo.fmbas.ui.extjs.dc.ChangeHistory_Dc,{}))
		.addDc("contract", Ext.create(atraxo.ops.ui.extjs.dc.CustomerLocationContracts_Dc,{}))
		.linkDc("flightEvent", "fuelOrderLocation",{fetchMode:"auto",fields:[
					{childField:"locationOrderId", parentField:"id"}]})
				.linkDc("note", "fuelOrderLocation",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("history", "fuelOrderLocation",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("flightEventHistory", "flightEvent",{fetchMode:"auto",fields:[
					{childField:"objectId", parentField:"id"}, {childField:"objectType", parentField:"entityAlias"}]})
				.linkDc("contract", "fuelOrderLocation",{fields:[
					{childField:"locId", parentField:"locId"}, {childParam:"startsOn", parentField:"fuelReleaseStartsOn"}, {childParam:"endsOn", parentField:"fuelReleaseEndsOn"}, {childParam:"fqlUnit", parentField:"unitId"}, {childParam:"fqlCurrency", parentField:"currId"}, {childParam:"avgMthId", parentField:"averageMethodId"}, {childParam:"finSrcId", parentField:"financialSourceId"}, {childParam:"period", parentField:"exchangeRateOffset"}, {childField:"id", parentParam:"newContractId"}]})
		;
	},

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		.addButton({name:"btnRescheduleFlightEventOpen",glyph:fp_asc.calendar_glyph.glyph,iconCls: fp_asc.calendar_glyph.css, disabled:true, handler: this.onBtnRescheduleFlightEventOpen,stateManager:[{ name:"selected_one", dc:"flightEvent", and: function(dc) {return (dc.record.data.invoiceStatus ==  __TYPES__.fuelTicket.invoiceStatus.notInvoiced && dc.record.data.billStatus == __TYPES__.fuelTicket.billStatus.notBilled);} }], scope:this})
		.addButton({name:"btnRescheduleFlightEventCancel",glyph:fp_asc.calendar_glyph.glyph,iconCls: fp_asc.calendar_glyph.css, disabled:false, handler: this.onBtnRescheduleFlightEventCancel, scope:this})
		.addButton({name:"btnRescheduleFlightEventSave",glyph:fp_asc.calendar_save_glyph.glyph,iconCls: fp_asc.calendar_save_glyph.css, disabled:false, handler: this.onBtnRescheduleFlightEventSave, scope:this})
		.addButton({name:"btnSaveCloseRemarkWdw",glyph:fp_asc.calendar_save_glyph.glyph,iconCls: fp_asc.calendar_save_glyph.css, disabled:false, handler: this.onBtnSaveCloseRemarkWdw, scope:this})
		.addButton({name:"btnCancelRemarkWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRemarkWdw, scope:this})
		.addButton({name:"btnSaveCloseCancelFlightEventRemarkWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseCancelFlightEventRemarkWdw, scope:this})
		.addButton({name:"btnCancelCancelFlightEventRemarkWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelCancelFlightEventRemarkWdw, scope:this})
		.addButton({name:"btnSaveCloseResetFlightEventRemarkWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseResetFlightEventRemarkWdw, scope:this})
		.addButton({name:"btnCancelResetlFlightEventRemarkWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelResetlFlightEventRemarkWdw, scope:this})
		.addButton({name:"btnSupplierSelect",glyph:fp_asc.list_glyph.glyph,iconCls: fp_asc.list_glyph.css, disabled:false, handler: this.onBtnSupplierSelect, scope:this})
		.addButton({name:"btnSelectSuppRemarkWdw",glyph:fp_asc.list_glyph.glyph,iconCls: fp_asc.list_glyph.css, disabled:false, handler: this.onBtnSelectSuppRemarkWdw, scope:this})
		.addButton({name:"btnCancelRemarkSuppWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCancelRemarkSuppWdw, scope:this})
		.addButton({name:"btnSupplierCancel",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnSupplierCancel, scope:this})
		.addButton({name:"btnCloseNoteWdw",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:false, handler: this.onBtnCloseNoteWdw, scope:this})
		.addButton({name:"btnSaveCloseNoteWdw",glyph:fp_asc.save_glyph.glyph,iconCls: fp_asc.save_glyph.css, disabled:false, handler: this.onBtnSaveCloseNoteWdw, scope:this})
		.addButton({name:"helpWdw",glyph:fp_asc.help_glyph.glyph,iconCls: fp_asc.help_glyph.css, disabled:false, handler: this.onHelpWdw, scope:this})
		.addButton({name:"cancelFlightEvent",glyph:fp_asc.cancel_glyph.glyph,iconCls: fp_asc.cancel_glyph.css, disabled:true, handler: this.onCancelFlightEvent,stateManager:[{ name:"selected_one", dc:"flightEvent", and: function(dc) {return (this.canCancel(dc));} }], scope:this})
		.addButton({name:"resetFlightEvent",glyph:fp_asc.rollback_glyph.glyph,iconCls: fp_asc.rollback_glyph.css, disabled:true, handler: this.onResetFlightEvent,stateManager:[{ name:"selected_one", dc:"flightEvent", and: function(dc) {return (this.canReset(dc));} }], scope:this})
		.addDcFormView("fuelOrderLocation", {name:"FuelOrderLocationDetails", xtype:"ops_FuelOrderLocation_Dc$Details"})
		.addDcGridView("flightEvent", {name:"FlightEvents", _hasTitle_:true, xtype:"ops_FlightEvent_Dc$List"})
		.addDcFormView("flightEvent", {name:"FlightEventEditor", xtype:"ops_FlightEvent_Dc$OrderEdit"})
		.addDcGridView("note", {name:"NotesList", _hasTitle_:true, xtype:"fmbas_Note_Dc$List"})
		.addDcFormView("note", {name:"NotesEdit", xtype:"fmbas_Note_Dc$Edit"})
		.addDcGridView("history", {name:"History", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("fuelOrderLocation", {name:"SuppHistory", xtype:"ops_FuelOrderLocation_Dc$Remark"})
		.addDcGridView("flightEventHistory", {name:"FlightEventHistory", _hasTitle_:true, xtype:"fmbas_ChangeHistory_Dc$ListReason"})
		.addDcFormView("fuelOrderLocation", {name:"locEdit", xtype:"ops_FuelOrderLocation_Dc$SupplySelect"})
		.addDcGridView("contract", {name:"suppList", xtype:"ops_CustomerLocationContracts_Dc$SupplierContracts"})
		.addDcFormView("flightEvent", {name:"RescheduleFlightEvent", xtype:"ops_FlightEvent_Dc$RescheduleFlightevent"})
		.addDcFormView("flightEvent", {name:"Remark", xtype:"ops_FlightEvent_Dc$Remark"})
		.addDcFormView("flightEvent", {name:"RemarkCancel", xtype:"ops_FlightEvent_Dc$Remark"})
		.addDcFormView("flightEvent", {name:"RemarkReset", xtype:"ops_FlightEvent_Dc$Remark"})
		.addPanel({name:"main", layout:"card", activeItem:0})
		.addPanel({name:"canvas1", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"canvas3", preventHeader:true, layout:{ type: "border" }, defaults:{split:true}})
		.addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"detailsTabFlightEvent", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
		.addPanel({name:"supplier", layout:{ type: "border" }, defaults:{split:true}})
		.addWindow({name:"wdwSupplier", _hasTitle_:true, width:1170, height:500, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("supplier")],  listeners:{ close:{fn:this._reloadFuelLocRecod_, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSupplierSelect"), this._elems_.get("btnSupplierCancel")]}]})
		.addWindow({name:"wdwRecheduleFlightEvent", _hasTitle_:true, width:740, height:300, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("RescheduleFlightEvent")], 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnRescheduleFlightEventSave"), this._elems_.get("btnRescheduleFlightEventCancel")]}]})
		.addWindow({name:"wdwRemarks", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("Remark")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseRemarkWdw"), this._elems_.get("btnCancelRemarkWdw")]}]})
		.addWindow({name:"wdwCancelFlightEventRemarks", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("RemarkCancel")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseCancelFlightEventRemarkWdw"), this._elems_.get("btnCancelCancelFlightEventRemarkWdw")]}]})
		.addWindow({name:"wdwResetFlightEventRemarks", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("RemarkReset")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseResetFlightEventRemarkWdw"), this._elems_.get("btnCancelResetlFlightEventRemarkWdw")]}]})
		.addWindow({name:"wdwRemarksSupp", _hasTitle_:true, width:600, height:230, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("SuppHistory")],  closable:false, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSelectSuppRemarkWdw"), this._elems_.get("btnCancelRemarkSuppWdw")]}]})
		.addWindow({name:"noteWdw", _hasTitle_:true, width:460, height:180, closeAction:'hide', resizable:true, layout:"fit", modal:true,
			items:[this._elems_.get("NotesEdit")],  listeners:{ close:{fn:this.onNoteWdwClose, scope:this} }, 
					dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
						items:[ this._elems_.get("btnSaveCloseNoteWdw"), this._elems_.get("btnCloseNoteWdw")]}]})
		.addTabPanelForUserFields({tabPanelName:"detailsTab", containerPanelName:"canvas1", dcName:"fuelOrderLocation"})
		
		.addTabPanelForUserFields({tabPanelName:"detailsTabFlightEvent", containerPanelName:"canvas3", dcName:"flightEvent"})
		; 					
	},
	
	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["canvas1"])
		.addChildrenTo("canvas1", ["FuelOrderLocationDetails", "detailsTab"], ["north", "center"])
		.addChildrenTo("canvas3", ["FlightEventEditor", "detailsTabFlightEvent"], ["north", "center"])
		.addChildrenTo("detailsTab", ["FlightEvents"])
		.addChildrenTo("detailsTabFlightEvent", ["FlightEventHistory"])
		.addChildrenTo("supplier", ["locEdit", "suppList"], ["north", "center"])
		.addToolbarTo("FuelOrderLocationDetails", "tlbEdit")
		.addToolbarTo("FlightEvents", "tlbFlightEventList")
		.addToolbarTo("NotesList", "tlbNoteList")
		.addToolbarTo("FlightEventEditor", "tlbFLightEventEditor");
	},
	
	/**
	 * Combine the rest of the components from main panel
	 */
	_linkElementsPhase2_: function() {
		this._getBuilder_()
		.addChildrenTo2("main", ["canvas3"])
		.addChildrenTo2("detailsTab", ["NotesList", "History"]);
	},
	
	/**
	 * Create toolbars
	 */
	_defineToolbars_: function() {
		this._getBuilder_()
		.beginToolbar("tlbEdit", {dc: "fuelOrderLocation"})
			.addButtons([])
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("helpWdw")])
			.addReports()
		.end()
		.beginToolbar("tlbFlightEventList", {dc: "flightEvent"})
			.addButtons([])
			.addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"main",showView:"canvas3"})
			.addSeparator().addSeparator()
			.addButtons([this._elems_.get("cancelFlightEvent"),this._elems_.get("resetFlightEvent"),this._elems_.get("btnRescheduleFlightEventOpen")])
			.addReports()
		.end()
		.beginToolbar("tlbNoteList", {dc: "note"})
			.addNew({iconCls:fp_asc.new_glyph.css,glyph:fp_asc.new_glyph.glyph,autoEdit:"false",inContainer:"main"}).addEdit({iconCls:fp_asc.edit_glyph.css,glyph:fp_asc.edit_glyph.glyph,inContainer:"canvas1",showView:"canvas2"}).addDelete({iconCls:fp_asc.delete_glyph.css,glyph:fp_asc.delete_glyph.glyph})
			.addReports()
		.end()
		.beginToolbar("tlbFLightEventEditor", {dc: "flightEvent"})
			.addBack({iconCls:fp_asc.back_glyph.css,glyph:fp_asc.back_glyph.glyph}).addPrevRec({glyph:fp_asc.prev_glyph.glyph}).addNextRec({glyph:fp_asc.next_glyph.glyph})
			.addReports()
		.end();
	}
	

	
	/**
	 * On-Click handler for button btnRescheduleFlightEventOpen
	 */
	,onBtnRescheduleFlightEventOpen: function() {
		this._getWindow_("wdwRecheduleFlightEvent").show();
	}
	
	/**
	 * On-Click handler for button btnRescheduleFlightEventCancel
	 */
	,onBtnRescheduleFlightEventCancel: function() {
		this._getWindow_("wdwRecheduleFlightEvent").close();
	}
	
	/**
	 * On-Click handler for button btnRescheduleFlightEventSave
	 */
	,onBtnRescheduleFlightEventSave: function() {
		this.rescheduleFlightEvent();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseRemarkWdw
	 */
	,onBtnSaveCloseRemarkWdw: function() {
		var successFn = function() {
			this._getWindow_("wdwRemarks").close();
			this._getWindow_("wdwRecheduleFlightEvent").close();
			this.rescheduleWarning();
			this._getDc_("fuelOrderLocation").doReloadRecord();
			this._getDc_("flightEvent").store.commitChanges();
			this._getDc_("flightEventHistory").doReloadPage();
		};
		var o={
			name:"reschedule",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("flightEvent").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelRemarkWdw
	 */
	,onBtnCancelRemarkWdw: function() {
		this._getWindow_("wdwRemarks").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseCancelFlightEventRemarkWdw
	 */
	,onBtnSaveCloseCancelFlightEventRemarkWdw: function() {
		var successFn = function() {
			this._getWindow_("wdwCancelFlightEventRemarks").close();
			this._getDc_("fuelOrderLocation").doReloadRecord();
			this._getDc_("flightEventHistory").doReloadPage();
		};
		var o={
			name:"cancel",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("flightEvent").doRpcData(o);
		this._refreshFuelOrder_();
	}
	
	/**
	 * On-Click handler for button btnCancelCancelFlightEventRemarkWdw
	 */
	,onBtnCancelCancelFlightEventRemarkWdw: function() {
		this._getWindow_("wdwCancelFlightEventRemarks").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseResetFlightEventRemarkWdw
	 */
	,onBtnSaveCloseResetFlightEventRemarkWdw: function() {
		var successFn = function() {
			this._getWindow_("wdwResetFlightEventRemarks").close();
			this._getDc_("fuelOrderLocation").doReloadRecord();
			this._getDc_("flightEventHistory").doReloadPage();
		};
		var o={
			name:"reset",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("flightEvent").doRpcData(o);
		this._refreshFuelOrder_();
	}
	
	/**
	 * On-Click handler for button btnCancelResetlFlightEventRemarkWdw
	 */
	,onBtnCancelResetlFlightEventRemarkWdw: function() {
		this._getWindow_("wdwResetFlightEventRemarks").close();
	}
	
	/**
	 * On-Click handler for button btnSupplierSelect
	 */
	,onBtnSupplierSelect: function() {
		this.openSelectSuppWdw();
	}
	
	/**
	 * On-Click handler for button btnSelectSuppRemarkWdw
	 */
	,onBtnSelectSuppRemarkWdw: function() {
		this.selectSupplier();
		var successFn = function(dc) {
			this._getDc_("history").doReloadPage();
			this._getWindow_("wdwRemarksSupp").close();
			this._getWindow_("wdwSupplier").close();
			this.displayWarningMessage(dc);
		};
		var o={
			name:"changeSupplier",
			callbacks:{
				successFn: successFn,
				successScope: this
			},
			modal:true
		};
		this._getDc_("fuelOrderLocation").doRpcData(o);
	}
	
	/**
	 * On-Click handler for button btnCancelRemarkSuppWdw
	 */
	,onBtnCancelRemarkSuppWdw: function() {
		this._getDc_("history").doCancel();
		this._getWindow_("wdwRemarksSupp").close();
	}
	
	/**
	 * On-Click handler for button btnSupplierCancel
	 */
	,onBtnSupplierCancel: function() {
		this._getWindow_("wdwSupplier").close();
	}
	
	/**
	 * On-Click handler for button btnCloseNoteWdw
	 */
	,onBtnCloseNoteWdw: function() {
		this._getDc_("note").doCancel();
		this._getWindow_("noteWdw").close();
	}
	
	/**
	 * On-Click handler for button btnSaveCloseNoteWdw
	 */
	,onBtnSaveCloseNoteWdw: function() {
		this.saveCloseNote();
		this._getDc_("note").doReloadPage();
	}
	
	/**
	 * On-Click handler for button helpWdw
	 */
	,onHelpWdw: function() {
		this.openHelp();
	}
	
	/**
	 * On-Click handler for button cancelFlightEvent
	 */
	,onCancelFlightEvent: function() {
		this.cancelFlightEvent();
	}
	
	/**
	 * On-Click handler for button resetFlightEvent
	 */
	,onResetFlightEvent: function() {
		this.resetFlightEvent();
	}
	
	,showNoteWdw: function() {	
		this._getWindow_("noteWdw").show();
	}
	
	,onNoteWdwClose: function() {	
		this._getDc_("note").doCancel();
	}
	
	,onShowUi: function() {	
		var bundle = "atraxo.mod.ops";
		var frame = "atraxo.ops.ui.extjs.frame.FuelOrder_Ui";
		getApplication().showFrame(frame,{
			url:Main.buildUiPath(bundle, frame, false),
			params: {
				id: this._getDc_("fuelOrderLocation").getRecord().get("id")
			},
			callback: function (params) {
				this.reloadFuelOrderLoc(params);
			}
		});
	}
	
	,_when_called_to_edit: function(params) {
		
						var dc = this._getDc_("fuelOrderLocation");
						dc.doClearQuery();
						dc.setFilterValue("id", params.fuelOrderLocationId);
						dc.setParamValue("fuelOrderStatus", params.fuelOrderStatus);
						dc.doQuery();
	}
	
	,_refreshFuelOrder_: function() {
		
						var frameName = "atraxo.ops.ui.extjs.frame.FuelOrder_Ui";
						var frameToGet = getApplication().getFrameInstance(frameName);
						var dcToGet = frameToGet._getDc_("fuelOrder");
						dcToGet.doReloadPage();
	}
	
	,_when_called_from_delivery_notes_: function(params) {
		
		
						var fuelOrderLocationDc = this._getDc_("fuelOrderLocation");
						var dc = this._getDc_("flightEvent");
		
						this._showStackedViewElement_("main", "canvas3");
		
						// Dan: Because the dc is a child of another dc, before setting new filter values we must clear the 
						// context filter values (is this case: the "invoiceId" previously set at "link-to invoice ( invoiceId : id )")
		
						dc.doEmptyCtxFilter();
						dc.doClearAllFilters();
		
						dc.setFilterValue("id", params.id);
		
						fuelOrderLocationDc.doQuery();
						dc.doQuery();
		
	}
	
	,_when_called_from_fuel_event: function(params) {
		
		
						var fuelOrderLocationDc = this._getDc_("fuelOrderLocation");				
						this._showStackedViewElement_("main", "canvas1");
						fuelOrderLocationDc.doEmptyCtxFilter();
						fuelOrderLocationDc.doClearAllFilters();
						fuelOrderLocationDc.setFilterValue("id", params.id);
						fuelOrderLocationDc.doQuery();
		
	}
	
	,_afterDefineDcs_: function() {
			
					var fuelOrderLocation = this._getDc_("fuelOrderLocation");
			
					fuelOrderLocation.on("showWindow", function(dc) {
						this._getDc_("contract").doQuery();
						this._getWindow_("wdwSupplier").show(undefined, function(){
						 	this.onShowSupplierWindow();
						},this);	
					} , this );
		
					//********* Note **********//
		
					var note = this._getDc_("note");
		
					note.on("afterDoNew", function (dc) {
						dc.setEditMode();
					    this.showNoteWdw();
					}, this);
		
					note.on("OnEditIn", function(dc) {
					   this._getWindow_("noteWdw").show();
					}, this);
					
					note.on("afterDoSaveSuccess", function(dc, ajaxResult) {
					    if (ajaxResult.options.options.doNewAfterSave === true) {
					        dc.doNew();
					    } else if (ajaxResult.options.options.doCloseNoteWindowAfterSave === true) {
					        this._getWindow_("noteWdw").close();
					    }
					}, this);
		
					fuelOrderLocation.on("onEditOut", function (dc) {
					    this.onShowUi();
					}, this);
		
	}
	
	,onShowSupplierWindow: function() {
		
					var wdwSupplier = this._getWindow_("wdwSupplier");
					var btnSupplierSelect = Ext.ComponentQuery.query("[name=btnSupplierSelect]")[0];
					var supplierGrid = this._get_("suppList").getView();
					var store = this._getDc_("contract").store;
		
					store.on("load", function(ds) {
						if (store.getCount() == 0) {
							btnSupplierSelect.setDisabled(true);
						}
						else {
							btnSupplierSelect.setDisabled(false);
						}
					});
		
					supplierGrid.getSelectionModel().on("selectionchange", function(sm, selectedRows, opts) {
					    if (selectedRows.length == 1) {
							btnSupplierSelect.setDisabled(false);
						}
						else {
							btnSupplierSelect.setDisabled(true);
						}
					}, this);
		
	}
	
	,selectSupplier: function() {
		
						var folDc = this._getDc_("fuelOrderLocation");
						var contractRec = this._getDc_("contract").getRecord();
						folDc.setParamValue("newContractId", contractRec.getId());
	}
	
	,saveCloseNote: function() {
		
					var note = this._getDc_("note");
					note.doSave({doCloseNoteWindowAfterSave: true });
	}
	
	,openHelp: function() {
		
					var url = Main.urlHelp+"/FuelOrders.html";
					window.open( url, "SONE_Help");
	}
	
	,rescheduleWarning: function() {
		
					var warningMessage = this._getDc_("flightEvent").getParamValue("warningMessage");
					if(warningMessage != ""){
						Main.warning(warningMessage);
					}
	}
	
	,setupHistoryWindow: function(curentDc,theWindow,theForm,saveBtn,cancelBtn,action,label,title,historyDc,status) {
		
					var window = this._getWindow_(theWindow);
					if(historyDc.getRecord() != null ){
						historyDc.getRecord().reject();
					}
					var remarksField = this._get_(theForm)._get_("remarks");
					var dc = this._getDc_(curentDc);
		
					var saveButton = this._get_(saveBtn);
					var cancelButton = this._get_(cancelBtn);
						
					saveButton.setDisabled(true);
					cancelButton.setDisabled(false);
					dc.setParamValue("action", action);			
					dc.setParamValue("remarks", "");
					if(status != null )dc.setParamValue("paramStatus", status);
						
					remarksField.setFieldLabel(label);
					remarksField.setValue("");
					window.setTitle(title);
		
					remarksField.on("change", function(field) {
						if (field.value.length > 0) {
							saveButton.setDisabled(false);
						} else {
						    saveButton.setDisabled(true);
						}
					});
					
					var f = this._get_(theForm);
					if (f.disabled) {
						f.enable();
					}
		
					window.show();
	}
	
	,rescheduleFlightEvent: function() {
		
						var label = "You are about to reschedule the flight event.<br/> Please provide a reason for doing the change.";
						var title = "Remark";
						var action = "Rescheduled";				
						this.setupHistoryWindow("flightEvent", "wdwRemarks", "Remark", "btnSaveCloseRemarkWdw", "btnCancelRemarkWdw", action, label, title,this._getDc_("flightEventHistory"),null );	
	}
	
	,cancelFlightEvent: function() {
		
						var label 	= "You are about to cancel the flight event.<br/> Please provide a reason for doing the change.";
						var title 	= "Remark";
						var action 	= __TYPES__.flightEvent.status.canceled;	
						var status 	= __TYPES__.flightEvent.status.canceled;			
						this.setupHistoryWindow("flightEvent", "wdwCancelFlightEventRemarks", "RemarkCancel", "btnSaveCloseCancelFlightEventRemarkWdw", "btnCancelCancelFlightEventRemarkWdw", action, label, title,this._getDc_("flightEventHistory"), status );	
	}
	
	,resetFlightEvent: function() {
		
						var label 	= "You are about to reset the flight event.<br/> Please provide a reason for doing the change.";
						var title 	= "Remark";
						var action 	= "Reseted";	
						var status 	= __TYPES__.flightEvent.status.scheduled;				
						this.setupHistoryWindow("flightEvent", "wdwResetFlightEventRemarks", "RemarkReset", "btnSaveCloseResetFlightEventRemarkWdw", "btnCancelResetlFlightEventRemarkWdw", action, label, title,this._getDc_("flightEventHistory"),status );	
	}
	
	,openSelectSuppWdw: function() {
		
						var label = "You are about to change the supplier.<br/> Please provide a reason for doing the change.";
						var title = "Remark";
						var action = "Supplier Change";			
						this.setupHistoryWindow("fuelOrderLocation", "wdwRemarksSupp", "SuppHistory", "btnSelectSuppRemarkWdw", "btnCancelRemarkSuppWdw", action, label , title,this._getDc_("history") );	
	}
	
	,displayWarningMessage: function(dc) {
		
						var warningMsg = dc.getRecord().get("warningMessage");
						if (Ext.isEmpty(warningMsg)) {
							return;
						}
						var arrMsg = warningMsg.split("||");
						if (arrMsg.length == 2) {
							Main.warning(arrMsg[1]);
						}
	}
	
	,canReset: function(dc) {
		
						var canceledFlightEvent 	= __TYPES__.flightEvent.status.canceled;
						var orderStatus 			= this._getDc_("fuelOrderLocation").params.get("fuelOrderStatus");
						var canceledOrder			= __TYPES__.fuelOrder.status.canceled;
						var rejectedFuelOrder 		= __TYPES__.fuelOrder.status.rejected;
						return dc.record.data.status == canceledFlightEvent && orderStatus != canceledOrder && orderStatus != rejectedFuelOrder;
	}
	
	,canCancel: function(dc) {
		 
						var data 				= dc.record.data;			
						var status 				= data.status;
						var scheduledStatus 	= __TYPES__.flightEvent.status.scheduled;
		
						var invoiceStatus 		= dc.record.data.invoiceStatus;
						var awaitingPayment 	= __TYPES__.fuelTicket.invoiceStatus.awaitingPayment;
						var paid				= __TYPES__.fuelTicket.invoiceStatus.paid;
						var billStatus			= dc.record.data.billStatus;
						var awaitingPaymentBill	= __TYPES__.fuelTicket.billStatus.awaitingPayment;
						var paidBill			= __TYPES__.fuelTicket.billStatus.paid;
		
						return status == scheduledStatus && invoiceStatus != awaitingPayment && invoiceStatus != paid && billStatus != awaitingPaymentBill && billStatus != paidBill;
	}
});
