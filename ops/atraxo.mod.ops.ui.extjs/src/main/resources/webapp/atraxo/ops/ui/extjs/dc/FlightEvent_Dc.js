/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FlightEvent_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ops.ui.extjs.ds.FlightEvent_DsParam,
	recordModel: atraxo.ops.ui.extjs.ds.FlightEvent_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FlightEvent_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FlightEvent_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addDateTimeField({name:"upliftDate", dataIndex:"upliftDate"})
			.addNumberField({name:"quantity", dataIndex:"quantity", sysDec:"dec_unit", maxLength:19})
			.addTextField({ name:"unit", dataIndex:"unitCode", maxLength:2})
			.addBooleanField({ name:"perCaptain", dataIndex:"perCaptain"})
			.addTextField({ name:"destination", dataIndex:"destCode", maxLength:25})
			.addCombo({ xtype:"combo", name:"status", dataIndex:"status", store:[ __OPS_TYPE__.FlightEventStatus._SCHEDULED_, __OPS_TYPE__.FlightEventStatus._FULFILLED_, __OPS_TYPE__.FlightEventStatus._CANCELED_, __OPS_TYPE__.FlightEventStatus._INVOICED_]})
			.addNumberField({name:"events", dataIndex:"events", maxLength:11})
			.addNumberField({name:"totalQuantity", dataIndex:"totalQuantity", sysDec:"dec_unit", maxLength:19})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FlightEvent_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FlightEvent_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDateColumn({ name:"upliftDate", dataIndex:"upliftDate", width:150, _mask_: Masks.DATETIME})
		.addNumberColumn({ name:"quantity", dataIndex:"quantity", width:120, sysDec:"dec_unit",  renderer:function(value) {return Ext.util.Format.number(Math.round(value), Main.getNumberFormat(2))}})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:40})
		.addBooleanColumn({ name:"perCaptain", dataIndex:"perCaptain", width:250})
		.addTextColumn({ name:"destination", dataIndex:"destCode", width:90})
		.addDateColumn({ name:"arrivalDate", dataIndex:"arrivalDate", width:180, _mask_: Masks.DATETIME})
		.addDateColumn({ name:"departerDate", dataIndex:"departerDate", width:180, _mask_: Masks.DATETIME})
		.addTextColumn({ name:"flightNo", dataIndex:"flightNo", width:75})
		.addTextColumn({ name:"registration", dataIndex:"registration", width:110})
		.addTextColumn({ name:"acType", dataIndex:"acTypeCode", width:110})
		.addTextColumn({ name:"product", dataIndex:"product", width:70})
		.addTextColumn({ name:"operator", dataIndex:"operatorCode", width:70})
		.addTextColumn({ name:"handler", dataIndex:"handlerCode", width:80})
		.addTextColumn({ name:"captain", dataIndex:"captain", width:120})
		.addTextColumn({ name:"status", dataIndex:"status", width:100})
		.addTextColumn({ name:"fuelRequestCode", dataIndex:"fuelRequestCode", hidden:true, width:100})
		.addTextColumn({ name:"eventType", dataIndex:"eventType", hidden:true, width:100})
		.addTextColumn({ name:"operationType", dataIndex:"operationType", hidden:true, width:100})
		.addTextColumn({ name:"aircraftCallSign", dataIndex:"aircraftCallSign", hidden:true, width:100})
		.addTextColumn({ name:"aircraftHomeBase", dataIndex:"aircraftHBCode", hidden:true, width:100})
		.addTextColumn({ name:"arrivalFrom", dataIndex:"arrivalFromCode", hidden:true, width:100})
		.addNumberColumn({ name:"events", dataIndex:"events", width:80})
		.addNumberColumn({ name:"totalQuantity", dataIndex:"totalQuantity", width:100, sysDec:"dec_unit"})
		.addTextColumn({ name:"billStatus", dataIndex:"billStatus", hidden:true, width:100})
		.addTextColumn({ name:"invoiceStatus", dataIndex:"invoiceStatus", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= GRID: ListRequest ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FlightEvent_Dc$ListRequest", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FlightEvent_Dc$ListRequest",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"airport", dataIndex:"locCode", width:80})
		.addTextColumn({ name:"operator", dataIndex:"operatorCode", width:70})
		.addTextColumn({ name:"registration", dataIndex:"registration", width:100})
		.addTextColumn({ name:"acType", dataIndex:"acTypeCode", width:100})
		.addTextColumn({ name:"destination", dataIndex:"destCode", width:100})
		.addDateColumn({ name:"arrivalDate", dataIndex:"arrivalDate", width:180, _mask_: Masks.DATETIME})
		.addDateColumn({ name:"departerDate", dataIndex:"departerDate", width:200, _mask_: Masks.DATETIME})
		.addDateColumn({ name:"upliftDate", dataIndex:"upliftDate", width:150, _mask_: Masks.DATETIME})
		.addNumberColumn({ name:"quantity", dataIndex:"quantity", width:120, sysDec:"dec_unit",  renderer:function(value) {return Ext.util.Format.number(Math.round(value), Main.getNumberFormat(2))}})
		.addTextColumn({ name:"unit", dataIndex:"unitCode", width:40})
		.addTextColumn({ name:"handler", dataIndex:"handlerCode", width:80})
		.addTextColumn({ name:"eventType", dataIndex:"eventType", width:100})
		.addTextColumn({ name:"operationType", dataIndex:"operationType", width:110})
		.addTextColumn({ name:"status", dataIndex:"status", width:100})
		.addTextColumn({ name:"fuelRequestCode", dataIndex:"fuelRequestCode", hidden:true, width:100})
		.addBooleanColumn({ name:"perCaptain", dataIndex:"perCaptain", hidden:true, width:100})
		.addTextColumn({ name:"aircraftCallSign", dataIndex:"aircraftCallSign", hidden:true, width:100})
		.addTextColumn({ name:"aircraftHomeBase", dataIndex:"aircraftHBCode", hidden:true, width:100})
		.addTextColumn({ name:"arrivalFrom", dataIndex:"arrivalFromCode", hidden:true, width:100})
		.addNumberColumn({ name:"events", dataIndex:"events", width:80})
		.addNumberColumn({ name:"totalQuantity", dataIndex:"totalQuantity", width:100, sysDec:"dec_unit"})
		.addTextColumn({ name:"billStatus", dataIndex:"billStatus", hidden:true, width:100})
		.addTextColumn({ name:"invoiceStatus", dataIndex:"invoiceStatus", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: QuotationEdit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FlightEvent_Dc$QuotationEdit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FlightEvent_Dc$QuotationEdit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"departure", bind:"{d.locCode}", dataIndex:"locCode", allowBlank:false, xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ]})
		.addLov({name:"destination", bind:"{d.destCode}", dataIndex:"destCode", xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:110,
			retFieldMapping: [{lovField:"id", dsField: "destId"} ]})
		.addLov({name:"operator", bind:"{d.operatorCode}", dataIndex:"operatorCode", allowBlank:false, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "operatorId"} ]})
		.addTextField({ name:"flightNo", bind:"{d.flightNo}", dataIndex:"flightNo", maxLength:4, labelWidth:120, minLength:0, listeners:{keydown: { scope: this, fn: function(field, e) { this._restrictNumeric_(field, e) }}}, enableKeyEvents:true})
		.addTextField({ name:"registration", bind:"{d.registration}", dataIndex:"registration", maxLength:10, labelWidth:110})
		.addLov({name:"acType", bind:"{d.acTypeCode}", dataIndex:"acTypeCode", xtype:"fmbas_AcTypesLov_Lov", maxLength:4,
			retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ,{lovField:"tankCapacity", dsField: "tankCap"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			fpchange:{scope:this, fn:this._setVal_}
		}})
		.addCombo({ xtype:"combo", name:"eventType", bind:"{d.eventType}", dataIndex:"eventType", allowBlank:false, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:120,listeners:{
			fpchange:{scope:this, fn:this._alertEvent_}
		}})
		.addCombo({ xtype:"combo", name:"operationType", bind:"{d.operationType}", dataIndex:"operationType", allowBlank:false, store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_], labelWidth:110,listeners:{
			fpchange:{scope:this, fn:this._alertOperation_}
		}})
		.addLov({name:"handler", bind:"{d.handlerCode}", dataIndex:"handlerCode", xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "handlerId"} ],
			filterFieldMapping: [{lovField:"fixedBaseOperator", value: "true"} ]})
		.addTextField({ name:"captain", bind:"{d.captain}", dataIndex:"captain", maxLength:64, labelWidth:120})
		.addTextField({ name:"aircraftCallSign", bind:"{d.aircraftCallSign}", dataIndex:"aircraftCallSign", maxLength:32, labelWidth:110})
		.addLov({name:"arrivalFrom", bind:"{d.arrivalFromCode}", dataIndex:"arrivalFromCode", xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "arrivalFromId"} ]})
		.addLov({name:"aircraftHB", bind:"{d.aircraftHBCode}", dataIndex:"aircraftHBCode", xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "aircraftHBId"} ]})
		.addDateTimeField({name:"upliftDate", bind:"{d.upliftDate}", dataIndex:"upliftDate", allowBlank:false, labelWidth:150})
		.addDateTimeField({name:"arrivalDate", bind:"{d.arrivalDate}", dataIndex:"arrivalDate", labelWidth:150})
		.addDateTimeField({name:"departerDate", bind:"{d.departerDate}", dataIndex:"departerDate", labelWidth:150})
		.addBooleanField({ name:"timeRef", bind:"{d.timeReference}", dataIndex:"timeReference", labelWidth:150})
		.addBooleanField({ name:"perCaptain", bind:"{d.perCaptain}", dataIndex:"perCaptain", labelWidth:210,listeners:{
			change:{scope:this, fn:this._setDisable_}
		}})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", _enableFn_: this._checkStatus_, width:172, sysDec:"dec_unit", maxLength:19})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: this._checkStatus_, width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.product}", dataIndex:"product", store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_]})
		.addNumberField({name:"events", bind:"{d.events}", dataIndex:"events", allowBlank:false, maxLength:11, labelWidth:110})
		.addHiddenField({ name:"eventTp", bind:"{d.eventTp}", dataIndex:"eventTp"})
		.addHiddenField({ name:"operationTp", bind:"{d.operationTp}", dataIndex:"operationTp"})
		.addHiddenField({ name:"tankCap", bind:"{d.tankCap}", dataIndex:"tankCap"})
		.addDisplayFieldText({ name:"flightDetails", bind:"{d.flightDetails}", dataIndex:"flightDetails", maxLength:64})
		.addDisplayFieldText({ name:"scheduleDetails", bind:"{d.scheduleDetails}", dataIndex:"scheduleDetails", maxLength:64})
		.addDisplayFieldText({ name:"fuelingDetails", bind:"{d.fuelingDetails}", dataIndex:"fuelingDetails", maxLength:64})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("departure"),this._getConfig_("destination"),this._getConfig_("operator")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("flightNo"),this._getConfig_("registration"),this._getConfig_("acType")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("eventType"),this._getConfig_("operationType"),this._getConfig_("handler")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("captain"),this._getConfig_("aircraftCallSign"),this._getConfig_("arrivalFrom")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantity"),this._getConfig_("unit")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("aircraftHB"),this._getConfig_("events")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"row", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"col1", width:900, layout:"anchor"})
		.addPanel({ name:"col2", width:900, layout:"anchor"})
		.addPanel({ name:"col3", width:900, layout:"anchor"})
		.addPanel({ name:"col4", width:900, layout:"anchor"})
		.addPanel({ name:"col5", width:900, layout:"anchor"})
		.addPanel({ name:"col6", width:900, layout:"anchor"})
		.addPanel({ name:"col7", width:300, layout:"anchor"})
		.addPanel({ name:"col8", width:300,  style:"margin-left: 100px", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "row"])
		.addChildrenTo("row", ["col7", "col8"])
		.addChildrenTo("col1", ["flightDetails"])
		.addChildrenTo("col2", ["row1"])
		.addChildrenTo("col3", ["row2"])
		.addChildrenTo("col4", ["row3"])
		.addChildrenTo("col5", ["row4"])
		.addChildrenTo("col6", ["row6"])
		.addChildrenTo("col7", ["scheduleDetails", "arrivalDate", "departerDate", "upliftDate", "timeRef"])
		.addChildrenTo("col8", ["fuelingDetails", "perCaptain", "row5", "product", "eventTp", "operationTp", "tankCap"]);
	},
	/* ==================== Business functions ==================== */
	
	_restrictNumeric_: function(field,e) {
		
						var code = e.browserEvent.keyCode;
						var keyCodes = [8,9,39,37,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105];
				        if (keyCodes.indexOf(code) < 0) {
				            e.stopEvent();
				        }
	},
	
	_checkStatus_: function() {
		
						var rec = this._controller_.getRecord();
						var status = rec.get("status");
						var result = false;
						var record = this._get_("perCaptain");
						var recVal = record.getValue();
		
						if (status !== "Submitted") {
							result = true;
							if(recVal) {
								result = false;
							}
						}
						return result;
	},
	
	_setVal_: function() {
		
		
						var perCaptain = this._get_("perCaptain");
						var quantity = this._get_("quantity");
						var tankCap = this._get_("tankCap");
						var tankCapVal = tankCap.getValue();
		
						if (perCaptain.checked === true) {
							quantity.setValue(tankCapVal);
						}	
		
	},
	
	_setDisable_: function() {
		
						var rec = this._get_("perCaptain");
						var recVal = rec.getValue();
						var quantity = this._get_("quantity");
						var unit = this._get_("unit");
						var acType = this._get_("acType");
						var tankCap = this._get_("tankCap");
						var tankCapVal = tankCap.getValue();
		
						var record = this._controller_.getRecord();
						var status = record.get("status");
						
						if (recVal) {
							quantity.setReadOnly(true);
							unit.setReadOnly(true);
							if(Ext.isEmpty(acType.getValue())) { 
								quantity.setValue("");
							}
							else {
								quantity.setValue(tankCapVal);
							}
						}
						else {
							if (status !== "Submitted") {
								quantity.setReadOnly(false);
								unit.setReadOnly(false);
								if(Ext.isEmpty(quantity.getValue()) && Ext.isEmpty(acType.getValue())){
									quantity.setValue("");
								}
							}
							
						}
	},
	
	_alertEvent_: function() {
		
						var field = this._get_("eventType");
						var fieldVal = field.getValue();
						var event = this._get_("eventTp");
						var eventVal = event.getValue();
						if(fieldVal!==eventVal) {
							Ext.Msg.show({
							title : "Note",
							msg : "You are changing the event type which was inherited from the Fuel Quote Location",
							icon : Ext.MessageBox.INFO,
							buttons : Ext.MessageBox.OK,
							scope : this });
							}
	},
	
	_alertOperation_: function() {
		
						var field = this._get_("operationType");
						var fieldVal = field.getValue();
						var event = this._get_("operationTp");
						var eventVal = event.getValue();
						if(fieldVal!==eventVal) {
							Ext.Msg.show({
							title : "Note",
							msg : "You are changing the operation type which was inherited from the Fuel Quote Location",
							icon : Ext.MessageBox.INFO,
							buttons : Ext.MessageBox.OK,
							scope : this });
							}								
	}
});

/* ================= EDIT FORM: RequestEdit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FlightEvent_Dc$RequestEdit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FlightEvent_Dc$RequestEdit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"departure", bind:"{d.locCode}", dataIndex:"locCode", _enableFn_: function(dc, rec) { return this._isEnabled_; } , allowBlank:false, xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ]})
		.addLov({name:"destination", bind:"{d.destCode}", dataIndex:"destCode", _enableFn_: function(dc, rec) { return this._isEnabled_; } , xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:110,
			retFieldMapping: [{lovField:"id", dsField: "destId"} ]})
		.addLov({name:"operator", bind:"{d.operatorCode}", dataIndex:"operatorCode", _enableFn_: function(dc, rec) { return this._isEnabled_; } , allowBlank:false, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "operatorId"} ]})
		.addTextField({ name:"flightNo", bind:"{d.flightNo}", dataIndex:"flightNo", _enableFn_: function(dc, rec) { return this._isEnabled_; } , labelWidth:120, minLength:0, maxLength:4, listeners:{keydown: { scope: this, fn: function(field, e) { this._restrictNumeric_(field, e) }}}, enableKeyEvents:true})
		.addTextField({ name:"registration", bind:"{d.registration}", dataIndex:"registration", _enableFn_: function(dc, rec) { return this._isEnabled_; } , maxLength:10, labelWidth:110})
		.addLov({name:"acType", bind:"{d.acTypeCode}", dataIndex:"acTypeCode", _enableFn_: function(dc, rec) { return this._isEnabled_; } , xtype:"fmbas_AcTypesLov_Lov", maxLength:4,
			retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ,{lovField:"tankCapacity", dsField: "tankCap"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ],listeners:{
			fpchange:{scope:this, fn:this._setVal_}
		}})
		.addCombo({ xtype:"combo", name:"eventType", bind:"{d.eventType}", dataIndex:"eventType", _enableFn_: function(dc, rec) { return this._isEnabled_; } , allowBlank:false, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:120})
		.addCombo({ xtype:"combo", name:"operationType", bind:"{d.operationType}", dataIndex:"operationType", _enableFn_: function(dc, rec) { return this._isEnabled_; } , allowBlank:false, store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_], labelWidth:110})
		.addLov({name:"handler", bind:"{d.handlerCode}", dataIndex:"handlerCode", _enableFn_: function(dc, rec) { return this._isEnabled_; } , xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "handlerId"} ],
			filterFieldMapping: [{lovField:"fixedBaseOperator", value: "true"} ]})
		.addTextField({ name:"captain", bind:"{d.captain}", dataIndex:"captain", _enableFn_: function(dc, rec) { return this._isEnabled_; } , maxLength:64, labelWidth:120})
		.addTextField({ name:"aircraftCallSign", bind:"{d.aircraftCallSign}", dataIndex:"aircraftCallSign", _enableFn_: function(dc, rec) { return this._isEnabled_; } , maxLength:32, labelWidth:110})
		.addLov({name:"arrivalFrom", bind:"{d.arrivalFromCode}", dataIndex:"arrivalFromCode", _enableFn_: function(dc, rec) { return this._isEnabled_; } , xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "arrivalFromId"} ]})
		.addLov({name:"aircraftHB", bind:"{d.aircraftHBCode}", dataIndex:"aircraftHBCode", _enableFn_: function(dc, rec) { return this._isEnabled_; } , xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "aircraftHBId"} ]})
		.addDateTimeField({name:"upliftDate", bind:"{d.upliftDate}", dataIndex:"upliftDate", _enableFn_: function(dc, rec) { return this._isEnabled_; } , allowBlank:false, labelWidth:150})
		.addDateTimeField({name:"arrivalDate", bind:"{d.arrivalDate}", dataIndex:"arrivalDate", _enableFn_: function(dc, rec) { return this._isEnabled_; } , labelWidth:150})
		.addDateTimeField({name:"departerDate", bind:"{d.departerDate}", dataIndex:"departerDate", _enableFn_: function(dc, rec) { return this._isEnabled_; } , labelWidth:150})
		.addBooleanField({ name:"timeRef", bind:"{d.timeReference}", dataIndex:"timeReference", _enableFn_: function(dc, rec) { return this._isEnabled_; } , labelWidth:150})
		.addBooleanField({ name:"perCaptain", bind:"{d.perCaptain}", dataIndex:"perCaptain", _enableFn_: function(dc, rec) { return this._isEnabled_; } , labelWidth:210,listeners:{
			change:{scope:this, fn:this._setDisable_}
		}})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", _enableFn_: this._checkStatus_, width:172, sysDec:"dec_unit", maxLength:19})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", _enableFn_: this._checkStatus_, width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.product}", dataIndex:"product", _enableFn_: function(dc, rec) { return this._isEnabled_; } , store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_]})
		.addNumberField({name:"events", bind:"{d.events}", dataIndex:"events", _enableFn_: function(dc, rec) { return this._isEnabled_; } , allowBlank:false, maxLength:11, labelWidth:110})
		.addHiddenField({ name:"eventTp", bind:"{d.eventTp}", dataIndex:"eventTp"})
		.addHiddenField({ name:"operationTp", bind:"{d.operationTp}", dataIndex:"operationTp"})
		.addHiddenField({ name:"tankCap", bind:"{d.tankCap}", dataIndex:"tankCap"})
		.addDisplayFieldText({ name:"flightDetails", bind:"{d.flightDetails}", dataIndex:"flightDetails", maxLength:64, labelWidth:300, labelAlign:"left"})
		.addDisplayFieldText({ name:"scheduleDetails", bind:"{d.scheduleDetails}", dataIndex:"scheduleDetails", maxLength:64, labelWidth:300, labelAlign:"left"})
		.addDisplayFieldText({ name:"fuelingDetails", bind:"{d.fuelingDetails}", dataIndex:"fuelingDetails", maxLength:64, labelWidth:300, labelAlign:"left"})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("departure"),this._getConfig_("destination"),this._getConfig_("operator")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("flightNo"),this._getConfig_("registration"),this._getConfig_("acType")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("eventType"),this._getConfig_("operationType"),this._getConfig_("handler")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("captain"),this._getConfig_("aircraftCallSign"),this._getConfig_("arrivalFrom")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantity"),this._getConfig_("unit")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("aircraftHB"),this._getConfig_("events")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"row", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"col1", width:900, layout:"anchor"})
		.addPanel({ name:"col2", width:900, layout:"anchor"})
		.addPanel({ name:"col3", width:900, layout:"anchor"})
		.addPanel({ name:"col4", width:900, layout:"anchor"})
		.addPanel({ name:"col5", width:900, layout:"anchor"})
		.addPanel({ name:"col6", width:900, layout:"anchor"})
		.addPanel({ name:"col7", width:300, layout:"anchor"})
		.addPanel({ name:"col8", width:300,  style:"margin-left: 100px", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "row"])
		.addChildrenTo("row", ["col7", "col8"])
		.addChildrenTo("col1", ["flightDetails"])
		.addChildrenTo("col2", ["row1"])
		.addChildrenTo("col3", ["row2"])
		.addChildrenTo("col4", ["row3"])
		.addChildrenTo("col5", ["row4"])
		.addChildrenTo("col6", ["row6"])
		.addChildrenTo("col7", ["scheduleDetails", "arrivalDate", "departerDate", "upliftDate", "timeRef"])
		.addChildrenTo("col8", ["fuelingDetails", "perCaptain", "row5", "product", "eventTp", "operationTp", "tankCap"]);
	},
	/* ==================== Business functions ==================== */
	
	_restrictNumeric_: function(field,e) {
		
						var code = e.browserEvent.keyCode;
						var keyCodes = [8,9,39,37,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105];
				        if (keyCodes.indexOf(code) < 0) {
				            e.stopEvent();
				        }
	},
	
	_checkStatus_: function() {
		
						var rec = this._controller_.getRecord();
						var status = rec.get("fuelRequestStatus");
						var result = false;
						var record = this._get_("perCaptain");
						var recVal = record.getValue();
		
						if (status !== "New" || status !== "Processed") {
							result = true;
							if (recVal) {
								result = false;
							}
						}
						return result && this._isEnabled_();
	},
	
	_afterApplyStates_: function() {
		
						var rec = this._get_("perCaptain");
						var recVal = rec.getValue();
						var quantity = this._get_("quantity");
						var acType = this._get_("acType");
						var tankCap = this._get_("tankCap");
						var tankCapVal = tankCap.getValue();
						
						if (recVal) {
							if(acType.getValue()===null) { 
								quantity.setValue("");
							}
							else {
								quantity.setValue(tankCapVal);
							}
						}
						else {
							if(quantity.getValue()===null){
								if(acType.getValue()==null) {
									quantity.setValue("");
								}
							}
						}
	},
	
	_setVal_: function() {
		
		
						var perCaptain = this._get_("perCaptain");
						var quantity = this._get_("quantity");
						var tankCap = this._get_("tankCap");
						var tankCapVal = tankCap.getValue();
		
						if (perCaptain.checked === true) {
							quantity.setValue(tankCapVal);
						}	
		
	},
	
	_setDisable_: function() {
		
						var rec = this._get_("perCaptain");
						var recVal = rec.getValue();
						var quantity = this._get_("quantity");
						var unit = this._get_("unit");
						var acType = this._get_("acType");
						var tankCap = this._get_("tankCap");
						var tankCapVal = tankCap.getValue();
						
						if (recVal) {
							quantity.setReadOnly(true);
							unit.setReadOnly(true);
							if(acType.getValue()==null) { 
								quantity.setValue("");
							}
							else {
								quantity.setValue(tankCapVal);
							}
						}
						else {
							quantity.setReadOnly(false);
							unit.setReadOnly(false);
							if(quantity.getValue()==null){
								if(acType.getValue()==null) {
									quantity.setValue("");
								}
							}
						}
	},
	
	_isEnabled_: function() {
		
						var record = this._controller_.getRecord();
						var invoiceStatus 		= record.data.invoiceStatus;
						var awaitingPayment 	=  __TYPES__.fuelTicket.invoiceStatus.awaitingPayment;
						var paid 				=  __TYPES__.fuelTicket.invoiceStatus.paid;
						var billStatus			= record.data.billStatus;
						var awaitingPaymentBill	= __TYPES__.fuelTicket.billStatus.awaitingPayment;
						var paidBill			= __TYPES__.fuelTicket.billStatus.paid;
						return invoiceStatus != awaitingPayment && invoiceStatus != paid && billStatus != awaitingPaymentBill && billStatus != paidBill;
	}
});

/* ================= EDIT FORM: OrderEdit ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FlightEvent_Dc$OrderEdit", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FlightEvent_Dc$OrderEdit",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addLov({name:"departure", bind:"{d.locCode}", dataIndex:"locCode", noEdit:true , allowBlank:false, xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ]})
		.addLov({name:"destination", bind:"{d.destCode}", dataIndex:"destCode", noEdit:true , xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:110,
			retFieldMapping: [{lovField:"id", dsField: "destId"} ]})
		.addLov({name:"operator", bind:"{d.operatorCode}", dataIndex:"operatorCode", noEdit:true , allowBlank:false, xtype:"fmbas_CustomerCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "operatorId"} ]})
		.addTextField({ name:"flightNo", bind:"{d.flightNo}", dataIndex:"flightNo", noEdit:true , labelWidth:120, minLength:0, maxLength:4, listeners:{keydown: { scope: this, fn: function(field, e) { this._restrictNumeric_(field, e) }}}, enableKeyEvents:true})
		.addTextField({ name:"registration", bind:"{d.registration}", dataIndex:"registration", noEdit:true , maxLength:10, labelWidth:110})
		.addLov({name:"acType", bind:"{d.acTypeCode}", dataIndex:"acTypeCode", noEdit:true , xtype:"fmbas_AcTypesLov_Lov", maxLength:4,
			retFieldMapping: [{lovField:"id", dsField: "acTypeId"} ,{lovField:"tankCapacity", dsField: "tankCap"} ],
			filterFieldMapping: [{lovField:"active", value: "true"} ]})
		.addCombo({ xtype:"combo", name:"eventType", bind:"{d.eventType}", dataIndex:"eventType", noEdit:true , allowBlank:false, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:120})
		.addCombo({ xtype:"combo", name:"operationType", bind:"{d.operationType}", dataIndex:"operationType", noEdit:true , allowBlank:false, store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_], labelWidth:110})
		.addLov({name:"handler", bind:"{d.handlerCode}", dataIndex:"handlerCode", noEdit:true , xtype:"fmbas_SupplierCodeLov_Lov", maxLength:32,
			retFieldMapping: [{lovField:"id", dsField: "handlerId"} ],
			filterFieldMapping: [{lovField:"fixedBaseOperator", value: "true"} ]})
		.addTextField({ name:"captain", bind:"{d.captain}", dataIndex:"captain", noEdit:true , maxLength:64, labelWidth:120})
		.addTextField({ name:"aircraftCallSign", bind:"{d.aircraftCallSign}", dataIndex:"aircraftCallSign", noEdit:true , maxLength:32, labelWidth:110})
		.addLov({name:"arrivalFrom", bind:"{d.arrivalFromCode}", dataIndex:"arrivalFromCode", noEdit:true , xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "arrivalFromId"} ]})
		.addLov({name:"aircraftHB", bind:"{d.aircraftHBCode}", dataIndex:"aircraftHBCode", noEdit:true , xtype:"fmbas_LocationsLov_Lov", maxLength:25, labelWidth:120,
			retFieldMapping: [{lovField:"id", dsField: "aircraftHBId"} ]})
		.addDateTimeField({name:"upliftDate", bind:"{d.upliftDate}", dataIndex:"upliftDate", noEdit:true , allowBlank:false, labelWidth:150})
		.addDateTimeField({name:"arrivalDate", bind:"{d.arrivalDate}", dataIndex:"arrivalDate", noEdit:true , labelWidth:150})
		.addDateTimeField({name:"departerDate", bind:"{d.departerDate}", dataIndex:"departerDate", noEdit:true , labelWidth:150})
		.addBooleanField({ name:"timeRef", bind:"{d.timeReference}", dataIndex:"timeReference", noEdit:true , labelWidth:150})
		.addBooleanField({ name:"perCaptain", bind:"{d.perCaptain}", dataIndex:"perCaptain", noEdit:true , labelWidth:210})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", noEdit:true , width:172, sysDec:"dec_unit", maxLength:19})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:60, xtype:"fmbas_MeasurmentUnitsLov_Lov", maxLength:2, hideLabel:"true",
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.product}", dataIndex:"product", noEdit:true , store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_]})
		.addNumberField({name:"events", bind:"{d.events}", dataIndex:"events", noEdit:true , maxLength:11, labelWidth:110})
		.addNumberField({name:"totalQuantity", bind:"{d.totalQuantity}", dataIndex:"totalQuantity", noEdit:true , sysDec:"dec_unit", maxLength:19})
		.addHiddenField({ name:"eventTp", bind:"{d.eventTp}", dataIndex:"eventTp"})
		.addHiddenField({ name:"operationTp", bind:"{d.operationTp}", dataIndex:"operationTp"})
		.addHiddenField({ name:"tankCap", bind:"{d.tankCap}", dataIndex:"tankCap"})
		.addDisplayFieldText({ name:"flightDetails", bind:"{d.flightDetails}", dataIndex:"flightDetails", noEdit:true , maxLength:64})
		.addDisplayFieldText({ name:"scheduleDetails", bind:"{d.scheduleDetails}", dataIndex:"scheduleDetails", noEdit:true , maxLength:64})
		.addDisplayFieldText({ name:"fuelingDetails", bind:"{d.fuelingDetails}", dataIndex:"fuelingDetails", noEdit:true , maxLength:64})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("departure"),this._getConfig_("destination"),this._getConfig_("operator")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("flightNo"),this._getConfig_("registration"),this._getConfig_("acType")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("eventType"),this._getConfig_("operationType"),this._getConfig_("handler")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("captain"),this._getConfig_("aircraftCallSign"),this._getConfig_("arrivalFrom")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("quantity"),this._getConfig_("unit")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("aircraftHB"),this._getConfig_("events"),this._getConfig_("totalQuantity")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"row", layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}})
		.addPanel({ name:"col1", width:900, layout:"anchor"})
		.addPanel({ name:"col2", width:1200, layout:"anchor"})
		.addPanel({ name:"col3", width:900, layout:"anchor"})
		.addPanel({ name:"col4", width:900, layout:"anchor"})
		.addPanel({ name:"col5", width:900, layout:"anchor"})
		.addPanel({ name:"col6", width:900, layout:"anchor"})
		.addPanel({ name:"col7", width:300, layout:"anchor"})
		.addPanel({ name:"col8", width:300,  style:"margin-left: 100px", layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5", "col6", "row"])
		.addChildrenTo("row", ["col7", "col8"])
		.addChildrenTo("col1", ["flightDetails"])
		.addChildrenTo("col2", ["row1"])
		.addChildrenTo("col3", ["row2"])
		.addChildrenTo("col4", ["row3"])
		.addChildrenTo("col5", ["row4"])
		.addChildrenTo("col6", ["row6"])
		.addChildrenTo("col7", ["scheduleDetails", "arrivalDate", "departerDate", "upliftDate", "timeRef"])
		.addChildrenTo("col8", ["fuelingDetails", "perCaptain", "row5", "product", "eventTp", "operationTp", "tankCap"]);
	},
	/* ==================== Business functions ==================== */
	
	_restrictNumeric_: function(field,e) {
		
						var code = e.browserEvent.keyCode;
						var keyCodes = [8,9,39,37,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105];
				        if (keyCodes.indexOf(code) < 0) {
				            e.stopEvent();
				        }
	}
});

/* ================= EDIT FORM: RescheduleFlightevent ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FlightEvent_Dc$RescheduleFlightevent", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FlightEvent_Dc$RescheduleFlightevent",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addDateTimeField({name:"oldArrivalTime", bind:"{d.arrivalDate}", dataIndex:"arrivalDate", noEdit:true , labelWidth:150})
		.addDateTimeField({name:"oldDepartureDate", bind:"{d.departerDate}", dataIndex:"departerDate", noEdit:true , labelWidth:150})
		.addDateTimeField({name:"oldUpliftDate", bind:"{d.upliftDate}", dataIndex:"upliftDate", noEdit:true , labelWidth:150})
		.addBooleanField({ name:"oldTimeReference", bind:"{d.timeReference}", dataIndex:"timeReference", noEdit:true , labelWidth:150})
		.addDateTimeField({name:"newArrivalTime", bind:"{d.newArrivalDate}", dataIndex:"newArrivalDate", labelWidth:150})
		.addDateTimeField({name:"newDepartureDate", bind:"{d.newDepartureDate}", dataIndex:"newDepartureDate", labelWidth:150})
		.addDateTimeField({name:"newUpliftDate", bind:"{d.newUpliftDate}", dataIndex:"newUpliftDate", allowBlank:false, labelWidth:150})
		.addBooleanField({ name:"newTimeReference", bind:"{d.newTimeReference}", dataIndex:"newTimeReference", labelWidth:150})
		.addDisplayFieldText({ name:"newScheduleTitle", bind:"{d.newSchedule}", dataIndex:"newSchedule", noEdit:true , width:300, maxLength:64, labelWidth:300, labelAlign:"left"})
		.addDisplayFieldText({ name:"oldScheduleTitle", bind:"{d.oldSchedule}", dataIndex:"oldSchedule", noEdit:true , width:300, maxLength:64, labelWidth:300, labelAlign:"left"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true, width:720, layout: {type:"hbox", align:'top', pack:'start', defaultMargins: {right:5, left:5}}, padding:"0 30 5 0"})
		.addPanel({ name:"col1", width:350, layout:"anchor"})
		.addPanel({ name:"col2", width:350, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2"])
		.addChildrenTo("col1", ["newScheduleTitle", "newArrivalTime", "newDepartureDate", "newUpliftDate", "newTimeReference"])
		.addChildrenTo("col2", ["oldScheduleTitle", "oldArrivalTime", "oldDepartureDate", "oldUpliftDate", "oldTimeReference"]);
	}
});

/* ================= EDIT FORM: Remark ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FlightEvent_Dc$Remark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FlightEvent_Dc$Remark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remarks}", paramIndex:"remarks", labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"remarkCol1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						return true;
	}
});
