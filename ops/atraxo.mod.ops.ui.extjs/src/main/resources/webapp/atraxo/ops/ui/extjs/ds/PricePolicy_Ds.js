/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
	
Ext.define("atraxo.ops.ui.extjs.ds.PricePolicy_Ds", {
	extend: 'Ext.data.Model',
	
	statics: {
		ALIAS: "ops_PricePolicy_Ds"
	},
	
	
	validators: {
		name: [{type: 'presence'}],
		validFrom: [{type: 'presence'}],
		validTo: [{type: 'presence'}]
	},
	
	fields: [
		{name:"margin", type:"string", noFilter:true, noSort:true},
		{name:"markup", type:"string", noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"acTypeId", type:"int", allowNull:true},
		{name:"acTypeCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"name", type:"string"},
		{name:"defaultMargin", type:"float", allowNull:true},
		{name:"minimalMargin", type:"float", allowNull:true},
		{name:"defaultMarkup", type:"float", allowNull:true},
		{name:"minimalMarkup", type:"float", allowNull:true},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"active", type:"boolean"},
		{name:"comments", type:"string"},
		{name:"yesNo", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
	
Ext.define("atraxo.ops.ui.extjs.ds.PricePolicy_DsFilter", {
	extend: 'Ext.data.Model',
	
	
	fields: [
		{name:"margin", type:"string", noFilter:true, noSort:true},
		{name:"markup", type:"string", noFilter:true, noSort:true},
		{name:"percent", type:"string", noFilter:true, noSort:true},
		{name:"customerId", type:"int", allowNull:true},
		{name:"customerCode", type:"string"},
		{name:"locationId", type:"int", allowNull:true},
		{name:"locationCode", type:"string"},
		{name:"acTypeId", type:"int", allowNull:true},
		{name:"acTypeCode", type:"string"},
		{name:"unitId", type:"int", allowNull:true},
		{name:"unitCode", type:"string"},
		{name:"currId", type:"int", allowNull:true},
		{name:"currCode", type:"string"},
		{name:"name", type:"string"},
		{name:"defaultMargin", type:"float", allowNull:true},
		{name:"minimalMargin", type:"float", allowNull:true},
		{name:"defaultMarkup", type:"float", allowNull:true},
		{name:"minimalMarkup", type:"float", allowNull:true},
		{name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
		{name:"active", type:"boolean", allowNull:true},
		{name:"comments", type:"string"},
		{name:"yesNo", type:"string", noFilter:true, noSort:true},
		{name:"id", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"refid", type:"string", noSort:true},
		{name:"clientId", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT, noSort:true},
		{name:"createdBy", type:"string", noSort:true},
		{name:"modifiedBy", type:"string", noSort:true},
		{name:"version", type:"int", allowNull:true, noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityAlias", type:"string", noFilter:true, noSort:true, alwaysHidden:true},
		{name:"entityFqn", type:"string", noFilter:true, noSort:true, alwaysHidden:true}
	]
});
