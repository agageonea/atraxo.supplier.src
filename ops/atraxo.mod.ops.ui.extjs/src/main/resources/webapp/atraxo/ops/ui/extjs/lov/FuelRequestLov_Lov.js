/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.lov.FuelRequestLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ops_FuelRequestLov_Lov",
	displayField: "requestCode", 
	_columns_: ["requestCode"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{requestCode}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.ops.ui.extjs.ds.FuelRequestLov_Ds
});
