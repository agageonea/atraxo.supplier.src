/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.lov.FuelQuoteLov_Lov", {
	extend: "e4e.lov.AbstractCombo",
	alias: "widget.ops_FuelQuoteLov_Lov",
	displayField: "code", 
	_columns_: ["code"],
	listConfig: {
		getInnerTpl: function() {
			return '<span>{code}</span>';
		},
		width:250, maxHeight:350
	},
	triggers : {
	     picker : {
	       handler : 'onTriggerClick',
	       scope : 'this'
	     }
	},
	recordModel: atraxo.ops.ui.extjs.ds.FuelQuoteLov_Ds
});
