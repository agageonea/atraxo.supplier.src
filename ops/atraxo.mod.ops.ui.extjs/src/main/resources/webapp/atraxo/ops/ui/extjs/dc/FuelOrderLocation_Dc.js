/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.define("atraxo.ops.ui.extjs.dc.FuelOrderLocation_Dc", {
	extend: "e4e.dc.AbstractDc",
	paramModel: atraxo.ops.ui.extjs.ds.FuelOrderLocation_DsParam,
	recordModel: atraxo.ops.ui.extjs.ds.FuelOrderLocation_Ds
});

/* ================= FILTER: Filter ================= */


Ext.define("atraxo.ops.ui.extjs.dc.FuelOrderLocation_Dc$Filter", {
	extend: "e4e.dc.view.AbstractDcvFilterPropGrid",
	alias: "widget.ops_FuelOrderLocation_Dc$Filter",

	_defineElements_: function() {
		this._getBuilder_()
			/* controls */
			.addLov({name:"location", dataIndex:"locCode", maxLength:25,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.LocationsLov_Lov", selectOnFocus:true, maxLength:25}})
			.addCombo({ xtype:"combo", name:"flightType", dataIndex:"flightType", store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_]})
			.addDateField({name:"startOn", dataIndex:"fuelReleaseStartsOn"})
			.addDateField({name:"endsOn", dataIndex:"fuelReleaseEndsOn"})
			.addLov({name:"supplier", dataIndex:"suppCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					filterFieldMapping: [{lovField:"isSupplier", value: "true"} ]}})
			.addLov({name:"iplAgent", dataIndex:"iplAgentCode", maxLength:32,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.SupplierCodeLov_Lov", selectOnFocus:true, maxLength:32,
					filterFieldMapping: [{lovField:"iplAgent", value: "true"} ]}})
			.addLov({name:"unitCode", dataIndex:"unitCode", maxLength:2,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.MeasurmentUnitsLov_Lov", selectOnFocus:true, maxLength:2}})
			.addLov({name:"currency", dataIndex:"currCode", maxLength:3,
				editor:{xtype:"atraxo.fmbas.ui.extjs.lov.CurrenciesLov_Lov", selectOnFocus:true, maxLength:3}})
			.addNumberField({name:"differential", dataIndex:"differential", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"fuelPrice", dataIndex:"basePrice", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"intoplaneFee", dataIndex:"intoPlaneFee", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"otherFees", dataIndex:"otherFees", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"taxes", dataIndex:"taxes", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"totalPricePerUom", dataIndex:"totalPricePerUom", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"flightFee", dataIndex:"flightFee", sysDec:"dec_crncy", maxLength:19})
			.addNumberField({name:"events", dataIndex:"events", maxLength:11})
		;
	}

});

/* ================= GRID: List ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrderLocation_Dc$List", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelOrderLocation_Dc$List",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addTextColumn({ name:"locCode", dataIndex:"locCode", width:100})
		.addTextColumn({ name:"flightType", dataIndex:"flightType", width:100})
		.addDateColumn({ name:"fuelReleaseStartsOn", dataIndex:"fuelReleaseStartsOn", _mask_: Masks.DATE})
		.addDateColumn({ name:"fuelReleaseEndsOn", dataIndex:"fuelReleaseEndsOn", _mask_: Masks.DATE})
		.addTextColumn({ name:"suppCode", dataIndex:"suppCode", width:80})
		.addTextColumn({ name:"iplAgent", dataIndex:"iplAgentCode", width:80})
		.addNumberColumn({ name:"quantity", dataIndex:"quantity", width:80, sysDec:"dec_unit",  renderer:function(value) {return Ext.util.Format.number(Math.round(value), Main.getNumberFormat(2))}})
		.addTextColumn({ name:"unitCode", dataIndex:"unitCode", width:50})
		.addTextColumn({ name:"currency", dataIndex:"currCode", width:70})
		.addNumberColumn({ name:"differential", dataIndex:"differential", width:80, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"fuelPrice", dataIndex:"basePrice", sysDec:"dec_crncy"})
		.addNumberColumn({ name:"intoplaneFee", dataIndex:"intoPlaneFee", sysDec:"dec_crncy"})
		.addNumberColumn({ name:"otherFees", dataIndex:"otherFees", sysDec:"dec_crncy"})
		.addNumberColumn({ name:"taxes", dataIndex:"taxes", width:80, sysDec:"dec_crncy"})
		.addNumberColumn({ name:"totalPricePerUom", dataIndex:"totalPricePerUom", sysDec:"dec_crncy"})
		.addNumberColumn({ name:"flightFee", dataIndex:"flightFee", sysDec:"dec_crncy"})
		.addTextColumn({ name:"operationalType", dataIndex:"operationalType", hidden:true, width:100})
		.addTextColumn({ name:"product", dataIndex:"product", hidden:true, width:100})
		.addTextColumn({ name:"contCode", dataIndex:"contractCode", hidden:true, width:100})
		.addTextColumn({ name:"iplCode", dataIndex:"iplAgentCode", hidden:true, width:100})
		.addTextColumn({ name:"deliveryMethod", dataIndex:"deliveryMethod", hidden:true, width:100})
		.addNumberColumn({ name:"events", dataIndex:"events", width:100})
		.addTextColumn({ name:"pricingBases", dataIndex:"priceBasis", hidden:true, width:100})
		.addNumberColumn({ name:"margin", dataIndex:"margin", hidden:true, width:100, sysDec:"dec_prc"})
		.addNumberColumn({ name:"paymentTerms", dataIndex:"paymentTerms", hidden:true, width:100})
		.addTextColumn({ name:"paymentRefDay", dataIndex:"paymentRefDate", hidden:true, width:100})
		.addTextColumn({ name:"invoiceFrequency", dataIndex:"invoiceFreq", hidden:true, width:100})
		.addTextColumn({ name:"forEx", dataIndex:"forex", hidden:true, width:100})
		.addTextColumn({ name:"averagingMethodCode", dataIndex:"averagingMethodCode", hidden:true, width:100})
		.addTextColumn({ name:"exchangeRateOffset", dataIndex:"exchangeRateOffset", hidden:true, width:100})
		.addTextColumn({ name:"pricePolicyType", dataIndex:"pricePolicyType", hidden:true, width:100})
		.addTextColumn({ name:"pricePolicyName", dataIndex:"pricePolicyName", hidden:true, width:100})
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});

/* ================= EDIT FORM: Details ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrderLocation_Dc$Details", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelOrderLocation_Dc$Details",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addButton({name:"btn", scope: this, handler: this.showSupplierWindow, text: "...", _enableFn_:this.isFuelOrderNew})
		.addTextField({ name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", noEdit:true , width:200, maxLength:32})
		.addTextField({ name:"location", bind:"{d.locCode}", dataIndex:"locCode", noEdit:true , allowBlank:false, width:180, maxLength:25, labelWidth:80})
		.addCombo({ xtype:"combo", name:"flightType", bind:"{d.flightType}", dataIndex:"flightType", noEdit:true , allowBlank:false, width:200, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:80})
		.addCombo({ xtype:"combo", name:"operationalType", bind:"{d.operationalType}", dataIndex:"operationalType", noEdit:true , allowBlank:false, width:220, store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_], labelWidth:120})
		.addNumberField({name:"quantity", bind:"{d.quantity}", dataIndex:"quantity", noEdit:true , width:130, sysDec:"dec_unit", maxLength:19, labelWidth:70})
		.addTextField({ name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:60, noLabel: true, maxLength:2})
		.addNumberField({name:"events", bind:"{d.events}", dataIndex:"events", noEdit:true , width:120, maxLength:11, labelWidth:60})
		.addCombo({ xtype:"combo", name:"product", bind:"{d.product}", dataIndex:"product", noEdit:true , width:160, store:[ __CMM_TYPE__.Product._JET_A_, __CMM_TYPE__.Product._JET_A1_, __CMM_TYPE__.Product._TS_1_, __CMM_TYPE__.Product._F_34_, __CMM_TYPE__.Product._JP_8_, __CMM_TYPE__.Product._RP_4_, __CMM_TYPE__.Product._T1_, __CMM_TYPE__.Product._A1_BIO_, __CMM_TYPE__.Product._CAT_2_, __CMM_TYPE__.Product._AVGAS_, __CMM_TYPE__.Product._AG_1_HL_, __CMM_TYPE__.Product._AG_80_, __CMM_TYPE__.Product._JA_1_A_, __CMM_TYPE__.Product._JAA_, __CMM_TYPE__.Product._JP_5_, __CMM_TYPE__.Product._NON_, __CMM_TYPE__.Product._OTH_], labelWidth:60})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", noEdit:true , maxLength:4})
		.addCombo({ xtype:"combo", name:"paymentRefDate", bind:"{d.paymentRefDate}", dataIndex:"paymentRefDate", noEdit:true , store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_]})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFreq}", dataIndex:"invoiceFreq", noEdit:true , store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_]})
		.addTextField({ name:"curr", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , maxLength:3})
		.addTextField({ name:"forex", bind:"{d.forex}", dataIndex:"forex", noEdit:true , maxLength:255})
		.addDateField({name:"fuelReleaseStartsOn", bind:"{d.fuelReleaseStartsOn}", dataIndex:"fuelReleaseStartsOn", noEdit:true , width:300, labelWidth:195, style:"margin-top: 5px"})
		.addDateField({name:"fuelReleaseEndsOn", bind:"{d.fuelReleaseEndsOn}", dataIndex:"fuelReleaseEndsOn", noEdit:true , width:160, labelWidth:60, style:"margin-top: 5px"})
		.addDisplayFieldText({ name:"days", bind:"{d.days}", dataIndex:"days", noEdit:true , maxLength:32, labelWidth:10})
		.addDisplayFieldText({ name:"supplyInfo", bind:"{d.supplyInfo}", dataIndex:"supplyInfo", noEdit:true , maxLength:64, labelWidth:120})
		.addTextField({ name:"supplier", bind:"{d.suppCode}", dataIndex:"suppCode", noEdit:true , width:200, maxLength:32})
		.addTextField({ name:"contract", bind:"{d.contractCode}", dataIndex:"contractCode", noEdit:true , width:190, maxLength:32, labelWidth:90})
		.addTextField({ name:"priceBasis", bind:"{d.priceBasis}", dataIndex:"priceBasis", noEdit:true , width:180, maxLength:32, labelWidth:80})
		.addTextField({ name:"contractIndexOffset", bind:"{d.indexOffset}", dataIndex:"indexOffset", noEdit:true , width:160, maxLength:32, labelWidth:60})
		.addNumberField({name:"contractFuelPrice", bind:"{d.fuelPrice}", dataIndex:"fuelPrice", noEdit:true , width:180, sysDec:"dec_crncy", maxLength:19, labelWidth:80})
		.addTextField({ name:"settlementCurrencyCode", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:50, maxLength:3, hideLabel:"true"})
		.addTextField({ name:"settlementUnitCode", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:50, maxLength:2, hideLabel:"true"})
		.addTextField({ name:"iplAgent", bind:"{d.iplAgentCode}", dataIndex:"iplAgentCode", noEdit:true , width:200, maxLength:32})
		.addTextField({ name:"deliveryMethod", bind:"{d.deliveryMethod}", dataIndex:"deliveryMethod", noEdit:true , width:220, maxLength:32, labelWidth:120})
		.addDisplayFieldText({ name:"pricingInfo", bind:"{d.pricingInfo}", dataIndex:"pricingInfo", maxLength:64, labelWidth:120})
		.addNumberField({name:"sellingPrice", bind:"{d.sellingFuelPrice}", dataIndex:"sellingFuelPrice", noEdit:true , width:200, sysDec:"dec_crncy", maxLength:19})
		.addTextField({ name:"sellingUnit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:40, maxLength:2, hideLabel:"true"})
		.addTextField({ name:"sellingCurrency", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:40, maxLength:3, hideLabel:"true"})
		.addNumberField({name:"markup", bind:"{d.differential}", dataIndex:"differential", noEdit:true , width:180, sysDec:"dec_crncy", maxLength:19, labelWidth:80})
		.addTextField({ name:"markupCurrency", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:40, maxLength:3, hideLabel:"true"})
		.addTextField({ name:"markupUnit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:40, maxLength:2, hideLabel:"true"})
		.addNumberField({name:"margin", bind:"{d.margin}", dataIndex:"margin", noEdit:true , width:120, sysDec:"dec_prc", maxLength:19, labelWidth:60})
		.addDisplayFieldText({ name:"percent", bind:"{d.percent}", dataIndex:"percent", maxLength:1, labelWidth:20})
		.addDisplayFieldText({ name:"invoiceStatus", bind:"{d.invoiceStatus}", dataIndex:"invoiceStatus", maxLength:64, labelWidth:90})
		.addTextField({ name:"supplierAccruals", bind:"{d.supplierAccruals}", dataIndex:"supplierAccruals", noEdit:true , width:200, maxLength:64})
		.addTextField({ name:"customerAccruals", bind:"{d.customerAccruals}", dataIndex:"customerAccruals", noEdit:true , width:200, maxLength:64})
		.addDisplayFieldText({ name:"sep", bind:"{d.separator}", dataIndex:"separator", noEdit:true , maxLength:16, labelWidth:1300})
		.addDisplayFieldText({ name:"sep1", bind:"{d.separator}", dataIndex:"separator", noEdit:true , maxLength:16, labelWidth:1300})
		.addDisplayFieldText({ name:"sep2", bind:"{d.separator}", dataIndex:"separator", noEdit:true , maxLength:16, labelWidth:1300})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("customer"),this._getConfig_("location"),this._getConfig_("flightType"),this._getConfig_("operationalType"),this._getConfig_("product"),this._getConfig_("quantity"),this._getConfig_("unit"),this._getConfig_("events")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("fuelReleaseStartsOn"),this._getConfig_("fuelReleaseEndsOn")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("sep")]})
		.add({name:"row4", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("supplyInfo")]})
		.add({name:"row5", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("supplier"),this._getConfig_("btn"),this._getConfig_("contract"),this._getConfig_("priceBasis"),this._getConfig_("contractIndexOffset"),this._getConfig_("contractFuelPrice"),this._getConfig_("settlementCurrencyCode"),this._getConfig_("settlementUnitCode")]})
		.add({name:"row6", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("iplAgent"),this._getConfig_("deliveryMethod")]})
		.add({name:"row7", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("sep1")]})
		.add({name:"row8", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("pricingInfo")]})
		.add({name:"row9", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("sellingPrice"),this._getConfig_("sellingCurrency"),this._getConfig_("sellingUnit"),this._getConfig_("markup"),this._getConfig_("markupCurrency"),this._getConfig_("markupUnit"),this._getConfig_("margin"),this._getConfig_("percent")]})
		.add({name:"row10", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("sep2")]})
		.add({name:"row11", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("invoiceStatus")]})
		.add({name:"row12", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("supplierAccruals"),this._getConfig_("customerAccruals")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:1360, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1"])
		.addChildrenTo("col1", ["row1", "row2", "row3", "row4", "row5", "row6", "row7", "row8", "row9", "row10", "row11", "row12"]);
	},
	/* ==================== Business functions ==================== */
	
	showSupplierWindow: function() {
		
						this._controller_.fireEvent("showWindow", this);
	},
	
	isFuelOrderNew: function() {
		
						return this._controller_.record.data.fuelOrderStatus == __TYPES__.fuelOrder.status.nevv;
	}
});

/* ================= EDIT FORM: SupplySelect ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrderLocation_Dc$SupplySelect", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelOrderLocation_Dc$SupplySelect",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextField({ name:"customer", bind:"{d.customerCode}", dataIndex:"customerCode", noEdit:true , width:170, maxLength:32})
		.addLov({name:"location", bind:"{d.locCode}", dataIndex:"locCode", noEdit:true , width:200, xtype:"fmbas_LocationsLov_Lov", maxLength:25,
			retFieldMapping: [{lovField:"id", dsField: "locId"} ]})
		.addCombo({ xtype:"combo", name:"flightType", bind:"{d.flightType}", dataIndex:"flightType", noEdit:true , width:220, store:[ __CMM_TYPE__.FlightTypeIndicator._UNSPECIFIED_, __CMM_TYPE__.FlightTypeIndicator._DOMESTIC_, __CMM_TYPE__.FlightTypeIndicator._INTERNATIONAL_], labelWidth:120})
		.addCombo({ xtype:"combo", name:"operationalType", bind:"{d.operationalType}", dataIndex:"operationalType", noEdit:true , width:220, store:[ __OPS_TYPE__.OperationType._COMMERCIAL_, __OPS_TYPE__.OperationType._BUSINESS_, __OPS_TYPE__.OperationType._PRIVATE_], labelWidth:110})
		.addNumberField({name:"paymentTerms", bind:"{d.paymentTerms}", dataIndex:"paymentTerms", noEdit:true , width:170, maxLength:4})
		.addCombo({ xtype:"combo", name:"paymentRefDate", bind:"{d.paymentRefDate}", dataIndex:"paymentRefDate", noEdit:true , width:200, store:[ __FMBAS_TYPE__.PaymentDay._INVOICE_DATE_, __FMBAS_TYPE__.PaymentDay._INVOICE_RECEIVING_DATE_, __FMBAS_TYPE__.PaymentDay._LAST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._FIRST_DELIVERY_DATE_, __FMBAS_TYPE__.PaymentDay._MIDPOINT_DELIVERY_DATE_], labelWidth:100})
		.addCombo({ xtype:"combo", name:"invoiceFreq", bind:"{d.invoiceFreq}", dataIndex:"invoiceFreq", noEdit:true , width:220, store:[ __FMBAS_TYPE__.InvoiceFreq._WEEKLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIMONTHLY_, __FMBAS_TYPE__.InvoiceFreq._MONTHLY_, __FMBAS_TYPE__.InvoiceFreq._QUARTERLY_, __FMBAS_TYPE__.InvoiceFreq._SEMIANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._ANNUALLY_, __FMBAS_TYPE__.InvoiceFreq._PER_DELIVERY_, __FMBAS_TYPE__.InvoiceFreq._DAILY_, __FMBAS_TYPE__.InvoiceFreq._EVERY_3_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_10_DAYS_, __FMBAS_TYPE__.InvoiceFreq._EVERY_], labelWidth:120})
		.addLov({name:"curr", bind:"{d.currCode}", dataIndex:"currCode", noEdit:true , width:230, xtype:"fmbas_CurrenciesLov_Lov", maxLength:3, labelWidth:180,
			retFieldMapping: [{lovField:"id", dsField: "currId"} ]})
		.addLov({name:"unit", bind:"{d.unitCode}", dataIndex:"unitCode", noEdit:true , width:50, noLabel: true, xtype:"fmbas_UnitsLov_Lov", maxLength:2,
			retFieldMapping: [{lovField:"id", dsField: "unitId"} ]})
		.addDateField({name:"fuelReleaseStartsOn", bind:"{d.fuelReleaseStartsOn}", dataIndex:"fuelReleaseStartsOn", noEdit:true , width:200})
		.addDateField({name:"fuelReleaseEndsOn", bind:"{d.fuelReleaseEndsOn}", dataIndex:"fuelReleaseEndsOn", noEdit:true , width:170, labelWidth:70})
		.addDisplayFieldText({ name:"text", bind:"{d.fuelReleaseVP}", dataIndex:"fuelReleaseVP", maxLength:64, labelAlign:"left", labelWidth:200})
		.addDisplayFieldText({ name:"text1", bind:"{d.supplyOpt}", dataIndex:"supplyOpt", maxLength:64, labelAlign:"left", labelWidth:200})
		.add({name:"row1", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("customer"),this._getConfig_("location"),this._getConfig_("flightType"),this._getConfig_("operationalType"),this._getConfig_("curr"),this._getConfig_("unit")]})
		.add({name:"row2", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("paymentTerms"),this._getConfig_("paymentRefDate"),this._getConfig_("invoiceFreq")]})
		.add({name:"row3", xtype: "fieldcontainer", layout: {type:"hbox"}, items: [this._getConfig_("fuelReleaseStartsOn"),this._getConfig_("fuelReleaseEndsOn")]})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true})
		.addPanel({ name:"col1", width:1200, layout:"anchor"})
		.addPanel({ name:"col2", width:1200, layout:"anchor"})
		.addPanel({ name:"col3", width:400, layout:"anchor"})
		.addPanel({ name:"col4", width:1200, layout:"anchor"})
		.addPanel({ name:"col5", width:400, layout:"anchor"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["col1", "col2", "col3", "col4", "col5"])
		.addChildrenTo("col1", ["row1"])
		.addChildrenTo("col2", ["row2"])
		.addChildrenTo("col3", ["text"])
		.addChildrenTo("col4", ["row3"])
		.addChildrenTo("col5", ["text1"]);
	}
});

/* ================= EDIT FORM: Remark ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrderLocation_Dc$Remark", {
	extend: "e4e.dc.view.AbstractDcvEditForm",
	alias: "widget.ops_FuelOrderLocation_Dc$Remark",

	/**
	 * Components definition
	 */
	_defineElements_: function() {
		this._getBuilder_()
		
		/* =========== controls =========== */
		.addTextArea({ name:"remarks", bind:"{p.remarks}", paramIndex:"remarks", labelAlign:"top"})
		/* =========== containers =========== */
		.addPanel({ name:"main", autoScroll:true,  layout:"fit"})
		.addPanel({ name:"remarkCol1", layout:"fit"});
	},

	/**
	 * Combine the components
	 */
	_linkElements_: function() {
		this._getBuilder_()
		.addChildrenTo("main", ["remarkCol1"])
		.addChildrenTo("remarkCol1", ["remarks"]);
	},
	/* ==================== Business functions ==================== */
	
	_beforeApplyStates_: function() {
		
						var remarks = this._get_("remarks");
						remarks.setRawValue("");
						remarks.setReadOnly(false);
						return true;
	}
});

/* ================= GRID: Temp ================= */

Ext.define("atraxo.ops.ui.extjs.dc.FuelOrderLocation_Dc$Temp", {
	extend: "e4e.dc.view.AbstractDcvGrid",
	alias: "widget.ops_FuelOrderLocation_Dc$Temp",


	/**
	 * Columns definition
	 */
	_defineColumns_: function() {
		this._getBuilder_()
		.addDefaults();
	},
	
	/**
	 * View-Filter Toolbar parameters
	 */
	_defineToolbarItems_: function() {
		this._getBuilder_()
		;
	}
});
