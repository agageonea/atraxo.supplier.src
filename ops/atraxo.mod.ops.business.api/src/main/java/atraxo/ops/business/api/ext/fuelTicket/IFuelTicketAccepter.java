package atraxo.ops.business.api.ext.fuelTicket;

import atraxo.fmbas.domain.ws.messgeXSD.MSG;
import seava.j4e.api.exceptions.BusinessException;

/**
 * Service interface with asynchronous methods for fuel ticket processing result acknowledgment.
 *
 * @author zspeter
 */
public interface IFuelTicketAccepter {

	/**
	 * Asynchronous method for invoice acknowledgment.
	 *
	 * @param request
	 * @return
	 * @throws BusinessException
	 */
	MSG acknoledgeFuelTicketProcessingResult(MSG request) throws BusinessException;

}
