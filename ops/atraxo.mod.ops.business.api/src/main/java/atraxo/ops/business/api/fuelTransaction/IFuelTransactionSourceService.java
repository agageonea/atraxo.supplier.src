/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelTransaction;

import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSourceParameters;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelTransactionSource} domain entity.
 */
public interface IFuelTransactionSourceService
		extends
			IEntityService<FuelTransactionSource> {

	/**
	 * Custom service findAll
	 *
	 * @return List<FuelTransactionSource>
	 */
	public List<FuelTransactionSource> findAll() throws BusinessException;

	/**
	 * Custom service fetch
	 *
	 * @return void
	 */
	public void fetch(List<FuelTransactionSource> list)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSource
	 */
	public FuelTransactionSource findByName(String name);

	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSource
	 */
	public FuelTransactionSource findByBusiness(Integer id);

	/**
	 * Find by reference: parameters
	 *
	 * @param parameters
	 * @return List<FuelTransactionSource>
	 */
	public List<FuelTransactionSource> findByParameters(
			FuelTransactionSourceParameters parameters);

	/**
	 * Find by ID of reference: parameters.id
	 *
	 * @param parametersId
	 * @return List<FuelTransactionSource>
	 */
	public List<FuelTransactionSource> findByParametersId(Integer parametersId);

	/**
	 * Find by reference: fueltransaction
	 *
	 * @param fueltransaction
	 * @return List<FuelTransactionSource>
	 */
	public List<FuelTransactionSource> findByFueltransaction(
			FuelTransaction fueltransaction);

	/**
	 * Find by ID of reference: fueltransaction.id
	 *
	 * @param fueltransactionId
	 * @return List<FuelTransactionSource>
	 */
	public List<FuelTransactionSource> findByFueltransactionId(
			Integer fueltransactionId);
}
