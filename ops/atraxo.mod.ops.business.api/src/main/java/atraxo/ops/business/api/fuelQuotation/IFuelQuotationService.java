/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelQuotation;

import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelQuotation} domain entity.
 */
public interface IFuelQuotationService extends IEntityService<FuelQuotation> {

	/**
	 * Custom service findForExpire
	 *
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findForExpire() throws BusinessException;

	/**
	 * Custom service expire
	 *
	 * @return void
	 */
	public void expire() throws BusinessException;

	/**
	 * Custom service updateStatus
	 *
	 * @return void
	 */
	public void updateStatus(FuelQuotation e, String action, String remarks)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FuelQuotation
	 */
	public FuelQuotation findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return FuelQuotation
	 */
	public FuelQuotation findByBusiness(Integer id);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: fuelRequest
	 *
	 * @param fuelRequest
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelRequest(FuelRequest fuelRequest);

	/**
	 * Find by ID of reference: fuelRequest.id
	 *
	 * @param fuelRequestId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelRequestId(Integer fuelRequestId);

	/**
	 * Find by reference: fuelOrder
	 *
	 * @param fuelOrder
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelOrder(FuelOrder fuelOrder);

	/**
	 * Find by ID of reference: fuelOrder.id
	 *
	 * @param fuelOrderId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelOrderId(Integer fuelOrderId);

	/**
	 * Find by reference: holder
	 *
	 * @param holder
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByHolder(Customer holder);

	/**
	 * Find by ID of reference: holder.id
	 *
	 * @param holderId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByHolderId(Integer holderId);

	/**
	 * Find by reference: fuelQuoteLocations
	 *
	 * @param fuelQuoteLocations
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelQuoteLocations(
			FuelQuoteLocation fuelQuoteLocations);

	/**
	 * Find by ID of reference: fuelQuoteLocations.id
	 *
	 * @param fuelQuoteLocationsId
	 * @return List<FuelQuotation>
	 */
	public List<FuelQuotation> findByFuelQuoteLocationsId(
			Integer fuelQuoteLocationsId);
}
