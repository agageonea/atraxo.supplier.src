/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelTransaction;

import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionEquipment;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelTransactionEquipment} domain entity.
 */
public interface IFuelTransactionEquipmentService
		extends
			IEntityService<FuelTransactionEquipment> {

	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionEquipment
	 */
	public FuelTransactionEquipment findByBusiness(Integer id);

	/**
	 * Find by reference: fuelTransaction
	 *
	 * @param fuelTransaction
	 * @return List<FuelTransactionEquipment>
	 */
	public List<FuelTransactionEquipment> findByFuelTransaction(
			FuelTransaction fuelTransaction);

	/**
	 * Find by ID of reference: fuelTransaction.id
	 *
	 * @param fuelTransactionId
	 * @return List<FuelTransactionEquipment>
	 */
	public List<FuelTransactionEquipment> findByFuelTransactionId(
			Integer fuelTransactionId);
}
