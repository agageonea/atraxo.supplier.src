/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelRequest;

import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelRequest} domain entity.
 */
public interface IFuelRequestService extends IEntityService<FuelRequest> {

	/**
	 * Custom service check
	 *
	 * @return void
	 */
	public void check(FuelRequest fuelRequest) throws BusinessException;

	/**
	 * Custom service generateFuelQuotation
	 *
	 * @return FuelQuotation
	 */
	public FuelQuotation generateFuelQuotation(FuelRequest fuelRequest)
			throws BusinessException;

	/**
	 * Custom service updateStatus
	 *
	 * @return void
	 */
	public void updateStatus(FuelRequest fuelRequest, String action,
			String remarks) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FuelRequest
	 */
	public FuelRequest findByCode(String requestCode);

	/**
	 * Find by unique key
	 *
	 * @return FuelRequest
	 */
	public FuelRequest findByBusiness(Integer id);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByContact(Contacts contact);

	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByContactId(Integer contactId);

	/**
	 * Find by reference: fuelQuotation
	 *
	 * @param fuelQuotation
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByFuelQuotation(FuelQuotation fuelQuotation);

	/**
	 * Find by ID of reference: fuelQuotation.id
	 *
	 * @param fuelQuotationId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByFuelQuotationId(Integer fuelQuotationId);

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findBySubsidiary(Customer subsidiary);

	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findBySubsidiaryId(Integer subsidiaryId);

	/**
	 * Find by reference: flightEvent
	 *
	 * @param flightEvent
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByFlightEvent(FlightEvent flightEvent);

	/**
	 * Find by ID of reference: flightEvent.id
	 *
	 * @param flightEventId
	 * @return List<FuelRequest>
	 */
	public List<FuelRequest> findByFlightEventId(Integer flightEventId);
}
