/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelTicket;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.aircraft.Aircraft;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.fuelTicket.CancelFuelTicketResult;
import atraxo.ops.domain.ext.fuelTicket.ProvidedQuantityInforamtionResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.ops_type.FuelTicketApprovalStatus;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelTicket} domain entity.
 */
public interface IFuelTicketService extends IEntityService<FuelTicket> {

	/**
	 * Custom service releaseFuelTicket
	 *
	 * @return void
	 */
	public void releaseFuelTicket(List<FuelTicket> fuelTickets)
			throws BusinessException;

	/**
	 * Custom service getFuelTicketApprovalStatusUpdated
	 *
	 * @return Boolean
	 */
	public Boolean getFuelTicketApprovalStatusUpdated(Object id)
			throws BusinessException;

	/**
	 * Custom service rejectFuelTicket
	 *
	 * @return void
	 */
	public void rejectFuelTicket(List<FuelTicket> fuelTickets, String reason)
			throws BusinessException;

	/**
	 * Custom service getFuelTicketsForTransmission
	 *
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> getFuelTicketsForTransmission()
			throws BusinessException;

	/**
	 * Custom service cancelFuelTicket
	 *
	 * @return CancelFuelTicketResult
	 */
	public CancelFuelTicketResult cancelFuelTicket(
			List<FuelTicket> fuelTickets, String reason)
			throws BusinessException;

	/**
	 * Custom service submitFuelTicket
	 *
	 * @return void
	 */
	public void submitFuelTicket(List<FuelTicket> fuelTickets)
			throws BusinessException;

	/**
	 * Custom service resetFuelTicket
	 *
	 * @return void
	 */
	public void resetFuelTicket(List<FuelTicket> fuelTickets, String reason)
			throws BusinessException;

	/**
	 * Custom service getUserLocation
	 *
	 * @return Locations
	 */
	public Locations getUserLocation(String loginName) throws BusinessException;

	/**
	 * Custom service verifyCustomer
	 *
	 * @return Customer
	 */
	public Customer verifyCustomer(String customer) throws BusinessException;

	/**
	 * Custom service updateWithoutBL
	 *
	 * @return void
	 */
	public void updateWithoutBL(List<FuelTicket> fuelTickets)
			throws BusinessException;

	/**
	 * Custom service synchronize
	 *
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> synchronize(List<FuelTicket> fueltickets)
			throws BusinessException;

	/**
	 * Custom service findByDateCustomerSupplierLocationFlightType
	 *
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDateCustomerSupplierLocationFlightType(
			Date validFrom, Date validTo, Suppliers supplier,
			Locations location, String type) throws BusinessException;

	/**
	 * Custom service saveFuelTicketHardCopy
	 *
	 * @return void
	 */
	public void saveFuelTicketHardCopy(FuelTicket fuelTicket,
			String ticketHardCopy) throws BusinessException;

	/**
	 * Custom service setFlagDelete
	 *
	 * @return void
	 */
	public void setFlagDelete(List<FuelTicket> archivedFuelTickets,
			Boolean isDeleted) throws BusinessException;

	/**
	 * Custom service insertToHistory
	 *
	 * @return void
	 */
	public void insertToHistory(FuelTicket e, String objectValue, String remarks)
			throws BusinessException;

	/**
	 * Custom service setFuelTicketApprovalStatus
	 *
	 * @return void
	 */
	public void setFuelTicketApprovalStatus(FuelTicket e, String reason,
			String action, FuelTicketApprovalStatus status)
			throws BusinessException;

	/**
	 * Custom service exportFuelEvents
	 *
	 * @return void
	 */
	public void exportFuelEvents(List<FuelTicket> fuelTickets)
			throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FuelTicket
	 */
	public FuelTicket findByBusinessKey(Locations departure, String ticketNo,
			Customer customer, Date deliveryDate);

	/**
	 * Find by unique key
	 *
	 * @return FuelTicket
	 */
	public FuelTicket findByBusinessKey(Long departureId, String ticketNo,
			Long customerId, Date deliveryDate);

	/**
	 * Find by unique key
	 *
	 * @return FuelTicket
	 */
	public FuelTicket findByBusiness(Integer id);

	/**
	 * Find by reference: departure
	 *
	 * @param departure
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDeparture(Locations departure);

	/**
	 * Find by ID of reference: departure.id
	 *
	 * @param departureId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDepartureId(Integer departureId);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: upliftUnit
	 *
	 * @param upliftUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByUpliftUnit(Unit upliftUnit);

	/**
	 * Find by ID of reference: upliftUnit.id
	 *
	 * @param upliftUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByUpliftUnitId(Integer upliftUnitId);

	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findBySupplier(Suppliers supplier);

	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findBySupplierId(Integer supplierId);

	/**
	 * Find by reference: fueller
	 *
	 * @param fueller
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByFueller(Suppliers fueller);

	/**
	 * Find by ID of reference: fueller.id
	 *
	 * @param fuellerId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByFuellerId(Integer fuellerId);

	/**
	 * Find by reference: destination
	 *
	 * @param destination
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDestination(Locations destination);

	/**
	 * Find by ID of reference: destination.id
	 *
	 * @param destinationId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDestinationId(Integer destinationId);

	/**
	 * Find by reference: registration
	 *
	 * @param registration
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByRegistration(Aircraft registration);

	/**
	 * Find by ID of reference: registration.id
	 *
	 * @param registrationId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByRegistrationId(Integer registrationId);

	/**
	 * Find by reference: finalDest
	 *
	 * @param finalDest
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByFinalDest(Locations finalDest);

	/**
	 * Find by ID of reference: finalDest.id
	 *
	 * @param finalDestId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByFinalDestId(Integer finalDestId);

	/**
	 * Find by reference: netUpliftUnit
	 *
	 * @param netUpliftUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByNetUpliftUnit(Unit netUpliftUnit);

	/**
	 * Find by ID of reference: netUpliftUnit.id
	 *
	 * @param netUpliftUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByNetUpliftUnitId(Integer netUpliftUnitId);

	/**
	 * Find by reference: beforeFuelingUnit
	 *
	 * @param beforeFuelingUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByBeforeFuelingUnit(Unit beforeFuelingUnit);

	/**
	 * Find by ID of reference: beforeFuelingUnit.id
	 *
	 * @param beforeFuelingUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByBeforeFuelingUnitId(
			Integer beforeFuelingUnitId);

	/**
	 * Find by reference: afterFuelingUnit
	 *
	 * @param afterFuelingUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAfterFuelingUnit(Unit afterFuelingUnit);

	/**
	 * Find by ID of reference: afterFuelingUnit.id
	 *
	 * @param afterFuelingUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAfterFuelingUnitId(Integer afterFuelingUnitId);

	/**
	 * Find by reference: densityUnit
	 *
	 * @param densityUnit
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDensityUnit(Unit densityUnit);

	/**
	 * Find by ID of reference: densityUnit.id
	 *
	 * @param densityUnitId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDensityUnitId(Integer densityUnitId);

	/**
	 * Find by reference: densityVolume
	 *
	 * @param densityVolume
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDensityVolume(Unit densityVolume);

	/**
	 * Find by ID of reference: densityVolume.id
	 *
	 * @param densityVolumeId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByDensityVolumeId(Integer densityVolumeId);

	/**
	 * Find by reference: acType
	 *
	 * @param acType
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAcType(AcTypes acType);

	/**
	 * Find by ID of reference: acType.id
	 *
	 * @param acTypeId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAcTypeId(Integer acTypeId);

	/**
	 * Find by reference: reseller
	 *
	 * @param reseller
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByReseller(Customer reseller);

	/**
	 * Find by ID of reference: reseller.id
	 *
	 * @param resellerId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByResellerId(Integer resellerId);

	/**
	 * Find by reference: ammountCurr
	 *
	 * @param ammountCurr
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAmmountCurr(Currencies ammountCurr);

	/**
	 * Find by ID of reference: ammountCurr.id
	 *
	 * @param ammountCurrId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByAmmountCurrId(Integer ammountCurrId);

	/**
	 * Find by reference: referenceTicket
	 *
	 * @param referenceTicket
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByReferenceTicket(FuelTicket referenceTicket);

	/**
	 * Find by ID of reference: referenceTicket.id
	 *
	 * @param referenceTicketId
	 * @return List<FuelTicket>
	 */
	public List<FuelTicket> findByReferenceTicketId(Integer referenceTicketId);
}
