package atraxo.ops.business.api.ext.fuelTicket;

import java.util.List;
import java.util.Set;

import atraxo.fmbas.business.api.ext.bpm.WorkflowNames;
import atraxo.fmbas.business.api.ext.bpm.WorkflowStartResult;
import atraxo.ops.domain.ext.fuelTicketValidator.FuelTicketWorkflowStarterValidatorResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.session.IOrganization;

public interface IFuelTicketWorkflowStarter {

	WorkflowStartResult startFuelTicketCancelWorkflow(FuelTicket fuelTicket, String remark) throws BusinessException;

	void startFuelTicketWorkflowAsync(Set<String> fuelTicketRefIds, String userCode, String clientId, List<IOrganization> organizations)
			throws BusinessException;

	void startFuelTicketWorkflow(List<FuelTicket> fuelTickets) throws BusinessException;

	WorkflowStartResult startFuelTicketReleaseWorkflow(List<FuelTicket> fuelTickets, boolean validationDone) throws BusinessException;

	WorkflowStartResult startFuelTicketApprovalWorkflow(FuelTicket fuelTicket, String note, String attachmentsArray, boolean validationDone)
			throws BusinessException;

	FuelTicketWorkflowStarterValidatorResult validateWorkflowStart(FuelTicket fuelTicket) throws Exception;

	public FuelTicketWorkflowStarterValidatorResult validateWorkflowStart(FuelTicket fuelTicket, WorkflowNames workflowName) throws Exception;

}
