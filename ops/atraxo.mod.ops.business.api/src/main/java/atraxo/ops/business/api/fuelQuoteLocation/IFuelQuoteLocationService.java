/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelQuoteLocation;

import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.cmm.domain.impl.prices.ContractPriceComponent;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.quotation.Quotation;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.fuelQuoteLocation.CapturedPriceInformationResult;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelQuoteLocation} domain entity.
 */
public interface IFuelQuoteLocationService
		extends
			IEntityService<FuelQuoteLocation> {

	/**
	 * Custom service convertPrice
	 *
	 * @return BigDecimal
	 */
	public BigDecimal convertPrice(ContractPriceComponent priceComponent,
			Integer finSrcId, Integer avgMthdId, String currencyCode,
			String unitCode) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FuelQuoteLocation
	 */
	public FuelQuoteLocation findByBusiness(Integer id);

	/**
	 * Find by reference: quote
	 *
	 * @param quote
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByQuote(FuelQuotation quote);

	/**
	 * Find by ID of reference: quote.id
	 *
	 * @param quoteId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByQuoteId(Integer quoteId);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByLocationId(Integer locationId);

	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findBySupplier(Suppliers supplier);

	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findBySupplierId(Integer supplierId);

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByContract(Contract contract);

	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByContractId(Integer contractId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByUnitId(Integer unitId);

	/**
	 * Find by reference: intoPlaneAgent
	 *
	 * @param intoPlaneAgent
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByIntoPlaneAgent(Suppliers intoPlaneAgent);

	/**
	 * Find by ID of reference: intoPlaneAgent.id
	 *
	 * @param intoPlaneAgentId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByIntoPlaneAgentId(
			Integer intoPlaneAgentId);

	/**
	 * Find by reference: quotation
	 *
	 * @param quotation
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByQuotation(Quotation quotation);

	/**
	 * Find by ID of reference: quotation.id
	 *
	 * @param quotationId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByQuotationId(Integer quotationId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByCurrencyId(Integer currencyId);

	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByAverageMethod(
			AverageMethod averageMethod);

	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByAverageMethodId(Integer averageMethodId);

	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByFinancialSource(
			FinancialSources financialSource);

	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByFinancialSourceId(
			Integer financialSourceId);

	/**
	 * Find by reference: pricingPolicy
	 *
	 * @param pricingPolicy
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByPricingPolicy(PricePolicy pricingPolicy);

	/**
	 * Find by ID of reference: pricingPolicy.id
	 *
	 * @param pricingPolicyId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByPricingPolicyId(Integer pricingPolicyId);

	/**
	 * Find by reference: flightEvents
	 *
	 * @param flightEvents
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByFlightEvents(FlightEvent flightEvents);

	/**
	 * Find by ID of reference: flightEvents.id
	 *
	 * @param flightEventsId
	 * @return List<FuelQuoteLocation>
	 */
	public List<FuelQuoteLocation> findByFlightEventsId(Integer flightEventsId);
}
