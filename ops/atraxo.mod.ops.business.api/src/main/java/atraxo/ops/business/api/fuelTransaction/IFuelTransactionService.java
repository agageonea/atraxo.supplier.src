/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelTransaction;

import atraxo.ops.domain.ext.fuelTicket.FetchResult;
import atraxo.ops.domain.ext.fuelTicket.ProcessResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionEquipment;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import atraxo.ops.domain.impl.ops_type.TicketType;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelTransaction} domain entity.
 */
public interface IFuelTransactionService
		extends
			IEntityService<FuelTransaction> {

	/**
	 * Custom service process
	 *
	 * @return void
	 */
	public void process(List<FuelTransaction> list, List<FuelTicket> insList)
			throws BusinessException;

	/**
	 * Custom service processWithoutSave
	 *
	 * @return void
	 */
	public void processWithoutSave(List<FuelTransaction> list,
			List<FuelTicket> insList) throws BusinessException;

	/**
	 * Custom service process
	 *
	 * @return FuelTicket
	 */
	public FuelTicket process(FuelTransaction ft) throws BusinessException;

	/**
	 * Custom service findAll
	 *
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findAll() throws BusinessException;

	/**
	 * Custom service getFuelTranscation
	 *
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> getFuelTranscation(Long offset)
			throws BusinessException;

	/**
	 * Custom service saveProcessedResult
	 *
	 * @return void
	 */
	public void saveProcessedResult(ProcessResult result)
			throws BusinessException;

	/**
	 * Custom service fetch
	 *
	 * @return FetchResult
	 */
	public FetchResult fetch(List<FuelTransaction> list)
			throws BusinessException;

	/**
	 * Custom service determineSubsidiaryAtImport
	 *
	 * @return void
	 */
	public void determineSubsidiaryAtImport(FuelTransaction ft)
			throws BusinessException;

	/**
	 * Custom service count
	 *
	 * @return Integer
	 */
	public Integer count() throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FuelTransaction
	 */
	public FuelTransaction findByBusinessKey(String airportCode,
			String supplierCode, Date transactionDate, String ticketNumber,
			TicketType ticketType);

	/**
	 * Find by unique key
	 *
	 * @return FuelTransaction
	 */
	public FuelTransaction findByBusiness(Integer id);

	/**
	 * Find by reference: importSource
	 *
	 * @param importSource
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByImportSource(
			FuelTransactionSource importSource);

	/**
	 * Find by ID of reference: importSource.id
	 *
	 * @param importSourceId
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByImportSourceId(Integer importSourceId);

	/**
	 * Find by reference: equipment
	 *
	 * @param equipment
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByEquipment(
			FuelTransactionEquipment equipment);

	/**
	 * Find by ID of reference: equipment.id
	 *
	 * @param equipmentId
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByEquipmentId(Integer equipmentId);

	/**
	 * Find by reference: quantity
	 *
	 * @param quantity
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByQuantity(FuelTransactionQuantity quantity);

	/**
	 * Find by ID of reference: quantity.id
	 *
	 * @param quantityId
	 * @return List<FuelTransaction>
	 */
	public List<FuelTransaction> findByQuantityId(Integer quantityId);
}
