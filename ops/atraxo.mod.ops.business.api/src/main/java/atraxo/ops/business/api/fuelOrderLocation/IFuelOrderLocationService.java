/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelOrderLocation;

import atraxo.cmm.domain.impl.cmm_type.FlightTypeIndicator;
import atraxo.cmm.domain.impl.contracts.Contract;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.financialSources.FinancialSources;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.timeserie.AverageMethod;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.fuelOrderLocation.FuelOrderLocationMessage;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelOrderLocation} domain entity.
 */
public interface IFuelOrderLocationService
		extends
			IEntityService<FuelOrderLocation> {

	/**
	 * Custom service generate
	 *
	 * @return void
	 */
	public void generate(FuelQuoteLocation locationQuote, FuelOrder fuelOrder)
			throws BusinessException;

	/**
	 * Custom service generate
	 *
	 * @return void
	 */
	public void generate(List<FuelQuoteLocation> locationQuotes,
			FuelOrder fuelOrder) throws BusinessException;

	/**
	 * Custom service changeSupplier
	 *
	 * @return void
	 */
	public void changeSupplier(FuelOrderLocation locationOrder,
			Integer contractId, String remarks) throws BusinessException;

	/**
	 * Custom service findDirectFuelOrder
	 *
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findDirectFuelOrder(Suppliers supplier,
			Customer customer, FlightTypeIndicator flightType,
			Locations location, Date date) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FuelOrderLocation
	 */
	public FuelOrderLocation findByBusiness(Integer id);

	/**
	 * Find by reference: fuelOrder
	 *
	 * @param fuelOrder
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFuelOrder(FuelOrder fuelOrder);

	/**
	 * Find by ID of reference: fuelOrder.id
	 *
	 * @param fuelOrderId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFuelOrderId(Integer fuelOrderId);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByLocationId(Integer locationId);

	/**
	 * Find by reference: supplier
	 *
	 * @param supplier
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findBySupplier(Suppliers supplier);

	/**
	 * Find by ID of reference: supplier.id
	 *
	 * @param supplierId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findBySupplierId(Integer supplierId);

	/**
	 * Find by reference: contract
	 *
	 * @param contract
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByContract(Contract contract);

	/**
	 * Find by ID of reference: contract.id
	 *
	 * @param contractId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByContractId(Integer contractId);

	/**
	 * Find by reference: iplAgent
	 *
	 * @param iplAgent
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByIplAgent(Suppliers iplAgent);

	/**
	 * Find by ID of reference: iplAgent.id
	 *
	 * @param iplAgentId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByIplAgentId(Integer iplAgentId);

	/**
	 * Find by reference: paymentCurrency
	 *
	 * @param paymentCurrency
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPaymentCurrency(
			Currencies paymentCurrency);

	/**
	 * Find by ID of reference: paymentCurrency.id
	 *
	 * @param paymentCurrencyId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPaymentCurrencyId(
			Integer paymentCurrencyId);

	/**
	 * Find by reference: paymentUnit
	 *
	 * @param paymentUnit
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPaymentUnit(Unit paymentUnit);

	/**
	 * Find by ID of reference: paymentUnit.id
	 *
	 * @param paymentUnitId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPaymentUnitId(Integer paymentUnitId);

	/**
	 * Find by reference: financialSource
	 *
	 * @param financialSource
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFinancialSource(
			FinancialSources financialSource);

	/**
	 * Find by ID of reference: financialSource.id
	 *
	 * @param financialSourceId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFinancialSourceId(
			Integer financialSourceId);

	/**
	 * Find by reference: averageMethod
	 *
	 * @param averageMethod
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByAverageMethod(
			AverageMethod averageMethod);

	/**
	 * Find by ID of reference: averageMethod.id
	 *
	 * @param averageMethodId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByAverageMethodId(Integer averageMethodId);

	/**
	 * Find by reference: pricePolicy
	 *
	 * @param pricePolicy
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPricePolicy(PricePolicy pricePolicy);

	/**
	 * Find by ID of reference: pricePolicy.id
	 *
	 * @param pricePolicyId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByPricePolicyId(Integer pricePolicyId);

	/**
	 * Find by reference: flightEvents
	 *
	 * @param flightEvents
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFlightEvents(FlightEvent flightEvents);

	/**
	 * Find by ID of reference: flightEvents.id
	 *
	 * @param flightEventsId
	 * @return List<FuelOrderLocation>
	 */
	public List<FuelOrderLocation> findByFlightEventsId(Integer flightEventsId);
}
