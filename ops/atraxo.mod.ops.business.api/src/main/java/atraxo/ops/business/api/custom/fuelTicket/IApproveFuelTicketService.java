package atraxo.ops.business.api.custom.fuelTicket;

import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import seava.j4e.api.exceptions.BusinessException;

public interface IApproveFuelTicketService {

	public void releaseFuelTicket(FuelTicket e) throws BusinessException;
}
