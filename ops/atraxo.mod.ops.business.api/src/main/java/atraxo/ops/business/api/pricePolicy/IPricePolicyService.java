/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.pricePolicy;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.currencies.Currencies;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.impl.pricePolicy.PricePolicy;
import java.math.BigDecimal;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link PricePolicy} domain entity.
 */
public interface IPricePolicyService extends IEntityService<PricePolicy> {

	/**
	 * Custom service convert
	 *
	 * @return BigDecimal
	 */
	public BigDecimal convert(Integer fromUnitId, Integer toUnitId,
			Integer fromCurrencyId, Integer toCurrencyId, BigDecimal value,
			Integer finSrcId, Integer avgMthdId, String offset)
			throws BusinessException;

	/**
	 * Custom service findByLocAndCust
	 *
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByLocAndCust(Locations location,
			Customer customer) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return PricePolicy
	 */
	public PricePolicy findByBusiness(Integer id);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: location
	 *
	 * @param location
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByLocation(Locations location);

	/**
	 * Find by ID of reference: location.id
	 *
	 * @param locationId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByLocationId(Integer locationId);

	/**
	 * Find by reference: aircraft
	 *
	 * @param aircraft
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByAircraft(AcTypes aircraft);

	/**
	 * Find by ID of reference: aircraft.id
	 *
	 * @param aircraftId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByAircraftId(Integer aircraftId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByUnitId(Integer unitId);

	/**
	 * Find by reference: currency
	 *
	 * @param currency
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByCurrency(Currencies currency);

	/**
	 * Find by ID of reference: currency.id
	 *
	 * @param currencyId
	 * @return List<PricePolicy>
	 */
	public List<PricePolicy> findByCurrencyId(Integer currencyId);
}
