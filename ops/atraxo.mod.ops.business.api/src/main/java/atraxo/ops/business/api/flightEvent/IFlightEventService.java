/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.flightEvent;

import atraxo.fmbas.domain.impl.ac.AcTypes;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.geo.Locations;
import atraxo.fmbas.domain.impl.suppliers.Suppliers;
import atraxo.fmbas.domain.impl.uom.Unit;
import atraxo.ops.domain.ext.flightEvent.AttachedItem;
import atraxo.ops.domain.ext.flightEvent.ProvidedQuantityInforamtionResult;
import atraxo.ops.domain.impl.flightEvent.FlightEvent;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuoteLocation.FuelQuoteLocation;
import atraxo.ops.domain.impl.fuelRequest.FuelRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FlightEvent} domain entity.
 */
public interface IFlightEventService extends IEntityService<FlightEvent> {

	/**
	 * Custom service updateOrDelete
	 *
	 * @return void
	 */
	public void updateOrDelete(Integer id, AttachedItem attachedItem)
			throws BusinessException;

	/**
	 * Custom service findByFuelOrder
	 *
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByFuelOrder(FuelOrder fuelOrder)
			throws BusinessException;

	/**
	 * Custom service reschedule
	 *
	 * @return void
	 */
	public void reschedule(FlightEvent flightEvent, Date arrivalDate,
			Date departureDate, Date fuelingDate, Boolean timeReference,
			String remark) throws BusinessException;

	/**
	 * Custom service updateWithoutBusiness
	 *
	 * @return void
	 */
	public void updateWithoutBusiness(FlightEvent e) throws BusinessException;

	/**
	 * Custom service updateWithoutBusiness
	 *
	 * @return void
	 */
	public void updateWithoutBusiness(List<FlightEvent> list)
			throws BusinessException;

	/**
	 * Custom service updateStatus
	 *
	 * @return void
	 */
	public void updateStatus(FlightEvent flightEvent, String action,
			String remarks, String status) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FlightEvent
	 */
	public FlightEvent findByBusiness(Integer id);

	/**
	 * Find by reference: locQuote
	 *
	 * @param locQuote
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByLocQuote(FuelQuoteLocation locQuote);

	/**
	 * Find by ID of reference: locQuote.id
	 *
	 * @param locQuoteId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByLocQuoteId(Integer locQuoteId);

	/**
	 * Find by reference: fuelRequest
	 *
	 * @param fuelRequest
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByFuelRequest(FuelRequest fuelRequest);

	/**
	 * Find by ID of reference: fuelRequest.id
	 *
	 * @param fuelRequestId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByFuelRequestId(Integer fuelRequestId);

	/**
	 * Find by reference: departure
	 *
	 * @param departure
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByDeparture(Locations departure);

	/**
	 * Find by ID of reference: departure.id
	 *
	 * @param departureId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByDepartureId(Integer departureId);

	/**
	 * Find by reference: destination
	 *
	 * @param destination
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByDestination(Locations destination);

	/**
	 * Find by ID of reference: destination.id
	 *
	 * @param destinationId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByDestinationId(Integer destinationId);

	/**
	 * Find by reference: aircraftType
	 *
	 * @param aircraftType
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByAircraftType(AcTypes aircraftType);

	/**
	 * Find by ID of reference: aircraftType.id
	 *
	 * @param aircraftTypeId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByAircraftTypeId(Integer aircraftTypeId);

	/**
	 * Find by reference: unit
	 *
	 * @param unit
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByUnit(Unit unit);

	/**
	 * Find by ID of reference: unit.id
	 *
	 * @param unitId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByUnitId(Integer unitId);

	/**
	 * Find by reference: arrivalFrom
	 *
	 * @param arrivalFrom
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByArrivalFrom(Locations arrivalFrom);

	/**
	 * Find by ID of reference: arrivalFrom.id
	 *
	 * @param arrivalFromId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByArrivalFromId(Integer arrivalFromId);

	/**
	 * Find by reference: operator
	 *
	 * @param operator
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByOperator(Customer operator);

	/**
	 * Find by ID of reference: operator.id
	 *
	 * @param operatorId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByOperatorId(Integer operatorId);

	/**
	 * Find by reference: handler
	 *
	 * @param handler
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByHandler(Suppliers handler);

	/**
	 * Find by ID of reference: handler.id
	 *
	 * @param handlerId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByHandlerId(Integer handlerId);

	/**
	 * Find by reference: aircraftHomeBase
	 *
	 * @param aircraftHomeBase
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByAircraftHomeBase(Locations aircraftHomeBase);

	/**
	 * Find by ID of reference: aircraftHomeBase.id
	 *
	 * @param aircraftHomeBaseId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByAircraftHomeBaseId(Integer aircraftHomeBaseId);

	/**
	 * Find by reference: locOrder
	 *
	 * @param locOrder
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByLocOrder(FuelOrderLocation locOrder);

	/**
	 * Find by ID of reference: locOrder.id
	 *
	 * @param locOrderId
	 * @return List<FlightEvent>
	 */
	public List<FlightEvent> findByLocOrderId(Integer locOrderId);
}
