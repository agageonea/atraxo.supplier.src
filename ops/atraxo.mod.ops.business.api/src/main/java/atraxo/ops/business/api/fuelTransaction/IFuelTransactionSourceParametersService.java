/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelTransaction;

import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSource;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionSourceParameters;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelTransactionSourceParameters} domain entity.
 */
public interface IFuelTransactionSourceParametersService
		extends
			IEntityService<FuelTransactionSourceParameters> {

	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSourceParameters
	 */
	public FuelTransactionSourceParameters findByCode(
			FuelTransactionSource fuelTransactionSource, String paramName);

	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSourceParameters
	 */
	public FuelTransactionSourceParameters findByCode(
			Long fuelTransactionSourceId, String paramName);

	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionSourceParameters
	 */
	public FuelTransactionSourceParameters findByBusiness(Integer id);

	/**
	 * Find by reference: fuelTransactionSource
	 *
	 * @param fuelTransactionSource
	 * @return List<FuelTransactionSourceParameters>
	 */
	public List<FuelTransactionSourceParameters> findByFuelTransactionSource(
			FuelTransactionSource fuelTransactionSource);

	/**
	 * Find by ID of reference: fuelTransactionSource.id
	 *
	 * @param fuelTransactionSourceId
	 * @return List<FuelTransactionSourceParameters>
	 */
	public List<FuelTransactionSourceParameters> findByFuelTransactionSourceId(
			Integer fuelTransactionSourceId);
}
