package atraxo.ops.business.api.ext.fuelTicket;

import java.util.List;

import atraxo.ops.domain.ext.fuelTicket.CancelFuelTicketResult;
import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import seava.j4e.api.exceptions.BusinessException;

/**
 * @author apetho
 */
public interface ICancelFuelTicket {

	CancelFuelTicketResult cancelFuelTickets(List<FuelTicket> fuelTickets) throws BusinessException;
}
