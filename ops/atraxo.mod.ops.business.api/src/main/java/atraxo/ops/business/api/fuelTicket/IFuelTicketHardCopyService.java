/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelTicket;

import atraxo.ops.domain.impl.fuelTicket.FuelTicket;
import atraxo.ops.domain.impl.fuelTicket.FuelTicketHardCopy;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelTicketHardCopy} domain entity.
 */
public interface IFuelTicketHardCopyService
		extends
			IEntityService<FuelTicketHardCopy> {

	/**
	 * Find by unique key
	 *
	 * @return FuelTicketHardCopy
	 */
	public FuelTicketHardCopy findByBk(FuelTicket fuelTicket);

	/**
	 * Find by unique key
	 *
	 * @return FuelTicketHardCopy
	 */
	public FuelTicketHardCopy findByBk(Long fuelTicketId);

	/**
	 * Find by unique key
	 *
	 * @return FuelTicketHardCopy
	 */
	public FuelTicketHardCopy findByBusiness(Integer id);

	/**
	 * Find by reference: fuelTicket
	 *
	 * @param fuelTicket
	 * @return List<FuelTicketHardCopy>
	 */
	public List<FuelTicketHardCopy> findByFuelTicket(FuelTicket fuelTicket);

	/**
	 * Find by ID of reference: fuelTicket.id
	 *
	 * @param fuelTicketId
	 * @return List<FuelTicketHardCopy>
	 */
	public List<FuelTicketHardCopy> findByFuelTicketId(Integer fuelTicketId);
}
