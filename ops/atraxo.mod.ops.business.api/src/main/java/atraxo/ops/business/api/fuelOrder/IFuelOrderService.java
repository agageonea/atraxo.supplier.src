/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelOrder;

import atraxo.fmbas.domain.impl.contacts.Contacts;
import atraxo.fmbas.domain.impl.customer.Customer;
import atraxo.fmbas.domain.impl.user.UserSupp;
import atraxo.ops.domain.impl.fuelOrder.FuelOrder;
import atraxo.ops.domain.impl.fuelOrderLocation.FuelOrderLocation;
import atraxo.ops.domain.impl.fuelQuotation.FuelQuotation;
import atraxo.ops.domain.impl.ops_type.FuelOrderStatus;
import java.util.Date;
import java.util.List;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelOrder} domain entity.
 */
public interface IFuelOrderService extends IEntityService<FuelOrder> {

	/**
	 * Custom service updateStatus
	 *
	 * @return void
	 */
	public void updateStatus(FuelOrder e, String action, String remarks,
			FuelOrderStatus status) throws BusinessException;

	/**
	 * Custom service generate
	 *
	 * @return void
	 */
	public void generate(FuelQuotation fq, Date postedOn, Contacts submited,
			String customerRefNo) throws BusinessException;

	/**
	 * Custom service cancel
	 *
	 * @return void
	 */
	public void cancel(FuelOrder fq, String remark) throws BusinessException;

	/**
	 * Find by unique key
	 *
	 * @return FuelOrder
	 */
	public FuelOrder findByCode(String code);

	/**
	 * Find by unique key
	 *
	 * @return FuelOrder
	 */
	public FuelOrder findByBusiness(Integer id);

	/**
	 * Find by reference: customer
	 *
	 * @param customer
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByCustomer(Customer customer);

	/**
	 * Find by ID of reference: customer.id
	 *
	 * @param customerId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByCustomerId(Integer customerId);

	/**
	 * Find by reference: fuelQuote
	 *
	 * @param fuelQuote
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByFuelQuote(FuelQuotation fuelQuote);

	/**
	 * Find by ID of reference: fuelQuote.id
	 *
	 * @param fuelQuoteId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByFuelQuoteId(Integer fuelQuoteId);

	/**
	 * Find by reference: contact
	 *
	 * @param contact
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByContact(Contacts contact);

	/**
	 * Find by ID of reference: contact.id
	 *
	 * @param contactId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByContactId(Integer contactId);

	/**
	 * Find by reference: registeredBy
	 *
	 * @param registeredBy
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByRegisteredBy(UserSupp registeredBy);

	/**
	 * Find by ID of reference: registeredBy.id
	 *
	 * @param registeredById
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByRegisteredById(String registeredById);

	/**
	 * Find by reference: subsidiary
	 *
	 * @param subsidiary
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findBySubsidiary(Customer subsidiary);

	/**
	 * Find by ID of reference: subsidiary.id
	 *
	 * @param subsidiaryId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findBySubsidiaryId(Integer subsidiaryId);

	/**
	 * Find by reference: fuelOrderLocations
	 *
	 * @param fuelOrderLocations
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByFuelOrderLocations(
			FuelOrderLocation fuelOrderLocations);

	/**
	 * Find by ID of reference: fuelOrderLocations.id
	 *
	 * @param fuelOrderLocationsId
	 * @return List<FuelOrder>
	 */
	public List<FuelOrder> findByFuelOrderLocationsId(
			Integer fuelOrderLocationsId);
}
