/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
package atraxo.ops.business.api.fuelTransaction;

import atraxo.ops.domain.impl.fuelTransaction.FuelTransaction;
import atraxo.ops.domain.impl.fuelTransaction.FuelTransactionQuantity;
import java.util.List;
import seava.j4e.api.service.business.IEntityService;

/**
 * Interface to expose business functions specific for {@link FuelTransactionQuantity} domain entity.
 */
public interface IFuelTransactionQuantityService
		extends
			IEntityService<FuelTransactionQuantity> {

	/**
	 * Find by unique key
	 *
	 * @return FuelTransactionQuantity
	 */
	public FuelTransactionQuantity findByBusiness(Integer id);

	/**
	 * Find by reference: fuelTransaction
	 *
	 * @param fuelTransaction
	 * @return List<FuelTransactionQuantity>
	 */
	public List<FuelTransactionQuantity> findByFuelTransaction(
			FuelTransaction fuelTransaction);

	/**
	 * Find by ID of reference: fuelTransaction.id
	 *
	 * @param fuelTransactionId
	 * @return List<FuelTransactionQuantity>
	 */
	public List<FuelTransactionQuantity> findByFuelTransactionId(
			Integer fuelTransactionId);
}
