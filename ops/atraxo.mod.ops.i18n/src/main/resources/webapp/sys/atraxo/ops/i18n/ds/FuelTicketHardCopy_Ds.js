Ext.define("atraxo.ops.i18n.ds.FuelTicketHardCopy_Ds", {
	file__lbl: ".File",
	fuelTicketId__lbl: ".Fuel Ticket(ID)",
	fuelTicketNr__lbl: ".Ticket #",
	fuelTicketNr__tlp: ".Fuel ticket number"
});
