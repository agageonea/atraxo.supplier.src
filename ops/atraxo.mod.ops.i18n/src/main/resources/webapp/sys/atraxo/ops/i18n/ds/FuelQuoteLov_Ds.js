Ext.define("atraxo.ops.i18n.ds.FuelQuoteLov_Ds", {
	code__lbl: ".Code",
	code__tlp: ".Code",
	customerCode__lbl: ".Customer",
	customerCode__tlp: ".Account code",
	customerId__lbl: ".Customer(ID)",
	status__lbl: ".Status",
	status__tlp: ".Status",
	subsidiaryCode__lbl: ".Issuer",
	subsidiaryCode__tlp: ".Account code",
	subsidiaryId__lbl: ".Holder(ID)",
	subsidiaryName__lbl: ".Holder(Name)",
	subsidiaryName__tlp: ".Name",
	validUntil__lbl: ".Valid until",
	validUntil__tlp: ".Valid until"
});
