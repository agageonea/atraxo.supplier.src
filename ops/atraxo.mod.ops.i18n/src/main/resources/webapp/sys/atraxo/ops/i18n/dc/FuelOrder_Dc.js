
Ext.define("atraxo.ops.i18n.dc.FuelOrder_Dc$Edit", {
	fuelQuoteCode__lbl: ".Quote #"
});

Ext.define("atraxo.ops.i18n.dc.FuelOrder_Dc$Filter", {
	subsidiaryCode__lbl: ".Receiver",
	subsidiaryCode__tlp: ".Receiver"
});

Ext.define("atraxo.ops.i18n.dc.FuelOrder_Dc$List", {
	subsidiaryCode__lbl: ".Receiver",
	subsidiaryCode__tlp: ".Receiver"
});
