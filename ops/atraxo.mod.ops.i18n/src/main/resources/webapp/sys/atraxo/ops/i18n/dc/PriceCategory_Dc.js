
Ext.define("atraxo.ops.i18n.dc.PriceCategory_Dc$PriceCategoryDetails", {
	comments__lbl: ".Comments",
	comments__tlp: ".Comments",
	continous__lbl: ".Continuous?",
	continous__tlp: ".Continuous?",
	period__lbl: ".Period",
	period__tlp: ".Period",
	quantityType__lbl: ".Quantity type",
	quantityType__tlp: ".Quantity type",
	vat__lbl: ".VAT",
	vat__tlp: ".VAT"
});

Ext.define("atraxo.ops.i18n.dc.PriceCategory_Dc$Totals", {
	total__lbl: ".Total converted price:"
});
