Ext.define("atraxo.ops.i18n.ds.CustomerLocationContracts_Ds", {
	avgMthId__lbl: ".Avg Mth Id",
	basePrice__lbl: ".Base Price",
	basePrice__tlp: ".Base Price",
	code__lbl: ".Contract",
	code__tlp: ".Contract",
	counterPartyCode__lbl: ".Supplier",
	counterPartyCode__tlp: ".Supplier",
	counterPartyId__lbl: ".Supplier(ID)",
	creditTerms__lbl: ".Credit terms",
	creditTerms__tlp: ".Credit terms",
	currencyCode__lbl: ".Settlement Curr",
	currencyCode__tlp: ".Alphabetic code",
	currencyId__lbl: ".Settlement Curr(ID)",
	delivery__lbl: ".Delivery",
	delivery__tlp: ".Delivery",
	dft__lbl: ".DFT",
	dft__tlp: ".DFT",
	differential__lbl: ".Differential",
	differential__tlp: ".Differential",
	endsOn__lbl: ".Ends On",
	exchangeRateOffset__lbl: ".Period",
	exchangeRateOffset__tlp: ".Period",
	exposure__lbl: ".Exposure %",
	exposure__tlp: ".Exposure %",
	finSrcId__lbl: ".Fin Src Id",
	fqlCurrency__lbl: ".Fql Currency",
	fqlUnit__lbl: ".Fql Unit",
	fuelPrice__lbl: ".Fuel Price",
	fuelPrice__tlp: ".Fuel Price",
	intoPlaneFee__lbl: ".Into Plane Fee",
	intoPlaneFee__tlp: ".Into Plane Fee",
	iplAgentCode__lbl: ".IPL Agent",
	iplAgentCode__tlp: ".IPL Agent",
	iplAgentId__lbl: ".Into Plane Agent(ID)",
	limitedTo__lbl: ".Dom/Int",
	limitedTo__tlp: ".Dom/Int",
	locId__lbl: ".Location(ID)",
	locName__lbl: ".Location(Name)",
	locName__tlp: ".Name",
	otherFees__lbl: ".Other Fees",
	otherFees__tlp: ".Other Fees",
	paymentTerms__lbl: ".Payment terms",
	paymentTerms__tlp: ".Payment terms",
	perFlightFee__lbl: ".Per Flight Fee",
	perFlightFee__tlp: ".Per Flight Fee",
	period__lbl: ".Location quote period",
	period__tlp: ".EXCHANGE_RATE_OFFSET",
	pricingBaseId__lbl: "",
	pricingBases__lbl: ".Price basis",
	pricingBases__tlp: ".Price basis",
	quotationId__lbl: "",
	quotationName__lbl: ".Quotation",
	quotationName__tlp: ".Quotation",
	scope__lbl: ".Scope",
	scope__tlp: ".Scope",
	startsOn__lbl: ".Starts On",
	taxes__lbl: ".Taxes",
	taxes__tlp: ".Taxes",
	totalPrice__lbl: ".Total Price",
	totalPrice__tlp: ".Total Price",
	type__lbl: ".Type",
	type__tlp: ".Type",
	unitCode__lbl: ".Settlement Unit",
	unitCode__tlp: ".Code",
	unitId__lbl: ".Settlement Unit(ID)",
	unit__lbl: ".Unit",
	unit__tlp: ".Unit"
});
