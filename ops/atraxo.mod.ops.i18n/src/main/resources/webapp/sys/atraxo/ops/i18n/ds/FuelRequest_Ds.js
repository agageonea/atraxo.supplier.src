Ext.define("atraxo.ops.i18n.ds.FuelRequest_Ds", {
	action__lbl: ".Action",
	contactId__lbl: ".Contact(ID)",
	contactName__lbl: ".Contact person",
	contactName__tlp: ".Contact person",
	customerCode__lbl: ".Customer",
	customerCode__tlp: ".Account code",
	customerEntityAlias__lbl: ".Entity Alias",
	customerId__lbl: ".Customer(ID)",
	customerRefId__lbl: ".Customer(Ref-ID)",
	customerReferenceNo__lbl: ".Reference number",
	customerReferenceNo__tlp: ".Reference number",
	endDate__lbl: ".To",
	endDate__tlp: ".Valid to?",
	fuelQuotationCode__lbl: ".Fuel Quotation",
	fuelQuotationCode__tlp: ".Code",
	fuelQuotationId__lbl: ".Fuel Quotation(ID)",
	isOpenRelease__lbl: ".Open release?",
	isOpenRelease__tlp: ".Open release?",
	labelField__lbl: "",
	processedBy__lbl: ".Captured by",
	processedBy__tlp: ".Captured by?",
	quotNo__lbl: ".Quote No.",
	quotNo__tlp: ".Fuel Quote number",
	remarks__lbl: ".Remarks",
	requestCode__lbl: ".Request No.",
	requestCode__tlp: ".Request No.",
	requestDate__lbl: ".Received on",
	requestDate__tlp: ".Received on",
	requestStatus__lbl: ".Status",
	requestStatus__tlp: ".Status",
	requestedService__lbl: ".Requested service",
	requestedService__tlp: ".Requested service",
	startDate__lbl: ".From",
	startDate__tlp: ".Valid from?",
	subsidiaryCode__lbl: ".Receiver",
	subsidiaryCode__tlp: ".Account code",
	subsidiaryId__lbl: ".Subsidiary(ID)",
	subsidiaryName__lbl: ".Subsidiary(Name)",
	subsidiaryName__tlp: ".Name",
	tmpField__lbl: ".TMP"
});
