
Ext.define("atraxo.ops.i18n.dc.FuelTicket_Dc$ApprovalNote", {
	approvalNote__lbl: ".Add your note to be sent with the request to approvers"
});

Ext.define("atraxo.ops.i18n.dc.FuelTicket_Dc$Edit", {
	acTypeLabel__lbl: ".Aircraft type",
	addRegistration__lbl: ".<i class='fa fa-plus'></i>",
	airlineDesLabel__lbl: ".Airline designator",
	customerLabel__lbl: ".Ship to",
	customsStatusLabel__lbl: ".Customs status",
	deliveryDateLabel__lbl: ".Delivery date",
	departureLabel__lbl: ".Departure",
	destinationLabel__lbl: ".Destination",
	finalDestLabel__lbl: ".Final destination",
	fuelerLabel__lbl: ".Fueler",
	fuelingOperLabel__lbl: ".Fueling operation",
	indicatorLabel__lbl: ".Flight type",
	productLabel__lbl: ".Product type",
	refTicketNo__lbl: ".Reference ticket",
	registrationLabel__lbl: ".Registration",
	registration__lbl: ".Aircraft registration",
	resellerLabel__lbl: ".Reseller",
	row1Label__lbl: ".Quantity",
	row2Label__lbl: ".Flight number",
	supplierLabel__lbl: ".Supplier",
	ticketNoLabel__lbl: ".Fuel ticket number"
});

Ext.define("atraxo.ops.i18n.dc.FuelTicket_Dc$Filter", {
	approvedBy__tlp: ".Name of the person who approved the Fuel Ticket",
	capturedBy__tlp: ".Name of the person who registered the Fuel Ticket"
});

Ext.define("atraxo.ops.i18n.dc.FuelTicket_Dc$List", {
	afterUnit__lbl: ".Unit",
	beforeUnit__lbl: ".Unit",
	btnHardCopy__lbl: ".Hard copy",
	btnHardCopy__tlp: ".Fuel ticket hard copy",
	customerName__lbl: ".Customer name",
	customer__lbl: ".Ship to",
	date__lbl: ".Date",
	densityMass__lbl: ".Mass",
	densityVol__lbl: ".Volume",
	flightID__lbl: ".Flight ID",
	netQuantUnit__lbl: ".Unit",
	upliftUnit__lbl: ".Unit"
});

Ext.define("atraxo.ops.i18n.dc.FuelTicket_Dc$NewFuelTicketNumber", {
	newFuelTicketNumber__lbl: ".New Fuel Ticket Number"
});

Ext.define("atraxo.ops.i18n.dc.FuelTicket_Dc$PopUp", {
	acTypeLabel__lbl: ".Aircraft type",
	addRegistration__lbl: ".<i class='fa fa-plus'></i>",
	airlineDesLabel__lbl: ".Airline designator",
	customerLabel__lbl: ".Ship to",
	customsStatusLabel__lbl: ".Customs status",
	deliveryDateLabel__lbl: ".Delivery date",
	departureLabel__lbl: ".Departure",
	destinationLabel__lbl: ".Destination",
	fuelReleaseLabel__lbl: ".Reseller",
	fuelerLabel__lbl: ".Fueler",
	fuelingOperLabel__lbl: ".Fueling operation",
	indicatorLabel__lbl: ".Flight type",
	productLabel__lbl: ".Product type",
	registrationLabel__lbl: ".Registration",
	row1Label__lbl: ".Quantity",
	row2Label__lbl: ".Flight number",
	row3Label__lbl: ".Density",
	row4Label__lbl: ".Temperature",
	row5Label__lbl: ".Net Quantity",
	supplierLabel__lbl: ".Supplier",
	ticketNoLabel__lbl: ".Fuel ticket #"
});

Ext.define("atraxo.ops.i18n.dc.FuelTicket_Dc$RevokeRemark", {
	cancelWindowText__lbl: ".You are about to revoke the selected fuel ticket(s). If ticket(s) have been invoiced and invoice has been released credit memo(s) will be generated automatically",
	emptyLine__lbl: ".	",
	revocationReason__lbl: ".Please indicate the revocation reason"
});

Ext.define("atraxo.ops.i18n.dc.FuelTicket_Dc$Tab", {
	fuelingEndLabel__lbl: ".Fueling end date",
	fuelingStartLabel__lbl: ".Fueling start date",
	meterEndIndexLabel__lbl: ".Meter end index",
	meterStartIndexLabel__lbl: ".Meter start index",
	refuelerArrivalLabel__lbl: ".Refueler arrival",
	row3Label__lbl: ".Net quantity",
	row4Label__lbl: ".Before fueling",
	row5Label__lbl: ".After fueling",
	row6Label__lbl: ".Density",
	row8Label__lbl: ".Temperature",
	transportLabel__lbl: ".Transport by"
});

Ext.define("atraxo.ops.i18n.dc.FuelTicket_Dc$TabPayment", {
	cardExpLabel__lbl: ".Card expiring date",
	cardHolderLabel__lbl: ".Name on card",
	cardNumberLabel__lbl: ".Card #",
	paymentTypeLabel__lbl: ".Payment type",
	row1Label__lbl: ".Amount received",
	row2Label__lbl: ".Send invoice by"
});
