
Ext.define("atraxo.ops.i18n.dc.CustomerLocationContracts_Dc$SupplierContracts", {
	contract__lbl: ".Contract",
	contract__tlp: ".Contract",
	credtiTerms__lbl: ".Credit terms",
	credtiTerms__tlp: ".Credit terms",
	delivery__lbl: ".Delivery",
	delivery__tlp: ".Delivery",
	dft__lbl: ".DFT",
	dft__tlp: ".DFT",
	domInt__lbl: ".Dom/Int",
	domInt__tlp: ".Dom/Int",
	exposure__lbl: ".Exposure %",
	exposure__tlp: ".Exposure %",
	fuelPrice__lbl: ".Fuel Price",
	fuelPrice__tlp: ".Fuel Price",
	paymentTerms__lbl: ".Payment terms",
	paymentTerms__tlp: ".Payment terms",
	pricingBase__lbl: ".Price basis",
	pricingBase__tlp: ".Price basis",
	scope__lbl: ".Scope",
	scope__tlp: ".Scope",
	supplier__lbl: ".Supplier",
	supplier__tlp: ".Supplier",
	totalPrice__lbl: ".Total Price",
	totalPrice__tlp: ".Total Price",
	type__lbl: ".Type",
	type__tlp: ".Type",
	unit__lbl: ".Unit",
	unit__tlp: ".Unit"
});
