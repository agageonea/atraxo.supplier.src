
Ext.define("atraxo.ops.i18n.dc.PricePolicy_Dc$Edit", {
	minimalMargin__lbl: ".Minimal"
});

Ext.define("atraxo.ops.i18n.dc.PricePolicy_Dc$Filter", {
	defaultMargin__lbl: ".Default Margin",
	defaultMarkup__lbl: ".Default Markup",
	validTo__lbl: ".Valid to"
});

Ext.define("atraxo.ops.i18n.dc.PricePolicy_Dc$List", {
	curency__lbl: ".Currency",
	defaultMargin__lbl: ".Default Margin",
	defaultMarkup__lbl: ".Default Markup",
	minimalMargin__lbl: ".Minimal Margin",
	minimalMarkup__lbl: ".Minimal Markup",
	unit__lbl: ".Unit",
	validTo__lbl: ".Valid to"
});
