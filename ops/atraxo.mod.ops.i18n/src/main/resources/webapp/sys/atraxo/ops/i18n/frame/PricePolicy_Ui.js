Ext.define("atraxo.ops.i18n.frame.PricePolicy_Ui", {
	/* view */
	wdwPricePolicy__ttl: ".New Pricing Policy",
	/* menu */
	/* button */
	btnOnEditWdwClose__lbl: ".Cancel",
	btnOnEditWdwClose__tlp: ".Cancel",
	btnSaveCloseWdw__lbl: ".Save & Close",
	btnSaveCloseWdw__tlp: ".Save & Close",
	btnSaveNewWdw__lbl: ".Save & New",
	btnSaveNewWdw__tlp: ".Save & New",
	helpWdw__lbl: ".Help",
	helpWdw__tlp: ".Help",
	helpWdwEdit__lbl: ".Help",
	helpWdwEdit__tlp: ".Help",
	
	title: ".Pricing Policies"
});
