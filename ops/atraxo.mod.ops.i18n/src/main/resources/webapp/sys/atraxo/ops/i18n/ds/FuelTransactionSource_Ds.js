Ext.define("atraxo.ops.i18n.ds.FuelTransactionSource_Ds", {
	fuelTransactions__lbl: ".Fuel transactions",
	fuelTransactions__tlp: ".Number of fuel transactions",
	name__lbl: ".File name",
	name__tlp: ".File name",
	processingDate__lbl: ".Processing date",
	processingDate__tlp: ".Date when the file was processed/imported"
});
