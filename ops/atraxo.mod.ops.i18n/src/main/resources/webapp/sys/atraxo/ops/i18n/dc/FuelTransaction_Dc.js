
Ext.define("atraxo.ops.i18n.dc.FuelTransaction_Dc$Edit", {
	acTypeLabel__lbl: ".Aircraft type",
	customerLabel__lbl: ".Ship to",
	customsStatusLabel__lbl: ".Customs status",
	deliveryDateLabel__lbl: ".Delivery date",
	departureLabel__lbl: ".Departure",
	destinationLabel__lbl: ".Destination",
	finalDestLabel__lbl: ".Final destination",
	flightIdLabel__lbl: ".Flight ID",
	flightTypeLabel__lbl: ".Flight type",
	fuelerLabel__lbl: ".Fueler",
	fuelingEndLabel__lbl: ".Fueling end at",
	fuelingOperLabel__lbl: ".Fueling operation",
	fuelingStartLabel__lbl: ".Fueling start at",
	productLabel__lbl: ".Product type",
	registrationLabel__lbl: ".Registration",
	registration__lbl: ".Registration",
	supplierLabel__lbl: ".Supplier",
	ticketIssuerLabel__lbl: ".Ticket issuer"
});

Ext.define("atraxo.ops.i18n.dc.FuelTransaction_Dc$List", {
	acType__lbl: ".Aircraft type",
	customer__lbl: ".Ship to",
	date__lbl: ".Date",
	departure__lbl: ".Departure",
	destination__lbl: ".Destination",
	finalDest__lbl: ".Final destination",
	flightId__lbl: ".Flight ID",
	importFileName__lbl: ".Import file",
	processingDate__lbl: ".Imported on",
	registration__lbl: ".Registration",
	supplier__lbl: ".Supplier",
	ticketNo__lbl: ".Ticket#",
	ticketType__lbl: ".Ticket type",
	validationResult__lbl: ".Validation result",
	validationState__lbl: ".Validation state"
});

Ext.define("atraxo.ops.i18n.dc.FuelTransaction_Dc$TabPayment", {
	cardExpLabel__lbl: ".Card expiring date",
	cardHolderLabel__lbl: ".Name on card",
	cardNumberLabel__lbl: ".Card #",
	paymentTypeLabel__lbl: ".Payment type",
	row1Label__lbl: ".Amount received"
});
