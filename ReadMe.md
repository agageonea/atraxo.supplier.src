# Supplier.One

### How to make relase

In the root is a batch file sone_release.bat

After start must enter the version(1.9.9.3).

The next step is the sone version. Default is 1. 

If you select 1 the source branch is master and the target is prod-1. 
If 2 the source branch is multitenant-spring and the target is prod-2.

If everything is ok you can start the build from jenkins. 

## Configure enviroment

###1. Directory structure:

The main directory I used was D:/work2 . All paths specified here will be relative to that path. Feel free to check with the structure during the configuration process. 
All the software to be installed will be provided in the next step.

1.1 Structure:

work2
|	env
|	|	ide
|	|	|	eclipse-meta <--- Instal Eclipse Standard here (including workspace settings)
|	|	|	eclipse-src <--- Instal STS here - (release folder)
|	|	sdk
|	|	|	java <--- Copy Java jdk 1.7.0 here
|	|	tools
|	|	|	ant <--- Instal Ant here
|	|	|	maven <--- Instal Maven here
|	|	|	db
|	|	web-server
|	|	|	tomcat <--- Instal Tomcat here
|	|	workspace
|	|	|	eclipse
|	|	|	|	eclipse-meta <--- set workspace for Eclipse Standard here
|	|	|	|	eclipse-src <--- set workspace for STS here
|	projects
|	|	lib
|	|	|	tomcat_lib <--- download from \\fs\atraxo\_common\supplier.ONE\Dev
|	|	mod <--- application specific libraries and projects (internal structure may vary depending on working project; this structure is specific for supplier-ONE project)
|	|	|	atraxo.supplier.src <--- git download
|   |   |   atraxo.supplier.meta <--- git download
|	|	prod
|	|	|	atraxo.dev <--- download from \\fs\atraxo\_common\supplier.ONE\Dev
|	|	web_app <--- download from \\fs\atraxo\_common\supplier.ONE\Dev
|	|
|

This directory structure can be created using this script. Copy it and save it as a .BAT file, then run it

	SET loc="D:\work2"

	md %loc%
	md %loc%\env
	md %loc%\env\.temp
	md %loc%\env\ide
	md %loc%\env\ide\eclipse-src
	md %loc%\env\ide\eclipse-meta
	md %loc%\env\tools
	md %loc%\env\tools\db
	md %loc%\env\tools\ant
	md %loc%\env\tools\maven
	md %loc%\env\sdk
	md %loc%\env\sdk\java
	md %loc%\env\sdk\ruby
	md %loc%\env\web-server
	md %loc%\env\web-server\apache
	md %loc%\env\web-server\tomcat
	md %loc%\env\workspace\atraxo
	md %loc%\env\workspace\eclipse\eclipse-src
	md %loc%\env\workspace\eclipse\eclipse-meta
	md %loc%\projects
	md %loc%\projects\commons
	md %loc%\projects\lib
	md %loc%\projects\mod
	md %loc%\projects\prod

###2. Prequisite software

Java: SE 7 
	jdk 1.7.0
	jre 7
url: http://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase7-521261.html


Eclipse Standard; version: Luna Release (4.4.0)
url: https://eclipse.org/downloads/packages/eclipse-standard-44/lunar
additional plug-in: \\fs\atraxo\_common\supplier.ONE\plugin\2015.07.01-v2-seava.dsl.all.updatesite.zip
	

Spring Tool Suite (STS); version: 3.7.0.RELEASE
url: https://spring.io/tools

Maven; version: Apache Maven 3.3.3
url: https://maven.apache.org/download.cgi
obs: download bin.zip

Tomcat; version: 8.0
url: http://tomcat.apache.org/download-80.cgi
obs: Download from binary distributions, Core, the first zip

MySQL Workbench; version: 6.3
url: https://dev.mysql.com/downloads/workbench/
obs: Does not require Oracle account, it's just a trick. Look for the fine text (free)

Ant; version: 1.9.6 (latest version)
url: http://ant.apache.org/bindownload.cgi

Git
url: https://git-scm.com/downloads

Install all programs (check with the file structure in order to see if the have a special instalation location)

2.1 Configuring enviorment variables

Add the folowing system variables (Names are mandatory, path depends if you followed the file structure indicated)

ANT_HOME	D:\work2\env\tools\ant
GIT_HOME	C:\Program Files(x86)\Git  <--- default instalation of Git on a x64 OS
JAVA_HOME	D:\work2\env\sdk\java
M2_HOME		D:\work2\env\tools\maven



###3 Generating private and public key for connecting to the Git server

Use Puttygen (google for download location) to generate a private and public key and save them in separated text 
files. Send the public key to the system administrator via e-mail and ask him to add your key to the SQL Server.
In both Eclipse Standard and STS you have to add the private key in order to be able to access the git repository.
In order to do this, in any of the two IDEs you are using follow these steps:
1 - Open the preferences window (Windos -> Preferences)
2 - In the search bar type SSH and select SSH2 from the left side; manually you can go to General -> 
	Network Connections -> SSH2 in the navigation panel on the left side
3 - In the General tab press "Add Private Key..."
4 - Select the text file where you saved your private key and press Open
5 - Press Apply and then OK

Note: You have to do this for both Eclipse Standard and STS

STS Setting:
	Window --> Preferences:
				- General -> Workspace -> Text file encoding -> UTF-8
				- Git -> check Ignore white space changes

3.1 Cloning repositories from git

In order to clone a git repository you will have to open up the "Git Repositories" view (Window -> Show View -> 	Other -> Git -> Git Repositories)
From this view select "Clone a Git repository". Ask for the URI for it may differ from project to project. Save the clone in a location in the D:/work2/projects/mod folder (create a separate folder for each one), depending on the use of the imported projects. I recommend that you clone all meta repositories with standard Eclipse and all src projects with STS.

If you import all repositories with standard Eclipse, you will have to disconnect the projects from Git after you import them in the workspace, and in STS you will only have to add an existing repository (in the Git Repositories view the "Add existing Git repository to this view" button and the select the directory where you downloaded the repository).



###3 Importing cloned projects into your workspace

3.1 Maven STS configuration
	
In D:\work2\env download lifecycle-mapping.xml from \\fs\atraxo\_common\supplier.ONE\Dev

After you downloaded this file you have to configure the Lifecycle Mappins for Maven in STS. In order to do this,
open the preferences window in STS, select Maven -> Lifecycle Mappings. Under "Chamge mapping file location:"
browse the location where you saved your "lifecycle-mappings.xml" file (D:\work2\env\lifecycle-mapping.xml).

Create C:\Users\[USERNAME]\.m2; Windows will not let you create the folder named .m2 unless you create it from a terminal using the 
command "mkdir .m2".
Download from \\fs\atraxo\_common\supplier.ONE\Dev the file named settings.xml and place it in C:\Users\[USERNAME]\.m2.

3.2 STS Java configuration 

In the STS preferences window type Java in the search bar and open the Installed JREs tab(manual Java->Installed JREs). There you will need to add the the jdk you downloaded earlier. To do this Add -> Standard VM -> Next -> Directory -> Search for where you installed java jdk 1.8 (It should be in D:\work2\env\sdk\java) -> OK. Now you have to select the newlly added library, so insead of the default selected jre, chose the one you have just added then press Apply and OK.
	

3.3 Importing projects

After you clone a git repository, you have to import the projects in that repository inside your IDE. For this,
right click anywhere inside in the Package Explorer view, and select "Import...". 

3.3.1 For Eclipse Standard, select General -> Existing Projects into Workspace, choose the root directory (the folder containing everything you 
downloaded form the Git server) and press Finish. 

3.3.2 For STS, the same steps apply only that instead of General -> Existing Projects into Workspace you will choose Maven -> Existing Maven Projects.

Obs: As a general rule, in STS we will import only the repositories that contain the source code (ending in src)
while in Eclipse Standard we will import both the src (abstracts, ad, fmbas, cmm, ops, acc, tender) and meta projects, separating them into different working sets (import src projects first)

3.4 Post import actions for STS

After you have imported all src projects you will have to navigate into each master project for each module and use the Maven install command (right click on the pom.xml from the the master project -> Run as -> Maven install (first on commons.src)). Do this beginning with the parent-global project in commons work-set. After you have done this for all the projects, select all, right click -> Maven -> Update Project. Do this before you import the atraxo.dev project into STS (downloaded form download from \\fs\atraxo\_common\supplier.ONE\Dev). If you imported this project already, select all projects except this one. After that it is recommended that you run a clean on all projects. To do this select all projects, open the Project tab in the toolbar and select clean.

It's important to add this lines in settings.xml from m2:

	<pluginRepository>
                    <id>atraxo-release</id>
                    <name>atraxo-release</name>
                    <url>http://maven:8081/artifactory/atraxo-release</url>
    </pluginRepository>


###4 Setting up the Tomcat server

You will have to do this only for STS. Open the servers View (Window -> Show View -> Other -> Server -> Servers)
Right-click inside the view and select New -> Server. Select the apropriate server type (Apache -> Tomcat v8.0 in this case) and press Finish.
Double-Click on the newly added server and the overview will appear. Select the modules tab and press "Add Web Module". For the path type in /atraxo and select the atraxo.lib.whj.index_jee web module and then press OK.
Now return to the overview tab and press "Open launch configuration". In the Arguments tab you will have to add to
the VM arguments the following arguments (just copy paste if you respected the directory structure I gave): -javaagent:d:\work2\env\web-server\tomcat\lib\spring-instrument-3.1.2.RELEASE.jar -Xms512m -Xmx1024m -XX:MaxPermSize=256m -DENVIRONMENT=mysql

You will need to download spring-instrument-3.1.2.RELEASE.jar and place it in d:\work2\env\web-server\tomcat\lib. 
Download from \\fs\atraxo\_common\supplier.ONE\Dev

In the overview tab of the Tomcat server, open the Timouts menu on the left and change the start in for whatever value it has to a value ranging form 200 to 300

After this you will need to add different jar's and projects into your classpath, 
but these vary from project to project.

Jars that you will need to get the server working:
	all the jars located in D:\work2\projects\lib\tomcat_lib
	spring-instrument-3.1.2.RELEASE.jar located in d:\work2\env\web-server\tomcat\lib

Projects that need to be added in ClassPath (Open launch configuration):
	atrxo.custom.sone
	atraxo.mod.abstracts  
	atraxo.mod.acc.bussiness
	atraxo.mod.acc.db
	atraxo.mod.acc.domain
	atraxo.mod.acc.presenter
	atraxo.mod.ad.bussiness
	atraxo.mod.ad.db
	atraxo.mod.ad.domain
	atraxo.mod.ad.presenter
	atraxo.mod.ad.security
	atraxo.mod.cmm.bussiness
	atraxo.mod.cmm.db
	atraxo.mod.cmm.domain
	atraxo.mod.cmm.presenter
	atraxo.mod.fmbas.bussiness
	atraxo.mod.fmbas.db
	atraxo.mod.fmbas.domain
	atraxo.mod.fmbas.presenter
	atraxo.mod.ops.bussiness
	atraxo.mod.ops.db
	atraxo.mod.ops.domain
	atraxo.mod.ops.presenter
	seava.lib.j4e
	atravo.dev

The projects that you need to include may vary depending on the application but generally you can include all the projects in your workspace, though not all may be needed.

	
 SRC
-----

ssh://git@brafs.haj.lan/git/atraxo-supplier/atraxo.supplier.src

 META
------

ssh://git@brafs/git/atraxo-supplier/atraxo.supplier.meta
