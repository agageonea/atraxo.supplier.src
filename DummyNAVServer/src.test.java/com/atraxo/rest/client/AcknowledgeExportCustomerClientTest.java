/**
 *
 */
package com.atraxo.rest.client;

import java.math.BigInteger;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.atraxo.rest.utils.XMLUtils;

import atraxo.domain.ws.Any;
import atraxo.domain.ws.Any.Acknowledgement;
import atraxo.domain.ws.Any.Acknowledgement.DataSets;
import atraxo.domain.ws.Any.Acknowledgement.DataSets.DataSet;
import atraxo.fmbas.business.ws.fromnav.AcknowledgeExportCustomers.Request;
import atraxo.fmbas.business.ws.fromnav.AcknowledgeExportCustomers.Request.HeaderCommon;
import atraxo.fmbas.business.ws.fromnav.AcknowledgeExportCustomers.Request.Payload;
import atraxo.fmbas.business.ws.fromnav.AcknowledgeExportCustomersResponse.Return;

/**
 * @author vhojda
 */
public class AcknowledgeExportCustomerClientTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAcknowledge() {

		Request request = new Request();
		HeaderCommon header = new HeaderCommon();
		request.setHeaderCommon(header);
		Payload value = new Payload();
		request.setPayload(value);

		Any anyAcknowledgement = new Any();
		Acknowledgement acknowledgement = new Acknowledgement();
		anyAcknowledgement.setAcknowledgement(acknowledgement);
		DataSets dataSets = new DataSets();
		DataSet dataSet = new DataSet();
		dataSet.setDescriprion("description for testing purpose");
		dataSet.setName("ATRAXO");
		dataSet.setCustomerAccountNumber(1234 + "");
		dataSet.setTransactionID(1234 + "");
		dataSet.setErpNumber(234 + "");
		dataSet.setTransactionID(2354245 + "");
		dataSet.setLogEntryNo(new BigInteger("100"));
		dataSet.setProcessed(true);
		dataSet.setDelivered(true);
		dataSets.getDataSet().add(dataSet);
		acknowledgement.setDataSets(dataSets);

		Assert.assertTrue(anyAcknowledgement != null);

		try {
			value.setAny(XMLUtils.toXML(anyAcknowledgement, Acknowledgement.class));
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		AcknowledgeExportCustomersClient client = new AcknowledgeExportCustomersClient();
		Assert.assertTrue(client != null);

		Return response = client.acknowledge(request);
		Assert.assertTrue(response != null);
	}

}
