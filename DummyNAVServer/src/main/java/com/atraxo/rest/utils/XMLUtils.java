package com.atraxo.rest.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author vhojda
 */
public class XMLUtils {

	final static String DEFAULT_JAXB_ENCODING = "UTF-8";// $NON-NLS-N$

	/**
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		XMLGregorianCalendar returnedDate = null;
		try {
			returnedDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return returnedDate;
	}

	/**
	 * @param xml
	 * @return
	 * @throws FactoryConfigurationError
	 * @throws XMLStreamException
	 */
	public static Object toObject(Element xmlElement, Class aClass, int var)
			throws JAXBException, XMLStreamException, FactoryConfigurationError, IllegalArgumentException {
		String msgXml = toString(xmlElement, true, false);
		Object msg = null;
		if (var == 1) {
			msg = toObject1(msgXml, aClass);
		} else {
			msg = toObject2(msgXml, aClass);
		}
		return msg;
	}

	/**
	 * @param node
	 * @param omitXmlDeclaration
	 * @param prettyPrint
	 * @return
	 */
	public static String toString(Node node, boolean omitXmlDeclaration, boolean prettyPrint) {
		if (node == null) {
			throw new IllegalArgumentException("node is null.");
		}

		try {
			// Remove unwanted whitespaces
			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = xpath.compile("//text()[normalize-space()='']");
			NodeList nodeList = (NodeList) expr.evaluate(node, XPathConstants.NODESET);

			for (int i = 0; i < nodeList.getLength(); ++i) {
				org.w3c.dom.Node nd = nodeList.item(i);
				nd.getParentNode().removeChild(nd);
			}

			// Create and setup transformer
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			if (omitXmlDeclaration == true) {
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			}

			if (prettyPrint == true) {
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			}

			// Turn the node into a string
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(node), new StreamResult(writer));
			return writer.toString();
		} catch (TransformerException e) {
			throw new RuntimeException(e);
		} catch (XPathExpressionException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param xml
	 * @return
	 */
	public static Object toObject2(String xml, Class aClass) {
		Object msg = null;
		try {
			InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
			XMLStreamReader xsr = XMLInputFactory.newFactory().createXMLStreamReader(is);
			XMLReaderWithoutNamespace xr = new XMLReaderWithoutNamespace(xsr);
			JAXBContext jc = JAXBContext.newInstance(aClass);
			Unmarshaller um = jc.createUnmarshaller();
			msg = um.unmarshal(xr);
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FactoryConfigurationError e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return msg;
	}

	/**
	 * @param xml
	 * @return
	 */
	public static Object toObject1(String xml, Class aClass) {
		Object msg = null;
		try {
			InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

			JAXBContext jaxbContext = JAXBContext.newInstance(aClass);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			msg = jaxbUnmarshaller.unmarshal(stream);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg;
	}

	/**
	 * @param object
	 * @return
	 * @throws JAXBException
	 */
	public static Element toXML(Object object, Class objectClass) throws JAXBException {

		DOMResult resultedDOM = new DOMResult();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(objectClass);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, DEFAULT_JAXB_ENCODING);
			jaxbMarshaller.marshal(object, resultedDOM);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw e;
		}

		Element rootElement = ((Document) resultedDOM.getNode()).getDocumentElement();
		return rootElement;
	}

	/**
	 * @param rootTag
	 * @return
	 */
	public static Element createRootElement(String rootTag) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder builder;
		try {
			builder = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		}
		Document doc = builder.newDocument();
		// create the root element node
		Element element = doc.createElement(rootTag);

		return element;
	}

}

class XMLReaderWithoutNamespace extends StreamReaderDelegate {
	public XMLReaderWithoutNamespace(XMLStreamReader reader) {
		super(reader);
	}

	@Override
	public String getAttributeNamespace(int arg0) {
		return "";
	}

	@Override
	public String getNamespaceURI() {
		return "";
	}
}
