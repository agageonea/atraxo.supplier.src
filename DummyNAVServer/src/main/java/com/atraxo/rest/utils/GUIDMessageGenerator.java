package com.atraxo.rest.utils;

import java.util.Random;

/**
 * Generator for unique Message IDs
 *
 * @author vhojda
 */
public class GUIDMessageGenerator {

	private static final String PREFIX = "GUID";
	private static final String SEPARATOR = "-";
	private static final int ROOT_COUNT = 99999999;

	/**
	 * @return
	 */
	public static String generateMessageID() {

		Random r = new Random(ROOT_COUNT);

		StringBuilder sb = new StringBuilder();
		sb.append(PREFIX);
		sb.append(SEPARATOR);
		sb.append("9999999");
		sb.append(SEPARATOR);
		sb.append(System.currentTimeMillis());
		sb.append(SEPARATOR);
		sb.append(r.nextInt(ROOT_COUNT));
		return sb.toString();
	}

	/**
	 * @param message
	 * @return
	 */
	public static String extractClientIdentifier(String message) {
		String id = null;
		int firstSeparatorIndex = message.indexOf(SEPARATOR) + 1;
		int secondSeparatorIndex = message.indexOf(SEPARATOR, firstSeparatorIndex);
		id = message.substring(firstSeparatorIndex, secondSeparatorIndex);
		return id;
	}
}
