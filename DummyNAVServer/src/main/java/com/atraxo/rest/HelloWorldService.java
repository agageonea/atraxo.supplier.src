package com.atraxo.rest;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@WebService
@Path("/hello")
public class HelloWorldService {

	@GET
	@Path("/{param}")
	public Response hello(@PathParam("param") String msg) {
		String output = "Hello  " + msg + "! We are rollin' !!!!";

		System.out.println(output);

		return Response.status(200).entity(output).build();

	}

	@GET
	@Path("/hello12")
	public Response helloWorld() {
		String output = "Hello World !!!!!!!!!!!!!!!!!";

		System.out.println(output);

		return Response.status(200).entity(output).build();
	}

}