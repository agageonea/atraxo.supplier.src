package com.atraxo.rest.client;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.xml.namespace.QName;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.apache.wss4j.dom.handler.WSHandlerConstants;

import atraxo.fmbas.business.ws.fromnav.AcknowledgeExportCustomers;
import atraxo.fmbas.business.ws.fromnav.AcknowledgeExportCustomersResponse;
import atraxo.fmbas.business.ws.fromnav.AcknowledgeExportCustomersWS;
import atraxo.fmbas.business.ws.fromnav.AcknowledgeExportCustomersWSImplService;
import atraxo.fmbas.business.ws.fromnav.BusinessException_Exception;
import atraxo.fmbas.business.ws.fromnav.IOException_Exception;
import atraxo.fmbas.business.ws.fromnav.IllegalAccessException_Exception;
import atraxo.fmbas.business.ws.fromnav.InstantiationException_Exception;
import atraxo.fmbas.business.ws.fromnav.JAXBException_Exception;
import atraxo.fmbas.business.ws.fromnav.SAXException_Exception;
import message_requestresponse.schemas.eaviation.common.pumaenergy.MSG;

public class AcknowledgeExportCustomersClient implements CallbackHandler {

	private static final QName SERVICE_NAME = new QName("http://fromNav.ws.business.fmbas.atraxo/", "AcknowledgeExportCustomersWSImplService");

	public MSG acknowledge(MSG message) {

		AcknowledgeExportCustomersResponse response = null;

		URL wsdlURL = AcknowledgeExportCustomersWSImplService.WSDL_LOCATION;
		AcknowledgeExportCustomersWSImplService ss = new AcknowledgeExportCustomersWSImplService(wsdlURL, SERVICE_NAME);
		AcknowledgeExportCustomersWS port = ss.getAcknowledgeExportCustomersWSImplPort();

		System.out.println("Invoking acknowledgeExportCustomers on " + wsdlURL + ".....");

		try {

			Client client = ClientProxy.getClient(port);

			Endpoint cxfEndpoint = client.getEndpoint();

			Map<String, Object> outProps = new HashMap<String, Object>();

			outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.SIGNATURE);
			outProps.put(WSHandlerConstants.SIGNATURE_USER, "myalias");
			outProps.put(WSHandlerConstants.SIG_KEY_ID, "DirectReference");
			outProps.put(WSHandlerConstants.PW_CALLBACK_CLASS, AcknowledgeExportCustomersClient.class.getName());
			outProps.put(WSHandlerConstants.SIG_PROP_FILE, "client_sign.properties");

			WSS4JOutInterceptor wssOut = new WSS4JOutInterceptor(outProps);
			cxfEndpoint.getOutInterceptors().add(wssOut);

			AcknowledgeExportCustomers acknowledgeExportCustomers = new AcknowledgeExportCustomers();

			acknowledgeExportCustomers.setMSG(message);
			response = port.acknowledgeExportCustomers(acknowledgeExportCustomers);

			System.out.println("acknowledgeExportCustomers.result=" + response.getMSG());
			System.out.println("Header common:" + response.getMSG().getHeaderCommon());
			System.out.println("Payload:" + response.getMSG().getPayload());

		} catch (IOException_Exception e) {
			System.out.println("Expected exception: IOException has occurred.");
			System.out.println(e.toString());
		} catch (JAXBException_Exception e) {
			System.out.println("Expected exception: JAXBException has occurred.");
			System.out.println(e.toString());
		} catch (BusinessException_Exception e) {
			System.out.println("Expected exception: BusinessException has occurred.");
			System.out.println(e.toString());
		} catch (SAXException_Exception e) {
			System.out.println("Expected exception: SAXException has occurred.");
			System.out.println(e.toString());
		} catch (InstantiationException_Exception e) {
			System.out.println("Expected exception: InstantiationException_Exception has occurred.");
			System.out.println(e.toString());
		} catch (IllegalAccessException_Exception e) {
			System.out.println("Expected exception: IllegalAccessException_Exception has occurred.");
			System.out.println(e.toString());
		}

		return response.getMSG();
	}

	@Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		pc.setPassword("keyStorePassword");
	}

}
