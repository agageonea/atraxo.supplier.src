package com.atraxo.rest;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atraxo.rest.utils.XmlFormatter;

@WebService
@Path("/fuelEvents")
public class FuelEventProcessService {

	@WebMethod
	@POST
	@Path("/exportToNav")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	public Response exportFuelEventsToNAV(String message) {

		System.out.println("---------------START EXPORT FUEL EVENTS---------------");
		String prettyPrintMSG = XmlFormatter.format(message);
		System.out.println(prettyPrintMSG);
		System.out.println("---------------END EXPORT FUEL EVENTS---------------");

		return Response.status(200).entity(message).type(MediaType.APPLICATION_XML).build();
	}
}
