package com.atraxo.rest;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import com.atraxo.rest.client.AcknowledgeExportCustomersClient;
import com.atraxo.rest.utils.XMLUtils;
import com.atraxo.rest.utils.XmlFormatter;

import atraxo.domain.ws.Any;
import atraxo.domain.ws.Any.Acknowledgement;
import atraxo.domain.ws.Any.Acknowledgement.DataSets;
import atraxo.domain.ws.Any.Acknowledgement.DataSets.DataSet;
import message_requestresponse.schemas.eaviation.common.pumaenergy.MSG;
import message_requestresponse.schemas.eaviation.common.pumaenergy.MSG.HeaderCommon;
import message_requestresponse.schemas.eaviation.common.pumaenergy.MSG.Payload;

@WebService
@Path("/customers")
public class CustomersService {

	@WebMethod
	@POST
	@Path("/export")
	@Consumes({ "application/xml", "application/json", "text/plain" })
	@Produces({ "application/xml", "application/json", "text/plain" })
	public Response exportCustomers(String msg) {

		String prettyPrintMSG = XmlFormatter.format(msg);

		System.out.println("---------------START EXPORT CUSTOMER---------------");
		System.out.println(prettyPrintMSG);

		AckExportCustomersThread ackThread = new AckExportCustomersThread(msg);
		ackThread.start();

		System.out.println("---------------END EXPORT CUSTOMER---------------");

		return Response.status(200).entity("").build();

	}

	@WebMethod
	@POST
	@Path("/exportToEBits")
	public Response exportCustomersToEBits(String msg) {
		String prettyPrintMSG = XmlFormatter.format(msg);
		return Response.status(200).entity(prettyPrintMSG).build();
	}

	private class AckExportCustomersThread extends Thread {

		private String msg;

		/**
		 * @param msg
		 */
		public AckExportCustomersThread(String msg) {
			super();
			this.msg = msg;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {

			try {
				Thread.sleep(30 * 60 * 1000); // 30 minutes
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			MSG receivedMessage = (MSG) XMLUtils.toObject1(this.msg, MSG.class);

			message_requestresponse.schemas.eaviation.common.pumaenergy.MSG request = new message_requestresponse.schemas.eaviation.common.pumaenergy.MSG();
			HeaderCommon header = new HeaderCommon();
			header.setCorrelationID(receivedMessage.getHeaderCommon().getMsgID());

			String xml = XMLUtils.toString(receivedMessage.getPayload().getAny(), true, false);

			List<String> codes = CustomersService.this.getCustomerCodesFromXML(xml);
			for (String code : codes) {
				header.setMsgID(Double.toString(Math.random()));
				header.setProcessStatus("Success");
				header.setTransportStatus("Processed");
				header.setUpdateSourceOnDelivery(false);
				header.setUpdateSourceOnReceive(false);
				header.setDestinationAddress("");
				header.setSourceResponseAddress("");
				header.setSourceUpdateStatusAddress("");
				header.setDestinationUpdateStatusAddress("");
				header.setMiddlewareUrlForPush("");
				header.setErrorCode("");
				header.setErrorDescription("");
				header.setProcessingErrorDescription("");
				header.setProcessStatus("");
				header.setObjectType("");
				header.setObjectName("Customer");
				header.setCommunicationType("Async");
				header.setEmailNotification("");

				header.setDestinationCompany("");
				header.setSourceCompany("");

				request.setHeaderCommon(header);
				Payload value = new Payload();
				request.setPayload(value);

				Any anyAck = new Any();
				Acknowledgement acknowledgement = new Acknowledgement();
				anyAck.setAcknowledgement(acknowledgement);
				acknowledgement.setAckType("");
				DataSets data = new DataSets();
				DataSet set = new DataSet();
				set.setCustomerAccountNumber(code);
				set.setDateTimeStamp(XMLUtils.toXMLGregorianCalendar(new Date()));
				set.setDelivered(true);
				set.setDescriprion("Descrtiption");
				set.setErpNumber(CustomersService.this.getRandom() + "");
				set.setLogEntryNo(new BigInteger("999"));
				set.setName("Customer");
				set.setProcessed(Boolean.TRUE);
				set.setTransactionID(System.currentTimeMillis() + "");
				data.getDataSet().add(set);
				acknowledgement.setDataSets(data);

				try {
					value.setAny(XMLUtils.toXML(anyAck, anyAck.getClass()));
				} catch (JAXBException e) {
					e.printStackTrace();
				}

				System.out.println("SENDING ACK for Export Customer ............." + code);
				try {
					AcknowledgeExportCustomersClient client = new AcknowledgeExportCustomersClient();
					client.acknowledge(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("DONE !");

			}
		}

	}

	private List<String> getCustomerCodesFromXML(String xml) {
		List<String> ids = new ArrayList<String>();

		String tag1 = "<CustomerAccountNumber>";
		String tag2 = "</CustomerAccountNumber>";

		int index = xml.indexOf(tag1);
		while (index != -1) {
			int index1 = xml.indexOf(tag1);
			int index2 = xml.indexOf(tag2);
			if (index1 != -1 && index2 != -1) {
				String id = xml.substring(index1 + tag1.length(), index2);
				ids.add(id);
				xml = xml.substring(index2 + tag2.length());
				index = xml.indexOf(tag1);
			} else {
				break;
			}
		}
		return ids;

		// TODO make this work
		// FuelPlusCustomerAccount customerAccount = null;
		// try {
		// customerAccount = (FuelPlusCustomerAccount) XMLUtils.toObject(el, FuelPlusCustomerAccount.class, 2);
		// } catch (IllegalArgumentException e1) {
		// e1.printStackTrace();
		// } catch (JAXBException e1) {
		// e1.printStackTrace();
		// } catch (XMLStreamException e1) {
		// e1.printStackTrace();
		// } catch (FactoryConfigurationError e1) {
		// e1.printStackTrace();
		// }
		// customerID = customerAccount.getCustomer().getFPEntryNo();
	}

	private int getRandom() {
		Random random = new Random();
		Integer nr = random.nextInt(9999999);
		if (nr < 0) {
			nr = nr * (-1);
		}
		return nr;
	}
}