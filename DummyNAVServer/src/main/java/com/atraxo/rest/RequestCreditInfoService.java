package com.atraxo.rest;

import javax.jws.WebMethod;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RequestCreditInfoService {

	@WebMethod
	@POST
	@Path("/requestCredit")
	@Consumes({ "application/xml", "application/json", "text/plain" })
	@Produces({ "application/xml", "application/json", "text/plain" })
	public Response requestCredit(String message) {

		return Response.status(200).entity(message).type(MediaType.APPLICATION_XML).build();
	}
}