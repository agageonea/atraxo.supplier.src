package com.atraxo.rest;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atraxo.rest.utils.XmlFormatter;

@WebService
@Path("/contracts")
public class ContractsService {

	@WebMethod
	@POST
	@Path("/exportToEBits")
	@Consumes({ "application/xml", "application/json", "text/plain" })
	@Produces({ "application/xml", "application/json", "text/plain" })
	public Response exportContracts(String msg) {

		String prettyPrintMSG = XmlFormatter.format(msg);

		System.out.println("---------------START EXPORT CONTRACTS---------------");
		System.out.println(prettyPrintMSG);

		System.out.println("---------------END EXPORT CONTRACTS---------------");

		return Response.status(200).entity(prettyPrintMSG).build();

	}

}