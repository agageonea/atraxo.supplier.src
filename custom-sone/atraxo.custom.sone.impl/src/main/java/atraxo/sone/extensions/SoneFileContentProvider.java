package atraxo.sone.extensions;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import seava.j4e.api.ISettings;
import seava.j4e.api.extensions.IExtensionFile;
import seava.j4e.api.extensions.IExtensionProvider;
import seava.j4e.api.extensions.IExtensions;

/**
 * Extension provider
 */
public class SoneFileContentProvider implements IExtensionProvider {

	@Autowired
	private ISettings settings;

	@Override
	public List<IExtensionFile> getFiles(String targetName) throws Exception {

		List<IExtensionFile> extensionFileList = new ArrayList<>();
		String productVersion = this.settings.getProductVersion();
		String appMenu = this.settings.get("applicationMenu");
		boolean portalMenu = false;
		if (appMenu != null) {
			portalMenu = "PortalApplicationMenu".equalsIgnoreCase(appMenu);
		}

		if (targetName.equals(UI_EXTJS_MAIN)) {
			if (portalMenu) {
				extensionFileList.add(new PortalApplicationMenuExtensionFile(productVersion));
				extensionFileList.add(new PortalHomeExtensionFile(productVersion));
			} else {
				extensionFileList.add(new SoneApplicationMenuExtensionFile(productVersion));
				extensionFileList.add(new SoneDashboardExtensionFile(productVersion));
			}
			// -----------------------------------------------
			// Dan: 05.02.2015
			// Start: Add support for Font Awesome glyph icons
			// -----------------------------------------------

			extensionFileList.add(new FontAwesomeExtensionFile(productVersion));
			extensionFileList.add(new OpenSansExtensionFile(productVersion)); // Support for Open Sans
			extensionFileList.add(new FinanceSolidExtensionFile(productVersion));
			extensionFileList.add(new Icon7StrokeExtensionFile(productVersion));

			// -----------------------------------------------
			// Dan: 06.02.2015
			// Start: Add support for Application menu types
			// -----------------------------------------------

			extensionFileList.add(new SoneApplicationMenuTypesExtensionFile(productVersion));
		}

		else if (!targetName.matches(IExtensions.UI_EXTJS_DASHBOARD)) {

			extensionFileList.add(new FontAwesomeExtensionFile(productVersion));
			extensionFileList.add(new Icon7StrokeExtensionFile(productVersion));
			extensionFileList.add(new OpenSansExtensionFile(productVersion)); // Support for Open Sans
			extensionFileList.add(new FinanceSolidExtensionFile(productVersion));
			extensionFileList.add(new SoneTypesExtensionFile(productVersion));
			extensionFileList.add(new TypeExtensionFile_Acc(productVersion));
			extensionFileList.add(new TypeExtensionFile_Ad(productVersion));
			extensionFileList.add(new TypeExtensionFile_Cmm(productVersion));
			extensionFileList.add(new TypeExtensionFile_Fmbas(productVersion));
			extensionFileList.add(new TypeExtensionFile_Ops(productVersion));
			extensionFileList.add(new ExternalReportLovDs_AdExt(productVersion));
			extensionFileList.add(new ExternalReportLov_AdExt(productVersion));
		}

		// ---------------------------------------------
		// End: Add support for Font Awesome glyph icons
		// ---------------------------------------------

		if ("atraxo.fmbas.ui.extjs.frame.Suppliers_Ui".equalsIgnoreCase(targetName)) {
			extensionFileList.add(new SupplierContractExtensionFile(productVersion));
		}

		if ("atraxo.fmbas.ui.extjs.frame.Customers_Ui".equalsIgnoreCase(targetName)) {
			extensionFileList.add(new CustomerContractExtensionFile(productVersion));
		}

		return extensionFileList;
	}
}
