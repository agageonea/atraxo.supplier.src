package atraxo.sone.extensions;

import seava.j4e.api.extensions.IExtensionFile;

/**
 * Extension provider
 */
public class OpenSansExtensionFile implements IExtensionFile {

	private String productVersion;

	/**
	 * @param productVersion
	 */
	public OpenSansExtensionFile(String productVersion) {
		this.productVersion = productVersion;
	}

	@Override
	public boolean isJs() throws Exception {
		return false;
	}

	@Override
	public boolean isCss() {
		return true;
	}

	@Override
	public String getFileExtension() {
		return ".css";
	}

	@Override
	public String getLocation() {
		return "font-open-sans/css/open-sans.css" + "?version=" + this.productVersion;
	}

	@Override
	public void setLocation(String location) {
		// nothing to do
	}

	@Override
	public boolean isRelativePath() {
		return true;
	}

	@Override
	public void setRelativePath(boolean relativePath) {
		// nothing to do
	}

}
