package atraxo.sone.extensions;

import seava.j4e.api.extensions.IExtensionFile;

/**
 * Extension provider for application menu
 */
public class SoneApplicationMenuExtensionFile implements IExtensionFile {

	private String productVersion;

	/**
	 * @param productVersion
	 */
	public SoneApplicationMenuExtensionFile(String productVersion) {
		this.productVersion = productVersion;
	}

	@Override
	public boolean isJs() throws Exception {
		return true;
	}

	@Override
	public boolean isCss() {
		return false;
	}

	@Override
	public String getFileExtension() {
		return ".js";
	}

	@Override
	public String getLocation() {
		return "js/e4e/base/SoneApplicationMenu.js" + "?version=" + this.productVersion;
	}

	@Override
	public void setLocation(String location) {
		// nothing to do
	}

	@Override
	public boolean isRelativePath() {
		return true;
	}

	@Override
	public void setRelativePath(boolean relativePath) {
		// nothing to do
	}

}
