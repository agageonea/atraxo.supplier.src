__APPMENU_TYPES__ = {
		
		bundles: {
			ops: "atraxo.mod.ops",
			cmm: "atraxo.mod.cmm",
			acc: "atraxo.mod.acc",
			fmbas: "atraxo.mod.fmbas"
		},
		frames: {
			fuelRequest: "atraxo.ops.ui.extjs.frame.FuelRequests_Ui",
			fuelQuote: "atraxo.ops.ui.extjs.frame.FuelQuotation_Ui",
			fuelTicket: "atraxo.ops.ui.extjs.frame.FuelTicket_Ui",
			salesContracts: "atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui",
			purchaseContracts: "atraxo.cmm.ui.extjs.frame.Contract_Ui",
			incomingInvoices: "atraxo.acc.ui.extjs.frame.Invoices_Ui"
		},
		glyphs: {
			fontAwesome: {
				create: "xf055@FontAwesome",
				session: "xf011@FontAwesome",
				profile: "xf007@FontAwesome",
				help: "xf059@FontAwesome",
				hideKPI : "xf108@FontAwesome"
			}
		}
}