/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 */
Ext.ns("e4e.base");


/**
 * Helper functions to build menu
 */
e4e.base.SoneApplicationMenu$Helper = {
    /**
     * - return the theme items
     * - mark the currently used 
     */
    getThemeMenuItems: function() {
        var menuItems = e4e.base.SoneApplicationMenu$Themes;
        var id = Ext.util.Cookies.get(Constants.COOKIE_NAME_THEME);
        for (var i = 0; i < menuItems.length; i++) {
        	if (menuItems[i]._themeId_ === id) {
            	var checkGlyph = "&nbsp;&nbsp;<i class='fa fa-check' class='sone-sidebar-check-glyph'></i>";
            	menuItems[i].text = menuItems[i].text.replace(checkGlyph,"")+checkGlyph;
            }
        }
        return menuItems;
	},

	_supLangs_ : null,
	
	_createSupportedLanguagesItems_ : function() {
		if( this._supLangs_ ){
			return this._supLangs_;
		}
		
		try{
			if( !Ext.isArray(supLangCodes) ){
				supLangCodes = [];
			}
		}catch(e){
			supLangCodes = [];
		}
		
		var res = [];
		var chgLang = function(btn){
			getApplication().changeCurrentLanguage(btn._langId_);
		};
		
		for (var i = 0; i < supLangCodes.length; i++) {
			var k = supLangCodes[i];
			var item = {
				xtype : 'button',
				autoEl : 'li',
				_langId_ : k._langId_,
				iconCls : "sone-lang-" + k._langId_,
				text : k.text,
				handler : chgLang
			};
			res.push(item);
		}
		
		this._supLangs_ = res;
		return res;
	},

    /**
     * - return the language items
     * - mark the currently used 
     */
    getLanguageMenuItems: function() {
        var menuItems = this._createSupportedLanguagesItems_();
        var id = Ext.util.Cookies.get(Constants.COOKIE_NAME_LANG);
        for (var i = 0; i < menuItems.length; i++) {
        	if (menuItems[i]._langId_ === id) {
            	var checkGlyph = "&nbsp;&nbsp;<i class='fa fa-check' class='sone-sidebar-check-glyph'></i>";
            	menuItems[i].text = menuItems[i].text.replace(checkGlyph,"")+checkGlyph;
            }
        }
        return menuItems;
    }
};


/**
 * Contributed menus
 */
e4e.base.SoneApplicationMenu$ContributedMenus = [];

/**
 * Themes
 */
e4e.base.SoneApplicationMenu$Themes = [{
	xtype: 'button',
    autoEl: 'li',
    iconCls: 'sone-theme-clarity',
    _themeId_ : "ext-theme-clarity",
    text: Main.translate("appmenuitem", "theme_clarity__lbl"),
    handler: function() {
        getApplication().changeCurrentTheme("ext-theme-clarity");
    }
}, {
	xtype: 'button',
    autoEl: 'li',
    iconCls: 'sone-theme-aqua',
    _themeId_ : "ext-theme-skyblue",
    text: Main.translate("appmenuitem", "theme_skyblue__lbl"),
    handler: function() {
        getApplication().changeCurrentTheme("ext-theme-skyblue");
    }
}, 
{
	xtype: 'button',
    autoEl: 'li',
    iconCls: 'sone-theme-puma',
    _themeId_ : "ext-theme-puma",
    text: Main.translate("appmenuitem", "theme_puma__lbl"),
    handler: function() {
        getApplication().changeCurrentTheme("ext-theme-puma");
    }
},
{
	xtype: 'button',
    autoEl: 'li',
    iconCls: 'sone-theme-clearsky',
    _themeId_ : "ext-theme-clearsky",
    text: Main.translate("appmenuitem", "theme_clearsky__lbl"),
    handler: function() {
        getApplication().changeCurrentTheme("ext-theme-clearsky");
    }
}]



/**
 * Help items
 */
e4e.base.SoneApplicationMenu$HelpItems = [

    {
        text: Main.translate("appmenuitem", "tools__lbl"),
        menu: new Ext.menu.Menu({
            items: [{
	                text: Main.translate("applicationMsg","developerToolsSyncBtn__lbl"),
	                handler: function() {
	                	
	                	var dc = Ext.create(atraxo.ad.ui.extjs.dc.DataSource_Dc,{});
	                	var successFn = function() {
	            			Main.info(Main.translate("applicationMsg","developerToolsMenuSync__lbl"));
	            		};
	            		var o={
	            			name:"synchronizeCatalog",
	            			callbacks:{
	            				successFn: successFn,
	            				successScope: this
	            			},
	            			modal:true
	            		};
	            		dc.doRpcFilter(o);
	            		
	                }
	            },{
	                text: "Upgrade",
	                handler: function() {
	                	
	                	var dc = Ext.create(atraxo.ad.ui.extjs.dc.Client_Dc,{});
	                	__dialogDestroying__ = false;
	                	dc.setFilterValue("code",getApplication().session.client.code);
	                	dc.doQuery();
	                	dc.on("afterDoQuerySuccess", function(ctrl) {
	                		Ext.MessageBox.confirm('Confirm', 'Are you sure you want to proceed?', function(btn){
	                			   if(btn === 'yes'){
	                				   var o={
	                               			name:"upgrade",
	                               			modal:true
	                               		};
	                                   	ctrl.doRpcData(o);
	                			   }
	                		});
	                	});
	                }
	            },
                
                {
                    text: Main
                        .translate("dcImp", "title"),
                    handler: function() {
                        Ext.create(
                            "e4e.dc.tools.DcImportWindow", {}).show();
                    }
                },

                {
                    text: Main.translate("appmenuitem",
                        "upload_imp__lbl"),
                    handler: function() {
                        (new e4e.base.FileUploadWindow({
                            _handler_: "dsCsvImport",
                            _fields_: {
                                dsName: {
                                    xtype: "combo",
                                    fieldLabel: Main
                                        .translate(
                                            "cmp",
                                            "dsName"),
                                    labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY,
                                    selectOnFocus: true,
                                    forceSelection: true,
                                    allowBlank: false,
                                    autoSelect: true,
                                    pageSize: 30,
                                    id: Ext.id(),
                                    displayField: "name",
                                    queryMode: 'remote',
                                    allQuery: "%",
                                    triggerAction: 'query',
                                    minChars: 0,
                                    matchFieldWidth: false,
                                    width: 350,
                                    listConfig: {
                                        width: 300
                                    },
                                    store: Ext
                                        .create(
                                            'Ext.data.Store', {
                                                fields: [
                                                    "id",
                                                    "name"
                                                ],
                                                listeners: {
                                                    beforeload: {
                                                    	fn: function(store,op) {
                                                            store.proxy.extraParams.data = Ext.encode({name: op.params.query + "%"});
                                                        }
                                                    }
                                                },
                                                proxy: {
                                                    type: 'ajax',
                                                    api: Main
                                                        .dsAPI(
                                                            Main.dsName.DS_LOV,
                                                            "json"),
                                                    actionMethods: {
                                                        read: 'POST'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        idProperty: 'id',
                                                        totalProperty: 'totalCount'
                                                    },
                                                    startParam: Main.requestParam.START,
                                                    limitParam: Main.requestParam.SIZE,
                                                    sortParam: Main.requestParam.SORT,
                                                    directionParam: Main.requestParam.SENSE
                                                }
                                            })
                                },
                                separator: {
                                    xtype: "combo",
                                    store: [";", ","],
                                    value: ",",
                                    width: 250,
                                    fieldLabel: Main
                                        .translate(
                                            "cmp",
                                            "csv_cfg_separator"),
                                    allowBlank: false,
                                    labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY
                                },
                                quoteChar: {
                                    xtype: "combo",
                                    store: ['"'],
                                    value: '"',
                                    width: 250,
                                    fieldLabel: Main
                                        .translate(
                                            "cmp",
                                            "csv_cfg_quote"),
                                    allowBlank: false,
                                    labelSeparator: Main.viewConfig.FORM_LABEL_SEPARATOR_FOR_MANDATORY
                                }
                            },
                            _succesCallbackScope_: this,
                            _succesCallbackFn_: function() {
                            }
                        })).show();
                    }
                }
            ]
        })
    }, "-", {
        text: Main.translate("appmenuitem", "frameInspector__lbl"),
        handler: function() {
            (new e4e.base.FrameInspector({})).show();
        }
    }, {
        text: Main.translate("cmp", "keyshortcut_title"),
        handler: function() {
            Ext.create("e4e.base.KeyboardShortcutsWindow", {}).show();
        }
    }, "-", {
        text: Main.translate("appmenuitem", "about__lbl"),
        handler: function() {
            (new Ext.Window({
                bodyPadding: 10,
                title: Main.translate("appmenuitem", "about__lbl"),
                tpl: e4e.base.TemplateRepository.APP_ABOUT,
                data: {
                    product: Main.productInfo
                },
                closable: true,
                modal: true,
                resizable: false
            })).show();
        }
    }, {
        text: Main.translate("appmenuitem", "usermanual__lbl"),
        handler: function() {
            var url = Main.urlHelp + "/WebHome.html";
            var wnd = window.open(url, Main.translate("appmenuitem", "help__lbl"));
            setTimeout(function() {
                wnd.focus();
            }, 1000);
        }
    }
]

/**
 * User account management
 */
e4e.base.SoneApplicationMenu$UserAccount = [{
        text: Main.translate("appmenuitem", "changepswd__lbl"),
        cls: 'sone-submenu',
        handler: function() {
            (new e4e.base.ChangePasswordWindow({})).show();
        }
    }
];

/**
 * Session management
 */
e4e.base.SoneApplicationMenu$SessionControl = [{
    text: Main.translate("appmenuitem", "logout__lbl"),
    cls: 'sone-submenu',
    handler: function() {
        getApplication().doLogout();
    }
}];

/**
 * Main application menu items
 */
e4e.base.SoneApplicationMenu$Items_ClientUser = [{
    xtype: "splitbutton",
    text: Main.translate("appmenuitem", "myaccount__lbl"),
    menu: new Ext.menu.Menu({
        items: ["-"].concat(e4e.base.SoneApplicationMenu$UserAccount)
    })
}, {
    xtype: "splitbutton",
    text: Main.translate("appmenuitem", "session__lbl"),
    menu: new Ext.menu.Menu({
        items: e4e.base.SoneApplicationMenu$SessionControl
    })
}, {
    xtype: "splitbutton",
    text: Main.translate("appmenuitem", "help__lbl"),
    menu: new Ext.menu.Menu({
        items: e4e.base.SoneApplicationMenu$HelpItems
    })

}];

e4e.base.SoneApplicationMenu$Items_SystemUser = [
{
    xtype: "splitbutton",
    text: Main.translate("appmenuitem", "session__lbl"),
    menu: new Ext.menu.Menu({
        items: e4e.base.SoneApplicationMenu$SessionControl
    })
}, {
    xtype: "splitbutton",
    text: Main.translate("appmenuitem", "help__lbl"),
    menu: new Ext.menu.Menu({
        items: e4e.base.SoneApplicationMenu$HelpItems
    })

}];

/**
 * Database menus
 */
Ext.define("e4e.base.DBMenu", {
    extend: "Ext.menu.Menu"
});

/**
 * Application header.
 */
Ext.define("e4e.base.SoneApplicationMenu", {
    extend: "Ext.panel.Panel",

    padding: 0,
    height: 115,
    id: Ext.id(),

    systemMenu: null,
    systemMenuAdded: null,

    dbMenu: null,
    dbMenuAdded: null,
    sideBarId: null,
    
    _cmpFrameTitleId_ : null,
    _cmpHeaderPanelId_: null,
    _cmpHeaderToolbarId_ : null,
    _cmpRightSideBarId_ : null,
    _cmpUserSideBarId_ : null,
    _cmpNotificationSideBarId_ : null,
    _subsidiaryPanelId_ : null,
    _subsidiaryContainerId_ : null,
    _idCmpProfileButton_ : null,
    _profileBtnEl_ : null,
    
    _parentMenuCmp_ : null,
    
    _idCmpKpiPanel_ : null,
    _idCmpLanguageContainer_ : null,
    _idCmpThemeContainer_ : null,
    _idCmpToolbarAvatarContainer_ : null,
    _idCmpNotificationCounter_ : null,
    _idCmpNotificationContainer_ : null,
    _appIsLoading_ : null,
    _idCmpAccordionMenu_ : null,
    _idCmpDockedMenu_ : null,
    
    /**
     * Set the user name in the corresponding element.
     */
    setUserText: function(v) {
        var _i = this.items.get("e4e.menu.ApplicationMenu$Item$UserName");
        if (_i) {
            _i.setText(v);
        }
    },

    /**
     * Set the client name in the corresponding element.
     */
    setClientText: function(v) {
        var _v = (!Ext.isEmpty(v)) ? v : "--";
        var _i = this.items.get("e4e.menu.ApplicationMenu$Item$ClientName");
        if (_i) {
            _i.setText(_v);
        }
    },

    /**
     * Set the default company name in the corresponding element.
     */
    setCompanyText: function(v) {
        var _v = (!Ext.isEmpty(v)) ? v : "--";
        var _i = this.items.get("e4e.menu.ApplicationMenu$Item$CompanyName");
        if (_i) {
            _i.setText(_v);
        }
    },

    /**
     * Dan: Create the sidebar trigger
     */

    _createSideBarTrigger_: function() {
    	
    	var panel = Ext.create('Ext.panel.Panel', {
        	width: 50,
        	height: 42,
	    	renderTo: Ext.getBody(),
	    	floating: true,
	    	cls: "sone-toolbar sone-menu-trigger",
	    	x: 0,
	    	y:0,
	    	id: Ext.id(),
	    	items: [{
	    		xtype: "panel",
	    		dock: "top",
	    		border: false,
		        frame: false,
		        ui: "appheader",
	    		items: [{
	    			xtype: "container",
		            height: '100%',
		            layout: {
		                align: 'middle',
		                pack: 'center',
		                type: 'hbox'
		            },
		            items: [{
		                xtype: "button",
		                glyph: "xf0c9@FontAwesome",
		                scale: 'medium',
		                height: 42, 
		                style: 'background: transparent; border:0px',
		                tooltip: Main.translate("appmenuitem", "menu_tip__tlp"),
		                scope: this,
		                handler: function() {
		                	
		                	var soneDockedMenu = Ext.getCmp("sone-docked-menu");
		    				var soneDockedMenuItems = Ext.getCmp("sone-docked-menu-items");
		    				var items = soneDockedMenuItems.items.items;
		    				
		    				Ext.getBody().mask()
		    				
		    				Ext.each(items, function(item) {
		    					item.setWidth(280);
		    					item.removeCls("x-btn-center");
		    					item.addCls(["x-btn-left","sone-button-menu-text"]);
		    				}, this);
		    				
		    				soneDockedMenu.setWidth(280);
		    				soneDockedMenu.addCls("open");
		    				soneDockedMenuItems.setWidth(280);
		                }
		            }]
	    		}]
	    	}]
    	});
    	return panel;
    },

   
    hideDockedSideBar: function(callBackFn, doAfterFn) {
        var sideBar = Ext.getCmp("sone-docked-menu-sidebar");
        var ctx = this;
        this._appIsLoading_ = true;
        sideBar.setXY(["-" + sideBar.getWidth(), 42],{
            easing: 'easeOut',
            callback: function() {
            	if (callBackFn) {
            		Ext.defer(callBackFn, 250, this);
            	}
            	if (doAfterFn) {
            		doAfterFn();
            	}
            	Ext.getBody().unmask();
            	ctx._clearActiveBtn_();
            }
        });
    },
    
    _addCreateItems_ : function() {

    	var ctx = this;
		var doAfterFn = function() {
			ctx._resizeMenu_();
		}
		
    	var items = [{
    		xtype: "button",
    		text: Main.translate("appmenuitem", "create_ticket__lbl"),
    		glyph: "63@finance-solid",
    		width: "100%",
    		cls: "sone-docked-item sone-docked-child-item",
    		handler: function() {
    			ctx.hideDockedSideBar(ctx.showFuelTicket,doAfterFn)
    		}
    	},{
    		xtype: "button",
    		text: Main.translate("appmenuitem", "create_request__lbl"),
    		glyph: "xe072@finance-solid",
    		width: "100%",
    		cls: "sone-docked-item sone-docked-child-item",
    		handler: function() {
    			ctx.hideDockedSideBar(ctx.showFuelRequest,doAfterFn)
    		}
    	},{
    		xtype: "button",
    		text: Main.translate("appmenuitem", "create_sales_contract__lbl"),
    		glyph: "xe07f@finance-solid",
    		width: "100%",
    		cls: "sone-docked-item sone-docked-child-item",
    		handler: function() {
    			ctx.hideDockedSideBar(ctx.showSalesContract,doAfterFn)
    		}
    	},{
    		xtype: "button",
    		text: Main.translate("appmenuitem", "create_purchase_contract__lbl"),
    		glyph: "xe073@finance-solid",
    		width: "100%",
    		cls: "sone-docked-item sone-docked-child-item",
    		handler: function() {
    			ctx.hideDockedSideBar(ctx.showPurchaseContract,doAfterFn)
    		}
    	},{
    		xtype: "button",
    		text: Main.translate("appmenuitem", "create_incoming_invoice__lbl"),
    		glyph: "100@finance-solid",
    		width: "100%",
    		cls: "sone-docked-item sone-docked-child-item",
    		handler: function() {
    			ctx.hideDockedSideBar(ctx.showIncomingInvoices,doAfterFn)
    		}
    	}];
    	return items;
    },
    
    /**
     * Dan: Hide the right sidebar
     */
    
    _hideRightSideBar_ : function(panel, callbackFn) {
    	var itemsContainer = panel.down('container');
    	itemsContainer.animate({
		    duration: 150,
		    from: {
		    	opacity: 1
		    },
		    to: {
		        opacity: 0
		    },
		    callback: function() {
		    	itemsContainer.removeAll();
		    	panel.setXY([Ext.getBody().getViewSize().width, 42],{
	                easing: 'easeOut',
	                callback: function() {
	                	Ext.getBody().unmask();
	                	if (callbackFn) {
	                		callbackFn();
	                	}
	                }
	            });
		    	
		    }
		});
    },
    
    /**
     * Dan: Hide the right sidebar on body click
     */
    
    _hideRightSideBarOnBodyClick_ : function(panel) {
    	panel.mon(Ext.getBody(), 'click', function(){
    		var itemsContainer = panel.down('container');
        	itemsContainer.animate({
    		    duration: 150,
    		    from: {
    		    	opacity: 1
    		    },
    		    to: {
    		        opacity: 0
    		    },
    		    callback: function() {
    		    	itemsContainer.removeAll();
    		    	panel.setXY([Ext.getBody().getViewSize().width, 42],{
    	                easing: 'easeOut',
    	                callback: function() {
    	                	Ext.getBody().unmask();
    	                }
    	            });
    		    }
    		});
        }, panel, { delegate: '.x-mask' });
    },
    
    /**
     * Dan: Hide the right sidebar on body click
     */
    
    _hideUserSideBarOnBodyClick_ : function(panel) {
    	
    	var subsidiaryPanel = Ext.getCmp(this._subsidiaryContainerId_);
    	
    	panel.mon(Ext.getBody(), 'click', function(){    		
    		panel.setXY([Ext.getBody().getViewSize().width, 42],{
                easing: 'easeOut',
                callback: function() {
                	Ext.getBody().unmask();
                	subsidiaryPanel.removeAll();
                	panel.destroy();
                }
            });
        }, panel, { delegate: '.x-mask' });
    },
    
    /**
     * Dan: Hide the notification sidebar on body click
     */
    
    _hideNotificationSideBarOnBodyClick_ : function(panel) {
    	
    	panel.mon(Ext.getBody(), 'click', function(){
    		panel.setXY([Ext.getBody().getViewSize().width, 42],{
                easing: 'easeOut',
                callback: function() {
                	Ext.getBody().unmask();
                }
            });
        }, panel, { delegate: '.x-mask' });
    },
    
    _hideNotificationSideBar_ : function(panel) {    	
    	panel.setXY([Ext.getBody().getViewSize().width, 42],{
            easing: 'easeOut',
            callback: function() {
            	Ext.getBody().unmask();
            }
        });
    },
    
    /**
     * Dan: Show the right sidebar
     */

    _showRightSideBar_ : function(items, scope) {

    	Ext.getBody().mask();
        var sideBar = Ext.getCmp(this._cmpRightSideBarId_);
        var right = Ext.getBody().getViewSize().width - 100;
        sideBar.setXY([right, 42],{
        	scope: this,
            easing: 'easeIn',
            callback: function() {
            	var itemsContainer = sideBar.down('container');
            	itemsContainer.add(items(scope));
            	itemsContainer.animate({
        		    duration: 150,
        		    from: {
        		    	opacity: 0
        		    },
        		    to: {
        		        opacity: 1
        		    }
        		});
            }
        }, this);
    },
    
    /**
     * Dan: Set the active subsidiary
     */
    
    _setActiveSubsidiary_ : function(context, id, code, volume, weight, currency, name) {
    	var ds = "fmbas_UserSubsidiary_Ds";
    	var subsidiaryPanel = Ext.getCmp(context._subsidiaryContainerId_);
    	Ext.Ajax.request({
        	url: Main.dsAPI(ds, "json").service,
        	method: "POST",
        	scope: this,
        	params : {
        		rpcName: "setActive",
        		rpcType: "data",
				data : Ext.encode({
					id: id,
					code : code,
					name : name,
					defaultVolumeUnitCode : volume,
					defaultWeightUnitCode : weight,
					defaultCurrencyCode : currency
				})
			},
        	success: function(response) {
        		var items = subsidiaryPanel.items.items, l = items.length, i = 0;
        		var hoverIcon = "<div class='sone-subsidiary-hover'><i class='fa fa-check'></i></div>";
        		var data = Ext.getResponseDataInJSON(response).data; 
        		
        		var soneUserName = document.getElementById("sone-user-name");
        		
        		soneUserName.innerHTML = getApplication().session.user.name+'<br>'+getApplication().session.client.name+' | '+data.code
        		
        		parent._ACTIVESUBSIDIARY_.code = data.code;
        		parent._ACTIVESUBSIDIARY_.id = data.id;
        		
        		parent._ACTIVESUBSIDIARY_.volumeUnitCode = data.defaultVolumeUnitCode;
        		parent._ACTIVESUBSIDIARY_.weightUnitCode  = data.defaultWeightUnitCode;
        		parent._ACTIVESUBSIDIARY_.currencyUnitCode  = data.defaultCurrencyCode;
        		
        		getApplication().session.user.activeSubsidiaryCode = data.code;
        		getApplication().session.user.activeSubsidiaryId = data.id;
        		getApplication().session.user.activeSubsidiaryName = data.name;
        		
        		
        		var defaultVolumeUnitCode, defaultWeightUnitCode, defaultCurrencyUnitCode;
        		if (!Ext.isEmpty(data.defaultVolumeUnitCode)) {
        			defaultVolumeUnitCode = data.defaultVolumeUnitCode;
        		}
        		else {
        			defaultVolumeUnitCode = parent._SYSTEMPARAMETERS_.sysvol;
        		}
        		
        		if (!Ext.isEmpty(data.defaultWeightUnitCode)) {
        			defaultWeightUnitCode = data.defaultWeightUnitCode;
        		}
        		else {
        			defaultWeightUnitCode = parent._SYSTEMPARAMETERS_.sysweight;
        		}
        		
        		if (!Ext.isEmpty(data.defaultCurrencyCode)) {
        			defaultCurrencyUnitCode = data.defaultCurrencyCode;
        		}
        		else {
        			defaultCurrencyUnitCode = parent._SYSTEMPARAMETERS_.syscrncy;
        		}
        		
        		getApplication().session.user.volumeUnitCode = defaultVolumeUnitCode;
        		getApplication().session.user.weightUnitCode = defaultWeightUnitCode;
        		getApplication().session.user.currencyUnitCode = defaultCurrencyUnitCode;
        			
        		for (i ; i<l; i++) {
        			if (items[i].xtype === "button") {
        				if (items[i]._subsidiaryId_ === id) {
        					items[i].removeCls("inactive");
        					items[i].addCls("active");
        					items[i]._active_ = true;
        					items[i].setText(items[i].getText().replace(hoverIcon, ''));
        				}
        				else {
        					items[i].removeCls("active");
        					items[i].addCls("inactive");
        					items[i]._active_ = false;
        					items[i].setText(items[i].getText()+hoverIcon);
        				}
        			}
        		}
        		
			}
		});
    },

    /**
     * Dan: Get the subsidiaries
     */
    
    _getSubsidiaries_ : function() {
    	
    	var ds = "fmbas_UserSubsidiary_Ds";
		Ext.Ajax.request({
        	url: Main.dsAPI(ds, "json").read,
        	method: "POST",
        	scope: this,
        	params : {
				data : Ext.encode({
					userCode : getApplication().getSession().getUser().code
				})
			},
        	success: function(response) {
        		
        		var data = Ext.getResponseDataInJSON(response).data;
        		var len = data.length;
        		var result = [];
        		var subsidiaryPanel = Ext.getCmp(this._subsidiaryContainerId_);
        		var handlerFn = this._setActiveSubsidiary_;
        		var ctx = this;
        		for (var i = 0; i<len; i++) {
        			
        			var mainSubsidiary = _ACTIVESUBSIDIARY_.code;
        			var statusCls = "inactive";
        			var hoverIcon = "";
        			var country = data[i].officeCountryName;
        			var newCountry = "";
        			var maxCountryLen = 21;
        			var countryAndCode;
        			var icon = '<i class="fa fa-share-alt"></i>';
        			
        			if (!Ext.isEmpty(country)) {
        				newCountry = " | "+country;
        			}
        			
        			countryAndCode = data[i].code+newCountry;
        			
        			if (countryAndCode.length > maxCountryLen) {
        				countryAndCode = countryAndCode.substring(0, maxCountryLen)+"...";
        			}
        			
        			if (mainSubsidiary === data[i].code) {
        				statusCls = "active";
        				icon = '<i class="fa fa-share-alt"></i>'
        			}
        			else {
        				hoverIcon = "<div class='sone-subsidiary-hover'><i class='fa fa-check'></i></div>";
        			}
        			result.push({
        				xtype : "button",
        				html: "<div class='sone-display-table'><div class='sone-subsidiary-icon "+statusCls+"'><div>"+icon+"</div></div><div class='sone-subsidiary-info'><span class='sone-subsidiary-name'>"+data[i].name+"</span>"+countryAndCode+hoverIcon+"</div></div>",
        				cls: "sone-subsidiary-box "+statusCls,
        				_subsidiaryId_ : data[i].id,
        				_subsidiaryCode_ : data[i].code,
        				_subsidiaryVolume_ : data[i].defaultVolumeUnitCode,
        				_subsidiaryWeight_ : data[i].defaultWeightUnitCode,
        				_subsidiaryCurrency_ : data[i].defaultCurrencyCode,
        				_subsidiaryName_ : data[i].name,
        				handler: function(el) {
       						handlerFn(ctx, el._subsidiaryId_, el._subsidiaryCode_, el._subsidiaryVolume_, el._subsidiaryWeight_, el._subsidiaryCurrency_, el._subsidiaryName_);
        				}
        			});
        		}
        		if (!Ext.isEmpty(subsidiaryPanel)) {
        			subsidiaryPanel.add(result);
        		}
        		
			}
		});
    },
    
    
    /**
     * Dan: Show the right sidebar
     */
    _showUserSideBar_ : function() {
    	
    	var f = function(ctx) {
    		Ext.getBody().mask();
            var sideBar = Ext.getCmp(ctx._cmpUserSideBarId_);
            var right = Ext.getBody().getViewSize().width - 280;
            sideBar.setXY([right, 42],{
            	scope: this,
                easing: 'easeIn',
                callback: function() {
                	ctx._getSubsidiaries_();
                }
            }, this);
    	}
    	
    	this._createUserSideBar_(f);
    	
    },
    
    _getNotifications_ : function() {
    	
    	var notifications = Main.notificationQueue;
    	var notificationContainer = Ext.get(this._idCmpNotificationContainer_);
    	var cls = "";
    	var glyph = "";
    	var notificationHtml = [];
		
		if (!Ext.isEmpty(notifications)) {

		    Ext.each(notifications, function(notification) {
		    	
		    	var currentTime;
		    	var timeStamp = (new Date(notification.time)).getTime() > 0;
		    	if (timeStamp === true) {
		    		currentTime = new Date(notification.time);
		    	}
		    	else {
		    		currentTime = new Date(notification.time.replace(" ","T"));
		    	}
		    	
				var hours = currentTime.getHours();
				var minutes = currentTime.getMinutes();
				var suffix = "AM";

				if (minutes < 10) {
					minutes = "0" + minutes;
				}
				if (hours >= 12) {
				    suffix = "PM";
				    hours = hours - 12;
				}
				if (hours === 0) {
				    hours = 12;
				}

		    	if (notification.type === "Error") {
		            cls = "modal-error";
		            glyph = "<i class='fa fa-exclamation-triangle'></i>";
		        } else if (notification.type === "Job") {
		            cls = "modal-success";
		            glyph = "<i class='fa fa-check'></i>";
		        } else if (notification.type === "Activity") {
		            cls = "modal-activity";
		            glyph = "<i class='fa fa-check'></i>";
		        } else if (notification.type === "Process") {
		            cls = "modal-process";
		            glyph = "<i class='fa fa-check'></i>";
		        }
		        
		    	 notificationHtml.push('<div class="sone-notification-container '+cls+'" id="'+notification.id+'-notification"><div class="sone-actions-container"><div class="sone-display-table"><div class="sone-display-table-cell"><div onclick=\"Notification.'+notification.methodName+'('+this._replaceDoubleQuotesWithSingleQuotes_(notification.methodParam)+')\"><i class="fa fa-cogs"></i></div></div><div class="sone-display-table-cell"><div onclick="Main.removeNotificationFromSideBar(\''+notification.id+'\',\''+notification.recordId+'\')"><i class="fa fa-times"></i></div></div></div></div><div class="sone-display-table"> <div class="sone-display-table-cell sone-modal-icon"> <div>' + glyph + '</div> </div> <div class="sone-display-table-cell sone-modal-body"> <div class="sone-display-table"> <div class="sone-display-table-cell"> <div class="sone-modal-title"> '+notification.title+' </div> </div> <div class="sone-display-table-cell sone-modal-time"> ' + hours + ':' + minutes + ' ' + suffix + ' </div> </div> <div class="sone-modal-text"> ' + notification.text + ' </div> </div> </div></div>');
		    }, this);
		}
		notificationHtml.reverse();
		notificationContainer.update(notificationHtml.join('\n'));
    },
    
    _replaceDoubleQuotesWithSingleQuotes_ : function(str) {
    	if (!Ext.isEmpty(str)) {
    		return str.replace(/"/g, "'");
    	}
    	else {
    		return str;
    	}
    	
    },
    
    /**
     * Dan: Show the notification sidebar
     */

    _showNotificationSideBar_ : function(ctx) {
    	
    	Ext.getBody().mask();
        var sideBar = Ext.getCmp(this._cmpNotificationSideBarId_);
        var right = Ext.getBody().getViewSize().width - 280;
        sideBar.getEl().dom.style.display="block";
        sideBar.setXY([right, 42],{
        	scope: this,
            easing: 'easeIn',
            callback: function() {
            	ctx._getNotifications_();
            }
        }, this);
    	
    },
    
    
    /**
     * Dan: Create the right sidebar panel
     */
    
    _createRightSideBar_ : function() {
    	
    	var right = Ext.getBody().getViewSize().width+30;
    	
    	var panel = Ext.create('Ext.panel.Panel', {
            id: this._cmpRightSideBarId_,
            frame: false,
            floating: true,
            shadow: false,
            height: '100%',
            width: 100,
            x: right,
            y: 42,
            cls: 'sone-right-sidebar',
            renderTo: Ext.getBody(),
            layout: {
                type: 'vbox',
                pack: 'center',
                align: 'center'
            },
            overflowY: 'auto',
            animScroll: true,   
            items: [{
            	xtype: "container",
            	opacity: 0,
            	type: 'vbox',
            	width: 85
            }],
            listeners: {
                afterrender: {
                    scope: this,
                    fn: function(panel) {
                        this._hideRightSideBarOnBodyClick_(panel);
                                                
                    }
                }
            }

        });
        return panel;
    	
    },
    
    _saveAvatar_ : function(img, idAvatarHolder) {
    	var dsName = "fmbas_UserSupp_Ds";
    	var b64Data = img.split(",")[1];

    	var job = Ext.Ajax.request({
    	    url: Main.dsAPI(dsName, "json").service,
    	    method: "POST",
    	    params: {
    	    	rpcName: "saveAvatar",
    	    	rpcType: "data",
    	        params: Ext.JSON.encode({
    	        	avatar: b64Data
    	        })
    	    },
    	    success: function() {
    	    	var avatarHolder = document.getElementById(idAvatarHolder);
    	    	var toolbarAvatar = document.getElementById(this._idCmpToolbarAvatar_);
    	    	_USERAVATAR_ = img.split(",")[1];
    	    	var newAvatar = "data:image/png;base64,"+_USERAVATAR_;
    	    	avatarHolder.style.background = "url("+newAvatar+") center center no-repeat";
    	    	toolbarAvatar.style.background = "url("+newAvatar+") center center no-repeat";
    	    	toolbarAvatar.style.backgroundSize = "30px 30px";
    	    },
    	    failure: function(response) {
    	        Main.error(Ext.getResponseErrorText(response));
    	    },
    	    scope: this
    	});
    	return job;
    },
    
    _triggerAvatarUpload_ : function(fileFieldId) {
    	var elem = document.getElementById(fileFieldId);
    	if(elem && document.createEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, false);
			elem.dispatchEvent(evt);
    	}
    },
    
    _convertAvatar_ : function(fileFieldId, ctx, idAvatarHolder, idCmpToolbarAvatar) {
    	
    	var scaleFactor = {
    			maxWidth: 320,
    			maxHeight: 240
    	}
    	
    	var uploadLimit = 200; // file size limit in Kb
    	if (typeof FileReader !== "undefined") {
    	    var size = document.getElementById(fileFieldId).files[0].size/1024;
    	    
    	    if (size >= uploadLimit) {
    	    	Main.warning(Main.translate("userSideBar", "uploadSizeExceeded__lbl"));
    	    	return;
    	    }
    	}
    	
    	var Resizer = function() {

    	    var maxWidth = 0,
    	        maxHeight = 0;
    	    var canvas = document.createElement('canvas');
    	    var img = new Image();
    	    var callback;

    	    var isFileOk = function(file) {
    	        if (!file || !file.type.match(/image.*/)) {
    	            return false;
    	        };
    	        return true;
    	    }

    	    img.onload = function() {
    	        var dimensions = getResizedDimensions(img.width, img.height);
    	        canvas.width = dimensions.width;
    	        canvas.height = dimensions.height;
    	        var ctx = canvas.getContext('2d');
    	        ctx.drawImage(img, 0, 0, dimensions.width, dimensions.height);
    	        if (callback) {
    	            callback(canvas.toDataURL());
    	            img.src = '';
    	            ctx.clearRect(0, 0, canvas.width, canvas.height);
    	        }
    	    }
    	    if (window.opera) { 
    	        img.onerror = img.onload; 
    	    }

    	    var createObjectURL = function(file) {
    	        if (window.URL) {
    	            return window.webkitURL.createObjectURL(file);
    	        } else if (window.URL && window.URL.createObjectURL) {
    	            return window.URL.createObjectURL(file);
    	        } else {
    	            return null;
    	        }
    	    }

    	    var getResizedDimensions = function(initW, initH) {
    	        var resizedWidth = maxWidth,
    	            resizedHeight = maxHeight,
    	            initialWidth = initW,
    	            initialHeight = initH;
    	        if (initialWidth <= maxWidth && initialHeight <= maxHeight) {
    	            resizedWidth = initialWidth;
    	            resizedHeight = initialHeight;
    	        } else {
    	            if (initialWidth < initialHeight) {
    	                var calcWidth = initialWidth * resizedHeight / initialHeight;
    	                if (calcWidth <= maxWidth) {
    	                    resizedWidth = calcWidth;
    	                } else {
    	                    resizedHeight = resizedHeight * resizedWidth / calcWidth;
    	                }
    	            } else {
    	                if (initialWidth > initialHeight) {
    	                    var calcHeight = initialHeight * resizedWidth / initialWidth;
    	                    if (calcHeight <= maxHeight) {
    	                        resizedHeight = calcHeight;
    	                    } else {
    	                        resizedWidth = resizedWidth * resizedHeight / calcHeight;
    	                    }
    	                } else {
    	                	if (scaleFactor.maxWidth > maxWidth) {
    	                		resizedWidth = Math.Min(maxHeight, maxWidth);
    	                	}
    	                    resizedHeight = resizedWidth;
    	                }
    	            }
    	        }
    	        return {
    	            width: resizedWidth,
    	            height: resizedHeight
    	        }
    	    }

    	    return {
    	        scale: function(file, width, height, action) {
    	            if (!isFileOk(file)) {
    	                return;
    	            }
    	            maxWidth = width;
    	            maxHeight = height;
    	            callback = action;
    	            img.src = createObjectURL(file);
    	        },
    	    }
    	}
    	var resizer = new Resizer();
        var file = document.getElementById(fileFieldId).files[0];
        
        var callback = function(scaledImg) {
        	ctx._saveAvatar_(scaledImg, idAvatarHolder, idCmpToolbarAvatar);
        }
        
        resizer.scale(file, 320, 240, callback);
    },
    
    
    
    /**
     * Dan: Create the user sidebar
     */
    
    _createUserSideBar_ : function(callbackFn) {
    	
    	var right = Ext.getBody().getViewSize().width;
    	var height = Ext.getBody().getViewSize().height-42;
    	var idAvatarUploadControl = Ext.id();
    	var idAvatarUploadField = Ext.id();
    	var idAvatarHolder = Ext.id();

    	var appendAvatar = function() {
    		var userAvatar = _USERAVATAR_;
    		var style = "";
        	if (!Ext.isEmpty(userAvatar)) {
        		var b64Avatar = 'data:image/png;base64,'+ userAvatar;
        		style = 'style="background:url('+b64Avatar+') center center no-repeat"';
        	}
        	return style;
    	}
    	
    	var extraUserInfo = '<br>'+getApplication().session.client.name+' | '+_ACTIVESUBSIDIARY_.code;
    	
    	if (getApplication().session.user.systemUser === true) {
    		extraUserInfo = "";
    	}
    	
    	var avatarHtml = ['<div class="sone-avatar">',
    	                  	'<div class="sone-avatar-holder" '+appendAvatar()+' id="'+idAvatarHolder+'">',
	    	                  	'<div class="sone-avatar-controls" id="'+idAvatarUploadControl+'">',
	    	                  		'<i class="fa fa-3x fa-search sone-edit-control"></i>',
	    	                  	'</div>',
    	                  	'</div>',
    	                  '</div>',
    	                  '<div class="sone-user-info">'+Main.translate("appmenuitem", "logged_in_as__lbl")+"<br>",
    	                  '<span class="sone-user-name" id="sone-user-name">'+getApplication().session.user.name+extraUserInfo+'</span>',
    	                  '<input type="file" id="'+idAvatarUploadField+'" style="display:none" accept="image/gif, image/jpeg, image/png">',
               	       	  '</div>'].join('\n');   
    	
    	var themeItems = [{
		    xtype: 'container',
		    autoEl: 'ul',
		    cls: 'sone-theme-menu',
		    id: this._idCmpThemeContainer_,
		    items: e4e.base.SoneApplicationMenu$Helper.getThemeMenuItems()
		    
		}];

    	var langItems = [{
		    xtype: 'container',
		    autoEl: 'ul',
		    cls: 'sone-lang-menu',
		    id: this._idCmpLanguageContainer_,
		    items: e4e.base.SoneApplicationMenu$Helper.getLanguageMenuItems()
		}];
    	
    	var subsidiaryItems = [{
		    xtype: 'container',
		    cls: 'sone-subsidiary-menu',
		    id: this._subsidiaryContainerId_
		}];
    	
    	var panel = Ext.create('Ext.panel.Panel', {
            id: this._cmpUserSideBarId_,
            frame: false,
            floating: true,
            shadow: false,
            height: height,
            width: 280,
            x: right,
            y: 42,
            cls: 'sone-user-sidebar',
            renderTo: Ext.getBody(),
            layout: 'border',
            style: "z-index: 99999 !important",
            animScroll: true,   
            items: [{
            	region:'north',
            	height: 'auto',
            	items : [{
            		xtype: "container",
            		html: '<div class="sone-logout-btn"><i class="fa fa-2x fa-power-off"></i></div>',
	            	listeners: {
	            		afterrender: {
	            			scope: this,
	            			fn: function(el) {
	            				el.getEl().on("click", function() {
	            					getApplication().doLogout();
	            				}, this);
	            			}
	            		}
	            	}
            		},{
            		xtype: "container",
	            	anchor: "100%",
	            	frame: true,
	            	height: 260,
	            	style: 'margin-bottom:-30px',
	            	cls: 'sone-avatar-container',
	            	layout: {
	                    align: 'middle',
	                    pack: 'center',
	                    type: 'hbox'
	                },
	                items: [{
	                	xtype: "container",
	                	html: avatarHtml,
	                	listeners: {
	                		afterrender: {
	                			scope: this,
	                			fn: function() {
	                				var fn = this._triggerAvatarUpload_;
	                				var convertFn = this._convertAvatar_;
	                				var ctx = this;
	                				document.getElementById(idAvatarUploadControl).addEventListener("click", function() {
	                					return fn(idAvatarUploadField);
	                		    	});
	                				document.getElementById(idAvatarUploadField).addEventListener("change", function() {
	                					return convertFn(idAvatarUploadField, ctx, idAvatarHolder, this._idCmpToolbarAvatar_);
	                		    	});
	                			}
	                		}
	                	}
	                }]
            	}]
            }, {
            	region:'center',
            	id: Ext.id(),
            	frame: false,
            	overflowY: 'auto',
                cls: 'sone-user-body',
            	layout: {
                    type: 'accordion',
                    titleCollapse: true,
                    multi: true,
                    animate: true,
                    activeOnTop: false,
                    hideCollapseTool: true,
                    fill: false
                },
                items: [{
                	title: 'Panel 0',
                    html: 'Content',
                    collapsed: false,
                    style: 'display:none'
                },
                {
                	title: Main.translate("appmenuitem", "subsidiary__lbl"),
                	id: this._subsidiaryPanelId_,   
                	items: subsidiaryItems,
                    collapsed: false,
                    hidden: getApplication().session.user.systemUser === true ? true : false
                },
                {
                    title: Main.translate("appmenuitem", "theme__lbl"),
                    items: themeItems,
                    collapsed: false
                },{
                    title: Main.translate("appmenuitem", "lang__lbl"),
                    items: langItems,
                    collapsed: false
                }]
            }],
            listeners: {
                afterrender: {
                    scope: this,
                    fn: function(panel) {
                        this._hideUserSideBarOnBodyClick_(panel);
                        if (callbackFn) {
                        	callbackFn(this);
                        }
                    }
                }
            }

        });
        return panel;
    	
    },
    
    _createNotificationSideBar_ : function() {
	 
	 	var right = Ext.getBody().getViewSize().width+30;
	 	var height = Ext.getBody().getViewSize().height-42;

    	var panel = Ext.create('Ext.panel.Panel', {
            id: this._cmpNotificationSideBarId_,
            frame: false,
            floating: true,
            shadow: false,
            height: height,
            width: 280,
            x: right,
            y: 42,
            cls: 'sone-notification-sidebar',
            renderTo: Ext.getBody(),
            layout: 'border',
            style: "z-index: 99999 !important",
            animScroll: true,
            items: [{
            	xtype: "container",
            	region: "north",
            	style: "padding: 10px 20px 20px 20px; text-align: right",
            	items: [{
            		xtype: "button",
            		text: Main.translate("notificationSideBar", "remove_all__lbl"),
            		cls: "sone-remove-all-btn",
            		handler: function() {
            			Main.removeAllNotifications(true)
            		}
            	}]
            },{
            	xtype: "container",
            	id: this._idCmpNotificationContainer_,
            	region: "center",
            	overflowY : "auto"
            }],
            listeners: {
                afterrender: {
                    scope: this,
                    fn: function(panel) {
                        this._hideNotificationSideBarOnBodyClick_(panel);
                    }
                }
            }

        });
        return panel;
    	
    },

    _createDockedMenuSidebar_ : function() {
    	var panel = Ext.create('Ext.panel.Panel', {
            id: "sone-docked-menu-sidebar",
            frame: false,
            floating: true,
            shadow: false,
            height: Ext.getBody().getViewSize().height-42,
            width: 280,
            x: -280,
            y: 42,
            cls: 'sone-sidebar sone-docked-sidebar',
            renderTo: Ext.getBody(),
            overflowY: 'auto',
            animScroll: true,
            layout: "vbox"
        });
        return panel;
    },
    
    /**
     * Dan: Create the docked menu panel
     */

    _createDockedMenu_: function() {
    	var menus = "";
    	if (Main.navigationTopMenus != null) {
    		menus = this._createMenus_(Main.navigationTopMenus);
    	}

        var panel = Ext.create('Ext.panel.Panel', {
        	width: 50,
        	height: Ext.getBody().getViewSize().height,
	    	cls: "sone-docked-menu",
	    	_activeBtnId_ : null,
	    	renderTo: Ext.getBody(),
	    	margin: "42 0 0 0",
	    	id: "sone-docked-menu",
	    	items: [{
	    		xtype: "panel",
            	frame: false,
            	overflowY: 'auto',
            	id: "sone-docked-menu-items",
            	layout: {
                    type: 'vbox'
                },
                items: menus
	    	}],
	    	listeners: {
            	afterrender: {
            		scope: this,
                	fn: function(panel) {
                		this._resizeMenuOnBodyClick_(panel);
                	}
            	}
            }

        });
        return panel;
    },
    
    _resizeMenuOnBodyClick_ : function(panel) {
    	
    	var ctx = this;
    	panel.mon(Ext.getBody(), 'click', function(){
    		
    		var soneDockedMenu = Ext.getCmp("sone-docked-menu");
    		var soneDockedMenuItems = Ext.getCmp("sone-docked-menu-items");
    		var soneDockedMenuSidebar = Ext.getCmp("sone-docked-menu-sidebar");
			var items = soneDockedMenuItems.items.items;

			panel.setWidth(50);
			
			Ext.each(items, function(item) {
				item.setWidth(50);
				item.removeCls(["x-btn-left","sone-button-menu-text"]);
				item.addCls("x-btn-center");
			}, this);
			
			soneDockedMenu.removeCls("open");
			soneDockedMenuSidebar.setXY([-280, 42],null);
			
			ctx._clearActiveBtn_();
			
        }, panel, { delegate: '.x-mask' });
    },
    
    _resizeMenu_ : function() {
    	
    	var soneDockedMenu = Ext.getCmp("sone-docked-menu");
		var soneDockedMenuItems = Ext.getCmp("sone-docked-menu-items");
		var items = soneDockedMenuItems.items.items;

		soneDockedMenu.setWidth(50);
		Ext.getBody().unmask();
		
		Ext.each(items, function(item) {
			item.setWidth(50);
			item.removeCls(["x-btn-left","sone-button-menu-text"]);
			item.addCls("x-btn-center");
		}, this);
		
		soneDockedMenu.removeCls("open");
    },

    /**
     * Create the application's product info element using the corresponding
     * properties
     */
    _createAppInfo_: function() {
        return {
            xtype: "tbtext",
            id: "e4e.menu.ApplicationMenu$Item$ProductInfo",
            text: "<span>" + Main.productInfo.name + " </span><br><span>" + Main.translate("appmenuitem", "version__lbl") + ": " + Main.productInfo.version + "</span></span>"
        };
    },

    showFuelTicket: function() {
        var bundle = __APPMENU_TYPES__.bundles.ops;
        var frame = __APPMENU_TYPES__.frames.fuelTicket;

        getApplication().showFrame(frame, {
            url: Main.buildUiPath(bundle, frame, false),
            callback: function() {
                this._when_called_from_appMenu_();
            }
        });
    },
    
    showFuelRequest: function() {
        var bundle = __APPMENU_TYPES__.bundles.ops;
        var frame = __APPMENU_TYPES__.frames.fuelRequest;

        getApplication().showFrame(frame, {
            url: Main.buildUiPath(bundle, frame, false),
            callback: function() {
                this._when_called_from_appMenu_();
            }
        });
    },
    
    showSalesContract: function() {
        var bundle = __APPMENU_TYPES__.bundles.fmbas;
        var frame = __APPMENU_TYPES__.frames.salesContracts;

        getApplication().showFrame(frame, {
            url: Main.buildUiPath(bundle, frame, false),
            callback: function() {
                this._when_called_from_appMenu_();
            }
        });
    },
    
    showPurchaseContract: function() {
        var bundle = __APPMENU_TYPES__.bundles.fmbas;
        var frame = __APPMENU_TYPES__.frames.purchaseContracts;

        getApplication().showFrame(frame, {
            url: Main.buildUiPath(bundle, frame, false),
            callback: function() {
                this._when_called_from_appMenu_();
            }
        });
    },
    
    showIncomingInvoices: function() {
    	
        var bundle = __APPMENU_TYPES__.bundles.acc;
        var frame = __APPMENU_TYPES__.frames.incomingInvoices;

        getApplication().showFrame(frame, {
            url: Main.buildUiPath(bundle, frame, false)
        });
    },

    /**
     * Create the header's right part
     */
    _createRight_: function() {
    	
    	var appendAvatar = function() {
    		var userAvatar = "";
    		if (typeof(_USERAVATAR_) !== "undefined") {
    			userAvatar = _USERAVATAR_;
    		}
    		var style = "";
        	if (!Ext.isEmpty(userAvatar)) {
        		var b64Avatar = 'data:image/png;base64,'+ userAvatar;
        		style = 'style="background:url('+b64Avatar+') center center no-repeat; background-size: 30px 30px"';
        	}
        	return style;
    	}
    	
    	var userProfile = "";
		if (typeof(_USERPROFILE_) !== "undefined") {
			userProfile = _USERPROFILE_;
		}
		
		var appMenuTypes = "";
		if (typeof(__APPMENU_TYPES__) !== "undefined") {
			appMenuTypes = __APPMENU_TYPES__;
		}
        var _usrm = ["->"];
        
        var userProfileName = userProfile.firstName;
        
        if (getApplication().session.user.systemUser === true) {
        	userProfileName = "SYS";
        }

        return _usrm.concat([
            this.createSystemMenu(),
            {
            	xtype: "button",
            	id: this._idCmpToolbarAvatarContainer_,
            	text: "<div id='"+this._idCmpToolbarAvatar_+"' "+appendAvatar()+" class='sone-toolbar-avatar'></div>",
            	scale: 'medium',
            	style: "padding:0px;",
            	cls: "sone-toolbar-avatar-container",
            	width: 47,
            	height: 30,
                listeners: {
                    click: {
                        scope: this,
                        fn: function() {
                        	if (Main.countNotifications() > 0) {
                        		this._showNotificationSideBar_(this);
                        	}
                        }
                    },
                    afterrender : {
                    	scope: this,
                    	fn: function() {
                    		var container = document.getElementById(this._idCmpToolbarAvatarContainer_);
                    		var counterEl = document.createElement("div");
							counterEl.className = "sone-notification-counter";
							counterEl.id = this._idCmpNotificationCounter_;
							container.appendChild(counterEl);
                    	}
                    }
                }
            }, {
            	xtype: 'button',
                id: this._idCmpProfileButton_,
                scale: 'medium',
                tooltip: Main.translate("appmenuitem", "account_settings__tlp"),
                text: Main.translate("appmenuitem", "user_hello__lbl")+" "+userProfileName,
                arrowCls: '',
                height: 42,
                cls: 'sone-menu-profile-button',
                listeners: {
                    click: {
                        scope: this,
                        fn: function() {
                        	this._showUserSideBar_();
                        }
                    },
                    afterrender: {
                    	scope: this,
                    	fn: function(el) {
                    		this._profileBtnEl_ = el;
                    	}
                    }
                }
            }, {
                xtype: 'button',
                text: Main.translate("appmenuitem", "dev_tools__lbl"),
                glyph: "xf0ad@FontAwesome",
                iconStyle: "color:red",
                scale: "medium",
                height: 42,
                cls: "sone-button-dev-tools",
                hidden:true,
                menu: new Ext.menu.Menu({
                    items: e4e.base.SoneApplicationMenu$HelpItems
                }),
                listeners : {                       
                    afterrender: function(el) {
                        this.keyNav = new Ext.util.KeyMap({
                            target: Ext.getBody().el,
                            binding: [{
                            	key: 13,
                                shift:true,
                                fn: function(){
                                	if (el.hidden === true) {
                                		el.setHidden(false);
                                	}
                                	else {
                                		el.setHidden(true);
                                	}
                                    
                                }
                            }],
                            scope: this
                        }); 
                    }
                }
            }, {
                xtype: "tbspacer",
                width: 5
            }, {
                xtype: 'button',
                scale: 'medium',
                glyph: !Ext.isEmpty(appMenuTypes) ? appMenuTypes.glyphs.fontAwesome.help : "",
                tooltip: Main.translate("appmenuitem", "get_help__tlp"),
                style: 'background:transparent; border:none',
                handler: function() {
                    var url = Main.urlHelp + "/WebHome.html";
                    var wnd = window.open(url, "Help");
                    setTimeout(function() {
                        wnd.focus();
                    }, 1000);
                }

            }, {
                xtype: "tbspacer",
                width: 5
            }, {
                xtype: 'button',
                scale: 'medium',
                tooltip: Main.translate("appmenuitem", "maximize__tlp"),
                glyph: !Ext.isEmpty(appMenuTypes) ? appMenuTypes.glyphs.fontAwesome.hideKPI : "",
                style: 'background:transparent; border:none',
                scope: this,
                handler: function() {
                    var panel = Ext.getCmp(this._cmpHeaderPanelId_);
                    
                    if (Ext.isEmpty(panel._isCollapsed_) || panel._isCollapsed_ === false) {
                    	panel.setHeight(55);
                    	panel._isCollapsed_ = true;
                    }
                    else {
                    	panel.setHeight(115);
                    	panel._isCollapsed_ = false;
                    }
                }
            }, {
                xtype: "tbspacer",
                width: 5
            }
        ]);
    },
    
    _createLeft_ : function() {
    	var html = '<span class="sone-application-title">supplier.ONE</span>';
    	if (getApplication().session.user.systemUser === true) {
    		html = '<span class="sone-application-title" style="padding-left:10px">supplier.ONE</span>';
    	}
    	var appTitle = {
    		xtype: 'container',
    	    html: html
    	}
    	return appTitle;
    },

    _createHeaderToolbar_ : function() {
    	
    	var _items = [].concat(this._createLeft_()).concat(this._createRight_());
    	
    	var toolbar =  Ext.create("Ext.toolbar.Toolbar", {
	        border: false,
	        frame: false,
	        id: this._cmpHeaderToolbarId_,
	        cls: 'sone-toolbar',
	        items: _items,
	        ui: "appheader"
    	});
    	
    	return toolbar;
    },
    
    _hideKpi_ : function(panel) {
    	panel.animate({
		    duration: 300,
		    from: {
		    	opacity: 1
		    },
		    to: {
		        opacity: 0
		    },
		    callback: function() {
		    	panel.setHtml("");
		    	panel._opacity_ = 0;
		    }
		});
    },
    
    _showKpi_: function(panel) {
    	panel.animate({
		    duration: 300,
		    from: {
		    	opacity: 0
		    },
		    to: {
		        opacity: 1
		    },
		    callback: function() {
		    	panel._opacity_ = 1;
		    }
		});
    },
    
    // Dan: config the KPI panel
    
    _configKpi_ : function(theFrame, kpis) {
    	var kpiPanel = Ext.getCmp(this._idCmpKpiPanel_);
    	
    	if (!Ext.isEmpty(theFrame)) {
	
    		if (!Ext.isEmpty(kpis)) {
        		if (kpis.length > 0) {
        			var wrapStart = '<div style="display:table">';
        			var kpiContent = [];
        			var newKpiContent = [];
        			var fontFamily = "";
        			var theGlyph = "";
        			var kpiOrder;
        			
        			for (var i = 0; i < kpis.length; i++) {
        				
        				if (kpis[i].active === true) {
        					if (kpis[i].glyphFont==="financeSolid") {
        						fontFamily = "fs";
        						theGlyph = "icon-"+kpis[i].glyphCode;
        						
        					}
        					else if (kpis[i].glyphFont==="fontAwesome") {
        						fontFamily = "fa";
        						theGlyph = "fa-"+kpis[i].glyphCode;
        					}
        					kpiOrder = kpis[i].order;
            				kpiContent[kpiOrder] = '<div style="width:20px; display:table-cell"></div><div style="width:45px; height:45px; background:'+kpis[i].color+'; border-radius: 100%; color: #FFFFFF; text-align:center; vertical-align:middle; display:table-cell"><i class="'+fontFamily+' '+theGlyph+'" style="font-size:2em"></i></div><div style="text-align:left; vertical-align:middle; display:table-cell; padding-left:10px"><div style="font-size:11px; text-transform:uppercase; color: #555555; white-space:nowrap">'+kpis[i].title+'</div><div style="font-size:22px; font-weight:400; color: #555555; margin-top:2px">'+kpis[i].value+'</div></div>';
            				
        				}
        			}
        			
        			// Rebuild indexes
        			var counter = 0;
        			Ext.each(kpiContent, function(item) {
        				counter ++;
        				if(typeof item !== 'undefined'){
        					newKpiContent.push(item);
        				};
        				
        			}, this);
        			kpiContent = newKpiContent;
        			
        			var wrapEnd = '</div>';
        			kpiPanel._opacity_ = 0;
        			this._hideKpi_(kpiPanel);
        			kpiPanel.animate({
        				scope: this,
            		    duration: 300,
            		    from: {
            		    	opacity: 1
            		    },
            		    to: {
            		        opacity: 0
            		    },
            		    callback: function() {
            		    	kpiPanel._opacity_ = 0;
            		    	kpiPanel.setHtml(wrapStart+kpiContent.join('\n')+wrapEnd);
            		    	this._showKpi_(kpiPanel);
            		    }
            		});
        		}
        		else {
        			if (kpiPanel._opacity_ === 1) {
        				this._hideKpi_(kpiPanel);
        			}
        		}
    		}
    		else {
    			if (kpiPanel._opacity_ === 1) {
    				this._hideKpi_(kpiPanel);
    			}
    		}
    	}
    	else {
    		if (kpiPanel._opacity_ === 1) {
    			this._hideKpi_(kpiPanel);
			}
    	}
    },
    
    _configHeader_ :function(params, kpis) {

    	// Set the header title
    	var title = Ext.getCmp(this._cmpFrameTitleId_);
    	
    	title.setText(params.title);
    	
    	if (!Ext.isEmpty(params.glyph) && params.glyph !== "null") {
    		title.setGlyph(params.glyph);
    	}
    	else if (params.hideKpi) {
    		title.setGlyph("");
    	}
    	else {
    		title.setGlyph("xf067@FontAwesome");
    	}
    	
    	this._configKpi_(params.frame, kpis);
    },

    initComponent: function(config) {

    	// Docked menu
    	if (getApplication().session.user.systemUser === false) {
    		this._createDockedMenu_();
    	}
    	
		this._createDockedMenuSidebar_();
		
		if (getApplication().session.user.systemUser === false) {
			this._createSideBarTrigger_();
		}        
       
        this.systemMenuAdded = false;
        this._idCmpKpiPanel_ = Ext.id();
        this._cmpHeaderToolbarId_ = Ext.id();
        this._cmpRightSideBarId_ = Ext.id();
        this._cmpUserSideBarId_ = Ext.id();
        this._subsidiaryPanelId_ = Ext.id();
        this._subsidiaryContainerId_ = Ext.id();
        this._idCmpToolbarAvatar_ = Ext.id();
        this._idCmpProfileButton_ = Ext.id();
        this._idCmpLanguageContainer_ = Ext.id();
        this._idCmpThemeContainer_ = Ext.id();
        this._idCmpNotificationCounter_ = Ext.id();
        this._idCmpToolbarAvatarContainer_ = Ext.id();
        this._cmpNotificationSideBarId_ = Ext.id();
        this._idCmpNotificationContainer_ = Ext.id();
        this._idCmpAccordionMenu_ = Ext.id();
        this._idCmpDockedMenu_ = Ext.id();

        var cfg = {
              border: false,
              id: Ext.id(),
              frame: false,
              layout:'anchor',
              dockedItems: this._createHeaderToolbar_(),
              items: [{
            	  xtype: "container",
            	  anchor: "100% 100%",
            	  margin: '10px 20px 10px 20px',
            	  layout: {
            		  type: 'hbox',
            		  pack: 'start',
            		  align: 'center'
                  },
                  items: [{
                      xtype: 'panel',
                      flex: 1,
                      items: [{
                    	  id: Ext.id(),
	            		  xtype: 'button',
	            		  disabled: true,
	            		  cls: "sone-frame-title",
	            		  scale: 'medium',
	            		  glyph: "xf015@FontAwesome",
	            		  text: Main.translate("appmenuitem", "home__lbl"),
	            		  listeners: {
	            			  afterrender: {
	            				  scope: this,
	            				  fn: function(el) {
	            					  this._cmpFrameTitleId_ = el.getId();
	            				  }
	            			  }
	            		  } 
                      }]
                  },{
                      xtype: 'panel',
                      flex: 2,
                      items: [{
                    	  xtype: "container",
		            	  layout: {
		            		  type: 'hbox',
		            		  pack: 'end',
		            		  align: 'center'
		                  },
		                  items: [{
		                	  xtype: "container",
		                	  id: this._idCmpKpiPanel_,
		                	  bodyPadding: "0px 5px 0px 0px",
		                	  style: "opacity: 0",
		                	  _opacity_ : 0
		                  }]
		                  
                      }]
                  }]
              }],
              listeners: {
    			  afterrender: {
    				  scope: this,
    				  fn: function(el) {
    					  this._cmpHeaderPanelId_ = el.getId();
    					  
    				  }
    			  }
    		  }
              
        };
        

        Ext.apply(this, cfg);
        this.callParent(arguments);

        this.on("afterrender", function() {
            Ext.Function.defer(this._insertDBMenus_, 500, this);
        }, this);
        
        this.on("setupHeader", function(params, kpis) {
            this._configHeader_(params, kpis);
        }, this);
        
        this.on("updateProfilebtnText", function(text) {
            this._profileBtnEl_.setText(text);
        }, this);
        
        this._createRightSideBar_();
        this._createNotificationSideBar_();
    },

    /**
     * System client menu management. A system client can manage application
     * clients (tenants). This feature will be moved in future to a stand-alone
     * system module where a platform administrator can manage clients as well
     * as other platform level management tasks.
     */
    createSystemMenu: function() {
        var list = Main.systemMenus;
        if (!Ext.isEmpty(list) && Ext.isArray(list)) {
            var _items = [];
            for (var i = 0; i < list.length; i++) {
                var e = list[i];
                var t = e.labelKey.split("/");
                var _item = {
                    text: Main.translate(t[0], t[1]),
                    arrowCls: '',
                    cls: "sone-submenu",
                    _bundle: e.bundle,
                    _frame: e.frame,
                    handler: function() {
                        getApplication().showFrameByName(this._bundle, this._frame);
                    }
                }
                _items[_items.length] = _item;
            }
        }

        var _menu = {
            xtype: "splitbutton",
            height: 42,
            scale: "medium",
            glyph: "xf013@FontAwesome",
            text: Main.translate("appmenuitem", "system__lbl"),
            menu: new Ext.menu.Menu({
                items: _items
            })
        };
        this.systemMenu = Ext.create('Ext.button.Button', _menu);
        
        if (getApplication().session.user.systemUser === true) {
        	return this.systemMenu;
        }
    },

    /**
     * Add the system client menu to the menu bar
     */
    addSystemMenu: function() {
        if (!this.systemMenuAdded) {
            this.createSystemMenu();
            this.insert(2, this.systemMenu);
            this.systemMenuAdded = true;
        }
    },

    /**
     * Remove the system client menu from the menu bar
     */
    removeSystemMenu: function() {
        if (this.systemMenuAdded) {
            this.remove(this.systemMenu);
            this.systemMenuAdded = false;
            this.systemMenu = null;
        }
    },

    /**
     * Insert menu elements loaded from database.
     */
    _insertDBMenus_: function() {
        if (this.rendered && this.dbMenu != null && this.dbMenuAdded !== true) {
            var l = this.dbMenu.length;
            for (var i = 0; i < l; i++) {
                this.insert(2 + i, this.dbMenu[i]);
            }
            this.dbMenuAdded = true;
        }
    },
    
    _afterGetAllMenuItems_ : function() {
    	
    	var soneAllMenuItemsContainer = Ext.getCmp("sone-all-menu-items-container");
    	var items = soneAllMenuItemsContainer.items.items;
		var itemCounter = items.length;
		var field =  Ext.getCmp("sone-all-menu-search-field");
		
		// Focus on the single element in the list
		
		if (itemCounter === 1) {
			Ext.each(items, function(item) {
				if (item.xtype === "button" && item.hidden === false) {
					field._triggerButton_ = item;
				}
				
			}, this);
		}
		else {
			field._triggerButton_ = null;
		}
    },
    

    _createAllMenuSearchField_ : function() {
    	var textField = {
			xtype: "textfield",
			margin: "0 15 10 15",
			id: "sone-all-menu-search-field",
			width: "100%",
			emptyText: "Search menu item",
			_filterEnabled_: null,
			_triggerButton_ : null,
			_itemsCounter_ : null,
    		enableKeyEvents: true,
    		readOnly: (Ext.isChrome?true:false), // hack to avoid autocomplete in Chrome
    		value: "",
			listeners: {
				change: {
					buffer: 500,
					scope: this,
	    			fn: function(field, newVal) {
	    				
	    				var soneAllMenuItemsContainer = Ext.getCmp("sone-all-menu-items-container");
	    					    				
	    				if( !field._filterEnabled_ ){
        					return;
        				}
	    				if (!Ext.isEmpty(newVal)) {
	    					this._getAllMenuItems_(field.getValue(), field);
	    				}
	    				else {	    					
	    					soneAllMenuItemsContainer.removeAll();
	    					field._triggerButton_ = null;
	    				}  

	    			}
				},
    			keypress : function(el,eventObject){
    				if (eventObject.getCharCode() === 13) {
    					var f = function() {
    						el._triggerButton_.handler();
    					}
    					if (!Ext.isEmpty(el._triggerButton_)) {
    						f();
    					}
    					else {
    						Ext.defer(f,500,this);
    					}    	            	
    	            }    	            
    	        },
    			focus : {
    				scope: this,
    				fn: function(el) {
    					el.setValue("");
    					el._filterEnabled_ = true;
    					Ext.defer(el.setReadOnly, 250, el, [false]);
    				}
    			},
    			afterrender: function(el) {
    				el.inputEl.dom.style = "-webkit-box-shadow: 0 0 0px 1000px white inset";
    				el.focus(false, 150);
    			}
			}	
    	}
    	return textField;
    },
    
    _createQuickCreateSearchField_ : function() {
    	var textField = {
			xtype: "textfield",
			margin: "0 15 10 15",
			width: "100%",
			emptyText: "Search menu item",
    		_filterEnabled_: null,
    		_triggerButton_ : null,
    		enableKeyEvents: true,
    		readOnly: (Ext.isChrome?true:false), // hack to avoid autocomplete in Chrome
    		value: "",
			listeners: {
				change: {
					scope: this,
	    			fn: function(field) {
	    				if( !field._filterEnabled_ ){
        					return;
        				}
	    				var soneQuickCreateContainer = Ext.getCmp("sone-quick-create-items-container");
	    				var items = soneQuickCreateContainer.items.items;
	    				var itemCounter = items.length;
        				Ext.each(items, function(item) {
        					if (item.xtype === "button") {
        						if (item.getText().toLowerCase().indexOf(field.getRawValue()) > -1) {
        							item.setHidden(false);
        						}
        						else {
        							item.setHidden(true);
        							itemCounter--;
        						}
        					}
        				}, this);
        				
        				// Focus on the single element in the list
        				
        				if (itemCounter === 1) {
            				Ext.each(items, function(item) {
            					if (item.xtype === "button" && item.hidden === false) {
            						field._triggerButton_ = item;
            					}
            					
            				}, this);
    					}
        				else {
        					field._triggerButton_ = null;
        				}
	    			}
				},
    			keypress : function(el,eventObject){
    	            if (eventObject.getCharCode() === Ext.EventObject.ENTER && !Ext.isEmpty(el._triggerButton_)) {
    	            	el._triggerButton_.handler();
    	            }
    	        },
    			focus : {
    				scope: this,
    				fn: function(el) {
    					el.setValue("");
    					el._filterEnabled_ = true;
    					Ext.defer(el.setReadOnly, 250, el, [false]);
    				}
    			},
    			afterrender: function(el) {
    				el.inputEl.dom.style = "-webkit-box-shadow: 0 0 0px 1000px white inset";
    				el.focus(false, 150);
    			}
			}	
    	}
    	return textField;
    },

    /**
     * Create an array of menus from an array of configuration objects
     */
    
    _setActiveBtn_ : function(btn) {
    	
    	var soneDockedMenu = Ext.getCmp("sone-docked-menu");
    	var activeBtn;
    	
    	if (!Ext.isEmpty(soneDockedMenu._activeBtnId_)) {
    		activeBtn = Ext.getCmp(soneDockedMenu._activeBtnId_);
    		activeBtn.removeCls("sone-button-menu-open");
    	}  
    	
    	btn.addCls("sone-button-menu-open");
		soneDockedMenu._activeBtnId_ = btn.id;
		
    },
    
    _clearActiveBtn_ : function() {
    	
    	var soneDockedMenu = Ext.getCmp("sone-docked-menu");
    	var activeBtn;
    	
    	if (!Ext.isEmpty(soneDockedMenu._activeBtnId_)) {
    		activeBtn = Ext.getCmp(soneDockedMenu._activeBtnId_);
    		activeBtn.removeCls("sone-button-menu-open");
    	}  
    },
    
    _triggerAllMenuSearch_ : function(el) {
    	
    	var soneDockedMenuSidebar = Ext.getCmp("sone-docked-menu-sidebar");
		var soneDockedMenu = Ext.getCmp("sone-docked-menu");
		var ctx = this;

		Ext.getBody().mask();
		soneDockedMenuSidebar.setXY([soneDockedMenu.getWidth(), 42],{
        	scope: this,
            easing: 'easeIn'
        });
		
		soneDockedMenuSidebar._isVisible_ = true;
		soneDockedMenuSidebar.setTitle(el.getText()+"<span>"+el._description_+"</span>");
		soneDockedMenuSidebar.removeAll();
		
		soneDockedMenuSidebar.add({
			xtype: "panel",
			layout: "vbox",
			width: 280,
			items: [ctx._createAllMenuSearchField_(), {
				xtype: "panel",
				width: 280,
				id: "sone-all-menu-items-container"
			}]
		});
		
		ctx._setActiveBtn_(el);
    },

	_createSearchMenuItem_ : function() {
		// search button
        var ctx = this;
		return {
			cls : "sone-menu sone-button-menu",
			listeners : {
				afterrender : {
					scope : this,
					fn : function(button) {
						button.setStyle("cursor", "pointer");

						Ext.getBody().keyNav = new Ext.util.KeyMap({
							target : Ext.getBody(),
							binding : [ {
								key : 191,
								shift : true,
								alt: true,
								fn : function() {
									ctx._triggerAllMenuSearch_(button);
								}
							} ],
							scope : this
						});

					}
				}
			},
			glyph : "xf002@FontAwesome",
			iconCls : "sone-menu-glyph sone-menu-button-glyph",
			name : "SearchMenu",
			text : Main.translate("appMenu", "search_menu_item__lbl", null, "SEARCH MENU ITEM"),
			_description_ : Main.translate("appMenu", "search_menu_item__dsc", null, "Search menu item"),
			xtype : "button",
			handler : function(el) {
				ctx._triggerAllMenuSearch_(el);
			}
		};
	},

	_createQuickCreateMenuItem_ : function() {
		// Quick create menu item
        var ctx = this;
		return {
			xtype : 'button',
			cls : 'sone-menu sone-button-menu',
			glyph : "xf055@FontAwesome",
			listeners : {
				click : {
					scope : this,
					fn : function(el) {
						var soneDockedMenuSidebar = Ext.getCmp("sone-docked-menu-sidebar");
						var soneDockedMenu = Ext.getCmp("sone-docked-menu");
						Ext.getBody().mask();
						soneDockedMenuSidebar.setXY([ soneDockedMenu.getWidth(), 42 ], {
							scope : this,
							easing : 'easeIn'
						});
						soneDockedMenuSidebar._isVisible_ = true;
						soneDockedMenuSidebar.setTitle(el.getText() + "<span>" + el._description_ + "</span>");
						soneDockedMenuSidebar.removeAll();

						soneDockedMenuSidebar.add({
							xtype : "panel",
							layout : "vbox",
							width : 280,
							items : [ ctx._createQuickCreateSearchField_(), {
								xtype : "panel",
								width : 280,
								id : "sone-quick-create-items-container",
								items : this._addCreateItems_()
							} ]
						});

						ctx._setActiveBtn_(el);
					}
				}
			},
			iconCls : "sone-menu-glyph sone-menu-button-glyph",
			name : "CreateMenu",
			text : Main.translate("appMenu", "quick_create_item__lbl", null, "NEW"),
			_description_ : Main.translate("appMenu", "quick_create_item__dsc", null, "Quickly create new entities")
		};
	},
	
	_preprocessMenuItem_: function(e){
        var key = (e.name+"").replace(/\s+/g, '').toLowerCase().replaceAll("-","_");
        e.db_name = e.name;
        e.name = key;
        e.title = Main.translate("appMenu", key+"__lbl", null, e.title);
        e.description = Main.translate("appMenu", key+"__dsc", null, e.description);
	},
	
    _createMenus_: function(cfgArray) {
        var _m = []; 
        _m.push(this._createQuickCreateMenuItem_());

        for (var i = 0; i < cfgArray.length; i++) {
            var e = cfgArray[i];
            this._preprocessMenuItem_(e);
            
            if (!e.text) {
            	e.xtype = "button";
            	e.text = e.title; 
            	e._description_ = e.description;
                e.cls = "sone-menu sone-button-menu";
                e.listeners = {
            		afterrender: {
            			scope: this,
            			fn: function(button) {
            				button.setStyle("cursor","pointer");
            			}
                    }
                };
                if (Ext.isEmpty(e.glyph)) {
                	e.glyph = "xf067@FontAwesome";
                }
                e.iconCls = "sone-menu-glyph sone-menu-button-glyph"
            }
            
            _m[_m.length] = this._createMenu_(e);
        }

        _m.push(this._createSearchMenuItem_());
        return _m;
    },

    /**
     * Create a menu from configuration object
     */
    _createMenu_: function(config) {
    	var ctx = this;
    	var result = Ext.apply({
    		handler: function(el) {
    			var soneDockedMenuSidebar = Ext.getCmp("sone-docked-menu-sidebar");
    			var soneDockedMenu = Ext.getCmp("sone-docked-menu");
    			Ext.getBody().mask();
    			soneDockedMenuSidebar.setXY([soneDockedMenu.getWidth(), 42],{
                	scope: this,
                    easing: 'easeIn'
                });
    			soneDockedMenuSidebar._isVisible_ = true;
    			soneDockedMenuSidebar.setTitle(el.getText()+"<span>"+el._description_+"</span>");
    			
    			ctx._getMenuItems_({
    				parentEl : this.getId(),
                    menu: config.db_name
                }, true);
    			
    			ctx._setActiveBtn_(el);
			}
    	},config);
        return result;
    },
    

    /**
     * Create a menu item which opens a standard application frame.
     */
    _createFirstLevelMenuItem_ : function(config) {
        var bundle_ = config.bundle;
        var frame_ = config.frame;
        var active = config.active;
        var title_ = config.title;
        var glyph_ = config.glyph;
        
        if (Ext.isEmpty(glyph_)) {
        	glyph_ = "xf067@FontAwesome";
        }
                
        return {
        	title: title_,
        	_bundle_ : bundle_,
        	_frame_ : frame_,
        	cls: "sone-submenu",
        	hidden: active === false ? true : false,
        	glyph: glyph_,
        	listeners: {
                afterrender: {
                	scope: this,
                	fn: function(panel) {
                    	panel.header.setStyle("cursor","pointer");
                        panel.header.el.on('click', function() {
                        	
                        	if (!Ext.isEmpty(frame_) && this._appIsLoading_ !== true) {
                        		var callBackFn = function() {
                        			var bundle = bundle_;
                                    var frame = frame_;
                                    var path = Main.buildUiPath(bundle, frame, false);
                                    getApplication().showFrame(frame, {
                                        url: path,
                                        frameTitle: title_
                                    });
                        		}
                        		var frameParams = {
                                	glyph: glyph_,
                                	title: title_
                                };  
                        		this._configHeader_(frameParams);
                                this.hideSideBar(callBackFn);
                        	}
                                
                        },this);
                        this._addChevron_(panel);
                    }
                }
            },
            layout: {
                type: 'accordion',
                titleCollapse: true,
                animate: true,
                activeOnTop: false,
                hideCollapseTool: true
            },
            defaults: {
            	style: "padding:0px 10px"
            }
        };
    },
    
    /**
     * Dan: Add cheavron to indicate that the panel is collapsible
     */
    
    _addChevron_: function(panel) {
    	Ext.defer(function() {
    		if (panel.items.length > 0) {
        		var header = panel.header;
            	var headerTitle = header.title.getText();
            	var newTitle = headerTitle+'&nbsp;&nbsp;<i class="fa fa-caret-down"></i>';
            	
            	header.setTitle(newTitle);
        	}
    	},100);
    },
    
    _displayMenuItems_ : function(items) {

    	if (getApplication().session.user.systemUser === false) {
    		
    		// Docked menu
        	var soneDockedMenu = Ext.getCmp("sone-docked-menu");
        	var soneDockedMenuSidebar = Ext.getCmp("sone-docked-menu-sidebar");
        	
        	if (Ext.isEmpty(soneDockedMenu) || Ext.isEmpty(soneDockedMenuSidebar)) {
        		return;
        	}
        	
        	var newChildItems = [];
        	var ctx = this;
        	
        	var searchField = {
    			id: Ext.id(),
        		xtype: "textfield",
        		margin: "0 15 10 15",
        		width: "100%",
        		emptyText: "Search menu item",
        		_filterEnabled_: null,
        		_triggerButton_ : null,
        		enableKeyEvents: true,
        		readOnly: (Ext.isChrome?true:false), // hack to avoid autocomplete in Chrome
        		value: "",
        		listeners: {
        			change: {
        				scope: this,
            			fn: function(el) {
            				if( !el._filterEnabled_ ){
            					return;
            				}
            				var items = soneDockedMenuSidebar.items.items;
            				var itemCounter = items.length-1;
            				Ext.each(items, function(item) {
            					if (item.xtype === "button") {
            						if (item.getText().toLowerCase().indexOf(el.getRawValue().toLowerCase()) > -1) {
            							item.setHidden(false);
            						}
            						else {
            							item.setHidden(true);
            							itemCounter--;
            						}
            					}
            					
            				}, this);
            				
            				// Focus on the single element in the list
            				
            				if (itemCounter === 1) {
                				Ext.each(items, function(item) {
                					if (item.xtype === "button" && item.hidden === false) {
                						el._triggerButton_ = item;
                					}
                					
                				}, this);
        					}
            				else {
            					el._triggerButton_ = null;
            				}
            			}
        			},
        			keypress : function(el,eventObject){
        	            if (eventObject.getCharCode() === Ext.EventObject.ENTER && !Ext.isEmpty(el._triggerButton_)) {
       	            		el._triggerButton_.handler();
        	            }
        	        },
        			focus : {
        				scope: this,
        				fn: function(el) {
        					el.setValue("");
        					el._filterEnabled_ = true;
        					var f = function(el){
        						try{
        							el.setReadOnly(false);
        						}catch(e){
        							// nothing to do
        						}
        					};
        					Ext.defer(f, 250, this, [el]);
        				}
        			},
        			afterrender: function(el) {
        				el.inputEl.dom.style = "-webkit-box-shadow: 0 0 0px 1000px white inset";
        				el.focus(false, 150);
        			}
        		}
            }

	    	Ext.each(items, function(item) {
	    		if (!Ext.isEmpty(item.frame)) {
					newChildItems.push({
						xtype: "button",
						id: Ext.id(),
						width: "100%",
						text: item.title,
						glyph: item.glyph,
						cls: "sone-docked-item sone-docked-child-item",
						tooltip: item.title,
						handler: function() {
							if (ctx._appIsLoading_ !== true) {
	                    		var callBackFn = function() {
	                    			var bundle = item.bundle;
	                                var frame = item.frame;
	                                var path = Main.buildUiPath(bundle, frame, false);
	                                getApplication().showFrame(frame, {
	                                    url: path,
	                                    frameTitle: item.title
	                                });
	                    		}
	                    		var doAfterFn = function() {
	                    			ctx._resizeMenu_();
	                    		}
	                    		var frameParams = {
	                            	glyph: item.glyph,
	                            	title: item.title
	                            };  
	                    		ctx._configHeader_(frameParams);
	                    		ctx.hideDockedSideBar(callBackFn, doAfterFn);
	                    		
	                    	}
						}
					});
				}
	    		else {
	    			newChildItems.push({
						xtype: "button",
						id: Ext.id(),
						width: "100%",
						text: item.title,
						glyph: item.glyph,
						_expanded_ : false,
						cls: "sone-docked-item sone-docked-child-item",
						handler: function() {
							if (this._expanded_ === false) {
								if (ctx._appIsLoading_ !== true) {
									ctx._getMenuItems_({
										parentEl : this.getId(),
										menuItemId: item.db_id
					                }, true);
		                    	}
							}
							else {
								var childItems = soneDockedMenuSidebar.items.items;
								var itemsToRemove = [];
								
								Ext.each(childItems, function(c) {
    								if (c._parentBtnId_ === this.id || c._rootBtnId_ === this.id) {
    									itemsToRemove.push(c.id);
    								}
    							}, this);
								
								Ext.each(itemsToRemove, function(r) {
    								soneDockedMenuSidebar.remove(r);
    							}, this);
								
    							this._expanded_ = false;
							}
							
						}
					});
	    		}
	    		
	    	});
        	
        	soneDockedMenuSidebar.removeAll();
    		soneDockedMenuSidebar.add(searchField);
    		soneDockedMenuSidebar.add(newChildItems);
    	}
    },
    
    _displayFilteredMenuItems_ : function(items, theSearchField) {

    	if (getApplication().session.user.systemUser === false) {
    		
    		// Docked menu
        	var soneDockedMenu = Ext.getCmp("sone-docked-menu");
        	var soneDockedMenuSidebar = Ext.getCmp("sone-docked-menu-sidebar");
        	var soneAllMenuItemsContainer = Ext.getCmp("sone-all-menu-items-container");
        	if (Ext.isEmpty(soneDockedMenu) || Ext.isEmpty(soneDockedMenuSidebar)) {
        		return;
        	}
        	var newChildItems = [];
        	var ctx = this;
        	
	    	Ext.each(items, function(item) {
	    		if (!Ext.isEmpty(item.frame)) {
					newChildItems.push({
						xtype: "button",
						id: Ext.id(),
						width: "100%",
						text: item.title,
						glyph: item.glyph,
						cls: "sone-docked-item sone-docked-child-item",
						handler: function() {
							if (ctx._appIsLoading_ !== true) {
	                    		var callBackFn = function() {
	                    			var bundle = item.bundle;
	                                var frame = item.frame;
	                                var path = Main.buildUiPath(bundle, frame, false);
	                                getApplication().showFrame(frame, {
	                                    url: path,
	                                    frameTitle: item.title
	                                });
	                    		}
	                    		var doAfterFn = function() {
	                    			ctx._resizeMenu_();
	                    		}
	                    		var frameParams = {
	                            	glyph: item.glyph,
	                            	title: item.title
	                            };  
	                    		ctx._configHeader_(frameParams);
	                    		ctx.hideDockedSideBar(callBackFn, doAfterFn);
	                    	}
						}
					});
				}
	    		else {
	    			newChildItems.push({
						xtype: "button",
						id: Ext.id(),
						width: "100%",
						text: item.title,
						glyph: item.glyph,
						_expanded_ : false,
						cls: "sone-docked-item sone-docked-child-item",
						handler: function() {
							if (this._expanded_ === false) {
								if (ctx._appIsLoading_ !== true) {
									ctx._getMenuItems_({
										parentEl : this.getId(),
										menuItemId: item.db_id
					                }, true);
		                    	}
							}
							else {
								var childItems = soneDockedMenuSidebar.items.items;
								var itemsToRemove = [];
								
								Ext.each(childItems, function(c) {
    								if (c._parentBtnId_ === this.id || c._rootBtnId_ === this.id) {
    									itemsToRemove.push(c.id);
    								}
    							}, this);
								Ext.each(itemsToRemove, function(r) {
    								soneDockedMenuSidebar.remove(r);
    							}, this);
    							this._expanded_ = false;
							}
						}
					});
	    		}
	    	});
	    	soneAllMenuItemsContainer.removeAll();
	    	soneAllMenuItemsContainer.add(newChildItems);
	    	
	    	//theSearchField
	    	var menuItems = soneAllMenuItemsContainer.items.items;
	    	// Focus on the single element in the list
			if (menuItems.length === 1) {
				Ext.each(items, function(item) {
					if (item.xtype === "button" && item.hidden === false) {
						theSearchField._triggerButton_ = item;
					}
				}, this);
			} else {
				theSearchField._triggerButton_ = null;
			}  
    	}
    },
    
    _displayChildMenuItems_ : function(items, params) {

    	if (getApplication().session.user.systemUser === false) {
    		
    		// Docked menu
        	var soneDockedMenu = Ext.getCmp("sone-docked-menu");
        	var soneDockedMenuSidebar = Ext.getCmp("sone-docked-menu-sidebar");
        	if (Ext.isEmpty(soneDockedMenu) || Ext.isEmpty(soneDockedMenuSidebar)) {
        		return;
        	}
        	
        	var newChildItems = [];
        	var ctx = this;
        	var parentBtn = Ext.getCmp(params.parentEl);
        	var secondLevelCls = "sone-docked-item sone-docked-child-item sone-docked-second-child-item";
        	var thirdLevelCls = "sone-docked-item sone-docked-child-item sone-docked-third-child-item";
        	
	    	Ext.each(items, function(item) {
	    		if (!Ext.isEmpty(item.frame)) {
					newChildItems.push({
						xtype: "button",
						id: item.id,
						width: "100%",
						text: item.title,
						glyph: item.glyph,
						_parentBtnId_ : params.parentEl,
						_rootBtnId_ : params.rootEl,
						cls: params.thirdLevel === true ? thirdLevelCls : secondLevelCls,
						handler: function() {
							if (ctx._appIsLoading_ !== true) {
	                    		var callBackFn = function() {
	                    			var bundle = item.bundle;
	                                var frame = item.frame;
	                                var path = Main.buildUiPath(bundle, frame, false);
	                                getApplication().showFrame(frame, {
	                                    url: path,
	                                    frameTitle: item.title
	                                });
	                    		}
	                    		var doAfterFn = function() {
	                    			ctx._resizeMenu_();
	                    		}
	                    		var frameParams = {
	                            	glyph: item.glyph,
	                            	title: item.title
	                            };  
	                    		ctx._configHeader_(frameParams);
	                    		ctx.hideDockedSideBar(callBackFn, doAfterFn);
	                    	}
						}
					});
				}
	    		else {
	    			newChildItems.push({
						xtype: "button",
						id: item.id,
						width: "100%",
						text: item.title,
						glyph: item.glyph,
						_parentBtnId_ : params.parentEl,
						_expanded_ : false,
						cls: "sone-docked-item sone-docked-child-item sone-docked-second-child-item",
						handler: function() {
							
							if (this._expanded_ === false) {
								if (ctx._appIsLoading_ !== true) {
									ctx._getMenuItems_({
										rootEl : params.parentEl,
										thirdLevel : true,
										parentEl : this.getId(),
										menuItemId: item.db_id
					                }, true);
		                    	}
							}
							else {
								var childItems = soneDockedMenuSidebar.items.items;
								var itemsToRemove = [];
								
								Ext.each(childItems, function(c) {									
    								if (c._parentBtnId_ === this.id) {
    									itemsToRemove.push(c.id);
    								}
    							}, this);
								
								Ext.each(itemsToRemove, function(r) {
    								soneDockedMenuSidebar.remove(r);
    							}, this);
								
    							this._expanded_ = false;
							}
						}
					});
	    		}
	    	});
	    	
	    	var currentIndex = soneDockedMenuSidebar.items.indexOf(parentBtn);
	    	soneDockedMenuSidebar.insert(currentIndex+1, newChildItems);
	    	parentBtn._expanded_ = true;
    	}
    },
    
    _translateMenuItem_: function(mi){
		var key = (mi.name+"").replace(/\s+/g, '').toLowerCase().replaceAll("-","_");
		mi.title = Main.translate("appMenuItems", key, null, mi.title);
    },
    
    _getAllMenuItems_: function(menuTitle, theSearchField) {
    	
    	var mitems = [];
    	var soneDockedMenuSidebar = Ext.getCmp("sone-docked-menu-sidebar");
    	
    	var childMenuItem = soneDockedMenuSidebar.down("button");
    	if (!Ext.isEmpty(childMenuItem)) {
        	var childMenuItemId = soneDockedMenuSidebar.down("button").id;
    		soneDockedMenuSidebar.remove(childMenuItemId);
    	}
    	
    	var request = Ext.Ajax.request({
    		url: Main.dsAPI(Main.dsName.MENU_ITEM, "json").read,
    		_isLoaded_: false,
            _isLoading_: false,
        	method: "POST",
        	scope: this,
            params: {
                filter: Ext.JSON.encode([{
                	id:null,
            		fieldName:"title",
            		operation:"like",
            		value1:"%"+menuTitle+"%",
            		groupOp:"OR1"
                }]),
                orderBy: Ext.JSON.encode([{
                    property: "sequenceNo",
                    direction: "ASC"
                }])
            },
            listeners: {
                scope: this,
                beforerequest: {
                    fn: function(request) {
                    	request._isLoaded_ = false;
                    	request._isLoading_ = true;
                    }
                }
            },
        	success: function(response) {
        		
        		var res = Ext.getResponseDataInJSON(response).data;
				
                for (var i = 0; i < res.length; i++) {
                    var e = res[i];
                    this._translateMenuItem_(e);

                    var glyph_;
                    if (!Ext.isEmpty(e.glyph)) {
                    	glyph_ = e.glyph;
                    }
                    else {
                    	glyph_ = "xf067@FontAwesome";
                    }
                    
                    mitems.push({
                        db_id: e.id,
                        description: e.description,
                        title: e.title,
                        frame: e.frame,
                        bundle: e.bundle,
                        glyph: glyph_,
                        active: e.active
                    });
                }
                
                request._isLoaded_ = true;
                request._isLoading_ = false;
                this._menuItems_ = mitems;

                this._displayFilteredMenuItems_(mitems, theSearchField);
			}
		});
    	
    	Ext.defer(this._afterGetAllMenuItems_, 800, this);
    },

    /**
     * Create a database menu items loader.
     */
    _getMenuItems_: function(params) {
    	var mitems = [];
    	var request = Ext.Ajax.request({
    		url: Main.dsAPI(Main.dsName.MENU_ITEM, "json").read,
    		_isLoaded_: false,
            _isLoading_: false,
        	method: "POST",
        	scope: this,
            params: {
                data: Ext.JSON.encode(params),
                orderBy: Ext.JSON.encode([{
                    property: "sequenceNo",
                    direction: "ASC"
                }])
            },
            listeners: {
                scope: this,
                beforerequest: {
                    fn: function(request) {
                    	request._isLoaded_ = false;
                    	request._isLoading_ = true;
                    }
                }
            },
        	success: function(response) {
        		
        		var res = Ext.getResponseDataInJSON(response).data;
                for (var i = 0; i < res.length; i++) {
                	
                    var e = res[i];
                    this._translateMenuItem_(e);
                    
                    var glyph_;
                    if (!Ext.isEmpty(e.glyph)) {
                    	glyph_ = e.glyph;
                    }
                    else {
                    	glyph_ = "xf067@FontAwesome";
                    }
                                        
                    if (e.active === true) {
                    	mitems.push({
                            db_id: e.id,
                            description: e.description,
                            title: e.title,
                            frame: e.frame,
                            bundle: e.bundle,
                            glyph: glyph_,
                            active: e.active
                        });
                    }
                }
                
                request._isLoaded_ = true;
                request._isLoading_ = false;
                this._menuItems_ = mitems;
                
                if (params.menu) {
                	this._displayMenuItems_(mitems, params);
                }
                else if (params.menuItemId && params.parentEl) {
                	this._displayChildMenuItems_(mitems, params);
                }
			}
		});
    }

});
