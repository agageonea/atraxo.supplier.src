Ext.override(e4e.base.HomePanel, {
	
	_westPanelId_ : Ext.id(),
	_centerPanelId_ : Ext.id(),
	_eastPanelId_ : Ext.id(),
	_configPanelId_ : Ext.id(),
	_widgetWindowId_ : Ext.id(),
	_layoutPickerId_ : Ext.id(),
	_widgetWindowContainerId_ : Ext.id(),
	_dashboardWindowId_ : Ext.id(),
	_idDashboardNameField_ : Ext.id(),
	_idDashboardDescField_ : Ext.id(),
	_idDashboardDefaultField_ : Ext.id(),
	_savedDashboardId_ : null,
	_savedLayoutId_ : null,
	_activityWidgets_ : [],
	_dashboardDirty_ : false,
	
	initComponent : function(config) {
		
		var menuConfig = [];
		var navigAccordeonCfg = null;
		
		if (Main.navigationTreeMenus != null && Main.navigationTreeMenus.length > 0 ) {
			
			for ( var k in Main.navigationTreeMenus) {
				menuConfig[menuConfig.length] = Main.navigationTreeMenus[k];
			}

			navigAccordeonCfg = {
				layout : 'accordion',
				layoutConfig : {
					animate : false,
					activeOnTop : true
				},
				id : this._Menu_AccordeonId_,
				region : 'west',
				width : 350,
				split : true,
				minSize : 200,
				maxSize : 500,
				title : Main.translate("appmenuitem", "appmenus__lbl"),
				collapsible : true,
				items : []
			}

			for (var i = 0; i < menuConfig.length; i++) {
				navigAccordeonCfg.items[i] = {
					title : menuConfig[i]["title"],
					layout : {
						type : 'fit'
					},
					items : [ {
						xtype : "navigationTree",
						id : "dnet-application-view-menu-" + menuConfig[i]["name"],
						_menuId_ : 1,
						_menuName_ : menuConfig[i]["name"],
						withStdFilterHeader : true,
						loader_PreloadChildren : true,
						listeners : {
							openMenuLink : {
								scope : this,
								fn : function(model) {
									var bundle = model.raw.bundle;
									var frame = model.raw.frame;
									var path = Main.buildUiPath(bundle, frame, false);
									getApplication().showFrame(frame, { url : path });
								}
							}
						}
					} ]
				};
			}
		}
		var me = this;
		var cfg = {
			layout : "border",
			title : this._TEXT_TITLE,
			//items : this._createItems_()
			bodyPadding: '32px 0px 0px 0px',
			dockedItems: [{
			    xtype: 'panel',
			    dock: 'top',
			    layout: "hbox",
			    cls: "sone-dashboard-config",
			    id: this._configPanelId_,
			    overlay: true,
			    dockedItems: [{
			    	xtype: "toolbar",
			    	style: "background:#fcfcfc !important",
			    	items: [
//			            {xtype: 'combo',
//			            triggerAction: 'all',
//			            emptyText: "My dashboards",
//			            height: 30,
//			            forceSelection: true,
//			            selectOnFocus: true},
			        "->",
			        {
			        	xtype: "button",
			        	glyph: "xf055@FontAwesome",
			        	cls: "sone-cfg-toolbar-btn",
			        	handler: function(b){
				        	var mainPanel = b.up("panel");
				        	var body = Ext.get("dnet-application-view-home-body");
				        	if (mainPanel.hasCls("panel-expanded")) {
				        		mainPanel.removeCls("panel-expanded");
				        		mainPanel.body.hide();
				        	}
				        	else {
				        		mainPanel.addCls("panel-expanded");
				        		mainPanel.body.show();
				        	}
				        }
			        },{
			        	xtype: "button",
			        	glyph: "xf040@FontAwesome",
			        	cls: "sone-cfg-toolbar-btn"
			        },{
			        	xtype: "button",
			        	glyph: "xf057@FontAwesome",
			        	cls: "sone-cfg-toolbar-btn"
			        }]
			    }],
			    items: this._createDashboardCfgItems_()
			}]
		}

		if (navigAccordeonCfg != null) {
			cfg.items[cfg.items.length] = navigAccordeonCfg;
		}

		Ext.apply(this, cfg);
		this.callParent(arguments);
	},
	
	_createDashboardCfgItems_ : function() {
		var items = [{
			xtype: "panel",
			height: 95,
			style: "border-bottom:1px solid #dadbdc !important; text-align: center",
			flex: 1,
			layout: "anchor",
			achor: "100% 100%",
			cls: "sone-layout-picker",
			id: this._layoutPickerId_,
			titleAlign: 'right',
			bodyStyle: "box-sizing: border-box; padding:5px; background: transparent",
			padding: "10px 0px 10px 0px",
			listeners: {
				boxready: {
					scope: this,
					fn: function(el) {
						this._buildLayoutItems_();
					}
				}
			}
		}];
		return items;
	},
	
	_buildDashboard_: function() {
		
		// Get the default dashboard for the current logged in user	
		
		var dashboardDs = "fmbas_Dashboard_Ds";
		Ext.Ajax.request({
        	url: Main.dsAPI(dashboardDs, "json").read,
        	method: "POST",
        	scope: this,
        	params: {
            data: Ext.JSON.encode({
          	  		createdBy: getApplication().session.user.code,
          	  		isDefault: true
	        	})
	        },
        	success: function(response) {
        		
        		var responseText = Ext.getResponseDataInJSON(response);
        		var res = responseText.data[0];
        		
        		if (responseText.totalCount > 0) {
        			
        			var layoutId = res.layoutId;
            		var layoutDs = "fmbas_Layout_Ds";
            		
            		this._savedDashboardId_ = res.id;
            		this._savedLayoutId_ = layoutId;
            		
            		// Get the layout config
            		
            		Ext.Ajax.request({
                    	url: Main.dsAPI(layoutDs, "json").read,
                    	method: "POST",
                    	scope: this,
                    	params: {
                        data: Ext.JSON.encode({
                        		id: layoutId
            	        	})
            	        },
                    	success: function(response) {
                    		var res = Ext.getResponseDataInJSON(response).data[0];
                    		var layoutCfg = res.cfgPath;
                    		
                    		this._getLayoutCfg_(layoutCfg);
                    		
            			}
            		});
        		}       		
        		
        		
			}
		});
		
	},
	
	_buildLayoutItems_ : function() {
		
    	var ds = "fmbas_Layout_Ds";
    	var items = [];
		Ext.Ajax.request({
        	url: Main.dsAPI(ds, "json").read,
        	method: "POST",
        	scope: this,
        	success: function(response) {
        		var res = Ext.getResponseDataInJSON(response).data;
        		var container = Ext.getCmp(this._layoutPickerId_);
        		Ext.each(res, function(item) {
        			items.push({
        				xtype: "button",
        				icon: item.thumbPath,
        				scale: "large",
        				iconAlign: "top",
        				scope: this,
        				name: item.name,
        				_dbId_ : item.id,
        				handler: function(el) {
        					this._getLayoutCfg_(item.cfgPath, el);
        				}
        			});
        		}, this);
        		container.suspendLayouts();
        		container.add(items);
        		container.resumeLayouts(true);
        		this._buildDashboard_();
			}
		});
	},
	
	_getLayoutCfg_ : function(path, initiator) {
		Ext.Ajax.request({
        	url: path,
        	scope: this,
        	success: function(response) {
        		var obj = Ext.getResponseDataInJSON(response);
        		
        		if (Ext.isArray(obj)) {
        			Ext.each(obj, function(o) {
        				o.cls = "sone-widget-area";
                		o.tools = [{
                			type: "plus",
                			scope: this,
                			handler : function(event,toolEl,headerEl){	
                				if (this._dashboardDirty_ === false) {
	                				var panelCmp = Ext.getCmp(headerEl.container.id);
	                				this._showWidgetWindow_(panelCmp);
                				}
                				else {
                					this._warnDirty_();
                				}
                	        }
                		}];
        			}, this);
        		}
        		else {
        			obj.cls = "sone-widget-area";
            		obj.tools = [{
            			type: "plus",
            			scope: this,
            			handler : function(event,toolEl,headerEl){	
            				if (this._dashboardDirty_ === false) {
	            				var panelCmp = Ext.getCmp(headerEl.container.id);
	            				this._showWidgetWindow_(panelCmp);
            				}
            				else {
            					this._warnDirty_();
            				}
            	        }
            		}];
        		}
        		this._addLayout_(obj, initiator);
			}
		});
	},
	
	_buildGaugeChart_ : function() {
		var chart = Ext.create({			
            xtype: 'polar',
            width:245,
            height: 125,
            animate: true,
            animation: Ext.isIE8 ? false : {
                easing: "backOut",
                duration: 500
            },
            background: '#FCFCFC',
            store: {
		        fields: ['val'],
		        data: [{
		            val: 85
		        }]
		    },
            axes: {
            	type: 'numeric',
            	majorTickSteps: 1,
                position: 'bottom'
            },
            series: {
            	colors: ["#F36148","#D7D7D7"],
                type: 'gauge',
                angleField: 'val',
                donut: 60
            },
            sprites: [{
            	type: "text",
                text: "85",
                font: "40px",
                x: 100,
                y: 100
            },{
            	type: "text",
                text: "average",
                font: "12px",
                x: 100,
                y: 113
            }]
		});
		return chart;
	},
	
	_buildColumnChart_ : function() {
		var chart = Ext.create({
			xtype: 'cartesian',
			flex: 1,
		    width: "100%",
		    //height: "100%",
			background: "#FCFCFC",
	        margin: 20,
	        interactions: "itemhighlight",
	        animation: Ext.isIE8 ? false : {
	            easing: "backOut",
	            duration: 500
	        },
		    store: {
		        fields: ["name", "g1"],
		        data: [
		            
		            {"name": "AAA", "g1": 22.96},
		            {"name": "BBB", "g1": 10.5},
		            {"name": "CCC", "g1": 20.87},
		            {"name": "DDD", "g1": 25.10},
		            {"name": "EEE", "g1": 16.87}
		        ]
		    },  
		
		    //set legend configuration
		    legend: {
		        docked: "bottom"
		    },
		
		    //define the x and y-axis configuration.
	
		    axes: [{
		        type: "category",
		        position: "bottom"
		    }, {
		        type: "numeric",
		        position: "left",
				grid: true
		    }],
		
		    //define the actual bar series.
	
	
			series: [{
	            type: "bar",
	            xField: "name",
				fill: true,
				title: ["Customer"],
	            yField: ["g1"],
				colors: ["#5D9BEE"],
	            style: {
	            	lineWidth: 1,
					fillOpacity: 0.7,
					minGapWidth: 20
	            },
	            highlight: {
	                fillStyle: "#5D9BEE",
					fillOpacity: 1,
	                radius: 0,
	                lineWidth: 1,
	                strokeStyle: "#5D9BEE"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        }]
		});
		return chart;
	},

	_buildLineChart_ : function() {
		var chart = Ext.create({
			xtype: 'cartesian',
			flex: 1,
		    width: "100%",
		    //height: "100%",
			background: "#FCFCFC",
	        margin: 20,
	        interactions: "itemhighlight",
	        animation: Ext.isIE8 ? false : {
	            easing: "backOut",
	            duration: 500
	        },
		    store: {
		        fields: ["name", "g1", "g2"],
		        data: [
		            {"name": "Jan.", "g1": 18.34,"g2": 0.04},
		            {"name": "Feb.", "g1": 2.67, "g2": 14.87},
		            {"name": "Mar.", "g1": 1.90, "g2": 5.72},
		            {"name": "Apr.", "g1": 21.37,"g2": 2.13},
		            {"name": "May", "g1": 2.67, "g2": 8.53},
		            {"name": "June", "g1": 18.22,"g2": 4.62},
		            {"name": "July", "g1": 28.51, "g2": 12.43},
		            {"name": "Aug.", "g1": 34.43, "g2": 4.40},
		            {"name": "Sep.", "g1": 21.65, "g2": 13.87},
		            {"name": "Oct.", "g1": 12.98, "g2": 35.44},
		            {"name": "Nov.", "g1": 22.96, "g2": 38.70},
		            {"name": "Dec.", "g1": 0.49, "g2": 51.90}
		        ]
		    },  
		
		    //set legend configuration
		    legend: {
		        docked: "bottom"
		    },
		
		    //define the x and y-axis configuration.
	
		    axes: [{
		        type: "category",
		        position: "bottom"
		    }, {
		        type: "numeric",
		        position: "left",
				grid: true
		    }],
		
		    //define the actual bar series.
	
	
			series: [{
	            type: "line",
	            xField: "name",
				fill: true,
				title: ["Previous period"],
	            yField: ["g1"],
				colors: ["#FFCE55"],
	            style: {
	                lineWidth: 2,
					fillOpacity: 0.7
	            },
	            marker: {
	                radius: 4
	            },
	            highlight: {
	                fillStyle: "#FB6E52",
					fillOpacity: 1,
	                radius: 5,
	                lineWidth: 2,
	                strokeStyle: "#FB6E52"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        },{
	            type: "line",
	            xField: "name",
				fill: true,
				title: ["Current period"],
	            yField: ["g2"],
				colors: ["#AC92ED"],
	            style: {
	                lineWidth: 2,
					fillOpacity: 0.7
	            },
	            marker: {
	                radius: 4
	            },
	            highlight: {
	                fillStyle: "#FB6E52",
					fillOpacity: 1,
	                radius: 5,
	                lineWidth: 2,
	                strokeStyle: "#FB6E52"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        }]
		});
		return chart;
	},
	
	_buildSecondLineChart_ : function() {
		var chart = Ext.create({
			xtype: 'cartesian',
			flex: 1,
		    width: "100%",
		    //height: "100%",
			background: "#FCFCFC",
	        margin: 20,
	        interactions: "itemhighlight",
	        animation: Ext.isIE8 ? false : {
	            easing: "backOut",
	            duration: 500
	        },
		    store: {
		        fields: ["name", "g1", "g2"],
		        data: [
		            {"name": "Jan.", "g1": 18.34,"g2": 0.04},
		            {"name": "Feb.", "g1": 2.67, "g2": 14.87},
		            {"name": "Mar.", "g1": 1.90, "g2": 5.72},
		            {"name": "Apr.", "g1": 21.37,"g2": 2.13},
		            {"name": "May", "g1": 22.96, "g2": 38.70},
		            {"name": "June", "g1": 0.49, "g2": 51.90},
		            {"name": "July", "g1": 20.87, "g2": 62.07},
		            {"name": "Aug.", "g1": 25.10, "g2": 78.46},
		            {"name": "Sep.", "g1": 16.87, "g2": 56.80},
		            {"name": "Oct.", "g1": 32, "g2": 8.53},
		            {"name": "Nov.", "g1": 18.22,"g2": 24.62},
		            {"name": "Dec.", "g1": 32.15,"g2": 10.5}
		           
		        ]
		    },  
		
		    //set legend configuration
		    legend: {
		        docked: "bottom"
		    },
		
		    //define the x and y-axis configuration.
	
		    axes: [{
		        type: "category",
		        position: "bottom"
		    }, {
		        type: "numeric",
		        position: "left",
				grid: true
		    }],
		
		    //define the actual bar series.
	
	
			series: [{
	            type: "line",
	            xField: "name",
				fill: true,
				title: ["Previous period"],
	            yField: ["g1"],
				colors: ["#A0D468"],
	            style: {
	                lineWidth: 2,
					fillOpacity: 0.7
	            },
	            marker: {
	                radius: 4
	            },
	            highlight: {
	                fillStyle: "#FB6E52",
					fillOpacity: 1,
	                radius: 5,
	                lineWidth: 2,
	                strokeStyle: "#FB6E52"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        },{
	            type: "line",
	            xField: "name",
				fill: true,
				title: ["Current period"],
	            yField: ["g2"],
				colors: ["#4FC0E8"],
	            style: {
	                lineWidth: 2,
					fillOpacity: 0.7
	            },
	            marker: {
	                radius: 4
	            },
	            highlight: {
	                fillStyle: "#FB6E52",
					fillOpacity: 1,
	                radius: 5,
	                lineWidth: 2,
	                strokeStyle: "#FB6E52"
	            },
	            tooltip: {
	                trackMouse: true,
	                style: "background: #fff",
	                showDelay: 0,
	                dismissDelay: 0,
	                hideDelay: 0,
	                renderer: function(tooltip, record, item) {
	                	tooltip.setHtml(item.field + ": " + record.get(item.field));
	                }
	            }
	        }]
		});
		return chart;
	},
	
	_buildGrid_ : function() {
		var grid = Ext.create('Ext.grid.Panel', {
			flex: 1,
			cls: "sone-child-grid",
	        store: {
		        fields: ["dueDate", "subject", "type", "status"],
		        data: [
		            {"dueDate": "10-10-2015", "subject": "New contract","type": "Task", "status": "Scheduled"},
		            {"dueDate": "15-10-2015", "subject": "Release invoice", "type": "Task", "status": "Completed"},
		            {"dueDate": "17-10-2015", "subject": "New contract","type": "Task", "status": "Scheduled"},
		            {"dueDate": "20-10-2015", "subject": "Release invoice","type": "Task", "status": "Completed"},
		            {"dueDate": "25-10-2015", "subject": "Release invoice","type": "Task", "status": "Completed"},
		            {"dueDate": "27-10-2015", "subject": "New contract","type": "Task", "status": "Completed"}
		        ]
		    },
	        columns: [
	            {text: "Due date", width:100, dataIndex: 'dueDate'},
	            {text: "Subject", width:100, dataIndex: 'subject'},
	            {text: "Type", width:100, dataIndex: 'type'},
	            {text: "Status", width:100, dataIndex: 'status'}
	        ]
	    });
		return grid;
	},
	
	_removeWidget_ : function(headerEl) {
		
		var widget = Ext.getCmp(headerEl.container.id);
    	var ds = "fmbas_DashboardWidget_Ds";
    	var layout = widget.up("panel");
    	var dashboardId = this._savedDashboardId_;
		var layoutId = this._savedLayoutId_;
    	
		widget.destroy();
		widget._isBooked_ = false;
    	
    	Ext.Ajax.request({
            url: Main.dsAPI(ds, "json").read,
            method: "POST",
            scope: this,
            params: {
              data: Ext.JSON.encode({
            	  dashboardId : dashboardId,
            	  layoutId : layoutId,
                  widgetId : widget._partConfig._dbId_,
                  layoutRegionId : layout._regionId_
              })
            },
            success: function(response) {
            	var res = Ext.getResponseDataInJSON(response).data[0];
            	Ext.Ajax.request({
                    url: Main.dsAPI(ds, "json").destroy,
                    method: "POST",
                    scope: this,
                    params: {
                      data: Ext.JSON.encode({
                    	  id: res.id
                      })
                    }
            	});
            	this._buildDashboard_();
            }
    	});
    	
	},
	
	_closeWidgetWindow_ : function() {
		var w = Ext.getCmp(this._widgetWindowId_);
		var cfgPanel = Ext.getCmp(this._configPanelId_);
		w.close();
	},
	
	_getWidgetRecord_ : function(widgetCmp) {
		var container = Ext.getCmp(widgetCmp._partConfig._containerId_);
		var widgetId = widgetCmp._partConfig._dbId_;
		var ds = "fmbas_Widget_Ds", r = [];
		
		Ext.Ajax.request({
        	url: Main.dsAPI(ds, "json").read,
        	method: "POST",
        	async: false,
//        	data: Ext.JSON.encode({id: widgetId}),
        	params: {
                data: Ext.JSON.encode({id: widgetId})
            },
        	scope: this,
        	success: function(response) {
        		var res = Ext.getResponseDataInJSON(response).data;
        		r.push({
        			ds: res[0].dsName,
        			id: widgetCmp._partConfig._dashboardWidgetDbId_,
        			layoutId: this._savedLayoutId_,
        			layoutRegionId: container._regionId_,
        			method: res[0].methodName,
        			widgetId: widgetCmp._partConfig._dbId_,
        			widgetName: res[0].name
        		});
			}
		});
		return r;
	},
	
	_warnDirty_ : function() {
		Main.warning(Main.translate("applicationMsg","dashboardDirty__lbl"));
	},
	
	_attachTools_ : function(obj, hideConfig) {
		var hide = false;
		if (hideConfig && hideConfig == true) {
			hide = true;
		}
		obj.tools = [{
			type: "gear",
			hidden: hide,
			scope: this,
			handler: function(event,toolEl,headerEl) {
				if (this._dashboardDirty_ === false) {
					var widgetCmp = Ext.getCmp(headerEl.container.id);
			    	var container = widgetCmp.child('[xtype=container]');
			    	var widget = this._getWidgetRecord_(widgetCmp);
			    	var assignedWidgetParams = this._getAssignedWidgetParameters_(widget[0].id);
			    	var dataList = [];
			    	Ext.each(assignedWidgetParams, function(param) {
			    		dataList.push({
			    			dashboardWidgetId: param.dashboardWidgetId,
			    			defaultValue: param.defaultValue,
			    			name: param.name,
			    			parameterId: param.parameterId,
			    			readOnly: param.readOnly,
			    			recordId: param.id,
			    			refBusinessValues: param.refBusinessValues,
			    			refValues: param.refValues,
			    			type: param.type,
			    			value: param.value,
			    			version: param.version,
			    			widgetId: param.widgetId
			    		});
			    	});
			    	this._createParamsForm_(widgetCmp, dataList, widget);	
				}
				else {
					this._warnDirty_();
				}
            }
		},{
	        type:'refresh',
	        tooltip: 'Refresh',
	        scope: this,
	        handler: function(event,toolEl,headerEl){	
	        	if (this._dashboardDirty_ === false) {
		        	var widgetCmp = Ext.getCmp(headerEl.container.id);
			    	var widget = this._getWidgetRecord_(widgetCmp);
			    	
			    	if (widget[0].widgetName === "My activities") {
			    		var widgetItems = this._getWidgetCfg_(widget);
			    		this._refreshActivities_();
			    	}		    	
			    	else if (widget[0].widgetName === "External link content") {		    		
			    		this._refreshExternalContent_(widgetCmp);
			    	}
			    	else {
			    		this._refreshWidgetData_(widget[0],widgetCmp);
			    	}
	        	}
				else {
					this._warnDirty_();
				}
	        }
	    },{
			type: "close",
			scope: this,
			handler: function(event,toolEl,headerEl) {
				if (this._dashboardDirty_ === false) {
					this._removeWidget_(headerEl);
				}
				else {
					this._warnDirty_();
				}
            }
		}];
	},
	
	_refreshWidgetData_ : function(widget,panel) {
		var rpcData = {
			id: widget.id
		}
		return Ext.Ajax.request({
        	url: Main.dsAPI(widget.ds, "json").service,
        	method: "POST",
        	scope: this,
        	params : {
        		rpcName: widget.method,
        		rpcType: "data",
        		data: Ext.JSON.encode(rpcData)
			},
        	success: function(response, opts) {
        		
        		var responseText = Ext.getResponseDataInJSON(response);
        		var res = responseText.params;
        		
        		if (!Ext.isEmpty(res.rpcResponse)) {
        			
        			var rpcResponse = Ext.JSON.decode(res.rpcResponse);       			
        			var item = this._getWidgetCfg_(widget)[0];
        			var updatedPanelCfg = this._renderWidgetData_(item,rpcResponse);
        			
    				var panelheader = panel.getHeader();
    				var childPanel = panel.child('[xtype=container]');
    				
    				if (widget.widgetName === "My activities") {
        		    	panelheader.setTitle('<div style="font-weight:600; height:35px; line-height:35px; display: inline-block">'+widget.widgetName+'</div><div class="sone-user-activities-counter">'+rpcResponse.length+'</div>');
        		    	childPanel = childPanel.down("container");
    				}
    				else if (widget.widgetName === "Uplifted volume") {
        		    	panelheader.setTitle('<div style="font-weight:600">'+widget.widgetName+'</div><div style="text-transform:none">'+rpcResponse['PERIOD']+'</div>');
    				}
    				childPanel.removeAll();    				
    				childPanel.removeAll();    				
    				if (widget.widgetName === "My activities") {
    					childPanel.add(updatedPanelCfg.items[0]);
    				}
    				else {
    					childPanel.setHtml(updatedPanelCfg.html);
    				}
        		}       		
			}
		});
	},
	
	_getAssignedWidgetParameters_ : function(dashboardWidgetId) {
		var paramList = [];
		var ds = "fmbas_AssignedWidgetParameter_Ds";
		Ext.Ajax.request({
        	url: Main.dsAPI(ds, "json").read,
        	method: "POST",
        	scope: this,
        	async: false,
        	params: {
            data: Ext.JSON.encode({
            		dashboardWidgetId: dashboardWidgetId
	        	})
	        },
        	success: function(response) {
        		var responseText = Ext.getResponseDataInJSON(response);
        		var res = responseText.data;
        		
        		if (responseText.totalCount > 0) {
        			paramList = res;
        		}
			}
		});
		return paramList;
	},
	
	_getWidgetParameters_ : function(widgetId) {
		var paramList = [];
		var ds = "fmbas_WidgetParameter_Ds";
		Ext.Ajax.request({
        	url: Main.dsAPI(ds, "json").read,
        	method: "POST",
        	scope: this,
        	async: false,
        	params: {
            data: Ext.JSON.encode({
            		widgetId: widgetId
	        	})
	        },
        	success: function(response) {
        		var responseText = Ext.getResponseDataInJSON(response);
        		var res = responseText.data;
        		if (responseText.totalCount > 0) {
        			Ext.each(res, function(item) {
        				paramList.push({
        					id : item.id,
        					value: item.value,
        					defaultValue: item.defaultValue,
        					readOnly: item.readOnly,
        					refValues: item.refValues,
        					type: item.type,
        					name: item.name,
        					refBusinessValues: item.refBusinessValues
            			});
        			});
        		}
			}
		});
		return paramList;
	},
	
	_saveAssignedParameters_ : function(container, response, widget) {
		
		var dsName = "fmbas_AssignedWidgetParameter_Ds";
		var res = Ext.getResponseDataInJSON(response).data[0];
		var widgetId = res.widgetId;
		var dashboardWidgetId = res.id;
		var widgetParams = this._getWidgetParameters_(widgetId);
		var dataList = [];
		var ctx = this;
		
		if (widgetParams.length > 0) {
			
			Ext.each(widgetParams, function(param) {
				dataList.push({
					dashboardWidgetId: dashboardWidgetId,
					widgetId: widgetId,
					parameterId : param.id,
					value: param.value,
					defaultValue: param.defaultValue,
					readOnly: param.readOnly,
					refValues: param.refValues,
					type : param.type,
					name: param.name,
					refBusinessValues: param.refBusinessValues
				});
			}, this);
			if (dataList.length > 0) {
				Ext.Ajax.request({
		            url: Main.dsAPI(dsName, "json").create,
		            method: "POST",
		            params: {
		                data: Ext.JSON.encode(dataList)
		            },
		            success: function(response) {
		            	var res = Ext.getResponseDataInJSON(response).data;
		            	Ext.each(dataList, function(item) {
		            		Ext.each(res, function(responseItem) {
			            		if (item.parameterId == responseItem.parameterId) {
			            			item.recordId = responseItem.id;
			            			item.dashboardWidgetId = responseItem.dashboardWidgetId;
			            			item.parameterId = responseItem.parameterId;
			            			item.version = responseItem.version;
			            			item.defaultValue = responseItem.defaultValue;
			            			item.refValues = responseItem.refValues;
			            			item.readOnly = responseItem.readOnly;
			            			item.defaultValue = responseItem.defaultValue;
			            			item.refBusinessValues = responseItem.refBusinessValues;
			            		}
		            		});
		            	});	
		            	container.addCls("x-panel-creating");
		            	ctx._createParamsForm_(container, dataList, widget, true, true);
		            }
				});
			}
		}
	},
	
	_createParamsForm_ : function(container, paramsList, widget, isFirstInstance, withBorder) {
		var params = [];
		var _paramsFormId_ = Ext.id();
		var context = this;
		this._dashboardDirty_ = true;
		Ext.each(paramsList, function(param) {
			var type = param.type;
			if (!Ext.isEmpty(type)) {

				//////////// CASE 1 - Type is enumeration ////////////

				if (type == "enumeration") {
					var arrayVals = param.refValues.split(",");
					var trimmedArray = [];
					Ext.each(arrayVals, function(val) {
						trimmedArray.push(val.trim());
					});
					
					cfg = {
						xtype: "combo",  
						fieldLabel: param.name,
						labelAlign: "top",
			            selectOnFocus: true,
			            queryMode: "local",
			            pageSize : 0,
			            editable: false,
			            store: trimmedArray,
						readOnly: param.readOnly == true ? true : false
					};
					
				}

				//////////// CASE 2 - Type is lov ////////////

				else if (type == "lov") {
					cfg = {
						xtype: param.refValues, 
						fieldLabel: param.name,
						labelAlign: "top",
						maxLength:32, 
						typeAhead: false,
						remoteFilter: true,
						remoteSort: true,
						forceSelection: true,
						readOnly: param.readOnly == true ? true : false
					};
				}
				
				//////////// CASE 3 - Type is string ////////////

				else if (type == "string") {
					cfg = {
						xtype:"textfield", 
						fieldLabel: param.name,
						labelAlign: "top",
						readOnly: param.readOnly == true ? true : false,
						allowBlank: (widget[0].widgetName === "External link content" && param.name === "URL") ? false : true,
					};				}
				
				//////////// Type is textarea ////////////

				else if (type == "textarea") {
					cfg = {
						xtype: "textareafield", 
						fieldLabel: param.name,
						labelAlign: "top",
						readOnly: param.readOnly == true ? true : false
					};
				}
				
				//////////// CASE 4 - Type is number ////////////

				else if (type == "integer") {
					cfg = {
						xtype: "numberfield", 
						fieldLabel: param.name,
						labelAlign: "top",
						readOnly: param.readOnly == true ? true : false
					};
				}
				
				else if (type == "key") {
					cfg = {
						xtype: "filefield", 
						fieldLabel: param.name,
						labelAlign: "top",
						allowBlank: false,
						readOnly: param.readOnly == true ? true : false
					};
				}
				
				cfg._oldRecord_ = {
					id: param.recordId,
					dashboardWidgetId: param.dashboardWidgetId,
					parameterId: param.parameterId,
					version: param.version,
					defaultValue: param.defaultValue,
					readOnly: param.readOnly,
					refValues: param.refValues,
					refBusinessValues: param.refBusinessValues					
				}
				if (!Ext.isEmpty(param.value)) {
					cfg.value = param.value;
				}
				else {
					cfg.value = param.defaultValue;
				}
				
				params.push(cfg);

			}
		}, this);
		var form = {
			xtype: "panel",
			id: _paramsFormId_,
			flex: 1,
			anchor: "100% 100%",
			bodyStyle: Ext.isEmpty(withBorder) ? "background:  transparent !important" : "background:  #FFF !important; border:1px solid #dadbdc !important",
			cls: "sone-chart-widget sone-portlet-params-form",
//			title: '<div style="font-weight:bold; padding:8px 0px">'+Main.translate("applicationMsg", "editWidgetParameters__lbl")+'</div>',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
			   xtype: 'form',
			   style: "background: #FFF !important",
			   bodyStyle: "background: #FFF !important",
			   cls: 'sone-portlet-params-form-inner',
			   layout: {
					type: 'vbox',
					align: 'stretch'
			   },
			   flex: 1,
			   padding: "8px 12px 0px 12px",
			   items: params,
		   	   buttonAlign: 'center',
			   buttons: [{
				   text: Main.translate("applicationMsg", "saveConfiguration__lbl"),
				   formBind: true,
				   scope: this,
				   handler: function(btn) {
					   
					   var form = btn.up('form');
                       var formItems = form.items.items;
                       var updateItems = []; 
                	   
                	   var theWidget = widget[0];
                	   
                	   var f = function(item, blobValue) {
                           updateItems.push({
                               __clientRecordId__: item._oldRecord_.id,
                               id: item._oldRecord_.id,
                               dashboardWidgetId: item._oldRecord_.dashboardWidgetId,
                               parameterId: item._oldRecord_.parameterId,
                               value: item.value,
                               blobValue : blobValue,
                               version: item._oldRecord_.version,
                               defaultValue: item._oldRecord_.defaultValue,
                               readOnly: item._oldRecord_.readOnly,
                               refValues: item._oldRecord_.refValues,
                               refBusinessValues: item._oldRecord_.refBusinessValues
                           });
                	   }
                	   if (theWidget.widgetName !== "Custom Image") {
                		   Ext.each(formItems, function(item) { 
                			   f(item, null);
                           });
                		   context._updateAssignedParamValues_(updateItems, container, btn);
                	   }
                	   else {
                		   Ext.each(formItems, function(item) {
                			   
                			   var fileField = form.down("filefield");
							   var file = fileField.el.down('input[type=file]').dom.files[0];
							   if (file == null) {
								   Main.info(Main.translate("applicationMsg","nofile__lbl"),{closable:true});
							   }
							   var fileType = file.type;
							   var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
							   if (($.inArray(fileType, ValidImageTypes) < 0) && file.size > 200000) {
							        Main.info(Main.translate("applicationMsg","imgSizeRestriction__lbl"),{closable:true});
							        return;
							   } else if ($.inArray(fileType, ValidImageTypes) < 0) {
								   Main.info(Main.translate("applicationMsg","imgExtensionRestriction__lbl"),{closable:true});
								   return;
							   } else if (file.size > 200000) {
								   Main.info(Main.translate("applicationMsg","imgExceedRestriction__lbl"),{closable:true});
								   return;
							   }
							   
							   var reader = new FileReader();
							   
							   reader.readAsDataURL(file);
							   reader.onload = function() {
								   var img = reader.result;
								   var imgToSave = img.split(",")[1];								   
								   f(item, imgToSave)
								   context._updateAssignedParamValues_(updateItems, container, btn); 								   
							   };
							   reader.onerror = function() {
							        //console.log('there are some problems');
							   };
    					   });
                	   }
                	   this._dashboardDirty_ = false;
					   
				   }
			   }, {
				   text: Main.translate("applicationMsg", "widgetCancelBtn__lbl"),
				   scope: this,
				   hidden: isFirstInstance === true ? true : false,
				   handler: function(btn) {
					   var form = Ext.getCmp(_paramsFormId_);
					   this._dashboardDirty_ = false;
					   form.destroy();
				   }
			   }]
		   }]
		}
		container.add(form);
		container._isBooked_ = true;
		
	},

	_updateAssignedParamValues_ : function(data, widget, btn) {
		var dsName = "fmbas_AssignedWidgetParameter_Ds";		
		Ext.Ajax.request({
            url: Main.dsAPI(dsName, "json").update,
            method: "POST",
            params: {
                data: Ext.JSON.encode(data)
            },
            success: function(response) {
            	var panel = widget.up('panel');
            	if (panel.hasCls("x-dashboard-panel")) {
            		var widgetRecord = this._getWidgetRecord_(panel)[0];
                	this._refreshWidgetData_(widgetRecord,panel);
                	if (widgetRecord.widgetName === "My activities") {
                		var form = btn.up('form');
                		var formPanel = form.up('panel');
                		formPanel.destroy();
                    	this._dashboardDirty_ = false;
                	}
            	}
            	else {
            		var form = btn.up('form');
            		var formPanel = form.up('panel');
            		formPanel.destroy();
            		this._buildDashboard_();
            	}
            	
            },
            failure: function(response) {
            	this._dashboardDirty_ = false;
    	        Main.error(Ext.getResponseErrorText(response)); 
            },
            scope: this
        });
	},
	
	_saveWidget_ : function(widget) {
		
		var widgetId = widget.id;
		var w = Ext.getCmp(this._widgetWindowId_);
		var container = w._targetContainer_; 
		var ctx = this;
		var regionId = container._regionId_;
		var dashboardId = this._savedDashboardId_;
		var layoutId = this._savedLayoutId_;
		
		var dsName = "fmbas_DashboardWidget_Ds";
		
		var portlets = [];
		var layoutColumn = null;
		var containerItems = container.items.items;	
		var widgetIndex = 0;
		if (containerItems.length > 0) {
			Ext.each(containerItems, function(item, index) {
				if (item.initialCls === "x-dashboard-column") {
					layoutColumn = item;					
				}
			},this);
		}			
		if (!Ext.isEmpty(layoutColumn)) {
			var layoutItems = layoutColumn.items.items;
			Ext.each(layoutItems, function(item, index) {
				if (item.initialCls === "x-dashboard-panel") {
					portlets.push({
						item: item,
						index: index
					});
				}
			},this)
		}
		
		Ext.Ajax.request({
            url: Main.dsAPI(dsName, "json").create,
            method: "POST",
            params: {
                data: Ext.JSON.encode({
                	dashboardId: dashboardId,
                	layoutId: layoutId,
                	widgetId: widgetId,
                	layoutRegionId: regionId,
                	widgetIndex: portlets.length > 0 ? portlets.length : widgetIndex
                })
            },
            success: function(response) {
            	
            	var res = Ext.getResponseDataInJSON(response).data[0], widgetRecord = [];
            	widgetRecord.push({
        			ds: res.widgetDs,
        			id: res.id,
        			layoutId: res.layoutId,
        			layoutRegionId: res.layoutRegionId,
        			method: res.widgetMethod,
        			widgetId: res.widgetId,
        			widgetName: res.widgetName
        		});
            	this._saveAssignedParameters_(container, response, widgetRecord);
            },
            failure: function(response) {
    	        Main.error(Ext.getResponseErrorText(response)); 
            },
            scope: this
        });
		this._closeWidgetWindow_();
	},
	
	_assignWidget_ : function(item) {
		this._saveWidget_(item);		
	},
	
	_createWidgetItems_ : function(containerId) {
		
		var ctx = this;
		var ds = "fmbas_Widget_Ds";
    	var items = [];
		Ext.Ajax.request({
        	url: Main.dsAPI(ds, "json").read,
        	method: "POST",
        	scope: this,
        	success: function(response) {
        		var res = Ext.getResponseDataInJSON(response).data;
        		var container = Ext.getCmp(this._widgetWindowContainerId_);
        		Ext.each(res, function(item) {
        			item.layoutRegionId = containerId;
        			items.push({
        				xtype: "panel",
        				cls: "sone-widget-item",
        				height:100,
        				margin: "0px 0px 20px 0px",
        				html: '<div style="display: table; table-layout: fixed; height:100px"><div style="display: table-cell; width: 100px; border:1px solid #CCC; min-height: 100px; box-sizing: border-box; padding: 5px; background: #f8f8f8 url(\''+item.thumbPath+'\') no-repeat top center"></div><div style="display: table-cell; padding-left: 10px"><span class="sone-widget-title">'+item.name+'</span>'+item.description+'</div></div>',
        				listeners: {
        					boxready: {
        						scope: this,
        						fn: function(panel) {
        							panel.body.on('click', function() {
        								ctx._assignWidget_(item);
        							});
        						}
        					}
        				}
        			});
        		}, this);
        		container.suspendLayouts();
        		container.add(items);
        		container.resumeLayouts(true);
			}
		});
	},
	
	_showWidgetWindow_ : function(container) {
		var targetContainer = Ext.getCmp("dnet-application-view-home");
		var ctx = this;
		var w = Ext.create('Ext.window.Window', {
		   height: 400,
		   width: 700, 
		   modal: true,
		   renderTo: targetContainer.getEl(),
		   id: this._widgetWindowId_,
		   _targetContainer_ : container,
		   overflowY: "auto",
		   items: [{
			   xtype: "container",
			   padding: "0px 15px 15px 15px",
			   id: this._widgetWindowContainerId_,
//			   listeners: {
//				   afterrender: {
//					   scope: this,
//					   fn: function() {
//						   this._createWidgetItems_(container._regionId_);
//					   }
//				   }
//			   },
			   items: this._createWidgetItems_()
		   }]
		});
		w.show();
	},
	
	_createWidgetSelector_ : function(panelCmp) {
//		var panel = {
//			layout: {
//                type: 'hbox',
//                align: 'center',
//                pack: 'center'
//            },
//			items: [{
//				xtype: "button",
//				glyph: "xf067@FontAwesome",
//				cls: "sone-select-widget-btn",
//				scope: this,
//				handler: function() {
//					this._showWidgetWindow_(panelCmp);
//				}
//			}]
//		}
//		return panel;	
		
		
//		var panel = {			
//			items: [{
//        	    xtype: 'dashboard',
//        	    layout: {
//        	        type: 'vbox',
//        	        align : 'stretch',
//        	        pack  : 'start'
//        	    },
//                parts: {
//                    portlet: {
//        				viewTemplate: {
//        					margin: 20,
//        					padding:20,
//        					style: "border:1px solid #c1c1c1 !important",
//        					html: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ullamcorper risus sit amet volutpat suscipit. Etiam scelerisque purus in tristique malesuada. Phasellus nec nisl non lorem ultrices egestas a eget erat. Aenean viverra dapibus ante sed malesuada. Suspendisse lobortis vehicula venenatis. Donec nec ex ac ex euismod tempus. Aenean convallis enim eu nisl condimentum condimentum. Suspendisse mollis ac sapien vel finibus. "
//        				}				
//        			}            
//                },	
//        		defaultContent: [{
//        			type: 'portlet',
//        			title: 'Test1',
//        		    height: 100,
//        		    flex:1
//        		}, {
//        			type: 'portlet',
//        			title: 'Test2',
//        		    height: 100,
//        		    flex:1
//        		}, {
//        			type: 'portlet',
//        			title: 'Test3',
//        		    height: 100,
//        		    flex:1
//        		}, {
//        			type: 'portlet',
//        			title: 'Test4',
//        		    height: 10,
//        		    flex:1
//        		}, {
//        			type: 'portlet',
//        			title: 'Test5',
//        		    height: 100,
//        		    flex:1
//        		}]
//        	}]
//            
//		}
//		return panel;
	},
	
	_configWidgetPanel_ : function(layout) {

		var me = this;
//		var mouseListeners = function(panel) {	
//			var panelCmp, listener;
//			listener = {
//				el: {
//			        mouseenter: {
//			            fn: function () {	
//			            	panelCmp = Ext.getCmp(panel.id);
//			            	if (panelCmp._isBooked_ == false) {
//			            		panelCmp.add(me._createWidgetSelector_(panelCmp));
//			            	}			            	
//			            }
//			        },
//			        mouseleave: {
//			            fn: function () {
//			            	panelCmp = Ext.getCmp(panel.id);
//			            	if (panelCmp._isBooked_ == false) {
//			            		panelCmp.removeAll();
//			            	}
//			            	
//			            }
//			        }
//			    }	
//			}
//			
//			return listener;
//		}
		
		var applyCfg = function(panel) {			
			var defaultCfg = {
				id: Ext.id(),
				_isBooked_ : false
			}			
			for (var key in defaultCfg) {
				panel[key] = defaultCfg[key];				
			}
//			panel.listeners = mouseListeners(panel);
		}
		
		Ext.each(layout, function(panel) {
			if (!Ext.isEmpty(panel.items)) {
				Ext.each(panel.items, function(childPanel) {
					applyCfg(childPanel);
				}, this);
			}
			else {
				applyCfg(panel);
			}
		}, this);
		
	},
	
	_saveDashboard_ : function(layoutBtn) {
		var nameField = Ext.getCmp(this._idDashboardNameField_);
		var descField = Ext.getCmp(this._idDashboardDescField_);
		var defaultField = Ext.getCmp(this._idDashboardDefaultField_);
		var name = nameField.getValue();
		var desc = descField.getValue();
		var isDefault = defaultField.getValue();
		var dsName = "fmbas_Dashboard_Ds";
		var window = Ext.getCmp(this._dashboardWindowId_);
		var cfgPanel = Ext.getCmp(this._configPanelId_);
		
		Ext.Ajax.request({
            url: Main.dsAPI(dsName, "json").create,
            method: "POST",
            params: {
                data: Ext.JSON.encode({
                    name: name,
                    description: desc,
                    layoutId: layoutBtn._dbId_,
                    isDefault: isDefault
                })
            },
            success: function(response) { 
            	// _savedDashboardId_
            	var res = Ext.getResponseDataInJSON(response).data[0];
            	
            	this._savedDashboardId_ = res.id;
            	this._savedLayoutId_ = layoutBtn._dbId_;
            	cfgPanel.body.hide();
            	cfgPanel.removeCls("panel-expanded");
        		window.close();
            },
            failure: function(response) {
    	        Main.error(Ext.getResponseErrorText(response));
            	cfgPanel.body.hide();
            	cfgPanel.removeCls("panel-expanded");
        		window.close();
            },
            scope: this
        });
		
	},
	
	_showSaveDashboardWdw_ : function(initiator) {
		var targetContainer = Ext.getCmp("dnet-application-view-home");
		var ctx = this;
		
		var w = Ext.create('Ext.window.Window', {
		   height: 300,
		   width: 360, 
		   modal: true,
		   renderTo: targetContainer.getEl(),
		   id: this._dashboardWindowId_,
		   title: Main.translate("applicationMsg", "saveDashboard__lbl"),
		   closable: false,		   
		   items: [{
			   xtype: 'form',
			   layout: "vbox",
			   bodyPadding: "0px 15px 0px 15px",
			   items: [{
				   xtype: "textfield",
				   fieldLabel: Main.translate("applicationMsg", "dashboardName__lbl"),
				   labelAlign: "top",
				   width: 330,
				   id: this._idDashboardNameField_,
				   allowBlank: false
			   }, {
				   xtype: "textarea",
				   fieldLabel: Main.translate("applicationMsg", "dashboardDescription__lbl"),
				   labelAlign: "top",
				   width: 330,
				   height: 120,
				   id: this._idDashboardDescField_,
				   allowBlank: false			   
			   }, {
				   xtype: "checkbox",
				   checked: true,
				   readOnly: true,
				   fieldLabel: Main.translate("applicationMsg", "dashboardSetAsDefault__lbl"),
				   id: this._idDashboardDefaultField_,
				   labelWidth: false,
	            	labelStyle: 'width: auto'
			   }],
		   	   buttonAlign: 'center',
			   buttons: [{
				   text: Main.translate("applicationMsg", "widgetSaveBtn__lbl"),
				   formBind: true,
				   scope: this,
				   handler: function() {
					   this._saveDashboard_(initiator);
				   }
			   }, {
				   text: Main.translate("applicationMsg", "widgetCancelBtn__lbl"),
				   scope: this,
				   handler: function() {
					   var w = Ext.getCmp(this._dashboardWindowId_);
					   w.close();
				   }
			   }]
		   }]
		});
		w.show();
	},
	
	_getWidgetCfg_ : function(widgets) {
		
		// Should get the widget JSON first
		var widgetDs = "fmbas_Widget_Ds";
		var items = [];
		Ext.each(widgets, function(widget) {			
			Ext.Ajax.request({
	        	url: Main.dsAPI(widgetDs, "json").read,
	        	method: "POST",
	        	scope: this,
	        	async: false,
	        	params: {
	            data: Ext.JSON.encode({
	          	  		id: widget.widgetId
		        	})
		        },
	        	success: function(response) {
	        		var responseText = Ext.getResponseDataInJSON(response);
	        		var res = responseText.data[0];
	        		var container = Ext.ComponentQuery.query('[_regionId_="'+widget.layoutRegionId+'"]')[0];
	        		if (responseText.totalCount > 0) {
	        			Ext.Ajax.request({
	        	        	url: res.cfgPath,
	        	        	scope: this,	  
	        	        	async: false,
	        	        	success: function(response) {
	        	        		var obj = Ext.getResponseDataInJSON(response);
	        	        		obj._containerId_ = container.id;
	        	        		items.splice(widget.widgetIndex,0,{
	    	        				layoutRegionId: widget.layoutRegionId,
	    	        				dashboardWidgetId: widget.id,
	    	        				id: widget.widgetId,
	    	        				widgetCfg: obj,
	            					ds: res.dsName,
	            					method: res.methodName,
	            					layoutId: widget.layoutId,
	            					widgetId: widget.widgetId,
	            					code: res.code,
	            					name: res.name,
	            					widgetIndex : widget.widgetIndex,
	            					dashboardId : widget.dashboardId,
	            					version : widget.widgetVersion
	    	        			});
	        	        	}
	        			});
	        		}
	        	}
			});
		});
		return items;	
		
	},
	
	_refreshExternalContent_ : function(widget) {
		var ctx = this;
		var widgetCfg = widget._partConfig;
		var widgetparams = this._getAssignedWidgetParameters_(widgetCfg._dashboardWidgetDbId_), url, requestHeader, requestBody, requestMethod, responseFormat, fn;
		fn = function() {
			if (!Ext.isEmpty(widgetparams) && widgetparams.length > 0) {
				Ext.each(widgetparams, function(param) {
					if (param.name === "METHOD") {
						requestMethod = param.value;
					} if (param.name === "URL") {
						url = param.value;
					} if (param.name === "HEADER") {
						requestHeader = param.value;
					} if (param.name === "BODY") {
						requestBody = param.value;
					} if (param.name === "RESPONSE FORMAT") {
						responseFormat = param.value;
					}    						
				},this);
				if (url && requestMethod && responseFormat) {
					
					var preFlightHeader = {};
					var preFlightBody = {};
					var headerParam = requestHeader.split("\n");
					var bodyParam = requestBody.split("\n");
					
					Ext.each(headerParam,function(p) {    							
						preFlightHeader[p.split(":")[0]] = p.split(":")[1];
					},this);
					
					Ext.each(bodyParam,function(b) {    							
						preFlightBody[b.split(":")[0]] = b.split(":")[1];
					},this);
					
					Ext.Ajax.request({
    		        	url: url,
    		        	method: requestMethod,
    		        	scope: this,
    		        	headers: preFlightHeader,
    		        	params : preFlightBody,
    		        	success: function(response, opts) {
    		        		var isJsonString = function(str) {
    		        		    try {
    		        		        JSON.parse(str);
    		        		    } catch (e) {
    		        		        return false;
    		        		    }
    		        		    return true;
    		        		}, responseText;
//    		        		if (isJsonString(response.responseText) === true) {
//    		        			responseText = Ext.JSON.decode(response.responseText)
//    		        		}
//    		        		else {
//    		        			responseText = response.responseText;
//    		        		}
    		        		responseText = Ext.getResponseDataInJSON(response);
    		        		widget.removeAll();
    		        		if (!Ext.isEmpty(responseText)) { 
    		        			var widgetRecord = ctx._getWidgetRecord_(widget);
    		        			var item = ctx._getWidgetCfg_(widgetRecord);
    		        			var updatedPanelCfg = ctx._renderWidgetData_(item[0],responseText);    		        			
    		    				var panelheader = widget.getHeader();
    		    				var childPanel = widget.child('[xtype=container]');
    		    				widget.removeAll();    				
    		    				widget.setHtml(updatedPanelCfg.html);
    		        		}
    					}
    				});
				}
			}
		}
		return fn();
		
		
//		var ctx = this;		
//		if (this._externalContentWidgets_.length > 0) {
//			Ext.each(this._externalContentWidgets_, function(w) {
//				var widgetPanel = w.up("panel");
//				var widgetRecord = this._getWidgetRecord_(widgetPanel)[0];				
//				var requestParams = {
//		    		rpcName: widgetRecord.method,
//		    		rpcType: "data",
//		    		data: Ext.JSON.encode({
//		    			id : widgetRecord.id
//		    		})
//				};
//				Ext.Ajax.request({
//		        	url: Main.dsAPI(widgetRecord.ds, "json").service,
//		        	method: "POST",
//		        	scope: this,
//		        	params : requestParams,
//		        	success: function(response, opts) {  
//		        		var responseText = Ext.getResponseDataInJSON(response);
//		        		var res = responseText.params;
//		        		if (!Ext.isEmpty(res.rpcResponse)) {		        			
//		        			var rpcResponse = Ext.JSON.decode(res.rpcResponse);		        			
//		        			var panel = widgetPanel;
//        					var panelheader = panel.header;
//        					var firstLevelItems = panel.items.items[0];
//                			var secondLevelItems = firstLevelItems.items.items[0];                			
//                			var accordionPanel = secondLevelItems.down("container");
//                			var item = widgetRecord;                			
//                			item._widgetParams_ = this._getAssignedWidgetParameters_(item.id);
//                			var activities = this._listActivities_(rpcResponse,item);
//        					panelheader.setTitle('<div style="font-weight:600; height:35px; line-height:35px; display: inline-block">'+widgetRecord.widgetName+'</div><div class="sone-user-activities-counter">'+rpcResponse.length+'</div>');
//        					accordionPanel.removeAll();
//        					accordionPanel.add(activities);
//		        		} 
//		        	}
//				});
//				
//			},this);
//		}
	},
	
	_refreshActivities_ : function() {
		
		var ctx = this;		
		if (this._activityWidgets_.length > 0) {
			Ext.each(this._activityWidgets_, function(w) {
				var widgetPanel = w.up("panel");
				var widgetRecord = this._getWidgetRecord_(widgetPanel)[0];				
				var requestParams = {
		    		rpcName: widgetRecord.method,
		    		rpcType: "data",
		    		data: Ext.JSON.encode({
		    			id : widgetRecord.id
		    		})
				};
				Ext.Ajax.request({
		        	url: Main.dsAPI(widgetRecord.ds, "json").service,
		        	method: "POST",
		        	scope: this,
		        	params : requestParams,
		        	success: function(response, opts) {  
		        		var responseText = Ext.getResponseDataInJSON(response);
		        		var res = responseText.params;
		        		if (!Ext.isEmpty(res.rpcResponse)) {		        			
		        			var rpcResponse = Ext.JSON.decode(res.rpcResponse);		        			
		        			var panel = widgetPanel;
        					var panelheader = panel.header;
        					var firstLevelItems = panel.items.items[0];
                			var secondLevelItems = firstLevelItems.items.items[0];                			
                			var accordionPanel = secondLevelItems.down("container");
                			var item = widgetRecord;                			
                			item._widgetParams_ = this._getAssignedWidgetParameters_(item.id);
                			var activities = this._listActivities_(rpcResponse,item);
        					panelheader.setTitle('<div style="font-weight:600; height:35px; line-height:35px; display: inline-block">'+widgetRecord.widgetName+'</div><div class="sone-user-activities-counter">'+rpcResponse.length+'</div>');
        					accordionPanel.removeAll();
        					accordionPanel.add(activities);
		        		} 
		        	}
				});
				
			},this);
		}
	},
	
	_listActivities_ : function(rpcResponse,item) {	
		
		var params = item._widgetParams_;
		var periodGroupParamValue = "";
		Ext.each(params, function(p) {
			if (p.name === "GROUP ACTIVITIES BY") {
				periodGroupParamValue = p.value;
			}
		}, this);
		
		var groupBy = function(collection, property) {
		    var i = 0, val, index,
	        values = [], result = [];
		    for (; i < collection.length; i++) {
		        val = collection[i][property];
		        index = values.indexOf(val);
		        if (index > -1)
		            result[index].push(collection[i]);
		        else {
		            values.push(val);
		            result.push([collection[i]]);
		        }
		    }
		    return result;
		}
		var groupedObj = groupBy(rpcResponse, "group");
		
		if (periodGroupParamValue === "Period") {
			groupedObj = groupBy(rpcResponse, "periodGroup");
		}
		
		var panelBody = [];
		for (var i =0;i<groupedObj.length;i++) {			
			var panelItems = [];			
			for (var z = 0; z < groupedObj[i].length; z++) {
								
				var openBtn = '&#9679; <i class="fa fa-external-link" aria-hidden="true" style="color: #83c250;" onclick="Open(\''+groupedObj[i][z].entityFrame+'\',\''+groupedObj[i][z].entityModule+'\',\''+groupedObj[i][z].entityId+'\',\''+groupedObj[i][z].group+'\')"></i>';
				if (Ext.isEmpty(groupedObj[i][z].entityId) || Ext.isEmpty(groupedObj[i][z].group)) {
					openBtn = "";
				}
				
				var theHtml = '<div class="sone-user-activities-table">'+
	        	'<div class="sone-user-activities-checkbox"><div class="sone-hover-icon" style="cursor:pointer" onclick="Complete(\''+groupedObj[i][z].workflowInstanceId+'\',\''+groupedObj[i][z].taskId+'\')" title="'+Main.translate("applicationMsg", "activityWidgetComplete__lbl")+'"></div></div>'+
	        	'<div class="sone-user-activities-body">'+
	        	'<span class="sone-user-activities-title">'+groupedObj[i][z].subject+'</span>'+
	        	'<span class="sone-tooltip-container" title="'+Main.translate("applicationMsg", "activityWidgetDateCreated__lbl")+'"><i class="fa fa-calendar" aria-hidden="true"></i> '+groupedObj[i][z].createdAt.split(" ")[0]+'</span> &#9679; <span class="sone-tooltip-container" title="'+Main.translate("applicationMsg", "activityWidgetRequester__lbl")+'"><i class="fa fa-user" aria-hidden="true"></i> '+groupedObj[i][z].createdBy+
	        	'</span> &#9679;  <span class="sone-tooltip-container" title="'+Main.translate("applicationMsg", "activityWidgetMoreInfo__lbl")+'"><i class="fa fa-info-circle" aria-hidden="true" style="color: #e67e22; cursor: pointer" onclick="Review(\''+groupedObj[i][z].detail+'\')"></i></span> <span class="sone-tooltip-container" title="'+Main.translate("applicationMsg", "activityWidgetOpenForReview__lbl")+'">'+openBtn+
	        	'</span></div>'+
	        	'</div>';
				
				if (periodGroupParamValue === "Period") {
					theHtml = '<div class="sone-user-activities-table">'+
		        	'<div class="sone-user-activities-checkbox"><div class="sone-hover-icon" style="cursor:pointer" onclick="Complete(\''+groupedObj[i][z].workflowInstanceId+'\',\''+groupedObj[i][z].taskId+'\')" title="'+Main.translate("applicationMsg", "activityWidgetComplete__lbl")+'"></div></div>'+
		        	'<div class="sone-user-activities-body">'+
		        	'<span class="sone-user-activities-title">'+groupedObj[i][z].subject+'</span>'+
		        	groupedObj[i][0].group+' &#9679; <span class="sone-tooltip-container" title="'+Main.translate("applicationMsg", "activityWidgetDateCreated__lbl")+'"><i class="fa fa-calendar" aria-hidden="true"></i> '+groupedObj[i][z].createdAt.split(" ")[0]+'</span> &#9679; <span class="sone-tooltip-container" title="'+Main.translate("applicationMsg", "activityWidgetRequester__lbl")+'"><i class="fa fa-user" aria-hidden="true"></i> '+groupedObj[i][z].createdBy+
		        	'</span> &#9679;  <span class="sone-tooltip-container" title="'+Main.translate("applicationMsg", "activityWidgetMoreInfo__lbl")+'"><i class="fa fa-info-circle" aria-hidden="true" style="color: #e67e22; cursor: pointer" onclick="Review(\''+groupedObj[i][z].detail+'\')"></i></span> <span class="sone-tooltip-container" title="'+Main.translate("applicationMsg", "activityWidgetOpenForReview__lbl")+'">'+openBtn+
		        	'</span></div>'+
		        	'</div>';
				}
				
				panelItems.push(theHtml);					
			}	
			panelBody.push({
				title: periodGroupParamValue !== "Period" ? groupedObj[i][0].group : groupedObj[i][0].periodGroup,
				collapsed: false,
				html: panelItems.join('\n')
			});
		}
		return panelBody;
	},
	
	_renderWidgetData_ : function(item,rpcResponse) {
		var ctx = this;
		var obj = item.widgetCfg;
		item._widgetParams_ = this._getAssignedWidgetParameters_(item.dashboardWidgetId);
		if (item.code == "ELC") {
			var widgetParams = item._widgetParams_, format, responseValue;
			if (widgetParams && widgetParams.length > 0) {
				Ext.each(widgetParams, function(wp) {
					if (wp.name === "RESPONSE FORMAT") {
						format = wp.value;
					}
				},this);
			}
			
			if (format === "JSON") {
				var highLightFn = function(json) {
				    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
				    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
				        var cls = 'number';
			            var newLine = "";
				        if (/^"/.test(match)) {
				            if (/:$/.test(match)) {
				                cls = 'key';
				                newLine = "<br>";
				            } else {
				                cls = 'string';
				            }
				        } else if (/true|false/.test(match)) {
				            cls = 'boolean';
				        } else if (/null/.test(match)) {
				            cls = 'null';
				        }
				        return newLine+'<span class="' + cls + '">' + match + '</span>';
				    });
				}
				if (typeof rpcResponse !== "string") {
					rpcResponse = JSON.stringify(rpcResponse);
				}
				responseValue = "<pre>"+highLightFn(rpcResponse)+"</pre>";
			}
			else {
				responseValue = rpcResponse;
			}
			obj.title = '<div style="font-weight:600; height:35px; line-height:35px">'+item.name+'</div>';
			obj.html = '<div style="width:100%; height:100%; overflow:auto">'+responseValue+'</div>';
		}		
		if (item.code == "UPV") {
			obj.title = '<div style="font-weight:600">'+item.name+'</div><div style="text-transform:none">'+rpcResponse['PERIOD']+'</div>';
			obj.html = '<div style="display:inline-block; position:relative; top:50%; transform: translateY(-50%); text-align:center"><span style="font-size:8vh; font-weight: 400">'+rpcResponse['value'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, Main.DECIMAL_SEP)+'</span><span style="font-size:3vh; text-transform:uppercase; padding-left:3px">'+rpcResponse['UNIT OF MEASURE']+'</span></div>'
		}
		if (item.code == "BFT") {
			
			var openBft = function() {
				var bundle = "atraxo.mod.ops";
				var frame = "atraxo.ops.ui.extjs.frame.FuelTicket_Ui";
				getApplication().showFrame(frame,{
					url:Main.buildUiPath(bundle, frame, false),
					callback: function () {
						this._showStackedViewElement_("TabbedPanel", "Buffered");
					}
				});
			}
			var bftValueId = Ext.id();
			
			obj.title = '<div style="font-weight:600; height:35px; line-height:35px">'+item.name+'</div>';
			obj.html = '<div style="font-size:10vh; font-weight: 400; cursor: pointer; display:inline-block; position:relative; top:50%; transform: translateY(-50%); text-align:center" id="'+bftValueId+'">'+rpcResponse['value']+'</div>';
			obj.listeners = {
				afterrender : {
					scope: this,
					fn: function(c) {
						document.getElementById(bftValueId).addEventListener ("click", openBft, false);
					}
				}
			}
		}
		if (item.code == "IMG") {
			obj.title = '<div style="font-weight:600">Custom Image</div>';
			obj.html = '<div style="width:100%; height:100%"><img style="width:100%; height:auto" src="' + rpcResponse['src'] +' "></div>'
		}
		if (item.code == "UAC") {
			var count = rpcResponse.length;				
			Review = function(description) {
				var id = Ext.id();
				return Ext.create("Ext.window.Window", {
					renderTo: Ext.getCmp("dnet-application-view-home").getEl(),
					title: Main.translate("applicationMsg", "activityInformationWidget__lbl"),
					modal: true,
					width: 500,
					height: 300,
					autoScroll: true,
					id: id,
					html: "<div style='padding:0px 10px 10px 10px'>"+description+"</div>",
					dockedItems: [{
			            xtype: 'toolbar',
			            flex: 1,
			            dock: 'bottom',
			            ui: 'footer',
			            items: [{
		                    xtype: 'button',
		                    text: 'OK',
		                    glyph: 'xf00c@FontAwesome',
		                    handler: function() {
		                    	Ext.getCmp(id).close();
		                    }
		                }]
			        }]
				}).show();
			}
			
			Open = function(m,f,id,group) {
				getApplication().showFrame(f, {
		            url: Main.buildUiPath(m, f, false),
		            callback: function() {
		                this._when_called_from_dshboard_(id,group);
		            }
		        });
			}
			
			var rpcFn = function(note,id,taskId,accept,windowId) {
				Ext.Ajax.request({
		        	url: Main.dsAPI("fmbas_WorkflowTask_Ds", "json").service,
		        	method: "POST",
		        	scope: this,
		        	params : {
		        		rpcName: "completeTask",
		        		rpcType: "data",
		        		data: Ext.JSON.encode({
		        			workflowTaskNote: note,
		        			workflowInstanceId: id,
		        			taskId: taskId,
		        			workflowTaskAcceptance: accept 
		        		})
					},
		        	success: function() {
                    	Ext.getCmp(windowId).close();
				    	ctx._refreshActivities_();
					},
					failure: function(response) {
                    	Ext.getCmp(windowId).close();
                    	//var r = Ext.getResponseDataInText(response);
                    	//var msg = r.split("|||")[1].split("||")[2];
                    	var msg = Ext.getResponseErrorText(response);
                    	Main.warning("The action cannot be completed because of the following reason:<br>"+msg);
                    	ctx._refreshActivities_();
					}
				});
			}
			
			
			Complete = function(workflowInstanceId,taskId) {
				var id = Ext.id();
				return Ext.create("Ext.window.Window", {
					renderTo: Ext.getCmp("dnet-application-view-home").getEl(),
					title: Main.translate("applicationMsg", "completeTaskWidget__lbl"),
					modal: true,
					width: 500,
					height: 300,
					autoScroll: true,
					id: id,
					items: [{
	                   xtype: 'form',
	                   bodyPadding: '0px 10px 0px 10px',
	            	   buttonAlign: "left",
	                   layout: {
	                       type: 'anchor'
	                   },
	                   items: [{
	                	   anchor: '100% 100%',
                           fieldLabel: Main.translate("applicationMsg", "widgetProvideNote__lbl"),
                           name: 'note',
                           allowBlank: false,
                           xtype: "textarea",
                           labelAlign: "top",
                           height: 210
                       }],
		               buttons: [{
		                    style:'margin-left:5px',
							xtype: 'button',
							text: Main.translate("applicationMsg", "widgetApproveChanges__lbl"),
							glyph: 'xf00c@FontAwesome',
							formBind: true,
							handler: function(btn) {
								var form = btn.up('form').getForm();
								var note = form.getValues().note;
								rpcFn(note,workflowInstanceId,taskId,true,id);
							}
		               },{
		                    xtype: 'button',
		                    text: Main.translate("applicationMsg", "widgetRejectChanges__lbl"),
		                    glyph: 'xf05e@FontAwesome',
							formBind: true,
		                    handler: function(btn) {
								var form = btn.up('form').getForm();
								var note = form.getValues().note;
								rpcFn(note,workflowInstanceId,taskId,false,id);
		                    }
		               }]
	                }]
				}).show();
			}

			obj.title = '<div style="font-weight:600; height:35px; line-height:35px; display: inline-block">'+item.name+'</div><div class="sone-user-activities-counter">'+count+'</div>';
			obj.layout = 'anchor';
			obj.listeners = {
		    	afterrender: {
		    		scope: this,
		    		fn: function(el) {
		    			var params = item._widgetParams_;
		    			var interval;
		    			Ext.each(params, function(param) {
		    				if (param.name === "AUTO REFRESH") {
		    					var value = param.value, defaultValue = param.defaultValue;
		    					if (!Ext.isEmpty(value)) {
		    						interval = value;
		    					}
		    					else {
		    						interval = defaultValue;
		    					}
		    				}
		    			}, this);
		    			interval = interval.split(" ")[0]*60*1000;
		    			setInterval(function(){
		    				ctx._refreshActivities_();		    				
		    			}, interval);
		    		}
		    	},
		    	boxready: {
		    		scope: this,
		    		fn: function(el) {
		    			this._activityWidgets_.push(el);
		    		}
		    	}
		    };
			obj.items = [{
				xtype: "container",
				overflowY: false,
				items: [{
					xtype: "container",
					style: "height:200px !important",
			        cls: "sone-user-activities",
					layout: {
				        type: "accordion",
				        hideCollapseTool : true,
				        titleCollapse: true,
				        animate: true,
				        activeOnTop: false,
				        multi: true
				    },
				    items: this._listActivities_(rpcResponse,item)
				}]
			}];
		}
		return obj;
		
	},
	
	_dropEndFn_ : function(e) {
		var el = Ext.getCmp("dnet-application-view-home");
		var items = el.items.items;
		var ctx = this;
		var payload = [];
		Ext.each(items, function(item) {
			if (item.xtype === "dashboard") {
				var columns = item.items.items;
				if (columns.length > 0) {
					var columnItems = columns[0].items.items;
					Ext.each(columnItems, function(portlet, index) {
						var portletCfg = portlet._partConfig;
						payload.push({
							version : portletCfg._widgetVersion_,
							id: portletCfg._dashboardWidgetDbId_,
							layoutId: portletCfg._layoutId_,
							widgetId: portletCfg._widgetId_,
							widgetIndex: index,
							layoutRegionId: portlet.up("dashboard")._regionId_,
							dashboardId: portletCfg._dashboardId_
						});
					}, this);
				}
			}
		}, this);
		this._updateDashboardWidgets_(payload);
		this._activityWidgets_ = [];
	},
	
	_updateDashboardWidgets_ : function(payload) {
		var dsName = "fmbas_DashboardWidget_Ds";
		Ext.Ajax.request({
            url: Main.dsAPI(dsName, "json").update,
            method: "POST",
            params: {
                data: Ext.JSON.encode(payload)
            },
            success: function() {
            	this._buildDashboard_();
            },
            failure: function(response) {
    	        Main.error(Ext.getResponseErrorText(response));   
            },
            scope: this
        });
		
	},
	
	_onDraggingEnd_ : function() {
		var el = Ext.getCmp("dnet-application-view-home");
		var items = el.items.items;
		if (!Ext.isEmpty(items)) {
			Ext.each(items, function(item) {
				if (item.xtype === "dashboard") {
					item.removeCls("sone-dragging-dashboard");
				}
			},this);
		}
	},
	
	_attachDropEvent_ : function() {
		var el = Ext.getCmp("dnet-application-view-home");
		var items = el.items.items;
		var ctx = this;
		if (!Ext.isEmpty(items)) {
			Ext.each(items, function(item) {
				if (item.xtype === "dashboard") {
					item.on("drop", function(e) {
						ctx._dropEndFn_(e);
					});
					item.el.on("mouseup", function() {
						ctx._onDraggingEnd_();
					});
				}
			},this);
		}
	},
	
	_getPortlets_ : function(container) {
		var portlets = [];
		var layoutColumn = null;
		var containerItems = container.items.items;	
		var widgetIndex = 0;
		if (containerItems.length > 0) {
			Ext.each(containerItems, function(item, index) {
				if (item.initialCls === "x-dashboard-column") {
					layoutColumn = item;					
				}
			},this);
		}			
		if (!Ext.isEmpty(layoutColumn)) {
			var layoutItems = layoutColumn.items.items;
			Ext.each(layoutItems, function(item, index) {
				if (item.initialCls === "x-dashboard-panel") {
					portlets.push(item);
				}
			},this)
		}
		return portlets;
	},
	
	_dragStartFn_ : function(portlet) {
		var form = portlet.down("form");		
		var el = Ext.getCmp("dnet-application-view-home");
		var items = el.items.items;
		
		if (!Ext.isEmpty(items)) {
			Ext.each(items, function(item) {
				if (item.xtype === "dashboard") {
					item.addCls("sone-dragging-dashboard");
				}
			},this);
		}
		
		if (!Ext.isEmpty(form)) {
			form.destroy();
		}
	},
	
	_attachDragEvent_ : function(portlet) {
		var ctx = this;
		portlet.el.on("drag", function(e) {
			ctx._dragStartFn_(portlet);
		});
	},
	
	_injectWidget_ : function(items) {
		
		var rpcData;		
		var ctx = this;
		var body = Ext.getCmp("dnet-application-view-home");
		var ready = 0;
		body.mask("Populating dashboard...");

		var afterAddFn = function() {
			var regions = body.items.items;
			var cols = [];
			Ext.each(regions, function(region) {
				var col = region.items.items[0];
				if (!Ext.isEmpty(col)) {
					cols.push(col);
				}				
			},this);
			if (cols.length > 0) {
				Ext.each(cols, function(c) {
					var portletArray = c.items.items;				
					Ext.each(portletArray, function(p) {
						ctx._attachDragEvent_(p);
					},this);
				},this);
			}
			
		}
		
		Ext.each(items, function(item, index) {
			var container = Ext.ComponentQuery.query('[_regionId_="'+item.layoutRegionId+'"]')[0];    		
    		var mainContainer = Ext.getCmp("dnet-application-view-home");  
    		var obj = item.widgetCfg;
    		var widgetIndex = parseInt(item.widgetIndex);
    		var rpcFn;
    		
    		var objFn = function(dashboardDbId) {    			
    			ctx._attachTools_(obj);    			
        		obj._containerId_ = container.id;
        		obj._dbId_ = item.id;
        		obj._dashboardWidgetDbId_ = dashboardDbId;
        		obj._widgetIndex_ = widgetIndex;
        		obj._widgetId_ = item.widgetId;
        		obj._layoutId_ = item.layoutId;
        		obj._widgetVersion_ = item.version;
        		obj._dashboardId_ = item.dashboardId;
        		container.addView(obj);
        		container._isBooked_ = true;
        		mainContainer._hasWidgets_ = true;
    		}
    		
    		rpcData = {
				id: item.dashboardWidgetId
			}
			
			// For each widget do the RPC and get the data
    		
    		// New logic because of the external content widget
    		
    		if (item.code !== "ELC") {
    			rpcFn = function() {
    				Ext.Ajax.request({
    		        	url: Main.dsAPI(item.ds, "json").service,
    		        	method: "POST",
    		        	scope: this,
    		        	params : {
    		        		rpcName: item.method,
    		        		rpcType: "data",
    		        		data: Ext.JSON.encode(rpcData)
    					},
    		        	success: function(response, opts) {
    		        		var responseText = Ext.getResponseDataInJSON(response);
    		        		var res = responseText.params;
    		        		
    		        		if (!Ext.isEmpty(res.rpcResponse)) {
    		        			
    		        			var rpcResponse = Ext.JSON.decode(res.rpcResponse);
    		        			// Specific object configuration for each widget
    			        		ctx._renderWidgetData_(item,rpcResponse);
    			        		objFn(item.dashboardWidgetId);
    		        		}
    		        		else {
    		        			obj.title = '<div style="font-weight:bold; padding:8px 0px">'+item.name+'</div>';
    		        			obj.html = '<div style="display:table; width:100%; height:100%"><div style="display: table-cell; text-align:center; vertical-align:middle"><div style="text-align:center; margin-bottom: 5px; color: #F1C40F"><i class="fa fa-3x fa-exclamation-triangle" aria-hidden="true"></i></div><div style="font-size:13px; padding-left:3px; text-align: center; color: #777777">Not enough data to display the widget!</div></div></div>'
    		        			objFn(item.dashboardWidgetId);
    		        		}
    		        		ready++;
    		        		if (ready == items.length) {
    		        			body.unmask();
    		        			afterAddFn();
    		        		}
    		        		
    					},
    					failure: function() {
    	        			obj.title = '<div style="font-weight:bold; padding:8px 0px">'+item.name+'</div>';
    	        			obj.html = '<div style="display:table; width:100%; height:100%"><div style="display: table-cell; text-align:center; vertical-align:middle"><div style="text-align:center; margin-bottom: 5px; color: #EB6B56"><i class="fa fa-3x fa-exclamation-triangle" aria-hidden="true"></i></div><div style="font-size:13px; padding-left:3px; text-align: center; color: #777777"><strong>Error:</strong> Cannot retrieve widget data.<br>There was e network error during the server request.<br></div></div></div>'
    	        			objFn(item.dashboardWidgetId);
    					}
    				});
    			}
    		}
    		else {
    			var widgetparams = this._getAssignedWidgetParameters_(item.dashboardWidgetId), url, requestHeader, requestBody, requestMethod, responseFormat;
    			rpcFn = function() {
    				if (!Ext.isEmpty(widgetparams) && widgetparams.length > 0) {    					
    					Ext.each(widgetparams, function(param) {
    						if (param.name === "METHOD") {
    							requestMethod = param.value;
    						} if (param.name === "URL") {
    							url = param.value;
    						} if (param.name === "HEADER") {
    							requestHeader = param.value;
    						} if (param.name === "BODY") {
    							requestBody = param.value;
    						} if (param.name === "RESPONSE FORMAT") {
    							responseFormat = param.value;
    						}    						
    					},this);
    	    			rpcData = {
    						id: item.dashboardWidgetId,    						
    						requestMethod: requestMethod,
    						url: url,
    						requestHeader: requestHeader,
    						requestBody: requestBody,
    						responseFormat: responseFormat
    					}
    					if (url && requestMethod && responseFormat) {
    						
    						var preFlightHeader = {};
    						var preFlightBody = {};
    						var headerParam = requestHeader.split("\n");
    						var bodyParam = requestBody.split("\n");
    						
    						Ext.each(headerParam,function(p) {    							
    							preFlightHeader[p.split(":")[0]] = p.split(":")[1];
    						},this);
    						
    						Ext.each(bodyParam,function(b) {    							
    							preFlightBody[b.split(":")[0]] = b.split(":")[1];
    						},this);
    						
    						Ext.Ajax.request({
    	    		        	url: url,
    	    		        	method: requestMethod,
    	    		        	scope: this,
    	    		        	headers: preFlightHeader,
    	    		        	params : preFlightBody,
    	    		        	success: function(response, opts) {
    	    		        		var isJsonString = function(str) {
    	    		        		    try {
    	    		        		        JSON.parse(str);
    	    		        		    } catch (e) {
    	    		        		        return false;
    	    		        		    }
    	    		        		    return true;
    	    		        		}, responseText;
//    	    		        		if (isJsonString(response.responseText) === true) {
//    	    		        			responseText = Ext.JSON.decode(response.responseText)
//    	    		        		}
//    	    		        		else {
//    	    		        			responseText = response.responseText;
//    	    		        		}
    	    		        		responseText = Ext.getResponseDataInJSON(response);
    	    		        		if (!Ext.isEmpty(responseText)) { 
    	    			        		ctx._renderWidgetData_(item,responseText);
    	    			        		objFn(item.dashboardWidgetId);
    	    		        		}
    	    		        		else {
    	    		        			obj.title = '<div style="font-weight:bold; padding:8px 0px">'+item.name+'</div>';
    	    		        			obj.html = '<div style="display:table; width:100%; height:100%"><div style="display: table-cell; text-align:center; vertical-align:middle"><div style="text-align:center; margin-bottom: 5px; color: #F1C40F"><i class="fa fa-3x fa-exclamation-triangle" aria-hidden="true"></i></div><div style="font-size:13px; padding-left:3px; text-align: center; color: #777777">Not enough data to display the widget!</div></div></div>'
    	    		        			objFn(item.dashboardWidgetId);
    	    		        		}
    	    		        		ready++;
    	    		        		if (ready == items.length) {
    	    		        			body.unmask();
    	    		        			afterAddFn();
    	    		        		}
    	    		        		
    	    					},
    	    					failure: function() {
    	    	        			obj.title = '<div style="font-weight:bold; padding:8px 0px">'+item.name+'</div>';
    	    	        			obj.html = '<div style="display:table; width:100%; height:100%"><div style="display: table-cell; text-align:center; vertical-align:middle"><div style="text-align:center; margin-bottom: 5px; color: #EB6B56"><i class="fa fa-3x fa-exclamation-triangle" aria-hidden="true"></i></div><div style="font-size:13px; padding-left:3px; text-align: center; color: #777777"><strong>Error:</strong> Cannot retrieve widget data.<br>There was e network error during the server request.<br></div></div></div>'
    	    	        			objFn(item.dashboardWidgetId);
    	    	        			ready++;
    	    		        		if (ready == items.length) {
    	    		        			body.unmask();
    	    		        		}
    	    					}
    	    				});
    					}
    				}
    			}
    		}

			rpcFn();
		}, this);
		
	},
	
	_getWidgets_ : function() {
		var items = [];
		var dashboardDs = "fmbas_DashboardWidget_Ds";
		
		Ext.Ajax.request({
        	url: Main.dsAPI(dashboardDs, "json").read,
        	method: "POST",
        	scope: this,
        	async: false,
        	params: {
            data: Ext.JSON.encode({
          	  		dashboardId: this._savedDashboardId_,
          	  		layoutId: this._savedLayoutId_
	        	})
	        },
        	success: function(response) {
        		var responseText = Ext.getResponseDataInJSON(response);
        		var res = responseText.data;        		
        		if (responseText.totalCount > 0) {
        			Ext.each(res, function(widget) {
        				items.push({
        					id: widget.id,
        					widgetId: widget.widgetId,
        					widgetName: widget.widgetName,
        					layoutId: widget.layoutId,
        					layoutRegionId : widget.layoutRegionId,
        					ds: widget.widgetDs,
        					method: widget.widgetMethod,
        					widgetIndex : widget.widgetIndex,
        					dashboardId : widget.dashboardId,
        					widgetVersion : widget.version
        				});
        			}, this);
        			this._injectWidget_(this._getWidgetCfg_(items));
        			this._attachDropEvent_();
        		}
			}
		});
	},	
	
	_addLayout_ : function(panelCfg, initiator) {
		var container = Ext.getCmp("dnet-application-view-home");
		var layout = panelCfg;
		var ctx = this;
		
		var addFn = function() {
			ctx._configWidgetPanel_(layout);
			container.removeAll();		
			container.add(layout);
			container._hasWidgets_ = false;
			if (initiator) {
				ctx._showSaveDashboardWdw_(initiator);
			}	
			else {
				ctx._getWidgets_();
			}
		}
		addFn();
//		if (container._hasWidgets_ == true) {
//			Ext.MessageBox.show({
//		        msg: "Changing the current layout will reset your current widgets. Are you sure you want to proceed?",
//		        buttons: Ext.MessageBox.OKCANCEL,
//				scope: this,
//		        fn: function(btn){
//		            if(btn == "ok"){
//		            	addFn();
//		            } else {
//		                return;
//		            }
//		        }
//		    }, this);
//		}
//		else {
//			addFn();
//		}
		
	}
	
//	_createItems_ : function() {
//		var items = [{
//			region : 'west',
//			id: this._westPanelId_,
//			frame : false,
//			margin: '8px 8px 8px 0px',
//			width: 405,
//			layout: {
//				type: 'vbox',
//				align: 'stretch'
//			},
//			items: [{
//				xtype: 'panel',
//				height: 160,
//				layout: {
//					type: 'hbox',
//					align: 'stretch'
//				},
//				items: [{
//					xtype: 'panel',
//					flex: 1,
//					cls: 'sone-chart-widget',
//					title: '<div style="font-weight:bold">Uplifted fuel</div><div style="text-transform:none">past 12 months</div>',
//					margin: '0px 4px 0px 0px',
//					layout: {
//						type: 'vbox',
//						align: 'center',
//						pack: 'center'
//					},
//					items: {
//						xtype: 'container',
//						html: '<span style="font-size:30px; font-weight: 600">1.230.450</span><span style="font-size:12px; text-transform:uppercase; padding-left:3px">ug</span>'
//					}
//				},{
//					xtype: 'panel',
//					flex: 1,
//					cls: 'sone-chart-widget',
//					title: '<div style="font-weight:bold">Sales contracts</div>',
//					margin: '0px 0px 0px 4px',
//					layout: {
//						type: 'vbox',
//						align: 'stretch'
//					},
//					items: {
//						xtype: 'panel',
//						flex: 1,
//						bodyStyle: 'padding: 25px 10px 5px 10px; box-zizing: border-box',
//						html: ['<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
//						       '<div style="font-size: 36px; line-height: 30px">76</div><div>Effective</div>',
//						       '</div></div>',
//						       '<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
//						       '<div style="display:table; width:100%;">',
//						       '<div style="display:table-cell; width: 50%; text-align:left; vertical-align:bottom">',
//						       '<div>',
//						       '<span style="font-weight:600; font-size: 16px">7</span><span style="padding-left:3px">draft</span>',
//						       '</div>',
//						       '<div>',
//						       '<span style="font-weight:600; font-size: 16px">15</span><span style="padding-left:3px">expired</span>',
//						       '</div>',
//						       '</div>',
//						       '<div style="display:table-cell; width: 50%; text-align:right; vertical-align:bottom" id="sparkLine1">test 2</div>',
//						       '</div>',
//						       '</div></div>',
//						].join('\n'),
//						listeners: {
//							afterrender: {
//								scope: this,
//								fn: function() {
//									$("#sparkLine1").sparkline([5,7,3,4,8,9,6,4], {
//									    type: 'bar',
//									    barWidth: 6,
//									    barSpacing: 2,
//									    height: 30,
//									    barColor: '#A0D468',
//									    negBarColor: '#A0D468'});
//								}
//							}
//						}
//					}
//				}]
//			},{
//				xtype: 'panel',
//				flex: 1,
//				margin: '8px 0px 0px 0px',
//				cls: 'sone-chart-widget',
//				title: '<div style="font-weight:bold">My activities</div>',
//				layout: {
//					type: 'vbox',
//					align: 'stretch'
//				},
//				items: this._buildGrid_()
//			}]
//		}, {
//			region : 'center',
//			id: this._centerPanelId_,
//			frame : false,	
//			margin: '8px 0px 8px 0px',
//			flex: 2,
//			layout: {
//				type: 'vbox',
//				align: 'stretch'
//			},
//			items: [{
//				xtype: 'panel',
//				flex: 1,
//				cls: 'sone-chart-widget',
//				title: '<div style="font-weight:bold">Upcoming payments</div><div style="text-transform:none">150.000 USD</div>',
//				layout: {
//					type: 'vbox',
//					align: 'stretch'
//				},
//				items: this._buildLineChart_()
//				
//			},{
//				xtype: 'panel',
//				flex: 1,
//				margin: '8px 0px 0px 0px',
//				cls: 'sone-chart-widget',
//				title: '<div style="font-weight:bold">Upcoming bills</div><div style="text-transform:none">90.000 USD</div>',
//				layout: {
//					type: 'vbox',
//					align: 'stretch'
//				},
//				items: this._buildSecondLineChart_()
//			}]
//		}, {
//			region : 'east',
//			id: this._eastPanelId_,
//			frame : false,
//			margin: '8px 0px 8px 8px',
//			width: 405,
//			layout: {
//				type: 'vbox',
//				align: 'stretch'
//			},
//			items: [{
//				xtype: 'panel',
//				height: 160,
//				layout: {
//					type: 'hbox',
//					align: 'stretch'
//				},
//				items: [{
//					xtype: 'panel',
//					flex: 1,
//					cls: 'sone-chart-widget',
//					//title: '<div style="font-weight:bold">Fuel quotes</div>',
//					title: ['<div style="display:table; width:100%; height: 100%">',
//					        '<div style="display: table-cell; width: 50%; text-transform: uppercase; font-weight: bold">Fuel quotes</div>',
//					        '<div style="display: table-cell; width: 50%; text-align: right">-2%<span style="padding-left:3px"><i class="fa fa-caret-down" style="color:#ED5564; font-size: 18px"></i></span></div>',
//					        '</div>'
//					].join('\n'),
//					margin: '0px 4px 0px 0px',
//					layout: {
//						type: 'vbox',
//						align: 'stretch'
//					},
//					items: {
//						xtype: 'panel',
//						flex: 1,
//						bodyStyle: 'padding: 25px 10px 5px 10px; box-zizing: border-box',
//						html: ['<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
//						       '<div style="font-size: 36px; line-height: 30px">89</div><div>Total</div>',
//						       '</div></div>',
//						       '<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
//						       '<div style="display:table; width:100%;">',
//						       '<div style="display:table-cell; width: 50%; text-align:left; vertical-align:bottom">',
//						       '<div>',
//						       '<span style="font-weight:600; font-size: 16px">25</span><span style="padding-left:3px">new</span>',
//						       '</div>',
//						       '<div>',
//						       '<span style="font-weight:600; font-size: 16px">20</span><span style="padding-left:3px">qualified</span>',
//						       '</div>',
//						       '</div>',
//						       '<div style="display:table-cell; width: 50%; text-align:right; vertical-align:bottom" id="sparkLine2"></div>',
//						       '</div>',
//						       '</div></div>',
//						].join('\n'),
//						listeners: {
//							afterrender: {
//								scope: this,
//								fn: function() {
//									$("#sparkLine2").sparkline([5,7,6,4,3,4,8,9], {
//									    type: 'bar',
//									    barWidth: 6,
//									    barSpacing: 2,
//									    height: 30,
//									    barColor: '#A0D468',
//									    negBarColor: '#A0D468'});
//								}
//							}
//						}
//					}
//				},{
//					xtype: 'panel',
//					flex: 1,
//					cls: 'sone-chart-widget',
//					title: ['<div style="display:table; width:100%; height: 100%">',
//					        '<div style="display: table-cell; width: 50%; text-transform: uppercase; font-weight: bold">Fuel orders</div>',
//					        '<div style="display: table-cell; width: 50%; text-align: right">7%<span style="padding-left:3px"><i class="fa fa-caret-up" style="color:#A0D468; font-size: 18px"></i></span></div>',
//					        '</div>'
//					].join('\n'),
//					margin: '0px 0px 0px 4px',
//					layout: {
//						type: 'vbox',
//						align: 'stretch'
//					},
//					items: {
//						xtype: 'panel',
//						flex: 1,
//						bodyStyle: 'padding: 25px 10px 5px 10px; box-zizing: border-box',
//						html: ['<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
//						       '<div style="font-size: 36px; line-height: 30px">132</div><div>Total</div>',
//						       '</div></div>',
//						       '<div style="display: table; height: 50%; width: 100%"><div style="display: table-cell; vertical-align:middle; text-align:right">',
//						       '<div style="display:table; width:100%;">',
//						       '<div style="display:table-cell; width: 50%; text-align:left; vertical-align:bottom">',
//						       '<div>',
//						       '<span style="font-weight:600; font-size: 16px">12</span><span style="padding-left:3px">confirmed</span>',
//						       '</div>',
//						       '<div>',
//						       '<span style="font-weight:600; font-size: 16px">120</span><span style="padding-left:3px">released</span>',
//						       '</div>',
//						       '</div>',
//						       '<div style="display:table-cell; width: 50%; text-align:right; vertical-align:bottom" id="sparkLine3"></div>',
//						       '</div>',
//						       '</div></div>',
//						].join('\n'),
//						listeners: {
//							afterrender: {
//								scope: this,
//								fn: function() {
//									$("#sparkLine3").sparkline([8,9,6,4,5,7,3,4], {
//									    type: 'bar',
//									    barWidth: 6,
//									    barSpacing: 2,
//									    height: 30,
//									    barColor: '#A0D468',
//									    negBarColor: '#A0D468'});
//								}
//							}
//						}
//					}
//				}]
//			},{
//				xtype: 'panel',
//				flex: 1,
//				margin: '8px 0px 0px 0px',
//				cls: 'sone-chart-widget',
//				title: '<div style="font-weight:bold">Total credit allocation</div><div style="text-transform:none">total allocated credit versus current utilization</div>',
//				layout: {
//					type: 'vbox',
//					align: 'center',
//					pack: 'center'
//				},
//				items: this._buildGaugeChart_()
//			},{
//				xtype: 'panel',
//				flex: 1,
//				margin: '8px 0px 0px 0px',
//				cls: 'sone-chart-widget',
//				title: '<div style="font-weight:bold">Top 5 customer credit utilization</div>',
//				layout: {
//					type: 'vbox',
//					align: 'stretch'
//				},
//				items: this._buildColumnChart_()
//			}]
//		}];
//		return items;
//	}
});
