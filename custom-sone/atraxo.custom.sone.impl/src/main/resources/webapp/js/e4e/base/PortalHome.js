Ext.override(e4e.base.HomePanel, {

initComponent : function(config) {

		var cfg = {
			layout : "border",
			items : this._createItems_()
		}

		Ext.apply(this, cfg);
		this.callParent(arguments);
	},

	_createItems_ : function() {
		var items = [{
				region : "center",
				frame : true,
				layout: {
	                align: 'middle',
	                pack: 'center',
	                type: 'hbox'
	            },
	            items: [{
	            	html: '<div style="text-align: center; font-size: 14px; font-weight:300; line-height:20px; width: 100%"><i class="fa fa-4x fa-exclamation-circle" aria-hidden="true" style="margin-bottom:5px; color: #0087c0"></i><br>Welcome to <strong>FuelPlus portal.ONE</strong><br>Select an item to see the details!</div>'
	            }]
			}];
		return items;
	}

});
