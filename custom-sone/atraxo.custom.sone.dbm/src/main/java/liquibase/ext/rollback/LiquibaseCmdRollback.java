package liquibase.ext.rollback;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.ext.exception.LiquibaseUtilException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.util.StringUtils;

public class LiquibaseCmdRollback {

	private final static Logger LOG = LoggerFactory.getLogger(LiquibaseCmdRollback.class);
	private final static String SERVICE_CODE_PARAM = "serviceCode=SONE";
	private final static String SERVICE_INSTANCE_REFID_PARAM = "refid=";
	private final static String PARAM_NAME_PARAM = "paramName=";
	public final static String JDBC_URL_PARAM_NAME = "JDBC_URL";
	public final static String DB_USER_PARM_NAME = "DB_USER";
	public final static String DB_PASS_PARAM_NAME = "DB_PASS";

	private static Properties prop;

	public static void main(String[] args) throws IOException {
		if (args.length < 3) {
			String usageExample = "Application must have the following parameters:\n"
					+ "url <subscription-service-url> rollback <version_number>\nurl <subscription-service-url> update\n"
					+ "jdbc <connection_string> rollback <version_number>\njdbc <connection_string> update\n"
					+ "<connection_string> example: \"jdbc:mysql://localhost:3306/test?user=root&password=root\"";
			System.out.println(usageExample);
			System.exit(-1);
		}
		String cmd = args[2];
		String tag = null;
		String message;
		switch (cmd) {
		case "rollback":
			if (args.length < 3) {
				System.out.println("You must speciffy a version number to rollback to");
				System.exit(-1);
			}
			tag = args[2];
			message = "Rolling back to version: " + tag;
			System.out.println(message);
			LOG.info(message);
			break;
		case "update":
			message = "Updating to latest version";
			System.out.println(message);
			LOG.info(message);
			break;
		default:
			System.out.println("Invalid command");
			System.exit(-1);

		}
		prop = new Properties();
		InputStream input = null;
		try {
			input = LiquibaseCmdRollback.class.getClassLoader().getResourceAsStream("dbm.properties");
			prop.load(input);
		} catch (IOException e) {
			LOG.error("Could not read properties file");
			LOG.error(e.getMessage());
			System.exit(-1);
		}
		if (args[0].equals("url")) {
			Map<String, Map<String, String>> connParams = getJdbcParamsFromServer(args[1]);
			Liquibase liquibase = null;
			ExecutorService executor = Executors.newFixedThreadPool(8);
			for (String refId : connParams.keySet()) {
				Runnable thread = new LiquibaseThread(cmd, tag, refId, connParams.get(refId));
				executor.execute(thread);
			}
			executor.shutdown();
			while (!executor.isShutdown()) {
			}
		} else {
			Liquibase liquibase = null;
			try {
				String connectionString = args[1];
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				Connection conn = DriverManager.getConnection(connectionString);
				Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(conn));
				liquibase = new Liquibase("changelog/db-changelog.xml", new ClassLoaderResourceAccessor(), database);
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException | LiquibaseException e) {
				LOG.error("Could not connect to the database", e);
				LOG.error(e.getMessage());
				System.exit(-1);
			}
			LOG.info("Connection to database established...");

			switch (cmd) {
			case "rollback":
				LOG.info("Starting rollback ");
				try {
					liquibase.rollback(tag, "");
				} catch (LiquibaseException e) {
					LOG.error("Could not rollback ", e);
					LOG.error(e.getMessage());
					System.out.println("Could not rollback. See log for details");
					System.exit(-1);
				}
				LOG.info("Rollback successful. Current version: " + tag);
				System.out.println("Rollback successful. Current version: " + tag);
				break;
			case "update":
				LOG.info("Starting update ");
				try {
					liquibase.update("");
				} catch (LiquibaseException e) {
					LOG.error("Could not update...", e);
					LOG.error(e.getMessage());
					System.out.println("Could not update. See log for details");
					System.exit(-1);
				}
				LOG.info("Update successful ");
				System.out.println("Update successful");
				break;
			default:
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Map<String, Map<String, String>> getJdbcParamsFromServer(String paramSourceURL) throws IOException {
		Map<String, Map<String, String>> params = new HashMap<>();
		URL url = new URL(paramSourceURL + "/api/data/serviceInstances/search/findByServiceCode?" + SERVICE_CODE_PARAM);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		String encoded = Base64.getEncoder().encodeToString(("admin:0Y7BDjXTvZ6l").getBytes(StandardCharsets.UTF_8));
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestProperty("Authorization", "Basic " + encoded);
		if (conn.getResponseCode() != 200) {
			LOG.error("Error on reading parameters from server: " + conn.getResponseCode());
			throw new RuntimeException();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		Map<String, Map> responseMap = new ObjectMapper().readValue(br, HashMap.class);
		List<Map<String, String>> serviceInstancesMap = (List<Map<String, String>>) responseMap.get("_embedded").get("serviceInstances");
		conn.disconnect();
		String errorString = "";
		for (Map<String, String> map : serviceInstancesMap) {
			Map<String, String> serviceInstanceParamsMap = new HashMap<>();
			String refId = map.get("refid");
			try {
				// getting jdbc url
				serviceInstanceParamsMap.put(JDBC_URL_PARAM_NAME, getJdbcParamsFromService(paramSourceURL, refId, JDBC_URL_PARAM_NAME));
				// getting db user
				serviceInstanceParamsMap.put(DB_USER_PARM_NAME, getJdbcParamsFromService(paramSourceURL, refId, DB_USER_PARM_NAME));
				// getting db pass
				serviceInstanceParamsMap.put(DB_PASS_PARAM_NAME, getJdbcParamsFromService(paramSourceURL, refId, DB_PASS_PARAM_NAME));
			} catch (LiquibaseUtilException e) {
				errorString.concat("Missing parameters for instance with refid: " + refId + "\n");
				continue;
			}
			params.put(refId, serviceInstanceParamsMap);
		}
		if (!StringUtils.isEmpty(errorString)) {
			LOG.warn(errorString);
		}
		return params;
	}

	@SuppressWarnings("unchecked")
	private static String getJdbcParamsFromService(String paramSourceURL, String refId, String paramName) throws IOException, LiquibaseUtilException {
		URL url = new URL(paramSourceURL + "/api/data/serviceInstanceParams/search/findByServiceParamParamNameAndServiceInstanceRefid?"
				+ PARAM_NAME_PARAM + paramName + "&" + SERVICE_INSTANCE_REFID_PARAM + refId);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		String user = prop.getProperty("proxy.service.username");
		String pass = prop.getProperty("proxy.service.password");
		String encoded = Base64.getEncoder().encodeToString((user + ":" + pass).getBytes(StandardCharsets.UTF_8));
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestProperty("Authorization", "Basic " + encoded);
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		Map<String, Map> responseMap = new ObjectMapper().readValue(br, HashMap.class);
		List<Map<String, String>> paramList = (List<Map<String, String>>) responseMap.get("_embedded").get("serviceInstanceParams");
		conn.disconnect();
		if (StringUtils.isEmpty(paramList.get(0).get("value"))) {
			throw new LiquibaseUtilException();
		}
		return paramList.get(0).get("value");

	}

}
