package liquibase.ext.rollback;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

public class LiquibaseThread implements Runnable {

	private final static Logger LOG = LoggerFactory.getLogger(LiquibaseThread.class);

	private String cmd;
	private String tag;
	private String refId;
	private Map<String, String> connProps;

	public LiquibaseThread(String cmd, String tag, String refId, Map<String, String> connProps) {
		this.cmd = cmd;
		this.tag = tag;
		this.refId = refId;
		this.connProps = connProps;
	}

	@Override
	public void run() {
		Liquibase liquibase = null;
		try {
			String connectionString = this.connProps.get(LiquibaseCmdRollback.JDBC_URL_PARAM_NAME) + "?" + "user="
					+ this.connProps.get(LiquibaseCmdRollback.DB_USER_PARM_NAME) + "&" + "password="
					+ this.connProps.get(LiquibaseCmdRollback.DB_PASS_PARAM_NAME);
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection conn = DriverManager.getConnection(connectionString);
			Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(conn));
			liquibase = new Liquibase("changelog/db-changelog.xml", new ClassLoaderResourceAccessor(), database);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException | LiquibaseException e) {
			LOG.error("Could not connect to the database", e);
			LOG.error(e.getMessage());
			return;
		}
		LOG.info("Connection to database established...");

		switch (this.cmd) {
		case "rollback":
			LOG.info("Starting rollback on instance with refId: " + this.refId);
			try {
				liquibase.rollback(this.tag, "");
			} catch (LiquibaseException e) {
				LOG.error("Could not rollback on instance with refId: " + this.refId, e);
				LOG.error(e.getMessage());
				System.out.println("Could not rollback on instance with refId: " + this.refId + "See log for details");
				return;
			}
			LOG.info("Rollback successful  on instance with refId: " + this.refId + " Current version: " + this.tag);
			System.out.println("Rollback successful  on instance with refId: " + this.refId + " Current version: " + this.tag);
			break;
		case "update":
			LOG.info("Starting update on instance with refId: " + this.refId);
			try {
				liquibase.update("");
			} catch (LiquibaseException e) {
				LOG.error("Could not updateon instance with refId: " + this.refId, e);
				LOG.error(e.getMessage());
				System.out.println("Could not updateon instance with refId: " + this.refId + ". See log for details");
				return;
			}
			LOG.info("Update successful on instance with refId: " + this.refId);
			System.out.println("Update successful on instance with refId: " + this.refId);
			break;
		default:
		}

	}

}
