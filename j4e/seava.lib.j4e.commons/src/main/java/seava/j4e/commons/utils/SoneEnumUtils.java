/**
 *
 */
package seava.j4e.commons.utils;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zspeter
 */
public class SoneEnumUtils {

	private static final Logger LOG = LoggerFactory.getLogger(SoneEnumUtils.class);

	/**
	 * Set fields of type Enum that are null with _EMPTY_;
	 *
	 * @param obj
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void setEmptyValues(Object obj) {
		for (Field field : obj.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true);
				if (field.getType().isEnum() && field.get(obj) == null) {
					field.set(obj, Enum.valueOf((Class<Enum>) field.getType(), "_EMPTY_"));
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				LOG.warn("Unable to set _EMPTY_ value for field: " + field.getName(), e);
			}
		}
	}

}
