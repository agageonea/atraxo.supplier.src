package seava.j4e.commons.utils.event;

public class PropertyChangeEvent extends java.beans.PropertyChangeEvent {

	private static final long serialVersionUID = 5965729066485535329L;

	private transient Object entity;

	public PropertyChangeEvent(Object source, String propertyName, Object oldValue, Object newValue, Object entity) {
		super(source, propertyName, oldValue, newValue);

		this.entity = entity;
	}

	public Object getEntity() {
		return this.entity;
	}
}
