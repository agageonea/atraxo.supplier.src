package seava.j4e.commons.security;

import java.io.Serializable;
import java.util.Map;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.IUserCache;

/**
 * Cached System parameters.
 *
 * @author zspeter
 */
public class UserCache implements IUserCache, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6063200150848382260L;
	private final Map<String, String> paramMap;

	/**
	 * @param map
	 */
	public UserCache(Map<String, String> map) {
		this.paramMap = map;
	}

	/**
	 * @param code
	 * @param clazz
	 * @return Return <code>code</code> parameter value as type <code>clazz</code>.
	 * @throws BusinessException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getSystemParameterValueAs(String code, Class<T> clazz) throws BusinessException {

		String value = this.paramMap.get(code);
		if (clazz.isInstance(value)) {
			return (T) value;
		}
		throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Cannot convert system parameter value " + code + " to type " + clazz.getName() + ".");
	}


}
