package seava.j4e.commons.utils.listener;

import java.beans.PropertyChangeListener;

import org.springframework.util.CollectionUtils;

import seava.j4e.commons.utils.listener.changes.Changes;

public abstract class ChangeListener implements PropertyChangeListener {

	protected Changes changes = new Changes();

	public Changes getEntityChanges() {
		return this.changes;
	}

	public void clearExistingChanges() {
		if (!CollectionUtils.isEmpty(this.changes.getValueChanges())) {
			this.changes.getValueChanges().clear();
		}
		if (!CollectionUtils.isEmpty(this.changes.getObjectAddedList())) {
			this.changes.getObjectAddedList().clear();
		}
		if (!CollectionUtils.isEmpty(this.changes.getObjectRemovedList())) {
			this.changes.getObjectRemovedList().clear();
		}
	}
}
