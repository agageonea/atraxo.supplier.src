package seava.j4e.commons.utils.comparator;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.function.Function;

import javax.persistence.JoinColumn;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import seava.j4e.commons.utils.event.PropertyAddedEvent;
import seava.j4e.commons.utils.event.PropertyChangeEvent;
import seava.j4e.commons.utils.event.PropertyRemovedEvent;
import seava.j4e.commons.utils.listener.ChangeListener;
import seava.j4e.commons.utils.listener.changes.Changes;

/**
 * Generic deep comparator of two Entities
 *
 * @author abolindu
 */
public class Comparator {

	private static final String PERSISTENCE = "_persistence_";

	private PropertyChangeSupport changeSupport;
	private ComparisonSpecialCases comparisonSpecialCases;
	private IgnoredFields ignoredFields;

	public Comparator(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}

	/**
	 * Special comparison cases can be added as a Map where the Key is the name of the field and the Value the function which will be executed
	 *
	 * @param specialCases
	 */
	public void withComparisonSecialCases(Map<String, Function<Object, EventType>> specialCases) {
		this.comparisonSpecialCases = new ComparisonSpecialCases();
		this.comparisonSpecialCases.putAll(specialCases);
	}

	public void withIgnoredFields(Class<?>... fielNames) {
		this.ignoredFields = new IgnoredFields();
		this.ignoredFields.addAll(Arrays.asList(fielNames));
	}

	@SuppressWarnings("unchecked")
	public void compare(Object oldEntity, Object newEntity) throws ReflectiveOperationException {
		for (Field field : oldEntity.getClass().getDeclaredFields()) {

			field.setAccessible(true);
			if (this.canCompare(field) && field.get(newEntity) != null) {

				Object fieldValue = this.getFieldValue(oldEntity, field);
				if (field.getType().equals(Collection.class)) {

					List<Object> originalEntityList = (List<Object>) field.get(oldEntity);
					List<Object> receivedEntityList = (List<Object>) field.get(newEntity);
					this.compareLists(originalEntityList, receivedEntityList, oldEntity);

				} else if (!field.get(newEntity).equals(fieldValue)) {

					this.compareFields(field.getName(), fieldValue, field.get(newEntity), oldEntity);

				}
			}
		}
	}

	private void compareFields(String fieldName, Object oldValue, Object newValue, Object sourceEntity) {
		if (!CollectionUtils.isEmpty(this.comparisonSpecialCases) && this.comparisonSpecialCases.containsKey(fieldName)) {
			EventType eventType = this.treatSpecialCases(fieldName, newValue);
			this.fireEvent(eventType, fieldName, oldValue, newValue, null, sourceEntity);
		} else {
			if (oldValue.getClass().equals(BigDecimal.class) && ((BigDecimal) newValue).compareTo((BigDecimal) oldValue) == 0) {
				return;
			}
			this.fireEvent(EventType.PROPERTY_CHANGED, fieldName, oldValue, newValue, null, sourceEntity);

		}
	}

	private void compareLists(List<Object> originalEntityList, List<Object> receivedEntityList, Object source) throws ReflectiveOperationException {
		for (Object originalEntity : originalEntityList) {
			boolean isEqual = false;
			ListIterator<Object> iterator = receivedEntityList.listIterator();
			while (iterator.hasNext()) {
				Object receivedEntity = iterator.next();
				if (originalEntity.equals(receivedEntity)) {
					this.compare(originalEntity, receivedEntity);
					iterator.remove();
					isEqual = true;
					break;
				}
			}

			if (!isEqual) {
				this.fireEvent(EventType.OBJECT_REMOVED, null, null, null, originalEntity, source);
			}
		}

		if (!receivedEntityList.isEmpty()) {
			for (Object receivedEntity : receivedEntityList) {
				this.fireEvent(EventType.OBJECT_ADDED, null, null, null, receivedEntity, source);
			}
		}
	}

	private EventType treatSpecialCases(String fieldName, Object fieldNewValue) {
		EventType eventType = EventType.DO_NOTHING;
		Function<Object, EventType> function = this.comparisonSpecialCases.get(fieldName);
		if (function != null) {
			eventType = function.apply(fieldNewValue);
		}
		return eventType;
	}

	private boolean canCompare(Field field) {
		boolean canCompare = true;
		if (field.getName().contains(PERSISTENCE) || this.ignoredFields.contains(field.getClass())) {
			canCompare = false;
		}
		return canCompare;
	}

	private Object getFieldValue(Object originalEntity, Field field) throws ReflectiveOperationException {
		Object fieldValue;
		if (field.isAnnotationPresent(JoinColumn.class)) {
			fieldValue = this.getValue(field, originalEntity);
		} else {
			fieldValue = field.get(originalEntity);
		}
		return fieldValue;
	}

	private Object getValue(Field field, Object entitySource) throws ReflectiveOperationException {
		String methodName = new StringBuilder("get").append(StringUtils.capitalize(field.getName())).toString();
		Method method = entitySource.getClass().getDeclaredMethod(methodName);
		return method.invoke(entitySource);
	}

	private void fireEvent(EventType eventType, String fieldName, Object oldValue, Object newValue, Object affectedEntity, Object sourceEntity) {
		java.beans.PropertyChangeEvent event = null;

		switch (eventType) {
		case PROPERTY_CHANGED:
			event = new PropertyChangeEvent(this, fieldName, oldValue, newValue, sourceEntity);
			break;
		case OBJECT_ADDED:
			event = new PropertyAddedEvent(this, affectedEntity, sourceEntity);
			break;
		case OBJECT_REMOVED:
			event = new PropertyRemovedEvent(this, affectedEntity, sourceEntity);
			break;
		default:
			break;
		}

		if (event != null) {
			this.changeSupport.firePropertyChange(event);
		}
	}

	public void addListener(PropertyChangeListener... listeners) {
		for (PropertyChangeListener listener : Arrays.asList(listeners)) {
			this.changeSupport.addPropertyChangeListener(listener);
		}
	}

	public Changes getChanges() {
		Changes changes = new Changes();
		List<PropertyChangeListener> listenerList = Arrays.asList(this.changeSupport.getPropertyChangeListeners());
		for (PropertyChangeListener listener : listenerList) {
			if (listener instanceof ChangeListener) {
				Changes clone = new Changes(((ChangeListener) listener).getEntityChanges());
				changes.addToChanges(clone);
				((ChangeListener) listener).clearExistingChanges();
			}
		}
		return changes;
	}

	public boolean hasChanges() {
		boolean hasChanges = true;
		Changes changes = new Changes();
		List<PropertyChangeListener> listenerList = Arrays.asList(this.changeSupport.getPropertyChangeListeners());
		for (PropertyChangeListener listener : listenerList) {
			if (listener instanceof ChangeListener) {
				changes.addToChanges(((ChangeListener) listener).getEntityChanges());
			}
		}
		if (CollectionUtils.isEmpty(changes.getObjectAddedList()) && CollectionUtils.isEmpty(changes.getObjectRemovedList())
				&& CollectionUtils.isEmpty(changes.getValueChanges())) {
			hasChanges = false;
		}
		return hasChanges;
	}
}
