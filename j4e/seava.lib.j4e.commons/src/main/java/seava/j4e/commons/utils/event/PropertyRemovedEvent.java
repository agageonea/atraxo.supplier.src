package seava.j4e.commons.utils.event;

/**
 * @author abolindu
 */
public class PropertyRemovedEvent extends java.beans.PropertyChangeEvent {

	private static final long serialVersionUID = 1320520419874070482L;

	private transient Object removedEntity;
	private transient Object sourceEntity;

	public PropertyRemovedEvent(Object source, Object removedEntity, Object sourceEntity) {
		super(source, null, null, null);
		this.removedEntity = removedEntity;
		this.sourceEntity = sourceEntity;
	}

	public Object getRemovedEntity() {
		return this.removedEntity;
	}

	public Object getSourceEntity() {
		return this.sourceEntity;
	}
}