package seava.j4e.commons.utils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import seava.j4e.api.Constants;
import seava.j4e.api.session.Session;

/**
 * Format in the entity the BigDecimal's decimal numbers.
 *
 * @author apetho
 */
public class BigDecimalFormater {

	/**
	 * @param entity
	 * @param decimal
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> void format(T entity, int decimal) throws IllegalArgumentException, IllegalAccessException {
		for (Field field : entity.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			if (java.lang.reflect.Modifier.isStatic(field.getModifiers()) || field.getType().equals(entity.getClass())) {
				continue;
			}
			if (field.getType().equals(List.class)) {
				List list = (List) field.get(entity);
				if (list == null) {
					continue;
				}
				for (int i = 0; i < list.size(); ++i) {
					format(list.get(i), decimal);
				}
			} else if (field.getType().equals(BigDecimal.class)) {
				Object o = field.get(entity);
				if (o == null) {
					continue;
				}
				BigDecimal formatedValue = ((BigDecimal) o).setScale(decimal, BigDecimal.ROUND_HALF_UP);
				field.set(entity, formatedValue);
			} else {
				if (!field.getType().isPrimitive()) {
					Object o = field.get(entity);
					if (o == null) {
						continue;
					}
					format(o, decimal);
				}
			}
		}
	}

	public static String dynamicNumberFormat(BigDecimal value, int decimalNumber) {
		if (value == null) {
			return "";
		}

		StringBuilder decimals = new StringBuilder(decimalNumber > 0 ? "." : "");
		for (int i = 0; i < decimalNumber; i++) {
			decimals.append("0");
		}

		String pattern = String.format(Constants.DYNAMIC_NUMBER_FORMAT, decimals);

		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
		otherSymbols.setDecimalSeparator(Session.user.get().getSettings().getDecimalSeparator().charAt(0));
		otherSymbols.setGroupingSeparator(Session.user.get().getSettings().getThousandSeparator().charAt(0));
		DecimalFormat df = new DecimalFormat(pattern, otherSymbols);
		return df.format(value);
	}

}
