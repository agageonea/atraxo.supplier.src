/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.commons.security;

import java.io.Serializable;

import seava.j4e.api.session.IClient;

public class AppClient implements IClient, Serializable {

	private static final long serialVersionUID = -9131543374115237340L;
	private final String id;
	private final String code;
	private final String name;
	private String subsidiaryId;

	public AppClient(String id, String code, String name) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.subsidiaryId = null;
	}

	public void setActiveSubsidiaryId(String subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getActiveSubsidiaryId() {
		return this.subsidiaryId;
	}


}
