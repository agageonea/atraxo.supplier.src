package seava.j4e.commons.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author vhojda
 */
public class XMLUtils {
	private final static Logger LOG = Logger.getLogger("XMLUtils");

	private static final String DEFAULT_JAXB_ENCODING = "UTF-8";// $NON-NLS-N$

	private static final String NULL_XML_NODE_EXCEPTION_MESSAGE = "Node is null, could not transform a null node to XML !";// $NON-NLS-N$

	private XMLUtils() {
	}

	/**
	 * @param node
	 * @param omitXmlDeclaration
	 * @param prettyPrint
	 * @return
	 */
	public static String toString(Node node, boolean omitXmlDeclaration, boolean prettyPrint) throws IllegalArgumentException {
		if (node == null) {
			throw new IllegalArgumentException(NULL_XML_NODE_EXCEPTION_MESSAGE);
		}

		try {
			// Remove unwanted whitespaces
			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = xpath.compile("//text()[normalize-space()='']");
			NodeList nodeList = (NodeList) expr.evaluate(node, XPathConstants.NODESET);

			for (int i = 0; i < nodeList.getLength(); ++i) {
				org.w3c.dom.Node nd = nodeList.item(i);
				nd.getParentNode().removeChild(nd);
			}

			// Create and setup transformer
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

			if (omitXmlDeclaration) {
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			}

			if (prettyPrint) {
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			}

			// Turn the node into a string
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(node), new StreamResult(writer));
			return writer.toString();
		} catch (TransformerException | XPathExpressionException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @param xmlElement
	 * @param aClass
	 * @return
	 * @throws JAXBException
	 * @throws XMLStreamException
	 * @throws FactoryConfigurationError
	 */
	public static Object toObject(Element xmlElement, Class<?> aClass) throws JAXBException, XMLStreamException, FactoryConfigurationError {
		String msgXml = toString(xmlElement, true, false);
		return toObject(msgXml, aClass, false);
	}

	/**
	 * @param xml
	 * @param aClass
	 * @return
	 * @throws JAXBException
	 * @throws XMLStreamException
	 * @throws FactoryConfigurationError
	 */
	public static Object toObject(String xml, Class<?> aClass, boolean withNameSpace)
			throws JAXBException, XMLStreamException, FactoryConfigurationError {
		InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
		XMLStreamReader xsr = XMLInputFactory.newFactory().createXMLStreamReader(is);
		JAXBContext jc = JAXBContext.newInstance(aClass);
		Unmarshaller um = jc.createUnmarshaller();
		if (withNameSpace) {
			return um.unmarshal(xsr);
		} else {
			XMLReaderWithoutNamespace xr = new XMLReaderWithoutNamespace(xsr);
			return um.unmarshal(xr);

		}
	}

	/**
	 * @param object
	 * @param objectClass
	 * @return
	 * @throws JAXBException
	 */
	public static Element toXML(Object object, Class<?> objectClass) throws JAXBException {

		try {
			BigDecimalFormater.format(object, 6);
		} catch (IllegalArgumentException | IllegalAccessException e1) {
			LOG.log(Level.WARNING, "Could not format object !", e1);
		}

		DOMResult resultedDOM = new DOMResult();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(objectClass);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, DEFAULT_JAXB_ENCODING);
			jaxbMarshaller.marshal(object, resultedDOM);
		} catch (JAXBException e) {
			throw e;
		}

		return ((Document) resultedDOM.getNode()).getDocumentElement();
	}

	/**
	 * @param object
	 * @param objectClass
	 * @param omitXmlDeclaration
	 * @param prettyPrint
	 * @return
	 * @throws JAXBException
	 */
	public static String toString(Object object, Class<?> objectClass, boolean omitXmlDeclaration, boolean prettyPrint) throws JAXBException {
		Element element = toXML(object, objectClass);
		return toString(element, omitXmlDeclaration, prettyPrint);
	}

	/**
	 * @param rootTag
	 * @return
	 */
	public static Element createRootElement(String rootTag) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		}
		Document doc = builder.newDocument();
		// create the root element node
		return doc.createElement(rootTag);
	}

}

class XMLReaderWithoutNamespace extends StreamReaderDelegate {
	public XMLReaderWithoutNamespace(XMLStreamReader reader) {
		super(reader);
	}

	@Override
	public String getAttributeNamespace(int arg0) {
		return "";
	}

	@Override
	public String getNamespaceURI() {
		return "";
	}
}
