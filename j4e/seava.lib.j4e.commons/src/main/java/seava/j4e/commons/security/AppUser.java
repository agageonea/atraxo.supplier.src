/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.commons.security;

import java.io.Serializable;

import seava.j4e.api.session.IClient;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IUserSettings;
import seava.j4e.api.session.IWorkspace;

public class AppUser implements IUser, Serializable {

	private static final long serialVersionUID = -9131543374115237340L;
	private final String code;
	private final String name;

	private final String loginName;
	private char[] password;

	private final String employeeId;
	private final String employeeCode;

	private final IClient client;
	private final IUserSettings settings;
	private final IUserProfile profile;
	private final IWorkspace workspace;

	private final boolean systemUser;

	public AppUser(String code, String name, String loginName, String password, String employeeId, String employeeCode, IClient client,
			IUserSettings settings, IUserProfile profile, IWorkspace workspace, boolean systemUser) {

		super();

		this.code = code;
		this.name = name;

		this.loginName = loginName;
		if (password != null) {
			this.password = password.toCharArray();
		}

		this.employeeId = employeeId;
		this.employeeCode = employeeCode;
		this.client = client;
		this.settings = settings;
		this.profile = profile;
		this.workspace = workspace;
		this.systemUser = systemUser;

	}

	@Override
	public String getClientId() {
		return this.client.getId();
	}

	@Override
	public String getClientCode() {
		return this.client.getCode();
	}

	@Override
	public String getEmployeeId() {
		return this.employeeId;
	}

	@Override
	public String getEmployeeCode() {
		return this.employeeCode;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getLoginName() {
		return this.loginName;
	}

	@Override
	public IClient getClient() {
		return this.client;
	}

	@Override
	public IUserSettings getSettings() {
		return this.settings;
	}

	@Override
	public IUserProfile getProfile() {
		return this.profile;
	}

	@Override
	public IWorkspace getWorkspace() {
		return this.workspace;
	}

	@Override
	public char[] getPassword() {
		return this.password;
	}

	@Override
	public void setPassword(char[] password) {
		this.password = password;
	}

	@Override
	public boolean isSystemUser() {
		return this.systemUser;
	}

}
