/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.commons;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import seava.j4e.api.Constants;
import seava.j4e.api.ISettings;
import seava.j4e.api.descriptor.ISysParamDefinition;
import seava.j4e.api.descriptor.ISysParamDefinitions;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.exceptions.InvalidDatabase;
import seava.j4e.api.service.ISysParamValueProvider;
import seava.j4e.api.session.Session;

public class Settings implements ISettings, ApplicationContextAware {

	private final static Logger LOG = LoggerFactory.getLogger(Settings.class);

	private ApplicationContext applicationContext;

	/**
	 * System properties defined in the application properties file.
	 */
	private final Map<String, String> properties;

	/**
	 * List of system parameter definitions. Each business module can declare and export system-parameter definitions. All of them are collected in
	 * this list.
	 */
	private List<ISysParamDefinitions> paramDefinitions;

	/**
	 * System parameters default values. Provided at startup
	 */
	private Map<String, String> paramDefaultValues;

	/**
	 * Client level values for system parameters.
	 */
	private Map<String, Map<String, String>> paramValues;

	/**
	 * Product-name.
	 */
	private String productName;

	/**
	 * Product-description.
	 */
	private String productDescription;

	/**
	 * Product-vendor
	 */
	private String productVendor;

	/**
	 * Product-url
	 */
	private String productUrl;

	/**
	 * Product-version
	 */
	private String productVersion;

	/**
	 * Report Center Url
	 */
	private String reportCenterUrl;

	/**
	 * Mail Merge Url
	 */
	private String mailMergeUrl;

	/**
	 * Web Dav Url
	 */
	private String webDavUrl;

	/**
	 * Web Dav Url
	 */
	private String webDavUser;

	/**
	 * Web Dav Url
	 */
	private String webDavPassword;

	/**
	 *
	 */
	private Boolean isNewClientApiEnabled;

	/**
	 *
	 */
	private String initFileLocation;

	public Settings(Map<String, String> properties) {

		this.properties = properties;

		Constants.set_server_date_format(this.properties.get(Constants.PROP_SERVER_DATE_FORMAT));
		Constants.set_server_time_format(this.properties.get(Constants.PROP_SERVER_TIME_FORMAT));
		Constants.set_server_datetime_format(this.properties.get(Constants.PROP_SERVER_DATETIME_FORMAT));
		Constants.set_server_alt_formats(this.properties.get(Constants.PROP_SERVER_ALT_FORMATS));
	}

	@Override
	public String get(String key) {
		return this.properties.get(key);
	}

	@Override
	public boolean getAsBoolean(String key) {
		return Boolean.valueOf(this.get(key));
	}

	@Override
	public String getParam(String paramName) throws InvalidConfiguration {
		this.checkParam(paramName);
		this.checkParamValue(paramName);
		if (Session.user.get() != null) {
			String clientId = Session.user.get().getClientId();

			if (clientId != null && !"".equals(clientId) && this.paramValues.get(clientId).containsKey(paramName)) {
				return this.paramValues.get(clientId).get(paramName);
			} else {
				return this.paramDefaultValues.get(paramName);
			}
		}
		return this.paramDefaultValues.get(paramName);
	}

	@Override
	public boolean getParamAsBoolean(String paramName) throws InvalidConfiguration {
		return Boolean.valueOf(this.getParam(paramName));
	}

	@Override
	public void reloadParams() throws Exception {
		this.populateParams();
	}

	@Override
	public void reloadParamValues() throws Exception {
		try {
			this.populateParamValues();
		} catch (InvalidConfiguration e) {
			throw new Exception(e);
		}
	}

	private void checkParam(String paramName) throws InvalidConfiguration {
		if (this.paramDefaultValues == null) {
			this.populateParams();
		}
		if (!this.paramDefaultValues.containsKey(paramName)) {
			// maybe it's a bundle plugged in after first populate ...
			// populate it once again
			this.populateParams();
			if (!this.paramDefaultValues.containsKey(paramName)) {
				throw new InvalidConfiguration("Parameter `" + paramName + "` has no value defined");
			}
		}
	}

	private void checkParamValue(String paramName) throws InvalidConfiguration {
		if (Session.user.get() != null) {
			String clientId = Session.user.get().getClientId();
			if (this.paramValues == null || (clientId != null && !"".equals(clientId) && this.paramValues.get(clientId) == null)) {
				this.populateParamValues();
			}
		}
	}

	private void populateParams() {
		this.paramDefaultValues = new HashMap<String, String>();
		for (ISysParamDefinitions pd : this.getParamDefinitions()) {
			for (ISysParamDefinition p : pd.getSysParamDefinitions()) {
				String name = p.getName();
				this.paramDefaultValues.put(name, p.getDefaultValue());
			}
		}
	}

	private void populateParamValues() throws InvalidConfiguration {
		if (this.paramValues == null) {
			this.paramValues = new HashMap<String, Map<String, String>>();
		}

		if (Session.user.get() != null && Session.user.get().getClientId() != null && !"".equals(Session.user.get().getClientId())) {
			this.paramValues.put(Session.user.get().getClientId(), new HashMap<String, String>());
			try {
				Map<String, String> values = this.applicationContext.getBean(ISysParamValueProvider.class).getParamValues(new Date());

				this.paramValues.put(Session.user.get().getClientId(), values);
			} catch (InvalidDatabase e) {
				// TODO: set a flag to force a setup
				LOG.error("Invalid database !", e);
			} catch (Exception e) {
				throw new InvalidConfiguration(e.getMessage(), e);
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<ISysParamDefinitions> getParamDefinitions() {
		if (this.paramDefinitions == null) {
			this.paramDefinitions = (List<ISysParamDefinitions>) this.applicationContext.getBean(Constants.SPRING_OSGI_SYSPARAM_DEFINITIONS);
			if (this.get(Constants.PROP_DEPLOYMENT).equals(Constants.PROP_DEPLOYMENT_JEE)) {
				// TODO: use a flag to check if already populated
				Map<String, ISysParamDefinitions> beans = this.applicationContext.getBeansOfType(ISysParamDefinitions.class);
				for (ISysParamDefinitions b : beans.values()) {
					this.paramDefinitions.add(b);
				}
			}
		}
		return this.paramDefinitions;
	}

	public void setParamDefinitions(List<ISysParamDefinitions> paramDefinitions) {
		this.paramDefinitions = paramDefinitions;
	}

	public Map<String, String> getParams() {
		return this.paramDefaultValues;
	}

	public void setParams(Map<String, String> params) {
		this.paramDefaultValues = params;
	}

	@Override
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String getProductDescription() {
		return this.productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	@Override
	public String getProductVendor() {
		return this.productVendor;
	}

	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}

	@Override
	public String getProductUrl() {
		return this.productUrl;
	}

	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

	@Override
	public String getProductVersion() {
		return this.productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	@Override
	public String getReportCenterUrl() {
		return this.reportCenterUrl;
	}

	public void setReportCenterUrl(String reportCenterUrl) {
		this.reportCenterUrl = reportCenterUrl;
	}

	@Override
	public String getMailMergeUrl() {
		return this.mailMergeUrl;
	}

	public void setMailMergeUrl(String mailMergeUrl) {
		this.mailMergeUrl = mailMergeUrl;
	}

	@Override
	public String getWebDavUrl() {
		return this.webDavUrl;
	}

	public void setWebDavUrl(String webDavUrl) {
		this.webDavUrl = webDavUrl;
	}

	@Override
	public String getWebDavUser() {
		return this.webDavUser;
	}

	public void setWebDavUser(String webDavUser) {
		this.webDavUser = webDavUser;
	}

	@Override
	public String getWebDavPassword() {
		return this.webDavPassword;
	}

	public void setWebDavPassword(String webDavPassword) {
		this.webDavPassword = webDavPassword;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	public void setIsNewClientApiEnabled(Boolean isNewClientApiEnabled) {
		this.isNewClientApiEnabled = isNewClientApiEnabled;
	}

	@Override
	public Boolean isNewClientApiEnabled() {
		return this.isNewClientApiEnabled;
	}

	public void setInitFileLocation(String initFileLocation) {
		this.initFileLocation = initFileLocation;
	}

	@Override
	public String getInitFileLocation() {
		return this.initFileLocation;
	}
}
