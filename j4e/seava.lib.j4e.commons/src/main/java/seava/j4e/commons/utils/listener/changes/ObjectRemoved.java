package seava.j4e.commons.utils.listener.changes;

public class ObjectRemoved {
	private Object removedEntity;
	private Object source;
	private String messageCode;

	public ObjectRemoved(Object removedEntity, Object source, String messageCode) {
		this.removedEntity = removedEntity;
		this.source = source;
		this.messageCode = messageCode;
	}

	public Object getRemovedEntity() {
		return this.removedEntity;
	}

	public Object getSource() {
		return this.source;
	}

	public String getMessageCode() {
		return this.messageCode;
	}
}
