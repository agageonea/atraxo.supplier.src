/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.commons.action.query;

import seava.j4e.api.action.query.ISortToken;

public class SortToken implements ISortToken {

	private String property;
	private String direction;

	public SortToken() {
	}

	public SortToken(String property) {
		this.property = property;
	}

	public SortToken(String property, String direction) {
		this.property = property;
		this.direction = direction;
	}

	@Override
	public String getProperty() {
		return this.property;
	}

	@Override
	public void setProperty(String property) {
		this.property = property;
	}

	@Override
	public String getDirection() {
		return this.direction;
	}

	@Override
	public void setDirection(String direction) {
		this.direction = direction;
	}

}
