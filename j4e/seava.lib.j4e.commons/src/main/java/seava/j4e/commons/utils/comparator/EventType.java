package seava.j4e.commons.utils.comparator;

public enum EventType {
	DO_NOTHING, OBJECT_ADDED, OBJECT_REMOVED, PROPERTY_CHANGED;
}
