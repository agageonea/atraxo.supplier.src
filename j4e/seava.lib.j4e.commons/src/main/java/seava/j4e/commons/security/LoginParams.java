/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.commons.security;

import seava.j4e.api.security.ILoginParams;

public class LoginParams implements ILoginParams {
	/**
	 * Client to connect to.
	 */
	private String clientCode;

	/**
	 * Language.
	 */
	private String language;

	private String userAgent;

	private String remoteHost;
	private String remoteIp;

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#getClientCode()
	 */
	@Override
	public String getClientCode() {
		return this.clientCode;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#setClientCode(java.lang.String)
	 */
	@Override
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#getLanguage()
	 */
	@Override
	public String getLanguage() {
		return this.language;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#setLanguage(java.lang.String)
	 */
	@Override
	public void setLanguage(String language) {
		this.language = language;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#getUserAgent()
	 */
	@Override
	public String getUserAgent() {
		return this.userAgent;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#setUserAgent(java.lang.String)
	 */
	@Override
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#getRemoteHost()
	 */
	@Override
	public String getRemoteHost() {
		return this.remoteHost;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#setRemoteHost(java.lang.String)
	 */
	@Override
	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#getRemoteIp()
	 */
	@Override
	public String getRemoteIp() {
		return this.remoteIp;
	}

	/*
	 * (non-Javadoc)
	 * @see seava.j4e.commons.security.ILoginParams#setRemoteIp(java.lang.String)
	 */
	@Override
	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
	}

}
