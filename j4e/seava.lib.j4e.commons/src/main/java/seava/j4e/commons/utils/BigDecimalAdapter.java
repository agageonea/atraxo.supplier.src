package seava.j4e.commons.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @author apetho
 */
public class BigDecimalAdapter extends XmlAdapter<String, BigDecimal> {

	private static final int DECIMAL_NUMBER = 6;

	@Override
	public BigDecimal unmarshal(String v) throws Exception {
		return new BigDecimal(v, MathContext.DECIMAL64);
	}

	@Override
	public String marshal(BigDecimal v) throws Exception {
		BigDecimal retVal = v.setScale(DECIMAL_NUMBER, BigDecimal.ROUND_HALF_UP);
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(DECIMAL_NUMBER);
		df.setMinimumFractionDigits(DECIMAL_NUMBER);
		df.setGroupingUsed(false);
		return df.format(retVal);
	}
}
