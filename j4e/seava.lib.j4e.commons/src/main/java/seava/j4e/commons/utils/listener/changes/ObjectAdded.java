package seava.j4e.commons.utils.listener.changes;

public class ObjectAdded {
	private Object addedEntity;
	private Object source;
	private String messageCode;

	public ObjectAdded(Object addedEntity, Object source, String messageCode) {
		this.addedEntity = addedEntity;
		this.source = source;
		this.messageCode = messageCode;
	}

	public Object getAddedEntity() {
		return this.addedEntity;
	}

	public Object getSource() {
		return this.source;
	}

	public String getMessageCode() {
		return this.messageCode;
	}
}
