package seava.j4e.commons.utils.event;

/**
 * @author abolindu
 */
public class PropertyAddedEvent extends java.beans.PropertyChangeEvent {

	private static final long serialVersionUID = 1320520419874070482L;

	private transient Object addedEntity;
	private transient Object sourceEntity;

	public PropertyAddedEvent(Object source, Object addedEntity, Object sourceEntity) {
		super(source, null, null, null);
		this.addedEntity = addedEntity;
		this.sourceEntity = sourceEntity;
	}

	public Object getAddedEntity() {
		return this.addedEntity;
	}

	public Object getSourceEntity() {
		return this.sourceEntity;
	}
}