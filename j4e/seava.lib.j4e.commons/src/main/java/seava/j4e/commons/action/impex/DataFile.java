package seava.j4e.commons.action.impex;

import seava.j4e.api.action.impex.IImportDataFile;

public class DataFile implements IImportDataFile {

	private String ds;
	private String file;

	private String ukFieldName;

	private String cUkFieldName;
	private String relFieldName;
	private String cDs;

	private ImportAction action;

	@Override
	public String getDs() {
		return this.ds;
	}

	@Override
	public void setDs(String ds) {
		this.ds = ds;
	}

	@Override
	public String getFile() {
		return this.file;
	}

	@Override
	public void setFile(String file) {
		this.file = file;
	}

	@Override
	public String getUkFieldName() {
		return this.ukFieldName;
	}

	@Override
	public void setUkFieldName(String ukFieldName) {
		this.ukFieldName = ukFieldName;
	}

	@Override
	public String getcUkFieldName() {
		return this.cUkFieldName;
	}

	@Override
	public void setcUkFieldName(String cUkFieldName) {
		this.cUkFieldName = cUkFieldName;
	}

	@Override
	public String getRelFieldName() {
		return this.relFieldName;
	}

	@Override
	public void setRelFieldName(String relFieldName) {
		this.relFieldName = relFieldName;
	}

	@Override
	public String getcDs() {
		return this.cDs;
	}

	@Override
	public void setcDs(String cDs) {
		this.cDs = cDs;
	}

	@Override
	public ImportAction getAction() {
		return this.action;
	}

	@Override
	public void setAction(ImportAction action) {
		this.action = action;
	}
}
