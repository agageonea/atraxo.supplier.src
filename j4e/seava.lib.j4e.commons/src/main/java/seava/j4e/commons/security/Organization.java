package seava.j4e.commons.security;

import java.io.Serializable;

import seava.j4e.api.session.IOrganization;

public class Organization implements IOrganization, Serializable {

	private static final long serialVersionUID = 1948188401887773972L;

	private final String code;
	private final String name;
	private boolean used;
	private final String id;

	public Organization(String code, String name, boolean used, String id) {
		super();
		this.code = code;
		this.name = name;
		this.used = used;
		this.id = id;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public boolean isUsed() {
		return this.used;
	}

	@Override
	public void setUsed(boolean arg) {
		this.used = arg;
	}

	@Override
	public String getId() {
		return this.id;
	}


}
