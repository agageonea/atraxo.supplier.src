package seava.j4e.commons.utils.listener.changes;

import java.util.ArrayList;
import java.util.List;

public class Changes {
	List<ValueChanged> valueChangedList;
	List<ObjectAdded> objectAddedList;
	List<ObjectRemoved> objectRemovedList;

	/**
	 * @param changes
	 */
	public Changes(Changes changes) {
		this.valueChangedList = changes.valueChangedList;
		this.objectAddedList = changes.objectAddedList;
		this.objectRemovedList = changes.objectRemovedList;
	}

	public Changes() {
	}

	public List<ValueChanged> getValueChanges() {
		return this.valueChangedList;
	}

	public void setValueChangedList(List<ValueChanged> valueChangedList) {
		this.valueChangedList = valueChangedList;
	}

	public void addToValueChanges(List<ValueChanged> valueChanged) {
		if (this.valueChangedList == null) {
			this.valueChangedList = new ArrayList<>();
		}
		this.valueChangedList.addAll(valueChanged);
	}

	public List<ObjectAdded> getObjectAddedList() {
		return this.objectAddedList;
	}

	public void setObjectAddedList(List<ObjectAdded> objectAddedList) {
		this.objectAddedList = objectAddedList;
	}

	public void addToObjectAddedList(List<ObjectAdded> objectAdded) {
		if (this.objectAddedList == null) {
			this.objectAddedList = new ArrayList<>();
		}
		this.objectAddedList.addAll(objectAdded);
	}

	public List<ObjectRemoved> getObjectRemovedList() {
		return this.objectRemovedList;
	}

	public void setObjectRemovedList(List<ObjectRemoved> objectRemovedList) {
		this.objectRemovedList = objectRemovedList;
	}

	public void addToObjectRemovedList(List<ObjectRemoved> objectRemoved) {
		if (this.objectRemovedList == null) {
			this.objectRemovedList = new ArrayList<>();
		}
		this.objectRemovedList.addAll(objectRemoved);
	}

	public void addToChanges(Changes changes) {
		if (changes.getValueChanges() != null) {
			this.addToValueChanges(changes.getValueChanges());
		}
		if (changes.getObjectAddedList() != null) {
			this.addToObjectAddedList(changes.getObjectAddedList());
		}
		if (changes.getObjectRemovedList() != null) {
			this.addToObjectRemovedList(changes.getObjectRemovedList());
		}
	}

}
