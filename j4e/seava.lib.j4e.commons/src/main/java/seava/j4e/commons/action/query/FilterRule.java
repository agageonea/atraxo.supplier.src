/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.commons.action.query;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import seava.j4e.api.Constants;
import seava.j4e.api.action.query.IFilterRule;

/**
 * Describes one filter rule.
 *
 * @author amathe
 */
public class FilterRule implements IFilterRule {

	private static final Logger LOG = Logger.getLogger("FilterRule");

	String fieldName;
	String operation;
	String value1;
	String value2;
	String groupOp;

	Class<?> dataType;
	Class<?> dataTypeConvertor;
	String dataTypeFQN;
	String dataTypeConvertorFQN;

	/**
	 * Default constructor
	 */
	public FilterRule() {
		super();
	}

	/**
	 * @param fieldName
	 * @param operation
	 * @param value1
	 * @param value2
	 * @param groupOp
	 */
	public FilterRule(String fieldName, String operation, String value1, String value2, String groupOp) {
		super();
		this.fieldName = fieldName;
		this.operation = operation;
		this.value1 = value1;
		this.value2 = value2;
		this.groupOp = groupOp;
	}

	@Override
	public String getFieldName() {
		return this.fieldName;
	}

	@Override
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	public String getOperation() {
		return this.operation;
	}

	@Override
	public void setOperation(String operation) {
		this.operation = operation;
	}

	@Override
	public String getValue1() {
		return this.value1;
	}

	@Override
	public void setValue1(String value1) {
		this.value1 = value1;
	}

	@Override
	public String getValue2() {
		return this.value2;
	}

	@Override
	public void setValue2(String value2) {
		this.value2 = value2;
	}

	@Override
	public String getGroupOp() {
		return (this.groupOp == null) ? "" : this.groupOp;
	}

	@Override
	public void setGroupOp(String groupOp) {
		this.groupOp = groupOp;
	}

	public String getDataTypeFQN() {
		return this.dataTypeFQN;
	}

	public void setDataTypeFQN(String dataTypeFQN) throws ClassNotFoundException {
		this.dataTypeFQN = dataTypeFQN;
		this.dataTypeConvertorFQN = new StringBuilder(dataTypeFQN).append("Converter").toString();
		if (this.dataType == null) {
			this.dataType = Class.forName(dataTypeFQN);
			if (this.dataType != String.class && this.dataType != Integer.class && this.dataType != Float.class && this.dataType != Long.class
					&& this.dataType != Date.class && this.dataType != Boolean.class && this.dataType != BigDecimal.class) {
				try {
					this.dataTypeConvertor = Class.forName(this.dataTypeConvertorFQN);
				} catch (ClassNotFoundException e) {
					LOG.log(Level.WARNING, "Converter class not found for data type: " + dataTypeFQN, e);
				}
			}
		}
	}

	public Object getConvertedValue1() throws Exception {
		return this.convertValue(this.value1);
	}

	public Object getConvertedValue2() throws Exception {
		return this.convertValue(this.value2);
	}

	private Object convertValue(String v) throws Exception {
		if (v == null) {
			return null;
		}
		if (this.dataType == Integer.class) {
			return Integer.parseInt(v);
		}
		if (this.dataType == Float.class) {
			return Float.parseFloat(v);
		}
		if (this.dataType == Long.class) {
			return Long.parseLong(v);
		}
		if (this.dataType == Boolean.class) {
			return Boolean.parseBoolean(v);
		}
		if (this.dataType == Date.class) {
			Object res = null;
			try {
				SimpleDateFormat df1 = new SimpleDateFormat(Constants.get_server_datetime_format());
				res = df1.parse(v);
			} catch (ParseException pe) {
				try {
					SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					res = df2.parse(v);
				} catch (ParseException pe2) {
					try {
						SimpleDateFormat df3 = new SimpleDateFormat(Constants.get_server_date_format());
						res = df3.parse(v);
					} catch (ParseException pe3) {
						SimpleDateFormat df4 = new SimpleDateFormat("yyyy-MM-dd");
						res = df4.parse(v);
					}
				}
			}
			return res;
		}
		if (this.dataType == BigDecimal.class) {
			return new BigDecimal(v);
		}
		if (this.dataTypeConvertor != null) {
			try {
				Method method = this.dataTypeConvertor.getMethod("convertToEntityAttribute", String.class);
				return method.invoke(this.dataTypeConvertor.newInstance(), v);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | InvocationTargetException | IllegalArgumentException
					| InstantiationException e) {
				LOG.log(Level.WARNING, String.format("Error occoured when try to create intance of type %s from %s", this.dataTypeConvertorFQN, v), e);
			}
		}
		return v;
	}
}
