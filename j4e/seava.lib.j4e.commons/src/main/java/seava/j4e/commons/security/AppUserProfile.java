/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.commons.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.util.CollectionUtils;

import seava.j4e.api.session.IOrganization;
import seava.j4e.api.session.IUserCache;
import seava.j4e.api.session.IUserProfile;

public class AppUserProfile implements IUserProfile, Serializable {

	private static final long serialVersionUID = -9131543374115237340L;
	private final boolean administrator;
	private final List<String> roles;

	private final boolean credentialsExpired;
	private final boolean accountExpired;
	private final boolean accountLocked;

	private List<IOrganization> organizations;
	private IUserCache cache;

	public AppUserProfile(boolean administrator, List<String> roles, boolean credentialsExpired, boolean accountExpired, boolean accountLocked) {
		super();
		this.administrator = administrator;
		this.roles = roles;
		this.credentialsExpired = credentialsExpired;
		this.accountExpired = accountExpired;
		this.accountLocked = accountLocked;
		this.organizations = new ArrayList<>();
	}

	@Override
	public boolean isAdministrator() {
		return this.administrator;
	}

	@Override
	public List<String> getRoles() {
		return this.roles;
	}

	@Override
	public boolean isCredentialsExpired() {
		return this.credentialsExpired;
	}

	@Override
	public boolean isAccountExpired() {
		return this.accountExpired;
	}

	@Override
	public boolean isAccountLocked() {
		return this.accountLocked;
	}

	@Override
	public List<IOrganization> getOrganizations() {
		if (this.organizations == null) {
			this.organizations = new ArrayList<>();
		}
		return this.organizations;
	}

	@Override
	public void addOrganisation(IOrganization org) {
		if (this.organizations == null) {
			this.organizations = new ArrayList<>();
		}
		this.organizations.add(org);
	}

	@Override
	public void setOrganisation(List<IOrganization> list) {
		this.organizations = list;
	}

	@Override
	public List<String> getOrganizationIds() {
		List<String> list = new ArrayList<>();
		if (CollectionUtils.isEmpty(this.getOrganizations())) {
			return Collections.emptyList();
		}
		for (IOrganization org : this.getOrganizations()) {
			list.add(org.getId());
		}
		return list;
	}

	@Override
	public void setCache(IUserCache cache) {
		this.cache = cache;
	}

	@Override
	public IUserCache getCache() {
		return this.cache;
	}
}
