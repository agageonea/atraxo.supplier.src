package seava.j4e.commons.utils.listener.changes;

public class ValueChanged {
	private String fieldName;
	private Object oldValue;
	private Object newValue;
	private Object source;
	private String messageCode;

	public ValueChanged(String fieldName, Object oldValue, Object newValue, Object source, String messageCode) {
		this.fieldName = fieldName;
		this.oldValue = oldValue;
		this.newValue = newValue;
		this.source = source;
		this.messageCode = messageCode;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public Object getOldValue() {
		return this.oldValue;
	}

	public Object getNewValue() {
		return this.newValue;
	}

	public Object getSource() {
		return this.source;
	}

	public String getMessageCode() {
		return this.messageCode;
	}
}
