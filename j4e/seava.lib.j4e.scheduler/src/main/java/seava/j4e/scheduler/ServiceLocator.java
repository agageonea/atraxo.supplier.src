package seava.j4e.scheduler;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import seava.j4e.api.service.job.IJob;
import seava.j4e.api.service.job.IJobFactory;

public class ServiceLocator implements ApplicationContextAware {

	static final Logger logger = LoggerFactory.getLogger(ServiceLocator.class);

	/**
	 * Spring application context
	 */
	protected ApplicationContext applicationContext;

	/**
	 * Job factories exported by bundles which declare jobs.
	 */
	private List<IJobFactory> jobFactories;

	public IJob findJob(String name) throws Exception {
		IJob srv = null;
		for (IJobFactory f : this.jobFactories) {
			try {
				srv = f.create(name);
				if (srv != null) {
					// srv.setDsServiceFactories(factories);
					// srv.setSystemConfig(this.systemConfig);
					return srv;
				}
			} catch (NoSuchBeanDefinitionException e) {
				// service not found in this factory, ignore
				logger.info("Could not find a service bean in this factory, will ignore !", e);
			}
		}
		return null;
	}

	/**
	 * Get job factories. If it is null attempts to retrieve it from Spring context by <code>osgiEntityJobFactories</code> alias.
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<IJobFactory> getEntityJobFactories() {
		if (this.jobFactories == null) {
			this.jobFactories = (List<IJobFactory>) this.applicationContext.getBean("osgiEntityJobFactories");
		}
		return this.jobFactories;
	}

	/**
	 * Set job factories
	 *
	 * @param entityServiceFactories
	 */
	public void setJobFactories(List<IJobFactory> jobFactories) {
		this.jobFactories = jobFactories;
	}

	/**
	 * Get applicationContext
	 *
	 * @return
	 */
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}

	/**
	 * Set applicationContext
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

}
