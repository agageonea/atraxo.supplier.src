package seava.j4e.scheduler.batch.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * @author zspeter
 */
public class DefaultActionListener implements JobExecutionListener {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultActionListener.class);

	@Override
	public void beforeJob(JobExecution jobExecution) {
		LOG.info("Starting job: " + jobExecution.getJobConfigurationName());
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		LOG.info("Job ended with status code: " + jobExecution.getExitStatus().getExitCode());
		ExitStatus status = jobExecution.getExitStatus();
		StringBuilder sb = new StringBuilder();
		for (Throwable throwable : jobExecution.getAllFailureExceptions()) {
			if (!jobExecution.getFailureExceptions().contains(throwable)) {
				jobExecution.addFailureException(throwable);
			}
			sb.append(throwable.getMessage()).append(";");
			status = status.addExitDescription(throwable);
		}

		if (sb.length() > 0) {
			sb.append("|");
		}

		sb.append(status.getExitDescription());
		status = new ExitStatus(status.getExitCode(), sb.toString());
		jobExecution.setExitStatus(status);
	}

}
