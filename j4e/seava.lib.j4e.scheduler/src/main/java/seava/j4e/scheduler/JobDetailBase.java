package seava.j4e.scheduler;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import seava.j4e.api.Constants;
import seava.j4e.api.service.job.IJob;

public class JobDetailBase implements IDnetQuartzJobDetail {

	private ServiceLocator serviceLocator;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		try {
			JobDataMap data = context.getJobDetail().getJobDataMap();
			String jobName = data.getString(Constants.QUARTZ_JOB_NAME);
			IJob job = this.getServiceLocator().findJob(jobName);
			if (job != null) {
				job.setExecutionContext(context);
				job.execute();
			}
		} catch (Exception e) {
			throw new JobExecutionException(e.getMessage(), e);
		}
	}

	@Override
	public ServiceLocator getServiceLocator() {
		return this.serviceLocator;
	}

	@Override
	public void setServiceLocator(ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}
}
