package seava.j4e.scheduler.quartz;

import org.quartz.Scheduler;
import org.quartz.SchedulerConfigException;
import org.quartz.SchedulerException;
import org.quartz.core.JobRunShell;
import org.quartz.core.JobRunShellFactory;
import org.quartz.spi.TriggerFiredBundle;

/**
 * Specific Quartz Job Run Shell Factory. Creates an Atraxo Job Run Shell {@link AtraxoJobRunSchell}.
 *
 * @author zspeter
 */
public class AtraxoJobRunSchellFactory implements JobRunShellFactory {

	private Scheduler scheduler;

	@Override
	public void initialize(Scheduler scheduler) throws SchedulerConfigException {
		this.scheduler = scheduler;

	}

	@Override
	public JobRunShell createJobRunShell(TriggerFiredBundle bundle) throws SchedulerException {
		return new AtraxoJobRunShell(this.scheduler, bundle);
	}

}
