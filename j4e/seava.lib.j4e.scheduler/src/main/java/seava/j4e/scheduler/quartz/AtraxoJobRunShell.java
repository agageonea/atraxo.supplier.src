package seava.j4e.scheduler.quartz;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.core.JobRunShell;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;

import seava.j4e.api.ISettings;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.InvalidConfiguration;
import seava.j4e.api.service.IClientInfoProvider;
import seava.j4e.api.session.IClient;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.IUserProfile;
import seava.j4e.api.session.IUserSettings;
import seava.j4e.api.session.IWorkspace;
import seava.j4e.api.session.Session;
import seava.j4e.commons.security.AppClient;
import seava.j4e.commons.security.AppUser;
import seava.j4e.commons.security.AppUserProfile;
import seava.j4e.commons.security.AppUserSettings;
import seava.j4e.domain.batchjob.JobParameter;

/**
 * Specific Job Run Shell. Before start the job add a session user into thread local {@link Session}.
 *
 * @author zspeter
 */
public class AtraxoJobRunShell extends JobRunShell {

	private ApplicationContext applicationContext;

	private ISettings settings;

	/**
	 * @param scheduler
	 * @param bndle
	 * @throws SchedulerException
	 */
	public AtraxoJobRunShell(Scheduler scheduler, TriggerFiredBundle bndle) throws SchedulerException {
		super(scheduler, bndle);
		this.applicationContext = (ApplicationContext) scheduler.getContext().get("applicationContext");
	}

	@Override
	protected void begin() throws SchedulerException {
		super.begin();
		try {
			String clientId = (String) this.jec.getMergedJobDataMap().get(JobParameter.CLIENTID.name());
			String clientCode = (String) this.jec.getMergedJobDataMap().get(JobParameter.CLIENTCODE.name());
			this.createSessionUser(clientId, clientCode);
		} catch (InvalidConfiguration e) {
			throw new SchedulerException(e);
		}
	}

	/**
	 * Settings getter. If null, attempts to resolve it from spring context.
	 *
	 * @return
	 */
	private ISettings getSettings() {
		if (this.settings == null) {
			this.settings = this.applicationContext.getBean(ISettings.class);
		}
		return this.settings;
	}

	private void createSessionUser(String clientId, String clientCode) throws InvalidConfiguration {
		String userCode = this.getSettings().getParam(SysParam.CORE_JOB_USER.name());
		//
		IClient client = new AppClient(clientId, clientCode, "");
		IUserSettings userSettings = AppUserSettings.newInstance(this.getSettings());
		IUserProfile profile = new AppUserProfile(true, null, false, false, false);

		// create an incomplete user first
		IUser user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, null, true);
		Session.user.set(user);

		// get the client workspace info
		IWorkspace ws = this.applicationContext.getBean(IClientInfoProvider.class).getClientWorkspace();
		user = new AppUser(userCode, userCode, null, userCode, null, null, client, userSettings, profile, ws, true);
		Session.user.set(user);
		this.sendMessage("updateJobSessionChannel", user);
	}

	protected void sendMessage(String to, Object content) {
		Message<Object> message = MessageBuilder.withPayload(content).build();
		this.applicationContext.getBean(to, MessageChannel.class).send(message);
	}

}
