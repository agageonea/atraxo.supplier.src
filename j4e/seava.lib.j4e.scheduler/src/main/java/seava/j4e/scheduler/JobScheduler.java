package seava.j4e.scheduler;

import java.util.Properties;

import javax.sql.DataSource;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.utils.DBConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import seava.j4e.api.service.job.IScheduler;

public class JobScheduler implements IScheduler, ApplicationContextAware {

	private ApplicationContext applicationContext;

	private ServiceLocator serviceLocator;

	private JobFactory jobFactory;

	final static Logger logger = LoggerFactory.getLogger(JobScheduler.class);

	public JobFactory getJobFactory() {
		return this.jobFactory;
	}

	public void setJobFactory(JobFactory jobFactory) {
		this.jobFactory = jobFactory;
	}

	Scheduler delegate;
	Properties quartzProperties;
	boolean autoStart;
	int autoStartDelay;
	boolean startOnDemand;

	DataSource dataSource;

	@Override
	public void init() throws Exception {

		MySchedulerFactory factory = new MySchedulerFactory();
		DbcpConnectionProvider cp = new DbcpConnectionProvider();
		cp.setDatasource(this.dataSource);
		DBConnectionManager.getInstance().addConnectionProvider("default", cp);

		factory.initialize(this.quartzProperties);

		ClassLoader ctcl = Thread.currentThread().getContextClassLoader();
		ClassLoader cccl = this.getClass().getClassLoader();
		ClassLoader jfcl = this.getClass().getClassLoader();

		Thread.currentThread().setContextClassLoader(cccl);
		this.delegate = factory.getScheduler();
		Thread.currentThread().setContextClassLoader(ctcl);

		this.delegate.setJobFactory(this.jobFactory);

		if (this.autoStart) {
			try {
				if (this.autoStartDelay > 0) {
					this.delegate.startDelayed(this.autoStartDelay);
				} else {
					this.delegate.start();
				}
			} catch (Exception e) {
				logger.error("Cannot start quartz scheduler. Reason is: " + e.getMessage(), e);
			}
		}
	}

	private void ensureDelegateIsStarted() throws SchedulerException {
		if (this.startOnDemand && this.delegate != null && !this.delegate.isStarted()) {
			this.delegate.start();
		}
	}

	@Override
	public void start() throws Exception {
		this.delegate.start();
	}

	@Override
	public void stop() throws Exception {
		this.delegate.shutdown();
	}

	@Override
	public Object getDelegate() throws Exception {
		this.ensureDelegateIsStarted();
		return this.delegate;
	}

	public Properties getQuartzProperties() {
		return this.quartzProperties;
	}

	public void setQuartzProperties(Properties quartzProperties) {
		this.quartzProperties = quartzProperties;
	}

	public boolean isAutoStart() {
		return this.autoStart;
	}

	public void setAutoStart(boolean autoStart) {
		this.autoStart = autoStart;
	}

	public int getAutoStartDelay() {
		return this.autoStartDelay;
	}

	public void setAutoStartDelay(int autoStartDelay) {
		this.autoStartDelay = autoStartDelay;
	}

	public DataSource getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public ServiceLocator getServiceLocator() {
		return this.serviceLocator;
	}

	public void setServiceLocator(ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	public boolean isStartOnDemand() {
		return this.startOnDemand;
	}

	public void setStartOnDemand(boolean startOnDemand) {
		this.startOnDemand = startOnDemand;
	}

}
