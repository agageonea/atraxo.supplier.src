package seava.j4e.scheduler.quartz;

import org.quartz.Scheduler;
import org.quartz.SchedulerConfigException;
import org.quartz.core.QuartzScheduler;
import org.quartz.core.QuartzSchedulerResources;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zspeter
 */
public class AtraxoSchedulerFactory extends StdSchedulerFactory {
	private static final Logger LOG = LoggerFactory.getLogger(AtraxoSchedulerFactory.class);

	@Override
	protected Scheduler instantiate(QuartzSchedulerResources rsrcs, QuartzScheduler qs) {
		Scheduler scheduler = super.instantiate(rsrcs, qs);
		AtraxoJobRunSchellFactory factory = new AtraxoJobRunSchellFactory();
		try {
			factory.initialize(scheduler);
		} catch (SchedulerConfigException e) {
			LOG.error(e.getMessage(), e);
		}
		rsrcs.setJobRunShellFactory(factory);
		return scheduler;
	}
}
