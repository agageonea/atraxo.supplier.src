package seava.j4e.scheduler.quartz;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.calendar.HolidayCalendar;
import org.quartz.listeners.JobChainingJobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.domain.batchjob.ActionDetail;
import seava.j4e.domain.batchjob.JobParameter;
import seava.j4e.domain.batchjob.TriggerDetail;

/**
 * @author zspeter
 */
public class PersistentJobScheduler {

	private static final String SCHEDULER_NOT_START = "Scheduler cannot be start.";
	private static final int HOUR_IN_DAY = 23;
	private static final int MIN_IN_HOUR = 60;
	private static final String CRON_FIELD_SEPARATOR = " ";
	private static final Logger logger = LoggerFactory.getLogger(PersistentJobScheduler.class);

	@Autowired
	private Scheduler scheduler;

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	/**
	 * Create, persist and schedule a job.
	 *
	 * @param jobClazz
	 * @param cronExpression
	 * @param details
	 * @param jobChainName
	 * @param clientId
	 * @param batchJobs
	 * @param jobListeners
	 * @return
	 * @throws BusinessException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Date schedulePersistentJobs(Class jobClazz, String cronExpression, TriggerDetail details, String jobChainName, String clientId,
			String clientCode, Map<String, ActionDetail> batchJobs, JobListener... jobListeners) throws BusinessException {
		try {
			Date nextFireTime;
			this.addJobListeners(jobListeners);
			JobChainingJobListener jobListener = (JobChainingJobListener) this.scheduler.getListenerManager().getJobListener(jobChainName);
			boolean find = true;
			if (jobListener == null) {
				find = false;
				jobListener = new JobChainingJobListener(jobChainName);
			}
			List<JobDetail> jobs = new LinkedList<>();
			JobKey jobKey = null;
			for (Entry<String, ActionDetail> entry : batchJobs.entrySet()) {
				String key = jobs.isEmpty() ? details.getuId() : entry.getKey();
				JobDetail job = this.buildJob(jobClazz, entry.getValue(), clientId, clientCode, key, details.getuId());
				jobs.add(job);
				if (jobKey != null) {
					jobListener.addJobChainLink(jobKey, job.getKey());
				}
				jobKey = job.getKey();
			}
			if (jobs.isEmpty()) {
				// no jobs are configured in job chain
				return null;
			}

			if (!find) {
				this.scheduler.getListenerManager().addJobListener(jobListener);
			}
			JobDetail firstJob = jobs.get(0);
			Trigger cronTrigger = this.getCronTrigger(cronExpression, details.getDate(), details.getTimezone(), details.getuId(), jobChainName);

			nextFireTime = cronTrigger.getNextFireTime() == null ? cronTrigger.getStartTime() : cronTrigger.getNextFireTime();
			if (this.scheduler.checkExists(firstJob.getKey())) {
				logger.info("Rescheduling the Job using cron expression: " + cronExpression);
				Trigger oldTrigger = this.scheduler.getTrigger(new TriggerKey(details.getuId()));
				if (oldTrigger == null) {
					Set<Trigger> newTriggers = new HashSet<>();
					newTriggers.add(cronTrigger);
					this.scheduler.scheduleJob(firstJob, newTriggers, true);
				} else {
					TriggerBuilder tb = oldTrigger.getTriggerBuilder();
					tb = tb.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression).inTimeZone(details.getTimezone()))
							.startAt(oldTrigger.getStartTime());
					if (this.scheduler.getCalendarNames().contains(jobChainName)) {
						tb.modifiedByCalendar(jobChainName);
					}
					Trigger newTrigger = tb.build();
					nextFireTime = newTrigger.getNextFireTime();
					this.scheduler.rescheduleJob(oldTrigger.getKey(), newTrigger);
				}
			} else {
				logger.info("Scheduling the Job using cron expression: " + cronExpression);
				this.scheduler.scheduleJob(firstJob, cronTrigger);
			}
			this.addJobsToScheduler(jobs, firstJob);
			this.scheduler.start();
			return nextFireTime;
		} catch (SchedulerException e) {
			logger.error(SCHEDULER_NOT_START, e);
			throw new BusinessException(ErrorCode.SCHEDULER_ERROR_CODE, e);
		}
	}

	/**
	 * Unschedule the job triggers.
	 *
	 * @param jobClazz
	 * @param uId - Job Key and trigger unique identifier.
	 * @param clientId
	 * @throws BusinessException
	 */
	public void unschedulePersistentJob(Class<?> jobClazz, String uId, String clientId, String clientCode) throws BusinessException {
		try {

			JobDetail job = this.buildJob(jobClazz, null, clientId, clientCode, uId, uId);

			if (this.scheduler.checkExists(job.getKey())) {
				logger.info("Unscheduling the Job");
				Trigger trigger = this.scheduler.getTrigger(new TriggerKey(job.getKey().getName()));
				if (trigger != null) {
					this.scheduler.unscheduleJob(trigger.getKey());
				} else {
					String msg = "Cannot unschedule the job " + job.getKey().getName() + ". Trigger not found.";
					logger.warn(msg);
				}
			}
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			throw new BusinessException(ErrorCode.SCHEDULER_ERROR_CODE, e);
		}

	}

	/**
	 * @param jobClazz
	 * @param jobChainName
	 * @param clientId
	 * @param batchJobs
	 * @param jobListeners
	 * @throws BusinessException
	 */
	@SuppressWarnings("rawtypes")
	public void runNow(Class jobClazz, String jobChainName, String clientId, String clientCode, Map<String, ActionDetail> batchJobs,
			JobListener... jobListeners) throws BusinessException {
		try {
			this.addJobListeners(jobListeners);
			JobChainingJobListener jobListener = (JobChainingJobListener) this.scheduler.getListenerManager().getJobListener(jobChainName);
			boolean find = true;
			if (jobListener == null) {
				find = false;
				jobListener = new JobChainingJobListener(jobChainName);
			}
			List<JobDetail> jobs = new ArrayList<>();
			JobDetail firstJob = null;
			JobKey jobKey = null;
			for (Entry<String, ActionDetail> entry : batchJobs.entrySet()) {
				JobDetail job = this.buildJob(jobClazz, entry.getValue(), clientId, clientCode, entry.getKey(), null);
				jobs.add(job);
				if (jobKey != null) {
					jobListener.addJobChainLink(jobKey, job.getKey());

				} else {
					firstJob = (JobDetail) job.clone();
				}
				jobKey = job.getKey();
			}
			if (!find) {
				this.scheduler.getListenerManager().addJobListener(jobListener);
			}
			for (JobDetail jobDetail : jobs) {
				this.scheduler.addJob(jobDetail, true);
			}
			if (firstJob != null) {
				this.scheduler.triggerJob(firstJob.getKey());
			}
		} catch (SchedulerException e) {
			logger.error(SCHEDULER_NOT_START, e);
			throw new BusinessException(ErrorCode.SCHEDULER_ERROR_CODE, e);
		}
	}

	private void addJobListeners(JobListener... jobListeners) throws SchedulerException {
		List<JobListener> list = Arrays.asList(jobListeners);
		for (JobListener l : list) {
			this.scheduler.getListenerManager().addJobListener(l);
		}
	}

	/**
	 * Create, persist and configure a job to run once at a specified date and time.
	 *
	 * @param jobClazz - The job class.
	 * @param date - Date when the job will be lunched.
	 * @param jobChainName - The name of the job.
	 * @param clientId - The client id that configured the job.
	 * @param batchJobs - Jobs
	 * @param jobListeners
	 * @throws BusinessException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Date runOnceAt(Class jobClazz, TriggerDetail triggerDetail, String jobChainName, String clientId, String clientCode,
			Map<String, ActionDetail> batchJobs, JobListener... jobListeners) throws BusinessException {
		try {
			Date nextFireTime;
			this.addJobListeners(jobListeners);
			JobChainingJobListener jobListener = (JobChainingJobListener) this.scheduler.getListenerManager().getJobListener(jobChainName);
			boolean find = true;
			if (jobListener == null) {
				find = false;
				jobListener = new JobChainingJobListener(jobChainName);
			}
			List<JobDetail> jobs = new ArrayList<>();
			JobKey jobKey = null;
			for (Entry<String, ActionDetail> entry : batchJobs.entrySet()) {
				String key = jobs.isEmpty() ? triggerDetail.getuId() : triggerDetail.getuId() + "." + entry.getKey();
				JobDetail job = this.buildJob(jobClazz, entry.getValue(), clientId, clientCode, key, triggerDetail.getuId());
				jobs.add(job);
				if (null != jobKey) {
					jobListener.addJobChainLink(jobKey, job.getKey());
				}
				jobKey = job.getKey();
			}
			if (jobs.isEmpty()) {
				// no action in job chain
				return null;
			}
			JobDetail firstJob = jobs.get(0);
			if (!find) {
				this.scheduler.getListenerManager().addJobListener(jobListener);
			}
			SimpleTrigger trigger = newTrigger().withIdentity(new TriggerKey(firstJob.getKey().getName())).startAt(triggerDetail.getDate())
					.withSchedule(SimpleScheduleBuilder.simpleSchedule().withRepeatCount(0)).build();
			nextFireTime = trigger.getNextFireTime();
			if (this.scheduler.checkExists(firstJob.getKey())) {
				logger.info("Rescheduling the Job");
				this.scheduler.checkExists(new TriggerKey(firstJob.getKey().getName()));
				Trigger oldTrigger = this.scheduler.getTrigger(new TriggerKey(firstJob.getKey().getName()));
				if (oldTrigger == null) {
					Set<Trigger> newTriggers = new HashSet<>();
					newTriggers.add(trigger);
					this.scheduler.scheduleJob(firstJob, newTriggers, true);
				} else {
					TriggerBuilder tb = oldTrigger.getTriggerBuilder();
					Trigger newTrigger = tb.startAt(triggerDetail.getDate()).withSchedule(SimpleScheduleBuilder.simpleSchedule().withRepeatCount(0))
							.build();
					nextFireTime = newTrigger.getNextFireTime();
					this.scheduler.rescheduleJob(oldTrigger.getKey(), newTrigger);
				}
			} else {
				logger.info("Schedule job chain " + jobChainName + " to run at " + new SimpleDateFormat().format(triggerDetail.getDate()));
				this.scheduler.scheduleJob(firstJob, trigger);
			}
			this.addJobsToScheduler(jobs, firstJob);
			this.scheduler.start();
			return nextFireTime;
		} catch (SchedulerException e) {
			logger.error(SCHEDULER_NOT_START, e);
			throw new BusinessException(ErrorCode.SCHEDULER_ERROR_CODE, e);
		}
	}

	private void addJobsToScheduler(List<JobDetail> jobs, JobDetail firstJob) throws SchedulerException {
		for (JobDetail jobDetail : jobs) {
			if (!jobDetail.equals(firstJob)) {
				this.scheduler.addJob(jobDetail, true);
			}
		}
	}

	/**
	 * Create, configure and persist a job the run daily with a specified day interval.
	 *
	 * @param jobClazz - The job class.
	 * @param jobchainName - The name of the job.
	 * @param clientId - The client id that configured the job.
	 * @param batchJobs - Jobs.
	 * @param triggerDetail
	 * @param jobListeners
	 * @return
	 * @throws BusinessException
	 */
	@SuppressWarnings("rawtypes")
	public Date runDaily(Class jobClazz, TriggerDetail triggerDetail, String jobchainName, String clientId, String clientCode,
			Map<String, ActionDetail> batchJobs, JobListener... jobListeners) throws BusinessException {

		Calendar cal = Calendar.getInstance();
		cal.setTime(triggerDetail.getDate());
		StringBuilder sb = new StringBuilder();
		// SEC
		sb.append("0").append(CRON_FIELD_SEPARATOR);
		// MIN
		this.buildStartMin(triggerDetail.getIntInMinutes(), triggerDetail.getDurationInMinutes(), cal, sb);
		// HOUR
		this.buildStartHour(triggerDetail.getDurationInHour(), triggerDetail.getDurationInMinutes(), cal, sb);
		// DAYS
		if (triggerDetail.getIntervalInDays() > 1) {
			sb.append("*/").append(triggerDetail.getIntervalInDays()).append(CRON_FIELD_SEPARATOR);
		} else {
			sb.append("*").append(CRON_FIELD_SEPARATOR);
		}
		// MONTH
		sb.append("*").append(CRON_FIELD_SEPARATOR);
		// Day of week
		sb.append("?").append(CRON_FIELD_SEPARATOR);
		return this.schedulePersistentJobs(jobClazz, sb.toString(), triggerDetail, jobchainName, clientId, clientCode, batchJobs, jobListeners);
	}

	/**
	 * Create, persist and configure a job to run on weekly basis.
	 *
	 * @param jobClazz - The job class.
	 * @param jobChainName - The name of the job.
	 * @param triggerDetail
	 * @param clientId - The client id that configured the job.
	 * @param batchJobs
	 * @param jobListeners
	 * @return
	 * @throws BusinessException
	 */
	@SuppressWarnings("rawtypes")
	public Date runWeekly(Class jobClazz, String jobChainName, Map<String, ActionDetail> batchJobs, TriggerDetail triggerDetail, String clientId,
			String clientCode, JobListener... jobListeners) throws BusinessException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(triggerDetail.getDate());
		StringBuilder sb = new StringBuilder();
		// SEC
		sb.append("0").append(CRON_FIELD_SEPARATOR);
		// MIN
		this.buildStartMin(triggerDetail.getIntInMinutes(), triggerDetail.getDurationInMinutes(), cal, sb);
		// HOUR
		this.buildStartHour(triggerDetail.getDurationInHour(), triggerDetail.getDurationInMinutes(), cal, sb);
		// DAYS
		sb.append("?").append(CRON_FIELD_SEPARATOR);
		// MONTH
		sb.append("*").append(CRON_FIELD_SEPARATOR);
		// Day of week
		for (Integer weekDays : triggerDetail.getOnDaysOfWeek()) {
			int dayNr = weekDays % 7;
			if (dayNr == 0) {
				sb.append(7).append(",");
			} else {
				sb.append(dayNr).append(",");
			}
		}
		sb.replace(sb.lastIndexOf(","), sb.length(), CRON_FIELD_SEPARATOR);
		if (triggerDetail.getWeekOffset() > 1) {
			try {
				HolidayCalendar calendar = this.buildHolidayCalendar(triggerDetail.getOnDaysOfWeek(), triggerDetail.getWeekOffset(),
						triggerDetail.getDate(), triggerDetail.getTimezone());
				this.scheduler.addCalendar(jobChainName, calendar, true, false);
			} catch (SchedulerException e) {
				throw new BusinessException(ErrorCode.SCHEDULER_ERROR_CODE, e);
			}
		}
		return this.schedulePersistentJobs(jobClazz, sb.toString(), triggerDetail, jobChainName, clientId, clientCode, batchJobs, jobListeners);
	}

	/**
	 * Create, persist and configure a job to run monthly basis on specified months and specified days in month.
	 *
	 * @param jobClazz - The job class.
	 * @param jobChainName - The name of the job.
	 * @param batchJobs
	 * @param clientId - The client id that configured the job.
	 * @param triggerDetail
	 * @param jobListeners
	 * @return
	 * @throws BusinessException
	 */
	@SuppressWarnings("rawtypes")
	public Date runMonthly(Class jobClazz, String jobChainName, Map<String, ActionDetail> batchJobs, TriggerDetail triggerDetail, String clientId,
			String clientCode, JobListener... jobListeners) throws BusinessException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(triggerDetail.getDate());
		StringBuilder sb = new StringBuilder();
		// SEC
		sb.append("0").append(CRON_FIELD_SEPARATOR);
		this.buildStartMin(triggerDetail.getIntInMinutes(), triggerDetail.getDurationInMinutes(), cal, sb);
		// HOUR
		this.buildStartHour(triggerDetail.getDurationInHour(), triggerDetail.getDurationInMinutes(), cal, sb);
		// DAYS
		if (triggerDetail.isOnMonthDays()) {
			if (triggerDetail.getOnDaysOfMonth().contains(0)) {
				sb.append("*").append(CRON_FIELD_SEPARATOR);
			} else if (triggerDetail.getOnDaysOfMonth().contains(32)) {
				sb.append("L").append(CRON_FIELD_SEPARATOR);
			} else {
				for (Integer day : triggerDetail.getOnDaysOfMonth()) {
					sb.append(day).append(",");
				}
				sb.replace(sb.lastIndexOf(","), sb.length(), CRON_FIELD_SEPARATOR);
			}
		} else {
			sb.append("?").append(CRON_FIELD_SEPARATOR);
		}
		// MONTHS
		this.buildMonthCronField(triggerDetail.getOnMonths(), sb);
		// DAY IN A WEEK OF MONTH
		if (triggerDetail.isOnMonthDays()) {
			sb.append("?").append(CRON_FIELD_SEPARATOR);
		} else {
			int weekNumber = triggerDetail.getWeekOfMonth();
			if (triggerDetail.getWeekOfMonth() == 5) {
				sb.append(triggerDetail.getWeekDay() + "L").append(CRON_FIELD_SEPARATOR);
			} else {
				sb.append(triggerDetail.getWeekDay() + "#" + weekNumber).append(CRON_FIELD_SEPARATOR);
			}
		}
		return this.schedulePersistentJobs(jobClazz, sb.toString(), triggerDetail, jobChainName, clientId, clientCode, batchJobs, jobListeners);

	}

	private void buildMonthCronField(Set<Integer> onMonths, StringBuilder sb) {
		if (onMonths.contains(0)) {
			sb.append("*").append(CRON_FIELD_SEPARATOR);
		} else {
			for (Integer month : onMonths) {
				sb.append(month).append(",");
			}
			sb.replace(sb.lastIndexOf(","), sb.length(), CRON_FIELD_SEPARATOR);
		}
	}

	private HolidayCalendar buildHolidayCalendar(Set<Integer> onDaysOfWeek, Integer weekOffset, Date date, TimeZone timeZone) {
		Calendar cal = GregorianCalendar.getInstance(timeZone);
		cal.setTime(date);
		int offset = weekOffset == null ? 0 : weekOffset;
		HolidayCalendar calendar = new HolidayCalendar();
		for (int i = 0; i < 52; i++) {
			if (i % offset != 0) {
				for (Integer weekDays : onDaysOfWeek) {
					cal.set(Calendar.DAY_OF_WEEK, weekDays);
					calendar.addExcludedDate(cal.getTime());
				}
			}
			cal.add(Calendar.WEEK_OF_YEAR, 1);
		}
		return calendar;
	}

	private void buildStartHour(Integer durationInHour, Integer durationInMinutes, Calendar cal, StringBuilder sb) {
		if (durationInMinutes != null) {
			if (durationInMinutes == 0) {
				sb.append("*").append(CRON_FIELD_SEPARATOR);
			} else {
				int startHour = cal.get(Calendar.HOUR_OF_DAY);
				sb.append(startHour).append("-").append(startHour + 1).append(CRON_FIELD_SEPARATOR);

			}
		} else {
			int startHour = cal.get(Calendar.HOUR_OF_DAY);
			if (durationInHour != null && durationInHour > 0) {
				sb.append(startHour).append("-").append(startHour + durationInHour <= HOUR_IN_DAY ? startHour + durationInHour : HOUR_IN_DAY)
						.append(CRON_FIELD_SEPARATOR);
			} else {
				sb.append(startHour).append(CRON_FIELD_SEPARATOR);
			}
		}
	}

	private void buildStartMin(Integer intInMinutes, Integer durationInMinutes, Calendar cal, StringBuilder sb) {
		int duration = durationInMinutes != null ? durationInMinutes : -1;
		if (intInMinutes != null && intInMinutes > 0 && intInMinutes < MIN_IN_HOUR) {
			if (duration > 0 && duration < MIN_IN_HOUR) {
				int startMin = cal.get(Calendar.MINUTE);
				for (int i = startMin; i < duration + startMin && i < MIN_IN_HOUR; i = i + intInMinutes) {
					sb.append(i).append(",");
				}
				sb.replace(sb.lastIndexOf(","), sb.length(), CRON_FIELD_SEPARATOR);
			} else if (duration == 0 || duration >= MIN_IN_HOUR) {
				int startMin = cal.get(Calendar.MINUTE);
				for (int i = startMin; i < startMin + MIN_IN_HOUR; i = i + intInMinutes) {
					sb.append(i % MIN_IN_HOUR).append(",");
				}
				sb.replace(sb.lastIndexOf(","), sb.length(), CRON_FIELD_SEPARATOR);
			} else {
				sb.append("*/").append(intInMinutes).append(CRON_FIELD_SEPARATOR);
			}
		} else {
			int startMin = cal.get(Calendar.MINUTE);
			sb.append(startMin).append(CRON_FIELD_SEPARATOR);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JobDetail buildJob(Class jobClazz, ActionDetail actionDetail, String clientId, String clientCode, String jobKeyName, String jobKeyGroup) {
		JobKey jobKey = jobKeyGroup == null ? new JobKey(jobKeyName) : new JobKey(jobKeyName, jobKeyGroup);
		return newJob(jobClazz).withDescription(actionDetail == null ? "" : actionDetail.getActionName()).withIdentity(jobKey).storeDurably()
				.usingJobData(this.getJobDataMap(actionDetail, clientId, clientCode)).build();
	}

	private JobDataMap getJobDataMap(ActionDetail actionDetail, String clientId, String clientCode) {
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put(JobParameter.CLIENTID.name(), clientId);
		jobDataMap.put(JobParameter.CLIENTCODE.name(), clientCode);
		if (actionDetail == null) {
			return jobDataMap;
		}
		jobDataMap.put(JobParameter.JOBNAME.name(), actionDetail.getBeanName());
		jobDataMap.put(JobParameter.ACTION.name(), actionDetail.getActionId());
		jobDataMap.put(JobParameter.ACTIONNAME.name(), actionDetail.getActionName());
		jobDataMap.put(JobParameter.JOBCHAIN.name(), actionDetail.getJobChainId());
		for (Entry<String, String> entry : actionDetail.getProperties().entrySet()) {
			jobDataMap.put(entry.getKey(), entry.getValue());
		}
		return jobDataMap;
	}

	private Trigger getCronTrigger(String cronExpression, Date startDate, TimeZone timezone, String triggerName, String calendarName)
			throws BusinessException {
		try {
			TriggerBuilder<CronTrigger> triggerBuilder = newTrigger().withIdentity(new TriggerKey(triggerName)).startAt(startDate)
					.withSchedule(CronScheduleBuilder.cronSchedule(new CronExpression(cronExpression)).inTimeZone(timezone));
			if (this.scheduler.getCalendarNames().contains(calendarName)) {
				triggerBuilder.modifiedByCalendar(calendarName);
			}
			return triggerBuilder.build();
		} catch (ParseException e) {
			logger.error("Cron expression cannot be parsed.", e);
			throw new BusinessException(ErrorCode.INVALID_CRON_EXPRESSION, "Expression (" + cronExpression + ") is not a valid cron expression.");
		} catch (SchedulerException e) {
			logger.error("Error occoured on trying to retrive calendars.", e);
			throw new BusinessException(ErrorCode.SCHEDULER_ERROR_CODE, e);
		}
	}

}
