package seava.j4e.domain.batchjob;

public enum JobParameter {
	CLIENTID, CLIENTCODE, JOBNAME, ACTION, JOBCHAIN, ACTIONNAME;
}
