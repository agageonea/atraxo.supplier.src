/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.domain.report;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum NotificationAction {

	_EMPTY_(""), _NOACTION_("NoAction"), _GOTO_("GoTo");

	private String name;

	private NotificationAction(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public static NotificationAction getByName(String name) {
		for (NotificationAction status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent NotificationAction with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
