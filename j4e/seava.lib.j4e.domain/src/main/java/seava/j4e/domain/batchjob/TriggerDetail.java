package seava.j4e.domain.batchjob;

import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

/**
 * @author zspeter
 */
public class TriggerDetail {
	private final String uId;
	private Date date;
	private TimeZone timezone;
	private Integer intervalInDays;
	private Integer intInMinutes;
	private Integer durationInHour;
	private Integer durationInMinutes;
	private Integer weekOffset;
	private Integer weekDay;
	private Integer weekOfMonth;
	private Set<Integer> onDaysOfWeek;
	private Set<Integer> onMonths;
	private Set<Integer> onDaysOfMonth;
	private boolean onMonthDays;

	/**
	 * @param uId
	 */
	public TriggerDetail(String uId) {
		this.uId = uId;
	}

	public boolean isOnMonthDays() {
		return this.onMonthDays;
	}

	public void setOnMonthDays(boolean onMonthDays) {
		this.onMonthDays = onMonthDays;
	}

	public Set<Integer> getOnMonths() {
		return this.onMonths;
	}

	public void setOnMonths(Set<Integer> onMonths) {
		this.onMonths = onMonths;
	}

	public Set<Integer> getOnDaysOfMonth() {
		return this.onDaysOfMonth;
	}

	public void setOnDaysOfMonth(Set<Integer> onDaysOfMonth) {
		this.onDaysOfMonth = onDaysOfMonth;
	}

	public Set<Integer> getOnDaysOfWeek() {
		return this.onDaysOfWeek;
	}

	public void setOnDaysOfWeek(Set<Integer> onDaysOfWeek) {
		this.onDaysOfWeek = onDaysOfWeek;
	}

	public Integer getWeekDay() {
		return this.weekDay;
	}

	public void setWeekDay(Integer weekDay) {
		this.weekDay = weekDay;
	}

	public Integer getWeekOfMonth() {
		return this.weekOfMonth;
	}

	public void setWeekOfMonth(Integer weekOfMonth) {
		this.weekOfMonth = weekOfMonth;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public TimeZone getTimezone() {
		return this.timezone;
	}

	public void setTimezone(TimeZone timezone) {
		this.timezone = timezone;
	}

	public Integer getIntervalInDays() {
		return this.intervalInDays;
	}

	public void setIntervalInDays(Integer intervalInDays) {
		this.intervalInDays = intervalInDays;
	}

	public Integer getIntInMinutes() {
		return this.intInMinutes;
	}

	public void setIntInMinutes(Integer intInMinutes) {
		this.intInMinutes = intInMinutes;
	}

	public Integer getDurationInHour() {
		return this.durationInHour;
	}

	public void setDurationInHour(Integer durationInHour) {
		this.durationInHour = durationInHour;
	}

	public Integer getDurationInMinutes() {
		return this.durationInMinutes;
	}

	public void setDurationInMinutes(Integer durationInMinutes) {
		this.durationInMinutes = durationInMinutes;
	}

	public Integer getWeekOffset() {
		return this.weekOffset;
	}

	public void setWeekOffset(Integer weekOffset) {
		this.weekOffset = weekOffset;
	}

	public String getuId() {
		return this.uId;
	}

}
