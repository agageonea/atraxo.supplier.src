/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.domain.report;

import seava.j4e.api.exceptions.InvalidEnumException;

public enum Category {

	_EMPTY_(""), _ACTIVITY_("Activity"), _JOB_("Job"), _ERROR_("Error"), _PROCESS_("Process"), _TIMEOUT_("Timeout");

	private String name;

	private Category(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public static Category getByName(String name) {
		for (Category status : values()) {
			if (status.getName().equalsIgnoreCase(name)) {
				return status;
			}
		}
		throw new InvalidEnumException("Inexistent Category with name: " + name);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
