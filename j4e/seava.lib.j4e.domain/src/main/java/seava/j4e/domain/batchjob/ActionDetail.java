package seava.j4e.domain.batchjob;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zspeter
 */
public class ActionDetail {
	private int actionId;
	private int jobChainId;
	private String beanName;
	private String actionName;
	private Map<String, String> properties;

	/**
	 * @param actionId
	 * @param actionName
	 * @param jobChainId
	 * @param beanName
	 */
	public ActionDetail(int actionId, String actionName, int jobChainId, String beanName) {
		this.actionId = actionId;
		this.jobChainId = jobChainId;
		this.beanName = beanName;
		this.actionName = actionName;
		this.properties = new HashMap<>();
	}

	public int getJobChainId() {
		return this.jobChainId;
	}

	public int getActionId() {
		return this.actionId;
	}

	public String getBeanName() {
		return this.beanName;
	}

	public String getActionName() {
		return this.actionName;
	}

	public Map<String, String> getProperties() {
		return this.properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	/**
	 * @param key
	 * @param value
	 */
	public void putToProperties(String key, String value) {
		this.properties.put(key, value);
	}

}
