/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.business.spring;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.orm.jpa.persistenceunit.MutablePersistenceUnitInfo;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitPostProcessor;

/**
 * DnetPersistenceUnitPostProcessor
 */
public class DnetPersistenceUnitPostProcessor implements PersistenceUnitPostProcessor {

	private String persistenceUnitName;
	private Properties persistenceProperties;

	private Map<String, List<String>> puiClasses = new HashMap<>();

	/**
	 * @param persistenceUnitName
	 * @param persistenceProperties
	 */
	public DnetPersistenceUnitPostProcessor(final String persistenceUnitName, final Properties persistenceProperties) {
		this.persistenceUnitName = persistenceUnitName;
		this.persistenceProperties = persistenceProperties;

	}

	@Override
	public void postProcessPersistenceUnitInfo(MutablePersistenceUnitInfo pui) {

		List<String> classes = this.puiClasses.get(pui.getPersistenceUnitName());
		if (classes == null) {
			classes = new ArrayList<>();
			this.puiClasses.put(pui.getPersistenceUnitName(), classes);
		}
		pui.getManagedClassNames().addAll(classes);

		classes.addAll(pui.getManagedClassNames());

		if (this.persistenceUnitName.equals(pui.getPersistenceUnitName())) {
			final Properties properties = pui.getProperties();

			for (final Map.Entry<Object, Object> entries : this.persistenceProperties.entrySet()) {
				properties.put(entries.getKey(), entries.getValue());
			}

		}
	}

}
