package seava.j4e.business.service.asgn;

import seava.j4e.api.service.business.IAsgnTxService;

/**
 * DefaultAsgnTxService
 *
 * @param <E>
 */
public class DefaultAsgnTxService<E> extends AbstractAsgnTxService<E> implements IAsgnTxService<E> {

	public DefaultAsgnTxService() {
		// default constructor empty
	}

	/**
	 * @param entityClass
	 */
	public DefaultAsgnTxService(Class<E> entityClass) {
		this.setEntityClass(entityClass);
	}

}