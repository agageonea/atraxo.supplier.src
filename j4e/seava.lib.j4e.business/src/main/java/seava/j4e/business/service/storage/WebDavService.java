package seava.j4e.business.service.storage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;

import org.omg.CORBA.portable.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.sardine.DavResource;
import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;

import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.StorageException;

/**
 * @author aradu, abolindu
 */
public class WebDavService {

	private static final String COULD_NOT_UPLOAD_FILE_TO_THE_WEB_DAV_SERVER = "Could not upload file to the WebDav server.";

	private static final Logger LOG = LoggerFactory.getLogger(WebDavService.class);

	public static final char WEBSEPARATOR = '/';

	private String user;
	private String password;
	private String url;
	private String dirUrl;

	private Sardine sardine;

	public WebDavService(String user, String password, String url, String dirUrl) {
		super();
		this.user = user;
		this.password = password;
		this.url = url;
		this.dirUrl = dirUrl;
	}

	@PostConstruct
	private void initialize() {
		if (this.user != null) {
			this.sardine = SardineFactory.begin(this.user, this.password);
		} else {
			this.sardine = SardineFactory.begin();
		}
	}

	public String getDirUrl() {
		return this.dirUrl;
	}

	/**
	 * List Directory
	 *
	 * @param url the path of the folder that has to be listed
	 * @param depth the depth 0 only for this folder 1 for subfolders
	 * @throws IOException
	 */
	public List<DavResource> listFiles(String url, int depth) throws IOException {
		return this.sardine.list(this.url + url, depth);
	}

	/**
	 * Move file
	 *
	 * @param from the path of the file that has to be moved
	 * @param to the path where the file is supposed to be moved
	 * @throws ApplicationException
	 */
	public void moveFile(String from, String to) throws StorageException {
		try {
			String folderPath = to.substring(0, to.lastIndexOf('/') + 1);
			if (LOG.isDebugEnabled()) {
				LOG.debug("Trying to move from: [" + from + "] to: [" + to + "]");
			}
			this.checkAndCreateFolders(folderPath);
			if (this.sardine.exists(this.url + folderPath)) {
				this.sardine.move(this.url + from, this.url + to);
			}

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new StorageException(ErrorCode.FILE_NOT_UPLOADED, "Couldn't move file on the WebDav server.", e);
		}
	}

	/**
	 * Upload files and creates the missing folders from its path
	 *
	 * @param out
	 * @param path
	 * @throws StorageException
	 */
	public void uploadToWebDav(ByteArrayOutputStream out, String path) throws StorageException {
		String fullPath = new StringBuilder(this.getDirUrl()).append(path).toString();
		String dirPath = fullPath.substring(0, fullPath.lastIndexOf('/'));
		if (this.existResource(fullPath)) {
			this.deleteResource(fullPath);
		}
		this.checkAndCreateFolders(dirPath);
		this.uploadFile(fullPath, out.toByteArray());
	}

	/**
	 * Upload file
	 *
	 * @param url WebDav inner URL string, containing the file name too
	 * @param filepath full file path
	 * @param contentType content-type of the file to upload; for example: "image/png"
	 * @throws StorageException
	 */
	public void uploadFile(String urlPath, String filepath, String contentType) throws StorageException {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Trying to upload [" + this.url + "] to the WebDav url [" + urlPath + "]");
			}
			Path path = Paths.get(filepath);
			byte[] data = Files.readAllBytes(path);
			String fileUrl = this.url + urlPath;
			if (!this.sardine.exists(fileUrl)) {
				this.sardine.put(fileUrl, data, contentType);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new StorageException(ErrorCode.FILE_NOT_UPLOADED, COULD_NOT_UPLOAD_FILE_TO_THE_WEB_DAV_SERVER, e);
		}
	}

	/**
	 * Upload file
	 *
	 * @param url WebDav inner URL string, containing the file name too
	 * @param dataStream
	 * @throws StorageException
	 */
	public void uploadFile(String urlPath, InputStream dataStream) throws StorageException {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Trying to upload an input stream to the WebDav url [" + urlPath + "]");
			}
			String fileUrl = this.url + urlPath;
			if (!this.sardine.exists(fileUrl)) {
				this.sardine.put(fileUrl, dataStream);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new StorageException(ErrorCode.FILE_NOT_UPLOADED, COULD_NOT_UPLOAD_FILE_TO_THE_WEB_DAV_SERVER, e);
		}
	}

	/**
	 * Upload file
	 *
	 * @param url WebDav inner URL string, containing the file name too
	 * @param data
	 * @throws StorageException
	 */
	public void uploadFile(String urlPath, byte[] data) throws StorageException {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Trying to upload a byte array to the WebDav url [" + urlPath + "]");
			}
			String fileUrl = this.url + urlPath;
			if (!this.sardine.exists(fileUrl)) {
				this.sardine.put(fileUrl, data);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new StorageException(ErrorCode.FILE_NOT_UPLOADED, COULD_NOT_UPLOAD_FILE_TO_THE_WEB_DAV_SERVER, e);
		}
	}

	/**
	 * Upload file
	 *
	 * @param url WebDav inner URL string, containing the file name too
	 * @param localFile
	 * @param contentType content-type of the file to upload; for example: "image/png"
	 * @throws StorageException
	 */
	public void uploadFile(String urlPath, File localFile, String contentType) throws StorageException {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Trying to upload a file to the WebDav url [" + urlPath + "]");
			}
			Path path = Paths.get(localFile.getPath());
			byte[] data = Files.readAllBytes(path);
			String fileUrl = this.url + urlPath;
			if (!this.sardine.exists(fileUrl)) {
				this.sardine.put(fileUrl, data, contentType);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new StorageException(ErrorCode.FILE_NOT_UPLOADED, COULD_NOT_UPLOAD_FILE_TO_THE_WEB_DAV_SERVER, e);
		}
	}

	/**
	 * Create a new directory; checks if the directory already exists
	 *
	 * @param url WebDav inner URL string, containing the new directory name too
	 * @throws StorageException
	 */
	private void createDirectory(String urlPath) throws StorageException {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Trying to create a WebDav directory with url [" + urlPath + "]");
			}
			String directoryUrl = this.url + urlPath;
			if (!this.sardine.exists(directoryUrl)) {
				this.sardine.createDirectory(directoryUrl);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			// throw new StorageException(ErrorCode.WEBDAV_CREATE_DIR, "Couldn't create new directory on the WebDav server.", e);
		}
	}

	/**
	 * Download file
	 *
	 * @param url WebDav inner URL string, containing the file name too
	 * @throws StorageException
	 */
	public InputStream downloadFile(String urlPath) throws StorageException {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Trying to download a file from the WebDav url [" + urlPath + "]");
			}
			String fileUrl = this.url + urlPath;
			fileUrl = fileUrl.replace(File.separator, "/");
			if (this.sardine.exists(fileUrl)) {
				InputStream stream = this.sardine.get(fileUrl);
				if (stream != null) {
					return stream;
				} else {
					throw new StorageException(ErrorCode.FILE_NOT_FOUND, "Couldn't download file from the WebDav server because it does not exist.");
				}
			} else {
				throw new StorageException(ErrorCode.FILE_NOT_FOUND, "Couldn't download file from the WebDav server because it does not exist.");
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new StorageException(ErrorCode.FILE_NOT_FOUND, "Couldn't download file from the WebDav server.", e);
		}
	}

	/**
	 * Deletes a file or folder resource from the WebDav server
	 *
	 * @param url WebDav resource URL
	 * @throws StorageException
	 */
	public void deleteResource(String urlPath) throws StorageException {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Trying to delete a resource from the WebDav url [" + urlPath + "]");
			}
			String fileUrl = this.url + urlPath;
			if (this.sardine.exists(fileUrl)) {
				this.sardine.delete(fileUrl);
			}

			fileUrl = fileUrl.substring(0, fileUrl.lastIndexOf('/') - 1);
			if (this.sardine.exists(fileUrl)) {
				this.sardine.delete(fileUrl);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new StorageException(ErrorCode.FILE_NOT_DELETED, "Couldn't delete file from the WebDav server.", e);
		}
	}

	/**
	 * Checks if a file or folder resource exist
	 *
	 * @param url WebDav URL
	 * @throws StorageException
	 * @return true if the resource exist; false otherwise
	 */
	public boolean existResource(String urlPath) throws StorageException {
		boolean existsResource = false;
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Checking if resource with the url [" + urlPath + "] exist.");
			}
			existsResource = this.sardine.exists(this.url + urlPath);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			// throw new StorageException(ErrorCode.WEBDAV_CHECK, "Couldn't check if resource exist.", e);
		}
		return existsResource;
	}

	/**
	 * Checks if a folder and all parent folders are created. If not creates it.
	 *
	 * @param folder
	 * @throws StorageException
	 */
	public void checkAndCreateFolders(String folder) throws StorageException {
		String[] dirs = folder.split("/");
		for (int i = 0; i < dirs.length; i++) {
			String dir = this.getFolder(dirs, i);
			this.checkAndCreateFolder(dir);
		}
	}

	/**
	 * Check if the folder exist; creates it if not
	 *
	 * @param dir
	 * @throws StorageException
	 */
	public void checkAndCreateFolder(String dir) throws StorageException {
		if (dir != null && dir.length() > 0 && !this.existResource(dir)) {
			this.createDirectory(dir);
		}
	}

	private String getFolder(String[] dirs, int index) {
		StringBuilder dir = new StringBuilder();
		for (int i = 0; i <= index; i++) {
			dir.append(dirs[i]).append(WEBSEPARATOR);
		}
		return dir.toString();
	}

	/**
	 * @return
	 */
	public String getCurentDateFolderPath() {
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November",
				"December" };
		Calendar cal = new GregorianCalendar();
		int currentYear = cal.get(Calendar.YEAR);
		String currentMonth = monthName[cal.get(Calendar.MONTH)];
		int currentDay = cal.get(Calendar.DAY_OF_MONTH);
		StringBuilder currentDateFolderPath = new StringBuilder().append(currentYear).append('/').append(currentMonth).append('/').append(currentDay);
		return currentDateFolderPath.toString();
	}

}