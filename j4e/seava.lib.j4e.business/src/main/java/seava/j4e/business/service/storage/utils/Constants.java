package seava.j4e.business.service.storage.utils;

public class Constants {

	public static final String FILE = "file";
	public static final String BUCKET = "bucket";
	public static final String TAG_KEY = "tagKey";
	public static final String FILENAME = "fileName";
	public static final String DATA_TYPE = "dataType";
	public static final String OBJECT_KEY = "objectKey";
	public static final String TO_OBJECT_KEY = "toObjectKey";
	public static final String FROM_OBJECT_KEY = "fromObjectKey";
	public static final String ENVIRONMENT_TYPE = "environmentType";
	public static final String AUTHORIZATION = "Authorization";

	public static final String TAG_URL = "/tag";
	public static final String EXISTS_URL = "/exists";
	public static final String UPLOAD_URL = "/upload";
	public static final String DELETE_URL = "/delete";
	public static final String DOWNLOAD_URL = "/download";
	public static final String MOVE_URL = "/move";

	public static final String TAG_DATA_TYPE = DATA_TYPE;
	public static final String TAG_FILE_NAME = FILENAME;

	public static final String UPLOAD_ACTION = "Trying to upload an Object with key [%s]";
	public static final String SUCCESSFULLY_UPLOADED = "Object with key [%s] successfully uploaded";
	public static final String UPLOAD_ERROR = "Upload unsuccessful for object with key [%s] with error message: [%s]";
	public static final String DELETE_ACTION = "Trying to delete Object with key [%s]";
	public static final String SUCCESSFULLY_DELETED = "Object with key [%s] successfully deleted";
	public static final String DELETE_ERROR = "Delete unsuccessful for object with key [%s] with error message: [%s]";
	public static final String DOWNLOAD_ACTION = "Trying to download an Object with key [%s]";
	public static final String SUCCESSFULLY_DOWNLOADED = "Object with key [%s] successfully downloaded";
	public static final String DOWNLOAD_ERROR = "Download unsuccessful for object with key [%s] with error message: [%s]";
	public static final String CHECK_EXISTENCE_ACTION = "Trying to check if Object with key [%s] exists";
	public static final String SUCCESSFULLY_CHECKED_EXISTENCE = "Checking Object with key [%s] existence action was proceed successfully";
	public static final String CHECK_EXISTENCE_ERROR = "Checking Object with key [%s] existence action was proceed unsuccessfully with error message: [%s]";
	public static final String MOVE_ACTION = "Trying to move Object with key [%s] to key [%s]";
	public static final String SUCCESSFULLY_MOVED = "Object successfully moved from key [%s] to key [%s]";

}
