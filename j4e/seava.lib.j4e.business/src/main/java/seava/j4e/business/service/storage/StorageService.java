package seava.j4e.business.service.storage;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.StorageException;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.storage.utils.Constants;
import seava.j4e.business.service.storage.utils.ExceptionChecker;

/**
 * @author abolindu
 */
public class StorageService {

	private static final Logger LOG = LoggerFactory.getLogger(StorageService.class);

	private static final String WEB_SEPARATOR = "/";

	private final String url;
	private final String username;
	private final String password;
	private final String tempPath;
	private final String authorization;
	private final String environmentType;

	public StorageService(String url, String username, String password, String tempPath, String environmentType) throws IOException {
		this.url = url;
		this.tempPath = tempPath;
		this.username = username;
		this.password = password;
		this.environmentType = environmentType;

		this.checkAndCreatePath(this.tempPath);
		this.authorization = this.encodeCredentials();
	}

	/**
	 * @throws IOException
	 */
	private void checkAndCreatePath(String pathString) throws IOException {
		Path path = Paths.get(pathString);
		if (!Files.exists(path)) {
			Files.createDirectories(path);
		}
	}

	/**
	 * @return string which represents the encoding of the username and the password
	 */
	private String encodeCredentials() {
		Base64 b = new Base64();
		return "Basic " + b.encodeAsString(new String(this.username + ":" + this.password).getBytes());
	}

	/**
	 * Downloads file for specified key.
	 *
	 * @param objectKey
	 * @return file content
	 * @throws StorageException which encapsulates the exception message
	 */
	public InputStream downloadFile(String objectKey) throws StorageException {
		LOG.info(String.format(Constants.DOWNLOAD_ACTION, objectKey));
		try {
			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(this.url + Constants.DOWNLOAD_URL);
			post.setHeader(Constants.AUTHORIZATION, this.authorization);

			List<NameValuePair> postParameters = new ArrayList<>();
			postParameters.add(new BasicNameValuePair(Constants.OBJECT_KEY, objectKey));
			postParameters.add(new BasicNameValuePair(Constants.ENVIRONMENT_TYPE, this.environmentType));
			postParameters.add(new BasicNameValuePair(Constants.BUCKET, Session.user.get().getClientId().toLowerCase()));
			post.setEntity(new UrlEncodedFormEntity(postParameters));

			HttpResponse response = client.execute(post);
			if (!HttpStatus.OK.toString().equals(String.valueOf(response.getStatusLine().getStatusCode()))) {
				throw new StorageException(ErrorCode.FILE_NOT_DOWNLOADED, "File with key " + objectKey + " not found!");
			}

			LOG.info(String.format(Constants.SUCCESSFULLY_DOWNLOADED, objectKey));
			return response.getEntity().getContent();
		} catch (Exception e) {
			LOG.error(String.format(Constants.DOWNLOAD_ERROR, objectKey, e.getMessage()), e);
			throw new StorageException(ErrorCode.FILE_NOT_DOWNLOADED, e.getMessage());
		}
	}

	/**
	 * Uploads file with specified key without needing the file name.
	 *
	 * @param dataStream
	 * @param objectKey
	 * @param dataType
	 * @throws StorageException which encapsulates the exception message
	 */
	public void uploadFile(InputStream dataStream, String objectKey, String dataType) throws StorageException {
		LOG.info(String.format(Constants.UPLOAD_ACTION, objectKey));
		try {
			File file = this.saveFileTemporary(dataStream, objectKey);

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(this.url + Constants.UPLOAD_URL);
			post.setHeader(Constants.AUTHORIZATION, this.authorization);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.addBinaryBody(Constants.FILE, file);
			builder.addTextBody(Constants.DATA_TYPE, dataType);
			builder.addTextBody(Constants.OBJECT_KEY, objectKey);
			builder.addTextBody(Constants.ENVIRONMENT_TYPE, this.environmentType);
			builder.addTextBody(Constants.BUCKET, Session.user.get().getClientId().toLowerCase());
			HttpEntity multipart = builder.build();
			post.setEntity(multipart);

			HttpResponse response = client.execute(post);
			file.delete();

			this.checkResponse(response);
			LOG.info(String.format(Constants.SUCCESSFULLY_UPLOADED, objectKey));
		} catch (Exception e) {
			LOG.error(String.format(Constants.UPLOAD_ERROR, objectKey, e.getMessage()), e);
			throw new StorageException(ErrorCode.FILE_NOT_UPLOADED, e.getMessage());
		}
	}

	/**
	 * Uploads file with specified key without needing the file name.
	 *
	 * @param byteMessage
	 * @param objectKey
	 * @param dataType
	 * @throws StorageException which encapsulates the exception message
	 */
	public void uploadFile(byte[] byteMessage, String objectKey, String dataType) throws StorageException {
		this.uploadFile(new ByteArrayInputStream(byteMessage), objectKey, dataType);
	}

	/**
	 * Uploads file with specified key with file name.
	 *
	 * @param dataStream
	 * @param objectKey
	 * @param fileName
	 * @param dataType
	 * @throws StorageException which encapsulates the exception message
	 */
	public void uploadFile(InputStream dataStream, String objectKey, String fileName, String dataType) throws StorageException {
		LOG.info(String.format(Constants.UPLOAD_ACTION, objectKey));
		try {
			File file = this.saveFileTemporary(dataStream, objectKey);
			fileName = this.configureFileName(fileName);

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(this.url + Constants.UPLOAD_URL);
			post.setHeader(Constants.AUTHORIZATION, this.authorization);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.addBinaryBody(Constants.FILE, file);
			builder.addTextBody(Constants.DATA_TYPE, dataType);
			builder.addTextBody(Constants.FILENAME, fileName);
			builder.addTextBody(Constants.OBJECT_KEY, objectKey);
			builder.addTextBody(Constants.ENVIRONMENT_TYPE, this.environmentType);
			builder.addTextBody(Constants.BUCKET, Session.user.get().getClientId().toLowerCase());
			HttpEntity multipart = builder.build();

			post.setEntity(multipart);

			HttpResponse response = client.execute(post);
			file.delete();

			this.checkResponse(response);
			LOG.info(String.format(Constants.SUCCESSFULLY_UPLOADED, objectKey));
		} catch (Exception e) {
			LOG.error(String.format(Constants.UPLOAD_ERROR, objectKey, e.getMessage()), e);
			throw new StorageException(ErrorCode.FILE_NOT_UPLOADED, e.getMessage());
		}
	}

	/**
	 * Uploads file with specified key with file name.
	 *
	 * @param byteMessage
	 * @param objectKey
	 * @param fileName
	 * @param dataType
	 * @throws StorageException which encapsulates the exception message
	 */
	public void uploadFile(byte[] byteMessage, String objectKey, String fileName, String dataType) throws StorageException {
		this.uploadFile(new ByteArrayInputStream(byteMessage), objectKey, fileName, dataType);
	}

	/**
	 * Deletes file with specified key with file name.
	 *
	 * @param objectKey
	 * @throws StorageException which encapsulates the exception message
	 */
	public void deleteFile(String objectKey) throws StorageException {
		LOG.info(String.format(Constants.DELETE_ACTION, objectKey));
		try {
			String urlPath = new StringBuilder(this.url).append(Constants.DELETE_URL).append(WEB_SEPARATOR).append(this.environmentType)
					.append(WEB_SEPARATOR).append(Session.user.get().getClientId().toLowerCase()).append(WEB_SEPARATOR).append(objectKey).toString();
			HttpClient client = HttpClients.createDefault();
			HttpDelete delete = new HttpDelete(urlPath);
			delete.setHeader(Constants.AUTHORIZATION, this.authorization);
			client.execute(delete);
			LOG.info(String.format(Constants.SUCCESSFULLY_DELETED, objectKey));
		} catch (IOException e) {
			LOG.error(String.format(Constants.DELETE_ERROR, objectKey, e.getMessage()), e);
			throw new StorageException(ErrorCode.FILE_NOT_DELETED, e.getMessage());
		}
	}

	/**
	 * Checks the existence of the file with specified key with file name.
	 *
	 * @param objectKey
	 * @return
	 * @throws StorageException which encapsulates the exception message
	 */
	public boolean existsResource(String objectKey) throws StorageException {
		LOG.info(String.format(Constants.CHECK_EXISTENCE_ACTION, objectKey));
		boolean existsResource = false;
		try {
			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(this.url + Constants.EXISTS_URL);
			post.setHeader(Constants.AUTHORIZATION, this.authorization);

			List<NameValuePair> postParameters = new ArrayList<>();
			postParameters.add(new BasicNameValuePair(Constants.OBJECT_KEY, objectKey));
			postParameters.add(new BasicNameValuePair(Constants.BUCKET, Session.user.get().getClientId().toLowerCase()));
			postParameters.add(new BasicNameValuePair(Constants.ENVIRONMENT_TYPE, this.environmentType));
			post.setEntity(new UrlEncodedFormEntity(postParameters));

			HttpResponse response = client.execute(post);
			existsResource = this.getResponseMessage(response);
		} catch (Exception e) {
			LOG.error(String.format(Constants.CHECK_EXISTENCE_ERROR, objectKey, e.getMessage()), e);
			throw new StorageException(ErrorCode.STORAGE_EXCEPTION, e.getMessage());
		}
		LOG.info(String.format(Constants.SUCCESSFULLY_CHECKED_EXISTENCE, objectKey));
		return existsResource;
	}

	/**
	 * Retrieves the value of the <b>fileName</b> tag of the specified object.
	 *
	 * @param objectKey
	 * @return the value of the <b>fileName</b> tag
	 * @throws StorageException which encapsulates the exception message
	 */
	public String getFileName(String objectKey) throws StorageException {
		try {
			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(this.url + Constants.TAG_URL);
			post.setHeader(Constants.AUTHORIZATION, this.authorization);

			List<NameValuePair> postParameters = new ArrayList<>();
			postParameters.add(new BasicNameValuePair(Constants.OBJECT_KEY, objectKey));
			postParameters.add(new BasicNameValuePair(Constants.TAG_KEY, Constants.FILENAME));
			postParameters.add(new BasicNameValuePair(Constants.ENVIRONMENT_TYPE, this.environmentType));
			postParameters.add(new BasicNameValuePair(Constants.BUCKET, Session.user.get().getClientId().toLowerCase()));
			post.setEntity(new UrlEncodedFormEntity(postParameters));

			HttpResponse response = client.execute(post);
			String responseMessage = new BufferedReader(new InputStreamReader(response.getEntity().getContent())).readLine();
			return responseMessage;
		} catch (Exception e) {
			throw new StorageException(ErrorCode.STORAGE_EXCEPTION, e.getMessage(), e);
		}
	}

	/**
	 * Retrieves the value of the <b>dataType</b> tag of the specified object.
	 *
	 * @param objectKey
	 * @return the value of the <b>dataType</b> tag
	 * @throws StorageException which encapsulates the exception message
	 */
	public String getDataType(String objectKey) throws StorageException {
		try {
			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(this.url + Constants.TAG_URL);
			post.setHeader(Constants.AUTHORIZATION, this.authorization);

			List<NameValuePair> postParameters = new ArrayList<>();
			postParameters.add(new BasicNameValuePair(Constants.OBJECT_KEY, objectKey));
			postParameters.add(new BasicNameValuePair(Constants.TAG_KEY, Constants.DATA_TYPE));
			postParameters.add(new BasicNameValuePair(Constants.ENVIRONMENT_TYPE, this.environmentType));
			postParameters.add(new BasicNameValuePair(Constants.BUCKET, Session.user.get().getClientId().toLowerCase()));
			post.setEntity(new UrlEncodedFormEntity(postParameters));

			HttpResponse response = client.execute(post);
			String responseMessage = new BufferedReader(new InputStreamReader(response.getEntity().getContent())).readLine();
			return responseMessage;
		} catch (Exception e) {
			throw new StorageException(ErrorCode.STORAGE_EXCEPTION, e.getMessage(), e);
		}
	}

	/**
	 * Moves file from a key to other key.
	 *
	 * @param from - the old object key
	 * @param to - the new object key
	 * @throws StorageException which encapsulates the exception message
	 */
	public void moveFile(String from, String to) throws StorageException {
		try {
			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(this.url + Constants.MOVE_URL);
			post.setHeader(Constants.AUTHORIZATION, this.authorization);

			List<NameValuePair> postParameters = new ArrayList<>();
			postParameters.add(new BasicNameValuePair(Constants.TO_OBJECT_KEY, to));
			postParameters.add(new BasicNameValuePair(Constants.FROM_OBJECT_KEY, from));
			postParameters.add(new BasicNameValuePair(Constants.ENVIRONMENT_TYPE, this.environmentType));
			postParameters.add(new BasicNameValuePair(Constants.BUCKET, Session.user.get().getClientId().toLowerCase()));
			post.setEntity(new UrlEncodedFormEntity(postParameters));

			HttpResponse response = client.execute(post);
			this.checkResponse(response);
		} catch (Exception e) {
			throw new StorageException(ErrorCode.FILE_NOT_MOVED, e.getMessage(), e);
		}
	}

	/**
	 * Replaces the blank spaces from the file name with underline.
	 *
	 * @param fileName
	 * @return new file name which will be used in the storage
	 */
	private String configureFileName(String fileName) {
		if (fileName.contains(" ")) {
			fileName = fileName.replace(" ", "_");
		}
		return fileName;
	}

	/**
	 * Saves file temporary, so it can be sent further to the Storage Microservice.
	 *
	 * @param dataStream
	 * @param objectKey
	 * @return file saved locally
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private File saveFileTemporary(InputStream dataStream, String objectKey) throws IOException, FileNotFoundException {
		LOG.info("Trying to save file locally.");
		OutputStream outputStream = null;
		try {
			byte[] buffer = new byte[dataStream.available()];
			dataStream.read(buffer);
			File file = new File(this.tempPath + File.separator + objectKey);

			if (objectKey.contains(File.separator)) {
				String directories = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
				this.checkAndCreatePath(directories);
			}

			outputStream = new FileOutputStream(file);
			outputStream.write(buffer);
			LOG.info("Succesfully saved file locally");
			return file;
		} catch (Exception e) {
			LOG.error("Error while trying to save the file locally.", e);
			throw e;
		} finally {
			outputStream.close();
			dataStream.close();
		}
	}

	/**
	 * Checks if the response has <b>Success</b> value.
	 *
	 * @param response
	 * @throws IOException
	 * @throws StorageException
	 */
	private void checkResponse(HttpResponse response) throws IOException, StorageException {
		String responseMessage = new BufferedReader(new InputStreamReader(response.getEntity().getContent())).readLine();
		if (!HttpStatus.OK.toString().equals(responseMessage)) {
			String errorMessage = ExceptionChecker.customizeS3ErrorMessages(responseMessage);
			throw new StorageException(ErrorCode.FILE_NOT_UPLOADED, errorMessage);
		}
	}

	/**
	 * @param response
	 * @return boolean value which is sent from Storage Microservice
	 * @throws UnsupportedOperationException
	 * @throws IOException
	 */
	private boolean getResponseMessage(HttpResponse response) throws UnsupportedOperationException, IOException {
		String responseMessage = new BufferedReader(new InputStreamReader(response.getEntity().getContent())).readLine();
		return Boolean.valueOf(responseMessage);
	}
}
