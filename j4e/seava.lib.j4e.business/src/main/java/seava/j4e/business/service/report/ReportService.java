package seava.j4e.business.service.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import seava.j4e.api.Constants;
import seava.j4e.api.ISettings;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.model.AppNotification;
import seava.j4e.api.service.notification.IAppNotificationService;
import seava.j4e.api.service.report.IReportService;
import seava.j4e.api.session.Session;
import seava.j4e.domain.report.Category;
import seava.j4e.domain.report.NotificationAction;
import seava.j4e.domain.report.Priority;

/**
 * @author apetho
 */
public class ReportService extends seava.j4e.business.service.AbstractBusinessBaseService implements IReportService {

	private static final String NO_CODE = "no_code";
	private static final String DATA2 = "data";
	private static final String DEV_MODE = "devMode";
	private static final String REPO_ID = "repoId";
	private static final String RUNNING = "RUNNING";
	private static final String DYNAMIC_REPORT = "Dynamic report";
	private static final String TITLE = "title";

	private static final String SUCCESS = "SUCCESS";
	private static final String REPORT_CENTER_PROBLEM = "Report center problem!";
	private static final String REPORT_CENTER_CONNECTION_ABORTED = "The connection to report center was aborted";

	@Autowired
	private IAppNotificationService appNotifsrv;
	@Autowired
	private ISettings settings;

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);

	@Override
	public byte[] downloadReport(Integer id) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Starting download report.");
		}
		String reportCenterUrl = this.getSettings().getReportCenterUrl();
		reportCenterUrl = reportCenterUrl.substring(0, reportCenterUrl.lastIndexOf('/')).concat("/dld/exec/").concat(Integer.toString(id));
		HttpGet get = new HttpGet(reportCenterUrl);
		HttpResponse reportResponse = null;
		int retry = 0;
		try {
			do {
				reportResponse = this.getHttpClient().execute(get);
				if (retry < 4 && (reportResponse == null || reportResponse.getStatusLine().getStatusCode() == 404)) {
					retry++;
				}
			} while (retry < 4 && (reportResponse == null || reportResponse.getStatusLine().getStatusCode() == 404));
			if (reportResponse == null || reportResponse.getEntity() == null || reportResponse.getEntity().getContent() == null) {
				throw new BusinessException(J4eErrorCode.REPORT_GENERATOR_PROBLEM,
						String.format(J4eErrorCode.REPORT_GENERATOR_PROBLEM.getErrMsg(), REPORT_CENTER_PROBLEM));
			}
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("End download report.");
			}
			return this.inputStream2ByteArray(reportResponse.getEntity().getContent());
		} catch (IOException e) {
			LOGGER.warn("Communication error with report server. Retry: " + retry, e);
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, e);
		}
	}

	@Override
	public boolean isReady(Integer id) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Starting to verify if the report is ready.");
		}
		JSONObject respJson = this.reportStatus(id);

		LOGGER.info("The report status, for id:" + id + " is: " + respJson.toString());

		if (SUCCESS.equals(respJson.optString(Constants.STATUS, RUNNING))) {
			return true;
		}
		return false;
	}

	@Override
	public void sendNotification(Integer id, String fileName) throws BusinessException {
		JSONObject respJson = this.reportStatus(id);
		this.sendNotification(respJson, fileName);
	}

	@Override
	public boolean uploadReport(byte[] rptDesign, String fileName) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Upload started");
		}
		String reportCenterUrl = this.getSettings().getReportCenterUrl();
		reportCenterUrl = reportCenterUrl.substring(0, reportCenterUrl.lastIndexOf('/'))
				.concat("/upl/report/" + this.getReportRepository() + "?format=json");

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(reportCenterUrl);

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		builder.addBinaryBody("file", rptDesign, ContentType.DEFAULT_BINARY, fileName);
		builder.addTextBody("comm", "report", ContentType.DEFAULT_BINARY);
		//
		HttpEntity entity = builder.build();
		post.setEntity(entity);

		try {
			HttpResponse response = client.execute(post);
			if (response.getStatusLine().getStatusCode() == 200) {
				String respStr = this.inputStream2String(response.getEntity().getContent());
				LOGGER.info(respStr);
				return true;
			} else {
				throw new BusinessException(ErrorCode.UPLOAD_UNSUCCESSFUL, ErrorCode.UPLOAD_UNSUCCESSFUL.getErrMsg());
			}
		} catch (IOException e) {
			LOGGER.warn(REPORT_CENTER_CONNECTION_ABORTED, e);
			throw new BusinessException(ErrorCode.UPLOAD_UNSUCCESSFUL, ErrorCode.UPLOAD_UNSUCCESSFUL.getErrMsg());
		}
	}

	@Override
	public Integer startReport(String data) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Report generation started");
		}

		String reportCenterUrl = this.getSettings().getReportCenterUrl();
		reportCenterUrl = reportCenterUrl.substring(0, reportCenterUrl.lastIndexOf('/')).concat("/runReport");

		JSONObject callJson = new JSONObject();
		callJson.put(REPO_ID, this.getReportRepository());
		callJson.put(DEV_MODE, true);
		callJson.put(DATA2, data);

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(reportCenterUrl);

		StringEntity requestEntity = new StringEntity(callJson.toString(), ContentType.APPLICATION_JSON);
		post.setEntity(requestEntity);

		try {
			HttpResponse response = client.execute(post);
			if (response.getStatusLine().getStatusCode() == 200) {
				JSONObject respJson = this.inputStream2Json(response.getEntity().getContent());
				LOGGER.info(respJson.toString());
				return respJson.optInt("id", -1);
			} else {
				throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, ErrorCode.COMMUNICATION_ERROR.getErrMsg());
			}
		} catch (IOException e) {
			LOGGER.warn(REPORT_CENTER_CONNECTION_ABORTED, e);
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, ErrorCode.COMMUNICATION_ERROR.getErrMsg());
		}
	}

	@Override
	public byte[] downloadRPTDesign(String fileName) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Starting download rtp design");
		}
		String reportCenterUrl = this.getSettings().getReportCenterUrl();
		reportCenterUrl = reportCenterUrl.substring(0, reportCenterUrl.lastIndexOf('/'))
				.concat("/dld/report/" + this.getReportRepository() + "/" + fileName);

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(reportCenterUrl);
		try {
			HttpResponse response = client.execute(get);
			if (response == null || response.getEntity() == null || response.getEntity().getContent() == null) {
				throw new BusinessException(J4eErrorCode.REPORT_GENERATOR_PROBLEM,
						String.format(J4eErrorCode.REPORT_GENERATOR_PROBLEM.getErrMsg(), REPORT_CENTER_PROBLEM));
			}
			return this.inputStream2ByteArray(response.getEntity().getContent());
		} catch (IOException e) {
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, e);
		}
	}

	@Override
	public String askForReports() throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Starting ask uploaded reports");
		}
		String reportCenterUrl = this.getSettings().getReportCenterUrl();
		reportCenterUrl = reportCenterUrl.substring(0, reportCenterUrl.lastIndexOf('/')).concat("/reports?repo=" + this.getReportRepository());

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(reportCenterUrl);
		try {
			HttpResponse response = client.execute(get);
			if (response == null || response.getEntity() == null || response.getEntity().getContent() == null) {
				throw new BusinessException(J4eErrorCode.REPORT_GENERATOR_PROBLEM,
						String.format(J4eErrorCode.REPORT_GENERATOR_PROBLEM.getErrMsg(), REPORT_CENTER_PROBLEM));
			}

			String respone = this.inputStream2String(response.getEntity().getContent());

			LOGGER.info("The uploaded reports are: " + respone);

			return respone;
		} catch (IOException e) {
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, e);
		}
	}

	/**
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public String getReportStatus(Integer id) throws BusinessException {
		JSONObject respJson = this.reportStatus(id);
		LOGGER.info("The report status, for id:" + id + " is: " + respJson.toString());
		return respJson.optString(Constants.STATUS, RUNNING);
	}

	@Override
	public void generateDialogReport(String dataXML) throws BusinessException {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("  --> report request data: {} ", new Object[] { dataXML });
		}
		JSONObject entityJson = new JSONObject();
		entityJson.put(DATA2, dataXML);
		entityJson.put(DEV_MODE, true);
		entityJson.put(REPO_ID, this.getReportRepository());

		String reportCenterUrl = this.getSettings().getReportCenterUrl();
		String url = reportCenterUrl + "runReport";

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		StringEntity requestEntity = new StringEntity(entityJson.toString(), ContentType.APPLICATION_JSON);
		post.setEntity(requestEntity);
		Integer reportId = -1;
		try {
			HttpResponse response = client.execute(post);
			if (response.getStatusLine().getStatusCode() == 200) {
				JSONObject respJson = this.inputStream2Json(response.getEntity().getContent());
				LOGGER.info(respJson.toString());
				reportId = respJson.optInt("id", -1);
			} else {
				throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, ErrorCode.COMMUNICATION_ERROR.getErrMsg());
			}
		} catch (IOException e) {
			LOGGER.warn(REPORT_CENTER_CONNECTION_ABORTED, e);
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, ErrorCode.COMMUNICATION_ERROR.getErrMsg());
		}
		if (reportId != -1) {
			this.startDownloader(reportId, null);
		}
	}

	/**
	 * @param respJson
	 */
	private void sendNotification(JSONObject respJson, String clientId, String loginName, String fileName) {
		String description;
		String category;
		String priority;
		String reportName = respJson.optString(TITLE, DYNAMIC_REPORT);
		Integer reportId = respJson.optInt("id");
		String reportCode = respJson.optString("code");
		String fileExt = respJson.optString("fileExt").replace(".", "");
		String link = "";
		String methodName = "";
		String methodParam = "";
		String finalFileName = fileName;
		if (StringUtils.isEmpty(finalFileName)) {
			finalFileName = reportCode.equalsIgnoreCase(NO_CODE) ? reportName : reportCode;
		}
		if (SUCCESS.equals(respJson.optString(Constants.STATUS, RUNNING))) {
			category = Category._PROCESS_.getName();
			description = this.buildDescription(reportId, finalFileName, fileExt);
			priority = Priority._DEFAULT_.getName();
			link = this.buildLink(reportId, finalFileName, fileExt);
			methodName = "downloadZip";
			methodParam = "'" + this.buildLink(reportId, finalFileName, fileExt) + "'";
		} else {
			category = Category._ERROR_.getName();
			description = String.format(J4eErrorCode.INTERFACE_NOTIFICATION_FAILURE.getErrMsg(), finalFileName);
			priority = Priority._MAX_.getName();
			reportName = String.format(J4eErrorCode.INTERFACE_NOTIFICATION_FAILURE.getErrMsg(),
					"null".equalsIgnoreCase(reportName) ? reportCode : reportName);
		}
		AppNotification appNotification = new AppNotification(1, reportName, description, clientId);
		appNotification.setAction(NotificationAction._NOACTION_.getName());
		appNotification.setCategory(category);
		appNotification.setEventDate(new Date());
		appNotification.setLifeTime(new Date());
		appNotification.setLink(link);
		appNotification.setPriority(priority);
		appNotification.addToUsers(loginName);
		appNotification.setMethodName(methodName);
		appNotification.setMethodParam(methodParam);
		this.appNotifsrv.add(appNotification);
	}

	private void sendNotification(JSONObject respJson, String fileName) {
		String clientId = Session.user.get().getClientId();
		String loginName = Session.user.get().getLoginName();
		this.sendNotification(respJson, clientId, loginName, fileName);
	}

	private String buildLink(Integer id, String fileName, String fileExt) {
		String externalScheme = this.settings.get(Constants.PROP_SYS_EXTERNAL_SCHEME);
		String externalServerName = this.settings.get(Constants.PROP_SYS_EXTERNAL_SERVER_NAME);
		String externalServerPort = this.settings.get(Constants.PROP_SYS_EXTERNAL_SERVER_PORT);
		String externalContextPath = this.settings.get(Constants.PROP_SYS_EXTERNAL_CONTEXT_PATH);
		String url = new StringBuilder(externalScheme).append("://").append(externalServerName).append(":").append(externalServerPort)
				.append(externalContextPath).toString();
		return url + "/download/" + id + "/" + fileExt + "/" + fileName;
	}

	/**
	 * @param address
	 * @return
	 */
	private String buildDescription(Integer id, String code, String fileExt) {
		StringBuilder description = new StringBuilder();
		description.append("<a target=\"_blank\" href=");
		description.append("download/" + id + "/" + fileExt + "/" + code.replace(" ", "_"));
		description.append(">");
		description.append(J4eErrorCode.REPORT_GENERATOR_SUCCESS.getErrMsg());
		description.append("</a>");
		return description.toString();
	}

	private JSONObject reportStatus(Integer id) throws BusinessException {
		String reportCenterUrl = this.getSettings().getReportCenterUrl();
		reportCenterUrl = reportCenterUrl.substring(0, reportCenterUrl.lastIndexOf('/')).concat("/reportExecutions(").concat(Integer.toString(id))
				.concat(")?$format=json");
		HttpGet post = new HttpGet(reportCenterUrl);
		HttpResponse reportResponse = null;
		try {
			reportResponse = this.getHttpClient().execute(post);
		} catch (IOException e) {
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, e);
		}
		if (reportResponse == null || reportResponse.getStatusLine().getStatusCode() != 200) {
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, "Communication Error");
		}
		try {
			JSONObject respJson = this.inputStream2Json(reportResponse.getEntity().getContent());
			LOGGER.info(respJson.toString());
			return respJson;
		} catch (IllegalStateException | IOException e) {
			throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, e);
		}
	}

	private JSONObject inputStream2Json(InputStream is) {
		String jsonString = this.inputStream2String(is);
		return new JSONObject(jsonString);
	}

	@SuppressWarnings("resource")
	private String inputStream2String(InputStream is) {
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	/**
	 * Build http client.
	 *
	 * @return
	 */
	private HttpClient getHttpClient() {
		return HttpClientBuilder.create().useSystemProperties().build();
	}

	/**
	 * Convert input stream to byte array.
	 *
	 * @param is
	 * @return
	 * @throws BusinessException
	 */
	private byte[] inputStream2ByteArray(InputStream is) throws BusinessException {
		try {
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[16384];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			buffer.flush();
			return buffer.toByteArray();
		} catch (IOException e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, e);
		}
	}

	private String getReportRepository() {
		return this.settings.get(Constants.PROP_REPORT_CENTER_REPO);
	}

	/**
	 * @param id
	 * @throws BusinessException
	 */
	public void startDownloader(Integer id, String fileName) throws BusinessException {
		int counter = 0;
		try {
			String reportStatus = this.getReportStatus(id);
			boolean isRunning = RUNNING.equalsIgnoreCase(reportStatus);
			while (isRunning && counter++ < 25) {
				Thread.sleep(10 ^ counter * 100);
				reportStatus = this.getReportStatus(id);
				isRunning = RUNNING.equalsIgnoreCase(reportStatus);
			}
			JSONObject respJson = this.reportStatus(id);
			this.sendNotification(respJson, fileName);
		} catch (InterruptedException e) {
			LOGGER.warn("Report download problem", e);
			Thread.currentThread().interrupt();
		}
	}
}
