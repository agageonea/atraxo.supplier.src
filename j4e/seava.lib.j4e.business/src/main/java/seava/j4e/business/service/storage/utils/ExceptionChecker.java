package seava.j4e.business.service.storage.utils;

import seava.j4e.api.exceptions.InvalidEnumException;

public class ExceptionChecker {

	enum ExceptionCodes {
		INVALID_TAG("InvalidTag", "File name contains invalid characters. <br> Please rename the file and try again.");

		private String code;
		private String message;

		public String getCode() {
			return this.code;
		}

		public String getMessage() {
			return this.message;
		}

		public static ExceptionCodes getByCode(String code) {
			for (ExceptionCodes s3Code : values()) {
				if (s3Code.getCode().equalsIgnoreCase(code)) {
					return s3Code;
				}
			}
			throw new InvalidEnumException("Inexistent ExceptionCodes with code: " + code);
		}

		private ExceptionCodes(String code, String message) {
			this.code = code;
			this.message = message;
		}
	}

	public static String customizeS3ErrorMessages(String message) {
		try {
			return ExceptionCodes.getByCode(message).getMessage();
		} catch (InvalidEnumException e) {
			// do nothing
		}
		return message;
	}
}
