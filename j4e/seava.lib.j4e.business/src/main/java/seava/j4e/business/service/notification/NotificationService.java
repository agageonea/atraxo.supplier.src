package seava.j4e.business.service.notification;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.async.DeferredResult;

import seava.j4e.api.model.AppNotification;
import seava.j4e.api.service.notification.IAppNotificationService;

/**
 * @author zspeter
 */
public class NotificationService implements IAppNotificationService {

	private class ClientResult {
		private String clientId;
		private String userId;
		private DeferredResult<AppNotification> result;

		public ClientResult(String clientId, String userId, DeferredResult<AppNotification> result) {
			this.clientId = clientId;
			this.userId = userId;
			this.result = result;
		}

		public String getClientId() {
			return this.clientId;
		}

		public String getUserId() {
			return this.userId;
		}

		public DeferredResult<AppNotification> getResult() {
			return this.result;
		}

	}

	private static final Logger LOG = LoggerFactory.getLogger(NotificationService.class);

	private final BlockingQueue<ClientResult> resultQueue = new LinkedBlockingQueue<>();

	@Override
	public void getUpdate(String clientId, String userId, DeferredResult<AppNotification> result) {
		final ClientResult clientResult = new ClientResult(clientId, userId, result);
		result.onCompletion(new Runnable() {
			@Override
			public void run() {
				LOG.info("Result removed from queue on completion.");
				NotificationService.this.resultQueue.remove(clientResult);
			}
		});

		result.onTimeout(new Runnable() {
			@Override
			public void run() {
				LOG.info("Result removed from queue on timeout.");
				NotificationService.this.resultQueue.remove(clientResult);
			}
		});

		this.resultQueue.add(clientResult);
		LOG.info("Result added to queue.");
	}

	@Override
	public void add(AppNotification e) {
		if (LOG.isDebugEnabled()) {
			LOG.info("Notification added on result.");
		}
		synchronized (this) {
			for (ClientResult result : this.resultQueue) {
				if (result.getClientId().equals(e.getClientId())
						&& (CollectionUtils.isEmpty(e.getUsers()) || e.getUsers().contains(result.getUserId()))) {
					result.getResult().setResult(e);
				}
			}
		}
	}
}
