package seava.j4e.business.message;

import java.util.Map;

import seava.j4e.api.model.IMessageData;

/**
 * MessageData
 */
public class MessageData implements IMessageData {

	/**
	 * A key identifying the source of the event. Usually it is a class name, but can be whatever string.
	 */
	private String source;

	/**
	 * An optional information regarding the source of the event, specifying the action which triggered this event. For example insert or update.
	 */
	private String action;

	/**
	 * Any other information which may be useful for the listeners, for example a list of ID's.
	 */
	private Map<String, Object> data;

	/**
	 * @param source
	 */
	public MessageData(String source) {
		super();
		this.source = source;
	}

	/**
	 * @param source
	 * @param action
	 */
	public MessageData(String source, String action) {
		super();
		this.source = source;
		this.action = action;
	}

	/**
	 * @param source
	 * @param action
	 * @param data
	 */
	public MessageData(String source, String action, Map<String, Object> data) {
		super();
		this.source = source;
		this.action = action;
		this.data = data;
	}

	@Override
	public String getSource() {
		return this.source;
	}

	@Override
	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public String getAction() {
		return this.action;
	}

	@Override
	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public Map<String, Object> getData() {
		return this.data;
	}

	@Override
	public void setData(Map<String, Object> data) {
		this.data = data;
	}

}
