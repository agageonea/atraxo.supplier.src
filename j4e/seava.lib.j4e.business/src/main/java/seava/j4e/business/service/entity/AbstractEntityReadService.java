/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.business.service.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.Assert;

import seava.j4e.api.exceptions.ApplicationException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.J4eErrorCode;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithSubsidiaryId;
import seava.j4e.api.session.Session;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * Implements the read actions as various finders for an entity-service. See the super-classes for more details.
 *
 * @author amathe
 * @param <E>
 */
public abstract class AbstractEntityReadService<E> extends AbstractBusinessBaseService {

	public abstract Class<E> getEntityClass();

	/**
	 * Create a new entity instance
	 *
	 * @return
	 * @throws BusinessException
	 */
	public E create() throws BusinessException {
		try {
			return this.getEntityClass().newInstance();
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.G_RUNTIME_ERROR, "Cannot create a new instance of " + this.getEntityClass().getCanonicalName(), e);
		}
	}

	/**
	 * Find entity by id.
	 *
	 * @param object
	 * @return
	 */
	public E findById(Object object) {
		return this.getEntityManager().find(this.getEntityClass(), object);
	}

	/**
	 * Find an entity of the specified class by its ID
	 *
	 * @param object
	 * @param claz
	 * @return
	 */
	public <T> T findById(Object object, Class<T> claz) {
		return this.getEntityManager().find(claz, object);
	}

	/**
	 * Find entities by the list of ids..
	 *
	 * @param ids
	 * @return
	 */
	public List<E> findByIds(List<Object> ids) {
		return this.findByIds(ids, this.getEntityClass());
	}

	/**
	 * Find entities of the specified class by the list of ids.
	 *
	 * @param ids
	 * @param claz
	 * @return
	 */
	public <T> List<T> findByIds(List<Object> ids, Class<T> claz) {
		if (IModelWithClientId.class.isAssignableFrom(claz)) {
			if (IModelWithSubsidiaryId.class.isAssignableFrom(claz)) {
				return this.getEntityManager()
						.createQuery("select e from " + claz.getSimpleName()
								+ " e where e.clientId = :clientId and e.id in :ids and e.subsidiaryId in :subsidiaryIds", claz)
						.setParameter("subsidiaryIds", Session.user.get().getProfile().getOrganizationIds()).setParameter("ids", ids)
						.setParameter("clientId", Session.user.get().getClient().getId()).getResultList();

			} else {
				return this.getEntityManager()
						.createQuery("select e from " + claz.getSimpleName() + " e where e.clientId = :clientId and e.id in :ids ", claz)
						.setParameter("ids", ids).setParameter("clientId", Session.user.get().getClient().getId()).getResultList();
			}
		} else {
			return this.getEntityManager().createQuery("select e from " + claz.getSimpleName() + " e where e.id in :ids ", claz)
					.setParameter("ids", ids).getResultList();
		}
	}

	/**
	 * Find entities by the list of reference ids
	 *
	 * @param refids
	 * @return
	 */
	public List<E> findByRefids(List<Object> refids) {
		return this.findByRefids(refids, this.getEntityClass());

	}

	/**
	 * Find entities of the specified class by the list of reference ids
	 *
	 * @param refids
	 * @param claz
	 * @return
	 */
	public <T> List<T> findByRefids(List<Object> refids, Class<T> claz) {
		if (IModelWithClientId.class.isAssignableFrom(claz)) {
			if (IModelWithSubsidiaryId.class.isAssignableFrom(claz)) {
				return this.getEntityManager()
						.createQuery("select e from " + claz.getSimpleName()
								+ " e where e.clientId = :clientId and e.refid in :refids and e.subsidiaryId in :subsidiaryIds", claz)
						.setParameter("subsidiaryIds", Session.user.get().getProfile().getOrganizationIds())
						.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("refids", refids).getResultList();
			} else {
				return this.getEntityManager()
						.createQuery("select e from " + claz.getSimpleName() + " e where e.clientId = :clientId and e.refid in :refids ", claz)
						.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("refids", refids).getResultList();
			}
		} else {
			return this.getEntityManager().createQuery("select e from " + claz.getSimpleName() + " e where e.refid in :refids ", claz)
					.setParameter("refids", refids).getResultList();
		}
	}

	/**
	 * Find an entity by its reference-id
	 *
	 * @param refid
	 * @return
	 */
	public E findByRefid(Object refid) {
		return this.findByRefid(refid, this.getEntityClass());
	}

	/**
	 * Find an entity of the specified class by its reference-id
	 *
	 * @param refid
	 * @param claz
	 * @return
	 */
	public <T> T findByRefid(Object refid, Class<T> claz) {
		if (IModelWithClientId.class.isAssignableFrom(claz)) {
			if (IModelWithSubsidiaryId.class.isAssignableFrom(claz)) {
				return this.getEntityManager()
						.createQuery("select e from " + this.getEntityClass().getSimpleName()
								+ " e where  e.clientId = :clientId and e.subsidiaryId in :subsidiaryIds and e.refid = :refid ", claz)
						.setParameter("subsidiaryIds", Session.user.get().getProfile().getOrganizationIds())
						.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("refid", refid).getResultList().get(0);

			} else {
				return this.getEntityManager()
						.createQuery(
								"select e from " + this.getEntityClass().getSimpleName() + " e where  e.clientId = :clientId and e.refid = :refid ",
								claz)
						.setParameter("clientId", Session.user.get().getClient().getId()).setParameter("refid", refid).getResultList().get(0);
			}
		} else {
			return this.getEntityManager().createQuery("select e from " + this.getEntityClass().getSimpleName() + " e where e.refid = :refid ", claz)
					.setParameter("refid", refid).getResultList().get(0);
		}
	}

	/**
	 * Find an entity by unique-key name.
	 *
	 * @param namedQueryName
	 * @param params
	 * @return
	 * @throws BusinessException
	 */
	public E findByUk(String namedQueryName, Map<String, Object> params) {
		return this.findByUk(namedQueryName, params, this.getEntityClass());
	}

	/**
	 * Find an entity of the specified class by its unique-key name.
	 *
	 * @param namedQueryName
	 * @param params
	 * @param claz
	 * @return
	 * @throws BusinessException
	 */
	public <T> T findByUk(String namedQueryName, Map<String, Object> params, Class<T> claz) {
		TypedQuery<T> q = this.getEntityManager().createNamedQuery(namedQueryName, claz);
		Set<String> keys = params.keySet();
		if (IModelWithClientId.class.isAssignableFrom(claz)) {
			if (IModelWithSubsidiaryId.class.isAssignableFrom(claz)) {
				q.setParameter("subsidiaryIds", Session.user.get().getProfile().getOrganizationIds());
			}
			q.setParameter("clientId", Session.user.get().getClient().getId());
		}
		for (String key : keys) {
			q.setParameter(key, params.get(key));
		}
		try {
			return q.getSingleResult();
		} catch (NoResultException nre) {
			StringBuilder sb = new StringBuilder();
			params.entrySet().forEach(e -> sb.append(e.getKey()).append(" = ").append(e.getValue()).append(", "));
			sb.delete(sb.lastIndexOf(","), sb.length());
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT, String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(), claz.getSimpleName(), sb),
					nre);
		} catch (NonUniqueResultException nure) {
			StringBuilder sb = new StringBuilder();
			params.entrySet().forEach(e -> sb.append(e.getKey()).append(" = ").append(e.getValue()).append(", "));
			sb.delete(sb.lastIndexOf(","), sb.length());
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(), claz.getSimpleName(), params), nure);
		}
	}

	/**
	 * Find an <E> entity using as filter criteria the specified list of attribute values. The filter criteria must uniquely identify an entity.
	 *
	 * @param params
	 * @return
	 * @throws BusinessException
	 */
	public E findEntityByAttributes(Map<String, Object> params) {
		return this.findEntityByAttributes(this.getEntityClass(), params);
	}

	/**
	 * Find an entity of the given type, using as filter criteria the specified list of attribute values. The filter criteria must uniquely identify
	 * an entity.
	 *
	 * @param entityClass
	 * @param params
	 * @param maxResults
	 * @return
	 * @throws BusinessException
	 */
	public <T> T findEntityByAttributes(Class<T> entityClass, Map<String, Object> params) {
		CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(entityClass);
		Root<T> root = cq.from(entityClass);
		cq.select(root);
		Assert.notNull(params);
		List<Predicate> predicateList = new ArrayList<>();
		List<Predicate> orPredicateList = new ArrayList<>();
		if (IModelWithClientId.class.isAssignableFrom(entityClass)) {
			if (IModelWithSubsidiaryId.class.isAssignableFrom(entityClass)) {
				predicateList.add(root.get("subsidiaryId").in(Session.user.get().getProfile().getOrganizationIds()));
			}
			predicateList.add(cb.equal(root.get("clientId"), Session.user.get().getClient().getId()));
		}
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			predicateList.add(cb.equal(root.get(entry.getKey()), entry.getValue()));
		}
		Predicate p1 = cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
		Predicate p2 = cb.or(orPredicateList.toArray(new Predicate[orPredicateList.size()]));
		cq.where(cb.and(p1, p2));
		TypedQuery<T> query = this.getEntityManager().createQuery(cq);
		try {
			return query.getSingleResult();
		} catch (NoResultException nre) {
			StringBuilder sb = new StringBuilder();
			params.entrySet().forEach(e -> sb.append(e.getKey()).append(" = ").append(e.getValue()).append(", "));
			sb.delete(sb.lastIndexOf(","), sb.length());
			throw new ApplicationException(J4eErrorCode.DB_NO_RESULT,
					String.format(J4eErrorCode.DB_NO_RESULT.getErrMsg(), entityClass.getSimpleName(), sb), nre);
		} catch (NonUniqueResultException nure) {
			StringBuilder sb = new StringBuilder();
			params.entrySet().forEach(e -> sb.append(e.getKey()).append(" = ").append(e.getValue()).append(", "));
			sb.delete(sb.lastIndexOf(","), sb.length());
			throw new ApplicationException(J4eErrorCode.DB_DUPLICATE_KEY_FOUND,
					String.format(J4eErrorCode.DB_DUPLICATE_KEY_FOUND.getErrMsg(), entityClass.getSimpleName(), params), nure);
		}
	}

	/**
	 * Find a list of <E> entities using as filter criteria the specified list of attribute values.
	 *
	 * @param params
	 * @return
	 * @throws BusinessException
	 */
	public List<E> findEntitiesByAttributes(Map<String, Object> params) {
		return this.findEntitiesByAttributes(this.getEntityClass(), params, -1);
	}

	/**
	 * Find a list of <E> entities using as filter criteria the specified list of attribute values.
	 *
	 * @param params
	 * @return
	 * @throws BusinessException
	 */
	public List<E> findEntitiesByAttributes(Map<String, Object> params, int maxResults) {
		return this.findEntitiesByAttributes(this.getEntityClass(), params, maxResults);
	}

	/**
	 * Find a list of <E> entities using as filter criteria the specified list of attribute values.
	 *
	 * @param entityClass
	 * @param params
	 * @return
	 * @throws BusinessException
	 */
	public <T> List<T> findEntitiesByAttributes(Class<T> entityClass, Map<String, Object> params) {
		return this.findEntitiesByAttributes(entityClass, params, -1);
	}

	/**
	 * Find a list of entities of the given type, using as filter criteria the specified list of attribute values.
	 *
	 * @param entityClass
	 * @param params
	 * @param maxResults - Set the maximum number of results to retrieve. Ignore variable in case of it is less or equal with 0;
	 * @return
	 * @throws BusinessException
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> findEntitiesByAttributes(Class<T> entityClass, Map<String, Object> params, int maxResults) {
		CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(entityClass);
		Root<T> root = cq.from(entityClass);
		cq.select(root);
		Assert.notNull(params);
		List<Predicate> predicateList = new ArrayList<>();
		List<Predicate> orPredicateList = new ArrayList<>();
		if (IModelWithClientId.class.isAssignableFrom(entityClass)) {
			if (IModelWithSubsidiaryId.class.isAssignableFrom(entityClass)) {
				predicateList.add(root.get("subsidiaryId").in(Session.user.get().getProfile().getOrganizationIds()));
			}
			predicateList.add(cb.equal(root.get("clientId"), Session.user.get().getClient().getId()));
		}
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			if (entry.getValue() instanceof Collection<?>) {
				Collection<?> entryList = (List<Object>) entry.getValue();
				predicateList.add(root.get(entry.getKey()).in(entryList));
			} else {
				predicateList.add(cb.equal(root.get(entry.getKey()), entry.getValue()));
			}
		}
		Predicate p1 = cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
		Predicate p2 = cb.or(orPredicateList.toArray(new Predicate[orPredicateList.size()]));
		cq.where(cb.and(p1, p2));
		TypedQuery<T> query = this.getEntityManager().createQuery(cq);
		if (maxResults > 0) {
			query.setMaxResults(maxResults);
		}
		return query.getResultList();
	}

}
