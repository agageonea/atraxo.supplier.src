package seava.j4e.business.bpm;

import javax.annotation.PostConstruct;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Wrapper over <code>ProcessEngine</code> and its services
 *
 * @author vhojda
 */
public class ActivitiBpmService {

	@Autowired
	@Qualifier("processEngine")
	private ProcessEngine processEngine;

	@Autowired
	@Qualifier("repositoryService")
	private RepositoryService repositoryService;

	@Autowired
	@Qualifier("runtimeService")
	private RuntimeService runtimeService;

	@Autowired
	@Qualifier("taskService")
	private TaskService taskService;

	@Autowired
	@Qualifier("historyService")
	private HistoryService historyService;

	@Autowired
	@Qualifier("managementService")
	private ManagementService managementService;

	@PostConstruct
	public void init() {

	}

	public ProcessEngine getProcessEngine() {
		return this.processEngine;
	}

	public void setProcessEngine(ProcessEngine processEngine) {
		this.processEngine = processEngine;
	}

	public RepositoryService getRepositoryService() {
		return this.repositoryService;
	}

	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}

	public RuntimeService getRuntimeService() {
		return this.runtimeService;
	}

	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}

	public TaskService getTaskService() {
		return this.taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public HistoryService getHistoryService() {
		return this.historyService;
	}

	public void setHistoryService(HistoryService historyService) {
		this.historyService = historyService;
	}

	public ManagementService getManagementService() {
		return this.managementService;
	}

	public void setManagementService(ManagementService managementService) {
		this.managementService = managementService;
	}

}
