package seava.j4e.business.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DateUtils
 */
public class DateUtils {

	static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

	private DateUtils() {
		throw new IllegalAccessError("Utility class");
	}

	/**
	 * Modify <code>date</code> by adding <code>amount</code> to a calendar field specified by <code>calFields</code>. Example: date: 2014-08-09
	 * calField: Calendar.MONTH amount: 2 return: 2014-10-09;
	 *
	 * @param date Input date.
	 * @param calField the calendar field.
	 * @param amount amount.
	 * @return
	 */
	public static Date modifyDate(Date date, int calField, int amount) {
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(date);
		cal.add(calField, amount);
		return cal.getTime();

	}

	public static Date getTodayWithoutTime() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
		XMLGregorianCalendar returnedDate = null;
		try {
			if (date != null) {
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(date);
				// returnedDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
				returnedDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1,
						cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND),
						DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);

			} else {
				returnedDate = DatatypeFactory.newInstance().newXMLGregorianCalendar();
			}
		} catch (DatatypeConfigurationException e) {
			logger.error("ERROR:could not cast to XMLGregorianCalendar the date " + date, e);
		}
		return returnedDate;
	}

	/**
	 * @param xmlCalendar
	 * @return
	 */
	public static Date fromXMLGregorianCalendar(XMLGregorianCalendar xmlCalendar) {
		Date date = xmlCalendar.toGregorianCalendar().getTime();
		return date;
	}

	public static Date removeTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
}
