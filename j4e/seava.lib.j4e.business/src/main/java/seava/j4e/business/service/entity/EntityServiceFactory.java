/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.business.service.entity;

import seava.j4e.api.service.business.IEntityService;
import seava.j4e.api.service.business.IEntityServiceFactory;
import seava.j4e.business.AbstractApplicationContextAware;

/**
 * EntityServiceFactory
 */
public class EntityServiceFactory extends AbstractApplicationContextAware implements IEntityServiceFactory {

	@Override
	public <E> IEntityService<E> create(String key) {
		@SuppressWarnings("unchecked")
		IEntityService<E> s = (IEntityService<E>) this.getApplicationContext().getBean(key);
		return s;
	}

	@Override
	public <E> IEntityService<E> create(Class<E> type) {
		@SuppressWarnings("unchecked")
		IEntityService<E> s = (IEntityService<E>) this.getApplicationContext().getBean(type);
		return s;
	}
}
