package seava.j4e.business.service.storage;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.business.service.AbstractBusinessBaseService;

public class WebDavSetupService extends AbstractBusinessBaseService {

	private static final Logger LOG = LoggerFactory.getLogger(WebDavSetupService.class);

	@Autowired
	private WebDavService webDavService;

	private String pathDir;

	public WebDavSetupService(String pathDir) {
		super();
		this.pathDir = pathDir;
	}

	@PostConstruct
	public void initialize() {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Initializing WebDav.");
				LOG.debug("Checking if base folders are created.");
			}
			this.webDavService.checkAndCreateFolders(this.pathDir);
		} catch (BusinessException e) {
			LOG.warn(e.getMessage(), e);
		}
	}

}
