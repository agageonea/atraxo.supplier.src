/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.business.service.setup;

import java.util.Iterator;
import java.util.List;

import seava.j4e.api.Constants;
import seava.j4e.api.setup.IInitDataProviderFactory;
import seava.j4e.api.setup.ISetupParticipant;
import seava.j4e.api.setup.ISetupTask;
import seava.j4e.business.service.AbstractBusinessBaseService;

/**
 * AbstractBusinessSetupParticipant
 */
public abstract class AbstractBusinessSetupParticipant extends AbstractBusinessBaseService {

	protected String targetName;
	protected List<ISetupTask> tasks;
	protected int ranking;

	/**
	 * @return
	 */
	public boolean hasWorkToDo() {
		if (this.tasks == null) {
			this.init();
		}
		return !this.tasks.isEmpty();
	}

	protected abstract void init();

	public List<ISetupTask> getTasks() {
		if (this.tasks == null) {
			this.init();
		}
		return this.tasks;
	}

	public String getTargetName() {
		return this.targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public String getBundleId() {
		return this.getApplicationContext().getId();
	}

	/**
	 * @param taskId
	 * @return
	 */
	public ISetupTask getTask(String taskId) {
		Iterator<ISetupTask> it = this.tasks.iterator();
		while (it.hasNext()) {
			ISetupTask t = it.next();
			if (t.getId().equals(taskId)) {
				return t;
			}
		}
		return null;
	}

	protected void beforeExecute() throws Exception {

	}

	/**
	 * @throws Exception
	 */
	public void execute() throws Exception {
		this.beforeExecute();
		this.onExecute();
		this.afterExecute();
	}

	protected void afterExecute() throws Exception {
	}

	protected abstract void onExecute() throws Exception;

	public int getRanking() {
		return this.ranking;
	}

	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	/**
	 * @param sp
	 * @return
	 */
	public int compareTo(ISetupParticipant sp) {
		return sp.getRanking() - this.ranking;
	}

	@SuppressWarnings("unchecked")
	protected List<IInitDataProviderFactory> getDataProviderFactories() {
		return (List<IInitDataProviderFactory>) this.getApplicationContext().getBean(Constants.SPRING_OSGI_INIT_DATA_PROVIDER_FACTORIES);
	}

}
