/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.business.service.entity;

import seava.j4e.api.service.business.IEntityService;

/**
 * DefaultEntityService
 *
 * @param <E>
 */
public class DefaultEntityService<E> extends AbstractEntityService<E> implements IEntityService<E> {

	private Class<E> entityClass;

	/**
	 * @param entityClass
	 */
	public DefaultEntityService(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * @param entityClass
	 * @return
	 */
	public static <E> DefaultEntityService<E> createService(Class<E> entityClass) {
		return new DefaultEntityService<E>(entityClass);
	}

	@Override
	public Class<E> getEntityClass() {
		return this.entityClass;
	}

}