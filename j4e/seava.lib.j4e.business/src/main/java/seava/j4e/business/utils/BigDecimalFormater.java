package seava.j4e.business.utils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

/**
 * Format in the entity the BigDecimal's decimal numbers.
 * 
 * @author apetho
 */
public class BigDecimalFormater {

	/**
	 * @param entity
	 * @param decimal
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> void format(T entity, int decimal) throws IllegalArgumentException, IllegalAccessException {
		for (Field field : entity.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			if (java.lang.reflect.Modifier.isStatic(field.getModifiers()) || field.getType().equals(entity.getClass())) {
				continue;
			}
			if (field.getType().equals(List.class)) {
				List list = (List) field.get(entity);
				if (list == null) {
					continue;
				}
				for (int i = 0; i < list.size(); ++i) {
					format(list.get(i), decimal);
				}
			} else if (field.getType().equals(BigDecimal.class)) {
				Object o = field.get(entity);
				if (o == null) {
					continue;
				}
				BigDecimal formatedValue = ((BigDecimal) o).setScale(decimal, BigDecimal.ROUND_HALF_UP);
				field.set(entity, formatedValue);
			} else {
				if (!field.getType().isPrimitive()) {
					Object o = field.get(entity);
					if (o == null) {
						continue;
					}
					format(o, decimal);
				}
			}
		}
	}

}
