/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.business.service.job;

import seava.j4e.api.service.job.IJob;
import seava.j4e.api.service.job.IJobFactory;
import seava.j4e.business.AbstractApplicationContextAware;

/**
 * BusinessJobFactory
 */
public class BusinessJobFactory extends AbstractApplicationContextAware implements IJobFactory {

	@Override
	public IJob create(String key) {
		return this.getApplicationContext().getBean(key, IJob.class);
	}

}
