/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.descriptor;

public class AsgnDescriptor<M> extends AbstractViewModelDescriptor<M> {

	public AsgnDescriptor(Class<M> modelClass) {
		super(modelClass);
	}
}
