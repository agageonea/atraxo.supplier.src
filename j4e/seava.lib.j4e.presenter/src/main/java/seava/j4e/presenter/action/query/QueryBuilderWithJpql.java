/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.action.query;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.time.DateUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.queries.FetchGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import seava.j4e.api.Constants;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.commons.action.query.FilterRule;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * @param <M>
 * @param <F>
 * @param <P>
 */
public class QueryBuilderWithJpql<M, F, P> extends AbstractQueryBuilder<M, F, P> {

	public static final String OP_LIKE = "like";
	public static final String OP_NOT_LIKE = "not like";

	public static final String OP_EQ = "=";
	public static final String OP_NOT_EQ = "<>";

	public static final String OP_LT = "<";
	public static final String OP_LT_EQ = "<=";

	public static final String OP_GT = ">";
	public static final String OP_GT_EQ = ">=";

	public static final String OP_BETWEEN = "between";

	private static final String OP_IN = "in";
	private static final String OP_NOT_IN = "not in";

	private static final String OP_NULL = "is null";
	private static final String OP_NOT_NULL = "is not null";

	private static final String OP_DATE = "OP_DATE";

	static final Logger logger = LoggerFactory.getLogger(QueryBuilderWithJpql.class);

	protected String defaultWhere;
	protected StringBuffer where;

	protected String defaultSort;
	protected StringBuffer sort;

	protected String baseEql;
	protected String baseEqlCount;
	protected String baseEqlSummary;

	/**
	 * Dirty work-around to avoid eclipselink bug when using fetch-groups with Cursor
	 */
	protected boolean forExport;

	private String entityAlias = "e";

	public String getBaseEql() {
		return this.baseEql;
	}

	public void setBaseEql(String baseEql) {
		this.baseEql = baseEql;
	}

	public String getBaseEqlCount() {
		return this.baseEqlCount;
	}

	public void setBaseEqlCount(String baseEqlCount) {
		this.baseEqlCount = baseEqlCount;
	}

	public String getBaseEqlSummary() {
		return this.baseEqlSummary;
	}

	public void setBaseEqlSummary(String baseEqlSummary) {
		this.baseEqlSummary = baseEqlSummary;
	}

	/**
	 * Create the query statement to fetch the result data which match the filter criteria.<br> Can be customized through the <code>before</code>,
	 * <code>on</code> and <code>after</code> methods.
	 *
	 * @return
	 * @throws Exception
	 */
	public String buildQueryStatement() throws Exception {
		this.beforeBuildQueryStatement();
		String qs = this.onBuildQueryStatement();
		this.afterBuildQueryStatement(qs);
		if (logger.isDebugEnabled()) {
			logger.debug("JQPL to execute: {}", qs);
		}
		return qs;
	}

	/**
	 * Template method to override with custom implementation. Fragments used in onBuildQueryStatement can be overriden.
	 *
	 * @throws Exception
	 */
	protected void beforeBuildQueryStatement() throws Exception {
		// is implemented in subclasses if is needed
	}

	/**
	 * Creates the JPQL query statement used to fetch the result data. It adds to <code>baseEql</code> the jpql fragments according to filter
	 * criteria, sort information and meta information provided by the model class. Customize it with the <code>before</code> and <code>after</code>
	 * methods or you can entirely override.
	 *
	 * @return
	 * @throws Exception
	 */
	protected String onBuildQueryStatement() throws Exception {
		StringBuffer eql = new StringBuffer(this.baseEql);
		this.addFetchJoins(eql);
		this.buildWhere();
		this.attachWhereClause(eql);
		this.buildSort();
		this.attachSortClause(eql);

		return eql.toString();
	}

	/**
	 * Post query string creation code.
	 *
	 * @param builtQueryStatement : The query statement which has been built.
	 * @throws Exception
	 */
	protected void afterBuildQueryStatement(String builtQueryStatement) throws Exception {
		// is implemented in subclasses if is needed
	}

	/**
	 * Create the count query statement to count the total number of results which match the filter criteria.<br> Can be customized through the
	 * <code>before</code>, <code>on</code> and <code>after</code> methods.
	 *
	 * @return
	 * @throws Exception
	 */
	public String buildCountStatement() throws Exception {
		this.beforeBuildCountStatement();
		String qs = this.onBuildCountStatement();
		this.afterBuildCountStatement(qs);
		if (logger.isDebugEnabled()) {
			logger.debug("count JQPL to execute: {}", qs);
		}
		return qs;
	}

	protected void beforeBuildCountStatement() throws Exception {
		// is implemented in subclasses if is needed
	}

	protected String onBuildCountStatement() throws Exception {
		StringBuffer eql = new StringBuffer(this.baseEqlCount);
		this.addFetchJoins(eql);
		this.attachWhereClause(eql);
		return eql.toString();
	}

	protected void afterBuildCountStatement(String builtQueryStatement) throws Exception {
		// is implemented in subclasses if is needed
	}

	/**
	 * Create the summary query statement to calculate the summary on records which match the filter criteria.<br> Can be customized through the
	 * <code>before</code>, <code>on</code> and <code>after</code> methods.
	 *
	 * @return
	 * @throws Exception
	 */
	public String buildSummaryStatement() throws Exception {
		this.beforeBuildsummaryStatement();
		String qs = this.onBuildSummaryStatement();
		this.afterBuildSummaryStatement(qs);
		if (logger.isDebugEnabled()) {
			logger.debug("summary JQPL to execute: {}", qs);
		}
		return qs;
	}

	protected void beforeBuildsummaryStatement() throws Exception {
		// is implemented in subclasses if is needed
	}

	protected String onBuildSummaryStatement() throws Exception {
		StringBuffer eql = new StringBuffer(this.baseEqlSummary);
		this.addFetchJoins(eql);
		this.attachWhereClause(eql);
		return eql.toString();
	}

	protected void afterBuildSummaryStatement(String builtQueryStatement) throws Exception {
		// is implemented in subclasses if is needed
	}

	/**
	 * Append fetch joins based on the model descriptor.
	 *
	 * @param eql
	 */
	private void addFetchJoins(StringBuffer eql) {
		if (logger.isDebugEnabled()) {
			logger.debug("Adding fetch joins ... ");
		}
		if (this.getDescriptor().getFetchJoins() != null) {
			Iterator<String> it = this.getDescriptor().getFetchJoins().keySet().iterator();
			while (it.hasNext()) {
				String p = it.next();
				String type = this.getDescriptor().getFetchJoins().get(p);
				if (type != null && "left".equals(type)) {
					eql.append(" left join fetch " + p);
				} else {
					eql.append(" join fetch " + p);
				}
			}
		}
	}

	/**
	 * Append where clause. Use the default where as well as the calculated one based on the filter criteria.
	 *
	 * @param eql
	 */
	private void attachWhereClause(StringBuffer eql) {
		if ((this.where != null && !"".equals(this.where.toString())) || (this.defaultWhere != null && !"".equals(this.defaultWhere))) {

			eql.append(" where ");
			if (this.defaultWhere != null && !"".equals(this.defaultWhere)) {
				eql.append(this.defaultWhere);
			}
			if (this.where != null && !"".equals(this.where.toString())) {
				if (this.defaultWhere != null && !"".equals(this.defaultWhere)) {
					eql.append(" and ");
				}
				eql.append(this.where);
			}
		}
	}

	/**
	 * Append order by. If there is an explicit order by use that one otherwise use the default one if any.
	 *
	 * @param eql
	 */
	private void attachSortClause(StringBuffer eql) {
		if (this.sort != null) {
			String orderBy = this.sort.toString();
			if (logger.isDebugEnabled()) {
				logger.debug("Attaching calculated order by: " + orderBy);
			}
			eql.append(" order by " + orderBy);
		} else {
			if (this.defaultSort != null && !"".equals(this.defaultSort)) {
				if (logger.isDebugEnabled()) {
					logger.debug("Attaching default order by: " + this.defaultSort);
				}
				eql.append(" order by " + this.defaultSort);
			}
		}
	}

	/**
	 * Build order by information.
	 */
	private void buildSort() {
		if (logger.isDebugEnabled()) {
			logger.debug("Building JPQL order by ...");
		}

		String[] sortColumnNames = this.getSortColumnNames();
		String[] sortColumnSense = this.getSortColumnSense();

		if (sortColumnNames != null && sortColumnNames.length > 0) {
			this.sort = new StringBuffer();
			for (int i = 0; i < sortColumnNames.length; i++) {
				if (i > 0) {
					this.sort.append(",");
				}
				if (this.getDescriptor().getOrderBys().containsKey(sortColumnNames[i])) {
					String[] fields = this.getDescriptor().getOrderBys().get(sortColumnNames[i]);

					for (int k = 0, l = fields.length; k < l; k++) {
						if (k > 0) {
							this.sort.append(",");
						}
						this.sort.append(this.entityAlias + "." + fields[k] + " " + sortColumnSense[i]);
					}
				} else {
					String keyPart = this.entityAlias + "." + this.getDescriptor().getRefPaths().get(sortColumnNames[i]);
					String alias = keyPart.substring(0, keyPart.lastIndexOf("."));
					if (!alias.equalsIgnoreCase(this.entityAlias)) {
						for (String key : this.getDescriptor().getFetchJoins().keySet()) {
							if (key.contains(alias)) {
								keyPart = key.substring(key.indexOf(" ")) + keyPart.substring(keyPart.lastIndexOf("."));
								break;
							}
						}
					}
					this.sort.append(keyPart + " " + sortColumnSense[i]);
				}
			}
		}

		if (logger.isDebugEnabled() && this.sort != null) {
			logger.debug(" -> order-by: " + this.sort.toString());
		}
	}

	private void buildWhere() throws Exception {
		this.beforeBuildWhere();
		this.onBuildWhere();
		this.afterBuildWhere();
	}

	protected void beforeBuildWhere() throws Exception {
		// is implemented in subclasses if is needed
	}

	protected void onBuildWhere() throws Exception {
		this.processFilter();
		this.processAdvancedFilter();
		if (logger.isDebugEnabled() && this.where != null) {
			logger.debug(" -> where: " + this.where.toString());
		}
	}

	// SONE-3059 - this function should be created in database to can filter date columns by a value
	// DELIMITER $$
	//
	// USE `auto_supplier`$$
	//
	// DROP FUNCTION IF EXISTS `ConvertDateToString`$$
	//
	// CREATE DEFINER=`root`@`%` FUNCTION `convertdatetostring`(d DATE) RETURNS CHAR(24) CHARSET utf8 COLLATE utf8_unicode_ci
	// BEGIN
	// RETURN DATE_FORMAT(d, '%d.%m.%Y %Y.%m.%d');
	// END$$
	//
	// DELIMITER ;

	private void appendFilterRule0(String source, FilterRule filterRule, int cnt, String prevGroupOp, String crtGroupOp) throws Exception {

		this.addFilterCondition(this.entityAlias + "." + source + " " + filterRule.getOperation(), prevGroupOp, crtGroupOp);

	}

	private void appendFilterRule1(String source, FilterRule filterRule, int cnt, String prevGroupOp, String crtGroupOp) throws Exception {
		String key = filterRule.getFieldName() + "_" + cnt;
		String op = filterRule.getOperation();

		if (op.equals(OP_DATE)) {
			this.addFilterCondition("FUNC('ConvertDateToString'," + this.entityAlias + "." + source + ") like :" + key, prevGroupOp, crtGroupOp);
		} else {
			this.addFilterCondition(this.entityAlias + "." + source + " " + op + " :" + key, prevGroupOp, crtGroupOp);
		}

		if (op.equals(OP_IN) || op.equals(OP_NOT_IN)) {

			String[] inVals = filterRule.getValue1().split(",");
			this.defaultFilterItems.put(key, Arrays.asList(inVals));

		} else if (op.equals(OP_LIKE) || op.equals(OP_NOT_LIKE) || op.equals(OP_DATE)) {

			this.defaultFilterItems.put(key, filterRule.getValue1());

		} else {

			this.defaultFilterItems.put(key, filterRule.getConvertedValue1());

		}
	}

	private void appendFilterRule2(String source, FilterRule filterRule, int cnt, String prevGroupOp, String crtGroupOp) throws Exception {

		String key1 = filterRule.getFieldName() + "_" + cnt + "_1";
		String key2 = filterRule.getFieldName() + "_" + cnt + "_2";

		this.addFilterCondition(this.entityAlias + "." + source + " " + filterRule.getOperation() + " :" + key1 + " and :" + key2, prevGroupOp,
				crtGroupOp);
		this.defaultFilterItems.put(key1, filterRule.getConvertedValue1());
		this.defaultFilterItems.put(key2, filterRule.getConvertedValue2());

	}

	private List<String> operations0 = null;
	private List<String> operations1 = null;
	private List<String> operations2 = null;

	private void appendJpqlFragmentForFilterRule(FilterRule filterRule, String refPath, int cnt, String prevGroupOp, String crtGroupOp)
			throws Exception {

		String op = filterRule.getOperation();

		if (this.operations0.contains(op)) {
			this.appendFilterRule0(refPath, filterRule, cnt, prevGroupOp, crtGroupOp);
		}
		if (this.operations1.contains(op)) {
			this.appendFilterRule1(refPath, filterRule, cnt, prevGroupOp, crtGroupOp);
		}
		if (this.operations2.contains(op)) {
			this.appendFilterRule2(refPath, filterRule, cnt, prevGroupOp, crtGroupOp);
		}

	}

	protected class FilterRuleComparator implements Comparator<IFilterRule> {
		// based on groupOp, we need to order the elements from parameters to can put the brackets in the right way
		@Override
		public int compare(IFilterRule arg0, IFilterRule arg1) {
			return arg0.getGroupOp().compareTo(arg1.getGroupOp());
		}
	}

	protected void processAdvancedFilter() throws Exception {

		if (this.filterRules == null || this.filterRules.isEmpty()) {
			return;
		}

		this.operations0 = Arrays.asList(new String[] { OP_NULL, OP_NOT_NULL });
		this.operations1 = Arrays.asList(new String[] { OP_LIKE, OP_NOT_LIKE, OP_EQ, OP_NOT_EQ, OP_LT, OP_LT_EQ, OP_GT, OP_GT_EQ, OP_IN, OP_NOT_IN,
				OP_DATE });
		this.operations2 = Arrays.asList(new String[] { OP_BETWEEN });

		Map<String, String> refpaths = this.getDescriptor().getRefPaths();
		Class<?> clz = this.getModelClass();
		Method m;
		int cnt = 0;
		String prevGroupOp;
		String crtGroupOp = "";
		Collections.sort(this.filterRules, new FilterRuleComparator());
		for (IFilterRule ifr : this.filterRules) {
			FilterRule fr = (FilterRule) ifr;
			String fieldName = fr.getFieldName();
			if (this.shouldProcessFilterField(fieldName, fieldName)) {
				// get the filter getter
				cnt++;
				try {
					m = clz.getMethod("get" + StringUtils.capitalize(fieldName));
					fr.setDataTypeFQN(m.getReturnType().getCanonicalName());
					prevGroupOp = crtGroupOp;
					crtGroupOp = fr.getGroupOp();
					this.appendJpqlFragmentForFilterRule(fr, refpaths.get(fieldName), cnt, prevGroupOp, crtGroupOp);
				} catch (NoSuchMethodException e) {
					if (logger.isDebugEnabled()) {
						logger.debug(e.getMessage());
					}
					throw new Exception("Invalid field name: " + fieldName, e);
				}
			}
		}
		prevGroupOp = crtGroupOp;
		crtGroupOp = "";
		this.resolveGroupOp(prevGroupOp, crtGroupOp);
	}

	protected void processFilter() throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Building JPQL where ...");
		}
		Map<String, String> refpaths = this.getDescriptor().getRefPaths();
		Map<String, String> jpqlFilterRules = this.getDescriptor().getJpqlFieldFilterRules();

		this.defaultFilterItems = new HashMap<String, Object>();

		Class<?> clz = this.getFilterClass();

		// The filter object could be a dedicated filter class or the
		// data-source model.
		// Ensure we do not apply filter on the framework specific properties

		List<String> excludes = Arrays.asList(new String[] { "_entity_", "__clientRecordId__" });

		String filterAttrString = null;
		Object fO = this.getFilter();
		if (fO instanceof AbstractDsModel<?>) {
			filterAttrString = ((AbstractDsModel<F>) fO).get__filterAttrString__();
		}
		if (filterAttrString == null) {
			filterAttrString = "";
		}

		while (clz != null) {
			Field[] fields = clz.getDeclaredFields();

			for (Field field : fields) {

				String fieldName = field.getName();
				if (this.isValidFilterField(field, excludes)) {
					String filterFieldName = fieldName;
					FilterFieldNameAndRangeType fnrt = this.resolveRealFilterFieldNameAndRangeType(field);
					fieldName = fnrt.getName();

					if (this.shouldProcessFilterField(fieldName, filterFieldName)) {

						// get the filter getter
						Method m = null;
						try {
							m = clz.getMethod("get" + StringUtils.capitalize(filterFieldName));
						} catch (Exception e) {
							if (logger.isDebugEnabled()) {
								logger.debug(e.getMessage(), e);
							}
						}

						if (m == null) {
							throw new Exception("Could not find method: get" + StringUtils.capitalize(filterFieldName));
						}
						// get the value
						Object fv = m.invoke(this.getFilter());

						if (fv != null) {
							if (m.getReturnType() == java.lang.String.class) {

								String _fv = (String) fv;
								String _op = OP_LIKE;
								String _criteria = null;

								boolean withParamPlaceholder = true;
								boolean withParamsAsList = false;

								if (jpqlFilterRules.containsKey(fieldName)) {
									// if this field has a special filter
									// condition use it
									this.addFilterCondition(jpqlFilterRules.get(fieldName));

								} else {
									// build the default condition on the mapped
									// entity field in case it is mapped
									if (refpaths.containsKey(fieldName)) {

										if (_fv.startsWith("[NULL]")) {
											_op = OP_NULL;
											withParamPlaceholder = false;
											_criteria = "(" + this.entityAlias + "." + refpaths.get(fieldName) + " " + _op + " OR "
													+ this.entityAlias + "." + refpaths.get(fieldName) + " = '' )";
										} else if (_fv.startsWith("[!NULL]")) {
											_op = OP_NOT_NULL;
											withParamPlaceholder = false;
											_criteria = "(" + this.entityAlias + "." + refpaths.get(fieldName) + " " + _op + " AND "
													+ this.entityAlias + "." + refpaths.get(fieldName) + " <> '' )";
										} else if (_fv.indexOf(',') != -1) {
											withParamsAsList = true;
											if (_fv.startsWith("[!]")) {
												_op = OP_NOT_IN;
												_fv = _fv.substring(3);
											} else {
												_op = OP_IN;
											}
										} else if ("id".equals(fieldName) || "refid".equals(fieldName) || "clientId".equals(fieldName)) {
											_op = OP_EQ;
										} else if ("subsidiaryId".equals(fieldName)) {
											_op = OP_IN;
											withParamPlaceholder = false;
											withParamsAsList = true;
											_criteria = "(" + this.entityAlias + "." + refpaths.get(fieldName) + " " + _op + " :" + fieldName + " )";
										}

										if (_criteria != null) {
											this.addFilterCondition(_criteria);
										} else {
											if (withParamPlaceholder) {
												this.addFilterCondition(this.entityAlias + "." + refpaths.get(fieldName) + " " + _op + " :"
														+ fieldName);
											} else {
												this.addFilterCondition(this.entityAlias + "." + refpaths.get(fieldName) + " " + _op);
											}
										}

									}
								}
								// add the value anyway , maybe it is used in
								// the ds-level where clause.
								// If there is no field-level filter condition,
								// the field is not mapped to an entity field,
								// and there is no filter condition at
								// data-source level to use this field as
								// parameter
								// it should be forced to null anywhere in
								// pre-query phase.
								if (!"__filterAttrString__".equalsIgnoreCase(fieldName)) {
									if (withParamsAsList) {
										this.defaultFilterItems.put(fieldName, Arrays.asList(_fv.split(",")));
									} else {
										this.defaultFilterItems.put(fieldName, _fv);
									}
								}
							} else {
								if (fnrt.getType() != FilterFieldNameAndRangeType.NO_RANGE) {
									if (fnrt.getType() == FilterFieldNameAndRangeType.RANGE_FROM) {
										this.addFilterCondition(this.entityAlias + "." + refpaths.get(fieldName) + " >= :" + filterFieldName);
									} else {
										this.addFilterCondition(this.entityAlias + "." + refpaths.get(fieldName) + " < :" + filterFieldName);
									}

									this.defaultFilterItems.put(filterFieldName, fv);
								} else {
									if (field.getType().equals(java.util.Date.class) && filterAttrString.indexOf(fieldName + "_cutTime") >= 0) {
										if (jpqlFilterRules.containsKey(fieldName)) {
											this.addFilterCondition(jpqlFilterRules.get(fieldName));
											this.defaultFilterItems.put(fieldName, fv);
										} else {
											String fieldName1 = fieldName + "_1";
											String fieldName2 = fieldName + "_2";
											this.addFilterCondition(this.entityAlias + "." + refpaths.get(fieldName) + " " + OP_BETWEEN + " :"
													+ fieldName1 + " AND :" + fieldName2);
											java.util.Date fv1 = (Date) fv;
											Calendar calendar = Calendar.getInstance();
											calendar.setTime(fv1);
											calendar = DateUtils.truncate(calendar, Calendar.DAY_OF_MONTH);
											fv1 = calendar.getTime();

											java.util.Date fv2 = (Date) fv;
											calendar.setTime(fv2);
											calendar = DateUtils.truncate(calendar, Calendar.DAY_OF_MONTH);
											calendar.add(Calendar.SECOND, 24 * 60 * 60 - 1);
											fv2 = calendar.getTime();

											this.defaultFilterItems.put(fieldName1, fv1);
											this.defaultFilterItems.put(fieldName2, fv2);
										}
									} else {
										if (jpqlFilterRules.containsKey(fieldName)) {
											this.addFilterCondition(jpqlFilterRules.get(fieldName));
										} else {
											this.addFilterCondition(this.entityAlias + "." + refpaths.get(fieldName) + " = :" + fieldName);
										}
										this.defaultFilterItems.put(fieldName, fv);
									}
								}
							}
						}
					}
				}
			}
			clz = clz.getSuperclass();
		}

	}

	protected void afterBuildWhere() throws Exception {
		// is implemented in subclasses if is needed
	}

	protected boolean resolveGroupOp(String prevGroupOp, String crtGroupOp) {
		boolean res = false;
		if (prevGroupOp == null) {
			prevGroupOp = "";
		}
		if (crtGroupOp == null) {
			crtGroupOp = "";
		}
		if (!prevGroupOp.equalsIgnoreCase(crtGroupOp)) {
			if ("".equals(prevGroupOp)) {
				if (this.where.length() > 0) {
					this.where.append(" and ( ");
				} else {
					this.where.append(" ( ");
				}
				res = true;
			} else {
				this.where.append(" ) ");
				if (!"".equals(crtGroupOp)) {
					this.where.append(" and ( ");
					res = true;
				}
			}
		}
		return res;
	}

	/**
	 * @param filter
	 */
	public void addFilterCondition(String filter) {
		this.addFilterCondition(filter, "", "");
	}

	/**
	 * @param filter
	 * @param prevGroupOp
	 * @param crtGroupOp
	 */
	public void addFilterCondition(String filter, String prevGroupOp, String crtGroupOp) {
		if (this.where == null) {
			this.where = new StringBuffer();
		}
		if (this.where.length() > 0) {
			boolean openB = this.resolveGroupOp(prevGroupOp, crtGroupOp);
			if (!openB) {
				if (!"".equals(crtGroupOp) && crtGroupOp != null) {
					this.where.append(" or ");
				} else {
					this.where.append(" and ");
				}
			}
		} else {
			this.resolveGroupOp(prevGroupOp, crtGroupOp);
		}
		this.where.append(filter);
	}

	protected void addCustomFilterItem(String key, Object value) {
		if (this.customFilterItems == null) {
			this.customFilterItems = new HashMap<String, Object>();
		}
		this.customFilterItems.put(key, value);
	}

	private void bindFilterParams(Query q) throws Exception {

		if (this.defaultFilterItems != null) {
			for (String key : this.defaultFilterItems.keySet()) {
				Object value = this.defaultFilterItems.get(key);
				try {
					if (value instanceof java.lang.String) {
						q.setParameter(key, ((String) value).replace('*', '%'));
					} else {
						q.setParameter(key, value);
					}
				} catch (IllegalArgumentException e) {
					if (logger.isDebugEnabled()) {
						logger.debug(e.getMessage(), e);
					}
				}
			}
		}
		if (this.customFilterItems != null) {
			for (String key : this.customFilterItems.keySet()) {
				Object value = this.customFilterItems.get(key);
				if (value instanceof java.lang.String) {
					q.setParameter(key, ((String) value).replace('*', '%'));
				} else {
					q.setParameter(key, value);
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Bound filter params:");
			for (Parameter<?> p : q.getParameters()) {
				try {
					logger.debug(" -> " + p.getName() + " = " + q.getParameterValue(p));
				} catch (Exception e) {
					// maybe a parameter has not been bound
					logger.debug(e.getMessage(), e);
				}
			}
		}
	}

	protected void addFetchGroup(Query q) {
		// see the reason of forExport flag
		boolean disableFecthGroups = this.getSettings().getAsBoolean(Constants.PROP_DISABLE_FETCH_GROUPS);
		if (this.forExport || disableFecthGroups) {
			return;
		}
		logger.debug("Adding fetchGroup...");
		FetchGroup fg = new FetchGroup("default");
		fg.setShouldLoad(true);

		if (this.getDescriptor().getRefPaths() != null) {
			Map<String, String> refPaths = this.getDescriptor().getRefPaths();
			Iterator<String> it = refPaths.keySet().iterator();
			while (it.hasNext()) {
				String p = it.next();
				fg.addAttribute(refPaths.get(p));
			}
			q.setHint(QueryHints.FETCH_GROUP, fg);
		}
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public Query createQuery() throws Exception {
		String jpql = this.buildQueryStatement();
		Query q = this.getEntityManager().createQuery(jpql);
		this.createQuery_(q);
		return q;
	}

	/**
	 * @param resultType
	 * @return
	 * @throws Exception
	 */
	public <E> TypedQuery<E> createQuery(Class<E> resultType) throws Exception {
		String jpql = this.buildQueryStatement();
		TypedQuery<E> q = this.getEntityManager().createQuery(jpql, resultType);
		this.createQuery_(q);
		return q;
	}

	/**
	 * @param q
	 * @throws Exception
	 */
	private void createQuery_(Query q) throws Exception {
		if (this.getDescriptor().getQueryHints() != null) {
			Map<String, Object> queryHints = this.getDescriptor().getQueryHints();
			Iterator<String> it = queryHints.keySet().iterator();
			while (it.hasNext()) {
				String p = it.next();
				q.setHint(p, queryHints.get(p));
			}
		}
		this.addFetchGroup(q);
		// when Fetch Group is used some data are not loaded. Need to search why.
		this.bindFilterParams(q);
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public Query createQueryCount() throws Exception {
		Query q = this.getEntityManager().createQuery(this.buildCountStatement());
		this.bindFilterParams(q);
		return q;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public Query createQuerySummary() throws Exception {
		Query q = this.getEntityManager().createQuery(this.buildSummaryStatement());
		this.bindFilterParams(q);
		return q;
	}

	public String getDefaultWhere() {
		return this.defaultWhere;
	}

	public void setDefaultWhere(String defaultWhere) {
		this.defaultWhere = defaultWhere;
	}

	public String getDefaultSort() {
		return this.defaultSort;
	}

	public void setDefaultSort(String defaultSort) {
		this.defaultSort = defaultSort;
	}

	public boolean isForExport() {
		return this.forExport;
	}

	public void setForExport(boolean forExport) {
		this.forExport = forExport;
	}

}
