/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.descriptor;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import seava.j4e.api.descriptor.IDsDefinition;
import seava.j4e.api.descriptor.IFieldDefinition;

@XmlRootElement(name = "dsDefinition")
@XmlAccessorType(XmlAccessType.FIELD)
public class DsDefinition implements IDsDefinition {

	/**
	 * Business name of the data-source component used to identify the component when serving a request.
	 */
	private String name;

	/**
	 * Data-source model class.
	 */
	private Class<?> modelClass;

	/**
	 * Data-source filter class.
	 */
	private Class<?> filterClass;

	/**
	 * Data-source param class.
	 */
	private Class<?> paramClass;

	/**
	 * Model fields
	 */
	@XmlElementWrapper(name = "modelFields")
	@XmlElement(name = "field", type = FieldDefinition.class)
	private List<IFieldDefinition> modelFields;

	/**
	 * Filter fields. If the data-source does not have own filter (it uses the model as filter ) these are the same as the model fields of course.
	 */
	@XmlElementWrapper(name = "filterFields")
	@XmlElement(name = "field", type = FieldDefinition.class)
	private List<IFieldDefinition> filterFields;

	/**
	 * Param fields. If the data-source does not have param model this will be null;
	 */
	@XmlElementWrapper(name = "paramFields")
	@XmlElement(name = "field", type = FieldDefinition.class)
	private List<IFieldDefinition> paramFields;

	/**
	 * Flag which indicates that this is an assignment component.
	 */
	private boolean asgn;

	/**
	 * Flag which indicates that this components provides only read-only functionality.
	 */
	private boolean readOnly;

	/**
	 * List of publicly exposed service methods.
	 */
	@XmlElementWrapper(name = "serviceMethods")
	@XmlElement(name = "serviceMethod")
	private List<String> serviceMethods;

	private void resolveFields(Class<?> claz, List<IFieldDefinition> fieldsList) {
		Class<?> theClass = claz;
		List<String> _tmp = new ArrayList<String>();
		while (theClass != null) {
			Field[] fields = theClass.getDeclaredFields();
			for (Field field : fields) {
				String fieldName = field.getName();
				if (!Modifier.isStatic(field.getModifiers())) {
					if (!fieldName.equals("_entity_") && !fieldName.equals("__clientRecordId__") && !_tmp.contains(fieldName)) {
						_tmp.add(fieldName);
						fieldsList.add(new FieldDefinition(fieldName, field.getType().getCanonicalName()));
					}
				}
			}
			theClass = theClass.getSuperclass();
		}
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Class<?> getModelClass() {
		return this.modelClass;
	}

	@Override
	public void setModelClass(Class<?> modelClass) {
		this.modelClass = modelClass;
	}

	@Override
	public Class<?> getFilterClass() {
		return this.filterClass;
	}

	@Override
	public void setFilterClass(Class<?> filterClass) {
		this.filterClass = filterClass;
	}

	@Override
	public Class<?> getParamClass() {
		return this.paramClass;
	}

	@Override
	public void setParamClass(Class<?> paramClass) {
		this.paramClass = paramClass;
	}

	@Override
	public boolean isAsgn() {
		return this.asgn;
	}

	@Override
	public void setAsgn(boolean isAsgn) {
		this.asgn = isAsgn;
	}

	@Override
	public boolean isReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	@Override
	public List<String> getServiceMethods() {
		return this.serviceMethods;
	}

	public void setServiceMethods(List<String> serviceMethods) {
		this.serviceMethods = serviceMethods;
	}

	@Override
	public void addServiceMethod(String serviceMethod) {
		if (this.serviceMethods == null) {
			this.serviceMethods = new ArrayList<String>();
		}
		this.serviceMethods.add(serviceMethod);
	}

	public void addServiceMethods(List<String> serviceMethods) {
		if (this.serviceMethods == null) {
			this.serviceMethods = new ArrayList<String>();
		}
		this.serviceMethods.addAll(serviceMethods);
	}

	@Override
	public List<IFieldDefinition> getModelFields() {
		if (this.modelClass != null && this.modelFields == null) {
			this.modelFields = new ArrayList<IFieldDefinition>();
			this.resolveFields(this.modelClass, this.modelFields);
		}
		return this.modelFields;
	}

	@Override
	public List<IFieldDefinition> getFilterFields() {
		if (this.filterClass != null && this.filterFields == null) {
			this.filterFields = new ArrayList<IFieldDefinition>();
			this.resolveFields(this.filterClass, this.filterFields);
		}
		return this.filterFields;
	}

	@Override
	public List<IFieldDefinition> getParamFields() {
		if (this.paramClass != null && this.paramFields == null) {
			this.paramFields = new ArrayList<IFieldDefinition>();
			this.resolveFields(this.paramClass, this.paramFields);
		}
		return this.paramFields;
	}

}
