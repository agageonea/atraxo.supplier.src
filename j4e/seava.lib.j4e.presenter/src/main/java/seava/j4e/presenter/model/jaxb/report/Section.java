package seava.j4e.presenter.model.jaxb.report;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Section {

	private int order;
	private String layoutType;
	private ReportSectionUrl url;
	private String title;

	@XmlAttribute
	public int getOrder() {
		return this.order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@XmlAttribute
	public String getLayoutType() {
		return this.layoutType;
	}

	public void setLayoutType(String layoutType) {
		this.layoutType = layoutType;
	}

	@XmlElement(name = "url")
	public ReportSectionUrl getUrl() {
		return this.url;
	}

	public void setUrl(ReportSectionUrl url) {
		this.url = url;
	}

	@XmlElement(name = "title")
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
