package seava.j4e.presenter.json.module;

import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonSerializer;

public class JsonModuleConfigurator<T> {

	private String typeClassName;
	private JsonSerializer<T> jsonSerializer;
	private JsonDeserializer<T> jsonDeserializer;

	public String getTypeClassName() {
		return this.typeClassName;
	}

	public void setTypeClassName(String typeClassName) {
		this.typeClassName = typeClassName;
	}

	public JsonSerializer<T> getJsonSerializer() {
		return this.jsonSerializer;
	}

	public void setJsonSerializer(JsonSerializer<T> jsonSerializer) {
		this.jsonSerializer = jsonSerializer;
	}

	public JsonDeserializer<T> getJsonDeserializer() {
		return this.jsonDeserializer;
	}

	public void setJsonDeserializer(JsonDeserializer<T> jsonDeserializer) {
		this.jsonDeserializer = jsonDeserializer;
	}

	@SuppressWarnings("unchecked")
	public Class<T> getClassForType() throws ClassNotFoundException {
		return (Class<T>) Class.forName(this.typeClassName);
	}

}
