/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.action.impex;

import java.util.List;

/**
 * CSV loader result which returns the parsed data as a list as well as the headers
 *
 * @author amathe
 * @param <T>
 */
public class DsCsvLoaderResult<T> {

	/**
	 * Parsed data as list of T beans
	 */
	private List<T> result;

	/**
	 * csv header
	 */
	private String[] header;

	public List<T> getResult() {
		return this.result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

	public String[] getHeader() {
		return this.header;
	}

	public void setHeader(String[] header) {
		this.header = header;
	}

}
