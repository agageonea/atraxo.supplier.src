package seava.j4e.presenter.action.impex.report.jaxb.adapters;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;

import seava.j4e.presenter.action.impex.report.jaxb.wrappers.ListWrapper;
import seava.j4e.presenter.model.jaxb.report.ColumnDef;

public class ColumnListAdapter extends XmlAdapter<ListWrapper<JAXBElement<ColumnDef>>, List<ColumnDef>> {

	@Override
	public ListWrapper<JAXBElement<ColumnDef>> marshal(List<ColumnDef> arg0) throws Exception {
		ListWrapper<JAXBElement<ColumnDef>> wrapper = new ListWrapper<>();
		wrapper.properties = new ArrayList<>();
		for (ColumnDef obj : arg0) {
			wrapper.properties.add(new JAXBElement<ColumnDef>(new QName("column"), ColumnDef.class, obj));

		}
		return wrapper;
	}

	@Override
	public List<ColumnDef> unmarshal(ListWrapper<JAXBElement<ColumnDef>> arg0) throws Exception {
		throw new OperationNotSupportedException();
	}

}
