package seava.j4e.presenter.model.domain.impex;

public class DsRelation {
	private String parentCode;
	private String childCode;

	public String getParentCode() {
		return this.parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getChildCode() {
		return this.childCode;
	}

	public void setChildCode(String childCode) {
		this.childCode = childCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.childCode == null) ? 0 : this.childCode.hashCode());
		result = prime * result + ((this.parentCode == null) ? 0 : this.parentCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		DsRelation other = (DsRelation) obj;
		if (this.childCode == null) {
			if (other.childCode != null) {
				return false;
			}
		} else if (!this.childCode.equals(other.childCode)) {
			return false;
		}
		if (this.parentCode == null) {
			if (other.parentCode != null) {
				return false;
			}
		} else if (!this.parentCode.equals(other.parentCode)) {
			return false;
		}
		return true;
	}

}
