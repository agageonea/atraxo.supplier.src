package seava.j4e.presenter.action.impex.report.jaxb.wrappers;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class RecordWrapper {

	@XmlElement
	public List<MapList> record = new ArrayList<MapList>();
}
