package seava.j4e.presenter.action.impex.report.jaxb.wrappers;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAnyElement;

public class MapList {

	@XmlAnyElement
	public List<JAXBElement<String>> properties = new ArrayList<>();

}