/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.service.ds;

import java.util.List;

import seava.j4e.api.service.business.IEntityServiceFactory;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.service.presenter.IDsServiceFactory;
import seava.j4e.presenter.AbstractApplicationContextAware;

public class DsServiceFactory extends AbstractApplicationContextAware implements IDsServiceFactory {

	private List<IEntityServiceFactory> entityServiceFactories;

	private String name;

	@SuppressWarnings("unchecked")
	@Override
	public <M, F, P> IDsService<M, F, P> create(String key) {
		IDsService<M, F, P> s = (IDsService<M, F, P>) this.getApplicationContext().getBean(key);
		return s;
	}

	public List<IEntityServiceFactory> getEntityServiceFactories() {
		return this.entityServiceFactories;
	}

	public void setEntityServiceFactories(List<IEntityServiceFactory> entityServiceFactories) {
		this.entityServiceFactories = entityServiceFactories;
	}

	@Override
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
