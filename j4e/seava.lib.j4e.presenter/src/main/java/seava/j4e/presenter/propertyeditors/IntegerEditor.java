/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.propertyeditors;

import java.beans.PropertyEditorSupport;

public class IntegerEditor extends PropertyEditorSupport {

	public IntegerEditor() {
		super();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		String input = (text != null ? text.trim() : null);
		if (input == null || input.equals("")) {
			// Treat empty String as null value.
			this.setValue(null);

		} else {
			try {
				this.setValue(Integer.parseInt(text));
			} catch (Exception e) {
				throw new IllegalArgumentException("Invalid integer value [" + text + "]", e);
			}
		}

	}
}
