/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.service.ds;

import java.io.File;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import seava.j4e.api.exceptions.ActionNotSupportedException;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.model.IModelWithClientId;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.api.model.IModelWithSubsidiaryId;
import seava.j4e.api.service.business.IEntityService;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.impex.ConfigCsvImport;
import seava.j4e.presenter.action.impex.DsCsvLoader;
import seava.j4e.presenter.action.impex.DsCsvLoaderResult;
import seava.j4e.presenter.exeptions.J4EPresenterErrorCodes;
import seava.j4e.presenter.exeptions.delegate.ExceptionProcessor;
import seava.j4e.presenter.model.AbstractDsModel;
import seava.j4e.presenter.model.domain.impex.DsRelation;

/**
 * Implements the write actions for an entity-ds. See the super-classes for more details.
 *
 * @author amathe
 * @param <M>
 * @param <F>
 * @param <P>
 * @param <E>
 */
public abstract class AbstractEntityDsWriteService<M extends AbstractDsModel<E>, F, P, E> extends AbstractEntityDsReadService<M, F, P, E> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractEntityDsWriteService.class);

	private boolean noInsert = false;
	private boolean noUpdate = false;
	private boolean noDelete = false;
	private boolean readOnly = false;

	/* ========================== INSERT =========================== */

	/**
	 * Provide custom logic to decide if the action can be executed.
	 *
	 * @return
	 */
	protected boolean canInsert() {
		return true;
	}

	/**
	 * Pre-insert event with the data-source values as received from client.
	 *
	 * @param ds
	 * @throws Exception
	 */
	protected void preInsert(M ds, P params) throws Exception {
	}

	/**
	 * Pre-insert event with data-source and a new entity instance populated from the data-source.
	 *
	 * @param ds
	 * @param e
	 * @throws Exception
	 */
	protected void preInsert(M ds, E e, P params) throws Exception {
	}

	/**
	 * Post-insert event. The entity has been persisted by the <code>entityManager</code>, but the possible changes which might alter the entity
	 * during its persist phase are not applied yet to the data-source.
	 *
	 * @param ds
	 * @param e
	 * @throws Exception
	 */
	protected void postInsertBeforeModel(M ds, E e, P params) throws Exception {
	}

	/**
	 * Post-insert event. The entity has been persisted by the <code>entityManager</code>, and the possible changes which might alter the entity
	 * during its persist phase are applied to the data-source.
	 *
	 * @param ds
	 * @param e
	 * @throws Exception
	 */
	protected void postInsertAfterModel(M ds, E e, P params) throws Exception {
	}

	/**
	 * Template method for <code>pre-insert</code>.
	 *
	 * @param list
	 * @throws Exception
	 */
	protected void preInsert(List<M> list, P params) throws Exception {

	}

	public void insert(List<M> list, P params) throws Exception {

		if (this.readOnly || this.noInsert || !this.canInsert()) {
			throw new ActionNotSupportedException("Insert not allowed in data-source " + this.getClass().getCanonicalName());
		}

		// add the client
		for (M ds : list) {
			if (ds instanceof IModelWithClientId) {
				((IModelWithClientId) ds).setClientId(Session.user.get().getClient().getId());
				if (ds instanceof IModelWithSubsidiaryId) {
					((IModelWithSubsidiaryId) ds).setSubsidiaryId(Session.user.get().getClient().getActiveSubsidiaryId());
				}
			}
			if (ds instanceof IModelWithId) {
				IModelWithId<?> _ds = (IModelWithId<?>) ds;
				if (_ds.getId() != null && _ds.getId().equals("")) {
					_ds.setId(null);
				}
			}
		}
		this.preInsert(list, params);

		// add entities in a queue and then try to insert them all in one
		// transaction
		IEntityService<E> _entityService = this.getEntityService();
		List<E> entities = new ArrayList<E>();

		for (M ds : list) {
			this.preInsert(ds, params);
			E e = _entityService.create();
			entities.add(e);
			((AbstractDsModel<E>) ds)._setEntity_(e);
			this.getConverter().modelToEntity(ds, e, true, _entityService.getEntityManager());

			this.preInsert(ds, e, params);
		}

		this.onInsert(list, entities, params);

		for (M ds : list) {
			E e = ((AbstractDsModel<E>) ds)._getEntity_();
			this.postInsertBeforeModel(ds, e, params);
			this.getConverter().entityToModel(e, ds, _entityService.getEntityManager(), null);
			this.postInsertAfterModel(ds, e, params);
		}
		this.postInsert(list, params);
	}

	/**
	 * Helper insert method for one object. It creates a list with this single object and delegates to the <code>insert(List<M> list, P params)</code>
	 * method
	 *
	 * @param ds
	 * @throws Exception
	 */
	public void insert(M ds, P params) throws Exception {
		List<M> list = new ArrayList<M>();
		list.add(ds);
		this.insert(list, params);
	}

	protected void onInsert(List<M> list, List<E> entities, P params) throws Exception {
		try {
			this.getEntityService().insert(entities);
		} catch (final TransactionSystemException ex) {
			LOG.info("Ds insert fault: ", ex);
			ExceptionProcessor.process(ex);
		}
	}

	/**
	 * Template method for <code>post-insert</code>.
	 *
	 * @param list
	 * @throws Exception
	 */
	protected void postInsert(List<M> list, P params) throws Exception {

	}

	/* ========================== UPDATE =========================== */

	/**
	 * Provide custom logic to decide if the action can be executed.
	 *
	 * @return
	 */
	protected boolean canUpdate() {
		return true;
	}

	/**
	 * Pre-insert event with the data-source values.
	 *
	 * @param ds
	 * @throws Exception
	 */
	protected void preUpdate(M ds, P params) throws Exception {
	}

	/**
	 * Pre-insert event with data-source and the corresponding source entity has been found. The entity has not been yet populated with the new values
	 * from the data-source.
	 *
	 * @param ds
	 * @param e
	 * @throws Exception
	 */
	protected void preUpdateBeforeEntity(M ds, E e, P params) throws Exception {
	}

	/**
	 * Pre-insert event with data-source and the corresponding source entity has been found. The new values from the data-source are already applied
	 * to the entity.
	 *
	 * @param ds
	 * @param e
	 * @throws Exception
	 */
	protected void preUpdateAfterEntity(M ds, E e, P params) throws Exception {
	}

	/**
	 * Post-update event. The entity has been merged by the <code>entityManager</code>, but the possible changes which might alter the entity during
	 * its merging phase are not applied yet to the data-source.
	 *
	 * @param ds
	 * @param e
	 * @throws Exception
	 */
	protected void postUpdateBeforeModel(M ds, E e, P params) throws Exception {
	}

	/**
	 * Post-update event. The entity has been merged by the <code>entityManager</code>, and the possible changes which might alter the entity during
	 * its merging phase are applied to the data-source.
	 *
	 * @param ds
	 * @param e
	 * @throws Exception
	 */
	protected void postUpdateAfterModel(M ds, E e, P params) throws Exception {
	}

	/**
	 * Template method for <code>pre-update</code>.
	 *
	 * @param list
	 * @throws Exception
	 */
	protected void preUpdate(List<M> list, P params) throws Exception {
	}

	public void update(List<M> list, P params) throws Exception {

		if (this.readOnly || this.noUpdate || !this.canUpdate()) {
			throw new ActionNotSupportedException("Update not allowed in data-source " + this.getClass().getCanonicalName());
		}

		this.preUpdate(list, params);

		IEntityService<E> _entityService = this.getEntityService();
		List<E> entities = _entityService.findByIds(this.collectIds(list));

		for (M ds : list) {
			this.preUpdate(ds, params);
			// TODO: optimize me
			E e = this.lookupEntityById(entities, ((IModelWithId<?>) ds).getId());
			this.preUpdateBeforeEntity(ds, e, params);
			ds._setEntity_(e);
			this.getConverter().modelToEntity(ds, e, false, _entityService.getEntityManager());
			this.preUpdateAfterEntity(ds, e, params);

		}
		this.onUpdate(list, entities, params);
		for (M ds : list) {
			E e = _entityService.getEntityManager().find(this.getEntityClass(), ((IModelWithId<?>) ds).getId());
			this.postUpdateBeforeModel(ds, e, params);
			this.getConverter().entityToModel(e, ds, _entityService.getEntityManager(), null);
			this.postUpdateAfterModel(ds, e, params);
		}
		this.postUpdate(list, params);
	}

	protected void onUpdate(List<M> list, List<E> entities, P params) throws BusinessException, Exception {
		try {
			this.getEntityService().update(entities);
		} catch (final TransactionSystemException ex) {
			LOG.info("Ds update fault: ", ex);
			ExceptionProcessor.process(ex);
		}
	}

	/**
	 * Helper update method for one object. It creates a list with this single object and delegates to the <code>update(List<M> list, P params)</code>
	 * method
	 *
	 * @param ds
	 * @throws Exception
	 */
	public void update(M ds, P params) throws Exception {
		List<M> list = new ArrayList<M>();
		list.add(ds);
		this.update(list, params);
	}

	/**
	 * Helper method to find an entity in a list given its ID
	 *
	 * @param list
	 * @param id
	 * @return
	 */
	protected <T> T lookupEntityById(List<T> list, Object id) throws BusinessException {
		for (T e : list) {
			if (((IModelWithId<?>) e).getId().equals(id)) {
				return e;
			}
		}
		throw new BusinessException(J4EPresenterErrorCodes.ENTITY_NOT_EXISTS, J4EPresenterErrorCodes.ENTITY_NOT_EXISTS.getErrMsg());
	}

	/**
	 * Template method for <code>post-update</code>.
	 *
	 * @param list
	 * @throws Exception
	 */
	protected void postUpdate(List<M> list, P params) throws Exception {

	}

	/* ========================== DELETE =========================== */

	/**
	 * Provide custom logic to decide if the action can be executed.
	 */
	protected boolean canDelete() {
		return true;
	}

	/**
	 * Template method for <code>pre-delete</code>.
	 */
	protected void preDelete(Object id) {
	}

	/**
	 * Template method for <code>post-delete</code>.
	 */
	protected void postDelete(Object id) {
	}

	/**
	 * Delete by id.
	 *
	 * @param id
	 * @throws Exception
	 */

	public void deleteById(Object id) throws Exception {
		if (this.readOnly || this.noDelete || !this.canDelete()) {
			throw new ActionNotSupportedException("Delete not allowed in data-source " + this.getClass().getCanonicalName());
		}
		this.preDelete(id);
		try {
			this.getEntityService().deleteById(id);
		} catch (final PersistenceException ex) {
			LOG.info("Ds delete fault: ", ex);
			ExceptionProcessor.process(ex);
		}
		this.postDelete(id);
	}

	protected void preDelete(List<Object> ids) throws Exception {
	}

	protected void postDelete(List<Object> ids) throws Exception {
	}

	/**
	 * Delete by list of ids
	 *
	 * @param ids
	 * @throws Exception
	 */

	public void deleteByIds(List<Object> ids) throws Exception {
		if (this.readOnly || this.noDelete || !this.canDelete()) {
			throw new ActionNotSupportedException("Delete not allowed in data-source " + this.getClass().getCanonicalName());
		}
		this.preDelete(ids);
		try {
			this.getEntityService().deleteByIds(ids);
		} catch (final PersistenceException ex) {
			LOG.info("Ds delete fault: ", ex);
			ExceptionProcessor.process(ex);
		}
		this.postDelete(ids);
	}

	/* ========================== IMPORT =========================== */

	public void doImport(String absoluteFileName, Object config) throws Exception {
		this.doImportAsInsert_(new File(absoluteFileName), config);
	}

	public void doImport(String relativeFileName, String path, Object config) throws Exception {
		this.doImportAsInsert_(new File(path + "/" + relativeFileName), config);
	}

	public void doImport(String absoluteFileName, String ukFieldName, int batchSize, Object config) throws Exception {
		this.doImportAsUpdate_(new File(absoluteFileName), ukFieldName, batchSize, config);
	}

	public void doImport(String relativeFileName, String path, String ukFieldName, int batchSize, Object config) throws Exception {
		this.doImportAsUpdate_(new File(path + "/" + relativeFileName), ukFieldName, batchSize, config);
	}

	public void doImport(InputStream inputStream, String sourceName, Object config) throws Exception {
		this.doImportAsInsert_(inputStream, sourceName, config);
	}

	public void doUpgrade(String absoluteFileName, Object config) throws Exception {
		this.doUpgrade(new File(absoluteFileName), config);
	}

	public void doUpgrade(String relativeFileName, String path, Object config) throws Exception {
		this.doUpgrade(new File(path + "/" + relativeFileName), config);
	}

	protected void doImportAsInsert_(InputStream inputStream, String sourceName, Object config) throws Exception {
		if (this.isReadOnly()) {
			throw new ActionNotSupportedException("Import not allowed.");
		}
		DsCsvLoader l = new DsCsvLoader();
		if (config != null && config instanceof ConfigCsvImport) {
			l.setConfig((ConfigCsvImport) config);
		}
		List<M> list = l.run(inputStream, this.getModelClass(), null, sourceName);
		this.insert(list, null);
		try {
			this.getEntityService().getEntityManager().flush();
		} catch (Exception e) {
			LOGGER.error("Could not flush!", e);
		}
	}

	protected void doImportAsInsert_(File file, Object config) throws Exception {
		if (this.isReadOnly()) {
			throw new ActionNotSupportedException("Import not allowed.");
		}
		DsCsvLoader l = new DsCsvLoader();
		if (config != null && config instanceof ConfigCsvImport) {
			l.setConfig((ConfigCsvImport) config);
		}
		List<M> list = l.run(file, this.getModelClass(), null);
		this.insert(list, null);
		try {
			this.getEntityService().getEntityManager().flush();
		} catch (Exception e) {
			LOGGER.error("Could not flush!", e);
		}
	}

	protected void doUpgrade(File file, Object config) throws Exception {
		if (this.isReadOnly()) {
			throw new ActionNotSupportedException("Import not allowed.");
		}
		DsCsvLoader l = new DsCsvLoader();
		if (config != null && config instanceof ConfigCsvImport) {
			l.setConfig((ConfigCsvImport) config);
		}
		List<M> list = l.run(file, this.getModelClass(), null);

		IEntityService<E> entityService = this.getEntityService();
		Method method = null;
		try {
			method = this.getEntityClass().getMethod("setClientId", String.class);
		} catch (NoSuchMethodException | SecurityException e) {
			// do nothing
			LOGGER.info("Could not get mnethod ,will do nothing!", e);
		}
		boolean hasEquals = false;
		for (Method m : this.getEntityClass().getDeclaredMethods()) {
			if ("equals".equalsIgnoreCase(m.getName())) {
				hasEquals = true;
				break;
			}
		}
		List<E> existingList = new ArrayList<>();
		if (hasEquals && method != null) {
			existingList = entityService.findEntitiesByAttributes(new HashMap<String, Object>());
			for (E e : existingList) {
				method.invoke(e, (String) null);
			}
		}
		List<E> newEntitites = new ArrayList<>();
		for (M ds : list) {
			try {
				E e = entityService.create();
				this.getConverter().modelToEntity(ds, e, true, entityService.getEntityManager());
				if (hasEquals) {
					if (!existingList.contains(e)) {
						newEntitites.add(e);
					}
				} else {
					entityService.insert(e);
				}
			} catch (Exception e) {
				// ignore
				LOG.info("Ds entity not imported: " + ds, e);
			}
		}
		try {
			entityService.insert(newEntitites);
		} catch (Exception e) {
			// ignore
			LOG.info("Entities are not imported: " + newEntitites, e);

		}

	}

	protected void doImportAsUpdate_(File file, String ukFieldName, int batchSize, Object config) throws Exception {
		if (this.isReadOnly()) {
			throw new ActionNotSupportedException("Import not allowed.");
		}
		Assert.notNull(ukFieldName, "For import as update you must specify the unique-key " + "field which is used to lookup the existing record.");

		// TODO: check type-> csv, json, etc
		DsCsvLoader l = new DsCsvLoader();
		if (config != null && config instanceof ConfigCsvImport) {
			l.setConfig((ConfigCsvImport) config);
		}
		DsCsvLoaderResult<M> result = l.run2(file, this.getModelClass(), null);
		List<M> list = result.getResult();
		String[] columns = result.getHeader();

		F filter = this.getFilterClass().newInstance();

		// TODO: optimize me to do the work in batches

		Method filterUkFieldSetter = this.getFilterClass().getMethod("set" + StringUtils.capitalize(ukFieldName), String.class);
		Method modelUkFieldGetter = this.getModelClass().getMethod("get" + StringUtils.capitalize(ukFieldName));

		Map<String, Method> modelSetters = new HashMap<String, Method>();
		Map<String, Method> modelGetters = new HashMap<String, Method>();

		int len = columns.length;

		for (int i = 0; i < len; i++) {
			String fieldName = columns[i];
			Class<?> clz = this.getModelClass();
			Field f = null;
			while (f == null && clz != null) {
				try {
					f = clz.getDeclaredField(fieldName);
				} catch (Exception e) {
					LOGGER.info("Could not get declared field " + fieldName + ", will do nothing !", e);
				}
				clz = clz.getSuperclass();
			}

			if (f != null) {
				Method modelSetter = this.getModelClass().getMethod("set" + StringUtils.capitalize(fieldName), f.getType());
				modelSetters.put(fieldName, modelSetter);

				Method modelGetter = this.getModelClass().getMethod("get" + StringUtils.capitalize(fieldName));
				modelGetters.put(fieldName, modelGetter);
			} else {

			}
		}

		List<M> targets = new ArrayList<M>();

		for (M newDs : list) {
			filterUkFieldSetter.invoke(filter, modelUkFieldGetter.invoke(newDs));
			List<M> res = this.find(filter);
			// TODO: add an extra flag for what to do if the target is not
			// found:
			// ignore or raise an error
			if (res.size() > 0) {
				M oldDs = this.find(filter).get(0);
				for (Map.Entry<String, Method> entry : modelSetters.entrySet()) {
					entry.getValue().invoke(oldDs, modelGetters.get(entry.getKey()).invoke(newDs));
				}
				targets.add(oldDs);
				// this.update(oldDs, null);
			}
		}

		this.update(targets, null);
		try {
			this.getEntityService().getEntityManager().flush();
		} catch (Exception e) {
			LOGGER.error("Could not flush!", e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private <T extends Annotation> Method getByAnnotation(Class clazz, String fName, String prefix, Class... parameterTypes)
			throws NoSuchMethodException, SecurityException {
		Method method = null;
		Method[] methods = clazz.getMethods();
		for (Method m : methods) {
			if (m.getName().equalsIgnoreCase(prefix + StringUtils.capitalize(fName))) {
				method = m;
				break;
			}
		}
		if (method == null && clazz.getSuperclass() != null) {
			this.getByAnnotation(clazz.getSuperclass(), fName, prefix, parameterTypes);
		}
		return method;
	}

	public void doImportAsAssign(String absoluteFileName, Object config, String pField, String cField, String relField, String cDsName)
			throws Exception {
		Class<E> cEntityClass = (Class<E>) this.findDsService(cDsName).getEntityClass();
		if (this.isReadOnly()) {
			throw new ActionNotSupportedException("Import not allowed.");
		}
		DsCsvLoader l = new DsCsvLoader();
		if (config != null && config instanceof ConfigCsvImport) {
			l.setConfig((ConfigCsvImport) config);
		}
		List<DsRelation> list = l.run(new File(absoluteFileName), DsRelation.class, new String[] { "parentCode", "childCode" });
		Class<E> clazz = this.getEntityClass();
		Method setter = this.getByAnnotation(clazz, relField, "set", List.class);
		Method pCodeGetter = this.getByAnnotation(clazz, pField, "get");
		Method cCodeGetter = this.getByAnnotation(cEntityClass, cField, "get");

		Map<String, E> updEntities = this.getEntities(clazz, list, pCodeGetter, true);

		Map<String, E> assEntities = this.getEntities(cEntityClass, list, cCodeGetter, false);
		Map<String, List<E>> map = new HashMap<>();
		for (DsRelation r : list) {
			if (map.containsKey(r.getParentCode())) {
				List<E> eList = map.get(r.getParentCode());
				eList.add(assEntities.get(r.getChildCode()));
				map.put(r.getParentCode(), eList);
			} else {
				List<E> eList = new ArrayList<>();
				eList.add(assEntities.get(r.getChildCode()));
				map.put(r.getParentCode(), eList);
			}
		}

		for (String parentCode : map.keySet()) {
			setter.invoke(updEntities.get(parentCode), map.get(parentCode));
		}

		IEntityService<E> srv = this.findEntityService(clazz);
		srv.update(new ArrayList<>(updEntities.values()));

	}

	private Map<String, E> getEntities(Class<E> clazz, List<DsRelation> list, Method method, boolean isParent)
			throws Exception, IllegalAccessException, InvocationTargetException {
		IEntityService<E> cSrv = this.findEntityService(clazz);
		List<E> entitiesList = cSrv.findEntitiesByAttributes(clazz, new HashMap<String, Object>());
		Map<String, E> entitiesMap = new LinkedHashMap<>();
		for (E e : entitiesList) {
			Object obj = method.invoke(e);
			Iterator<DsRelation> iterator = list.iterator();
			while (iterator.hasNext()) {
				DsRelation r = iterator.next();
				if (isParent) {
					if (r.getParentCode().equalsIgnoreCase((String) obj)) {
						entitiesMap.put(r.getParentCode(), e);
						break;
					}
				} else {
					if (r.getChildCode().equalsIgnoreCase((String) obj)) {
						entitiesMap.put(r.getChildCode(), e);
						break;
					}

				}
			}
		}
		return entitiesMap;
	}

	// ======================== Getters-setters ===========================

	public boolean isNoInsert() {
		return this.noInsert;
	}

	public void setNoInsert(boolean noInsert) {
		this.noInsert = noInsert;
	}

	public boolean isNoUpdate() {
		return this.noUpdate;
	}

	public void setNoUpdate(boolean noUpdate) {
		this.noUpdate = noUpdate;
	}

	public boolean isNoDelete() {
		return this.noDelete;
	}

	public void setNoDelete(boolean noDelete) {
		this.noDelete = noDelete;
	}

	public boolean isReadOnly() {
		return this.readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

}
