/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.action.impex;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.UUID;

import seava.j4e.api.Constants;
import seava.j4e.api.action.impex.IExportInfo;

public abstract class AbstractDsExport<M> {

	private IExportInfo exportInfo;

	private File outFile;
	private String outFileName;
	private String outFilePath;
	protected String outFileExtension;

	protected Writer writer;

	private Map<String, Object> properties;

	private SimpleDateFormat serverDateFormat;

	public AbstractDsExport() {
		super();
		this.init();
	}

	public abstract void write(M data, boolean isFirst) throws Exception;

	private void init() {
		this.serverDateFormat = new SimpleDateFormat(Constants.get_server_datetime_format());

		if (this.outFileName == null) {
			this.outFileName = UUID.randomUUID().toString();
		}

	}

	public SimpleDateFormat getServerDateFormat() {
		return this.serverDateFormat;
	}

	public void setServerDateFormat(SimpleDateFormat serverDateFormat) {
		this.serverDateFormat = serverDateFormat;
	}

	public void begin() throws Exception {
		this.openWriter();
		this.beginContent();
	}

	public void end() throws Exception {
		this.endContent();
		this.closeWriter();
	}

	protected abstract void beginContent() throws Exception;

	protected abstract void endContent() throws Exception;

	private void openWriter() throws Exception {
		if (this.outFile == null) {
			if (this.outFilePath == null || this.outFileName == null || this.outFileExtension == null) {
				throw new Exception("Either a File or a file-path, file-name and file-extension must be provided");
			}
			File dir = new File(this.outFilePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			this.outFile = File.createTempFile(this.outFileName, "." + this.outFileExtension, dir);
		}
		// FileWriter fstream = new FileWriter(this.outFile);
		// this.writer = new BufferedWriter(fstream);
		this.writer = new OutputStreamWriter(new FileOutputStream(this.outFile), "UTF-8");

	}

	private void closeWriter() throws IOException {
		this.writer.flush();
		this.writer.close();
	}

	public IExportInfo getExportInfo() {
		return this.exportInfo;
	}

	public void setExportInfo(IExportInfo exportInfo) {
		this.exportInfo = exportInfo;
	}

	public File getOutFile() {
		return this.outFile;
	}

	public void setOutFile(File outFile) {
		this.outFile = outFile;
	}

	public String getOutFilePath() {
		return this.outFilePath;
	}

	public void setOutFilePath(String outFilePath) {
		this.outFilePath = outFilePath;
	}

	public String getOutFileName() {
		return this.outFileName;
	}

	public void setOutFileName(String outFileName) {
		this.outFileName = outFileName;
	}

	public String getOutFileExtension() {
		return this.outFileExtension;
	}

	public Map<String, Object> getProperties() {
		return this.properties;
	}

	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

}
