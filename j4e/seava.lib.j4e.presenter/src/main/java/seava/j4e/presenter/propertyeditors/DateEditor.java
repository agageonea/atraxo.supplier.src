/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.propertyeditors;

import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.j4e.api.Constants;

public class DateEditor extends PropertyEditorSupport {

	static final Logger LOGGER = LoggerFactory.getLogger(DateEditor.class);

	private String mask;
	private String altMask;

	private SimpleDateFormat format;
	private List<SimpleDateFormat> altFormats;

	public DateEditor() {
		super();
		this.initDefaults();
	}

	public DateEditor(Object source) {
		super(source);
		this.initDefaults();
	}

	private void initDefaults() {

		this.mask = Constants.get_server_date_format();
		this.altMask = Constants.get_server_alt_formats();
		this.format = new SimpleDateFormat(this.mask);

		this.altFormats = new ArrayList<SimpleDateFormat>();
		String[] t = this.altMask.split(";");
		for (int i = 0, len = t.length; i < len; i++) {
			this.altFormats.add(new SimpleDateFormat(t[i]));
		}
	}

	@Override
	public String getAsText() {
		return this.format.format((Date) this.getValue());
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (text == null || text.equals("")) {
			this.setValue(null);
		} else {
			boolean ok = false;
			for (SimpleDateFormat df : this.altFormats) {
				try {
					this.setValue(df.parse(text));
					ok = true;
					break;
				} catch (Exception e) {
					// ignore and try the next one
					if (LOGGER.isTraceEnabled()) {
						LOGGER.trace("Could not parse, will ignore and try the next one !", e);
					}
				}
			}
			if (!ok) {
				throw new IllegalArgumentException("Date value [" + text + "] doesn't match any of the expected formats:  [" + this.altMask + "]");
			}
		}
	}

}
