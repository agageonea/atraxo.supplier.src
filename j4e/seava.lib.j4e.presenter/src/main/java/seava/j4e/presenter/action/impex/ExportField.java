package seava.j4e.presenter.action.impex;

import java.lang.reflect.Method;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "fieldGetterName", "fieldGetter" })
public class ExportField {

	private String name;
	private String title;
	private String width;
	private String mask;
	private String type = "string";
	private String align = "left";

	@JsonIgnore
	private String fieldGetterName;

	@JsonIgnore
	private Method fieldGetter;

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWidth() {
		return this.width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getMask() {
		return this.mask;
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

	public String getAlign() {
		return this.align;
	}

	public void setAlign(String align) {
		this.align = align;
	}

	public Method _getFieldGetter() {
		return this.fieldGetter;
	}

	public void _setFieldGetter(Method fieldGetter) {
		this.fieldGetter = fieldGetter;
	}

}
