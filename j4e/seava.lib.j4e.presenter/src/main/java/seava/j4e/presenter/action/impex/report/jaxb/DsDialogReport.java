package seava.j4e.presenter.action.impex.report.jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import seava.j4e.api.action.impex.IDsReport;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.presenter.action.impex.DialogReportData;
import seava.j4e.presenter.action.impex.DialogReportInfo;
import seava.j4e.presenter.model.jaxb.report.DialogReport;
import seava.j4e.presenter.model.jaxb.report.Parameter;

public class DsDialogReport<M, F> extends DsAbstractReport<M, F> implements IDsReport<M, F> {

	public DsDialogReport() throws BusinessException {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(DialogReport.class, Parameter.class);
			this.jaxbMarshaller = jaxbContext.createMarshaller();
			this.jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			this.report = new DialogReport();
		} catch (JAXBException e) {
			throw new BusinessException(ErrorCode.XML_MARSHALL_ERROR, e);
		}
	}

	public void setCode(String code) {
		((DialogReport) this.report).setCode(code);
	}

	public void setFormat(String format) {
		((DialogReport) this.report).setFormat(format);
	}

	private void setTitle(String title) {
		((DialogReport) this.report).setTitle(title);
	}

	public void setParameter(List<Parameter> list) {
		((DialogReport) this.report).setParamList(list);
	}

	public void addParameter(Parameter parameter) {
		DialogReport dialogReport = (DialogReport) this.report;
		List<Parameter> list = dialogReport.getParamList();
		if (list == null) {
			list = new ArrayList<Parameter>();
			dialogReport.setParamList(list);
		}
		list.add(parameter);

	}

	public void setData(DialogReportInfo info) throws Exception {
		this.setTitle(info.getTitle());
		List<Parameter> list = new ArrayList<Parameter>();
		for (DialogReportData reportData : info.getDataList()) {
			Parameter param = new Parameter();
			param.setName(reportData.getName());
			param.setType(reportData.getType());
			param.setValue(reportData.getValue());
			list.add(param);
		}
		this.setParameter(list);
	}

	public String getCode() {
		return ((DialogReport) this.report).getCode();
	}
}
