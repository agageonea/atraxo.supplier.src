/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.service.ds;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.jpa.JpaQuery;
import org.eclipse.persistence.queries.Cursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import seava.j4e.api.action.impex.IDialogReportInfo;
import seava.j4e.api.action.impex.IDsExport;
import seava.j4e.api.action.impex.IDsReport;
import seava.j4e.api.action.impex.IExportInfo;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.query.ISortToken;
import seava.j4e.api.service.presenter.IDsReadService;
import seava.j4e.api.service.report.IReportService;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.impex.DialogReportInfo;
import seava.j4e.presenter.action.impex.report.jaxb.DsDialogReport;
import seava.j4e.presenter.action.impex.report.jaxb.DsDynamicReport;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.presenter.model.AbstractDsModel;

/**
 * Implements the read actions for an entity-ds. See the super-classes for more details.
 *
 * @author amathe
 * @param <M>
 * @param <F>
 * @param <P>
 * @param <E>
 */
public abstract class AbstractEntityDsReadService<M extends AbstractDsModel<E>, F, P, E> extends AbstractEntityDsBaseService<M, F, P, E>
		implements IDsReadService<M, F, P> {

	private static final int DEFAULT_RESULT_START = 0;
	private static final int DEFAULT_RESULT_SIZE = 10000;

	static final Logger LOGGER = LoggerFactory.getLogger(AbstractEntityDsReadService.class);

	private Map<String, String> summaryRules;

	public Map<String, String> getSummaryRules() {
		return this.summaryRules;
	}

	public void setSummaryRules(Map<String, String> summaryRules) {
		this.summaryRules = summaryRules;
	}

	/* ========================== QUERY =========================== */

	/**
	 * Count results for the given query context.
	 *
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	@Override
	public Long count(IQueryBuilder<M, F, P> builder) throws Exception {
		if (builder == null) {
			throw new Exception("Cannot run a count query with null query builder.");
		}
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;
		Object count = bld.createQueryCount().getSingleResult();
		if (count instanceof Integer) {
			return ((Integer) count).longValue();
		} else {
			return (Long) count;
		}
	}

	/**
	 * Summaries and aggregate values for the given query context.
	 *
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> summaries(IQueryBuilder<M, F, P> builder, List<M> findResult) throws Exception {
		if (builder == null) {
			throw new Exception("Cannot run a summary query with null query builder.");
		}
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;
		Map<String, Object> sum = new HashMap<String, Object>();
		if (bld.getBaseEqlSummary() != null) {
			List /* <Object[]> */ result = bld.createQuerySummary().getResultList();

			// List res = bld
			// .getEntityManager() //.createNativeQuery(sqlString, resultClass)
			// .createQuery(
			// // "select count(e.country.id) , "
			// // + " avg(e.country.id) , "
			// // + " sum(e.id) ,count(e.id) "
			// // + " from Country e left outer join e.country cRef order by cRef.code")
			// "select count(e.id), count(b.id), count(e.code), avg(e.id) " //, count(e.code), avg(e.id), count(e.country.id)
			// + "from Country e left join e.country as b")
			// .getResultList();

			SortedSet<String> keys = new TreeSet<String>(this.getSummaryRules().keySet());
			if (keys.size() > 1) {
				Object[] o = (Object[]) result.get(0);
				int i = 0;
				for (String key : keys) {
					if (i < o.length) {
						sum.put(key, o[i]);
						i++;
					} else {
						break;
					}
				}
			} else {
				Object o = result.get(0);
				sum.put(keys.first(), o);
			}
		}
		this.postSummaries(builder, findResult, sum);
		return sum;
	}

	/**
	 * @param builder
	 * @param findResult
	 * @param sum
	 * @throws Exception
	 */
	protected void postSummaries(IQueryBuilder<M, F, P> builder, List<M> findResult, Map<String, Object> sum) throws Exception {
	}

	@Override
	public List<M> find(F filter) throws Exception {
		return this.find(filter, null, null, DEFAULT_RESULT_START, DEFAULT_RESULT_SIZE, null);
	}

	@Override
	public List<M> find(F filter, int resultStart, int resultSize) throws Exception {
		return this.find(filter, null, null, resultStart, resultSize, null);
	}

	@Override
	public List<M> find(F filter, P params) throws Exception {
		return this.find(filter, params, null, DEFAULT_RESULT_START, DEFAULT_RESULT_SIZE, null);
	}

	@Override
	public List<M> find(F filter, P params, int resultStart, int resultSize) throws Exception {
		return this.find(filter, params, null, resultStart, resultSize, null);
	}

	@Override
	public List<M> find(F filter, List<IFilterRule> filterRules) throws Exception {
		return this.find(filter, null, filterRules, DEFAULT_RESULT_START, DEFAULT_RESULT_SIZE, null);
	}

	@Override
	public List<M> find(F filter, List<IFilterRule> filterRules, int resultStart, int resultSize) throws Exception {
		return this.find(filter, null, filterRules, resultStart, resultSize, null);
	}

	@Override
	public List<M> find(F filter, P params, List<IFilterRule> filterRules) throws Exception {
		return this.find(filter, params, filterRules, DEFAULT_RESULT_START, DEFAULT_RESULT_SIZE, null);
	}

	@Override
	public List<M> find(F filter, P params, List<IFilterRule> filterRules, int resultStart, int resultSize) throws Exception {
		return this.find(filter, params, filterRules, resultStart, resultSize, null);
	}

	@Override
	public List<M> find(F filter, P params, List<IFilterRule> filterRules, List<ISortToken> sortTokens) throws Exception {
		return this.find(filter, params, filterRules, DEFAULT_RESULT_START, DEFAULT_RESULT_SIZE, sortTokens);
	}

	@Override
	public List<M> find(F filter, P params, List<IFilterRule> filterRules, int resultStart, int resultSize, List<ISortToken> sortTokens)
			throws Exception {
		QueryBuilderWithJpql<M, F, P> bld = null;
		bld = (QueryBuilderWithJpql<M, F, P>) this.createQueryBuilder();
		bld.setFilter(filter);
		bld.setParams(params);
		bld.setFilterRules(filterRules);
		bld.addFetchLimit(resultStart, resultSize);
		return this.find(bld);
	}

	@Override
	public List<M> find(IQueryBuilder<M, F, P> builder) throws Exception {
		return this.find(builder, null);
	}

	@Override
	public List<M> find(IQueryBuilder<M, F, P> builder, List<String> fieldNames) throws Exception {

		if (builder == null) {
			throw new Exception("Cannot run a query with null query builder.");
		}
		this.preFind(builder);
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;

		List<E> entities = bld.createQuery(this.getEntityClass()).setFirstResult(bld.getResultStart()).setMaxResults(bld.getResultSize())
				.getResultList();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Found {} results. Applying entity-to-model conversion ( {} -> {} ) for the result.",
					new Object[] { entities.size() + "", this.getEntityClass().getSimpleName(), this.getModelClass().getSimpleName() });
		}

		List<M> result = this.getConverter().entitiesToModels(entities, bld.getEntityManager(), fieldNames);

		this.postFind(builder, result);
		return result;
	}

	/**
	 * Template method for pre-query.
	 *
	 * @param filter
	 * @param params
	 * @param builder
	 * @throws Exception
	 */
	protected void preFind(IQueryBuilder<M, F, P> builder) throws Exception {
	}

	protected void postFind(IQueryBuilder<M, F, P> builder, List<M> result) throws Exception {
	}

	/**
	 * Find one result by ID
	 *
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Override
	public M findById(Object id) throws Exception {
		return this.findById(id, this.getParamClass().newInstance());
	}

	/**
	 * Find one result by ID
	 *
	 * @param id
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@Override
	public M findById(Object id, P params) throws Exception {
		Method setter = this.getFilterClass().getMethod("setId", Object.class);
		F filter = this.getFilterClass().newInstance();
		setter.invoke(filter, id);
		List<M> result = this.find(filter, params, null);
		if (result.size() == 0) {
			return null;
		} else {
			return result.get(0);
		}
	}

	/**
	 * Find results by a list of IDs
	 *
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<M> findByIds(List<Object> ids) throws Exception {
		// TODO Implement me !
		return null;
	}

	/* ========================== EXPORT =========================== */
	/**
	 * Set data using dynamic export.
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Async
	public void doReport(IQueryBuilder<M, F, P> builder, IDsReport<M, F> writer, IExportInfo exportInfo, IUser user) throws Exception {
		Session.user.set(user);
		List<M> datas = this.find(builder);
		((DsDynamicReport) writer).setData(datas, exportInfo);
		IReportService reportService = this.getApplicationContext().getBean("reportService", IReportService.class);

		String reportXML = ((DsDynamicReport) writer).toXmlString();
		reportService.generateDialogReport(reportXML);
	}

	@Override
	public void doDialogReport(IDsReport<M, F> report, IDialogReportInfo reportInfo) throws Exception {
		((DsDialogReport<M, F>) report).setData((DialogReportInfo) reportInfo);
	}

	/* ========================== EXPORT =========================== */
	/**
	 * Export data
	 *
	 * @param filter
	 * @param params
	 * @param builder
	 * @param writer
	 * @throws Exception
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void doExport(IQueryBuilder<M, F, P> builder, IDsExport<M> writer) throws Exception {

		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;
		bld.setForExport(true);

		EntityManager em = bld.getEntityManager().getEntityManagerFactory().createEntityManager();
		bld.setEntityManager(em);

		try {

			Query q = bld.createQuery(this.getEntityClass()).setHint(QueryHints.CURSOR, true).setHint(QueryHints.CURSOR_INITIAL_SIZE, 100)
					.setHint(QueryHints.CURSOR_PAGE_SIZE, 100).setHint(QueryHints.READ_ONLY, HintValues.TRUE).setFirstResult(bld.getResultStart())
					.setMaxResults(bld.getResultSize());

			Cursor c = q.unwrap(JpaQuery.class).getResultCursor();

			M ds;
			writer.begin();
			boolean isFirst = true;
			while (c.hasMoreElements()) {
				ds = this.getModelClass().newInstance();
				this.getConverter().entityToModel((E) c.nextElement(), ds, bld.getEntityManager(), null);
				writer.write(ds, isFirst);
				isFirst = false;
			}
			writer.end();
			c.close();
		} finally {
			em.close();
		}

	}

	/**
	 * Create a new query-builder instance.
	 *
	 * @return
	 * @throws Exception
	 */
	@Override
	public IQueryBuilder<M, F, P> createQueryBuilder() throws Exception {
		IQueryBuilder<M, F, P> qb = null;
		if (this.getQueryBuilderClass() == null) {
			qb = new QueryBuilderWithJpql<M, F, P>();
		} else {
			qb = this.getQueryBuilderClass().newInstance();
		}
		this._prepareQueryBuilder(qb);
		return qb;
	}

	/**
	 * Prepare the query builder injecting necessary dependencies.
	 *
	 * @param qb
	 * @throws Exception
	 */
	private void _prepareQueryBuilder(IQueryBuilder<M, F, P> qb) throws Exception {
		qb.setModelClass(this.getModelClass());
		qb.setFilterClass(this.getFilterClass());
		qb.setParamClass(this.getParamClass());
		qb.setDescriptor(this.getDescriptor());
		qb.setSettings(this.getSettings());
		if (qb instanceof QueryBuilderWithJpql) {
			QueryBuilderWithJpql<M, F, P> jqb = (QueryBuilderWithJpql<M, F, P>) qb;
			jqb.setEntityManager(this.getEntityService().getEntityManager());
			jqb.setBaseEql("select e from " + this.getEntityClass().getSimpleName() + " e");
			jqb.setBaseEqlCount("select count(1) from " + this.getEntityClass().getSimpleName() + " e");

			StringBuffer sumFields = new StringBuffer();
			Map<String, String> sRules = this.getSummaryRules();
			// HashSet<String> sRules = this.getSummaryRules();

			// ONLY FOR TEST !!!!!!!!!!!!!!!!!!!!!!!! TIBI
			// D:\work2\projects\mod\atraxo-fmbas\atraxo.fmbas.src\atraxo.mod.fmbas.presenter\src\main\resources\META-INF\spring\beans\fmbas-geo-presenter.xml
			// <bean id="fmbas_Country_Ds"
			// <property name="summaryRules">
			// <map>
			// <entry key="name" value="sum(e.id)">
			// </entry>
			// <entry key="continent" value="count(e.name)">
			// </entry>
			// </map>
			// </property>
			// sRules = rules;
			// this.setSummaryRules(rules);

			if (sRules != null) {
				SortedSet<String> keys = new TreeSet<String>(sRules.keySet());
				for (String key : keys) {
					String value = sRules.get(key);
					if (sumFields.length() > 0) {
						sumFields.append(", ");
					}
					sumFields.append(value);
				}
				// for (Map.Entry<String, String> entry : sRules.entrySet()) {
				// // for (String entry : sRules) {
				// // String key = entry.getKey();
				// String value = entry.getValue();
				// if (sumFields.length() > 0) {
				// sumFields.append(", ");
				// }
				// sumFields.append(value);
				// // sumFields.append(entry);
				// }
			}
			if (sumFields.length() > 0) {
				jqb.setBaseEqlSummary("select " + sumFields.toString() + " from " + this.getEntityClass().getSimpleName() + " e");
			} else {
				jqb.setBaseEqlSummary(null);
			}

			if (this.getDescriptor().isWorksWithJpql()) {
				jqb.setDefaultWhere(this.getDescriptor().getJpqlDefaultWhere());
				jqb.setDefaultSort(this.getDescriptor().getJpqlDefaultSort());
			}
		}
	}

	@Override
	public IDsExport<M> createExporter(String dataFormat) throws Exception {
		return null;
	}
}
