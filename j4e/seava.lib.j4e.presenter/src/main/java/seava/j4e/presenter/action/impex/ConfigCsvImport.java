/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.action.impex;

public class ConfigCsvImport {

	private char separator = ',';
	private char quoteChar = '"';
	private String encoding = "UTF-8";

	public ConfigCsvImport() {
		super();
	}

	public ConfigCsvImport(char separator, char quoteChar, String encoding) {
		super();
		this.separator = separator;
		this.quoteChar = quoteChar;
		this.encoding = encoding;
	}

	public char getSeparator() {
		return this.separator;
	}

	public void setSeparator(char separator) {
		this.separator = separator;
	}

	public char getQuoteChar() {
		return this.quoteChar;
	}

	public void setQuoteChar(char quoteChar) {
		this.quoteChar = quoteChar;
	}

	public String getEncoding() {
		return this.encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

}
