package seava.j4e.presenter.model.jaxb.report;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NumberFormat {

	private char decSep;
	private char thSel;

	public String getDecSep() {
		return String.valueOf(this.decSep);
	}

	@XmlElement
	public void setDecSep(String decSep) {
		this.decSep = decSep.charAt(0);
	}

	public String getThSel() {
		return String.valueOf(this.thSel);
	}

	@XmlElement
	public void setThSel(String thSel) {
		this.thSel = thSel.charAt(0);
	}

}
