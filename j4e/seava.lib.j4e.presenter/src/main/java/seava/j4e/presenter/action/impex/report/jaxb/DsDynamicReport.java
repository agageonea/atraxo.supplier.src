package seava.j4e.presenter.action.impex.report.jaxb;

import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;

import seava.j4e.api.action.impex.IDsReport;
import seava.j4e.api.action.impex.IExcludeInfo;
import seava.j4e.api.action.impex.IExportInfo;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.enums.DateFormatAttribute;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.impex.ExportField;
import seava.j4e.presenter.action.impex.ExportInfo;
import seava.j4e.presenter.action.impex.report.jaxb.wrappers.MapList;
import seava.j4e.presenter.model.jaxb.report.ColumnDef;
import seava.j4e.presenter.model.jaxb.report.DynamicReport;
import seava.j4e.presenter.model.jaxb.report.NumberFormat;
import seava.j4e.presenter.model.jaxb.report.QueryParam;
import seava.j4e.presenter.model.jaxb.report.ReportData;
import seava.j4e.presenter.model.jaxb.report.ReportSectionUrl;
import seava.j4e.presenter.model.jaxb.report.Section;

public class DsDynamicReport<M, F> extends DsAbstractReport<M, F> implements IDsReport<M, F> {

	private static final String QUERY_PARAMETERS = "Query Parameters: ";
	private static final String DATA_TYPE = "inline";
	private static final String LAYOUT_TYPE = "OnColumns";

	private ReportData reportData;
	private ReportSectionUrl url;

	public DsDynamicReport() throws BusinessException {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(ReportData.class, ColumnDef.class, QueryParam.class, DynamicReport.class, Section.class,
					MapList.class);
			this.jaxbMarshaller = jaxbContext.createMarshaller();
			this.jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			this.reportData = new ReportData();
			this.report = new DynamicReport();

			List<Section> sections = new ArrayList<>();
			Section section = new Section();
			section.setLayoutType(LAYOUT_TYPE);
			section.setOrder(0);
			this.url = new ReportSectionUrl();
			this.url.setType(DATA_TYPE);
			section.setUrl(this.url);
			sections.add(section);
			section.setTitle(QUERY_PARAMETERS);
			((DynamicReport) this.report).setSections(sections);

		} catch (JAXBException e) {
			throw new BusinessException(ErrorCode.XML_MARSHALL_ERROR, e);
		}
	}

	public void setReportFormat(String format) {
		((DynamicReport) this.report).setFormat(format);
	}

	public void setReportCode(String code) {
		((DynamicReport) this.report).setCode(code);
	}

	public void setNumberFormat() {
		NumberFormat format = new NumberFormat();
		format.setDecSep(Session.user.get().getSettings().getDecimalSeparator());
		format.setThSel(Session.user.get().getSettings().getThousandSeparator());

		this.reportData.setNumberFormat(format);
	}

	public void setData(List<M> datas, IExportInfo exportInfo) throws Exception {
		List<ColumnDef> columnDefs = this.buildColumnDef(exportInfo);
		List<Map<String, String>> records = this.buildRecords(datas, exportInfo);

		this.reportData.setColumnDef(columnDefs);
		this.reportData.setRecords(records);
		this.reportData.setTitle(((ExportInfo) exportInfo).getTitle());
		this.reportData.setBy(Session.user.get().getCode());
		this.reportData.setCode(((ExportInfo) exportInfo).getTitle());
		this.setNumberFormat();
		this.url.setReportData(this.getReportDataAsXmlString());
	}

	public void addFilterParams(F filter, IExcludeInfo info) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		List<QueryParam> queryParams = new ArrayList<>();
		for (Method method : filter.getClass().getMethods()) {
			String methodName = method.getName();

			if (methodName.startsWith("get") && method.getDeclaringClass().equals(filter.getClass())) {
				if (info.getFields().contains(StringUtils.uncapitalise(methodName.substring(3)))) {
					continue;
				}
				String returnType = method.getReturnType().getName();
				Object value = method.invoke(filter);
				if (value != null) {
					QueryParam param = new QueryParam();
					param.setName(methodName.substring(3));
					param.setType(returnType);
					param.setValue(value.toString());
					queryParams.add(param);
				}
			}
		}
		if (this.reportData.getQueryParams() != null) {
			this.reportData.getQueryParams().addAll(queryParams);
		}
	}

	public void addAdvancedFilterPArams(List<IFilterRule> advancedFilterList) {
		List<QueryParam> queryParams = new ArrayList<>();
		for (IFilterRule filterRule : advancedFilterList) {
			QueryParam param = new QueryParam();
			param.setName(filterRule.getFieldName());
			param.setValue(filterRule.getValue1());
			queryParams.add(param);

		}
		if (this.reportData.getQueryParams() != null) {
			this.reportData.getQueryParams().addAll(queryParams);
		}
	}

	private List<ColumnDef> buildColumnDef(IExportInfo iExportInfo) throws Exception {
		List<ColumnDef> columnDefs = new LinkedList<>();
		ExportInfo exportInfo = (ExportInfo) iExportInfo;
		for (ExportField exportField : exportInfo.getColumns()) {
			ColumnDef columnDef = new ColumnDef();
			columnDef.setName(exportField.getName());
			columnDef.setLabel(exportField.getTitle());
			columnDef.setWidth(exportField.getWidth() == null ? 100 : Integer.parseInt(exportField.getWidth()));
			columnDef.setType(exportField.getType());
			columnDef.setAlign(exportField.getAlign());
			if (!StringUtils.isEmpty(exportField.getMask())) {
				columnDef.setFmt(
						Session.user.get().getSettings().getDateFormatMask(DateFormatAttribute.valueOf("JAVA_" + exportField.getMask()).name()));
			}
			columnDefs.add(columnDef);

		}
		return columnDefs;
	}

	private List<Map<String, String>> buildRecords(List<M> datas, IExportInfo iExportInfo)
			throws Exception, IllegalAccessException, InvocationTargetException {
		List<Map<String, String>> records = new LinkedList<>();
		ExportInfo exportInfo = (ExportInfo) iExportInfo;
		for (M data : datas) {
			Map<String, String> map = new LinkedHashMap<>();
			for (ExportField exportField : exportInfo.getColumns()) {
				String tag = exportField.getName();
				if (tag == null) {
					continue;
				}
				Method getter = exportField._getFieldGetter();
				if (getter != null) {
					Object obj = getter.invoke(data);
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					if (obj != null) {
						if (obj instanceof Date) {
							map.put(tag, df.format((Date) obj));
						} else {
							if (obj instanceof Boolean) {
								map.put(tag, (Boolean) obj ? "Yes" : "No");
							} else {
								map.put(tag, obj.toString());
							}
						}
					} else {
						map.put(tag, "");
					}
				} else {
					map.put(tag, "");
				}
			}
			records.add(map);
		}
		return records;
	}

	public String getReportDataAsXmlString() throws BusinessException {
		try {
			Writer writer = new StringWriter();
			this.jaxbMarshaller.marshal(this.reportData, writer);
			return writer.toString();
		} catch (JAXBException e) {
			throw new BusinessException(ErrorCode.XML_MARSHALL_ERROR, e);
		}
	}
}
