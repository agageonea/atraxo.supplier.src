package seava.j4e.presenter.model.jaxb.report;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ColumnDef {

	private String name;
	private String label;
	private String align;
	private int width;
	private String type;
	private String fmt;

	public String getName() {
		return this.name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return this.label;
	}

	@XmlElement
	public void setLabel(String label) {
		this.label = label;
	}

	public String getAlign() {
		return this.align;
	}

	@XmlElement
	public void setAlign(String align) {
		this.align = align;
	}

	public int getWidth() {
		return this.width;
	}

	@XmlElement
	public void setWidth(int width) {
		this.width = width;
	}

	public String getType() {
		return this.type;
	}

	@XmlElement
	public void setType(String type) {
		this.type = type;
	}

	public String getFmt() {
		return this.fmt;
	}

	@XmlElement
	public void setFmt(String fmt) {
		this.fmt = fmt;
	}

}
