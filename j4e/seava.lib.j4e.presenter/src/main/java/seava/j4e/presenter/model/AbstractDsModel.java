/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.model;

public abstract class AbstractDsModel<E> {

	/**
	 * Required when inserting a list of objects to be able to identify the entity as there is no id yet.
	 */
	private E _entity_;

	/**
	 * Field to transport fake ID's generated on client-side (user interface). If more than one new record is sent to server to be inserted, the
	 * client doesn't know how to match the records received in response with those sent. This field is used to identify the result.
	 */
	private String __clientRecordId__;

	/**
	 * Some informations to manipulate SQL when creating the query
	 */
	private String __filterAttrString__;

	public AbstractDsModel() {
	}

	public AbstractDsModel(E e) {
		this._entity_ = e;
	}

	public E _getEntity_() {
		return this._entity_;
	}

	public void _setEntity_(E entity) {
		this._entity_ = entity;
	}

	public String get__clientRecordId__() {
		return this.__clientRecordId__;
	}

	public void set__clientRecordId__(String clientRecordId) {
		this.__clientRecordId__ = clientRecordId;
	}

	public String get__filterAttrString__() {
		return this.__filterAttrString__;
	}

	public void set__filterAttrString__(String __filterAttrString__) {
		this.__filterAttrString__ = __filterAttrString__;
	}

}
