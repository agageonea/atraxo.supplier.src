package seava.j4e.presenter.model.jaxb.report;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
public class ReportSectionUrl {

	private String type;
	private String reportData;

	@XmlAttribute
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlValue
	public String getReportData() {
		return this.reportData;
	}

	public void setReportData(String reportData) {
		this.reportData = reportData;
	}

}
