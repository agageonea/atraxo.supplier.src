package seava.j4e.presenter.action.impex;

import java.util.ArrayList;
import java.util.List;

import seava.j4e.api.action.impex.IExcludeInfo;

public class ExcludeInfo implements IExcludeInfo {

	private List<String> fields;

	public ExcludeInfo() {
		this.fields = new ArrayList<>();
	}

	public List<String> getFields() {
		return this.fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

}
