package seava.j4e.presenter.model.jaxb.report;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import seava.j4e.presenter.action.impex.report.jaxb.adapters.ColumnListAdapter;
import seava.j4e.presenter.action.impex.report.jaxb.adapters.QueryParamListAdapter;
import seava.j4e.presenter.action.impex.report.jaxb.adapters.RecordAdapter;

@XmlRootElement
public class ReportData {

	private String code;
	private String layout;
	private String title;
	private String by;
	private Date on;
	private NumberFormat numberFormat;
	private List<QueryParam> queryParams;
	private List<ColumnDef> columnDef;
	private List<Map<String, String>> records;

	public ReportData() {
		this.numberFormat = new NumberFormat();
		this.queryParams = new LinkedList<>();
		this.columnDef = new LinkedList<>();
		this.records = new LinkedList<>();
	}

	public String getCode() {
		return this.code;
	}

	@XmlAttribute
	public void setCode(String code) {
		this.code = code;
	}

	public String getLayout() {
		return this.layout;
	}

	@XmlAttribute
	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getTitle() {
		return this.title;
	}

	@XmlAttribute
	public void setTitle(String title) {
		this.title = title;
	}

	public String getBy() {
		return this.by;
	}

	@XmlAttribute
	public void setBy(String by) {
		this.by = by;
	}

	public Date getOn() {
		return this.on;
	}

	@XmlAttribute
	public void setOn(Date on) {
		this.on = on;
	}

	@XmlJavaTypeAdapter(ColumnListAdapter.class)
	public List<ColumnDef> getColumnDef() {
		return this.columnDef;
	}

	@XmlElement
	public void setColumnDef(List<ColumnDef> columnDef) {
		this.columnDef = columnDef;
	}

	public void setNumberFormat(NumberFormat numberFormat) {
		this.numberFormat = numberFormat;
	}

	@XmlElement
	public void setQueryParams(List<QueryParam> queryParams) {
		this.queryParams = queryParams;
	}

	@XmlElement
	public void setRecords(List<Map<String, String>> records) {
		this.records = records;
	}

	public NumberFormat getNumberFormat() {
		return this.numberFormat;
	}

	@XmlJavaTypeAdapter(QueryParamListAdapter.class)
	public List<QueryParam> getQueryParams() {
		return this.queryParams;
	}

	@XmlJavaTypeAdapter(RecordAdapter.class)
	public List<Map<String, String>> getRecords() {
		return this.records;
	}

}
