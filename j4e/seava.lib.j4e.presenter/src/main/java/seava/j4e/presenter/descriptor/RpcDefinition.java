/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.descriptor;

import seava.j4e.presenter.service.AbstractPresenterBaseService;

public class RpcDefinition {

	private final Class<? extends AbstractPresenterBaseService> delegateClass;
	private final String methodName;
	private boolean reloadFromEntity;
	private boolean entityShouldExists;

	public RpcDefinition(Class<? extends AbstractPresenterBaseService> delegateClass, String methodName) {
		super();
		this.delegateClass = delegateClass;
		this.methodName = methodName;
	}

	public RpcDefinition(Class<? extends AbstractPresenterBaseService> delegateClass, String methodName, boolean reloadFromEntity) {
		super();
		this.delegateClass = delegateClass;
		this.methodName = methodName;
		this.reloadFromEntity = reloadFromEntity;
	}

	public RpcDefinition(Class<? extends AbstractPresenterBaseService> delegateClass, String methodName, boolean reloadFromEntity,
			boolean entityShouldExists) {
		super();
		this.delegateClass = delegateClass;
		this.methodName = methodName;
		this.reloadFromEntity = reloadFromEntity;
		this.entityShouldExists = entityShouldExists;
	}

	public Class<? extends AbstractPresenterBaseService> getDelegateClass() {
		return this.delegateClass;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public boolean getReloadFromEntity() {
		return this.reloadFromEntity;
	}

	public boolean isEntityShouldExists() {
		return this.entityShouldExists;
	}

}