package seava.j4e.presenter.service.ds;

import seava.j4e.api.action.impex.IImportDataFile;
import seava.j4e.api.service.presenter.IImportDataFileService;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

public class ImportDataFileService extends AbstractPresenterBaseService implements IImportDataFileService {

	/**
	 * Import one data-file from a data-package. Locate the proper ds-service to delegate the work to
	 */
	@Override
	public void execute(IImportDataFile dataFile) throws Exception {

		String dsName = dataFile.getDs();
		String fileName = dataFile.getFile();
		if (IImportDataFile.ImportAction.UPGRADE.equals(dataFile.getAction())) {
			this.findDsService(dsName).doUpgrade(fileName, null);
		} else {
			if (dataFile.getcDs() != null) {
				this.findDsService(dsName).doImportAsAssign(fileName, null, dataFile.getUkFieldName(), dataFile.getcUkFieldName(),
						dataFile.getRelFieldName(), dataFile.getcDs());
			} else if (dataFile.getUkFieldName() != null && !dataFile.getUkFieldName().equals("")) {
				this.findDsService(dsName).doImport(fileName, dataFile.getUkFieldName(), 0, null);
			} else {
				this.findDsService(dsName).doImport(fileName, null);
			}
		}
	}
}