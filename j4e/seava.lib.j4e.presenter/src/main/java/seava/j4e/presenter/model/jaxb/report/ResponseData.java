package seava.j4e.presenter.model.jaxb.report;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RunReportResult")
public class ResponseData {

	private boolean success = true;
	private List<ReportExecutionResponse> reports = new ArrayList<ReportExecutionResponse>();

	@XmlElement(name = "success", required = true)
	public boolean isSuccess() {
		return this.success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@XmlElement(name = "reports")
	public List<ReportExecutionResponse> getReports() {
		return this.reports;
	}

	public void setReports(List<ReportExecutionResponse> reports) {
		this.reports = reports;
	}
}
