package seava.j4e.presenter.exeptions;

import java.util.Locale;
import java.util.ResourceBundle;

import seava.j4e.api.exceptions.IErrorCode;
import seava.j4e.api.session.Session;

public enum J4EPresenterErrorCodes implements IErrorCode {
	DUPLICATE_ENTITY(500, "Duplicate entity: %s", "Error"),
	FOREIGN_KEY_CONSTRAIT(501, "Could not delete this entity because it is linked to another entity!", "Error"),
	ENTITY_NOT_EXISTS(502, "Could not update because some of the selected entities has not exists anymore!", "Error");

	private final ResourceBundle boundle;

	private final int errNo;
	private final String errMsg;
	private final String errGroup;

	private J4EPresenterErrorCodes(int errNo, String errMsg, String errGroup) {
		this.errNo = errNo;
		this.errMsg = errMsg;
		this.errGroup = errGroup;
		this.boundle = ResourceBundle.getBundle("locale.j4e.presenter.error.messages", new Locale(Session.user.get().getSettings().getLanguage()));
	}

	@Override
	public String getErrGroup() {
		return this.boundle.containsKey(this.errGroup) ? this.boundle.getString(this.errGroup) : this.errGroup;
	}

	@Override
	public int getErrNo() {
		return this.errNo;
	}

	@Override
	public String getErrMsg() {
		return this.boundle.containsKey(Integer.toString(this.errNo)) ? this.boundle.getString(Integer.toString(this.errNo)) : this.errMsg;
	}

}
