package seava.j4e.presenter.action.impex.report.jaxb.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.OperationNotSupportedException;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;

import seava.j4e.presenter.action.impex.report.jaxb.wrappers.MapList;
import seava.j4e.presenter.action.impex.report.jaxb.wrappers.RecordWrapper;

public class RecordAdapter extends XmlAdapter<RecordWrapper, List<Map<String, String>>> {

	@Override
	public RecordWrapper marshal(List<Map<String, String>> data) throws Exception {

		RecordWrapper wrapper = new RecordWrapper();
		wrapper.record = new ArrayList<MapList>();
		for (Map<String, String> row : data) {
			MapList mapList = new MapList();
			for (Map.Entry<String, String> col : row.entrySet()) {
				mapList.properties.add(new JAXBElement<String>(new QName(col.getKey()), String.class, col.getValue()));
			}
			wrapper.record.add(mapList);
		}
		return wrapper;
	}

	@Override
	public List<Map<String, String>> unmarshal(RecordWrapper wrapper) throws Exception {
		throw new OperationNotSupportedException();

	}
}
