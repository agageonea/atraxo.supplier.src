/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.descriptor;

public class DsDescriptor<M> extends AbstractViewModelDescriptor<M> {

	public DsDescriptor(Class<M> modelClass) {
		super(modelClass);
	}
}
