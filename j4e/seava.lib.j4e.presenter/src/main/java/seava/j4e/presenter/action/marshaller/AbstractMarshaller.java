/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.action.marshaller;

public class AbstractMarshaller<M, F, P> {

	protected Class<M> modelClass;
	protected Class<F> filterClass;
	protected Class<P> paramClass;

	protected Class<M> getModelClass() {
		return this.modelClass;
	}

	public Class<F> getFilterClass() {
		return this.filterClass;
	}

	protected Class<P> getParamClass() {
		return this.paramClass;
	}
}
