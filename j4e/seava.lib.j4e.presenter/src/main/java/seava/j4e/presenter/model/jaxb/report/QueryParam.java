package seava.j4e.presenter.model.jaxb.report;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class QueryParam {

	private String name;
	private String value;
	private String type;

	public String getName() {
		return this.name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.value;
	}

	@XmlElement
	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return this.type;
	}

	@XmlElement
	public void setType(String type) {
		this.type = type;
	}

}
