package seava.j4e.presenter.action.impex.report.jaxb.adapters;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;

import seava.j4e.presenter.action.impex.report.jaxb.wrappers.ListWrapper;
import seava.j4e.presenter.model.jaxb.report.QueryParam;

public class QueryParamListAdapter extends XmlAdapter<ListWrapper<JAXBElement<QueryParam>>, List<QueryParam>> {

	@Override
	public ListWrapper<JAXBElement<QueryParam>> marshal(List<QueryParam> arg0) throws Exception {
		ListWrapper<JAXBElement<QueryParam>> wrapper = new ListWrapper<>();
		wrapper.properties = new ArrayList<>();
		for (QueryParam obj : arg0) {
			wrapper.properties.add(new JAXBElement<QueryParam>(new QName("queryParam"), QueryParam.class, obj));

		}
		return wrapper;
	}

	@Override
	public List<QueryParam> unmarshal(ListWrapper<JAXBElement<QueryParam>> arg0) throws Exception {
		throw new OperationNotSupportedException();
	}

}