package seava.j4e.presenter.action.impex.report.jaxb;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.w3c.dom.Document;

import seava.j4e.api.action.impex.IDsReport;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.presenter.model.jaxb.report.Report;

public abstract class DsAbstractReport<M, F> implements IDsReport<M, F> {

	private static final String CDATA_ELEMENT = "url";
	protected Marshaller jaxbMarshaller;
	protected Report report;

	@Override
	public String toXmlString() throws BusinessException {

		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			Document document = docBuilderFactory.newDocumentBuilder().newDocument();

			this.jaxbMarshaller.marshal(this.report, document);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer nullTransformer = transformerFactory.newTransformer();
			nullTransformer.setOutputProperty(OutputKeys.INDENT, "yes");
			nullTransformer.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS, CDATA_ELEMENT);
			nullTransformer.transform(new DOMSource(document), new StreamResult(os));

			return os.toString(StandardCharsets.UTF_8.name());
		} catch (JAXBException e) {
			throw new BusinessException(ErrorCode.XML_MARSHALL_ERROR, e);
		} catch (UnsupportedEncodingException e) {
			throw new BusinessException(ErrorCode.XML_MARSHALL_ERROR, e);
		} catch (ParserConfigurationException e) {
			throw new BusinessException(ErrorCode.XML_MARSHALL_ERROR, e);
		} catch (TransformerConfigurationException e) {
			throw new BusinessException(ErrorCode.XML_MARSHALL_ERROR, e);
		} catch (TransformerException e) {
			throw new BusinessException(ErrorCode.XML_MARSHALL_ERROR, e);
		}
	}
}
