package seava.j4e.presenter.action.impex.report.jaxb.adapters;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;

import seava.j4e.presenter.action.impex.report.jaxb.wrappers.ListWrapper;
import seava.j4e.presenter.model.jaxb.report.Parameter;

public class ParameterListAdapter extends XmlAdapter<ListWrapper<JAXBElement<Parameter>>, List<Parameter>> {

	@Override
	public ListWrapper<JAXBElement<Parameter>> marshal(List<Parameter> arg0) throws Exception {
		ListWrapper<JAXBElement<Parameter>> wrapper = new ListWrapper<>();
		wrapper.properties = new ArrayList<>();
		for (Parameter obj : arg0) {
			wrapper.properties.add(new JAXBElement<Parameter>(new QName("param"), Parameter.class, obj));
		}
		return wrapper;
	}

	@Override
	public List<Parameter> unmarshal(ListWrapper<JAXBElement<Parameter>> arg0) throws Exception {
		throw new OperationNotSupportedException();
	}

}
