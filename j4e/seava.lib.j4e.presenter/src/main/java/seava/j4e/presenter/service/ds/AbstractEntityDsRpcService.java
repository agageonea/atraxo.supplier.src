/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.service.ds;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.model.IModelWithId;
import seava.j4e.presenter.descriptor.RpcDefinition;
import seava.j4e.presenter.exeptions.J4EPresenterErrorCodes;
import seava.j4e.presenter.model.AbstractDsModel;
import seava.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * Implements the rpc(remote-procedure call) actions for an entity-ds. See the super-classes for more details.
 *
 * @author amathe
 * @param <M>
 * @param <F>
 * @param <P>
 * @param <E>
 */
public abstract class AbstractEntityDsRpcService<M extends AbstractDsModel<E>, F, P, E> extends AbstractEntityDsWriteService<M, F, P, E> {

	private Map<String, RpcDefinition> rpcData = new HashMap<String, RpcDefinition>();
	private Map<String, RpcDefinition> rpcFilter = new HashMap<String, RpcDefinition>();

	// ======================== RPC ===========================

	/**
	 * Execute an arbitrary service method with the data object.
	 */
	@Override
	public void rpcData(String procedureName, M ds, P params) throws Exception {

		if (!this.rpcData.containsKey(procedureName)) {
			throw new Exception("No such procedure defined: `" + procedureName + "`");
		}

		RpcDefinition def = this.rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(), def.getMethodName(), this.getModelClass());

		EntityManager em = this.getEntityService().getEntityManager();
		if (def.isEntityShouldExists() && ds instanceof IModelWithId && ((IModelWithId<?>) ds).getId() != null) {
			E e = em.find(this.getEntityClass(), ((IModelWithId<?>) ds).getId());
			if (e == null) {
				throw new BusinessException(J4EPresenterErrorCodes.ENTITY_NOT_EXISTS, J4EPresenterErrorCodes.ENTITY_NOT_EXISTS.getErrMsg());
			}
		}

		if (rpcMethod.isWithParams()) {
			rpcMethod.getMethod().invoke(delegate, ds, params);
		} else {
			rpcMethod.getMethod().invoke(delegate, ds);
		}

		if (def.getReloadFromEntity()) {
			if (ds instanceof IModelWithId) {
				if (((IModelWithId<?>) ds).getId() != null) {
					E e = em.find(this.getEntityClass(), ((IModelWithId<?>) ds).getId());
					this.getConverter().entityToModel(e, ds, em, null);
				}
			}
		}
		// delegate.execute(ds);
	}

	/**
	 * Execute an arbitrary service method with the data object returning a stream as result.
	 *
	 * @param procedureName
	 * @param ds
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@Override
	public InputStream rpcDataStream(String procedureName, M ds, P params) throws Exception {

		if (!this.rpcData.containsKey(procedureName)) {
			throw new Exception("No such procedure defined: " + procedureName);
		}

		RpcDefinition def = this.rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(), def.getMethodName(), this.getModelClass());

		InputStream result = null;
		if (rpcMethod.isWithParams()) {
			result = (InputStream) rpcMethod.getMethod().invoke(delegate, ds, params);
		} else {
			result = (InputStream) rpcMethod.getMethod().invoke(delegate, ds);
		}
		return result;
	}

	/**
	 * Execute an arbitrary service method with the filter object.
	 *
	 * @param procedureName
	 * @param filter
	 * @param params
	 * @throws Exception
	 */
	@Override
	public void rpcFilter(String procedureName, F filter, P params) throws Exception {

		if (!this.rpcFilter.containsKey(procedureName)) {
			throw new Exception("No such procedure defined: " + procedureName);
		}

		RpcDefinition def = this.rpcFilter.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(), def.getMethodName(), this.getFilterClass());

		if (rpcMethod.isWithParams()) {
			rpcMethod.getMethod().invoke(delegate, filter, params);
		} else {
			rpcMethod.getMethod().invoke(delegate, filter);
		}
	}

	/**
	 * Execute an arbitrary service method with the filter object returning a stream as result.
	 *
	 * @param procedureName
	 * @param filter
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@Override
	public InputStream rpcFilterStream(String procedureName, F filter, P params) throws Exception {

		if (!this.rpcFilter.containsKey(procedureName)) {
			throw new Exception("No such procedure defined: " + procedureName);
		}

		RpcDefinition def = this.rpcFilter.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(), def.getMethodName(), this.getFilterClass());

		InputStream result = null;
		if (rpcMethod.isWithParams()) {
			result = (InputStream) rpcMethod.getMethod().invoke(delegate, filter, params);
		} else {
			result = (InputStream) rpcMethod.getMethod().invoke(delegate, filter);
		}

		return result;
	}

	/**
	 * Execute an arbitrary service method with a list of data objects. Contributed by Jan Fockaert
	 *
	 * @param procedureName
	 * @param list
	 * @param params
	 * @throws Exception
	 */
	@Override
	public void rpcData(String procedureName, List<M> list, P params) throws Exception {

		if (!this.rpcData.containsKey(procedureName)) {
			throw new Exception("No such procedure defined: " + procedureName);
		}

		RpcDefinition def = this.rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(), def.getMethodName(), List.class);
		if (rpcMethod.isWithParams()) {
			rpcMethod.getMethod().invoke(delegate, list, params);
		} else {
			rpcMethod.getMethod().invoke(delegate, list);
		}

		if (def.getReloadFromEntity()) {
			for (M ds : list) {
				if (ds instanceof IModelWithId) {
					Object _id = ((IModelWithId<?>) ds).getId();
					if (_id != null && !"".equals(_id)) {
						EntityManager em = this.getEntityService().getEntityManager();
						E e = em.find(this.getEntityClass(), _id);
						this.getConverter().entityToModel(e, ds, em, null);
					}
				}
			}
		}
	}

	/**
	 * Execute an arbitrary service method with a list of data objects returning a stream as result. Contributed by Jan Fockaert
	 *
	 * @param procedureName
	 * @param list
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@Override
	public InputStream rpcDataStream(String procedureName, List<M> list, P params) throws Exception {

		if (!this.rpcData.containsKey(procedureName)) {
			throw new Exception("No such procedure defined: " + procedureName);
		}

		RpcDefinition def = this.rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(), def.getMethodName(), List.class);

		InputStream result = null;
		if (rpcMethod.isWithParams()) {
			result = (InputStream) rpcMethod.getMethod().invoke(delegate, list, params);
		} else {
			result = (InputStream) rpcMethod.getMethod().invoke(delegate, list);
		}

		return result;
	}

	/**
	 * Execute an arbitrary service method with a list of data IDs.
	 *
	 * @param procedureName
	 * @param list
	 * @param params
	 * @throws Exception
	 */
	@Override
	public void rpcIds(String procedureName, List<Object> list, P params) throws Exception {

		if (!this.rpcData.containsKey(procedureName)) {
			throw new Exception("No such procedure defined: " + procedureName);
		}

		RpcDefinition def = this.rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(), def.getMethodName(), List.class);
		if (rpcMethod.isWithParams()) {
			rpcMethod.getMethod().invoke(delegate, list, params);
		} else {
			rpcMethod.getMethod().invoke(delegate, list);
		}
	}

	/**
	 * Execute an arbitrary service method with a list of data IDs returning a stream as result.
	 *
	 * @param procedureName
	 * @param list
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@Override
	public InputStream rpcIdsStream(String procedureName, List<Object> list, P params) throws Exception {

		if (!this.rpcData.containsKey(procedureName)) {
			throw new Exception("No such procedure defined: " + procedureName);
		}

		RpcDefinition def = this.rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(), def.getMethodName(), List.class);

		InputStream result = null;
		if (rpcMethod.isWithParams()) {
			result = (InputStream) rpcMethod.getMethod().invoke(delegate, list, params);
		} else {
			result = (InputStream) rpcMethod.getMethod().invoke(delegate, list);
		}

		return result;
	}

	/**
	 * Helper method to find the rpc method to be invoked.
	 *
	 * @param claz
	 * @param methodName
	 * @param dataClass
	 * @return
	 * @throws Exception
	 */
	protected RpcMethod getRpcMethod(Class<? extends AbstractPresenterBaseService> claz, String methodName, Class<?> dataClass) throws Exception {
		Method m = null;
		boolean withParams = false;
		try {
			m = claz.getMethod(methodName, dataClass, this.getParamClass());
			withParams = true;
		} catch (NoSuchMethodException e) {
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace("No such method " + methodName + " for class " + dataClass + " , will get the method without class params!", e);
			}
			try {
				m = claz.getMethod(methodName, dataClass);
			} catch (NoSuchMethodException ex) {
				throw new Exception("Delegate class " + claz.getCanonicalName() + " does not implement a `" + methodName + "("
						+ dataClass.getSimpleName() + ")` method.", ex);
			}
		}

		return new RpcMethod(m, withParams);
	}

	public Map<String, RpcDefinition> getRpcData() {
		return this.rpcData;
	}

	public void setRpcData(Map<String, RpcDefinition> rpcData) {
		this.rpcData = rpcData;
	}

	public Map<String, RpcDefinition> getRpcFilter() {
		return this.rpcFilter;
	}

	public void setRpcFilter(Map<String, RpcDefinition> rpcFilter) {
		this.rpcFilter = rpcFilter;
	}

	private class RpcMethod {
		private Method method;
		private boolean withParams;

		public RpcMethod(Method method, boolean withParams) {
			super();
			this.method = method;
			this.withParams = withParams;
		}

		public Method getMethod() {
			return this.method;
		}

		public boolean isWithParams() {
			return this.withParams;
		}

	}
}
