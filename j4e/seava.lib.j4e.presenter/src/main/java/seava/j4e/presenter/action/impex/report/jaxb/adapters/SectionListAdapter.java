package seava.j4e.presenter.action.impex.report.jaxb.adapters;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.namespace.QName;

import seava.j4e.presenter.action.impex.report.jaxb.wrappers.ListWrapper;
import seava.j4e.presenter.model.jaxb.report.Section;

public class SectionListAdapter extends XmlAdapter<ListWrapper<JAXBElement<Section>>, List<Section>> {

	@Override
	public ListWrapper<JAXBElement<Section>> marshal(List<Section> arg0) throws Exception {
		ListWrapper<JAXBElement<Section>> wrapper = new ListWrapper<>();
		wrapper.properties = new ArrayList<>();
		for (Section obj : arg0) {
			wrapper.properties.add(new JAXBElement<Section>(new QName("section"), Section.class, obj));

		}
		return wrapper;
	}

	@Override
	public List<Section> unmarshal(ListWrapper<JAXBElement<Section>> arg0) throws Exception {
		throw new OperationNotSupportedException();
	}

}
