/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.libextensions;

import au.com.bytecode.opencsv.bean.HeaderColumnNameMappingStrategy;

public class HeaderColumnNameMappingStrategy_Dnet<T> extends HeaderColumnNameMappingStrategy<T> {

	public String[] getHeader() {
		return this.header;
	}
}
