package seava.j4e.presenter.model.jaxb.report;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import seava.j4e.presenter.action.impex.report.jaxb.adapters.SectionListAdapter;

@XmlRootElement(name = "dynamic-report")
public class DynamicReport implements Report {

	private String format;
	private List<Section> sections;
	private String code;

	@XmlElement
	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	@XmlElement
	public List<Section> getSections() {
		return this.sections;
	}

	@XmlElement
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@XmlJavaTypeAdapter(SectionListAdapter.class)
	public void setSections(List<Section> sections) {
		this.sections = sections;
	}

}
