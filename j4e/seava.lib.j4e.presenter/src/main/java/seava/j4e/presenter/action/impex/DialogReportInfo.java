package seava.j4e.presenter.action.impex;

import java.util.ArrayList;
import java.util.List;

import seava.j4e.api.action.impex.IDialogReportInfo;

public class DialogReportInfo implements IDialogReportInfo {

	private String title;
	private List<DialogReportData> dataList;

	public DialogReportInfo() {
		this.dataList = new ArrayList<DialogReportData>();
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<DialogReportData> getDataList() {
		return this.dataList;
	}

	public void setDataList(List<DialogReportData> dataList) {
		this.dataList = dataList;
	}

}
