/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.propertyeditors;

import java.beans.PropertyEditorSupport;

public class LongEditor extends PropertyEditorSupport {

	public LongEditor() {
		super();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		String input = (text != null ? text.trim() : null);
		if (input == null || input.equals("")) {
			// Treat empty String as null value.
			this.setValue(null);

		} else {
			try {
				this.setValue(Long.parseLong(text));
			} catch (Exception e) {
				throw new IllegalArgumentException("Invalid long value [" + text + "]", e);
			}
		}

	}

}
