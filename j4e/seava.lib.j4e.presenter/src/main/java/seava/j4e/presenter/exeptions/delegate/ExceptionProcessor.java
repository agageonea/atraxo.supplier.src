package seava.j4e.presenter.exeptions.delegate;

import org.apache.commons.lang.exception.ExceptionUtils;

import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.presenter.exeptions.J4EPresenterErrorCodes;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class ExceptionProcessor {

	private static final String APHOSTROFE = "\\'";
	private static final int MY_SQL_DUPLICATE_ENTRY = 1062;
	private static final int MY_SQL_FOREIG_KEY_CONSTRAIT = 1451;

	public static void process(Exception exception) throws Exception {
		Throwable root = ExceptionUtils.getRootCause(exception);
		if (root instanceof MySQLIntegrityConstraintViolationException) {
			MySQLIntegrityConstraintViolationException mysqlException = (MySQLIntegrityConstraintViolationException) root;
			switch (mysqlException.getErrorCode()) {
			case MY_SQL_DUPLICATE_ENTRY: {
				String message = root.getMessage();
				String[] messageArray = message.split(APHOSTROFE);
				throw new BusinessException(J4EPresenterErrorCodes.DUPLICATE_ENTITY, String.format(
						J4EPresenterErrorCodes.DUPLICATE_ENTITY.getErrMsg(), messageArray.length > 0 ? messageArray[1] : ""));
			}
			case MY_SQL_FOREIG_KEY_CONSTRAIT: {
				throw new BusinessException(J4EPresenterErrorCodes.FOREIGN_KEY_CONSTRAIT, J4EPresenterErrorCodes.FOREIGN_KEY_CONSTRAIT.getErrMsg());
			}
			default: {
				throw new Exception(exception.getMessage(), exception.getCause());
			}
			}
		} else {
			throw new Exception(exception.getMessage(), exception.getCause());
		}
	}

}
