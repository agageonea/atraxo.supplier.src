/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.propertyeditors;

import java.beans.PropertyEditorSupport;

/**
 * BooleanEditor : transform different strings to boolean true/false
 */
public class BooleanEditor extends PropertyEditorSupport {

	public static final String VALUE_TRUE = "true";
	public static final String VALUE_FALSE = "false";

	public static final String VALUE_ON = "on";
	public static final String VALUE_OFF = "off";

	public static final String VALUE_YES = "yes";
	public static final String VALUE_NO = "no";

	public static final String VALUE_1 = "1";
	public static final String VALUE_0 = "0";

	private final String trueString;

	private final String falseString;

	private final boolean allowEmpty;

	/**
	 * Default constructor
	 */
	public BooleanEditor() {
		this(null, null, true);
	}

	/**
	 * @param allowEmpty
	 */
	public BooleanEditor(boolean allowEmpty) {
		this(null, null, allowEmpty);
	}

	/**
	 * @param trueString
	 * @param falseString
	 * @param allowEmpty
	 */
	public BooleanEditor(String trueString, String falseString, boolean allowEmpty) {
		this.trueString = trueString;
		this.falseString = falseString;
		this.allowEmpty = allowEmpty;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		String input = text != null ? text.trim() : "";
		if (this.allowEmpty && ("".equals(input))) {
			// Treat empty String as null value.
			this.setValue(null);
		} else if (this.trueString != null && input.equalsIgnoreCase(this.trueString)) {
			this.setValue(Boolean.TRUE);
		} else if (this.falseString != null && input.equalsIgnoreCase(this.falseString)) {
			this.setValue(Boolean.FALSE);
		} else if (this.trueString == null
				&& (input.equalsIgnoreCase(VALUE_TRUE) || input.equalsIgnoreCase(VALUE_ON) || input.equalsIgnoreCase(VALUE_YES) || input
						.equals(VALUE_1))) {
			this.setValue(Boolean.TRUE);
		} else if (this.falseString == null
				&& (input.equalsIgnoreCase(VALUE_FALSE) || input.equalsIgnoreCase(VALUE_OFF) || input.equalsIgnoreCase(VALUE_NO) || input
						.equals(VALUE_0))) {
			this.setValue(Boolean.FALSE);
		} else {
			throw new IllegalArgumentException("Invalid boolean value [" + text + "]");
		}
	}

	@Override
	public String getAsText() {
		if (Boolean.TRUE.equals(this.getValue())) {
			return this.trueString != null ? this.trueString : VALUE_TRUE;
		} else if (Boolean.FALSE.equals(this.getValue())) {
			return this.falseString != null ? this.falseString : VALUE_FALSE;
		} else {
			return "";
		}
	}

}