package seava.j4e.presenter.service.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.request.async.DeferredResult;

import seava.j4e.api.model.AppNotification;
import seava.j4e.api.service.notification.IAppNotificationService;

public class NotificationPresenterService {

	@Autowired
	@Qualifier("notificationBusinessService")
	private IAppNotificationService srv;

	public void getUpdate(String clientId, String userId, DeferredResult<AppNotification> result) {
		this.srv.getUpdate(clientId, userId, result);
	}
}
