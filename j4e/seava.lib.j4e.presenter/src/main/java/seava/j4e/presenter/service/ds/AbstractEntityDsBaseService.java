/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.service.ds;

import java.util.Map;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.module.SimpleModule;

import seava.j4e.api.Constants;
import seava.j4e.api.action.result.IDsConverter;
import seava.j4e.api.action.result.IDsMarshaller;
import seava.j4e.api.service.business.IEntityService;
import seava.j4e.api.service.presenter.IDsBaseService;
import seava.j4e.presenter.action.marshaller.JsonMarshaller;
import seava.j4e.presenter.action.marshaller.XmlMarshaller;
import seava.j4e.presenter.converter.AbstractDsConverter;
import seava.j4e.presenter.converter.DefaultDsConverter;
import seava.j4e.presenter.descriptor.DsDescriptor;
import seava.j4e.presenter.descriptor.ViewModelDescriptorManager;
import seava.j4e.presenter.json.module.JsonModuleConfigurator;
import seava.j4e.presenter.model.AbstractDsModel;
import seava.j4e.presenter.service.AbstractPresenterReadService;

/**
 * Base abstract class for entity based data-source service hierarchy. An entity-data-source(referred to as entity-ds) is a specialized data-source
 * which provides view-model perspective(M) from a given persistence perspective(E). <br> Subclasses implement standard functionality for standard
 * read actions (query, export ), write actions (insert, update, delete, import) and remote procedure call like method invocation (rpc). Adds to its
 * super-class an entity-type information.
 *
 * @author amathe
 * @param <M>
 * @param <F>
 * @param <P>
 * @param <E>
 */
public abstract class AbstractEntityDsBaseService<M extends AbstractDsModel<E>, F, P, E> extends AbstractPresenterReadService<M, F, P>
		implements IDsBaseService<M, F, P> {

	/**
	 * Source entity type it works with.
	 */
	private Class<E> entityClass;

	/**
	 * Converter class to be used for entity-to-ds and ds-to-entity conversions.
	 */
	private Class<? extends AbstractDsConverter<M, E>> converterClass;

	/**
	 * DS descriptor.
	 */
	private DsDescriptor<M> descriptor;

	/**
	 * DS <-> Entity converter
	 */
	private AbstractDsConverter<M, E> converter;

	public DsDescriptor<M> getDescriptor() throws Exception {
		if (this.descriptor == null) {
			boolean useCache = this.getSettings().get(Constants.PROP_WORKING_MODE).equals(Constants.PROP_WORKING_MODE_PROD);
			this.descriptor = ViewModelDescriptorManager.getDsDescriptor(this.getModelClass(), useCache);
		}
		return this.descriptor;
	}

	public void setDescriptor(DsDescriptor<M> descriptor) {
		this.descriptor = descriptor;
	}

	@Override
	public IDsMarshaller<M, F, P> createMarshaller(String dataFormat) throws Exception {
		IDsMarshaller<M, F, P> marshaller = null;
		if (dataFormat.equals(IDsMarshaller.JSON)) {
			marshaller = new JsonMarshaller<>(this.getModelClass(), this.getFilterClass(), this.getParamClass());
			this.registerModules(marshaller);
		} else if (dataFormat.equals(IDsMarshaller.XML)) {
			marshaller = new XmlMarshaller<>(this.getModelClass(), this.getFilterClass(), this.getParamClass());
		}
		return marshaller;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void registerModules(IDsMarshaller<M, F, P> marshaller) throws ClassNotFoundException {
		Map<String, JsonModuleConfigurator> registry = this.getApplicationContext().getBeansOfType(JsonModuleConfigurator.class);
		ObjectMapper objectMapper = (ObjectMapper) marshaller.getDelegate();
		for (String key : registry.keySet()) {
			JsonModuleConfigurator register = registry.get(key);
			SimpleModule module = new SimpleModule(key, Version.unknownVersion());
			module.addSerializer(register.getClassForType(), register.getJsonSerializer());
			module.addDeserializer(register.getClassForType(), register.getJsonDeserializer());
			objectMapper.registerModule(module);
		}
	}

	@Override
	public Class<E> getEntityClass() {
		return this.entityClass;
	}

	public void setEntityClass(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	public Class<? extends AbstractDsConverter<M, E>> getConverterClass() {
		return this.converterClass;
	}

	public void setConverterClass(Class<? extends AbstractDsConverter<M, E>> converterClass) {
		this.converterClass = converterClass;
	}

	@SuppressWarnings("unchecked")
	protected IDsConverter<M, E> getConverter() throws Exception {

		if (this.converter != null) {
			return this.converter;
		}

		if (this.converterClass != null) {
			this.converter = this.converterClass.newInstance();
		} else {
			this.converter = DefaultDsConverter.class.newInstance();
		}

		this.converter.setApplicationContext(this.getApplicationContext());
		this.converter.setDescriptor(this.getDescriptor());
		this.converter.setEntityClass(this.getEntityClass());
		this.converter.setModelClass(this.getModelClass());
		this.converter.setServiceLocator(this.getServiceLocator());

		return this.converter;
	}

	public IEntityService<E> getEntityService() throws Exception {
		return this.findEntityService(this.getEntityClass());
	}

}
