/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.presenter.service.asgn;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.module.SimpleModule;
import org.eclipse.persistence.internal.databaseaccess.DatabaseCall;
import org.eclipse.persistence.internal.expressions.ParameterExpression;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.jpa.JpaQuery;
import org.eclipse.persistence.queries.DatabaseQuery;
import org.eclipse.persistence.sessions.DatabaseRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.StringUtils;

import seava.j4e.api.Constants;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.result.IDsMarshaller;
import seava.j4e.api.descriptor.IAsgnContext;
import seava.j4e.api.service.business.IAsgnTxService;
import seava.j4e.api.service.business.IAsgnTxServiceFactory;
import seava.j4e.api.session.Session;
import seava.j4e.presenter.action.marshaller.JsonMarshaller;
import seava.j4e.presenter.action.query.AbstractQueryBuilder;
import seava.j4e.presenter.action.query.QueryBuilderWithJpql;
import seava.j4e.presenter.descriptor.AsgnDescriptor;
import seava.j4e.presenter.descriptor.ViewModelDescriptorManager;
import seava.j4e.presenter.json.module.JsonModuleConfigurator;
import seava.j4e.presenter.model.AbstractAsgnModel;
import seava.j4e.presenter.service.AbstractPresenterReadService;

/**
 * Base abstract class for assignment service. An assignment component is used to manage the many-to-many associations between entities.
 *
 * @author amathe
 * @param <M>
 * @param <F>
 * @param <P>
 * @param <E>
 */
public abstract class AbstractAsgnService<M extends AbstractAsgnModel<E>, F, P, E> extends AbstractPresenterReadService<M, F, P> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractAsgnService.class);

	/**
	 * Source entity type it works with.
	 */
	private Class<E> entityClass;

	private Class<? extends AbstractQueryBuilder<M, F, P>> leftQueryBuilderClass;

	private Class<? extends AbstractQueryBuilder<M, F, P>> rightQueryBuilderClass;

	private AsgnDescriptor<M> descriptor;

	private List<IAsgnTxServiceFactory> asgnTxServiceFactories;

	private IAsgnContext ctx;

	private String asgnTxFactoryName;

	private IAsgnTxService<E> txService;

	/**
	 * Advanced filter rules.
	 */
	protected List<IFilterRule> filterRules;

	/**
	 * @return
	 * @throws Exception
	 */
	private IAsgnTxService<E> findAsgnTxService() throws Exception {
		IAsgnTxService<E> s = this.getServiceLocator().findAsgnTxService(Constants.SPRING_DEFAULT_ASGN_TX_SERVICE, this.asgnTxFactoryName);
		s.setEntityClass(this.entityClass);
		return s;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public IAsgnTxService<E> getTxService() throws Exception {
		if (this.txService == null) {
			this.txService = this.findAsgnTxService();
		}
		return this.txService;
	}

	/**
	 * Add the specified list of IDs to the selected ones.
	 *
	 * @param selectionId
	 * @param ids
	 * @throws Exception
	 */
	public void moveRight(String selectionId, List<String> ids) throws Exception {
		this.getTxService().doMoveRight(this.ctx, selectionId, ids);
	}

	/**
	 * Add all the available values to the selected ones.
	 *
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param filterField
	 * @param filterValue
	 * @throws Exception
	 */
	public void moveRightAll(String selectionId, F filter, P params, String filterField, String filterValue) throws Exception {

		IQueryBuilder<M, F, P> builder = this.createLeftQueryBuilder();
		builder.setFilterRules(this.filterRules);

		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;

		String baseEql = bld.getBaseEql();
		boolean forExport = bld.isForExport();

		bld.setForExport(true);
		bld.setBaseEql("select e.id from " + this.getEntityClass().getSimpleName() + " e ");
		bld.addFilterCondition(" e.clientId = :clientId ");
		bld.addFilterCondition(" e." + this.ctx.getLeftPkField() + " not in (select x.itemId from TempAsgnLine x where x.selectionId = :selectionId)");

		boolean hasFilter = StringUtils.hasText(filterField) && StringUtils.hasText(filterValue);
		if (hasFilter) {
			bld.addFilterCondition(" e." + filterField + " like :" + filterField + " ");
		}

		bld.setFilter(filter);
		bld.setParams(params);

		TypedQuery<Object> q = bld.createQuery(Object.class);
		org.eclipse.persistence.sessions.Session session = bld.getEntityManager().unwrap(JpaEntityManager.class).getActiveSession();
		q.setParameter("clientId", Session.user.get().getClient().getId());
		q.setParameter("selectionId", selectionId);
		if (hasFilter) {
			q.setParameter(filterField, filterValue);
		}

		DatabaseQuery dbQuery = q.unwrap(JpaQuery.class).getDatabaseQuery();
		DatabaseRecord p = new DatabaseRecord();
		dbQuery.prepareCall(session, p);
		dbQuery.bindAllParameters();
		DatabaseCall call = dbQuery.getCall();

		List<?> sqlParams = call.getParameters();
		List<Object> passParams = new ArrayList<Object>();
		for (Object obj : sqlParams) {
			if (obj instanceof ParameterExpression) {
				ParameterExpression pe = (ParameterExpression) obj;
				Object pv = q.getParameterValue(pe.getField().getName());
				passParams.add(pv);
			} else {
				passParams.add(obj);
			}
		}

		bld.setBaseEql(baseEql);
		bld.setForExport(forExport);

		String where = dbQuery.getSQLString();

		this.getTxService().doMoveRightAll(this.getCtx(), selectionId, where, passParams);
	}

	/**
	 * Remove the specified list of IDs from the selected ones.
	 *
	 * @param selectionId
	 * @param ids
	 * @throws Exception
	 */
	public void moveLeft(String selectionId, List<String> ids) throws Exception {
		this.getTxService().doMoveLeft(this.ctx, selectionId, ids);
	}

	/**
	 * Remove all the selected values.
	 *
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param filterField
	 * @param filterValue
	 * @throws Exception
	 */
	public void moveLeftAll(String selectionId, F filter, P params, String filterField, String filterValue) throws Exception {
		// String where = "";
		// if (StringUtils.hasText(filterField) && StringUtils.hasText(filterValue)) {
		// where = " t." + filterField + " like '" + filterValue + "' ";
		// }
		// this.getTxService().doMoveLeftAll(this.getCtx(), selectionId, where);

		IQueryBuilder<M, F, P> builder = this.createRightQueryBuilder();
		builder.setFilterRules(this.filterRules);

		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;

		String baseEql = bld.getBaseEql();
		boolean forExport = bld.isForExport();

		bld.setForExport(true);
		bld.setBaseEql("select e.id from " + this.getEntityClass().getSimpleName() + " e ");
		bld.addFilterCondition(" e.clientId = :clientId ");
		bld.addFilterCondition(" e." + this.ctx.getLeftPkField() + " in (select x.itemId from TempAsgnLine x where x.selectionId = :selectionId)");

		boolean hasFilter = StringUtils.hasText(filterField) && StringUtils.hasText(filterValue);
		if (hasFilter) {
			bld.addFilterCondition(" e." + filterField + " like :" + filterField + " ");
		}

		bld.setFilter(filter);
		bld.setParams(params);

		TypedQuery<Object> q = bld.createQuery(Object.class);
		org.eclipse.persistence.sessions.Session session = bld.getEntityManager().unwrap(JpaEntityManager.class).getActiveSession();
		q.setParameter("clientId", Session.user.get().getClient().getId());
		q.setParameter("selectionId", selectionId);
		if (hasFilter) {
			q.setParameter(filterField, filterValue);
		}

		DatabaseQuery dbQuery = q.unwrap(JpaQuery.class).getDatabaseQuery();
		DatabaseRecord p = new DatabaseRecord();
		dbQuery.prepareCall(session, p);
		dbQuery.bindAllParameters();
		DatabaseCall call = dbQuery.getCall();

		List<?> sqlParams = call.getParameters();
		List<Object> passParams = new ArrayList<>();
		for (Object obj : sqlParams) {
			if (obj instanceof ParameterExpression) {
				ParameterExpression pe = (ParameterExpression) obj;
				Object pv = q.getParameterValue(pe.getField().getName());
				passParams.add(pv);
			} else {
				passParams.add(obj);
			}
		}

		bld.setBaseEql(baseEql);
		bld.setForExport(forExport);

		String where = dbQuery.getSQLString();

		this.getTxService().doMoveLeftAll(this.getCtx(), selectionId, where, passParams);
	}

	/**
	 * @param selectionId
	 * @param objectId
	 * @throws Exception
	 */
	public void save(String selectionId, String objectId) throws Exception {
		this.getTxService().doSave(this.ctx, selectionId, objectId);
	}

	/**
	 * Initialize the component's temporary workspace.
	 *
	 * @param asgnName
	 * @param objectId
	 * @return the UUID of the selection
	 * @throws Exception
	 */
	public String setup(String asgnName, String objectId) throws Exception {
		return this.getTxService().doSetup(this.ctx, asgnName, objectId);
	}

	/**
	 * Clean-up the temporary selections.
	 *
	 * @param selectionId
	 * @throws Exception
	 */
	public void cleanup(String selectionId) throws Exception {
		this.getTxService().doCleanup(this.ctx, selectionId);
	}

	/**
	 * Restores all the changes made by the user to the initial state.
	 *
	 * @param selectionId
	 * @param objectId
	 * @throws Exception
	 */
	public void reset(String selectionId, String objectId) throws Exception {
		this.getTxService().doReset(this.ctx, selectionId, objectId);
	}

	/**
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public List<M> findLeft(String selectionId, F filter, P params, IQueryBuilder<M, F, P> builder) throws Exception {
		if (builder == null) {
			throw new Exception("Cannot run a query with null query builder.");
		}
		this.preFindLeft(builder);
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;

		bld.addFilterCondition(" e.clientId = :clientId and e." + this.ctx.getLeftPkField()
				+ " not in (select x.itemId from TempAsgnLine x where x.selectionId = :selectionId)");

		bld.setFilter(filter);
		bld.setParams(params);

		List<M> result = new ArrayList<>();
		TypedQuery<E> q = bld.createQuery(this.getEntityClass());
		q.setParameter("clientId", Session.user.get().getClient().getId());
		q.setParameter("selectionId", selectionId);
		List<E> list = q.setFirstResult(bld.getResultStart()).setMaxResults(bld.getResultSize()).getResultList();
		for (E e : list) {
			M m = this.getModelClass().newInstance();
			this.entityToModel(e, m);
			result.add(m);
		}
		this.postFindLeft(builder, result);
		return result;
	}

	protected void preFindLeft(IQueryBuilder<M, F, P> builder) throws Exception {
	}

	protected void postFindLeft(IQueryBuilder<M, F, P> builder, List<M> result) throws Exception {
	}

	/**
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public List<M> findRight(String selectionId, F filter, P params, IQueryBuilder<M, F, P> builder) throws Exception {
		if (builder == null) {
			throw new Exception("Cannot run a query with null query builder.");
		}
		this.preFindRight(builder);
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;

		bld.addFilterCondition(" e.clientId = :clientId and e." + this.ctx.getLeftPkField()
				+ " in (select x.itemId from TempAsgnLine x where x.selectionId = :selectionId)");
		bld.setFilter(filter);
		bld.setParams(params);

		List<M> result = new ArrayList<>();

		TypedQuery<E> q = bld.createQuery(this.getEntityClass());
		q.setParameter("clientId", Session.user.get().getClient().getId());
		q.setParameter("selectionId", selectionId);
		List<E> list = q.setFirstResult(bld.getResultStart()).setMaxResults(bld.getResultSize()).getResultList();
		for (E e : list) {
			M m = this.getModelClass().newInstance();
			this.entityToModel(e, m);
			result.add(m);
		}
		this.postFindRight(builder, result);
		return result;
	}

	protected void preFindRight(IQueryBuilder<M, F, P> builder) throws Exception {
	}

	protected void postFindRight(IQueryBuilder<M, F, P> builder, List<M> result) throws Exception {
	}

	/**
	 * @param e
	 * @param m
	 * @throws Exception
	 */
	public void entityToModel(E e, M m) throws Exception {
		ExpressionParser parser = new SpelExpressionParser();
		StandardEvaluationContext context = new StandardEvaluationContext(e);
		Map<String, String> refpaths = this.getDescriptor().getE2mConv();
		Method[] methods = this.getModelClass().getMethods();
		for (Method method : methods) {
			if (!"set__clientRecordId__".equals(method.getName()) && method.getName().startsWith("set")) {
				String fn = StringUtils.uncapitalize(method.getName().substring(3));
				try {
					method.invoke(m, parser.parseExpression(refpaths.get(fn)).getValue(context));
				} catch (Exception exc) {
					LOGGER.error("Could not invoke the method on object !", exc);
				}
			}
		}
	}

	/**
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public Long countLeft(String selectionId, F filter, P params, IQueryBuilder<M, F, P> builder) throws Exception {
		return this.count_(selectionId, filter, params, builder);
	}

	/**
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public Long countRight(String selectionId, F filter, P params, IQueryBuilder<M, F, P> builder) throws Exception {
		return this.count_(selectionId, filter, params, builder);
	}

	/**
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	protected Long count_(String selectionId, F filter, P params, IQueryBuilder<M, F, P> builder) throws Exception {
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;
		Query q = bld.createQueryCount();
		q.setParameter("clientId", Session.user.get().getClient().getId());
		q.setParameter("selectionId", selectionId);
		Object count = q.getSingleResult();
		if (count instanceof Integer) {
			return ((Integer) count).longValue();
		} else {
			return (Long) count;
		}
	}

	/**
	 * @param dataFormat
	 * @return
	 * @throws Exception
	 */
	public IDsMarshaller<M, F, P> createMarshaller(String dataFormat) throws Exception {
		IDsMarshaller<M, F, P> marshaller = null;
		if (dataFormat.equals(IDsMarshaller.JSON)) {
			marshaller = new JsonMarshaller<M, F, P>(this.getModelClass(), this.getFilterClass(), this.getParamClass());
			this.registerModules(marshaller);
		}
		return marshaller;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private <T> void registerModules(IDsMarshaller<M, F, P> marshaller) throws ClassNotFoundException {
		Map<String, JsonModuleConfigurator> registry = this.getApplicationContext().getBeansOfType(JsonModuleConfigurator.class);
		ObjectMapper objectMapper = (ObjectMapper) marshaller.getDelegate();
		for (String key : registry.keySet()) {
			JsonModuleConfigurator register = registry.get(key);
			SimpleModule module = new SimpleModule(key, Version.unknownVersion());
			module.addSerializer(register.getClassForType(), register.getJsonSerializer());
			module.addDeserializer(register.getClassForType(), register.getJsonDeserializer());
			objectMapper.registerModule(module);
		}
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public IQueryBuilder<M, F, P> createLeftQueryBuilder() throws Exception {
		IQueryBuilder<M, F, P> qb;
		if (this.getLeftQueryBuilderClass() == null) {
			if (this.getQueryBuilderClass() == null) {
				qb = new QueryBuilderWithJpql<M, F, P>();
			} else {
				qb = this.getQueryBuilderClass().newInstance();
			}
		} else {
			qb = this.getLeftQueryBuilderClass().newInstance();
		}
		this.prepareQueryBuilder(qb);
		return qb;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public IQueryBuilder<M, F, P> createRightQueryBuilder() throws Exception {
		IQueryBuilder<M, F, P> qb;
		if (this.getRightQueryBuilderClass() == null) {
			if (this.getQueryBuilderClass() == null) {
				qb = new QueryBuilderWithJpql<M, F, P>();
			} else {
				qb = this.getQueryBuilderClass().newInstance();
			}
		} else {
			qb = this.getRightQueryBuilderClass().newInstance();
		}
		this.prepareQueryBuilder(qb);
		return qb;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	protected void prepareQueryBuilder(IQueryBuilder<M, F, P> qb) throws Exception {
		qb.setModelClass(this.getModelClass());
		qb.setFilterClass(this.getFilterClass());
		qb.setParamClass(this.getParamClass());
		qb.setDescriptor(this.getDescriptor());
		qb.setSettings(this.getSettings());
		if (qb instanceof QueryBuilderWithJpql) {
			QueryBuilderWithJpql<M, F, P> jqb = (QueryBuilderWithJpql<M, F, P>) qb;
			jqb.setEntityManager(this.getTxService().getEntityManager());
			jqb.setBaseEql("select e from " + this.getEntityClass().getSimpleName() + " e");
			jqb.setBaseEqlCount("select count(1) from " + this.getEntityClass().getSimpleName() + " e");

			if (this.getDescriptor().isWorksWithJpql()) {
				jqb.setDefaultWhere(this.getDescriptor().getJpqlDefaultWhere());
				jqb.setDefaultSort(this.getDescriptor().getJpqlDefaultSort());
			}
		}

	}

	// ==================== getters- setters =====================

	public Class<E> getEntityClass() {
		return this.entityClass;
	}

	public void setEntityClass(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	public Class<? extends AbstractQueryBuilder<M, F, P>> getLeftQueryBuilderClass() {
		return this.leftQueryBuilderClass;
	}

	public void setLeftQueryBuilderClass(Class<? extends AbstractQueryBuilder<M, F, P>> leftQueryBuilderClass) {
		this.leftQueryBuilderClass = leftQueryBuilderClass;
	}

	public Class<? extends AbstractQueryBuilder<M, F, P>> getRightQueryBuilderClass() {
		return this.rightQueryBuilderClass;
	}

	public void setRightQueryBuilderClass(Class<? extends AbstractQueryBuilder<M, F, P>> rightQueryBuilderClass) {
		this.rightQueryBuilderClass = rightQueryBuilderClass;
	}

	public List<IAsgnTxServiceFactory> getAsgnTxServiceFactories() {
		return this.asgnTxServiceFactories;
	}

	public void setAsgnTxServiceFactories(List<IAsgnTxServiceFactory> asgnTxServiceFactories) {
		this.asgnTxServiceFactories = asgnTxServiceFactories;
	}

	public IAsgnContext getCtx() {
		return this.ctx;
	}

	public void setCtx(IAsgnContext ctx) {
		this.ctx = ctx;
	}

	public String getAsgnTxFactoryName() {
		return this.asgnTxFactoryName;
	}

	public void setAsgnTxFactoryName(String asgnTxFactoryName) {
		this.asgnTxFactoryName = asgnTxFactoryName;
	}

	public AsgnDescriptor<M> getDescriptor() throws Exception {
		if (this.descriptor == null) {
			boolean useCache = this.getSettings().get(Constants.PROP_WORKING_MODE).equals(Constants.PROP_WORKING_MODE_PROD);
			this.descriptor = ViewModelDescriptorManager.getAsgnDescriptor(this.getModelClass(), useCache);
		}
		return this.descriptor;
	}

	public void setDescriptor(AsgnDescriptor<M> descriptor) {
		this.descriptor = descriptor;
	}

	public List<IFilterRule> getFilterRules() {
		return this.filterRules;
	}

	public void setFilterRules(List<IFilterRule> filterRules) {
		this.filterRules = filterRules;
	}

	/**
	 * @param filterRules
	 */
	public void addFilterRules(List<? extends IFilterRule> filterRules) {
		this.filterRules = new ArrayList<IFilterRule>();
		for (IFilterRule r : filterRules) {
			this.filterRules.add(r);
		}
	}

}
