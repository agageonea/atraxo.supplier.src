package seava.j4e.web.settings.init;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 * ApplicationInitilizer
 */
public class ApplicationInitilizer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	private static final Logger log = LoggerFactory.getLogger(ApplicationInitilizer.class);

	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		try {
			Properties props = PropertiesLoaderUtils.loadProperties(new ClassPathResource("seava.properties"));
			PropertiesPropertySource ps = new PropertiesPropertySource("properties", props);
			XmlWebApplicationContext appContext = (XmlWebApplicationContext) applicationContext;
			String[] locArr = appContext.getConfigLocations();
			List<String> locations = new ArrayList<>();
			locations.addAll(Arrays.asList(locArr));
			if (ps.getProperty("sys.security.authentication") != null) {
				String securityAuth = (String) ps.getProperty("sys.security.authentication");
				if (securityAuth.isEmpty() || "DB".equalsIgnoreCase(securityAuth)) {
					locations.add("classpath:META-INF/spring/jee/j4e-security.xml");
				} else {
					locations.add("classpath:META-INF/spring/jee/j4e-" + securityAuth + "-security.xml");
				}
			} else {
				locations.add("classpath:META-INF/spring/jee/j4e-security.xml");

			}
			appContext.setConfigLocations(locations.toArray(locArr));
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}
}