package seava.j4e.web.controller.fpcache;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import seava.j4e.api.ISettings;
import seava.j4e.web.controller.AbstractBaseController;

/**
 * @author tlukacs
 */
// https://www.sitepoint.com/common-pitfalls-avoid-using-html5-application-cache/

@Controller
public class FpCacheController extends AbstractBaseController {

	private static final Logger log = LoggerFactory.getLogger(FpCacheController.class);

	private static final String VERSION_PARAM = "%VERSION";

	// @formatter:off
	private static final String[] FPCACHE_FRAME = {
		"CACHE MANIFEST",
		"",
		"# extjs lib",
		"statics/js/ext-all.js?version=%VERSION",
		"statics/js/sencha-charts.js?version=%VERSION",
		"",
		"# jquery",
		"statics/js/jquery-1.11.3.min.js?version=%VERSION",
		"statics/js/jquery.sparkline.min.js?version=%VERSION",
		"",
		"# framework",
		"statics/js/e4e-all.js?version=%VERSION",
		"",
		"# css",
		"statics/font-awesome-4.7.0/css/font-awesome.min.css?version=%VERSION",
		"statics/font-open-sans/css/open-sans.css?version=%VERSION",
		"statics/font-finance-solid/styles.css?version=%VERSION",
		"",
		"# files from extension providers?version=%VERSION",
		"statics/atraxo/fmbas/ui/extjs/ext/types/Types.js?version=%VERSION",
		"statics/atraxo/acc/ui/extjs/acc_type.js?version=%VERSION",
		"statics/atraxo/ad/ui/extjs/ad.js?version=%VERSION",
		"statics/atraxo/cmm/ui/extjs/cmm_type.js?version=%VERSION",
		"statics/atraxo/fmbas/ui/extjs/fmbas_type.js?version=%VERSION",
		"statics/atraxo/ops/ui/extjs/ops_type.js?version=%VERSION",
		"",
		"# cached dialogs",
		"ui-extjs/frame/atraxo.mod.ops/atraxo.ops.ui.extjs.frame.FuelTicket_Ui.js?version=%VERSION",
		"ui-extjs/frame/atraxo.mod.acc/atraxo.acc.ui.extjs.frame.FuelEvents_Ui.js?version=%VERSION",
		"ui-extjs/frame/atraxo.mod.acc/atraxo.acc.ui.extjs.frame.Invoices_Ui.js?version=%VERSION",
		"ui-extjs/frame/atraxo.mod.acc/atraxo.acc.ui.extjs.frame.OutgoingInvoices_Ui.js?version=%VERSION",
		"ui-extjs/frame/atraxo.mod.cmm/atraxo.cmm.ui.extjs.frame.ContractCustomer_Ui.js?version=%VERSION",
		"",
		"# All other resources need to be downloaded from server",
		"# without this configuration the browser is unable to download the required files",
		"NETWORK:",
		"*"
	};
	
	// @formatter:off
	private static final String[] DEV_CACHE_FRAME = {
		"CACHE MANIFEST",
		"",
		"statics/button-icons/ext-theme-clarity/style.css?version=%VERSION",
		"statics/ext-theme-clarity/ext-theme-clarity-all.css?version=%VERSION",
		"statics/custom-css/all.css?version=%VERSION",
		"statics/custom-css/ext-theme-clarity.css?version=%VERSION",
		"statics/custom-css/charts.css?version=%VERSION",
		"",
		"statics/js/ext-all-debug.js?version=%VERSION",
		"statics/js/jquery-1.11.3.min.js?version=%VERSION",
		"statics/js/jquery.sparkline.min.js?version=%VERSION",
		"statics/en/extjs.js?version=%VERSION",
		"statics/en/e4e.js?version=%VERSION",
		"statics/js/sencha-charts-debug.js?version=%VERSION",
		"statics/js/Main.js?version=%VERSION",
		"statics/js/extjs-extend.js?version=%VERSION",
		"statics/js/extjs-ux-extend.js?version=%VERSION",
		"statics/js/Notification.js?version=%VERSION",
		"statics/js/e4e/base/NavigationTree.js?version=%VERSION",
		"statics/js/e4e/base/TemplateRepository.js?version=%VERSION",
		"statics/js/e4e/base/Session.js?version=%VERSION",
		"statics/js/e4e/base/Application.js?version=%VERSION",
		"statics/js/e4e/base/SoneApplicationMenu.js?version=%VERSION",
		"statics/js/e4e/base/LoginWindow.js?version=%VERSION",
		"statics/js/e4e/base/ChangePasswordWindow.js?version=%VERSION",
		"statics/js/e4e/base/SelectCompanyWindow.js?version=%VERSION",
		"statics/js/e4e/base/FrameInspector.js?version=%VERSION",
		"statics/js/e4e/base/UserPreferences.js?version=%VERSION",
		"statics/js/e4e/base/Abstract_View.js?version=%VERSION",
		"statics/js/e4e/base/DisplayField.js?version=%VERSION",
		"statics/js/e4e/base/KeyboardShortcutsWindow.js?version=%VERSION",
		"statics/js/e4e/base/KeyBindings.js?version=%VERSION",
		"statics/js/e4e/base/HomePanel.js?version=%VERSION",
		"statics/js/e4e/base/FrameNavigatorWithIframe.js?version=%VERSION",
		"statics/js/e4e/base/FileUploadWindow.js?version=%VERSION",
		"statics/js/e4e/base/WorkflowFormWithHtmlWindow.js?version=%VERSION",
		"statics/js/e4e/base/WfAbstractFormWindowExtjs.js?version=%VERSION",
		"statics/js/e4e/base/WfStartFormWindowExtjs.js?version=%VERSION",
		"statics/js/e4e/base/WfTaskFormWindowExtjs.js?version=%VERSION",
		"statics/js/e4e/base/WorkflowFormFactory.js?version=%VERSION",
		"statics/js/e4e/asgn/AbstractAsgn.js?version=%VERSION",
		"statics/js/e4e/asgn/AbstractAsgnGrid.js?version=%VERSION",
		"statics/js/e4e/asgn/AbstractAsgnUi.js?version=%VERSION",
		"statics/js/e4e/asgn/AbstractAsgnPanel.js?version=%VERSION",
		"statics/js/e4e/asgn/AsgnGridBuilder.js?version=%VERSION",
		"statics/js/e4e/asgn/AsgnUiBuilder.js?version=%VERSION",
		"statics/js/e4e/asgn/AsgnWindow.js?version=%VERSION",
		"statics/js/e4e/dc/DcState.js?version=%VERSION",
		"statics/js/e4e/dc/AbstractDc.js?version=%VERSION",
		"statics/js/e4e/dc/DcActionsFactory.js?version=%VERSION",
		"statics/js/e4e/dc/DcCommandFactory.js?version=%VERSION",
		"statics/js/e4e/dc/DcContext.js?version=%VERSION",
		"statics/js/e4e/dc/FlowContext.js?version=%VERSION",
		"statics/js/e4e/dc/DcActionsStateManager.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDc_View.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDc_Grid.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDc_Form.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDc_PropGrid.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDcvGrid.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDcvEditableGrid.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDcvEditForm.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDcvEditPropGrid.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDcvTree.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDcvFilterForm.js?version=%VERSION",
		"statics/js/e4e/dc/view/AbstractDcvFilterPropGrid.js?version=%VERSION",
		"statics/js/e4e/dc/view/DcvPopovers.js?version=%VERSION",
		"statics/js/e4e/dc/view/DcvFilterFormBuilder.js?version=%VERSION",
		"statics/js/e4e/dc/view/DcvEditFormBuilder.js?version=%VERSION",
		"statics/js/e4e/dc/view/DcvFilterPropGridBuilder.js?version=%VERSION",
		"statics/js/e4e/dc/view/DcvEditPropGridBuilder.js?version=%VERSION",
		"statics/js/e4e/dc/view/DcvGridBuilder.js?version=%VERSION",
		"statics/js/e4e/dc/view/DcvEditableGridBuilder.js?version=%VERSION",
		"statics/js/e4e/dc/command/AbstractDcCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/AbstractDcAsyncCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/AbstractDcSyncCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcQueryCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcClearQueryCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcEnterQueryCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcNewCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcNewInChildCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcCopyCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcSaveCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcSaveInChildCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcCancelCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcCancelOnFieldsCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcCancelSelectedCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcEditInCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcEditOutCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcDeleteCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcReloadRecCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcReloadPageCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcPrevRecCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcNextRecCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcRpcDataCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcRpcDataListCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcRpcIdListCommand.js?version=%VERSION",
		"statics/js/e4e/dc/command/DcRpcFilterCommand.js?version=%VERSION",
		"statics/js/e4e/dc/tools/DcReport.js?version=%VERSION",
		"statics/js/e4e/dc/tools/DcBulkEditWindow.js?version=%VERSION",
		"statics/js/e4e/dc/tools/DcImportWindow.js?version=%VERSION",
		"statics/js/e4e/dc/tools/DcExportWindow.js?version=%VERSION",
		"statics/js/e4e/dc/tools/DcCustomExportWindow.js?version=%VERSION",
		"statics/js/e4e/dc/tools/DcPrintWindow.js?version=%VERSION",
		"statics/js/e4e/dc/tools/DcFilterWindow.js?version=%VERSION",
		"statics/js/e4e/dc/tools/DcGridLayoutWindow.js?version=%VERSION",
		"statics/js/e4e/dc/tools/DcChartWindow.js?version=%VERSION",
		"statics/js/e4e/ui/FrameBuilder.js?version=%VERSION",
		"statics/js/e4e/ui/ActionBuilder.js?version=%VERSION",
		"statics/js/e4e/ui/FrameButtonStateManager.js?version=%VERSION",
		"statics/js/e4e/ui/AbstractUi.js?version=%VERSION",
		"statics/js/e4e/ui/WizardWindow.js?version=%VERSION",
		"statics/js/e4e/lov/AbstractCombo.js?version=%VERSION",
		"statics/js/e4e/lov/AbstractStaticCombo.js?version=%VERSION",
		"statics/js/e4e/lov/LovAsgn.js?version=%VERSION",
		"statics/js/viewFilterToolbar/tlb-toolbarCfg.js?version=%VERSION",
		"statics/js/viewFilterToolbar/tlb-toolbarModels.js?version=%VERSION",
		"statics/js/viewFilterToolbar/tlb-filterWindow.js?version=%VERSION",
		"statics/js/viewFilterToolbar/tlb-viewCombo.js?version=%VERSION",
		"statics/js/viewFilterToolbar/tlb-resetButton.js?version=%VERSION",
		"statics/js/viewFilterToolbar/tlb-filterCombo.js?version=%VERSION",
		"statics/js/viewFilterToolbar/tlb-viewWindow.js?version=%VERSION",
		"statics/js/viewFilterToolbar/tlb-toolbar.js?version=%VERSION",
		"",
		"statics/font-awesome-4.7.0/css/font-awesome.min.css?version=%VERSION",
		"statics/font-open-sans/css/open-sans.css?version=%VERSION",
		"statics/font-finance-solid/styles.css?version=%VERSION",
		"",
		"# All other resources need to be downloaded from server",
		"# without this configuration the browser is unable to download the required files",
		"NETWORK:",
		"*"
	};

	private static final String[] FPCACHE_MAIN = {
		"CACHE MANIFEST",
		"",
		"# extjs lib",
		"statics/js/ext-all.js?version=%VERSION",
		"statics/js/sencha-charts.js?version=%VERSION",
		"",
		"# jquery",
		"statics/js/jquery-1.11.3.min.js?version=%VERSION",
		"statics/js/jquery.sparkline.min.js?version=%VERSION",
		"",
		"# framework",
		"statics/js/e4e-all.js?version=%VERSION",
		"",
		"# css",
		"statics/font-awesome-4.7.0/css/font-awesome.min.css?version=%VERSION",
		"statics/font-open-sans/css/open-sans.css?version=%VERSION",
		"statics/font-finance-solid/styles.css?version=%VERSION",
		"",
		"# All other resources need to be downloaded from server",
		"# without this configuration the browser is unable to download the required files",
		"NETWORK:",
		"*"
	};
	// @formatter:on

	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/fpcache_frame.appcache", method = RequestMethod.GET)
	@ResponseBody
	public void fpcache_frame(HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			String version = this.getVersion();
			ServletOutputStream out = response.getOutputStream();
			for (String s : FPCACHE_FRAME) {
				out.println(s.replaceFirst(VERSION_PARAM, version));
			}
			out.flush();
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug(e.getMessage(), e);
			}
			throw e;
		} finally {
			this.finishRequest();
		}
	}
	
	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/devcache_frame.appcache", method = RequestMethod.GET)
	@ResponseBody
	public void devcache_frame(HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			String version = this.getVersion();
			ServletOutputStream out = response.getOutputStream();
			for (String s : DEV_CACHE_FRAME) {
				out.println(s.replaceFirst(VERSION_PARAM, version));
			}
			out.flush();
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug(e.getMessage(), e);
			}
			throw e;
		} finally {
			this.finishRequest();
		}
	}

	@RequestMapping(value = "/fpcache_main.appcache", method = RequestMethod.GET)
	@ResponseBody
	public void fpcache_main(HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			String version = this.getVersion();
			ServletOutputStream out = response.getOutputStream();
			for (String s : FPCACHE_MAIN) {
				out.println(s.replaceFirst(VERSION_PARAM, version));
			}
			out.flush();
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug(e.getMessage(), e);
			}
			throw e;
		} finally {
			this.finishRequest();
		}
	}

	private String getVersion() {
		String version = "_";
		ISettings cfg = null;
		try {
			cfg = this.getSettings();
			version = cfg.getProductVersion();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return version;
	}

}
