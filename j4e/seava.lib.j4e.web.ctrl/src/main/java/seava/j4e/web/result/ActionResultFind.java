/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.result;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import seava.j4e.api.action.result.IActionResultFind;

@XmlRootElement(name = "result")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActionResultFind extends AbstractResultData implements IActionResultFind {

	/**
	 * Total number of results which match the filter.
	 */
	private Long totalCount;

	/**
	 * Actual result data list.
	 */
	@XmlElementWrapper(name = "dataList")
	@XmlElement(name = "data")
	private List<?> data;

	private Map<String, Object> summaries;

	@Override
	public Map<String, Object> getSummaries() {
		return this.summaries;
	}

	@Override
	public void setSummaries(Map<String, Object> summaries) {
		this.summaries = summaries;
	}

	/**
	 * Parameters.
	 */
	private Object params;

	/**
	 * @return the totalCount
	 */
	@Override
	public Long getTotalCount() {
		return this.totalCount;
	}

	/**
	 * @param totalCount
	 *            the totalCount to set
	 */
	@Override
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * @return the data
	 */
	@Override
	public List<?> getData() {
		return this.data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	@Override
	public void setData(List<?> data) {
		this.data = data;
	}

	@Override
	public Object getParams() {
		return this.params;
	}

	@Override
	public void setParams(Object params) {
		this.params = params;
	}

}
