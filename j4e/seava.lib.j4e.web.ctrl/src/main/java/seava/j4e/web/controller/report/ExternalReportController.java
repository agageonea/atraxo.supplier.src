package seava.j4e.web.controller.report;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import seava.j4e.api.Constants;
import seava.j4e.api.service.presenter.IExtReportService;
import seava.j4e.web.controller.AbstractBaseController;

/**
 * @author tlukacs
 */
@Controller
public class ExternalReportController extends AbstractBaseController {

	static final Logger logger = LoggerFactory.getLogger(ExternalReportController.class);

	/**
	 * @param reportId
	 * @param dataFormat
	 * @param dataString
	 * @param paramString
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/{reportId}")
	@ResponseBody
	public String runReport(@PathVariable String reportId,
			@RequestParam(value = Constants.REQUEST_PARAM_DATA_FORMAT, required = false) String dataFormat,
			@RequestParam(value = Constants.REQUEST_PARAM_DATA, required = false, defaultValue = "{}") String dataString,
			@RequestParam(value = Constants.REQUEST_PARAM_PARAMS, required = false, defaultValue = "{}") String paramString,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			if (logger.isInfoEnabled()) {
				logger.info("Processing request: run external report '" + reportId + "' : " + dataFormat);
			}

			this.prepareRequest(request, response);
			response.setCharacterEncoding("UTF-8");

			IExtReportService srv = this.findReportService();

			if (dataFormat != null && !"".equals(dataFormat)) {

				Map<String, Object> paramMap = new HashMap<>();
				// put all parameters and values from data into a single map
				// the values from data are put last, to override any other values (if exists)
				Map<String, String[]> reqMap = request.getParameterMap();
				for (Map.Entry<String, String[]> e : reqMap.entrySet()) {
					String k = e.getKey();
					if (!"data".equalsIgnoreCase(k) && !"params".equalsIgnoreCase(k)) {
						paramMap.put(k, e.getValue()[0]);
					}
				}
				if (reqMap.containsKey("params")) {
					String[] sp = reqMap.get("params");
					HashMap<String, Object> paramsMap = new ObjectMapper().readValue(sp[0], HashMap.class);
					for (Map.Entry<String, Object> pe : paramsMap.entrySet()) {
						paramMap.put(pe.getKey(), pe.getValue());
					}
				}
				if (reqMap.containsKey("data")) {
					String[] sd = reqMap.get("data");
					HashMap<String, Object> dataMap = new ObjectMapper().readValue(sd[0], HashMap.class);
					for (Map.Entry<String, Object> de : dataMap.entrySet()) {
						paramMap.put(de.getKey(), de.getValue());
					}
				}

				srv.runExternalReport(reportId, dataFormat, paramMap, null);
				return "";

			} else {
				// if no format specified, instead of reportId we should get the DS name and need to check if
				// report exists defined for that DS
				boolean result = srv.isExternalReportExists(reportId);
				return String.valueOf(result);
			}
		} catch (Exception e) {
			return this.handleManagedException(null, e, response);
		} finally {
			this.finishRequest();
		}
	}

	/**
	 * Lookup a report service.
	 *
	 * @return
	 * @throws Exception
	 */
	private IExtReportService findReportService() throws Exception {
		IExtReportService srv = this.getApplicationContext().getBean("ExternalReport", IExtReportService.class);
		if (srv == null) {
			throw new Exception("Entity service `ExternalReport` not found. Contact system administrator!");
		}
		return srv;
	}

}
