/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.result;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import seava.j4e.api.action.result.IActionResultRpc;

@XmlRootElement(name = "result")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActionResultRpc extends AbstractResultData implements IActionResultRpc {

	/**
	 * Data value-object.
	 */
	private Object data;

	/**
	 * Parameters.
	 */
	private Object params;

	@Override
	public Object getData() {
		return this.data;
	}

	@Override
	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public Object getParams() {
		return this.params;
	}

	@Override
	public void setParams(Object params) {
		this.params = params;
	}

}
