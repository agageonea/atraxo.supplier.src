package seava.j4e.web.controller.client;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import seava.j4e.api.Constants;
import seava.j4e.api.ISettings;
import seava.j4e.api.action.impex.ICreateClient;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.web.controller.AbstractBaseController;
import seava.j4e.web.result.ActionNewClient;

/**
 * @author apetho
 */
@Controller
public class ClientController extends AbstractBaseController {

	private static final String STARTED = "STARTED";
	private static final String CREATE_CLIENT_SERVICE = "createClientService";
	private static final String FAILED = "Failed";
	private static final String DATE_FORMAT = "yyyy.MM.dd hh:mm:ss";
	private static final Logger logger = LoggerFactory.getLogger(ClientController.class);

	@RequestMapping(value = "/client", params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.NEW_CLIENT, method = RequestMethod.POST)
	@ResponseBody
	public String newClient(@RequestHeader(value = Constants.REQUEST_PARAM_CLIENT_NAME, required = true) String clientName,
			@RequestHeader(value = Constants.REQUEST_PARAM_CLIENT_CODE, required = true) String clientCode,
			@RequestHeader(value = Constants.REQUEST_PARAM_ADMIN_USER_CODE, required = true) String adminUserCode,
			@RequestHeader(value = Constants.REQUEST_PARAM_ADMIN_USER_NAME, required = true) String adminUserName,
			@RequestHeader(value = Constants.REQUEST_PARAM_ADMIN_LOGIN_NAME, required = true) String adminLoginName,
			@RequestHeader(value = Constants.REQUEST_PARAM_ADMIN_LOGIN_PASSWORD, required = true) String adminLoginPasword,
			@RequestHeader(value = Constants.REQUEST_PARAM_CALLBACK_URL, required = false) String callbackUrl, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ", new Object[] { Constants.NEW_CLIENT });
			}

			ActionNewClient anc = new ActionNewClient();
			anc.setAdminUserCode(adminUserCode);
			anc.setAdminUserName(adminUserName);
			anc.setClientCode(clientCode);
			anc.setClientName(clientName);
			anc.setStartTime(new Date());

			if (logger.isDebugEnabled()) {
				logger.debug("  --> clientCode: {} ", new Object[] { clientCode });
				logger.debug("  --> clientName: {} ", new Object[] { clientName });
				logger.debug("  --> adminUserCode: {} ", new Object[] { adminUserCode });
				logger.debug("  --> adminUserName: {} ", new Object[] { adminUserName });
				logger.debug("  --> adminLoginName: {} ", new Object[] { adminLoginName });
			}

			ISettings settings = this.getApplicationContext().getBean(ISettings.class);

			ObjectMapper om = new ObjectMapper();
			om.setDateFormat(new SimpleDateFormat(DATE_FORMAT));

			if (!settings.isNewClientApiEnabled()) {
				anc.setStatus(FAILED);
				anc.setDescription("The interface is disabled!");
				stopWatch.stop();
				return om.writeValueAsString(anc);
			}

			String paramsVerifyStr = this.verifyParams(clientName, clientCode, adminUserCode, adminUserName, adminLoginName, adminLoginPasword);
			if (!StringUtils.isEmpty(paramsVerifyStr)) {
				anc.setStatus(FAILED);
				anc.setDescription(paramsVerifyStr);
				stopWatch.stop();
				return om.writeValueAsString(anc);
			}

			String passwsord = new String(Base64.decodeBase64(adminLoginPasword), StandardCharsets.UTF_8);
			ICreateClient clientService = this.getApplicationContext().getBean(CREATE_CLIENT_SERVICE, ICreateClient.class);
			clientService.newClient(clientName, clientCode, adminUserCode, adminUserName, adminLoginName, passwsord, callbackUrl);
			anc.setStatus(STARTED);
			stopWatch.stop();
			return om.writeValueAsString(anc);
		} catch (Exception e) {
			return this.handleManagedException(ErrorCode.DB_QUERY_ERROR, e, response);
		} finally {
			this.finishRequest();
		}

	}

	private String verifyParams(String clientName, String clientCode, String adminUserCode, String adminUserName, String adminLoginName,
			String adminLoginPasword) {
		StringBuilder sb = new StringBuilder();
		if (StringUtils.isEmpty(clientName)) {
			sb.append("clientName");
		}
		if (StringUtils.isEmpty(clientCode)) {
			sb.append(StringUtils.isEmpty(sb.toString()) ? "clientCode" : " | " + "clientCode");
		}
		if (StringUtils.isEmpty(adminUserCode)) {
			sb.append(StringUtils.isEmpty(sb.toString()) ? "adminUserCode" : " | " + "adminUserCode");
		}
		if (StringUtils.isEmpty(adminUserName)) {
			sb.append(StringUtils.isEmpty(sb.toString()) ? "adminUserName" : " | " + "adminUserName");
		}
		if (StringUtils.isEmpty(adminLoginName)) {
			sb.append(StringUtils.isEmpty(sb.toString()) ? "adminLoginName" : " | " + "adminLoginName");
		}
		if (StringUtils.isEmpty(adminLoginPasword)) {
			sb.append(StringUtils.isEmpty(sb.toString()) ? "adminLoginPasword" : " | " + "adminLoginPasword");
		}
		if (!StringUtils.isEmpty(sb.toString())) {
			sb.append(" is mandatory");
		}
		return sb.toString();
	}

}
