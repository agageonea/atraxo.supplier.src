/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.controller.ui.extjs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.j4e.api.Constants;

/**
 * DependencyLoader
 */
public class DependencyLoader {

	static final Logger logger = LoggerFactory.getLogger(DependencyLoader.class);

	private ObjectMapper jsonMapper = new ObjectMapper();
	private HttpClient httpClient;

	private String urlUiExtjsModules;
	private String urlUiExtjsModuleSubpath;
	private String host;
	private String workingMode;

	/**
	 * Include component bundle in url?
	 */
	private boolean moduleUseBundle;

	/**
	 * @param host
	 * @param workingMode
	 */
	public DependencyLoader(String host, String workingMode) {
		super();
		this.host = host;
		this.workingMode = workingMode;
	}

	/**
	 * @param bundle
	 * @param name
	 * @param file
	 * @throws Exception
	 */
	public void packFrameCmp(String bundle, String name, File file) throws Exception {

		List<String> list = new ArrayList<>();
		this.resolveFrameDependencies(bundle, name, null, list, null);
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8")); // ISO-8859-1
		try {
			for (String dep : list) {
				this.writeContentCmp(dep, writer);
			}
		} finally {
			writer.close();
		}
	}

	/**
	 * Pack translation files for the frame components.
	 *
	 * @param bundle
	 * @param name
	 * @param language
	 * @param file
	 * @throws Exception
	 */
	public void packFrameTrl(String bundle, String name, String language, File file) throws Exception {
		List<String> list = new ArrayList<>();
		this.resolveFrameDependencies(bundle, name, language, null, list);
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "ISO-8859-1"));
		try {
			for (String dep : list) {
				this.writeContentTrl(dep, language, writer);
			}
		} finally {
			writer.close();
		}
	}

	/**
	 * Resolve the dependencies for the given frame.
	 *
	 * @param cmp
	 * @return
	 * @throws Exception
	 */
	public Dependencies resolveFrameDependencies(String cmp) throws Exception {
		Dependencies d = new Dependencies();
		this.resolveAllDependencies(cmp, d);
		return d;
	}

	/**
	 * Resolve all the dependencies used in this frame adding them to the specified lists.One list for the components the other one for the
	 * translations for the given language..
	 *
	 * @param bundle
	 * @param frameFqn
	 * @param language
	 * @param listCmp
	 * @param listTrl
	 * @throws Exception
	 */
	public void resolveFrameDependencies(String bundle, String frameFqn, String language, List<String> listCmp, List<String> listTrl)
			throws Exception {

		String cmp = bundle + "/frame/" + frameFqn;

		if (logger.isDebugEnabled()) {
			logger.debug("Resolving frame dependencies for: {}", cmp);
		}

		Dependencies d = this.resolveFrameDependencies(cmp);

		if (d.getDs() != null) {
			for (String dep : d.getDs()) {
				String[] tokens = dep.split("/");
				String _bundle = tokens[0];
				String _type = tokens[1];
				String _name = tokens[2];

				if (listCmp != null) {
					listCmp.add(this.urlCmp(_bundle, _type, _name));
				}

				if (listTrl != null) {
					listTrl.add(this.urlTrl(_bundle, _type, _name, language));
				}

			}
		}

		if (d.getLov() != null) {
			for (String dep : d.getLov()) {
				if (listCmp != null) {
					listCmp.add(this.urlCmp(dep));
				}
			}
		}

		if (d.getAsgn() != null) {
			for (String dep : d.getAsgn()) {
				if (listCmp != null) {
					listCmp.add(this.urlCmp(dep));
				}
			}
		}

		if (d.getDc() != null) {
			for (String dep : d.getDc()) {

				String[] tokens = dep.split("/");
				String _bundle = tokens[0];
				String _type = tokens[1];
				String _name = tokens[2];

				if (listCmp != null) {
					listCmp.add(this.urlCmp(_bundle, _type, _name));
				}
				if (listTrl != null) {
					listTrl.add(this.urlTrl(_bundle, _type, _name, language));
				}
			}
		}

		if (listCmp != null) {
			listCmp.add(this.urlCmp(bundle, Dependencies.TYPE_UI, frameFqn));
		}
		if (listTrl != null) {
			listTrl.add(this.urlTrl(bundle, Dependencies.TYPE_UI, frameFqn, language));
		}

		if (d.getDcCust() != null) {
			for (String dep : d.getDcCust()) {

				String[] tokens = dep.split("/");
				String _bundle = tokens[0];
				String _type = tokens[1];
				String _name = tokens[2];

				if (listCmp != null) {
					listCmp.add(this.urlCmp(_bundle, _type, _name));
				}
				if (listTrl != null) {
					listTrl.add(this.urlTrl(_bundle, _type, _name, language));
				}
			}
		}

		if (d.getUiCust() != null) {
			for (String dep : d.getUiCust()) {

				String[] tokens = dep.split("/");
				String _bundle = tokens[0];
				String _type = tokens[1];
				String _name = tokens[2];

				if (listCmp != null) {
					listCmp.add(this.urlCmp(_bundle, _type, _name));
				}
				if (listTrl != null) {
					listTrl.add(this.urlTrl(_bundle, _type, _name, language));
				}
			}
		}
	}

	private void writeContentTrl(String cmp, String language, Writer writer) throws Exception {
		this.writeContentByUrl(cmp, writer);
	}

	private void writeContentCmp(String cmp, Writer writer) throws Exception {
		this.writeContentByUrl(cmp, writer);
	}

	private void writeContentByUrl(String url, Writer writer) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Loading content from URL: {}", url);
		}
		HttpGet get = new HttpGet(url);
		try {
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = this.getHttpClient().execute(get, responseHandler);
			writer.write(responseBody);
		} catch (Exception e) {
			logger.error("Cannot find content at url " + url, e);
		} finally {
			get.releaseConnection();
		}
	}

	/**
	 * Resolve the dependencies of the given component and returns them in a list.
	 *
	 * @param cmp
	 * @return
	 * @throws Exception
	 */
	private List<String> resolveCmpDependencies(String cmp) throws Exception {
		String url = this.urlDpd(cmp);
		if (!url.startsWith(this.host)) {
			url = this.host + url;
		}
		HttpGet get = new HttpGet(url);
		ResponseHandler<String> responseHandler = new BasicResponseHandler();

		List<String> result = null;
		try {
			if (logger.isDebugEnabled()) {
				logger.debug("Calling http request: " + url);
			}

			String responseBody = this.getHttpClient().execute(get, responseHandler);

			result = this.getJsonMapper().readValue(responseBody, new TypeReference<List<String>>() {
			});

		} catch (HttpResponseException e) {
			if (e.getStatusCode() == 404) {
				logger.warn("Cannot find dependencies for component " + cmp + " at " + url);
			} else {
				if(logger.isDebugEnabled()){
					logger.debug(e.getMessage(),e);
				}
			}
		} catch (ClientProtocolException e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(),e);
			}
		} catch (IOException e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(),e);
			}
		}
		get.releaseConnection();
		return result;
	}

	/**
	 * Recursively resolve all the dependencies of the given component adding the elements to the given stack.
	 *
	 * @param cmp
	 * @param stack
	 * @throws Exception
	 */
	private void resolveAllDependencies(String cmp, Dependencies stack) throws Exception {

		List<String> deps = this.resolveCmpDependencies(cmp);
		if (deps != null) {
			for (String dep : deps) {

				String _type = dep.split("/")[1];

				if (_type.matches(Dependencies.TYPE_DS)) {
					stack.addDs(dep);
				} else if (_type.matches(Dependencies.TYPE_DC)) {
					stack.addDc(dep);
				} else if (_type.matches(Dependencies.TYPE_DC_CUST)) {
					stack.addDcCust(dep);
				} else if (_type.matches(Dependencies.TYPE_LOV)) {
					stack.addLov(dep);
				} else if (_type.matches(Dependencies.TYPE_ASGN)) {
					stack.addAsgn(dep);
				} else if (_type.matches(Dependencies.TYPE_UI_CUST)) {
					stack.addUiCust(dep);
				}

				this.resolveAllDependencies(dep, stack);
			}
		}
	}

	// private helpers

	/**
	 * Return the URL of the dependencies file for the specified component.
	 *
	 * @param bundle
	 * @param type
	 * @param name
	 * @return
	 */
	private String urlDpd(String cmp) {
		String[] tokens = cmp.split("/");
		String bundle = tokens[0];
		String type = tokens[1];
		String fqn = tokens[2];

		return this.urlDpd(bundle, type, fqn);
	}

	/**
	 * Return the URL of the dependencies file for the specified component.
	 *
	 * @param bundle
	 * @param type
	 * @param name
	 * @return
	 */
	private String urlDpd(String bundle, String type, String fqn) {
		String _bp = "";
		// if (this.moduleUseBundle) {
		// _bp = "/" + bundle + "/" + bundle + ".ui.extjs"
		// + this.urlUiExtjsModuleSubpath;
		// }
		return this.urlUiExtjsModules + _bp + "/" + fqn.replaceAll("\\.", "/") + ".jsdp";
	}

	/**
	 * Return the URL for the specified component.
	 *
	 * @param bundle
	 * @param type
	 * @param name
	 * @return
	 */
	private String urlCmp(String cmp) {
		String[] tokens = cmp.split("/");
		String bundle = tokens[0];
		String type = tokens[1];
		String name = tokens[2];

		return this.urlCmp(bundle, type, name);
	}

	/**
	 * Return the URL for the specified component.
	 *
	 * @param bundle
	 * @param type
	 * @param name
	 * @return
	 */
	private String urlCmp(String bundle, String type, String fqn) {

		String _bp = "";
		// if (this.moduleUseBundle) {
		// _bp = "/" + bundle + "/" + bundle + ".ui.extjs"
		// + this.urlUiExtjsModuleSubpath;
		// }

		String url = this.urlUiExtjsModules + _bp + "/" + fqn.replaceAll("\\.", "/") + ".js";
		if (logger.isDebugEnabled()) {
			logger.debug("Component/Type/Bundle: `{}/{}/{}`, url: `{}` ", new Object[] { fqn, type, bundle, url });
		}
		return url;
	}

	// /**
	// * Return the URL of the translation file for the specified component.
	// *
	// * @param bundle
	// * @param type
	// * @param name
	// * @param language
	// * @return
	// */
	// private String urlTrl(String cmp, String language) {
	// String[] tokens = cmp.split("/");
	// String bundle = tokens[0];
	// String type = tokens[1];
	// String name = tokens[2];
	//
	// return this.urlTrl(bundle, type, name, language);
	// }

	/**
	 * Return the URL of the translation file for the specified component.
	 *
	 * @param bundle
	 * @param type
	 * @param name
	 * @param language
	 * @return
	 */
	private String urlTrl(String bundle, String type, String fqn, String language) {

		String _bp = "";
		// if (this.moduleUseBundle) {
		// _bp = "/" + bundle + "/" + bundle + ".i18n"
		// + this.urlUiExtjsModuleSubpath;
		// }
		String url = this.urlUiExtjsModules + _bp + "/" + language + "/" + fqn.replace(".ui.extjs.", ".i18n.").replaceAll("\\.", "/") + ".js";
		if (logger.isDebugEnabled()) {
			logger.debug("Component/Type/Bundle/Language: `{}/{}/{}/{}`, url: `{}` ", new Object[] { fqn, type, bundle, language, url });
		}
		return url;
	}

	public HttpClient getHttpClient() {
		if (this.httpClient == null) {
			this.httpClient = HttpClientBuilder.create().useSystemProperties().build();
		}
		return this.httpClient;
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public ObjectMapper getJsonMapper() {
		return this.jsonMapper;
	}

	public void setJsonMapper(ObjectMapper jsonMapper) {
		this.jsonMapper = jsonMapper;
	}

	public String getUrlUiExtjsModules() {
		return this.urlUiExtjsModules;
	}

	public void setUrlUiExtjsModules(String urlUiExtjsModules) {
		if (urlUiExtjsModules.startsWith("http")) {
			this.urlUiExtjsModules = urlUiExtjsModules;
		} else {
			if (Constants.PROP_WORKING_MODE_PROD.equals(this.workingMode)) {
				this.urlUiExtjsModules = this.host + urlUiExtjsModules;
			} else {
				this.urlUiExtjsModules = urlUiExtjsModules;
			}
		}
	}

	public String getUrlUiExtjsModuleSubpath() {
		return this.urlUiExtjsModuleSubpath;
	}

	public void setUrlUiExtjsModuleSubpath(String urlUiExtjsModuleSubpath) {
		this.urlUiExtjsModuleSubpath = urlUiExtjsModuleSubpath;
	}

	public boolean isModuleUseBundle() {
		return this.moduleUseBundle;
	}

	public void setModuleUseBundle(boolean moduleUseBundle) {
		this.moduleUseBundle = moduleUseBundle;
	}

}
