/**
 *
 */
package seava.j4e.web.controller.monitoring;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author tlukacs
 */
public class MonitoringHttpSessions implements HttpSessionListener {

	private static int activeSessions = 0;

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		increment();
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		decrement();
	}

	/**
	 * @return the number of sessions
	 */
	public static synchronized int getActiveSessions() {
		return activeSessions;
	}

	// private helpers

	private static synchronized void increment() {
		activeSessions++;
	}

	private static synchronized void decrement() {
		if (activeSessions > 0) {
			activeSessions--;
		}
	}
}
