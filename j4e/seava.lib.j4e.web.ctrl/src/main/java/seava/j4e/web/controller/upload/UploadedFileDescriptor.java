/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.controller.upload;

import seava.j4e.api.descriptor.IUploadedFileDescriptor;

public class UploadedFileDescriptor implements IUploadedFileDescriptor {

	String originalName;
	String newName;
	String contentType;
	long size;

	@Override
	public String getOriginalName() {
		return this.originalName;
	}

	@Override
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	@Override
	public String getNewName() {
		return this.newName;
	}

	@Override
	public void setNewName(String newName) {
		this.newName = newName;
	}

	@Override
	public String getContentType() {
		return this.contentType;
	}

	@Override
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Override
	public long getSize() {
		return this.size;
	}

	@Override
	public void setSize(long size) {
		this.size = size;
	}

}
