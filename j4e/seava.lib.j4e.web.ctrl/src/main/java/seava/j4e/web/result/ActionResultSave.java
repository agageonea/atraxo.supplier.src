/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.result;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import seava.j4e.api.action.result.IActionResultSave;

@XmlRootElement(name = "result")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActionResultSave extends AbstractResultData implements IActionResultSave {

	/**
	 * Actual result data list.
	 */
	@XmlElementWrapper(name = "data-list")
	@XmlElement(name = "data")
	private List<?> data;

	/**
	 * Parameters.
	 */
	private Object params;

	/**
	 * @return the data
	 */
	@Override
	public List<?> getData() {
		return this.data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	@Override
	public void setData(List<?> data) {
		this.data = data;
	}

	@Override
	public Object getParams() {
		return this.params;
	}

	@Override
	public void setParams(Object params) {
		this.params = params;
	}
}
