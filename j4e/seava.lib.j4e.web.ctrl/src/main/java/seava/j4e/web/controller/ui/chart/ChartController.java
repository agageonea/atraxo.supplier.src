package seava.j4e.web.controller.ui.chart;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import seava.j4e.api.Constants;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.query.ISortToken;
import seava.j4e.api.action.result.IDsMarshaller;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.session.ISessionUser;
import seava.j4e.web.controller.AbstractBaseController;

/**
 * ChartController
 */
@Controller
public class ChartController extends AbstractBaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChartController.class);

	@RequestMapping(value = "/{resourceName}", method = RequestMethod.GET)
	protected ModelAndView home(@PathVariable String resourceName,
			@RequestParam(value = "chartType", required = true, defaultValue = "line") String chartType,
			@RequestParam(value = "xField", required = true) String xField, @RequestParam(value = "yField", required = true) String yField,
			@RequestParam(value = "title", required = true, defaultValue = "") String title,
			@RequestParam(value = Constants.REQUEST_PARAM_FILTER, required = false, defaultValue = "{}") String filterString,
			@RequestParam(value = Constants.REQUEST_PARAM_ADVANCED_FILTER, required = false, defaultValue = "") String filterRulesString,
			@RequestParam(value = Constants.REQUEST_PARAM_PARAMS, required = false, defaultValue = "{}") String paramString,
			@RequestParam(value = Constants.REQUEST_PARAM_SORT, required = false, defaultValue = "") String orderByCol,
			@RequestParam(value = Constants.REQUEST_PARAM_SENSE, required = false, defaultValue = "") String orderBySense,
			@RequestParam(value = Constants.REQUEST_PARAM_ORDERBY, required = false, defaultValue = "") String orderBy, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String jspName = "chart/google-chart";
		try {
			@SuppressWarnings("unused")
			ISessionUser su = (ISessionUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (java.lang.ClassCastException e) {
			LOGGER.warn("Could not get authentication, will redirect to login page!", e);
			response.sendRedirect(this.getSettings().get(Constants.PROP_LOGIN_PAGE));
			return null;
		}

		Map<String, Object> model = new HashMap<>();
		this.prepareRequest(request, response);

		// -------------------------------

		this.authorizeDsAction(resourceName, Constants.DS_ACTION_QUERY, null);

		IDsService<Object, Object, Object> service = this.getServiceLocator().findDsService(resourceName);

		IDsMarshaller<Object, Object, Object> marshaller = service.createMarshaller(IDsMarshaller.JSON);

		Object filter = marshaller.readFilterFromString(filterString);
		Object params = marshaller.readParamsFromString(paramString);

		IQueryBuilder<Object, Object, Object> builder = service.createQueryBuilder().addFetchLimit(0, 10000).addFilter(filter).addParams(params);

		if (orderBy != null && !"".equals(orderBy)) {
			List<ISortToken> sortTokens = marshaller.readSortTokens(orderBy);
			builder.addSortInfo(sortTokens);
		} else {
			builder.addSortInfo(orderByCol, orderBySense);
		}

		if (filterRulesString != null && !"".equals(filterRulesString)) {
			List<IFilterRule> filterRules = marshaller.readFilterRules(filterRulesString);
			builder.addFilterRules(filterRules);
		}

		List<?> list = service.find(builder);

		model.put("dataList", list);

		model.put("xField", xField);
		model.put("yField", yField);
		model.put("title", title);

		// -------------------------------
		String chartKey = "chart";
		if ("line".equals(chartType)) {
			model.put(chartKey, "google.visualization.LineChart");
		} else if ("pie".equals(chartType)) {
			model.put(chartKey, "google.visualization.PieChart");
		} else if ("bar".equals(chartType)) {
			model.put(chartKey, "google.visualization.BarChart");
		} else if ("column".equals(chartType)) {
			model.put(chartKey, "google.visualization.ColumnChart");
		} else if ("candlestick".equals(chartType)) {
			model.put(chartKey, "google.visualization.CandlestickChart");
		} else if ("geo".equals(chartType)) {
			model.put(chartKey, "google.visualization.GeoChart");
		}

		/* ========== extensions =========== */

		this.finishRequest();
		return new ModelAndView(jspName, model);
	}
}
