package seava.j4e.web.controller.monitoring;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import seava.j4e.api.ISettings;
import seava.j4e.web.controller.AbstractBaseController;

/**
 * @author tlukacs
 */

@Controller
public class MonitoringController extends AbstractBaseController {

	private static final Logger log = LoggerFactory.getLogger(MonitoringController.class);

	private static int finishedCount;
	private static boolean asF;
	private static boolean dbF;
	private static boolean mmF;
	private static boolean rcF;
	private static boolean wdF;

	private DataSource dataSource;

	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/monitoring", method = RequestMethod.GET)
	@ResponseBody
	public void monitoring(HttpServletRequest request, HttpServletResponse response) throws Exception {

		long tReqStart = System.currentTimeMillis();
		initMonitoring();

		try {
			List<String> info = new ArrayList<>();
			ISettings cfg = this.getSettings();

			Monitor mAS = new Monitor(MonitorType.APP_SERVER, request, cfg, this.dataSource);
			Thread tAS = new Thread(mAS, "APP_SERVER");
			Monitor mDB = new Monitor(MonitorType.DATABASE, request, cfg, this.dataSource);
			Thread tDB = new Thread(mDB, "DATABASE");
			Monitor mRC = new Monitor(MonitorType.REPORT_CENTER, request, cfg, this.dataSource);
			Thread tRC = new Thread(mRC, "REPORT_CENTER");
			Monitor mMM = new Monitor(MonitorType.MAIL_MERGE, request, cfg, this.dataSource);
			Thread tMM = new Thread(mMM, "MAIL_MERGE");
			Monitor mWD = new Monitor(MonitorType.WEB_DAV, request, cfg, this.dataSource);
			Thread tWD = new Thread(mWD, "WEB_DAV");

			tWD.start();
			tMM.start();
			tRC.start();
			tAS.start();
			tDB.start();

			int cnt = 24;
			int wait = Monitor.HTTP_MONITORING_TIMEOUT * 50;
			while (cnt > 0) {
				cnt--;
				Thread.sleep(wait);
				if (finishedCount >= 5) {
					log.debug("All threads terminated the job.");
					break;
				}
			}

			boolean asRes = mAS.isAppInfoRes();
			boolean dbRes = mDB.isDbInfoRes();

			ServletOutputStream out = response.getOutputStream();
			if (asRes && dbRes) {
				out.println("OK");
			} else {
				out.println("ERROR");
			}
			if (asF) {
				this.readDataFrom(mAS, info);
			} else {
				this.addNoResponse(info, "Application Server");
			}
			if (dbF) {
				this.readDataFrom(mDB, info);
			} else {
				this.addNoResponse(info, "Database");
			}
			if (rcF) {
				this.readDataFrom(mRC, info);
			} else {
				this.addNoResponse(info, "Report Center");
			}
			if (mmF) {
				this.readDataFrom(mMM, info);
			} else {
				this.addNoResponse(info, "MailMerge");
			}
			if (wdF) {
				this.readDataFrom(mWD, info);
			} else {
				this.addNoResponse(info, "WebDav Server");
			}

			long tReqStop = System.currentTimeMillis();
			Monitor.addReqDuration(info, "Monitoring call", tReqStart, tReqStop);
			for (String s : info) {
				out.println(s);
			}
			out.flush();
		} catch (Exception e) {
			try {
				response.getOutputStream().println("ERROR");
				response.getOutputStream().println(e.getMessage());
				response.getOutputStream().flush();
			} catch (Exception e1) {
				if (log.isDebugEnabled()) {
					log.debug(e1.getMessage(), e1);
				}
			}
			throw e;
		} finally {
			this.finishRequest();
		}
	}

	private void readDataFrom(Monitor m, List<String> info) {
		List<String> l = m.getInfo();
		synchronized (l) {
			info.addAll(l);
		}
	}

	private void addNoResponse(List<String> info, String fromWhere) {
		info.add("No response received from " + fromWhere + " in " + Monitor.HTTP_MONITORING_TIMEOUT + " seconds.");
	}

	private static void initMonitoring() {
		MonitoringController.finishedCount = 0;
		asF = false;
		dbF = false;
		mmF = false;
		rcF = false;
		wdF = false;
	}

	/**
	 * @param type
	 */
	public static synchronized void monitorFinished(MonitorType type) {
		switch (type) {
		case APP_SERVER:
			finishedCount++;
			asF = true;
			break;
		case DATABASE:
			finishedCount++;
			dbF = true;
			break;
		case MAIL_MERGE:
			finishedCount++;
			mmF = true;
			break;
		case REPORT_CENTER:
			finishedCount++;
			rcF = true;
			break;
		case WEB_DAV:
			finishedCount++;
			wdF = true;
			break;
		default:
			break;
		}
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return this.dataSource;
	}

}

enum MonitorType {
	APP_SERVER, DATABASE, REPORT_CENTER, MAIL_MERGE, WEB_DAV;
}

class Monitor implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(MonitoringController.class);

	public static final int HTTP_MONITORING_TIMEOUT = 4;

	private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	// private HttpServletRequest httpRequest;
	private DataSource dataSource;
	ISettings cfg;

	private List<String> info;

	private boolean appInfoRes;
	private boolean dbInfoRes;
	private MonitorType monitorType;

	public Monitor(MonitorType type, HttpServletRequest req, ISettings settings, DataSource ds) {
		this.info = new ArrayList<>();
		// this.httpRequest = req;
		this.monitorType = type;
		this.cfg = settings;
		this.dataSource = ds;
	}

	@Override
	public void run() {
		switch (this.monitorType) {
		case APP_SERVER:
			this.appInfoRes = this.collectAppServerInfo();
			break;
		case DATABASE:
			this.dbInfoRes = this.collectDbInfo();
			break;
		case MAIL_MERGE:
			this.collectMailMergeInfo();
			break;
		case REPORT_CENTER:
			this.collectReportCenterInfo();
			break;
		case WEB_DAV:
			this.collectWebDavInfo();
			break;
		default:
			break;
		}
		MonitoringController.monitorFinished(this.monitorType);
	}

	public List<String> getInfo() {
		return this.info;
	}

	public boolean isAppInfoRes() {
		return this.appInfoRes;
	}

	public boolean isDbInfoRes() {
		return this.dbInfoRes;
	}

	public static void addReqDuration(List<String> list, String srv, long tReqStart, long tReqStop) {
		list.add(srv + " response time is: " + (tReqStop - tReqStart) + " ms.");
	}

	private boolean collectAppServerInfo() {
		boolean res = true;
		try {
			if (this.cfg == null) {
				res = false;
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			res = false;
		}
		if (!res) {
			log.error("Could not access application settings!");
			this.info.add("ERROR: could not access application settings!");
		} else {

			this.info.add("Application Server info");
			Date date = new Date();
			this.info.add("Request server time: " + this.dateFormat.format(date));
			this.info.add("Product name: " + this.cfg.getProductName());
			this.info.add("Product vendor: " + this.cfg.getProductVendor());

			try {
				this.info.add("HostName: " + InetAddress.getLocalHost().getHostName());
			} catch (Exception e1) {
				this.info.add("HostName: null");
				log.warn(e1.getMessage(), e1);
			}

			// try {
			// String protocol;
			// if (this.httpRequest.isSecure()) {
			// protocol = "https://";
			// } else {
			// protocol = "http://";
			// }
			//
			// RequestConfig config = RequestConfig.custom().setConnectTimeout(HTTP_MONITORING_TIMEOUT * 1000)
			// .setConnectionRequestTimeout(HTTP_MONITORING_TIMEOUT * 1000).setSocketTimeout(HTTP_MONITORING_TIMEOUT * 1000).build();
			//
			// BasicCookieStore cookieStore = new BasicCookieStore();
			// HttpSession httpSession = this.httpRequest.getSession();
			// if (httpSession != null) {
			// BasicClientCookie cookie = new BasicClientCookie("JSESSIONID", httpSession.getId());
			// cookie.setDomain(this.httpRequest.getLocalAddr());
			// cookie.setPath("/");
			// cookieStore.addCookie(cookie);
			// }
			//
			// HttpGet req = new HttpGet(protocol + this.httpRequest.getLocalAddr() + ":" + this.httpRequest.getLocalPort()
			// + this.cfg.get(Constants.PROP_CTXPATH) + Constants.URL_UI_EXTJS);
			//
			// CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(config).setDefaultCookieStore(cookieStore).build();
			//
			// long tReqStart = System.currentTimeMillis();
			// HttpResponse resp = client.execute(req);
			// long tReqStop = System.currentTimeMillis();
			// StatusLine sl = resp.getStatusLine();
			// res = sl.getStatusCode() < 400;
			// this.info.add("App Server Response Code : " + sl);
			// addReqDuration(this.info, "App Server", tReqStart, tReqStop);
			// client.close();
			// } catch (IOException e) {
			// this.info.add("App Server Response Code : " + e.getMessage());
			// log.error(e.getMessage(), e);
			// res = false;
			// }

			// if (res) {
			this.info.add("Application server: Up and running");
			// } else {
			// this.info.add("Application server: Something is wrong, server is not responding to the request as expected");
			// }
		}

		// Getting the runtime reference from system to read the memory utilization status
		Runtime runtime = Runtime.getRuntime();
		int mb = 1024 * 1024;
		this.info.add("Heap memory utilization [MB]");
		this.info.add("Used memory: " + (runtime.totalMemory() - runtime.freeMemory()) / mb);
		this.info.add("Free memory: " + runtime.freeMemory() / mb);
		this.info.add("Total memory: " + runtime.totalMemory() / mb);
		this.info.add("Max memory: " + runtime.maxMemory() / mb);
		this.info.add("Java version: " + Runtime.class.getPackage().getImplementationVersion());

		this.info.add("HTTP sessions: " + MonitoringHttpSessions.getActiveSessions());

		return res;
	}

	private boolean collectDbInfo() {
		PreparedStatement stmt = null;
		Connection con = null;
		ResultSet rs = null;
		boolean res = true;

		try {
			this.info.add("Database info");

			con = this.dataSource.getConnection();

			stmt = con.prepareStatement("SELECT dateexecuted, tag FROM DATABASECHANGELOG WHERE tag IS NOT NULL ORDER BY dateexecuted DESC LIMIT 1");
			long tReqStart = System.currentTimeMillis();
			rs = stmt.executeQuery();
			long tReqStop = System.currentTimeMillis();

			this.info.add("Database: Up and running");
			if (rs.next()) {
				this.info.add("Liquibase version: " + rs.getString("tag"));
				this.info.add("Date executed: " + this.dateFormat.format(rs.getTimestamp("dateexecuted")));
			}
			addReqDuration(this.info, "Database", tReqStart, tReqStop);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			this.info.add("DB Error");
			this.info.add(e.getMessage());
			res = false;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				log.error(e1.getMessage(), e1);
			}
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception e1) {
				log.error(e1.getMessage(), e1);
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception e2) {
				log.error(e2.getMessage(), e2);
			}
		}

		return res;
	}

	private void collectReportCenterInfo() {
		try {
			RequestConfig config = RequestConfig.custom().setConnectTimeout(HTTP_MONITORING_TIMEOUT * 1000)
					.setConnectionRequestTimeout(HTTP_MONITORING_TIMEOUT * 1000).setSocketTimeout(HTTP_MONITORING_TIMEOUT * 1000).build();

			HttpGet request = new HttpGet(this.cfg.getReportCenterUrl().replaceFirst("rest/reports/sch.json", "test/index.html"));
			CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
			long tReqStart = System.currentTimeMillis();
			HttpResponse response = client.execute(request);
			long tReqStop = System.currentTimeMillis();
			this.info.add("Report Center Response Code : " + response.getStatusLine());
			addReqDuration(this.info, "Report Center", tReqStart, tReqStop);
			client.close();
		} catch (IOException e) {
			this.info.add("Report Center Response Code : " + e.getMessage());
			log.error(e.getMessage(), e);
		}
	}

	private void collectMailMergeInfo() {
		try {
			RequestConfig config = RequestConfig.custom().setConnectTimeout(HTTP_MONITORING_TIMEOUT * 1000)
					.setConnectionRequestTimeout(HTTP_MONITORING_TIMEOUT * 1000).setSocketTimeout(HTTP_MONITORING_TIMEOUT * 1000).build();

			HttpGet request = new HttpGet(this.cfg.getMailMergeUrl());
			CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
			long tReqStart = System.currentTimeMillis();
			HttpResponse response = client.execute(request);
			long tReqStop = System.currentTimeMillis();
			this.info.add("MailMerge Response Code : " + response.getStatusLine());
			addReqDuration(this.info, "MailMerge", tReqStart, tReqStop);
			client.close();
		} catch (IOException e) {
			this.info.add("MailMerge Response Code : " + e.getMessage());
			log.error(e.getMessage(), e);
		}
	}

	private void collectWebDavInfo() {
		try {
			RequestConfig config = RequestConfig.custom().setConnectTimeout(HTTP_MONITORING_TIMEOUT * 1000)
					.setConnectionRequestTimeout(HTTP_MONITORING_TIMEOUT * 1000).setSocketTimeout(HTTP_MONITORING_TIMEOUT * 1000).build();

			HttpGet request = new HttpGet(this.cfg.getWebDavUrl());
			String auth = this.cfg.getWebDavUser() + ":" + this.cfg.getWebDavPassword();
			byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
			String authHeader = "Basic " + new String(encodedAuth);
			request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
			CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
			long tReqStart = System.currentTimeMillis();
			HttpResponse response = client.execute(request);
			long tReqStop = System.currentTimeMillis();
			this.info.add("WebDav Server Response Code : " + response.getStatusLine());
			addReqDuration(this.info, "WebDav Server", tReqStart, tReqStop);
			client.close();
		} catch (IOException e) {
			this.info.add("WebDav Server Response Code : " + e.getMessage());
			log.error(e.getMessage(), e);
		}
	}

}
