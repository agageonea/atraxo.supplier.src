package seava.j4e.web.spring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

/**
 * Temporary override as workaround for bug: SPR-10999 <br> <br> mvc:resources should support multiple locations by placeholder <br> <br>
 * https://jira.spring.io/browse/SPR-10999
 *
 * @author amathe
 */
public class MyResourceHttpRequestHandler extends ResourceHttpRequestHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(MyResourceHttpRequestHandler.class);

	/**
	 * Set a {@code List} of {@code Resource} paths to use as sources for serving static resources.
	 */
	@Override
	public void setLocations(List<Resource> locations) {
		List<Resource> processed = new ArrayList<Resource>();

		if (locations != null) {
			for (Resource location : locations) {
				try {
					if (location == null) {
						// statics.location is empty
						continue;
					}
					if (location.getURL().toString().contains(",")) {
						String[] tokens = location.getURL().toString().split(",");
						for (int i = 0; i < tokens.length; i++) {

							Resource _location = new UrlResource(tokens[i]);
							processed.add(_location);
						}
					} else {
						processed.add(location);
					}
				} catch (IOException e) {
					LOGGER.error("Got input/output exception when setting locations !", e);
				}
			}
		}
		super.setLocations(processed);
	}
}