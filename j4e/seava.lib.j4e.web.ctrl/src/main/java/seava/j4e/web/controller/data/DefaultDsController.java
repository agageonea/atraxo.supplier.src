/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.controller.data;

import org.springframework.web.bind.annotation.RequestMapping;

import seava.j4e.api.Constants;

@RequestMapping(value = Constants.CTXPATH_DS + "/{resourceName}.{dataFormat}")
public class DefaultDsController<M, F, P> extends AbstractDsWriteController<M, F, P> {

}
