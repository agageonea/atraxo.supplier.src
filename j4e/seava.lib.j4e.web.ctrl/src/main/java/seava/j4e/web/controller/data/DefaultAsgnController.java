/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.controller.data;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import seava.j4e.api.Constants;

@Controller
@RequestMapping(value = Constants.CTXPATH_ASGN + "/{resourceName}.{dataFormat}")
public class DefaultAsgnController<M, F, P> extends AbstractAsgnController<M, F, P> {

}
