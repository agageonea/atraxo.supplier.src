/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.controller.ui.extjs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import seava.j4e.api.Constants;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.extensions.IExtensions;
import seava.j4e.api.session.ISessionUser;
import seava.j4e.api.setup.ISetupParticipant;
import seava.j4e.api.setup.IStartupParticipant;

/**
 * UiExtjsMainController
 */
@Controller
public class UiExtjsMainController extends AbstractUiExtjsController {

	private static final Logger logger = LoggerFactory.getLogger(UiExtjsMainController.class);

	protected List<ISetupParticipant> setupParticipants;
	protected List<IStartupParticipant> startupParticipants;

	@RequestMapping(value = "*", method = RequestMethod.GET)
	protected ModelAndView home(HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			ISessionUser su = (ISessionUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			if (su.isSessionLocked()) {
				throw new BusinessException(ErrorCode.SEC_SESSION_LOCKED, "Session has been locked.");
			}
		} catch (Exception e) {
			logger.info("Could not authentication, will redirect to login page !", e);
			response.sendRedirect(this.getSettings().get(Constants.PROP_LOGIN_PAGE));
			return null;
		}

		Map<String, Object> model = new HashMap<>();
		this._prepare(model, request, response);

		/* ========== extensions =========== */

		model.put("extensions", this.getExtensionFiles(IExtensions.UI_EXTJS_MAIN, this.uiExtjsSettings.getUrlCore()));

		model.put("extensionsContent", this.getExtensionContent(IExtensions.UI_EXTJS_MAIN));

		String logo = this.getSettings().getParam(SysParam.CORE_LOGO_URL_EXTJS.name());

		if (logo != null && !"".equals(logo)) {
			model.put("logo", logo);
		}
		return new ModelAndView(this.viewName, model);
	}

	public List<ISetupParticipant> getSetupParticipants() {
		return this.setupParticipants;
	}

	public void setSetupParticipants(List<ISetupParticipant> setupParticipants) {
		this.setupParticipants = setupParticipants;
	}

	public List<IStartupParticipant> getStartupParticipants() {
		return this.startupParticipants;
	}

	public void setStartupParticipants(List<IStartupParticipant> startupParticipants) {
		this.startupParticipants = startupParticipants;
	}

}
