/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.controller.ui.extjs;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import seava.j4e.api.Constants;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.exceptions.NotAuthorizedRequestException;
import seava.j4e.api.session.ISessionUser;

/**
 * UiExtjsFrameController
 */
@Controller
@RequestMapping(value = "/frame", method = RequestMethod.GET)
public class UiExtjsFrameController extends AbstractUiExtjsController {

	private String cacheFolder;
	private Boolean cacheFolderWritable;

	/**
	 * Handler for a frame html page.
	 *
	 * @param frame
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{bundle}/{frameFQN}", method = RequestMethod.GET)
	protected ModelAndView home(@PathVariable("frameFQN") String frame, HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			try {
				@SuppressWarnings("unused")
				ISessionUser su = (ISessionUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			} catch (java.lang.ClassCastException e) {
				throw new BusinessException(ErrorCode.SEC_NOT_AUTHENTICATED, "Not authenticated", e);
			}

			Map<String, Object> model = new HashMap<>();
			this._prepare(model, request, response);

			String[] tmp = request.getPathInfo().split("/");
			String frameFQN = tmp[tmp.length - 1];
			String bundle = tmp[tmp.length - 2];
			String[] t = frameFQN.split("\\.");
			String frameName = t[t.length - 1];

			model.put("item", frameFQN);
			model.put("itemSimpleName", frameName);
			model.put("bundle", bundle);

			// get extensions
			model.put("extensions", this.getExtensionFiles(frameFQN, this.uiExtjsSettings.getUrlModules()));

			model.put("extensionsContent", this.getExtensionContent(frameFQN));

			String logo = this.getSettings().getParam(SysParam.CORE_LOGO_URL_EXTJS.name());

			if (logo != null && !"".equals(logo)) {
				model.put("logo", logo);
			}

			if (Constants.PROP_WORKING_MODE_DEV.matches(this.getSettings().get(Constants.PROP_WORKING_MODE))) {

				List<String> listCmp = new ArrayList<>();
				List<String> listTrl = new ArrayList<>();

				DependencyLoader loader = this.getDependencyLoader(request);
				String lang = (String) model.get("shortLanguage");
				loader.resolveFrameDependencies(bundle, frameFQN, lang, listCmp, listTrl);

				this.addDefaultLanguageFiles(listTrl, lang);

				model.put("frameDependenciesCmp", listCmp);
				model.put("frameDependenciesTrl", listTrl);

			} else {
				if (this.cacheFolderWritable == null) {
					synchronized (this) {
						if (this.cacheFolderWritable == null) {

							if (this.cacheFolder == null) {
								this.cacheFolder = this.getUiExtjsSettings().getCacheFolder();
							}

							File cf = new File(this.cacheFolder);
							if (!cf.exists()) {

								if (!cf.mkdirs()) {
									throw new Exception("Cache folder " + this.cacheFolder + " does not exist and could not be created.");
								}
							}

							if (!cf.isDirectory() || !cf.canWrite()) {
								throw new Exception("Cache folder " + this.cacheFolder
										+ " is not writeable. Cannot pack and cache the frame dependencies for the configured `prod` working mode. ");
							}
							this.cacheFolderWritable = true;
						}
					}
				}
			}
			return new ModelAndView(this.viewName, model);
		} catch (Exception e) {
			this.handleManagedExceptionAsHtml(null, e, response);
			return null;
		}
	}

	private void addDefaultLanguageFiles(List<String> listTrl, String lang) {
		// load the System and the English language files if needed
		if (!"sys".equalsIgnoreCase(lang)) {
			String key = "/" + lang + "/";
			String newS;
			List<String> listTrlDef = new ArrayList<>();

			for (String s : listTrl) {
				newS = s.replaceFirst(key, "/sys/");
				if (!s.equals(newS)) {
					listTrlDef.add(newS);
				}
			}
			if (!"en".equalsIgnoreCase(lang)) {
				for (String s : listTrl) {
					newS = s.replaceFirst(key, "/en/");
					if (!s.equals(newS)) {
						listTrlDef.add(newS);
					}
				}
			}
			// merge new list to the main list
			listTrl.addAll(0, listTrlDef);
		}
	}

	/**
	 * Handler to return the cached js file with the dependent components.
	 *
	 * @param bundle
	 * @param frame
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{bundle}/{frame}.js", method = RequestMethod.GET)
	@ResponseBody
	public String frameCmpJs(@PathVariable("bundle") String bundle, @PathVariable("frame") String frame, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		try {
			@SuppressWarnings("unused")
			ISessionUser su = (ISessionUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (java.lang.ClassCastException e) {
			logger.info("Not authenticated, will throw NotAuthorizedRequestException !", e);
			throw new NotAuthorizedRequestException("Not authenticated");
		}

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String fileName = frame + ".js";
		File f = new File(this.cacheFolder + "/" + bundle + "." + fileName);

		if (!f.exists()) {
			DependencyLoader loader = this.getDependencyLoader(request);
			loader.packFrameCmp(bundle, frame, f);
		}

		this.sendFile(f, response.getOutputStream());

		return null;
	}

	/**
	 * Handler to return the cached js file with the dependent translations.
	 *
	 * @param bundle
	 * @param frame
	 * @param language
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{bundle}/{language}/{frame}.js", method = RequestMethod.GET)
	@ResponseBody
	public String frameTrlJs(@PathVariable("bundle") String bundle, @PathVariable("frame") String frame, @PathVariable("language") String language,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			@SuppressWarnings("unused")
			ISessionUser su = (ISessionUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (java.lang.ClassCastException e) {
			logger.info("Not authenticated, will throw NotAuthorizedRequestException !", e);
			throw new NotAuthorizedRequestException("Not authenticated");
		}

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		String fileName = frame + "-" + language + ".js";
		File f = new File(this.cacheFolder + "/" + bundle + "." + fileName);
		if (!f.exists()) {
			DependencyLoader loader = this.getDependencyLoader(request);
			loader.packFrameTrl(bundle, frame, language, f);
		}

		this.sendFile(f, response.getOutputStream());
		return null;
	}

	/**
	 * Helper method to create , configure and return an DependencyLoader instance
	 *
	 * @return
	 */
	private DependencyLoader getDependencyLoader(HttpServletRequest request) {
		String protocol = request.getScheme();
		String host = protocol + "://" + request.getLocalName();
		if (request.getLocalPort() != 80) {
			host += ":" + request.getLocalPort();
		}
		String workingMode = this.getSettings().get(Constants.PROP_WORKING_MODE);
		host += "/";
		if (logger.isDebugEnabled()) {
			logger.debug("Get dependency loader for host: " + host + ", modules url: " + this.getUiExtjsSettings().getUrlModules()
					+ ", working mode: " + workingMode);
		}
		DependencyLoader loader = new DependencyLoader(host, workingMode);
		loader.setUrlUiExtjsModules(this.getUiExtjsSettings().getUrlModules());
		loader.setModuleUseBundle(this.getUiExtjsSettings().isModuleUseBundle());
		loader.setUrlUiExtjsModuleSubpath(this.getUiExtjsSettings().getModuleSubpath());
		return loader;
	}
}
