/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.controller.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import seava.j4e.api.Constants;
import seava.j4e.api.action.impex.IDsExport;
import seava.j4e.api.action.impex.IDsReport;
import seava.j4e.api.action.impex.IExcludeInfo;
import seava.j4e.api.action.impex.IExportInfo;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.query.ISortToken;
import seava.j4e.api.action.result.IActionResultFind;
import seava.j4e.api.action.result.IDsMarshaller;
import seava.j4e.api.descriptor.IDsDefinition;
import seava.j4e.api.descriptor.IDsDefinitions;
import seava.j4e.api.enums.SysParam;
import seava.j4e.api.exceptions.BusinessException;
import seava.j4e.api.exceptions.ErrorCode;
import seava.j4e.api.model.AppNotification;
import seava.j4e.api.service.presenter.IDsService;
import seava.j4e.api.service.report.IReportService;
import seava.j4e.api.session.IUser;
import seava.j4e.api.session.Session;
import seava.j4e.domain.report.Category;
import seava.j4e.presenter.action.impex.DialogReportInfo;
import seava.j4e.presenter.action.impex.ExportInfo;
import seava.j4e.presenter.action.impex.report.jaxb.DsDialogReport;
import seava.j4e.presenter.action.impex.report.jaxb.DsDynamicReport;
import seava.j4e.presenter.action.marshaller.ModelPrinter;
import seava.j4e.presenter.action.marshaller.XmlMarshaller;
import seava.j4e.presenter.model.AbstractDsModel;
import seava.j4e.presenter.service.notification.NotificationPresenterService;
import seava.j4e.web.result.ActionResultFind;

/**
 * @author
 * @param <M>
 * @param <F>
 * @param <P>
 */
public class AbstractDsReadController<M, F, P> extends AbstractDsController<M, F, P> {

	private static final String REPO_ID = "repoId";
	private static final String DEV_MODE = "devMode";
	private static final String DATA = "data";
	protected static final String DEFAULT_RESULT_START = "0";
	protected static final String DEFAULT_RESULT_SIZE = "1000";
	protected static final String DEFAULT_RESULT_SIZE_EXPORT = "1000000";
	private static final int RETRY_COUNT = 3;

	static final Logger logger = LoggerFactory.getLogger(AbstractDsReadController.class);

	private HttpClient httpClient;

	/**
	 * @param resourceName
	 * @param dataFormat
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_NOTIFY)
	@ResponseBody
	public DeferredResult<AppNotification> notify(@PathVariable String resourceName, @PathVariable String dataFormat, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		if (logger.isInfoEnabled()) {
			logger.info("Processing request: {}.{} -> action = {} ", new Object[] { resourceName, dataFormat, Constants.DS_ACTION_NOTIFY });
		}

		this.prepareRequest(request, response);

		this.authorizeDsAction(resourceName, Constants.DS_ACTION_NOTIFY, null);
		IUser u = Session.user.get();
		AppNotification appNotification = new AppNotification(-1, "", "", u.getClientId());
		appNotification.setCategory(Category._TIMEOUT_.getName());
		final DeferredResult<AppNotification> result = new DeferredResult<>(60 * 1000l, appNotification);
		if (u.getClientId() != null) {
			NotificationPresenterService updateService = this.getApplicationContext().getBean(NotificationPresenterService.class);

			updateService.getUpdate(u.getClientId(), u.getCode(), result);
		}

		stopWatch.stop();
		return result;
	}

	/**
	 * Default handler for find action.
	 *
	 * @param resourceName
	 * @param dataFormat
	 * @param filterString
	 * @param filterAttrString
	 * @param filterRulesString
	 * @param paramString
	 * @param resultStart
	 * @param resultSize
	 * @param page
	 * @param orderByCol
	 * @param orderBySense
	 * @param orderBy
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_QUERY)
	@ResponseBody
	public String find(@PathVariable String resourceName, @PathVariable String dataFormat,
			@RequestParam(value = Constants.REQUEST_PARAM_FILTER, required = false, defaultValue = "{}") String filterString,
			@RequestParam(value = Constants.REQUEST_PARAM_FILTER_ATTRIBUTES, required = false, defaultValue = "{}") String filterAttrString,
			@RequestParam(value = Constants.REQUEST_PARAM_ADVANCED_FILTER, required = false, defaultValue = "") String filterRulesString,
			@RequestParam(value = Constants.REQUEST_PARAM_PARAMS, required = false, defaultValue = "{}") String paramString,
			@RequestParam(value = Constants.REQUEST_PARAM_START, required = false, defaultValue = DEFAULT_RESULT_START) int resultStart,
			@RequestParam(value = Constants.REQUEST_PARAM_SIZE, required = false, defaultValue = DEFAULT_RESULT_SIZE) int resultSize,
			@RequestParam(value = Constants.REQUEST_PARAM_PAGE, required = false, defaultValue = "0") int page,
			@RequestParam(value = Constants.REQUEST_PARAM_SORT, required = false, defaultValue = "") String orderByCol,
			@RequestParam(value = Constants.REQUEST_PARAM_SENSE, required = false, defaultValue = "") String orderBySense,
			@RequestParam(value = Constants.REQUEST_PARAM_ORDERBY, required = false, defaultValue = "") String orderBy, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (page > 0) {
				// page parameter has priority over resultStart - needed because of infinite scrolling grid
				resultStart = (page - 1) * resultSize;
			}

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ", new Object[] { resourceName, dataFormat, Constants.DS_ACTION_QUERY });
			}

			if (logger.isDebugEnabled()) {
				logger.debug("  --> request-filter: {} ", new Object[] { filterString });
				logger.debug("  --> request-filter-attr: {} ", new Object[] { filterAttrString });
				logger.debug("  --> request-params: {} ", new Object[] { paramString });
				logger.debug("  --> request-orderBy: sort={}, sense={}, orderBy={}", new Object[] { orderByCol, orderBySense, orderBy });
				logger.debug("  --> request-result-range: {} ",
						new Object[] { Integer.toString(resultStart), Integer.toString(resultStart + resultSize) });
			}
			this.prepareRequest(request, response);

			this.authorizeDsAction(resourceName, Constants.DS_ACTION_QUERY, null);

			IDsService<M, F, P> service = this.findDsService(resourceName);
			IDsMarshaller<M, F, P> marshaller = service.createMarshaller(IDsMarshaller.JSON);

			F filter = marshaller.readFilterFromString(filterString);
			P params = marshaller.readParamsFromString(paramString);
			if (filter instanceof AbstractDsModel<?>) {
				((AbstractDsModel<F>) filter).set__filterAttrString__(filterAttrString);
			}

			IQueryBuilder<M, F, P> builder = service.createQueryBuilder().addFetchLimit(resultStart, resultSize).addFilter(filter).addParams(params);

			if (orderBy != null && !"".equals(orderBy)) {
				List<ISortToken> sortTokens = marshaller.readSortTokens(orderBy);
				builder.addSortInfo(sortTokens);
			} else {
				builder.addSortInfo(orderByCol, orderBySense);
			}

			if (filterRulesString != null && !"".equals(filterRulesString)) {
				List<IFilterRule> filterRules = marshaller.readFilterRules(filterRulesString);
				builder.addFilterRules(filterRules);
			}

			List<M> list = service.find(builder);
			long totalCount = service.count(builder);

			Map<String, Object> summaries = service.summaries(builder, list);

			IActionResultFind result = this.packfindResult(list, params, totalCount, summaries);
			stopWatch.stop();
			result.setExecutionTime(stopWatch.getTime());

			String out;

			if (dataFormat.equals(IDsMarshaller.XML)) {
				IDsMarshaller<M, F, P> resultMarshaller = service.createMarshaller(dataFormat);
				out = resultMarshaller.writeResultToString(result);
				response.setContentType("text/xml; charset=UTF-8");
			} else {
				out = marshaller.writeResultToString(result);
				response.setContentType("text/plain; charset=UTF-8");
			}

			return out;
		} catch (Exception e) {
			return this.handleManagedException(ErrorCode.DB_QUERY_ERROR, e, response);
		} finally {
			this.finishRequest();
		}
	}

	/**
	 * @param resourceName
	 * @param dataFormat
	 * @param exportId
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_DOWNLOAD_REPORT, method = { RequestMethod.GET,
			RequestMethod.POST })
	@ResponseBody
	public void downloadReport(@PathVariable String resourceName, @PathVariable String dataFormat,
			@RequestParam(value = Constants.REQUEST_PARAM_REPORT_ID, required = true) int exportId, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (logger.isInfoEnabled()) {
			logger.info("Processing request: {}.{} -> action = {} ", new Object[] { resourceName, dataFormat, Constants.DS_ACTION_REPORT });
		}

		this.prepareRequest(request, response);

		this.authorizeDsAction(resourceName, Constants.DS_ACTION_REPORT, null);

		if (dataFormat.equals(Constants.DATA_FORMAT_DOC)) {
			response.setContentType("application/msword; charset=UTF-8");
		}
		response.setHeader("Content-Disposition",
				"filename=" + resourceName.substring(resourceName.indexOf('_') + 1, resourceName.lastIndexOf('_')) + "." + dataFormat + ";");

		IReportService reportService = this.getApplicationContext().getBean("reportService", IReportService.class);
		int counter = 0;
		while (!reportService.isReady(exportId) && counter < RETRY_COUNT) {
			Thread.sleep(1000);
			counter++;
		}
		if (reportService.isReady(exportId)) {
			reportService.sendNotification(exportId, null);
			byte[] report = reportService.downloadReport(exportId);
			response.getOutputStream().write(report);
			response.getOutputStream().close();
		} else {
			throw new Exception("Report time out");
		}
	}

	/**
	 * @param resourceName
	 * @param dataFormat
	 * @param dataString
	 * @param exportInfoString
	 * @param code
	 * @param dialogReportInfo
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_DIALOG_REPORT, method = { RequestMethod.GET,
			RequestMethod.POST })
	@ResponseBody
	public void dialogReport(@PathVariable String resourceName, @PathVariable String dataFormat,

			@RequestParam(value = Constants.REQUEST_PARAM_DATA, required = false, defaultValue = "{}") String dataString,
			@RequestParam(value = Constants.REQUEST_PARAM_EXPORT_INFO, required = true, defaultValue = "") String exportInfoString,
			@RequestParam(value = Constants.REQUEST_PARAM_REPORT_CODE, required = true) String code,
			@RequestParam(value = Constants.REQUEST_DIALOG_REPORT_INFO, required = false, defaultValue = "{}") String dialogReportInfo,
			HttpServletRequest request, HttpServletResponse response)

			throws Exception {
		try {
			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat, Constants.DS_ACTION_DIALOG_REPORT });
			}

			this.prepareRequest(request, response);

			this.authorizeDsAction(resourceName, Constants.DS_ACTION_DIALOG_REPORT, null);
			IDsService<M, F, P> service = this.findDsService(resourceName);

			IDsMarshaller<M, F, P> marshaller = service.createMarshaller("json");

			DialogReportInfo info = (DialogReportInfo) marshaller.readDialogReportInfo(dialogReportInfo);

			DsDialogReport<M, F> report = (DsDialogReport<M, F>) this.getApplicationContext().getBean(Constants.SPRING_DSDIALOGREPORT_WRITER);
			report.setCode(code);
			report.setFormat(dataFormat.toUpperCase());
			service.doDialogReport(report, info);

			this.requestForReport(response, report);
		} catch (Exception e) {
			this.handleManagedException(null, e, response);
		} finally {
			this.finishRequest();
		}

	}

	/**
	 * @param resourceName
	 * @param reportName
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_DOWNLOAD_RPT_DESIGN, method = { RequestMethod.GET,
			RequestMethod.POST })
	@ResponseBody
	public void downloadRPTDesign(@PathVariable String resourceName, @PathVariable String dataFormat,
			@RequestParam(value = Constants.REQUEST_PARAM_FILE_NAME, required = false) String fileName, HttpServletRequest request,
			HttpServletResponse response)

			throws Exception {
		try {
			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat, Constants.DS_ACTION_DOWNLOAD_RPT_DESIGN });
			}

			this.prepareRequest(request, response);
			this.authorizeDsAction(resourceName, Constants.DS_ACTION_DIALOG_REPORT, null);

			String finalFileName = StringUtils.isEmpty(fileName) ? dataFormat + "." + "rptdesign" : fileName;
			response.setHeader("Content-Description", "File Transfer");
			response.setHeader("Content-Disposition", "attachment; filename=" + finalFileName + ";");
			response.setContentType("application/rptdesign; charset=UTF-8");

			IReportService reportService = this.getApplicationContext().getBean("reportService", IReportService.class);
			byte[] design = reportService.downloadRPTDesign(dataFormat);
			response.getOutputStream().write(design);
			response.getOutputStream().close();
		} catch (Exception e) {
			this.handleManagedException(null, e, response);
		} finally {
			this.finishRequest();
		}

	}

	/**
	 * @param resourceName
	 * @param dataFormat
	 * @param filterString
	 * @param filterAttrString
	 * @param filterRulesString
	 * @param paramString
	 * @param resultStart
	 * @param resultSize
	 * @param orderByCol
	 * @param orderBySense
	 * @param orderBy
	 * @param exportDownload
	 * @param exportInfoString
	 * @param exportExludeInfoString
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_REPORT, method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public void report(@PathVariable String resourceName, @PathVariable String dataFormat,
			@RequestParam(value = Constants.REQUEST_PARAM_FILTER, required = false, defaultValue = "{}") String filterString,
			@RequestParam(value = Constants.REQUEST_PARAM_FILTER_ATTRIBUTES, required = false, defaultValue = "{}") String filterAttrString,
			@RequestParam(value = Constants.REQUEST_PARAM_ADVANCED_FILTER, required = false, defaultValue = "") String filterRulesString,
			@RequestParam(value = Constants.REQUEST_PARAM_PARAMS, required = false, defaultValue = "{}") String paramString,
			@RequestParam(value = Constants.REQUEST_PARAM_START, required = false, defaultValue = DEFAULT_RESULT_START) int resultStart,
			@RequestParam(value = Constants.REQUEST_PARAM_SIZE, required = false, defaultValue = DEFAULT_RESULT_SIZE_EXPORT) int resultSize,
			@RequestParam(value = Constants.REQUEST_PARAM_SORT, required = false, defaultValue = "") String orderByCol,
			@RequestParam(value = Constants.REQUEST_PARAM_SENSE, required = false, defaultValue = "") String orderBySense,
			@RequestParam(value = Constants.REQUEST_PARAM_ORDERBY, required = false, defaultValue = "") String orderBy,
			@RequestParam(value = Constants.REQUEST_PARAM_EXPORT_DOWNLOAD, required = false, defaultValue = "true") Boolean exportDownload,
			@RequestParam(value = Constants.REQUEST_PARAM_EXPORT_INFO, required = true, defaultValue = "") String exportInfoString,
			@RequestParam(value = Constants.REQUEST_PARAM_EXCLUDE_FIELDS, required = true, defaultValue = "") String exportExludeInfoString,
			HttpServletRequest request, HttpServletResponse response)

			throws Exception {
		if (logger.isInfoEnabled()) {
			logger.info("Processing request: {}.{} -> action = {} ", new Object[] { resourceName, dataFormat, Constants.DS_ACTION_REPORT });
		}

		if (logger.isDebugEnabled()) {
			logger.debug("  --> request-filter: {} ", new Object[] { filterString });
			logger.debug("  --> request-filter-attr: {} ", new Object[] { filterAttrString });
			logger.debug("  --> request-params: {} ", new Object[] { paramString });
			logger.debug("  --> request-result-range: {} ",
					new Object[] { Integer.toString(resultStart), Integer.toString(resultStart + resultSize) });
		}

		this.prepareRequest(request, response);

		this.authorizeDsAction(resourceName, Constants.DS_ACTION_REPORT, null);

		IDsService<M, F, P> service = this.findDsService(resourceName);
		IDsMarshaller<M, F, P> marshaller = service.createMarshaller("json");

		F filter = marshaller.readFilterFromString(filterString);
		P params = marshaller.readParamsFromString(paramString);
		if (filter instanceof AbstractDsModel<?>) {
			((AbstractDsModel<F>) filter).set__filterAttrString__(filterAttrString);
		}

		IQueryBuilder<M, F, P> builder = service.createQueryBuilder().addFetchLimit(resultStart, resultSize).addFilter(filter).addParams(params);

		if (orderBy != null && !"".equals(orderBy)) {
			List<ISortToken> sortTokens = marshaller.readSortTokens(orderBy);
			builder.addSortInfo(sortTokens);
		} else {
			builder.addSortInfo(orderByCol, orderBySense);
		}

		DsDynamicReport<M, F> writer = (DsDynamicReport<M, F>) this.getApplicationContext().getBean(Constants.SPRING_DSREPORT_WRITER);
		if (filterRulesString != null && !"".equals(filterRulesString)) {
			List<IFilterRule> filterRules = marshaller.readFilterRules(filterRulesString);
			builder.addFilterRules(filterRules);
			writer.addAdvancedFilterPArams(filterRules);

		}
		IExcludeInfo excludeInfo = marshaller.readExcludeInfo(exportExludeInfoString);
		IExportInfo exportInfo = marshaller.readExportInfo(exportInfoString);
		exportInfo.prepare(service.getModelClass());

		writer.setReportFormat(dataFormat.toUpperCase());
		writer.addFilterParams(filter, excludeInfo);
		service.doReport(builder, writer, exportInfo, Session.user.get());
	}

	/**
	 * Default handler for export action.
	 *
	 * @param resourceName
	 * @param dataFormat
	 * @param filterString
	 * @param filterRulesString
	 * @param paramString
	 * @param resultStart
	 * @param resultSize
	 * @param orderByCol
	 * @param orderBySense
	 * @param orderBy
	 * @param exportDownload
	 * @param exportInfoString
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_EXPORT, method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public String export(@PathVariable String resourceName, @PathVariable String dataFormat,
			@RequestParam(value = Constants.REQUEST_PARAM_FILTER, required = false, defaultValue = "{}") String filterString,
			@RequestParam(value = Constants.REQUEST_PARAM_ADVANCED_FILTER, required = false, defaultValue = "") String filterRulesString,
			@RequestParam(value = Constants.REQUEST_PARAM_PARAMS, required = false, defaultValue = "{}") String paramString,
			@RequestParam(value = Constants.REQUEST_PARAM_START, required = false, defaultValue = DEFAULT_RESULT_START) int resultStart,
			@RequestParam(value = Constants.REQUEST_PARAM_SIZE, required = false, defaultValue = DEFAULT_RESULT_SIZE_EXPORT) int resultSize,
			@RequestParam(value = Constants.REQUEST_PARAM_SORT, required = false, defaultValue = "") String orderByCol,
			@RequestParam(value = Constants.REQUEST_PARAM_SENSE, required = false, defaultValue = "") String orderBySense,
			@RequestParam(value = Constants.REQUEST_PARAM_ORDERBY, required = false, defaultValue = "") String orderBy,
			@RequestParam(value = Constants.REQUEST_PARAM_EXPORT_DOWNLOAD, required = false, defaultValue = "true") Boolean exportDownload,
			@RequestParam(value = Constants.REQUEST_PARAM_EXPORT_INFO, required = true, defaultValue = "") String exportInfoString,
			HttpServletRequest request, HttpServletResponse response)

			throws Exception {
		try {
			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ", new Object[] { resourceName, dataFormat, Constants.DS_ACTION_EXPORT });
			}

			if (logger.isDebugEnabled()) {
				logger.debug("  --> request-filter: {} ", new Object[] { filterString });
				logger.debug("  --> request-params: {} ", new Object[] { paramString });
				logger.debug("  --> request-result-range: {} ",
						new Object[] { Integer.toString(resultStart), Integer.toString(resultStart + resultSize) });
			}

			this.prepareRequest(request, response);

			this.authorizeDsAction(resourceName, Constants.DS_ACTION_EXPORT, null);

			IDsService<M, F, P> service = this.findDsService(resourceName);
			IDsMarshaller<M, F, P> marshaller = service.createMarshaller("json");

			F filter = marshaller.readFilterFromString(filterString);
			P params = marshaller.readParamsFromString(paramString);

			IQueryBuilder<M, F, P> builder = service.createQueryBuilder().addFetchLimit(resultStart, resultSize).addFilter(filter).addParams(params);

			if (orderBy != null && !"".equals(orderBy)) {
				List<ISortToken> sortTokens = marshaller.readSortTokens(orderBy);
				builder.addSortInfo(sortTokens);
			} else {
				builder.addSortInfo(orderByCol, orderBySense);
			}

			if (filterRulesString != null && !"".equals(filterRulesString)) {
				List<IFilterRule> filterRules = marshaller.readFilterRules(filterRulesString);
				builder.addFilterRules(filterRules);
			}

			IDsExport<M> writer = service.createExporter(dataFormat);

			if (dataFormat.equals(Constants.DATA_FORMAT_CSV)) {
				writer = (IDsExport<M>) this.getApplicationContext().getBean(Constants.SPRING_DSEXPORT_WRITER_CSV);
			}
			if (dataFormat.equals(Constants.DATA_FORMAT_JSON)) {
				writer = (IDsExport<M>) this.getApplicationContext().getBean(Constants.SPRING_DSEXPORT_WRITER_JSON);
			}
			if (dataFormat.equals(Constants.DATA_FORMAT_XML)) {
				writer = (IDsExport<M>) this.getApplicationContext().getBean(Constants.SPRING_DSEXPORT_WRITER_XML);
			}
			if (dataFormat.equals(Constants.DATA_FORMAT_PDF)) {
				writer = (IDsExport<M>) this.getApplicationContext().getBean(Constants.SPRING_DSEXPORT_WRITER_XML);
			}
			if (dataFormat.equals(Constants.DATA_FORMAT_HTML)) {
				writer = (IDsExport<M>) this.getApplicationContext().getBean(Constants.SPRING_DSEXPORT_WRITER_HTML);
				Map<String, Object> properties = new HashMap<>();
				properties.put("cssUrl", this.getSettings().getParam(SysParam.CORE_EXP_HTML_CSS.name()));
				writer.setProperties(properties);
			}

			if (writer == null) {
				throw new Exception("Invalid data-format " + dataFormat);
			}

			IExportInfo exportInfo = marshaller.readExportInfo(exportInfoString);
			exportInfo.prepare(service.getModelClass());

			writer.setExportInfo(exportInfo);

			writer.setOutFilePath(Session.user.get().getWorkspace().getTempPath());
			service.doExport(builder, writer);

			if (exportDownload) {
				// get the file content
				if (dataFormat.equals(Constants.DATA_FORMAT_CSV)) {
					response.setContentType("application/vnd.ms-excel; charset=UTF-8");
				}
				if (dataFormat.equals(Constants.DATA_FORMAT_JSON)) {
					response.setContentType("text/plain; charset=UTF-8");
				}
				if (dataFormat.equals(Constants.DATA_FORMAT_HTML)) {
					response.setContentType("text/html; charset=UTF-8");
				}
				if (dataFormat.equals(Constants.DATA_FORMAT_XML)) {
					response.setContentType("text/xml; charset=UTF-8");
				}
				response.setHeader("Content-Description", "File Transfer");
				response.setHeader("Content-Disposition",
						"inline; filename=\"" + service.getModelClass().getSimpleName() + "." + dataFormat.toLowerCase() + "\";");

				this.sendFile(writer.getOutFile(), response.getOutputStream());
				return null;
			} else {
				// just send the file name
				return "{success:true, file:\"" + writer.getOutFile().getName() + "\"}";
			}
		} catch (Exception e) {
			throw e;
		} finally {
			this.finishRequest();
		}
	}

	/**
	 * Returns information about the given resource ( data-source )
	 *
	 * @param resourceName
	 * @param dataFormat
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_INFO)
	public String info(@PathVariable String resourceName, @PathVariable String dataFormat, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ", new Object[] { resourceName, dataFormat, Constants.DS_ACTION_INFO });
			}

			this.prepareRequest(request, response);

			String out;
			IDsDefinition def = null;

			if (this.getSettings().get(Constants.PROP_DEPLOYMENT).equals(Constants.PROP_DEPLOYMENT_JEE)) {
				IDsDefinitions defs = this.getApplicationContext().getBean(IDsDefinitions.class);
				if (defs.containsDs(resourceName)) {
					def = defs.getDsDefinition(resourceName);
				}
			} else {
				@SuppressWarnings("unchecked")
				List<IDsDefinitions> defsList = (List<IDsDefinitions>) this.getApplicationContext().getBean("osgiDsDefinitions");
				for (IDsDefinitions defs : defsList) {
					if (defs.containsDs(resourceName)) {
						def = defs.getDsDefinition(resourceName);
					}
				}
			}

			if (def == null) {
				throw new Exception("Ds resource is not defined! ResourceName: " + resourceName);
			}
			def.getModelFields();
			def.getFilterFields();
			def.getParamFields();
			IDsService<M, F, P> service = this.findDsService(resourceName);

			if (dataFormat.equals(IDsMarshaller.JSON)) {

				IDsMarshaller<M, F, P> marshaller = service.createMarshaller(dataFormat);
				response.setContentType("text/plain; charset=UTF-8");

				out = ((ObjectMapper) marshaller.getDelegate()).writeValueAsString(def);
				PrintWriter w = response.getWriter();
				w.write(out);
				w.flush();
				return null;
			} else if (dataFormat.equals(IDsMarshaller.XML)) {

				// IDsMarshaller<M, F, P> marshaller = service
				// .createMarshaller(dataFormat);
				// StringWriter writer = new StringWriter();
				//
				//
				// ((XmlMarshaller<M, F, P>)
				// marshaller).createMarshaller(
				// def.getClass()).marshal(def, writer);
				//
				//
				// response.setContentType("text/xml; charset=UTF-8");
				// out = writer.toString();
				// PrintWriter w = response.getWriter();
				// w.write(out);
				// w.flush();
				// return null;
			} else if ("html".equals(dataFormat)) {

				IDsMarshaller<M, F, P> marshaller = service.createMarshaller(IDsMarshaller.XML);

				StringWriter writer = new StringWriter();
				((XmlMarshaller<M, F, P>) marshaller).createMarshaller(def.getClass()).marshal(def, writer);
				out = writer.toString();
				String t1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
				String t2 = "<?xml-stylesheet type=\"text/xsl\" href=\"/seava/resources/xsl/ds-info.xsl\"?>";
				out = out.replace(t1, t1 + '\n' + t2);
				response.setContentType("text/xml; charset=UTF-8");

				PrintWriter w = response.getWriter();
				w.write(out);
				w.flush();
				return null;
			}

			throw new Exception("Data-source " + resourceName + " cannot be found.");

		} catch (Exception e) {
			return this.handleException(e, response);
		} finally {
			this.finishRequest();
		}
	}

	/**
	 * @param resourceName
	 * @param dataFormat
	 * @param filterString
	 * @param filterRulesString
	 * @param paramString
	 * @param resultStart
	 * @param resultSize
	 * @param orderByCol
	 * @param orderBySense
	 * @param orderBy
	 * @param exportDownload
	 * @param exportInfoString
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params = Constants.REQUEST_PARAM_ACTION + "=" + Constants.DS_ACTION_PRINT)
	@ResponseBody
	public String print(@PathVariable String resourceName, @PathVariable String dataFormat,
			@RequestParam(value = Constants.REQUEST_PARAM_FILTER, required = false, defaultValue = "{}") String filterString,
			@RequestParam(value = Constants.REQUEST_PARAM_ADVANCED_FILTER, required = false, defaultValue = "") String filterRulesString,
			@RequestParam(value = Constants.REQUEST_PARAM_PARAMS, required = false, defaultValue = "{}") String paramString,
			@RequestParam(value = Constants.REQUEST_PARAM_START, required = false, defaultValue = DEFAULT_RESULT_START) int resultStart,
			@RequestParam(value = Constants.REQUEST_PARAM_SIZE, required = false, defaultValue = DEFAULT_RESULT_SIZE) int resultSize,
			@RequestParam(value = Constants.REQUEST_PARAM_SORT, required = false, defaultValue = "") String orderByCol,
			@RequestParam(value = Constants.REQUEST_PARAM_SENSE, required = false, defaultValue = "") String orderBySense,
			@RequestParam(value = Constants.REQUEST_PARAM_ORDERBY, required = false, defaultValue = "") String orderBy,
			@RequestParam(value = Constants.REQUEST_PARAM_EXPORT_DOWNLOAD, required = false, defaultValue = "true") Boolean exportDownload,
			@RequestParam(value = Constants.REQUEST_PARAM_EXPORT_INFO, required = true, defaultValue = "") String exportInfoString,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ", new Object[] { resourceName, dataFormat, Constants.DS_ACTION_PRINT });
			}

			if (logger.isDebugEnabled()) {

				logger.debug("  --> request-filter: {} ", new Object[] { filterString });
				logger.debug("  --> request-params: {} ", new Object[] { paramString });
				logger.debug("  --> request-result-range: {} ",
						new Object[] { Integer.toString(resultStart), Integer.toString(resultStart + resultSize) });
			}

			this.prepareRequest(request, response);

			this.authorizeDsAction(resourceName, Constants.DS_ACTION_EXPORT, null);

			IDsService<M, F, P> service = this.findDsService(resourceName);

			IDsMarshaller<M, F, P> marshaller = service.createMarshaller("json");

			F filter = marshaller.readFilterFromString(filterString);
			P params = marshaller.readParamsFromString(paramString);

			ExportInfo exportInfo = marshaller.readDataFromString(exportInfoString, ExportInfo.class);
			exportInfo.prepare(service.getModelClass());

			IQueryBuilder<M, F, P> builder = service.createQueryBuilder().addFetchLimit(resultStart, resultSize).addFilter(filter).addParams(params);

			if (orderBy != null && !"".equals(orderBy)) {
				List<ISortToken> sortTokens = marshaller.readSortTokens(orderBy);
				builder.addSortInfo(sortTokens);
			} else {
				builder.addSortInfo(orderByCol, orderBySense);
			}

			if (filterRulesString != null && !"".equals(filterRulesString)) {
				List<IFilterRule> filterRules = marshaller.readFilterRules(filterRulesString);
				builder.addFilterRules(filterRules);
			}

			List<M> data = service.find(builder);

			File _tplDir;
			String _tplName;

			String _tpl = this.getSettings().getParam(SysParam.CORE_PRINT_HTML_TPL.name());

			if (_tpl == null || "".equals(_tpl)) {
				_tpl = "print-template/print.ftl";
			}

			_tpl = Session.user.get().getWorkspace().getWorkspacePath() + "/" + _tpl;
			File _tplFile = new File(_tpl);

			_tplDir = _tplFile.getParentFile();
			_tplName = _tplFile.getName();

			if (!_tplFile.exists()) {

				if (!_tplDir.exists()) {
					_tplDir.mkdirs();
				}

				Resource resource = new ClassPathResource("seava/j4e/web/ftl/data/print.ftl");
				FileUtils.copyInputStreamToFile(resource.getInputStream(), _tplFile);
			}

			Configuration cfg = new Configuration();
			cfg.setObjectWrapper(ObjectWrapper.DEFAULT_WRAPPER);
			cfg.setDirectoryForTemplateLoading(_tplDir);

			Map<String, Object> root = new HashMap<>();

			root.put("printer", new ModelPrinter());
			root.put(DATA, data);
			root.put("filter", filter);
			root.put("params", params);
			root.put("client", Session.user.get().getClient());

			Map<String, Object> reportConfig = new HashMap<>();
			reportConfig.put("logo", this.getSettings().getParam(SysParam.CORE_LOGO_URL_REPORT.name()));
			reportConfig.put("runBy", Session.user.get().getName());
			reportConfig.put("runAt", new Date());
			reportConfig.put("title", exportInfo.getTitle());
			reportConfig.put("orientation", exportInfo.getLayout());
			reportConfig.put("columns", exportInfo.getColumns());
			reportConfig.put("filter", exportInfo.getFilter());

			root.put("cfg", reportConfig);

			if (dataFormat.equals(Constants.DATA_FORMAT_HTML)) {
				response.setContentType("text/html; charset=UTF-8");
			}

			Template temp = cfg.getTemplate(_tplName);

			if (exportDownload) {
				Writer out = new OutputStreamWriter(response.getOutputStream(), response.getCharacterEncoding());
				temp.process(root, out);
				out.flush();
				return null;
			} else {
				String path = Session.user.get().getWorkspace().getTempPath();
				String fileName = UUID.randomUUID().toString();
				File outFile = new File(path + "/" + fileName + "." + dataFormat);

				FileOutputStream fos = new FileOutputStream(outFile);
				try {
					Writer out = new OutputStreamWriter(fos, "UTF-8");
					try {
						temp.process(root, out);
						out.flush();
					} finally {
						out.close();
					}
				} finally {
					fos.close();
				}
				// just send the file name
				return "{success:true, file:\"" + outFile.getName() + "\"}";
			}

		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug(e.getMessage(), e);
			}
			this.handleException(e, response);
			return null;
		} finally {
			this.finishRequest();
		}

	}

	/**
	 * @param data
	 * @param params
	 * @param totalCount
	 * @param summaries
	 * @return
	 */
	public IActionResultFind packfindResult(List<M> data, P params, long totalCount, Map<String, Object> summaries) {
		IActionResultFind pack = new ActionResultFind();
		pack.setData(data);
		pack.setParams(params);
		pack.setTotalCount(totalCount);
		pack.setSummaries(summaries);
		return pack;
	}

	private HttpClient getHttpClient() {
		if (this.httpClient == null) {
			this.httpClient = HttpClientBuilder.create().useSystemProperties().build();
		}
		return this.httpClient;
	}

	private void requestForReport(HttpServletResponse response, IDsReport<M, F> report) throws BusinessException, IllegalStateException, IOException {
		if (logger.isDebugEnabled()) {
			logger.debug("  --> report request data: {} ", new Object[] { report.toXmlString() });
		}
		JSONObject entityJson = new JSONObject();
		entityJson.put(DATA, report.toXmlString());
		entityJson.put(DEV_MODE, true);
		entityJson.put(REPO_ID, this.getSettings().get(Constants.PROP_REPORT_CENTER_REPO));

		String reportCenterUrl = this.getSettings().getReportCenterUrl();
		String url = reportCenterUrl + "runReport";
		InputStream is = this.makePost(entityJson, url);
		@SuppressWarnings("resource")
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
		String responseStr = s.hasNext() ? s.next() : "";
		JSONObject responseJson = new JSONObject(responseStr);
		logger.info(responseJson.toString());
		response.getOutputStream().print(responseJson.getInt("id"));
		response.getOutputStream().flush();
	}

	private InputStream makePost(JSONObject entityJson, String url) throws BusinessException, IOException {
		HttpPost post = new HttpPost(url);
		post.setEntity(new StringEntity(entityJson.toString(), ContentType.APPLICATION_JSON));

		HttpResponse reportResponse = null;
		int retry = 0;
		do {
			try {
				reportResponse = this.getHttpClient().execute(post);
			} catch (IOException e) {
				if (retry >= RETRY_COUNT) {
					throw new BusinessException(ErrorCode.COMMUNICATION_ERROR, e);
				}
				retry++;
				logger.warn("Communication error with report server. Retry: " + retry, e);
			}
		} while (reportResponse == null);
		return reportResponse.getEntity().getContent();
	}

	/**
	 * Copy date from input to output.
	 *
	 * @param input
	 * @param output
	 * @return
	 * @throws IOException
	 */
	private long stream(InputStream input, OutputStream output) throws IOException {
		ReadableByteChannel inputChannel = null;
		WritableByteChannel outputChannel = null;

		try {
			inputChannel = Channels.newChannel(input);
			outputChannel = Channels.newChannel(output);
			ByteBuffer buffer = ByteBuffer.allocate(10240);
			long size = 0;

			while (inputChannel.read(buffer) != -1) {
				buffer.flip();
				size += outputChannel.write(buffer);
				buffer.clear();
			}

			return size;
		} finally {
			if (outputChannel != null) {
				try {
					outputChannel.close();
				} catch (IOException ignore) {
					if (logger.isDebugEnabled()) {
						logger.debug(ignore.getMessage(), ignore);
					}
				}
			}
			if (inputChannel != null) {
				try {
					inputChannel.close();
				} catch (IOException ignore) {
					if (logger.isDebugEnabled()) {
						logger.debug(ignore.getMessage(), ignore);
					}
				}
			}
		}
	}
}
