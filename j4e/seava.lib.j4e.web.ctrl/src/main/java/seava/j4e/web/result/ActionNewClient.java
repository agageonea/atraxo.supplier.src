package seava.j4e.web.result;

import java.util.Date;

public class ActionNewClient {

	private Date startTime;
	private String status;
	private String clientCode;
	private String clientName;
	private String adminUserCode;
	private String adminUserName;
	private String description;

	public ActionNewClient(Date startTime, String status, String clientCode, String clientName, String adminUserCode, String adminUserName,
			String description) {
		super();
		this.startTime = startTime;
		this.status = status;
		this.clientCode = clientCode;
		this.clientName = clientName;
		this.adminUserCode = adminUserCode;
		this.adminUserName = adminUserName;
		this.description = description;
	}

	public ActionNewClient() {

	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getClientCode() {
		return this.clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getClientName() {
		return this.clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getAdminUserCode() {
		return this.adminUserCode;
	}

	public void setAdminUserCode(String adminUserCode) {
		this.adminUserCode = adminUserCode;
	}

	public String getAdminUserName() {
		return this.adminUserName;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}
}
