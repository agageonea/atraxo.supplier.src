/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.web.controller.ui.extjs;

import java.util.ArrayList;
import java.util.List;

/**
 * Dependencies
 */
public class Dependencies {

	public static final String TYPE_DS = "ds";
	public static final String TYPE_DC = "dc";
	public static final String TYPE_DC_CUST = "dc_cust";
	public static final String TYPE_LOV = "lov";
	public static final String TYPE_ASGN = "asgn";
	public static final String TYPE_UI = "frame";
	public static final String TYPE_UI_CUST = "frame_cust";

	private List<String> ds;
	private List<String> dc;
	private List<String> lov;
	private List<String> asgn;
	private List<String> uiCust;
	private List<String> dcCust;

	/**
	 * @param cmp
	 */
	public void addDs(String cmp) {
		if (this.ds == null) {
			this.ds = new ArrayList<>();
		}
		if (!this.ds.contains(cmp)) {
			this.ds.add(cmp);
		}
	}

	/**
	 * @param cmp
	 */
	public void addDc(String cmp) {
		if (this.dc == null) {
			this.dc = new ArrayList<>();
		}
		if (!this.dc.contains(cmp)) {
			this.dc.add(cmp);
		}
	}

	/**
	 * @param cmp
	 */
	public void addLov(String cmp) {
		if (this.lov == null) {
			this.lov = new ArrayList<>();
		}
		if (!this.lov.contains(cmp)) {
			this.lov.add(cmp);
		}
	}

	/**
	 * @param cmp
	 */
	public void addAsgn(String cmp) {
		if (this.asgn == null) {
			this.asgn = new ArrayList<>();
		}
		if (!this.asgn.contains(cmp)) {
			this.asgn.add(cmp);
		}
	}

	/**
	 * @param cmp
	 */
	public void addUiCust(String cmp) {
		if (this.uiCust == null) {
			this.uiCust = new ArrayList<>();
		}
		if (!this.uiCust.contains(cmp)) {
			this.uiCust.add(cmp);
		}
	}

	/**
	 * @param cmp
	 */
	public void addDcCust(String cmp) {
		if (this.dcCust == null) {
			this.dcCust = new ArrayList<>();
		}
		if (!this.dcCust.contains(cmp)) {
			this.dcCust.add(cmp);
		}
	}

	public List<String> getDs() {
		return this.ds;
	}

	public void setDs(List<String> ds) {
		this.ds = ds;
	}

	public List<String> getDc() {
		return this.dc;
	}

	public void setDc(List<String> dc) {
		this.dc = dc;
	}

	public List<String> getLov() {
		return this.lov;
	}

	public void setLov(List<String> lov) {
		this.lov = lov;
	}

	public List<String> getAsgn() {
		return this.asgn;
	}

	public void setAsgn(List<String> asgn) {
		this.asgn = asgn;
	}

	public List<String> getUiCust() {
		return this.uiCust;
	}

	public void setUi(List<String> uiCust) {
		this.uiCust = uiCust;
	}

	public List<String> getDcCust() {
		return this.dcCust;
	}

	public void setDcCust(List<String> dcCust) {
		this.dcCust = dcCust;
	}
}
