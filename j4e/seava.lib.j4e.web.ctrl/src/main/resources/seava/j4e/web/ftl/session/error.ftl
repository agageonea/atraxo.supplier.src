<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>Sign-in: ${productName} | ${productVersion}</title>
	<link rel="stylesheet" type="text/css" href="${loginPageCss}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>
<body onload="javascript:onDocumentReady();">
	<br />
	<br />
	<br />
	<br />
	<br />
	<table align=center width=450 style='vertical-align: middle; align: center'>
		<tr>
			<td colspan=2 align=center style='font-family: arial, verdana; '>
			${productName} | ${productVersion}
			</td>
		</tr>
		<tr>
			<td style='border-top: 1px solid gray;' colspan=2>&nbsp;</td>
		</tr>
		<tr>
			<td colspan=2 style='padding: 10px;'>
				<table width='100%'>
					<tr>
						<td align=center class="text"><IMG
							style="border: 0px solid #ccc;" src="${loginPageLogo}"></td>
						<td
							style='padding-left: 20px; font-family: arial, verdana; font-size: 16px; color: red; font-weight: bold;'>
							${msg}
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<#if error??>
			<tr>
				<td colspan=2 style="text-align: center; color: red;">&nbsp; ${error} &nbsp;</td>
			</tr>
		</#if> 		
		<tr>
			<td
				style='border-top: 1px solid gray; font-family: arial, verdana; font-size: 11px;'
				colspan=2 align=center><br> &copy; ${currentYear}	${productVendor}.</td>
		</tr>
	</table>
</body>
</html>