
	<!-- Extjs  -->
	
	<script type="text/javascript" src="${urlUiExtjsLib}/js/ext-all.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/extjs-core-extend.js?version=${productVersion}"></script>
	
	<!-- jQuery  -->
	<script type="text/javascript" src="${urlUiExtjsLib}/js/jquery-1.11.3.min.js?version=${productVersion}"></script> 
	<script type="text/javascript" src="${urlUiExtjsLib}/js/jquery.sparkline.min.js?version=${productVersion}"></script>
	
	<!-- Locale -->
	
	<#if shortLanguage != "sys">
	<script type="text/javascript" src="${urlUiExtjsCoreI18n}/sys/e4e.js?version=${productVersion}"></script>
	<#if shortLanguage != "en">
	<script type="text/javascript" src="${urlUiExtjsCoreI18n}/en/e4e.js?version=${productVersion}"></script>
	</#if>
	</#if>
	<script type="text/javascript"
		src="${urlUiExtjsCoreI18n}/${shortLanguage}/extjs.js?version=${productVersion}"></script>
	<script type="text/javascript"
		src="${urlUiExtjsCoreI18n}/${shortLanguage}/e4e.js?version=${productVersion}"></script>

	<!-- Sencha charts -->

	<script type="text/javascript" src="${urlUiExtjsLib}/js/sencha-charts.js?version=${productVersion}"></script>
	
	<!-- Framework -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e-all.js?version=${productVersion}"></script>
