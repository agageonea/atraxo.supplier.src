<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">

<#if sysCfg_workingMode == "prod">
<html>
</#if>
<#if sysCfg_workingMode != "prod">
<html manifest="${ctxpath}/devcache_frame.appcache">
</#if>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" > 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
	<meta http-equiv="Cache-Control" content="no-cache" >
	<meta http-equiv="pragma" content="no-cache" >
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>${item} | ${productName} </title>
	<script type="text/javascript">
		__ITEM__ = "${item}";
		__LANGUAGE__ = "${shortLanguage}";
		__THEME__ = "${theme}";
	</script>
	<script type="text/javascript">${constantsJsFragment}</script>

	<!-- Theme -->	
	
	<link rel="stylesheet" type="text/css"
		href="${ctxpath}/statics/button-icons/ext-theme-clarity/style.css?version=${productVersion}">	
	<link rel="stylesheet" type="text/css"
		href="${urlUiExtjsThemes}/ext-theme-clarity/ext-theme-clarity-all.css?version=${productVersion}">
	<link rel="stylesheet" type="text/css"
		href="${ctxpath}/statics/custom-css/${theme}.css?version=${productVersion}">
	<link rel="stylesheet" type="text/css"
		href="${ctxpath}/statics/custom-css/charts.css?version=${productVersion}">	
	<link rel="stylesheet" type="text/css"
		href="${ctxpath}/statics/custom-css/all.css?version=${productVersion}">
		
</head>
<body>

	<#include "_loading_mask.ftl">

	<script type="text/javascript">
		if (document && document.getElementById("n21-loading-msg")) {
			document.getElementById("n21-loading-msg").innerHTML = "...";
		}
	</script>

	<#if sysCfg_workingMode == "dev">
		<#include "_includes_dev.ftl">
	</#if>
    <#if sysCfg_workingMode == "prod">
		<#include "_includes_prod.ftl">
	</#if>

    <#include "_dnet_params.ftl">

    <#if sysCfg_workingMode == "dev">
		<#list frameDependenciesTrl as _include>
			   <script type="text/javascript" src="${_include}?version=${productVersion}"></script>
		</#list>
		<#list frameDependenciesCmp as _include>
			   <script type="text/javascript" src="${_include}?version=${productVersion}"></script>
		</#list>
	</#if>
    <#if sysCfg_workingMode == "prod">
		<#if shortLanguage != "sys">
		<script type="text/javascript" src="${ctxpath}/ui-extjs/frame/${bundle}/sys/${item}.js?version=${productVersion}"></script>
		<#if shortLanguage != "en">
		<script type="text/javascript" src="${ctxpath}/ui-extjs/frame/${bundle}/en/${item}.js?version=${productVersion}"></script>
		</#if>
		</#if>
		<script type="text/javascript" src="${ctxpath}/ui-extjs/frame/${bundle}/${shortLanguage}/${item}.js?version=${productVersion}"></script>
		<script type="text/javascript" src="${ctxpath}/ui-extjs/frame/${bundle}/${item}.js?version=${productVersion}"></script>
	</#if>

	${extensions}

	<script type="text/javascript">
		if (document && document.getElementById("n21-loading-msg")) {
			document.getElementById("n21-loading-msg").innerHTML = Main
					.translate("msg", "initialize") + "...";
					/* + " ${itemSimpleName}..."; */
		}
	</script>

	<script>
		var theFrameInstance = null;
		var __dialogInitializing__ = null;
		var __dialogDestroying__ = null;
		var __theViewport__ = null;
		Ext.onReady(function() {
			if (getApplication().getSession().useFocusManager) {
				Ext.FocusManager.enable(true);
			}
		<#include "_on_ready.ftl">
		var frameReports = [];

			${extensionsContent}

			var cfg = {
				layout : "fit",
				xtype : "container",
				items : [ {
					xtype : "${itemSimpleName}",
					border : false,
					_reports_ : frameReports,
					listeners : {
						afterrender : {
							fn : function(p) {
								theFrameInstance = this;
							}
			    		}
					}
				} ]
			};
			__theViewport__ = new Ext.Viewport(cfg);

			var map = KeyBindings.createFrameKeyMap(theFrameInstance);

		});

	<#include "_loading_mask_remove.ftl">
	

	</script>
</body>
</html>