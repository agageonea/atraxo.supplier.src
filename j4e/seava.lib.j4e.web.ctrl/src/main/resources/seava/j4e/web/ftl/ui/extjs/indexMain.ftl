<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
<#if sysCfg_workingMode != "prod">
<html>
</#if>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Cache-Control" content="no-cache" >
	<meta http-equiv="pragma" content="no-cache" >
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>${productName} | ${productVersion}</title>
	<link rel="shortcut icon" href="${ctxpath}/statics/custom-resources/favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<script type="text/javascript">
		${constantsJsFragment}
	</script>

	<!-- Theme /-->	
	
	<link rel="stylesheet" type="text/css"
		href="${ctxpath}/statics/button-icons/ext-theme-clarity/style.css?version=${productVersion}">
	<link rel="stylesheet" type="text/css"
		href="${urlUiExtjsThemes}/ext-theme-clarity/ext-theme-clarity-all.css?version=${productVersion}">
	<link rel="stylesheet" type="text/css"
		href="${ctxpath}/statics/custom-css/all.css?version=${productVersion}">
	<link rel="stylesheet" type="text/css"
		href="${ctxpath}/statics/custom-css/${theme}.css?version=${productVersion}">
	<link rel="stylesheet" type="text/css"
		href="${ctxpath}/statics/custom-css/charts.css?version=${productVersion}">
		
	<style type="text/css">
		.sone-logo-container {
			background: url("${ctxpath}/statics/resources/images/applogo.png") no-repeat center 15px !important;
		}
	</style>
	
	<#include "_loading_mask.ftl">

	<#if sysCfg_workingMode == "dev">
		<#include "_includes_dev.ftl">
	</#if>
    <#if sysCfg_workingMode == "prod">
		<#include "_includes_prod.ftl">
	</#if>

    <#include "_dnet_params.ftl">

	${extensions}
	
			   <script type="text/javascript" src="${ctxpath}/statics/sys/atraxo/ad/i18n/ds/Client_Ds.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/sys/atraxo/ad/i18n/ds/ClientLov_Ds.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/sys/atraxo/ad/i18n/dc/Client_Dc.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/sys/atraxo/ad/i18n/frame/Client_Ui.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/atraxo/ad/ui/extjs/ds/Client_Ds.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/atraxo/ad/ui/extjs/ds/ClientLov_Ds.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/atraxo/ad/ui/extjs/lov/Clients_Lov.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/atraxo/ad/ui/extjs/dc/Client_Dc.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/atraxo/ad/ui/extjs/frame/Client_Ui.js?version=${productVersion}"></script>
			   
			   <script type="text/javascript" src="${ctxpath}/statics/sys/atraxo/ad/i18n/ds/DataSource_Ds.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/sys/atraxo/ad/i18n/dc/DataSource_Dc.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/atraxo/ad/ui/extjs/ds/DataSource_Ds.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/atraxo/ad/ui/extjs/dc/DataSource_Dc.js?version=${productVersion}"></script>
			   
			   <script type="text/javascript" src="${ctxpath}/statics/atraxo/fmbas/ui/extjs/ds/MeasurmentUnitsVolumeLov_Ds.js?version=${productVersion}"></script>
			   <script type="text/javascript" src="${ctxpath}/statics/atraxo/fmbas/ui/extjs/lov/MeasurmentUnitsVolumeLov_Lov.js?version=${productVersion}"></script>
		
</head>
<body>

	<script language="javascript" type"text/javascript">
    Ext.onReady(function(){

		<#include "_on_ready.ftl">

		e4e.base.Session.auth = "${sysAuthentication}";

		e4e.base.Session.user.code = "${user.code}";
		e4e.base.Session.user.loginName = "${user.loginName}";
		e4e.base.Session.user.name = "${user.name}";
        e4e.base.Session.user.systemUser = ${user.systemUser?string};

        <#if ((user.client??) && (user.client.id??)) >
			e4e.base.Session.client.id = "${user.client.id}";
			e4e.base.Session.client.code = "${user.client.code}";
			e4e.base.Session.client.name = "${user.client.name}";
		</#if>
		e4e.base.Session.locked = false;
		e4e.base.Session.roles = [${userRolesStr}];

		${extensionsContent}
        
      	var tr = e4e.base.TemplateRepository;
      	
      	var margin = 0;
      	
      	if (typeof(_SYSTEMPARAMETERS_) != "undefined" && (_SYSTEMPARAMETERS_.docked_menu == "true" || _SYSTEMPARAMETERS_.docked_menu == "TRUE")) {
      		margin = "0 0 0 50";
      	}
		
		__application__ = e4e.base.Application;
		__application__.menu = new e4e.base.${applicationMenu}({ region:"north" });
		__application__.view = new Ext.Viewport({
			layout:"border",
		    forceLayout:true,
		    items: [{
		    	region: "center",
		    	xtype: "container",
		    	layout: "border",
		    	id:"dnet-application-view",
		    	margin: margin, // Docked menu
		    	items:[{
					region:"center",
				   	xtype:"tabpanel",
					deferredRender:false,
					activeTab:0,
					plain : true,
					cls: "dnet-home",
					_taskCounter_ :0,
					plugins: Ext.create("Ext.ux.TabCloseMenu", {}),
			   		id:"dnet-application-view-body",
			   		listeners: {
			   			tabchange: {
			    			scope: this,
			    			fn: function() {
			    			
			    				// =================================================
			    				// Dan: update the application header and glyph icon
			    				// =================================================
			    				
			    				var app = getApplication();
			    				var menu = app.menu;
			    				
			    				if (app.getActiveTab().getId() != "dnet-application-view-home") {
			    					var frame = app.getActiveFrameInstance();
			    					
			    					if (!Ext.isEmpty(frame)) {
			    						menu.fireEvent("setupHeader",frame._frameParameters_, frame._kpiParameters_);
			    						frame.fireEvent("tabChanged");
			    					}
			    				}
			    				else {
			    					var homeParams = {
			    						glyph: "xf015@FontAwesome",
			    						title: Main.translate("appmenuitem", "home__lbl"),
			    						frame: null
			    					}
			    					menu.fireEvent("setupHeader",homeParams);
			    				}
			    			}
			    		},
			    		beforeremove: {
			    			scope: this,
			    			fn: function(tabPanel,cmp,eOpts){
			    				var app = getApplication();
			    				var frame = app.navigator.getFrameInstance(cmp.initialConfig.fqn)
		    					if (!Ext.isEmpty(frame)) {
		    						if(frame.isInitializing()){
		    							return false;
		    						} else {
		    							frame.setDestroying(true);
		    						}
		    					}
			    			}
			    		},
			    		viewPortReady: {
			    			scope: this,
			    			fn: function(panel) {
			    				var items = {
			    					xtype:"dnetHomePanel",
									id:"dnet-application-view-home"
			    				}
			    				<#include "_loading_mask_remove.ftl">
		    					panel.add(items);
		    					panel.setActiveTab(0);
		    					__application__.run();
			    			}
			    		}
			   		}}
			    ], 
			    listeners: {
			    	afterrender: {
		    			scope: this,
		    			fn: function(viewPort) {
		    				var f = function(viewPort) {
		    					var items = __application__.menu;
		    					var bodyPanel = Ext.getCmp("dnet-application-view-body");
		    					viewPort.add(items);
		    					bodyPanel.fireEvent("viewPortReady", bodyPanel);
		    					
		    					var f2 = function(vp) {
		    						vp.resetItemMargins();
		    						vp.updateLayout();
		    					};
		    					Ext.defer(f2, 1500, this, [viewPort]);
		    				}
		    				Ext.defer(f, 250, this, [viewPort]);
		    			}
		    		}
			    }
		    }]
      	});
      	
		
		var map = KeyBindings.createMainKeyMap();

    });    

    var confirm_leave = true;
    var leave_message = "Are you sure you want to leave?"
    function _leave_page(e) {
            if( confirm_leave ===true)
            {
                if(!e) e = window.event;
                e.cancelBubble = true;
                e.returnValue = leave_message;
                if (e.stopPropagation) 
                {
                    e.stopPropagation();
                    e.preventDefault();
                }
                return leave_message;
            }
        } 
        
    function resizeReset() {
    
  		var appMenu = getApplication().menu;
  		
    	var rightSideBar = Ext.getCmp(appMenu._cmpRightSideBarId_);
    	var userSideBar = Ext.getCmp(appMenu._cmpUserSideBarId_);
    	var subsidiaryPanel = Ext.getCmp(appMenu._subsidiaryContainerId_);
    	var notificationSideBar = Ext.getCmp(appMenu._cmpNotificationSideBarId_);
    	var menuSideBar = Ext.getCmp(appMenu.sideBarId);
    	var newWidth = Ext.getBody().getViewSize().width;
    	var height = Ext.getBody().getViewSize().height-42;
    	var soneDockedMenu = Ext.getCmp("sone-docked-menu");
    	var soneDockedMenuSideBar = Ext.getCmp("sone-docked-menu-sidebar");
    	
    	if (!Ext.isEmpty(menuSideBar)) {
    		menuSideBar.hide();
			menuSideBar.setXY([-menuSideBar.width, 0]);
			menuSideBar.show();
			//menuSideBar.setHeight(Ext.getBody().getViewSize().height);
    	}
    	
    	if (!Ext.isEmpty(rightSideBar)) {
	    	rightSideBar.hide();
			rightSideBar.setXY([newWidth, 0]);
			rightSideBar.down('container').removeAll();
			rightSideBar.show();
    	}
    	
    	if (!Ext.isEmpty(userSideBar)) {
			userSideBar.destroy();
    		Ext.getBody().unmask();
    	}
    	
    	if (!Ext.isEmpty(notificationSideBar)) {
    		notificationSideBar.getEl().dom.style.display="none";
    		notificationSideBar.setXY([newWidth, 0]);
    		Ext.getBody().unmask();
    	}
    	
    	if (!Ext.isEmpty(soneDockedMenuSideBar)) {
			soneDockedMenuSideBar.hide();
			soneDockedMenuSideBar.setXY([-soneDockedMenuSideBar.width, 0]);
			soneDockedMenuSideBar.show();
			//soneDockedMenuSideBar.setHeight(Ext.getBody().getViewSize().height-42);
    	}
    	
    	if (!Ext.isEmpty(soneDockedMenu)) {
			//soneDockedMenu.setHeight(Ext.getBody().getViewSize().height);
			
    	}
    	
  	}    
          
    window.onbeforeunload=_leave_page;
    window.addEventListener("resize", resizeReset);
    
    // Change the mask opacity if the mask is for the main view and not the iframe
    
    if( self != top ) {
	   headTag = document.getElementsByTagName("head")[0].innerHTML;
	   var frameCSS = headTag + '<style type="text/css">.x-mask {opacity: 0.8}</style>';
	   document.getElementsByTagName('head')[0].innerHTML = frameCSS;
	} 
	else {
	   headTag = document.getElementsByTagName("head")[0].innerHTML;
	   var frameCSS = headTag + '<style type="text/css">.x-mask {opacity: 0.8}</style>';
	   document.getElementsByTagName('head')[0].innerHTML = frameCSS;
	}
	
  </script>
</body>
</html>