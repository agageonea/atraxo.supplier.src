
	<!-- Extjs  -->	 
	
	<script type="text/javascript" src="${urlUiExtjsLib}/js/ext-all-debug.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/extjs-core-extend.js?version=${productVersion}"></script>
	
	<!-- jQuery  -->	
	<script type="text/javascript" src="${urlUiExtjsLib}/js/jquery-1.11.3.min.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsLib}/js/jquery.sparkline.min.js?version=${productVersion}"></script>

	<!-- Locale -->
	<#if shortLanguage != "sys">
	<script type="text/javascript" src="${urlUiExtjsCoreI18n}/sys/e4e.js?version=${productVersion}"></script>
	<#if shortLanguage != "en">
	<script type="text/javascript" src="${urlUiExtjsCoreI18n}/en/e4e.js?version=${productVersion}"></script>
	</#if>
	</#if>
	<script type="text/javascript" src="${urlUiExtjsCoreI18n}/${shortLanguage}/extjs.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCoreI18n}/${shortLanguage}/e4e.js?version=${productVersion}"></script>

	<!-- Sencha charts -->
	
	<script type="text/javascript" src="${urlUiExtjsLib}/js/sencha-charts-debug.js?version=${productVersion}"></script>
	
	<!-- Framework -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/Main.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/extjs-extend.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/extjs-ux-extend.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/Notification.js?version=${productVersion}"></script>
	
	<!-- Base -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/NavigationTree.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/TemplateRepository.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/Session.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/Application.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/${applicationMenu}.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/LoginWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/ChangePasswordWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/SelectCompanyWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/FrameInspector.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/UserPreferences.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/Abstract_View.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/DisplayField.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/KeyboardShortcutsWindow.js?version=${productVersion}"></script>		
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/KeyBindings.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/HomePanel.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/${applicationDashboard}.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/FrameNavigatorWithIframe.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/FileUploadWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/WorkflowFormWithHtmlWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/WfAbstractFormWindowExtjs.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/WfStartFormWindowExtjs.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/WfTaskFormWindowExtjs.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/base/WorkflowFormFactory.js?version=${productVersion}"></script>
	
	<!-- asgn -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/asgn/AbstractAsgn.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/asgn/AbstractAsgnGrid.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/asgn/AbstractAsgnUi.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/asgn/AbstractAsgnPanel.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/asgn/AsgnGridBuilder.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/asgn/AsgnUiBuilder.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/asgn/AsgnWindow.js?version=${productVersion}"></script>
	
	<!-- dc -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/DcState.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/AbstractDc.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/DcActionsFactory.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/DcCommandFactory.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/DcContext.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/FlowContext.js?version=${productVersion}"></script>	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/DcActionsStateManager.js?version=${productVersion}"></script>
	
	<!-- dc-view -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDc_View.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDc_Grid.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDc_Form.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDc_PropGrid.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDcvGrid.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDcvEditableGrid.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDcvEditForm.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDcvEditPropGrid.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDcvTree.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDcvFilterForm.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/AbstractDcvFilterPropGrid.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/DcvPopovers.js?version=${productVersion}"></script>
	
	<!-- dc-view-builder -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/DcvFilterFormBuilder.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/DcvEditFormBuilder.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/DcvFilterPropGridBuilder.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/DcvEditPropGridBuilder.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/DcvGridBuilder.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/view/DcvEditableGridBuilder.js?version=${productVersion}"></script>
	
	<!-- dc-commands -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/AbstractDcCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/AbstractDcAsyncCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/AbstractDcSyncCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcQueryCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcClearQueryCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcEnterQueryCommand.js?version=${productVersion}"></script>	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcNewCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcNewInChildCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcCopyCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcSaveCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcSaveInChildCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcCancelCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcCancelOnFieldsCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcCancelSelectedCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcEditInCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcEditOutCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcCloseCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcDeleteCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcReloadRecCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcReloadPageCommand.js?version=${productVersion}"></script>		
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcPrevRecCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcNextRecCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcRpcDataCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcRpcDataListCommand.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcRpcIdListCommand.js?version=${productVersion}"></script>		
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/command/DcRpcFilterCommand.js?version=${productVersion}"></script>
	
	<!-- dc-tools -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcReport.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcBulkEditWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcImportWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcExportWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcCustomExportWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcPrintWindow.js?version=${productVersion}"></script>
<!--	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcSortWindow.js?version=${productVersion}"></script>
-->	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcFilterWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcGridLayoutWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/dc/tools/DcChartWindow.js?version=${productVersion}"></script>	
	
	<!-- frame -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/ui/FrameBuilder.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/ui/ActionBuilder.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/ui/FrameButtonStateManager.js?version=${productVersion}"></script>
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/ui/AbstractUi.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/ui/AbstractUiExtension.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/ui/WizardWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/lov/AbstractCombo.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/lov/AbstractStaticCombo.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/e4e/lov/LovAsgn.js?version=${productVersion}"></script>
	
	<!-- View & filter toolbar -->
	
	<script type="text/javascript" src="${urlUiExtjsCore}/js/viewFilterToolbar/tlb-toolbarCfg.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/viewFilterToolbar/tlb-toolbarModels.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/viewFilterToolbar/tlb-filterWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/viewFilterToolbar/tlb-viewCombo.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/viewFilterToolbar/tlb-resetButton.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/viewFilterToolbar/tlb-filterCombo.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/viewFilterToolbar/tlb-viewWindow.js?version=${productVersion}"></script>
	<script type="text/javascript" src="${urlUiExtjsCore}/js/viewFilterToolbar/tlb-toolbar.js?version=${productVersion}"></script>

