/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * DtoField
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DtoField {

	/**
	 * Dto-field marked with this flag with true value will act as the root element for the source entity class.<br> By default is false.
	 *
	 * @return
	 */
	boolean rootElement() default false;

	/**
	 * Reference path to an attribute starting from the base entity.<br> For example:<br> <code>name</code> or <code>department.name</code> or
	 * <code>region.country.name</code> <br> If it is not specified then the name of the field this annotation is attached to will be used on base
	 * entity.
	 */
	String path() default "";

}
