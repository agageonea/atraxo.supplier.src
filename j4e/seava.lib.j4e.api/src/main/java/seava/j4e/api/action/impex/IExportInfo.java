package seava.j4e.api.action.impex;

import java.util.List;

/**
 * Export descriptor. Includes columns and filter information for the export and print operations
 *
 * @author amathe
 */
public interface IExportInfo {

	/**
	 * @param modelClass
	 */
	public void prepare(Class<?> modelClass);

	/**
	 * @return
	 */
	public List<String> getFieldNames();

}
