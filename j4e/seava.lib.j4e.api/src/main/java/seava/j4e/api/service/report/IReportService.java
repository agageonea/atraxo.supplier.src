package seava.j4e.api.service.report;

import seava.j4e.api.exceptions.BusinessException;

/**
 * @author apetho
 */
public interface IReportService {

	/**
	 * Download the generated report from the report center.
	 *
	 * @param id - {@link Integer} - Report id.
	 * @return - byte array - The generated report.
	 * @throws BusinessException
	 */
	byte[] downloadReport(Integer id) throws BusinessException;

	/**
	 * Verify if the report generation is ready.
	 *
	 * @param id - {@link Integer} - Report id.
	 * @return - {@link Boolean} True if the report is ready. False if the report is not ready.
	 * @throws BusinessException
	 */
	boolean isReady(Integer id) throws BusinessException;

	/**
	 * Send notification to the user with the link.
	 *
	 * @param id - {@link Integer} - Report id.
	 * @param fileName - {@link String} - The generated Report file name.
	 * @throws BusinessException
	 */
	void sendNotification(Integer id, String fileName) throws BusinessException;

	/**
	 * Upload the new rptdesign to the report server.
	 *
	 * @param rptDesign - The report design in byte array format.
	 * @param fileName - {@link String} - The file name.
	 * @return - {@link Boolean} True if the upload was success and false if not.
	 * @throws BusinessException
	 */
	boolean uploadReport(byte[] rptDesign, String fileName) throws BusinessException;

	/**
	 * Start to generate a new report.
	 *
	 * @param data - {@link String} - Http data in xml format.
	 * @return - {@link Integer} - The new report id.
	 * @throws BusinessException
	 */
	Integer startReport(String data) throws BusinessException;

	/**
	 * Download the uploaded rptdesign file.
	 *
	 * @param fileName - {@link String} - The file name.
	 * @return - The rptdesign in byte array format.
	 * @throws BusinessException
	 */
	byte[] downloadRPTDesign(String fileName) throws BusinessException;

	/**
	 * Get the upload reports.
	 *
	 * @return - {@link String} - The uploaded reports.
	 * @throws BusinessException
	 */
	String askForReports() throws BusinessException;

	/**
	 * Get the report generation status.
	 *
	 * @param id - {@link Integer} - Report id.
	 * @return - {@link String} - The report generation status. Can be SUCCESS, RUNNING and FAIL.
	 * @throws BusinessException
	 */
	String getReportStatus(Integer id) throws BusinessException;

	/**
	 * @param dataXML
	 * @throws BusinessException
	 */
	void generateDialogReport(String dataXML) throws BusinessException;
}
