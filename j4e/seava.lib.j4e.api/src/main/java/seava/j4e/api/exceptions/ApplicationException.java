/**
 *
 */
package seava.j4e.api.exceptions;

/**
 * @author zspeter
 */
public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final IErrorCode errorCode;
	private final String errorDetails;
	private boolean withIcon = true;

	/**
	 * @param errorCode
	 * @param errorDetails
	 */
	public ApplicationException(IErrorCode errorCode, String errorDetails) {
		super(errorDetails);
		this.errorCode = errorCode;
		this.errorDetails = errorDetails;
	}

	/**
	 * @param errorCode
	 * @param errorDetails
	 * @param t
	 */
	public ApplicationException(IErrorCode errorCode, String errorDetails, Throwable t) {
		super(errorDetails, t);
		this.errorCode = errorCode;
		this.errorDetails = errorDetails;
	}

	public IErrorCode getErrorCode() {
		return this.errorCode;
	}

	public String getErrorDetails() {
		return this.errorDetails;
	}

	public boolean isWithIcon() {
		return this.withIcon;
	}
}