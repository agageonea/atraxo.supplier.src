/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.business;

import java.util.List;
import java.util.Map;

import seava.j4e.api.exceptions.BusinessException;

/**
 * IEntityWriteService
 *
 * @param <E>
 */
public interface IEntityWriteService<E> extends IEntityReadService<E> {

	/**
	 * Insert a list of new entities. This should be a transactional method.
	 *
	 * @param list
	 * @throws BusinessException
	 */
	public void insert(List<E> list) throws BusinessException;

	/**
	 * Helper insert method for one single entity. It creates a list with this single entity and delegates to the <code> insert(List<E> list)</code>
	 * method
	 *
	 * @param e
	 * @throws BusinessException
	 */
	public void insert(E e) throws BusinessException;

	/**
	 * Update a list of existing entities. This should be a transactional method.
	 *
	 * @param list
	 * @throws BusinessException
	 */
	public void update(List<E> list) throws BusinessException;

	/**
	 * Helper update method for one single entity. It creates a list with this single entity and delegates to the <code> update(List<E> list)</code>
	 * method
	 *
	 * @param e
	 * @throws BusinessException
	 */
	public void update(E e) throws BusinessException;

	/**
	 * Execute an update JPQL statement. This should be a transactional method.
	 *
	 * @param jpqlStatement
	 * @param parameters
	 * @return
	 * @throws BusinessException
	 */
	public int update(String jpqlStatement, Map<String, Object> parameters) throws BusinessException;

	/**
	 * @param id
	 * @throws BusinessException
	 */
	public void deleteById(Object id) throws BusinessException;

	/**
	 * @param ids
	 * @throws BusinessException
	 */
	public void deleteByIds(List<Object> ids) throws BusinessException;

	/**
	 * @param list
	 * @throws BusinessException
	 */
	public void delete(List<E> list) throws BusinessException;

}
