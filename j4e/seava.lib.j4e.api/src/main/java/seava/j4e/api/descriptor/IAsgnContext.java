package seava.j4e.api.descriptor;

/**
 * AsgnContext
 */
public interface IAsgnContext {

	/**
	 * @return
	 */
	public String getLeftTable();

	/**
	 * @return
	 */
	public String getLeftPkField();

	/**
	 * @return
	 */
	public String getRightTable();

	/**
	 * @return
	 */
	public String getRightObjectIdField();

	/**
	 * @return
	 */
	public String getRightItemIdField();

}
