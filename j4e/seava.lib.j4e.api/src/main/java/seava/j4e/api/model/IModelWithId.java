/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.model;

/**
 * Interface to be implemented by all models(entities and view-objects) which have an <code>id</code> primary key.
 *
 * @author amathe
 * @param <T>
 */
public interface IModelWithId<T> {

	/**
	 * @return T
	 */
	public T getId();

	/**
	 * @param id
	 */
	public void setId(T id);
}
