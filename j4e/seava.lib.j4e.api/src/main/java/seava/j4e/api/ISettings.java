/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api;

import java.util.List;

import seava.j4e.api.descriptor.ISysParamDefinitions;
import seava.j4e.api.exceptions.InvalidConfiguration;

/**
 * ISettings
 */
public interface ISettings {

	/**
	 * @param key
	 * @return
	 */
	public String get(String key);

	/**
	 * @param key
	 * @return
	 */
	public boolean getAsBoolean(String key);

	/**
	 * @param paramName
	 * @return
	 * @throws InvalidConfiguration
	 */
	public String getParam(String paramName) throws InvalidConfiguration;

	/**
	 * @param paramName
	 * @return
	 * @throws InvalidConfiguration
	 */
	public boolean getParamAsBoolean(String paramName) throws InvalidConfiguration;

	/**
	 * Reload the parameter definitions.
	 */
	public void reloadParams() throws Exception;

	/**
	 * Reload the parameter values for the current client.
	 */
	public void reloadParamValues() throws Exception;

	/**
	 * @return
	 */
	public List<ISysParamDefinitions> getParamDefinitions();

	/**
	 * @return
	 */
	public String getProductName();

	/**
	 * @return
	 */
	public String getProductDescription();

	/**
	 * @return
	 */
	public String getProductVendor();

	/**
	 * @return
	 */
	public String getProductUrl();

	/**
	 * @return
	 */
	public String getProductVersion();

	/**
	 * @return
	 */
	public String getReportCenterUrl();

	/**
	 * @return
	 */
	public String getMailMergeUrl();

	/**
	 * @return
	 */
	public String getWebDavUrl();

	/**
	 * @return
	 */
	public String getWebDavUser();

	/**
	 * @return
	 */
	public String getWebDavPassword();

	/**
	 * @return
	 */
	public Boolean isNewClientApiEnabled();

	/**
	 * @return
	 */
	public String getInitFileLocation();

}
