/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.setup;

import java.util.List;
import java.util.Map;

/**
 * ISetupTask
 */
public interface ISetupTask {

	/**
	 * @return
	 */
	public String getId();

	/**
	 * @return
	 */
	public String getTitle();

	/**
	 * @return
	 */
	public String getDescription();

	/**
	 * @return
	 */
	public List<ISetupTaskParam> getParams();

	/**
	 * @param values
	 */
	public void setParamValues(Map<String, Object> values);
}
