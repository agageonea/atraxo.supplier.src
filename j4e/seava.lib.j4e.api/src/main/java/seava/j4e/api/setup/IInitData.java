/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.setup;

import java.util.List;

/**
 * IInitData
 */
public interface IInitData {

	/**
	 * @return
	 */
	public abstract String getSequence();

	/**
	 * @param sequence
	 */
	public abstract void setSequence(String sequence);

	/**
	 * @return
	 */
	public abstract String getName();

	/**
	 * @param name
	 */
	public abstract void setName(String name);

	/**
	 * @return
	 */
	public abstract List<IInitDataItem> getItems();

	/**
	 * @param items
	 */
	public abstract void setItems(List<IInitDataItem> items);

	/**
	 * @return
	 */
	public abstract boolean isMandatory();

	/**
	 * @param mandatory
	 */
	public abstract void setMandatory(boolean mandatory);

}