/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.security;

/**
 * ThreadLocal variable holder used to send extra login parameters to the authentication service.
 */
public class LoginParamsHolder {

	public static final ThreadLocal<ILoginParams> params = new ThreadLocal<>();
}