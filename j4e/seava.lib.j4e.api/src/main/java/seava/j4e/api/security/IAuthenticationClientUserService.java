package seava.j4e.api.security;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * IAuthenticationClientUserService
 */
public interface IAuthenticationClientUserService extends UserDetailsService {

}
