/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.query;

/**
 * Filter rule
 */
public interface IFilterRule {
	/**
	 * @return
	 */
	public String getFieldName();

	/**
	 * @param fieldName
	 */
	public void setFieldName(String fieldName);

	/**
	 * @return
	 */
	public String getOperation();

	/**
	 * @param operation
	 */
	public void setOperation(String operation);

	/**
	 * @return
	 */
	public String getValue1();

	/**
	 * @param value1
	 */
	public void setValue1(String value1);

	/**
	 * @return
	 */
	public String getValue2();

	/**
	 * @param value2
	 */
	public void setValue2(String value2);

	/**
	 * @return
	 */
	public String getGroupOp();

	/**
	 * @param groupOp
	 */
	public void setGroupOp(String groupOp);
}
