/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.descriptor;

import java.util.Collection;

/**
 * SysParamDefinitons
 */
public interface ISysParamDefinitions {

	/**
	 * @return
	 */
	public Collection<ISysParamDefinition> getSysParamDefinitions();
}
