package seava.j4e.api.exceptions;

import java.io.Serializable;

/**
 * IErrorCode
 */
public interface IErrorCode extends Serializable {

	/**
	 * @return
	 */
	public String getErrGroup();

	/**
	 * @return
	 */
	public int getErrNo();

	/**
	 * @return
	 */
	public String getErrMsg();
}
