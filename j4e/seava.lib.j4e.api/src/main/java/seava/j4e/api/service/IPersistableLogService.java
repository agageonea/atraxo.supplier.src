package seava.j4e.api.service;

import seava.j4e.api.exceptions.BusinessException;

/**
 * IPersistableLogService
 */
public interface IPersistableLogService {

	public static final String PL_TYPE_JOB = "job";

	/**
	 * @return
	 */
	public String getType();

	/**
	 * @param log
	 * @return
	 * @throws BusinessException
	 */
	public String insert(IPersistableLog log) throws BusinessException;
}
