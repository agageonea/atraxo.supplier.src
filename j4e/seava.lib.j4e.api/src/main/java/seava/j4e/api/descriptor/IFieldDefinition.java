package seava.j4e.api.descriptor;

/**
 * FieldDefinition
 */
public interface IFieldDefinition {

	/**
	 * @return
	 */
	public abstract String getName();

	/**
	 * @return
	 */
	public abstract String getClassName();

}