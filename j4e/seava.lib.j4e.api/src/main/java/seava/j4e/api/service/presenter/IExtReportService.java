package seava.j4e.api.service.presenter;

import java.util.Map;

import seava.j4e.api.exceptions.BusinessException;

/**
 * IExtReportService
 */
public interface IExtReportService {

	/**
	 * @param externalReportId
	 * @param format
	 * @param paramMap
	 * @param fileName
	 * @throws BusinessException
	 */
	public void runExternalReport(String externalReportId, String format, Map<String, Object> paramMap, String fileName) throws BusinessException;

	/**
	 * @param dsName
	 * @return
	 * @throws BusinessException
	 */
	public boolean isExternalReportExists(String dsName) throws BusinessException;

}
