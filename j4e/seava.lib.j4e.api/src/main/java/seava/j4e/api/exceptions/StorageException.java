package seava.j4e.api.exceptions;

/**
 * For any storage exception.
 *
 * @author zspeter
 */
public class StorageException extends BusinessException {

	/**
	 *
	 */
	private static final long serialVersionUID = -6187257132624942188L;

	public StorageException(IErrorCode errorCode, String errorDetails, Throwable exception) {
		super(errorCode, errorDetails, exception);
	}

	public StorageException(IErrorCode errorCode, String errorDetails) {
		super(errorCode, errorDetails);
	}

	public StorageException(IErrorCode errorCode, Throwable exception) {
		super(errorCode, exception);
	}

}
