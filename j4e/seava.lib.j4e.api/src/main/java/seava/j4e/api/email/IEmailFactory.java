package seava.j4e.api.email;

/**
 * EmailFactory
 */
public interface IEmailFactory {

	/**
	 * @param type
	 * @return
	 */
	public IEmail createEmail(int type);

	/**
	 * @return
	 */
	public ITextEmail createTextEmail();

	/**
	 * @return
	 */
	public IHtmlEmail createHtmlEmail();

	/**
	 * @return
	 */
	public IMultiPartEmail createMultiPartEmail();
}
