package seava.j4e.api.service.presenter;

import seava.j4e.api.action.impex.IImportDataFile;

/**
 * IImportDataFileService
 */
public interface IImportDataFileService {

	/**
	 * @param dataFile
	 * @throws Exception
	 */
	public void execute(IImportDataFile dataFile) throws Exception;

}
