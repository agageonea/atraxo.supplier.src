/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.model;

/**
 * Interface to implement by all domain entities except the Client domain entity. A client is an isolated self-contained environment.
 *
 * @author AMATHE
 */
public interface IModelWithClientId {

	/**
	 * @return
	 */
	public String getClientId();

	/**
	 * @param clientId
	 */
	public void setClientId(String clientId);
}
