/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.presenter;

import java.util.List;

import seava.j4e.api.ISettings;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.result.IDsMarshaller;
import seava.j4e.api.service.business.IAsgnTxServiceFactory;

/**
 * IAsgnService
 *
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IAsgnService<M, F, P> {

	/**
	 * Saves the selection(s).
	 *
	 * @param selectionId
	 * @param objectId
	 * @throws Exception
	 */
	public void save(String selectionId, String objectId) throws Exception;

	/**
	 * Restores all the changes made by the user to the initial state.
	 *
	 * @param selectionId
	 * @param objectId
	 * @throws Exception
	 */
	public void reset(String selectionId, String objectId) throws Exception;

	/**
	 * Add the specified list of IDs to the selected ones.
	 *
	 * @param selectionId
	 * @param ids
	 * @throws Exception
	 */
	public void moveRight(String selectionId, List<String> ids) throws Exception;

	/**
	 * Add all the available values to the selected ones.
	 *
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param filterField
	 * @param filterValue
	 * @throws Exception
	 */
	public void moveRightAll(String selectionId, F filter, P params, String filterField, String filterValue) throws Exception;

	/**
	 * Remove the specified list of IDs from the selected ones.
	 *
	 * @param selectionId
	 * @param ids
	 * @throws Exception
	 */
	public void moveLeft(String selectionId, List<String> ids) throws Exception;

	/**
	 * Remove all the selected values.
	 *
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param filterField
	 * @param filterValue
	 * @throws Exception
	 */
	public void moveLeftAll(String selectionId, F filter, P params, String filterField, String filterValue) throws Exception;

	/**
	 * Initialize the temporary table with the existing selection. Creates a record in the TEMP_ASGN table and the existing selections in
	 * TEMP_ASGN_LINE.
	 *
	 * @param asgnName
	 * @param objectId
	 * @return the UUID of the selection
	 * @throws Exception
	 */
	public String setup(String asgnName, String objectId) throws Exception;

	/**
	 * Clean-up the temporary tables.
	 *
	 * @param selectionId
	 * @throws Exception
	 */
	public void cleanup(String selectionId) throws Exception;

	/**
	 * @return
	 */
	public List<IFilterRule> getFilterRules();

	/**
	 * @param filterRules
	 */
	public void setFilterRules(List<IFilterRule> filterRules);

	/**
	 * @param filterRules
	 */
	public void addFilterRules(List<? extends IFilterRule> filterRules);

	/**
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public List<M> findLeft(String selectionId, F filter, P params, IQueryBuilder<M, F, P> builder) throws Exception;

	/**
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public List<M> findRight(String selectionId, F filter, P params, IQueryBuilder<M, F, P> builder) throws Exception;

	/**
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public Long countLeft(String selectionId, F filter, P params, IQueryBuilder<M, F, P> builder) throws Exception;

	/**
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public Long countRight(String selectionId, F filter, P params, IQueryBuilder<M, F, P> builder) throws Exception;

	/**
	 * @return
	 * @throws Exception
	 */
	public IQueryBuilder<M, F, P> createLeftQueryBuilder() throws Exception;

	/**
	 * @return
	 * @throws Exception
	 */
	public IQueryBuilder<M, F, P> createRightQueryBuilder() throws Exception;

	/**
	 * @param dataFormat
	 * @return
	 * @throws Exception
	 */
	public IDsMarshaller<M, F, P> createMarshaller(String dataFormat) throws Exception;

	/**
	 * @return
	 */
	public List<IAsgnTxServiceFactory> getAsgnTxServiceFactories();

	/**
	 * @param asgnTxServiceFactories
	 */
	public void setAsgnTxServiceFactories(List<IAsgnTxServiceFactory> asgnTxServiceFactories);

	/**
	 * @return
	 */
	public ISettings getSettings();

	/**
	 * @param settings
	 */
	public void setSettings(ISettings settings);

}
