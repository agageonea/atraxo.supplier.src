/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.presenter;

/**
 * Interface to be implemented by any data-source service.
 *
 * @author amathe
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IDsService<M, F, P> extends IDsWriteService<M, F, P> {

}
