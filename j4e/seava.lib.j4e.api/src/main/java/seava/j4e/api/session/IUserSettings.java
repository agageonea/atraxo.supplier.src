/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.session;

import java.text.SimpleDateFormat;

import seava.j4e.api.enums.DateFormatAttribute;

/**
 * IUserSettings
 */
public interface IUserSettings {

	/**
	 * @return
	 */
	public String getLanguage();

	/**
	 * @param language
	 */
	public void setLanguage(String language);

	/**
	 * @return
	 */
	public String getNumberFormat();

	/**
	 * @return
	 */
	public String getDecimalSeparator();

	/**
	 * @return
	 */
	public String getThousandSeparator();

	/**
	 * @param key
	 * @return
	 */
	public String getDateFormatMask(String key);

	/**
	 * @param dfa
	 * @return
	 */
	public SimpleDateFormat getDateFormat(DateFormatAttribute dfa);

	/**
	 * @param key
	 * @return
	 */
	public SimpleDateFormat getDateFormat(String key);

	/**
	 * @return
	 */
	public int getMaxButtonsOnToolbars();
}
