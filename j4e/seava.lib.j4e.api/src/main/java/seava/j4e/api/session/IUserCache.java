package seava.j4e.api.session;

import seava.j4e.api.exceptions.BusinessException;

/**
 *
 * @author zspeter
 *
 */
public interface IUserCache {

	/**
	 * @param code
	 * @param clazz
	 * @return Return <code>code</code> parameter value as type <code>clazz</code>.
	 * @throws BusinessException
	 */
	<T> T getSystemParameterValueAs(String code, Class<T> clazz) throws BusinessException;

}
