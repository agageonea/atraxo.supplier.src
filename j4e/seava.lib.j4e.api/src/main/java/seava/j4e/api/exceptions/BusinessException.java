/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.exceptions;

import org.springframework.util.StringUtils;

/**
 * BusinessException
 */
public class BusinessException extends Exception {

	private static final long serialVersionUID = 1L;
	private static final String SEPARATOR = "|";

	private IErrorCode errorCode;
	private String errorDetails;
	private boolean withIcon = true;

	/**
	 * @param errorCode
	 * @param errorDetails
	 */
	public BusinessException(IErrorCode errorCode, String errorDetails) {
		super(errorDetails);
		this.errorCode = errorCode;
		this.errorDetails = errorDetails;
	}

	/**
	 * @param errorCode
	 * @param exception
	 */
	public BusinessException(IErrorCode errorCode, Throwable exception) {
		this.errorCode = errorCode;
		this.initCause(exception);
	}

	/**
	 * @param errorCode
	 * @param errorDetails
	 * @param exception
	 */
	public BusinessException(IErrorCode errorCode, String errorDetails, Throwable exception) {
		this.initCause(exception);
		this.errorCode = errorCode;
		this.errorDetails = errorDetails;
	}

	public IErrorCode getErrorCode() {
		return this.errorCode;
	}

	public boolean isWithIcon() {
		return this.withIcon;
	}

	public void setWithIcon(boolean withIcon) {
		this.withIcon = withIcon;
	}

	@Override
	public String getMessage() {
		return StringUtils.hasLength(this.errorDetails) ? this.errorDetails : this.errorCode.getErrMsg();
	}

}
