/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.setup;

/**
 * IInitDataProviderFactory
 */
public interface IInitDataProviderFactory {

	/**
	 * @return
	 */
	public IInitDataProvider createProvider();

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @param name
	 */
	public void setName(String name);
}
