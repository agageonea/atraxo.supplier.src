package seava.j4e.api.service.notification;

import org.springframework.web.context.request.async.DeferredResult;

import seava.j4e.api.model.AppNotification;

/**
 * IAppNotificationService
 */
public interface IAppNotificationService {

	/**
	 * @param clientId
	 * @param userId
	 * @param result
	 */
	void getUpdate(String clientId, String userId, DeferredResult<AppNotification> result);

	/**
	 * @param e
	 */
	void add(AppNotification e);
}
