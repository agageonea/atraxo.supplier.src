/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import seava.j4e.api.descriptor.IUploadedFileDescriptor;

/**
 * IFileUploadService
 */
public interface IFileUploadService {

	/**
	 * @return
	 */
	public List<String> getParamNames();

	/**
	 * @param fileDescriptor
	 * @param inputStream
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> execute(IUploadedFileDescriptor fileDescriptor, InputStream inputStream, Map<String, String> params) throws Exception;

}
