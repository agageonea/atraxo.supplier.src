package seava.j4e.api.service;

import seava.j4e.api.session.IWorkspace;

/**
 * IClientInfoProvider
 */
public interface IClientInfoProvider {

	/**
	 * @return
	 */
	public IWorkspace getClientWorkspace();
}
