package seava.j4e.api.session;

/**
 * IOrganization
 */
public interface IOrganization {

	/**
	 * @return
	 */
	public String getCode();

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @return
	 */
	public boolean isUsed();

	/**
	 * @param arg
	 */
	public void setUsed(boolean arg);

	/**
	 * @return
	 */
	public String getId();

}
