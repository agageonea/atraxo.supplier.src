/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.presenter;

import seava.j4e.api.ISettings;
import seava.j4e.api.action.result.IDsMarshaller;

/**
 * IDsBaseService
 *
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IDsBaseService<M, F, P> {

	/**
	 * @return
	 */
	public Class<M> getModelClass();

	/**
	 * @return
	 */
	public Class<?> getEntityClass();

	/**
	 * @return
	 */
	public Class<F> getFilterClass();

	/**
	 * @return
	 */
	public Class<P> getParamClass();

	/**
	 * @param dataFormat
	 * @return
	 * @throws Exception
	 */
	public IDsMarshaller<M, F, P> createMarshaller(String dataFormat) throws Exception;

	/**
	 * @return
	 */
	public ISettings getSettings();

	/**
	 * @param settings
	 */
	public void setSettings(ISettings settings);

}
