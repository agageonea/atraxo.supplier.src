package seava.j4e.api.security;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * IAuthenticationSystemUserService
 */
public interface IAuthenticationSystemUserService extends UserDetailsService {

}
