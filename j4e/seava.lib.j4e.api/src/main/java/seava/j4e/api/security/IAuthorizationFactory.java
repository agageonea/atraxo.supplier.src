/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.security;

/**
 * IAuthorizationFactory
 */
public interface IAuthorizationFactory {

	/**
	 * Return an assignment authorization service.
	 *
	 * @return
	 */
	public IAuthorization getAsgnAuthorizationProvider();

	/**
	 * Return a data-source authorization service.
	 *
	 * @return
	 */
	public IAuthorization getDsAuthorizationProvider();

	/**
	 * Return a job authorization service.
	 *
	 * @return
	 */
	public IAuthorization getJobAuthorizationProvider();
}
