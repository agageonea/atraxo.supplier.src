/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.exceptions;

/**
 * NotAuthorizedRequestException
 */
public class NotAuthorizedRequestException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public NotAuthorizedRequestException(String message) {
		super(message);
	}

}