/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.descriptor;

/**
 * SysParamDefinition
 */
public interface ISysParamDefinition {

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @return
	 */
	public String getTitle();

	/**
	 * @return
	 */
	public String getDescription();

	/**
	 * @return
	 */
	public String getDataType();

	/**
	 * @return
	 */
	public String getDefaultValue();

	/**
	 * @return
	 */
	public String getListOfValues();

}