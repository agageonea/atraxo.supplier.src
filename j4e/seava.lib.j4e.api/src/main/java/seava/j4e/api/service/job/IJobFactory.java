package seava.j4e.api.service.job;

/**
 * IJobFactory
 */
public interface IJobFactory {

	/**
	 * @param key
	 * @return
	 */
	public IJob create(String key);

}
