package seava.j4e.api.service.presenter;

import seava.j4e.api.descriptor.IReportExecutionContext;

/**
 * IReportService
 */
public interface IReportService {

	/**
	 * @param reportFqn
	 * @return
	 * @throws Exception
	 */
	public Object getReport(String reportFqn) throws Exception;

	/**
	 * @param context
	 * @throws Exception
	 */
	public void runReport(IReportExecutionContext context) throws Exception;

}
