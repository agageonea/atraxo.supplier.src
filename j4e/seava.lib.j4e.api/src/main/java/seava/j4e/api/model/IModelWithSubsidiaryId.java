package seava.j4e.api.model;

/**
 *
 * @author zspeter
 *
 */
public interface IModelWithSubsidiaryId {

	/**
	 *
	 * @return
	 */
	public String getSubsidiaryId();

	/**
	 *
	 * @param subsidiaryId
	 */
	public void setSubsidiaryId(String subsidiaryId);
}
