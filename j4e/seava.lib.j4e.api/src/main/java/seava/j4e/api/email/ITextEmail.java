package seava.j4e.api.email;

import seava.j4e.api.exceptions.EmailException;

/**
 * TextEmail
 */
public interface ITextEmail extends IEmail {

	@Override
	public IEmail setMsg(String msg) throws EmailException;
}
