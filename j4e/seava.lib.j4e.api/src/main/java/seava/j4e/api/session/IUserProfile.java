/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.session;

import java.util.List;

/**
 * IUserProfile
 */
public interface IUserProfile {

	/**
	 * @return
	 */
	public boolean isAdministrator();

	/**
	 * @return
	 */
	public List<String> getRoles();

	/**
	 * @return
	 */
	public boolean isCredentialsExpired();

	/**
	 * @return
	 */
	public boolean isAccountExpired();

	/**
	 * @return
	 */
	public boolean isAccountLocked();

	/**
	 * @return
	 */
	public List<IOrganization> getOrganizations();

	/**
	 * @param org
	 */
	public void addOrganisation(IOrganization org);

	/**
	 * @param list
	 */
	public void setOrganisation(List<IOrganization> list);

	/**
	 * @return a list with organization ids.
	 */
	public List<String> getOrganizationIds();

	/**
	 * @param cache
	 */
	public void setCache(IUserCache cache);

	/**
	 * @return
	 */
	public IUserCache getCache();

}