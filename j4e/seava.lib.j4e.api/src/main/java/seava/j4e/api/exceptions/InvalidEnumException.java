package seava.j4e.api.exceptions;

/**
 * Exception to use when an enum cannot be created from static value.
 *
 * @author tlukacs
 */
public class InvalidEnumException extends RuntimeException {

	private static final long serialVersionUID = -8584258928751852870L;

	/**
	 * @param message
	 */
	public InvalidEnumException(String message) {
		super(message);
	}
}
