/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.impex;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @param <M>
 */
public interface IDsExport<M> {

	/**
	 * @return
	 */
	public IExportInfo getExportInfo();

	/**
	 * @param exportInfo
	 */
	public void setExportInfo(IExportInfo exportInfo);

	/**
	 * @throws Exception
	 */
	public void begin() throws Exception;

	/**
	 * @throws Exception
	 */
	public void end() throws Exception;

	/**
	 * @param data
	 * @param isFirst
	 * @throws Exception
	 */
	public void write(M data, boolean isFirst) throws Exception;

	/**
	 * @return
	 * @throws IOException
	 */
	public File getOutFile() throws IOException;

	/**
	 * @param outFile
	 */
	public void setOutFile(File outFile);

	/**
	 * @return
	 */
	public String getOutFilePath();

	/**
	 * @param outFilePath
	 */
	public void setOutFilePath(String outFilePath);

	/**
	 * @return
	 */
	public String getOutFileName();

	/**
	 * @param outFileName
	 */
	public void setOutFileName(String outFileName);

	/**
	 * @return
	 */
	public String getOutFileExtension();

	/**
	 * @return
	 */
	public Map<String, Object> getProperties();

	/**
	 * @param properties
	 */
	public void setProperties(Map<String, Object> properties);

}
