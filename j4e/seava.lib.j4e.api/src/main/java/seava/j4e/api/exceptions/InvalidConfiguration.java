/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.exceptions;

/**
 * InvalidConfiguration
 */
public class InvalidConfiguration extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public InvalidConfiguration(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param e
	 */
	public InvalidConfiguration(String message, Exception e) {
		super(message, e);
	}
}
