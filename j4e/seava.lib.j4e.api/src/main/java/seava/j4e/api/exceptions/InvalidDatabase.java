package seava.j4e.api.exceptions;

/**
 * InvalidDatabase
 */
public class InvalidDatabase extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public InvalidDatabase(String message) {
		super(message);
	}
}
