/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.business;

/**
 * IAsgnTxServiceFactory
 */
public interface IAsgnTxServiceFactory {

	/**
	 * @param key
	 * @return
	 */
	public <E> IAsgnTxService<E> create(String key);

	/**
	 * @param type
	 * @return
	 */
	public <E> IAsgnTxService<E> create(Class<E> type);

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @param name
	 */
	public void setName(String name);
}
