/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.business;

import java.util.List;

import javax.persistence.EntityManager;

import seava.j4e.api.descriptor.IAsgnContext;

/**
 * IAsgnTxService
 *
 * @param <E>
 */
public interface IAsgnTxService<E> {

	/**
	 * Saves the selection(s).
	 *
	 * @param ctx
	 * @param selectionId
	 * @param objectId
	 * @throws Exception
	 */
	public void doSave(IAsgnContext ctx, String selectionId, String objectId) throws Exception;

	/**
	 * Restores all the changes made by the user to the initial state.
	 *
	 * @param ctx
	 * @param selectionId
	 * @param objectId
	 * @throws Exception
	 */
	public void doReset(IAsgnContext ctx, String selectionId, String objectId) throws Exception;

	/**
	 * Add the specified list of IDs to the selected ones.
	 *
	 * @param ctx
	 * @param selectionId
	 * @param ids
	 * @throws Exception
	 */
	public void doMoveRight(IAsgnContext ctx, String selectionId, List<String> ids) throws Exception;

	/**
	 * Add all the available values to the selected ones.
	 *
	 * @param ctx
	 * @param selectionId
	 * @param where
	 * @param sqlParams
	 * @throws Exception
	 */
	public void doMoveRightAll(IAsgnContext ctx, String selectionId, String where, List<?> sqlParams) throws Exception;

	/**
	 * Remove the specified list of IDs from the selected ones.
	 *
	 * @param ctx
	 * @param selectionId
	 * @param ids
	 * @throws Exception
	 */
	public void doMoveLeft(IAsgnContext ctx, String selectionId, List<String> ids) throws Exception;

	/**
	 * Remove all the selected values.
	 *
	 * @param ctx
	 * @param selectionId
	 * @param where
	 * @throws Exception
	 */
	public void doMoveLeftAll(IAsgnContext ctx, String selectionId, String where, List<?> sqlParams) throws Exception;

	/**
	 * Initialize the temporary table with the existing selection. Creates a record in the TEMP_ASGN table and the existing selections in
	 * TEMP_ASGN_LINE.
	 *
	 * @param ctx
	 * @param asgnName
	 * @param objectId
	 * @return the UUID of the selection
	 * @throws Exception
	 */
	public String doSetup(IAsgnContext ctx, String asgnName, String objectId) throws Exception;

	/**
	 * Clean-up the temporary tables.
	 *
	 * @param ctx
	 * @param selectionId
	 * @throws Exception
	 */
	public void doCleanup(IAsgnContext ctx, String selectionId) throws Exception;

	/**
	 * @return
	 */
	public EntityManager getEntityManager();

	/**
	 * @param em
	 */
	public void setEntityManager(EntityManager em);

	/**
	 * @return
	 */
	public Class<E> getEntityClass();

	/**
	 * @param entityClass
	 */
	public void setEntityClass(Class<E> entityClass);

}