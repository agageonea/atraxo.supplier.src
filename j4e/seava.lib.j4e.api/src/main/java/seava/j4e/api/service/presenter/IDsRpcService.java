/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.presenter;

import java.io.InputStream;
import java.util.List;

/**
 * IDsRpcService
 *
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IDsRpcService<M, F, P> extends IDsBaseService<M, F, P> {

	/**
	 * @param procedureName
	 * @param filter
	 * @param params
	 * @throws Exception
	 */
	public void rpcFilter(String procedureName, F filter, P params) throws Exception;

	/**
	 * @param procedureName
	 * @param ds
	 * @param params
	 * @throws Exception
	 */
	public void rpcData(String procedureName, M ds, P params) throws Exception;

	/**
	 * @param procedureName
	 * @param list
	 * @param params
	 * @throws Exception
	 */
	public void rpcData(String procedureName, List<M> list, P params) throws Exception;

	/**
	 * @param procedureName
	 * @param list
	 * @param params
	 * @throws Exception
	 */
	public void rpcIds(String procedureName, List<Object> list, P params) throws Exception;

	/**
	 * @param procedureName
	 * @param filter
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public InputStream rpcFilterStream(String procedureName, F filter, P params) throws Exception;

	/**
	 * @param procedureName
	 * @param ds
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public InputStream rpcDataStream(String procedureName, M ds, P params) throws Exception;

	/**
	 * @param procedureName
	 * @param list
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public InputStream rpcDataStream(String procedureName, List<M> list, P params) throws Exception;

	/**
	 * @param procedureName
	 * @param list
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public InputStream rpcIdsStream(String procedureName, List<Object> list, P params) throws Exception;

}
