/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.business;

import java.util.Map;

import javax.persistence.EntityManager;

import seava.j4e.api.ISettings;
import seava.j4e.api.exceptions.BusinessException;

/**
 * IEntityBaseService
 *
 * @param <E>
 */
public interface IEntityBaseService<E> {

	/**
	 * Get EntityManager
	 *
	 * @return
	 */
	public EntityManager getEntityManager();

	/**
	 * Set EntityManager
	 *
	 * @param em
	 */
	public void setEntityManager(EntityManager em);

	/**
	 * @return
	 * @throws BusinessException
	 */
	public E create() throws BusinessException;

	/**
	 * @return
	 */
	public ISettings getSettings();

	/**
	 * @param settings
	 */
	public void setSettings(ISettings settings);

	/**
	 * @param processDefinitionKey
	 * @param businessKey
	 * @param variables
	 * @throws BusinessException
	 */
	public void doStartWfProcessInstanceByKey(String processDefinitionKey, String businessKey, Map<String, Object> variables)
			throws BusinessException;

	/**
	 * @param processDefinitionId
	 * @param businessKey
	 * @param variables
	 * @throws BusinessException
	 */
	public void doStartWfProcessInstanceById(String processDefinitionId, String businessKey, Map<String, Object> variables) throws BusinessException;

	/**
	 * @param messageName
	 * @param businessKey
	 * @param processVariables
	 * @throws BusinessException
	 */
	public void doStartWfProcessInstanceByMessage(String messageName, String businessKey, Map<String, Object> processVariables)
			throws BusinessException;
}
