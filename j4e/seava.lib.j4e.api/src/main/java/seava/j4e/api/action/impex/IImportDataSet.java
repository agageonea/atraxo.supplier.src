package seava.j4e.api.action.impex;

import java.util.List;

/**
 * Import DataSet
 */
public interface IImportDataSet {

	/**
	 * @return
	 */
	public List<IImportDataFile> getDataFiles();

	/**
	 * @param dataFiles
	 */
	public void setDataFiles(List<IImportDataFile> dataFiles);
}
