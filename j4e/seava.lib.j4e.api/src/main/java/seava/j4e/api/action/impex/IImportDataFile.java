package seava.j4e.api.action.impex;

/**
 * Import Data File
 */
public interface IImportDataFile {

	/**
	 * Insert or Upgrade
	 */
	enum ImportAction {
		INSERT,
		UPGRADE
	};

	/**
	 * @return
	 */
	public String getDs();

	/**
	 * @param ds
	 */
	public void setDs(String ds);

	/**
	 * @return
	 */
	public String getFile();

	/**
	 * @param file
	 */
	public void setFile(String file);

	/**
	 * @return
	 */
	public String getUkFieldName();

	/**
	 * @param ukFieldName
	 */
	public void setUkFieldName(String ukFieldName);

	/**
	 * @return
	 */
	public String getcUkFieldName();

	/**
	 * @param cUkFieldName
	 */
	public void setcUkFieldName(String cUkFieldName);

	/**
	 * @return
	 */
	public String getRelFieldName();

	/**
	 * @param rRelFieldName
	 */
	public void setRelFieldName(String rRelFieldName);

	/**
	 * @return
	 */
	public String getcDs();

	/**
	 * @param cDs
	 */
	public void setcDs(String cDs);

	/**
	 * @return
	 */
	public ImportAction getAction();

	/**
	 * @param action
	 */
	public void setAction(ImportAction action);

}
