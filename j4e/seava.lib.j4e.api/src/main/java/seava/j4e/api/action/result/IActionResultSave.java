/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.result;

import java.util.List;

/**
 * Action Result Save
 */
public interface IActionResultSave extends IActionResult {

	/**
	 * @return
	 */
	public List<?> getData();

	/**
	 * @param data
	 */
	public void setData(List<?> data);

	/**
	 * @return
	 */
	public Object getParams();

	/**
	 * @param params
	 */
	public void setParams(Object params);

}
