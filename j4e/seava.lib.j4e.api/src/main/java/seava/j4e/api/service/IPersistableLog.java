package seava.j4e.api.service;

import java.util.List;
import java.util.Map;

/**
 * IPersistableLog
 */
public interface IPersistableLog {

	/**
	 * @param message
	 */
	public void info(String message);

	/**
	 * @param message
	 */
	public void error(String message);

	/**
	 * @param message
	 */
	public void warning(String message);

	/**
	 * @param key
	 * @param value
	 */
	public void setProperty(String key, Object value);

	/**
	 * @param key
	 * @return
	 */
	public Object getProperty(String key);

	/**
	 * @return
	 */
	public Map<String, Object> getProperties();

	/**
	 * @return
	 */
	public List<IPersistableLogMessage> getMessages();

}
