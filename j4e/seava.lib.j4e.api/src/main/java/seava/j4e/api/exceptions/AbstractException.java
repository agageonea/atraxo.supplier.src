/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.exceptions;

/**
 * AbstractException
 */
public abstract class AbstractException extends Exception {

	private static final long serialVersionUID = 1L;
	protected String errorCode;

	/**
	 * Default constructor
	 */
	public AbstractException() {
		super();
	}

	protected AbstractException(String message) {
		super(message);
		this.errorCode = null;
	}

	protected AbstractException(String errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	protected AbstractException(String message, Throwable exception) {
		super(message);
		this.initCause(exception);
		this.errorCode = null;
	}

	protected AbstractException(String errorCode, String message, Throwable exception) {
		super(message);
		this.initCause(exception);
		this.errorCode = errorCode;
	}

}
