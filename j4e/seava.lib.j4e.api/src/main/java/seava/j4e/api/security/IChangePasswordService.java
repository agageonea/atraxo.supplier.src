/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.security;

/**
 * IChangePasswordService
 */
public interface IChangePasswordService {

	/**
	 * @param userCode
	 * @param newPassword
	 * @param oldPassword
	 * @param clientId
	 * @param clientCode
	 * @throws Exception
	 */
	public void doChangePassword(String userCode, String newPassword, String oldPassword, String clientId, String clientCode) throws Exception;
}
