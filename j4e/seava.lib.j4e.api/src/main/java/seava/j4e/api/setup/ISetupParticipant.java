/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.setup;

import java.util.List;

/**
 * ISetupParticipant
 */
public interface ISetupParticipant extends Comparable<ISetupParticipant> {

	/**
	 * @return
	 */
	public List<ISetupTask> getTasks();

	/**
	 * @param taskId
	 * @return
	 */
	public ISetupTask getTask(String taskId);

	/**
	 * @return
	 */
	public String getBundleId();

	/**
	 * @return
	 */
	public String getTargetName();

	/**
	 * @throws Exception
	 */
	public void execute() throws Exception;

	/**
	 * @return
	 */
	public boolean hasWorkToDo();

	/**
	 * @return
	 */
	public int getRanking();

	/**
	 * @param ranking
	 */
	public void setRanking(int ranking);

}
