package seava.j4e.api.descriptor;

/**
 * JobDefinition
 */
public interface IJobDefinition {

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @param name
	 */
	public void setName(String name);

	/**
	 * @return
	 */
	public Class<?> getJavaClass();

	/**
	 * @param javaClass
	 */
	public void setJavaClass(Class<?> javaClass);
}
