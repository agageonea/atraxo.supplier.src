/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.setup;

/**
 * ISetupTaskParam
 */
public interface ISetupTaskParam {

	public static final String PREFIX = "stp_";

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @param name
	 */
	public void setName(String name);

	/**
	 * @return
	 */
	public String getTitle();

	/**
	 * @param title
	 */
	public void setTitle(String title);

	/**
	 * @return
	 */
	public String getDescription();

	/**
	 * @param description
	 */
	public void setDescription(String description);

	/**
	 * @return
	 */
	public String getFieldType();

	/**
	 * @param fieldType
	 */
	public void setFieldType(String fieldType);

	/**
	 * @return
	 */
	public String getDataType();

	/**
	 * @param dataType
	 */
	public void setDataType(String dataType);

	/**
	 * @return
	 */
	public String getDefaultValue();

	/**
	 * @param defaultValue
	 */
	public void setDefaultValue(String defaultValue);

	/**
	 * @return
	 */
	public String getListOfValues();

	/**
	 * @param listOfValues
	 */
	public void setListOfValues(String listOfValues);

	/**
	 * @return
	 */
	public String getValue();

	/**
	 * @param value
	 */
	public void setValue(String value);

	/**
	 * @return
	 */
	public boolean isRequired();

	/**
	 * @param required
	 */
	public void setRequired(boolean required);
}
