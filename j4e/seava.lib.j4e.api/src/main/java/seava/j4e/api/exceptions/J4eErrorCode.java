package seava.j4e.api.exceptions;

import java.util.Locale;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.j4e.api.session.Session;

public enum J4eErrorCode implements IErrorCode {

	REPORT_GENERATOR_PROBLEM(8000, "%s", "Error"),
	INTERFACE_NOTIFICATION_SUCCESS(8001, "%s was completed successfully.", "Error"),
	INTERFACE_NOTIFICATION_FAILURE(8002, "%s failed. Check report result for details.", "Error"),
	REPORT_GENERATOR_SUCCESS(8003, "The report was generated successfully", "Error"),
	INVALID_FILE_PATH(8004, "Invalid file path %s", "Error"),

	DB_DUPLICATE_KEY_FOUND(8010, "Duplicate key found for entity %s based on following fields: %s.", "Error"),
	DB_NO_RESULT(8011, "Entity %s not found based on following fields: %s.", "Error");

	private final Logger logger = LoggerFactory.getLogger(J4eErrorCode.class);

	private final ResourceBundle boundle;

	private final int errNo;
	private final String errMsg;
	private final String errGroup;

	private J4eErrorCode(int errNo, String errMsg, String errGroup) {
		this.errNo = errNo;
		this.errMsg = errMsg;
		this.errGroup = errGroup;
		String language;
		try {
			language = Session.user.get().getSettings().getLanguage();
		} catch (NullPointerException e) {
			language = "en_US";
			this.logger.warn("Warning:could no retrieve language for session user, will use the default one:" + language, e);
		}
		this.boundle = ResourceBundle.getBundle("locale.sone.cmm.error.messages", new Locale(language));
	}

	@Override
	public String getErrGroup() {
		return this.boundle.containsKey(this.errGroup) ? this.boundle.getString(this.errGroup) : this.errGroup;
	}

	@Override
	public int getErrNo() {
		return this.errNo;
	}

	@Override
	public String getErrMsg() {
		return this.boundle.containsKey(Integer.toString(this.errNo)) ? this.boundle.getString(Integer.toString(this.errNo)) : this.errMsg;
	}
}
