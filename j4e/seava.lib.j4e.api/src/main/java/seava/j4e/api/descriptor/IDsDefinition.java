/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.descriptor;

import java.util.List;

/**
 * DsDefinition
 */
public interface IDsDefinition {

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @param name
	 */
	public void setName(String name);

	/**
	 * @return
	 */
	public Class<?> getModelClass();

	/**
	 * @param modelClass
	 */
	public void setModelClass(Class<?> modelClass);

	/**
	 * @return
	 */
	public List<IFieldDefinition> getModelFields();

	/**
	 * @return
	 */
	public Class<?> getFilterClass();

	/**
	 * @param filterClass
	 */
	public void setFilterClass(Class<?> filterClass);

	/**
	 * @return
	 */
	public List<IFieldDefinition> getFilterFields();

	/**
	 * @return
	 */
	public Class<?> getParamClass();

	/**
	 * @param paramClass
	 */
	public void setParamClass(Class<?> paramClass);

	/**
	 * @return
	 */
	public List<IFieldDefinition> getParamFields();

	/**
	 * @return
	 */
	public boolean isAsgn();

	/**
	 * @return
	 */
	public boolean isReadOnly();

	/**
	 * @param isAsgn
	 */
	public void setAsgn(boolean isAsgn);

	/**
	 * @param serviceMethod
	 */
	public void addServiceMethod(String serviceMethod);

	/**
	 * @return
	 */
	public List<String> getServiceMethods();

}
