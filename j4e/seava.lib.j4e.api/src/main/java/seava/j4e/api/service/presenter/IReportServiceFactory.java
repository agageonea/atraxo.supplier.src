package seava.j4e.api.service.presenter;

/**
 * IReportServiceFactory
 */
public interface IReportServiceFactory {

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @param key
	 * @return
	 */
	public IReportService create(String key);
}
