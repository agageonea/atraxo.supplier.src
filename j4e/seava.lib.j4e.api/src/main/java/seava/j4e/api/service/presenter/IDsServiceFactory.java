/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.presenter;

/**
 * IDsServiceFactory
 */
public interface IDsServiceFactory {

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @param key
	 * @return
	 */
	public <M, F, P> IDsService<M, F, P> create(String key);
}
