package seava.j4e.api.session;

/**
 * IWorkspace
 */
public interface IWorkspace {

	/**
	 * @return
	 */
	public String getWorkspacePath();

	/**
	 * @return
	 */
	public String getImportPath();

	/**
	 * @return
	 */
	public String getExportPath();

	/**
	 * @return
	 */
	public String getTempPath();

}
