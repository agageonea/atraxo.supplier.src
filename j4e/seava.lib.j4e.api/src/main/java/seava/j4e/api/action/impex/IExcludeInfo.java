package seava.j4e.api.action.impex;

import java.util.List;

/**
 * Exclude Info
 */
public interface IExcludeInfo {

	/**
	 * @return
	 */
	public List<String> getFields();

	/**
	 * @param fields
	 */
	public void setFields(List<String> fields);

}
