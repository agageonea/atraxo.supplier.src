/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.result;

/**
 * Root interface of the result types
 *
 * @author amathe
 */
public interface IActionResult {
	/**
	 * Get the total execution time in milliseconds.
	 *
	 * @return
	 */
	public long getExecutionTime();

	/**
	 * Set the total execution time in milliseconds.
	 *
	 * @param executionTime
	 */
	public void setExecutionTime(long executionTime);
}
