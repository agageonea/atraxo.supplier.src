/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.query;

import java.util.List;
import java.util.Map;

import seava.j4e.api.ISettings;
import seava.j4e.api.descriptor.IViewModelDescriptor;

/**
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IQueryBuilder<M, F, P> {

	/**
	 * Add fetch limit constraint. <br>
	 * Fetch <code>resultSize</code> number of results starting from <code>resultStart</code> position.
	 *
	 * @param resultStart
	 * @param resultSize
	 * @return this
	 */
	public IQueryBuilder<M, F, P> addFetchLimit(int resultStart, int resultSize);

	/**
	 * Add sort information. The number of elements in the two arrays must be the same.
	 *
	 * @param columnList
	 *            Array of field names E.g. {"name", "code"}
	 * @param senseList
	 *            Array of sense E.g. {"asc", "desc"}
	 * @return this
	 */
	public IQueryBuilder<M, F, P> addSortInfo(String[] columnList, String[] senseList);

	/**
	 * Add sort information. The number of elements in the strings must be the same.
	 *
	 * @param columns
	 *            Comma delimited column names E.g. "name,code"
	 * @param sense
	 *            Comma delimited column names E.g. "asc,desc"
	 * @return this
	 */
	public IQueryBuilder<M, F, P> addSortInfo(String columns, String sense);

	/**
	 * Add sort information.
	 *
	 * @param sortTokens
	 *            The sort tokens E.g. {"name", "code desc", "id asc"}
	 * @return this
	 * @throws Exception
	 */
	public IQueryBuilder<M, F, P> addSortInfo(String[] sortTokens) throws Exception;

	/**
	 * Add sort information.
	 *
	 * @param sortTokens
	 * @return this
	 */
	public IQueryBuilder<M, F, P> addSortInfo(List<? extends ISortToken> sortTokens);

	/**
	 * @param filterRules
	 * @return
	 */
	public IQueryBuilder<M, F, P> addFilterRules(List<? extends IFilterRule> filterRules);

	/**
	 * @return
	 */
	public Class<M> getModelClass();

	/**
	 * @param modelClass
	 */
	public void setModelClass(Class<M> modelClass);

	/**
	 * @return
	 */
	public Class<F> getFilterClass();

	/**
	 * @param filterClass
	 */
	public void setFilterClass(Class<F> filterClass);

	/**
	 * @return
	 */
	public Class<P> getParamClass();

	/**
	 * @param paramClass
	 */
	public void setParamClass(Class<P> paramClass);

	/**
	 * @return
	 */
	public F getFilter();

	/**
	 * @param filter
	 */
	public void setFilter(F filter);

	/**
	 * @return
	 */
	public List<IFilterRule> getFilterRules();

	/**
	 * @param filterRules
	 */
	public void setFilterRules(List<IFilterRule> filterRules);

	/**
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	public IQueryBuilder<M, F, P> addFilter(F filter) throws Exception;

	/**
	 * @return
	 */
	public P getParams();

	/**
	 * @param params
	 */
	public void setParams(P params);

	/**
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public IQueryBuilder<M, F, P> addParams(P params) throws Exception;

	/**
	 * @return
	 */
	public IViewModelDescriptor<M> getDescriptor();

	/**
	 * @param descriptor
	 */
	public void setDescriptor(IViewModelDescriptor<M> descriptor);

	/**
	 * @return
	 */
	public ISettings getSettings();

	/**
	 * @param settings
	 */
	public void setSettings(ISettings settings);

	/**
	 * give possibility to add custom object to the query builder these objects can be used in the custom code where the query builder is passed as
	 * parameter
	 *
	 * @return
	 */
	public Map<String, Object> getExtraAttributes();

	/**
	 * @param extraAttributes
	 */
	public void setExtraAttributes(Map<String, Object> extraAttributes);

	/**
	 * @param key
	 * @param attr
	 */
	public void putExtraAttribute(String key, Object attr);

	/**
	 * @param key
	 * @return
	 */
	public Object getExtraAttribute(String key);

}
