/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.setup;

import java.util.List;

/**
 * IInitDataProvider
 */
public interface IInitDataProvider {

	/**
	 * @return
	 */
	public List<IInitData> getList();

	/**
	 * @param list
	 */
	public void setList(List<IInitData> list);

	/**
	 * @param initData
	 */
	public void addToList(IInitData initData);
}
