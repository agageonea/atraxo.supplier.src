/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.setup;

import java.io.File;

/**
 * IInitDataItem
 */
public interface IInitDataItem {

	/**
	 * @return
	 */
	public abstract String getDestPath();

	/**
	 * @param destPath
	 */
	public abstract void setDestPath(String destPath);

	/**
	 * @return
	 */
	public abstract String getSequence();

	/**
	 * @param sequence
	 */
	public abstract void setSequence(String sequence);

	/**
	 * @return
	 */
	public abstract String getDsName();

	/**
	 * @param dsName
	 */
	public abstract void setDsName(String dsName);

	/**
	 * @return
	 */
	public abstract String getFileName();

	/**
	 * @param fileName
	 */
	public abstract void setFileName(String fileName);

	/**
	 * @return
	 */
	public abstract File getFile();

	/**
	 * @param file
	 */
	public abstract void setFile(File file);

	/**
	 * @return
	 */
	public abstract String getUkFieldName();

	/**
	 * @param ukFieldName
	 */
	public abstract void setUkFieldName(String ukFieldName);

}