/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.presenter;

import java.io.InputStream;
import java.util.List;

/**
 * IDsWriteService
 *
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IDsWriteService<M, F, P> extends IDsReadService<M, F, P> {

	/**
	 * Handler for insert event.
	 *
	 * @param ds
	 *            data-source instance
	 * @param params
	 * @throws Exception
	 */
	public void insert(M ds, P params) throws Exception;

	/**
	 * Handler for insert event.
	 *
	 * @param list
	 *            data-source instances list
	 * @param params
	 * @throws Exception
	 */
	public void insert(List<M> list, P params) throws Exception;

	/**
	 * Handler for update event.
	 *
	 * @param ds
	 *            data-source instance
	 * @param params
	 * @throws Exception
	 */
	public void update(M ds, P params) throws Exception;

	/**
	 * Handler for update event.
	 *
	 * @param list
	 *            data-source instances list
	 * @param params
	 * @throws Exception
	 */
	public void update(List<M> list, P params) throws Exception;

	/**
	 * Handler for delete given a data-source id.
	 *
	 * @param id
	 */
	public void deleteById(Object id) throws Exception;

	/**
	 * Handler for delete given a list of data-source id`s.
	 *
	 * @param ids
	 */
	public void deleteByIds(List<Object> ids) throws Exception;

	/**
	 * Handler for basic data import given an input stream. Performs an insert.
	 *
	 * @param inputStream
	 * @param sourceName
	 * @param config
	 * @throws Exception
	 */
	public void doImport(InputStream inputStream, String sourceName, Object config) throws Exception;

	/**
	 * Handler for basic data import given a file-name as absolute location. Performs an insert.
	 *
	 * @param absoluteFileName
	 * @param config
	 * @throws Exception
	 */
	public void doImport(String absoluteFileName, Object config) throws Exception;

	/**
	 * Handler for basic data import given a file-name and directory where it resides. Performs an insert.
	 *
	 * @param relativeFileName
	 * @param path
	 * @param config
	 * @throws Exception
	 */
	public void doImport(String relativeFileName, String path, Object config) throws Exception;

	/**
	 * Handler for basic data import given a file-name and directory where it resides. <br>
	 * Performs an update. <br>
	 * Tries to read the value from the database using the <code>ukFieldName</code> as an unique key field, apply the changes read from the file to be
	 * imported and updates the result.
	 *
	 * @param absoluteFileName
	 * @param ukFieldName
	 * @param batchSize
	 * @param config
	 * @throws Exception
	 */
	public void doImport(String absoluteFileName, String ukFieldName, int batchSize, Object config) throws Exception;

	/**
	 * Handler for basic data import given a file-name and directory where it resides. <br>
	 * Performs an update. <br>
	 * Tries to read the value from the database using the <code>ukFieldName</code> as an unique key field, apply the changes read from the file to be
	 * imported and updates the result.
	 *
	 * @param relativeFileName
	 * @param path
	 * @param ukFieldName
	 * @param batchSize
	 * @param config
	 * @throws Exception
	 */
	public void doImport(String relativeFileName, String path, String ukFieldName, int batchSize, Object config) throws Exception;

	/**
	 * @param absoluteFileName
	 * @param config
	 * @throws Exception
	 */
	public void doUpgrade(String absoluteFileName, Object config) throws Exception;

	/**
	 * @param relativeFileName
	 * @param config
	 * @param pField
	 * @param cField
	 * @param relField
	 * @param cDsName
	 * @throws Exception
	 */
	public void doImportAsAssign(String relativeFileName, Object config, String pField, String cField, String relField, String cDsName)
			throws Exception;

}
