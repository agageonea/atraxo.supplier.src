package seava.j4e.api.descriptor;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;

/**
 * ReportExecutionContext
 */
public interface IReportExecutionContext {

	/**
	 * @return
	 */
	public String getFormat();

	/**
	 * @param format
	 */
	public void setFormat(String format);

	/**
	 * @return
	 */
	public File getResultFile();

	/**
	 * @param resultFile
	 */
	public void setResultFile(File resultFile);

	/**
	 * @return
	 */
	public OutputStream getResultOutputStream();

	/**
	 * @param resultOutputStream
	 */
	public void setResultOutputStream(OutputStream resultOutputStream);

	/**
	 * @return
	 */
	public PrintWriter getResultWriter();

	/**
	 * @param resultWriter
	 */
	public void setResultWriter(PrintWriter resultWriter);

	/**
	 * @return
	 */
	public Map<String, Object> getParameters();

	/**
	 * @param parameters
	 */
	public void setParameters(Map<String, Object> parameters);

}
