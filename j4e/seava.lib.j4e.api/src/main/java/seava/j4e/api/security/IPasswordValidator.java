package seava.j4e.api.security;

/**
 * IPasswordValidator
 */
public interface IPasswordValidator {

	/**
	 * @param passwordToValidate
	 * @throws Exception
	 */
	public void validate(String passwordToValidate) throws Exception;
}
