/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * SortField
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {})
public @interface SortField {

	/**
	 * Field name to sort.<br>
	 * Not null.
	 *
	 * @return
	 */
	String field() default "";

	/**
	 * Descending sort. Default false.
	 *
	 * @return
	 */
	boolean desc() default false;
}
