/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.descriptor;

import java.util.Map;

/**
 * @param <M>
 */
public interface IViewModelDescriptor<M> {

	/**
	 * @return
	 */
	public Class<M> getModelClass();

	/**
	 * @return
	 */
	public Map<String, String> getRefPaths();

	/**
	 * @return
	 */
	public boolean isWorksWithJpql();

	/**
	 * @return
	 */
	public String getJpqlDefaultWhere();

	/**
	 * @return
	 */
	public String getJpqlDefaultSort();

	/**
	 * @return
	 */
	public Map<String, String> getJpqlFieldFilterRules();

	/**
	 * @return
	 */
	public Map<String, String> getFetchJoins();

	/**
	 * @return
	 */
	public Map<String, Object> getQueryHints();

	/**
	 * @return
	 */
	public Map<String, String[]> getOrderBys();
}
