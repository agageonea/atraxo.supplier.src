/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.result;

import java.util.List;
import java.util.Map;

/**
 * Action Result Find
 */
public interface IActionResultFind extends IActionResult {

	/**
	 * @return
	 */
	public Long getTotalCount();

	/**
	 * @param totalCount
	 */
	public void setTotalCount(Long totalCount);

	/**
	 * @return
	 */
	public List<?> getData();

	/**
	 * @param data
	 */
	public void setData(List<?> data);

	/**
	 * @return
	 */
	public Object getParams();

	/**
	 * @param params
	 */
	public void setParams(Object params);

	/**
	 * @return
	 */
	public Map<String, Object> getSummaries();

	/**
	 * @param summaries
	 */
	public void setSummaries(Map<String, Object> summaries);

}
