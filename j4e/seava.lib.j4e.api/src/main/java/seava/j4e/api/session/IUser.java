/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.session;

/**
 * IUser
 */
public interface IUser {

	/**
	 * @return
	 */
	public String getCode();

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @return
	 */
	public String getLoginName();

	/**
	 * @return
	 */
	public char[] getPassword();

	/**
	 * @param password
	 */
	public void setPassword(char[] password);

	/**
	 * @return
	 */
	public String getClientId();

	/**
	 * @return
	 */
	public String getClientCode();

	/**
	 * @return
	 */
	public String getEmployeeId();

	/**
	 * @return
	 */
	public String getEmployeeCode();

	/**
	 * @return
	 */
	public IClient getClient();

	/**
	 * @return
	 */
	public IUserSettings getSettings();

	/**
	 * @return
	 */
	public IUserProfile getProfile();

	/**
	 * @return
	 */
	public IWorkspace getWorkspace();

	/**
	 * @return
	 */
	public boolean isSystemUser();

}
