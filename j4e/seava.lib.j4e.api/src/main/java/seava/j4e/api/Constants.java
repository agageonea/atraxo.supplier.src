/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api;

/**
 * Constants
 */
public class Constants {

	/* =========================================================== */
	/* ========================= general ========================= */
	/* =========================================================== */

	public static final String STATUS = "status";

	/* =========================================================== */
	/* ==================== data type/format ===================== */
	/* =========================================================== */

	public static final String DATA_FORMAT_CSV = "csv";
	public static final String DATA_FORMAT_JSON = "json";
	public static final String DATA_FORMAT_XML = "xml";
	public static final String DATA_FORMAT_HTML = "html";
	public static final String DATA_FORMAT_PDF = "pdf";
	public static final String DATA_FORMAT_XLS = "xls";
	public static final String DATA_FORMAT_DOC = "doc";

	public static final String DATA_TYPE_STRING = "string";
	public static final String DATA_TYPE_BOOLEAN = "boolean";
	public static final String DATA_TYPE_NUMBER = "number";

	/* =========================================================== */
	/* =================== request parameters ==================== */
	/* =========================================================== */

	public static final String REQUEST_PARAM_THEME = "theme";
	public static final String REQUEST_PARAM_LANG = "lang";
	public static final String REQUEST_PARAM_ACTION = "action";
	public static final String REQUEST_PARAM_DATA = "data";
	public static final String REQUEST_DIALOG_REPORT_INFO = "dialogExportInfo";
	public static final String REQUEST_PARAM_FILTER = "data";
	public static final String REQUEST_PARAM_FILTER_ATTRIBUTES = "filterAttr";
	public static final String REQUEST_PARAM_ADVANCED_FILTER = "filter";
	public static final String REQUEST_PARAM_PARAMS = "params";
	public static final String REQUEST_PARAM_SORT = "orderByCol";
	public static final String REQUEST_PARAM_SENSE = "orderBySense";
	public static final String REQUEST_PARAM_START = "resultStart";
	public static final String REQUEST_PARAM_SIZE = "resultSize";
	public static final String REQUEST_PARAM_PAGE = "page";
	public static final String REQUEST_PARAM_ORDERBY = "orderBy";
	public static final String REQUEST_PARAM_EXPORT_INFO = "export_info";
	public static final String REQUEST_PARAM_EXPORT_DOWNLOAD = "download";
	public static final String REQUEST_PARAM_REPORT_CODE = "code";
	public static final String REQUEST_PARAM_EXCLUDE_FIELDS = "exclude_fields";
	public static final String REQUEST_PARAM_DATA_FORMAT = "dataFormat";

	public static final String REQUEST_PARAM_SERVICE_NAME_PARAM = "rpcName";
	public static final String REQUEST_PARAM_ASGN_OBJECT_ID = "objectId";
	public static final String REQUEST_PARAM_ASGN_SELECTION_ID = "selectionId";

	public static final String REQUEST_PARAM_REPORT_ID = "reportId";

	public static final String REQUEST_PARAM_CLIENT_NAME = "clientName";
	public static final String REQUEST_PARAM_CLIENT_CODE = "clientCode";
	public static final String REQUEST_PARAM_ADMIN_USER_CODE = "adminUserCode";
	public static final String REQUEST_PARAM_ADMIN_USER_NAME = "adminUserName";
	public static final String REQUEST_PARAM_ADMIN_LOGIN_NAME = "adminLoginName";
	public static final String REQUEST_PARAM_ADMIN_LOGIN_PASSWORD = "adminLoginPasword";
	public static final String REQUEST_PARAM_CALLBACK_URL = "callbackUrl";

	public static final String REQUEST_PARAM_FILE_NAME = "fileName";

	/* =========================================================== */
	/* =============== Request parameter action ================== */
	/* =========================================================== */

	public static final String DS_ACTION_INFO = "info";
	public static final String DS_ACTION_QUERY = "find";
	public static final String DS_ACTION_INSERT = "insert";
	public static final String DS_ACTION_UPDATE = "update";
	public static final String DS_ACTION_DELETE = "delete";
	public static final String DS_ACTION_SAVE = "save";
	public static final String DS_ACTION_IMPORT = "import";
	public static final String DS_ACTION_EXPORT = "export";
	public static final String DS_ACTION_REPORT = "report";
	public static final String DS_ACTION_DIALOG_REPORT = "dialogReport";
	public static final String DS_ACTION_DOWNLOAD_RPT_DESIGN = "downloadRPTDesign";
	public static final String DS_ACTION_DOWNLOAD_REPORT = "downloadReport";
	public static final String DS_ACTION_PRINT = "print";
	public static final String DS_ACTION_RPC = "rpc";
	public static final String DS_ACTION_RPC_TYPE = "rpcType";
	public static final String DS_ACTION_RPC_TYPE_DATA = "data";
	public static final String DS_ACTION_RPC_TYPE_FILTER = "filter";
	public static final String DS_ACTION_RPC_TYPE_DATALIST = "dataList";
	public static final String DS_ACTION_RPC_TYPE_IDLIST = "idList";
	public static final String DS_ACTION_NOTIFY = "notify";

	public static final String ASGN_ACTION_QUERY_LEFT = "findLeft";
	public static final String ASGN_ACTION_QUERY_RIGHT = "findRight";
	public static final String ASGN_ACTION_MOVE_LEFT = "moveLeft";
	public static final String ASGN_ACTION_MOVE_RIGHT = "moveRight";
	public static final String ASGN_ACTION_MOVE_LEFT_ALL = "moveLeftAll";
	public static final String ASGN_ACTION_MOVE_RIGHT_ALL = "moveRightAll";
	public static final String ASGN_ACTION_SETUP = "setup";
	public static final String ASGN_ACTION_RESET = "reset";
	public static final String ASGN_ACTION_SAVE = "save";
	public static final String ASGN_ACTION_CLEANUP = "cleanup";

	public static final String SESSION_ACTION_SHOW_LOGIN = "login";
	public static final String SESSION_ACTION_LOGIN = "doLogin";
	public static final String SESSION_ACTION_LOGOUT = "doLogout";
	public static final String SESSION_ACTION_LOCK = "doLock";
	public static final String SESSION_ACTION_CHANGEPASSWORD = "changePassword";
	public static final String SESSION_ACTION_SAML_LOGOUT = "samlLogout";
	public static final String SESSION_ACTION_SHOW_ERROR = "error";

	public static final String NEW_CLIENT = "newClient";

	/* =========================================================== */
	/* ================ Servlet/Context path ==================== */
	/* =========================================================== */

	/* Servlet paths defined in web.xml */
	public static final String SERVLETPATH_UI_EXTJS = "/ui-extjs";
	public static final String SERVLETPATH_SECURITY = "/security";
	public static final String SERVLETPATH_UPLOAD = "/upload";
	public static final String SERVLETPATH_DOWNLOAD = "/download";

	public static final String SERVLETPATH_WORKFLOW = "/workflow";
	public static final String SERVLETPATH_DATA = "/data";
	public static final String SERVLETPATH_EXTERNAL_REPORT = "/externalreport";

	/* Context paths */
	public static final String CTXPATH_SESSION = "/session";
	public static final String CTXPATH_ASGN = "/asgn";
	public static final String CTXPATH_DS = "/ds";

	public static final String URL_UI_EXTJS = SERVLETPATH_UI_EXTJS;
	public static final String URL_SESSION = SERVLETPATH_SECURITY + CTXPATH_SESSION;
	public static final String URL_UPLOAD = SERVLETPATH_UPLOAD;
	public static final String URL_DOWNLOAD = SERVLETPATH_DOWNLOAD;
	public static final String URL_WORKFLOW = SERVLETPATH_WORKFLOW;
	public static final String URL_EXTERNAL_REPORT = SERVLETPATH_EXTERNAL_REPORT;

	public static final String URL_DATA_DS = SERVLETPATH_DATA + CTXPATH_DS;
	public static final String URL_DATA_ASGN = SERVLETPATH_DATA + CTXPATH_ASGN;

	/* =========================================================== */
	/* ====================== cookie name ======================== */
	/* =========================================================== */

	public static final String COOKIE_NAME_THEME = "app-theme";
	public static final String COOKIE_NAME_LANG = "app-lang";

	/* =========================================================== */
	/* ================== spring bean aliases ==================== */
	/* =========================================================== */

	public static final String SPRING_OSGI_ENTITY_SERVICE_FACTORIES = "osgiEntityServiceFactories";
	public static final String SPRING_OSGI_DS_SERVICE_FACTORIES = "osgiDsServiceFactories";
	public static final String SPRING_OSGI_REPORT_SERVICE_FACTORIES = "osgiReportServiceFactories";
	public static final String SPRING_OSGI_ASGN_SERVICE_FACTORIES = "osgiAsgnServiceFactories";
	public static final String SPRING_OSGI_ASGN_TX_SERVICE_FACTORIES = "osgiAsgnTxServiceFactories";
	public static final String SPRING_OSGI_DS_DEFINITIONS = "osgiDsDefinitions";
	public static final String SPRING_OSGI_SYSPARAM_DEFINITIONS = "osgiSysParamDefinitions";
	public static final String SPRING_OSGI_JOB_DEFINITIONS = "osgiJobDefinitions";
	public static final String SPRING_OSGI_JOB_SCHEDULER = "osgiJobScheduler";
	public static final String SPRING_OSGI_PERSISTABLE_LOG_SERVICES = "osgiPersistableLogServices";
	public static final String SPRING_OSGI_INIT_DATA_PROVIDER_FACTORIES = "osgiInitDataProviderFactories";
	public static final String SPRING_OSGI_FILE_UPLOAD_SERVICE_FACTORIES = "osgiFileUploadServiceFactories";

	public static final String SPRING_OSGI_EXTENSION_PROVIDERS = "osgiExtensionProviders";
	public static final String SPRING_OSGI_EXTENSION_CONTENT_PROVIDERS = "osgiExtensionContentProviders";

	public static final String SPRING_DEFAULT_ASGN_TX_SERVICE = "defaultAsgnTxService";
	public static final String SPRING_AUTH_MANAGER = "authenticationManager";

	public static final String SPRING_MSG_IMPORT_DATA_FILE = "msgImportDataFile";

	public static final String SPRING_DSEXPORT_WRITER_CSV = "dsExportWriterCsv";
	public static final String SPRING_DSEXPORT_WRITER_XML = "dsExportWriterXml";
	public static final String SPRING_DSEXPORT_WRITER_JSON = "dsExportWriterJson";
	public static final String SPRING_DSEXPORT_WRITER_HTML = "dsExportWriterHtml";

	public static final String SPRING_DSREPORT_WRITER = "dsReportWriter";
	public static final String SPRING_DSDIALOGREPORT_WRITER = "dsDialogReportWriter";

	/* =========================================================== */
	/* ===================== quartz scheduler ==================== */
	/* =========================================================== */

	public static final String QUARTZ_JOB_NAME = "__JOB_NAME__";

	/* =========================================================== */
	/* ==================== application roles ==================== */
	/* =========================================================== */

	public static final String ROLE_ADMIN_CODE = "ADMIN";
	public static final String ROLE_ADMIN_NAME = "Administrator";
	public static final String ROLE_ADMIN_DESC = "Administrator role for un-restricted access to business functions";

	public static final String ROLE_USER_CODE = "CONNECT";
	public static final String ROLE_USER_NAME = "Application access";
	public static final String ROLE_USER_DESC = "Application role which allows a user to access the application";

	/* =========================================================== */
	/* =================== system properties ===================== */
	/* =========================================================== */

	public static final String PROP_WORKSPACE = "workspace";

	public static final String PROP_WORKING_MODE = "workingMode"; // dev, prod
	public static final String PROP_WORKING_MODE_DEV = "dev";
	public static final String PROP_WORKING_MODE_PROD = "prod";
	public static final String PROP_DEPLOYMENT = "deployment"; // jee, virgo
	public static final String PROP_DEPLOYMENT_JEE = "jee";
	public static final String PROP_DEPLOYMENT_VIRGO = "virgo";
	public static final String PROP_CTXPATH = "ctxpath";
	public static final String PROP_HELP_PATH = "helpPath";
	public static final String PROP_APPLICATION_MENU = "applicationMenu";
	public static final String PROP_APPLICATION_DASHBOARD = "applicationDashboard";
	public static final String PROP_APPLICATION_STYLECLASS = "actionsStyleClass";
	public static final String PROP_SHOW_VIEW_AND_FILTER_TOOLBAR = "showViewAndFilterToolbar";

	public static final String PROP_LOGIN_PAGE = "loginPage";
	public static final String PROP_LOGIN_PAGE_LOGO = "loginPageLogo";
	public static final String PROP_LOGIN_PAGE_CSS = "loginPageCss";

	public static final String PROP_SERVER_DATE_FORMAT = "serverDateFormat";
	public static final String PROP_SERVER_TIME_FORMAT = "serverTimeFormat";
	public static final String PROP_SERVER_DATETIME_FORMAT = "serverDateTimeFormat";
	public static final String PROP_SERVER_ALT_FORMATS = "serverAltFormats";
	public static final String PROP_EXTJS_MODEL_DATE_FORMAT = "extjsModelDateFormat";

	public static final String PROP_EXTJS_DATE_FORMAT = "extjsDateFormat";
	public static final String PROP_EXTJS_TIME_FORMAT = "extjsTimeFormat";
	public static final String PROP_EXTJS_DATETIME_FORMAT = "extjsDateTimeFormat";
	public static final String PROP_EXTJS_DATETIMESEC_FORMAT = "extjsDateTimeSecFormat";
	public static final String PROP_EXTJS_MONTH_FORMAT = "extjsMonthFormat";
	public static final String PROP_EXTJS_ALT_FORMATS = "extjsAltFormats";

	public static final String PROP_JAVA_DATE_FORMAT = "javaDateFormat";
	public static final String PROP_JAVA_TIME_FORMAT = "javaTimeFormat";
	public static final String PROP_JAVA_DATETIME_FORMAT = "javaDateTimeFormat";
	public static final String PROP_JAVA_DATETIMESEC_FORMAT = "javaDateTimeSecFormat";
	public static final String PROP_JAVA_MONTH_FORMAT = "javaMonthFormat";
	public static final String PROP_JAVA_ALT_FORMATS = "javaAltFormats";

	public static final String PROP_LANGUAGE = "language";
	public static final String PROP_NUMBER_FORMAT = "numberFormat";

	public static final String PROP_MAX_BUTTONS_ON_TOOLBARS = "maxButtonsOnToolbars";

	public static final String PROP_DISABLE_FETCH_GROUPS = "disableFetchGroups";

	public static final String PROP_SYS_AUTHENTICATION = "sysAuthentication";

	public static final String PROP_SYS_EXTERNAL_SCHEME = "sysExternalScheme";
	public static final String PROP_SYS_EXTERNAL_SERVER_NAME = "sysExternalServerName";
	public static final String PROP_SYS_EXTERNAL_SERVER_PORT = "sysExternalServerPort";
	public static final String PROP_SYS_EXTERNAL_CONTEXT_PATH = "sysExternalClontextPath";

	public static final String PROP_REPORT_CENTER_REPO = "reportCenterRepo";

	/* =========================================================== */
	/* ==================== default values ======================= */
	/* =========================================================== */

	public static final String DEFAULT_LANGUAGE = "en_US";
	public static final String DEFAULT_NUMBER_FORMAT = "0,000.00";
	public static final String DYNAMIC_NUMBER_FORMAT = "#,##0%s";

	public static final int DEFAULT_MAX_BUTTONS_ON_TOOLBARS = 7;

	private static String DEFAULT_SERVER_DATE_FORMAT = "yyyy-MM-dd";
	private static String DEFAULT_SERVER_TIME_FORMAT = "HH:mm:ss";
	private static String DEFAULT_SERVER_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static String DEFAULT_SERVER_ALT_FORMATS = "yyyy-MM-dd'T'HH:mm:ss;yyyy-MM-dd HH:mm:ss;yyyy-MM-dd'T'HH:mm;yyyy-MM-dd HH:mm;yyyy-MM-dd";
	private static String DEFAULT_EXTJS_MODEL_DATE_FORMAT = "Y-m-d\\TH:i:s";

	/* =========================================================== */
	/* ======================== colors =========================== */
	/* =========================================================== */

	public static final String COLOR_RED = "#ED5564";
	public static final String COLOR_ORANGE = "#FB6E52";
	public static final String COLOR_YELLOW = "#FFCE55";
	public static final String COLOR_GRASS_GREEN = "#A0D468";
	public static final String COLOR_LIGHT_GREEN = "#48CFAE";
	public static final String COLOR_LIGHT_BLUE = "#4FC0E8";
	public static final String COLOR_PURPLE = "#AC92ED";
	public static final String COLOR_PINK = "#EC87BF";
	public static final String COLOR_LIGHT_GRAY = "#F6F7FB";
	public static final String COLOR_MEDIUM_GRAY = "#CCD0D9";
	public static final String COLOR_DARK_GRAY = "#656D78";

	/* =========================================================== */
	/* ================ entity code allocation =================== */
	/* =========================================================== */

	/*
	 * Possible modes to allocate code for entities with code and name field
	 */
	/**
	 * Manually allocate code
	 */
	public static final int ENTITY_CODE_MANUAL = 1;
	/**
	 * Derive code from name.
	 */
	public static final int ENTITY_CODE_DERIVED = 2;
	/**
	 * Use a sequence to create a code from.
	 */
	public static final int ENTITY_CODE_SEQUENCE = 3;

	/* =========================================================== */
	/* ========================= others ========================== */
	/* =========================================================== */

	/**
	 * UUID_GENERATOR_NAME
	 */
	public static final String UUID_GENERATOR_NAME = "system-uuid";

	/**
	 * CURRENT_LANGUAGE
	 */
	private static String CURRENT_LANGUAGE = "";

	/* ========================= functions ========================== */

	private Constants() {
		throw new IllegalAccessError("Utility class");
	}

	/**
	 * Hack to expose the server date format masks as statics for property editors. The values are set by the {@link Settings} constructor
	 *
	 * @return
	 */
	public static final String get_server_date_format() {
		return DEFAULT_SERVER_DATE_FORMAT;
	}

	/**
	 * @return
	 */
	public static final String get_server_time_format() {
		return DEFAULT_SERVER_TIME_FORMAT;
	}

	/**
	 * @return
	 */
	public static final String get_server_datetime_format() {
		return DEFAULT_SERVER_DATETIME_FORMAT;
	}

	/**
	 * @return
	 */
	public static final String get_server_alt_formats() {
		return DEFAULT_SERVER_ALT_FORMATS;
	}

	/**
	 * @return
	 */
	public static final String get_extjs_model_date_format() {
		return DEFAULT_EXTJS_MODEL_DATE_FORMAT;
	}

	/**
	 * @param v
	 */
	public static final synchronized void set_server_date_format(String v) {
		DEFAULT_SERVER_DATE_FORMAT = v;
	}

	/**
	 * @param v
	 */
	public static final synchronized void set_server_time_format(String v) {
		DEFAULT_SERVER_TIME_FORMAT = v;
	}

	/**
	 * @param v
	 */
	public static final synchronized void set_server_datetime_format(String v) {
		DEFAULT_SERVER_DATETIME_FORMAT = v;
	}

	/**
	 * @param v
	 */
	public static final synchronized void set_server_alt_formats(String v) {
		DEFAULT_SERVER_ALT_FORMATS = v;
	}

	/**
	 * @param v
	 */
	public static final synchronized void set_extjs_model_date_format(String v) {
		DEFAULT_EXTJS_MODEL_DATE_FORMAT = v;
	}

	/**
	 * @return the current_language
	 */
	public static String get_current_language() {
		return CURRENT_LANGUAGE;
	}

	/**
	 * @param current_language
	 */
	public static synchronized void set_current_language(String current_language) {
		CURRENT_LANGUAGE = current_language;
	}

}
