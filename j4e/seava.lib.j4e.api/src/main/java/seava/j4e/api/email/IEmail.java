package seava.j4e.api.email;

import seava.j4e.api.exceptions.EmailException;

/**
 * Email
 */
public interface IEmail {

	public static final int TYPE_TEXT = 1;
	public static final int TYPE_HTML = 2;

	/**
	 * @param d
	 */
	public void setDebug(boolean d);

	/**
	 * @param email
	 * @return
	 * @throws EmailException
	 */
	public IEmail addBcc(String email) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @return
	 * @throws EmailException
	 */
	public IEmail addBcc(String email, String name) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @param charset
	 * @return
	 * @throws EmailException
	 */
	public IEmail addBcc(String email, String name, String charset) throws EmailException;

	/**
	 * @param email
	 * @return
	 * @throws EmailException
	 */
	public IEmail addCc(String email) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @return
	 * @throws EmailException
	 */
	public IEmail addCc(String email, String name) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @param charset
	 * @return
	 * @throws EmailException
	 */
	public IEmail addCc(String email, String name, String charset) throws EmailException;

	/**
	 * @param name
	 * @param value
	 */
	public void addHeader(String name, String value);

	/**
	 * @param email
	 * @return
	 * @throws EmailException
	 */
	public IEmail addReplyTo(String email) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @return
	 * @throws EmailException
	 */
	public IEmail addReplyTo(String email, String name) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @param charset
	 * @return
	 * @throws EmailException
	 */
	public IEmail addReplyTo(String email, String name, String charset) throws EmailException;

	/**
	 * @param email
	 * @return
	 * @throws EmailException
	 */
	public IEmail addTo(String email) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @return
	 * @throws EmailException
	 */
	public IEmail addTo(String email, String name) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @param charset
	 * @return
	 * @throws EmailException
	 */
	public IEmail addTo(String email, String name, String charset) throws EmailException;

	/**
	 * @param aSubject
	 * @return
	 */
	public IEmail setSubject(String aSubject);

	/**
	 * @return
	 * @throws EmailException
	 */
	public String send() throws EmailException;

	/**
	 * @param msg
	 * @return
	 * @throws EmailException
	 */
	public IEmail setMsg(String msg) throws EmailException;

	/**
	 * @param email
	 * @return
	 * @throws EmailException
	 */
	public IEmail setFrom(String email) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @return
	 * @throws EmailException
	 */
	public IEmail setFrom(String email, String name) throws EmailException;

	/**
	 * @param email
	 * @param name
	 * @param charset
	 * @return
	 * @throws EmailException
	 */
	public IEmail setFrom(String email, String name, String charset) throws EmailException;

	/**
	 * @param newCharset
	 */
	public void setCharset(String newCharset);
}
