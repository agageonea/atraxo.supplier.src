/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.exceptions;

/**
 * ActionNotSupportedException
 */
public class ActionNotSupportedException extends AbstractException {

	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 */
	public ActionNotSupportedException() {
		super("Action not supported.");
	}

	/**
	 * @param message
	 */
	public ActionNotSupportedException(String message) {
		super(message);
	}

}