package seava.j4e.api.service;

/**
 * PersistableLogMessage
 */
public class PersistableLogMessage implements IPersistableLogMessage {

	private String type;
	private String message;

	/**
	 * @param type
	 * @param message
	 */
	public PersistableLogMessage(String type, String message) {
		super();
		this.type = type;
		this.message = message;
	}

	@Override
	public String getType() {
		return this.type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;
	}

}
