/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.descriptor;

/**
 * UploadFileDescriptor
 */
public interface IUploadedFileDescriptor {

	/**
	 * @return
	 */
	public String getOriginalName();

	/**
	 * @param originalName
	 */
	public void setOriginalName(String originalName);

	/**
	 * @return
	 */
	public String getNewName();

	/**
	 * @param newName
	 */
	public void setNewName(String newName);

	/**
	 * @return
	 */
	public String getContentType();

	/**
	 * @param contentType
	 */
	public void setContentType(String contentType);

	/**
	 * @return
	 */
	public long getSize();

	/**
	 * @param size
	 */
	public void setSize(long size);
}
