package seava.j4e.api.email;

import seava.j4e.api.exceptions.EmailException;

/**
 * HtmlEmail
 */
public interface IHtmlEmail extends IEmail {

	/**
	 * @param aHtml
	 * @return
	 * @throws EmailException
	 */
	public IHtmlEmail setHtmlMsg(String aHtml) throws EmailException;

	/**
	 * @param aText
	 * @return
	 * @throws EmailException
	 */
	public IHtmlEmail setTextMsg(String aText) throws EmailException;

}
