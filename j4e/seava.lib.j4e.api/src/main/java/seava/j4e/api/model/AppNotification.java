package seava.j4e.api.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zspeter
 */
public class AppNotification {

	private Integer id;
	private String title;
	private String description;
	private String priority;
	private String category;
	private Date eventDate;
	private Date lifeTime;
	private String action;
	private String link;
	private String clientId;
	private String methodName;
	private String methodParam;
	private List<String> users;

	/**
	 * @param id
	 * @param title
	 * @param description
	 * @param clientId
	 */
	public AppNotification(Integer id, String title, String description, String clientId) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.clientId = clientId;
		this.users = new ArrayList<>();
	}

	/**
	 * @param id
	 */
	public void addToUsers(String id) {
		if (this.users == null) {
			this.users = new ArrayList<>();
		}
		this.users.add(id);
	}

	public List<String> getUsers() {
		return this.users;
	}

	public void setUsers(List<String> users) {
		this.users = users;
	}

	public String getPriority() {
		return this.priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getEventDate() {
		return this.eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getLifeTime() {
		return this.lifeTime;
	}

	public void setLifeTime(Date lifeTime) {
		this.lifeTime = lifeTime;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getId() {
		return this.id;
	}

	public String getTitle() {
		return this.title;
	}

	public String getDescription() {
		return this.description;
	}

	public String getClientId() {
		return this.clientId;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMethodParam() {
		return this.methodParam;
	}

	public void setMethodParam(String methodParam) {
		this.methodParam = methodParam;
	}
}
