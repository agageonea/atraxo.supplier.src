/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.query;

/**
 * Sort Token
 */
public interface ISortToken {

	/**
	 * @return
	 */
	public String getProperty();

	/**
	 * @param property
	 */
	public void setProperty(String property);

	/**
	 * @return
	 */
	public String getDirection();

	/**
	 * @param direction
	 */
	public void setDirection(String direction);

}
