/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.result;

import java.util.List;

import javax.persistence.EntityManager;

/**
 * Interface to be implemented by a view-object entity converter
 *
 * @author amathe
 * @param <M>
 * @param <E>
 */
public interface IDsConverter<M, E> {

	/**
	 * @param m
	 * @param e
	 * @param isInsert
	 * @param em
	 * @throws Exception
	 */
	public void modelToEntity(M m, E e, boolean isInsert, EntityManager em) throws Exception;

	/**
	 * @param e
	 * @param m
	 * @param em
	 * @param fieldNames
	 * @throws Exception
	 */
	public void entityToModel(E e, M m, EntityManager em, List<String> fieldNames) throws Exception;

	/**
	 * @param entities
	 * @param em
	 * @param fieldNames
	 * @return
	 * @throws Exception
	 */
	public List<M> entitiesToModels(List<E> entities, EntityManager em, List<String> fieldNames) throws Exception;

}
