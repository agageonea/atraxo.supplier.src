/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.descriptor;

import java.util.Collection;

/**
 * DsDefinitions
 */
public interface IDsDefinitions {

	/**
	 * Check if this context contains the given data-source definition.
	 *
	 * @param name
	 * @return
	 */
	public boolean containsDs(String name);

	/**
	 * Returns the definition for the given data-source name.
	 *
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public IDsDefinition getDsDefinition(String name) throws Exception;

	/**
	 * Returns a collection with all the data-source definitions from this context.
	 *
	 * @return
	 * @throws Exception
	 */
	public Collection<IDsDefinition> getDsDefinitions() throws Exception;
}
