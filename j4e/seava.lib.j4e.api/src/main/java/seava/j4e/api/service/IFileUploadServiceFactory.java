/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service;

/**
 * IFileUploadServiceFactory
 */
public interface IFileUploadServiceFactory {

	/**
	 * @param key
	 * @return
	 */
	public IFileUploadService create(String key);

}
