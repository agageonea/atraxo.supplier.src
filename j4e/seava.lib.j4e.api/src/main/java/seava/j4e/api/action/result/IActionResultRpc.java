package seava.j4e.api.action.result;

/**
 * Action Result RPC
 */
public interface IActionResultRpc extends IActionResult {

	/**
	 * @return
	 */
	public Object getData();

	/**
	 * @param data
	 */
	public void setData(Object data);

	/**
	 * @return
	 */
	public Object getParams();

	/**
	 * @param params
	 */
	public void setParams(Object params);

}
