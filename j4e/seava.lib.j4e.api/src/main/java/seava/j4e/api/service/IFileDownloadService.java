package seava.j4e.api.service;

import java.io.InputStream;

/**
 * IFileDownloadService
 */
public interface IFileDownloadService {

	/**
	 * @param string
	 * @throws Exception
	 */
	public InputStream execute(String id, String name) throws Exception;

}
