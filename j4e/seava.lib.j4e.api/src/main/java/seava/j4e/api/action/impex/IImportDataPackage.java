package seava.j4e.api.action.impex;

import java.util.List;

/**
 * Import Data Package
 */
public interface IImportDataPackage {

	/**
	 * @return
	 */
	public String getLocation();

	/**
	 * @param location
	 */
	public void setLocation(String location);

	/**
	 * @param dataSet
	 */
	public void addToList(IImportDataSet dataSet);

	/**
	 * @return
	 */
	public List<IImportDataSet> getDataSets();

	/**
	 * @param dataSets
	 */
	public void setDataSets(List<IImportDataSet> dataSets);

}
