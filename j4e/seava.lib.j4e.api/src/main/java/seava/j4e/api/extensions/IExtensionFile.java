package seava.j4e.api.extensions;

/**
 * IExtensionFile
 */
public interface IExtensionFile {

	/**
	 * Returns true if this is a .js file
	 *
	 * @return
	 * @throws Exception
	 */
	public abstract boolean isJs() throws Exception;

	/**
	 * Returns true if this is a .css file
	 *
	 * @return
	 */
	public abstract boolean isCss();

	/**
	 * @return
	 */
	public abstract String getFileExtension();

	/**
	 * @return
	 */
	public abstract String getLocation();

	/**
	 * @param location
	 */
	public abstract void setLocation(String location);

	/**
	 * @return
	 */
	public abstract boolean isRelativePath();

	/**
	 * @param relativePath
	 */
	public abstract void setRelativePath(boolean relativePath);

}