/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.business;

/**
 * IEntityServiceFactory
 */
public interface IEntityServiceFactory {

	/**
	 * @param key
	 * @return
	 */
	public <E> IEntityService<E> create(String key);

	/**
	 * @param type
	 * @return
	 */
	public <E> IEntityService<E> create(Class<E> type);
}
