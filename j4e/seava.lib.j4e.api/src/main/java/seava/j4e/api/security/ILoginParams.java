package seava.j4e.api.security;

/**
 * ILoginParams
 */
public interface ILoginParams {

	/**
	 * @return
	 */
	public abstract String getClientCode();

	/**
	 * @param clientCode
	 */
	public abstract void setClientCode(String clientCode);

	/**
	 * @return
	 */
	public abstract String getLanguage();

	/**
	 * @param language
	 */
	public abstract void setLanguage(String language);

	/**
	 * @return
	 */
	public abstract String getUserAgent();

	/**
	 * @param userAgent
	 */
	public abstract void setUserAgent(String userAgent);

	/**
	 * @return
	 */
	public abstract String getRemoteHost();

	/**
	 * @param remoteHost
	 */
	public abstract void setRemoteHost(String remoteHost);

	/**
	 * @return
	 */
	public abstract String getRemoteIp();

	/**
	 * @param remoteIp
	 */
	public abstract void setRemoteIp(String remoteIp);

}