/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.presenter;

import java.util.List;
import java.util.Map;

import seava.j4e.api.action.impex.IDialogReportInfo;
import seava.j4e.api.action.impex.IDsExport;
import seava.j4e.api.action.impex.IDsReport;
import seava.j4e.api.action.impex.IExportInfo;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.IQueryBuilder;
import seava.j4e.api.action.query.ISortToken;
import seava.j4e.api.session.IUser;

/**
 * IDsReadService
 *
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IDsReadService<M, F, P> extends IDsRpcService<M, F, P> {

	/**
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public M findById(Object id) throws Exception;

	/**
	 * @param id
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public M findById(Object id, P params) throws Exception;

	/**
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public List<M> findByIds(List<Object> ids) throws Exception;

	/**
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter) throws Exception;

	/**
	 * @param filter
	 * @param resultStart
	 * @param resultSize
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter, int resultStart, int resultSize) throws Exception;

	/**
	 * @param filter
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter, P params) throws Exception;

	/**
	 * @param filter
	 * @param params
	 * @param resultStart
	 * @param resultSize
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter, P params, int resultStart, int resultSize) throws Exception;

	/**
	 * @param filter
	 * @param filterRules
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter, List<IFilterRule> filterRules) throws Exception;

	/**
	 * @param filter
	 * @param filterRules
	 * @param resultStart
	 * @param resultSize
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter, List<IFilterRule> filterRules, int resultStart, int resultSize) throws Exception;

	/**
	 * @param filter
	 * @param params
	 * @param filterRules
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter, P params, List<IFilterRule> filterRules) throws Exception;

	/**
	 * @param filter
	 * @param params
	 * @param filterRules
	 * @param resultStart
	 * @param resultSize
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter, P params, List<IFilterRule> filterRules, int resultStart, int resultSize) throws Exception;

	/**
	 * @param filter
	 * @param params
	 * @param filterRules
	 * @param sortTokens
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter, P params, List<IFilterRule> filterRules, List<ISortToken> sortTokens) throws Exception;

	/**
	 * @param filter
	 * @param params
	 * @param filterRules
	 * @param resultStart
	 * @param resultSize
	 * @param sortTokens
	 * @return
	 * @throws Exception
	 */
	public List<M> find(F filter, P params, List<IFilterRule> filterRules, int resultStart, int resultSize, List<ISortToken> sortTokens)
			throws Exception;

	/**
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public List<M> find(IQueryBuilder<M, F, P> builder) throws Exception;

	/**
	 * @param builder
	 * @param fieldNames
	 * @return
	 * @throws Exception
	 */
	public List<M> find(IQueryBuilder<M, F, P> builder, List<String> fieldNames) throws Exception;

	/**
	 * @param builder
	 * @return
	 * @throws Exception
	 */
	public Long count(IQueryBuilder<M, F, P> builder) throws Exception;

	/**
	 * @param builder
	 * @param findResult
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> summaries(IQueryBuilder<M, F, P> builder, List<M> findResult) throws Exception;

	/**
	 * @param builder
	 * @param writer
	 * @throws Exception
	 */
	public void doExport(IQueryBuilder<M, F, P> builder, IDsExport<M> writer) throws Exception;

	/**
	 * @param builder
	 * @param writer
	 * @param exportInfo
	 * @param userCode
	 * @param clientCode
	 * @throws Exception
	 */
	public void doReport(IQueryBuilder<M, F, P> builder, IDsReport<M, F> writer, IExportInfo exportInfo, IUser user) throws Exception;

	/**
	 * @param report
	 * @param reportInfo
	 * @throws Exception
	 */
	public void doDialogReport(IDsReport<M, F> report, IDialogReportInfo reportInfo) throws Exception;

	/**
	 * @return
	 * @throws Exception
	 */
	public IQueryBuilder<M, F, P> createQueryBuilder() throws Exception;

	/**
	 * @param dataFormat
	 * @return
	 * @throws Exception
	 */
	public IDsExport<M> createExporter(String dataFormat) throws Exception;

}
