package seava.j4e.api.service;

/**
 * IPersistableLogMessage
 */
public interface IPersistableLogMessage {

	public static final String INFO = "info";
	public static final String ERROR = "error";
	public static final String WARNING = "warning";

	/**
	 * @return
	 */
	public String getType();

	/**
	 * @param type
	 */
	public void setType(String type);

	/**
	 * @return
	 */
	public String getMessage();

	/**
	 * @param message
	 */
	public void setMessage(String message);

}
