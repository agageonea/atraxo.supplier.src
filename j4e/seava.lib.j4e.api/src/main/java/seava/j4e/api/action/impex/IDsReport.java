package seava.j4e.api.action.impex;

import seava.j4e.api.exceptions.BusinessException;

/**
 * @param <M>
 * @param <F>
 */
public interface IDsReport<M, F> {

	/**
	 * @return
	 * @throws BusinessException
	 */
	public String toXmlString() throws BusinessException;

}
