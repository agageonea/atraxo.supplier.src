/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.session;

/**
 * Session
 */
public class Session {

	public static final ThreadLocal<IUser> user = new ThreadLocal<>();

	private Session() {
		throw new IllegalAccessError("Utility class");
	}
}
