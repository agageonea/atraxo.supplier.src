/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.result;

import java.io.OutputStream;
import java.util.List;

import seava.j4e.api.action.impex.IDialogReportInfo;
import seava.j4e.api.action.impex.IExcludeInfo;
import seava.j4e.api.action.impex.IExportInfo;
import seava.j4e.api.action.query.IFilterRule;
import seava.j4e.api.action.query.ISortToken;

/**
 * Ds Marshaller
 *
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IDsMarshaller<M, F, P> {

	public static final String XML = "xml";
	public static final String JSON = "json";
	public static final String CSV = "csv";

	/**
	 * @return
	 */
	public Object getDelegate();

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public IDialogReportInfo readDialogReportInfo(String source) throws Exception;

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public IExportInfo readExportInfo(String source) throws Exception;

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public List<ISortToken> readSortTokens(String source) throws Exception;

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public List<IFilterRule> readFilterRules(String source) throws Exception;

	/**
	 * @param source
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public <T> T readDataFromString(String source, Class<T> type) throws Exception;

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public M readDataFromString(String source) throws Exception;

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public List<M> readListFromString(String source) throws Exception;

	/**
	 * @param source
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public <T> List<T> readListFromString(String source, Class<T> type) throws Exception;

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public F readFilterFromString(String source) throws Exception;

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public P readParamsFromString(String source) throws Exception;

	/**
	 * @param m
	 * @return
	 * @throws Exception
	 */
	public String writeDataToString(M m) throws Exception;

	/**
	 * @param list
	 * @return
	 * @throws Exception
	 */
	public String writeListToString(List<M> list) throws Exception;

	/**
	 * @param f
	 * @return
	 * @throws Exception
	 */
	public String writeFilterToString(F f) throws Exception;

	/**
	 * @param p
	 * @return
	 * @throws Exception
	 */
	public String writeParamsToString(P p) throws Exception;

	/**
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public String writeResultToString(IActionResultFind result) throws Exception;

	/**
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public String writeResultToString(IActionResultSave result) throws Exception;

	/**
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public String writeResultToString(IActionResultDelete result) throws Exception;

	/**
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public String writeResultToString(IActionResultRpc result) throws Exception;

	/**
	 * @param m
	 * @param out
	 * @throws Exception
	 */
	public void writeDataToStream(M m, OutputStream out) throws Exception;

	/**
	 * @param list
	 * @param out
	 * @throws Exception
	 */
	public void writeListToStream(List<M> list, OutputStream out) throws Exception;

	/**
	 * @param f
	 * @param out
	 * @throws Exception
	 */
	public void writeFilterToStream(F f, OutputStream out) throws Exception;

	/**
	 * @param p
	 * @param out
	 * @throws Exception
	 */
	public void writeParamsToStream(P p, OutputStream out) throws Exception;

	/**
	 * @param result
	 * @param out
	 * @throws Exception
	 */
	public void writeResultToStream(IActionResultFind result, OutputStream out) throws Exception;

	/**
	 * @param result
	 * @param out
	 * @throws Exception
	 */
	public void writeResultToStream(IActionResultSave result, OutputStream out) throws Exception;

	/**
	 * @param result
	 * @param out
	 * @throws Exception
	 */
	public void writeResultToStream(IActionResultRpc result, OutputStream out) throws Exception;

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public IExcludeInfo readExcludeInfo(String source) throws Exception;
}
