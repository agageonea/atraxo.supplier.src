package seava.j4e.api.service.job;

/**
 * IScheduler
 */
public interface IScheduler {

	/**
	 * @throws Exception
	 */
	public void init() throws Exception;

	/**
	 * @throws Exception
	 */
	public void start() throws Exception;

	/**
	 * @throws Exception
	 */
	public void stop() throws Exception;

	/**
	 * @return
	 * @throws Exception
	 */
	public Object getDelegate() throws Exception;
}
