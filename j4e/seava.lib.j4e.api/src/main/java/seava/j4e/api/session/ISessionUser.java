/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.session;

import java.util.Date;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * Session user object which contains intrinsic user data and session related information (connection time, remote address, etc)
 *
 * @author amathe
 */
public interface ISessionUser extends UserDetails {

	/**
	 * @return
	 */
	public IUser getUser();

	/**
	 * @return
	 */
	public String getUserAgent();

	/**
	 * @return
	 */
	public Date getLoginDate();

	/**
	 * @return
	 */
	public String getRemoteHost();

	/**
	 * @return
	 */
	public String getRemoteIp();

	/**
	 * @return
	 */
	public boolean isSessionLocked();

	/**
	 * lockSession
	 */
	public void lockSession();

}
