/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.setup;

/**
 * IStartupParticipant
 */
public interface IStartupParticipant {

	/**
	 * @throws Exception
	 */
	public void execute() throws Exception;

}
