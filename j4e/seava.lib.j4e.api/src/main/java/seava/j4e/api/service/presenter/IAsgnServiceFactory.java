/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.service.presenter;

/**
 * IAsgnServiceFactory
 */
public interface IAsgnServiceFactory {

	/**
	 * @param key
	 * @return
	 */
	public <M, F, P> IAsgnService<M, F, P> create(String key);
}
