package seava.j4e.api.action.impex;

/**
 * @author apetho
 */
public interface ICreateClient {

	void newClient(String clientName, String clientCode, String adminUserCode, String adminUserName, String adminLoginName, String adminLoginPasword,
			String callbackUrl) throws Exception;

}
