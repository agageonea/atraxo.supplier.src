/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.action.result;

/**
 * Action Result Delete
 */
public interface IActionResultDelete extends IActionResult {

}
