/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.api.session;

/**
 * IClient
 */
public interface IClient {

	/**
	 * @return
	 */
	public String getId();

	/**
	 * @return
	 */
	public String getCode();

	/**
	 * @return
	 */
	public String getName();

	/**
	 * @return
	 */
	public String getActiveSubsidiaryId();
}
