package seava.j4e.iata.formatter;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Formatter for Iata IATABDT:D182Base type;
 *
 * @author zspeter
 */
public class D182BaseFormatter {

	private D182BaseFormatter() {
	}

	/**
	 * @param value
	 * @return
	 */
	public static String printBigDecimal(BigDecimal value) {
		BigDecimal scaledValue = value.setScale(2, RoundingMode.HALF_UP);
		return scaledValue.toString();

	}

	/**
	 * @param value
	 * @return
	 */
	public static BigDecimal parseBigDecimal(String value) {
		return new BigDecimal(value);
	}

}
