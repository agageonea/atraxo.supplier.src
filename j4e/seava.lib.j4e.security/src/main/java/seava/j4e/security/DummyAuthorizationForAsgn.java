/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.security;

import seava.j4e.api.security.IAuthorization;

public class DummyAuthorizationForAsgn implements IAuthorization {

	@Override
	public void authorize(String dsName, String action, String rpcMethod) throws Exception {
		// If it doesn't throw exception is authorized
	}

}
