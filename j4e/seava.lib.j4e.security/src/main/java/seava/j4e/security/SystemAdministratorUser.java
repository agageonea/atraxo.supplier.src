/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.security;

import java.util.List;

public class SystemAdministratorUser {

	private final String code;
	private final String name;
	private final String loginName;
	private final String password;

	private List<String> roles;

	public SystemAdministratorUser(String code, String name, String loginName, String password) {
		super();
		this.code = code;
		this.name = name;
		this.loginName = loginName;
		this.password = password;

	}

	public String getCode() {
		return this.code;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public String getPassword() {
		return this.password;
	}

	public String getName() {
		return this.name;
	}

	public List<String> getRoles() {
		return this.roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

}
