/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code package.
 */
package seava.j4e.security;

import java.util.ArrayList;
import java.util.List;

public class SystemAdministratorUsers {

	private List<SystemAdministratorUser> users;

	public List<SystemAdministratorUser> getUsers() {
		return this.users;
	}

	public void setUsers(List<SystemAdministratorUser> users) {
		this.users = users;
	}

	public void addUser(SystemAdministratorUser user) {
		if (this.users == null) {
			this.users = new ArrayList<SystemAdministratorUser>();
		}
		this.users.add(user);
	}

	public SystemAdministratorUser findByUserName(String username) {
		for (SystemAdministratorUser u : this.users) {
			if (u.getLoginName().equals(username)) {
				return u;
			}
		}
		return null;
	}
}
