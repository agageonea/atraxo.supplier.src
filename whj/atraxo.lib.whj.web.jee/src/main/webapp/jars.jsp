<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ page import="java.net.URLClassLoader"%>
<%@ page import="java.net.URL"%>
<%@ page import="java.io.PrintWriter"%>

<%
	ClassLoader cl = ClassLoader.getSystemClassLoader();

	URL[] urls = ((URLClassLoader) cl).getURLs();

	for (URL url : urls) {
		out.println(url.getFile() + "</br>");
	}
%>

